﻿using System.Text.RegularExpressions;

namespace HFC.LicensingMultisite.Biz.Extensions
{
    public static class StringExtension
    {
        public static string RemoveAllNonNumericCharacters(this string oldString)
        {
            return Regex.Replace(oldString, "[^.0-9]", "");
        }
    }
}
