﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz.Extensions
{
    public static class EmailSmtpExtension
    {
        public static bool Send(this MailMessage msg)
        {
            bool success = false;
            SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["cc_smtp_host"], int.Parse(ConfigurationManager.AppSettings["cc_smtp_port"]));

            try
            {
                client.Send(msg);
                success = true;
            }
            catch (SmtpException smtpEx)
            {
                List<string> addlInfo = new List<string>();
                addlInfo.Add("Email: " + msg.To[0].Address);
                addlInfo.Add("Status Code: " + smtpEx.StatusCode);
            }
            catch (Exception ex)
            {
                throw new System.InvalidOperationException(ex.ToString());
            }

            return success;
        }
    }
}
