﻿using HFC.LicensingMultisite.Biz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;

namespace HFC.LicensingMultisite.Biz
{
    public class PageInitializer
    {
        private Page _page;
        public string CurrentUtmCampaign { get { return string.IsNullOrEmpty(_page.QueryStringFromRawUrl("utm_campaign")) ? CookieManager.UtmCampaign : _page.QueryStringFromRawUrl("utm_campaign"); } }

        public string InitPage(Page page)
        {
            _page = page;
            return initPageInfo(page);
        }

        private string initPageInfo(Page page)
        {
            setCookieIfEligible();
            return getPhone();
        }

        private void setCookieIfEligible()
        {
            if (CookieManager.First_Visit == null)
                CookieManager.First_Visit = DateTime.UtcNow.ToString();
            
            
            if (!string.IsNullOrEmpty(this.CurrentUtmCampaign))
                CookieManager.UtmCampaign = this.CurrentUtmCampaign;
        }

        private string getPhone()
        {

            if ( string.IsNullOrEmpty(CookieManager.UtmCampaign))
                return UTMPhoneLookup.CorporatePhoneSwapFor(_page);
            return string.Empty;
        }
    }
}
