﻿using Ektron.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;

namespace HFC.LicensingMultisite.Biz
{
    public static class PageHelper
    {
        public static string QueryStringFor(this Page page, string name)
        {
            return page.Request.QueryString[name];
        }

        public static string QueryStringFor(this Control ctrl, string name)
        {
            if (ctrl.Page.Request.QueryString[name] == null) return string.Empty;
            return ctrl.Page.Request.QueryString[name];
        }

        public static string QueryStringFromRawUrl(this Page page, string name)
        {
            var url = page.Request.RawUrl;
            if (!url.Contains('?')) return string.Empty;

            var qstring = url.Substring(url.LastIndexOf('?'));

            var querystring = HttpUtility.ParseQueryString(qstring);

            if (querystring.GetValues(name) == null) return string.Empty;

            return querystring.GetValues(name)[0];
        }

        public static UserLocationData UserLocationData(this Page page)
        {
            return UserContext.GetLocationInfo(HttpContext.Current.Request["remote_addr"]);
        }
    }
}
