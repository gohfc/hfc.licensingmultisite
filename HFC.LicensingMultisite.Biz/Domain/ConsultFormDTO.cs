﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace HFC.LicensingMultisite.Biz
{
    public class ConsultFormDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PostalCode { get; set; }
        public string Referrer { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string LeadSource { get; set; }
        public string Country { get; set; }
        public string RequestUrl { get; set; }
        public string RequestType { get; set; }
        public string UtmCampaign { get; set; }
        public string IP { get; set; } 
        public string FirstVisitDateUTC { get; set; }

    }   
}
