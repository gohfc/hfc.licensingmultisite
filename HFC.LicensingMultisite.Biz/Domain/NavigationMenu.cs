﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ektron.Cms.Organization;

namespace HFC.LicensingMultisite.Biz
{
    public class NavigationMenu
    {
        public static NavigationMenu Make(IMenuData menu)
        {
            return new NavigationMenu { Menu = menu };
        }

        public IMenuData Menu { get; private set; }

        public IList<IMenuItemData> Desktop { get { return Menu.Items.Where(x => x.Text.ToLower() == "desktop").SingleOrDefault().Items.ToList(); } }
        public IList<IMenuItemData> Mobile { get { return Menu.Items.Where(x => x.Text.ToLower() == "mobile").SingleOrDefault().Items.ToList(); } }

    }
}
