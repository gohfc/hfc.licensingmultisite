﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    public class QueueDTO
    {
        public RequestInHomeFormDTO RequestInHomeFormDTO { get; set; }
        public int RequestID { get; set; }
        public int AttemptCount { get; set; }
        public DateTimeOffset? LastUpdated { get; set; }
    }
}
