﻿
namespace HFC.LicensingMultisite.Biz
{
    public class RequestInHomeFormDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PostalCode { get; set; }
        public string Referrer { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string RequestUrl { get; set; }
        public string RequestType { get; set; }
        public string UtmCampaign { get; set; }
        public string IP { get; set; }
    }   
}
