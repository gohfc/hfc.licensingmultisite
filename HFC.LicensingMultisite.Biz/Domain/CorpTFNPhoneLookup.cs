﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    public class CorpTFNPhoneLookup
    {
        public static CorpTFNPhoneLookup Make(string urlVariable, string country, string phone)
        {
            return new CorpTFNPhoneLookup
            {
                URLVariable = urlVariable,
                Country = country,
                Phone = phone
            };
        }

        public string URLVariable { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
    }

    public static partial class ext
    {
        public static IList<CorpTFNPhoneLookup> ToCorpTFN(this  EKCorpTFNPhoneLookup.root root)
        {
            var list = new List<CorpTFNPhoneLookup>();
            if (root != null && root.TFNItems != null)
                root.TFNItems.ToList().ForEach(x => list.Add(CorpTFNPhoneLookup.Make(x.URLVariable, x.Country, x.Phone)));
            return list;
        }
    }
}
