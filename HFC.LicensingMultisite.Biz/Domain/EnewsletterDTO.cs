﻿
namespace HFC.LicensingMultisite.Biz
{
    public sealed class EnewsletterDTO
    {
        //FK in CampMailer
        public byte Concept { get; set; }

        public string Email { get; set; }

        public string ZipCode { get; set; }

        public bool IsUnsub { get; set; }

        public string Referrer { get; set; }

        public string RequestUrl { get; set; }
    }
}
