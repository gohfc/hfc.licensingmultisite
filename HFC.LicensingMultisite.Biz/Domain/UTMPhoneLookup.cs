﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace HFC.LicensingMultisite.Biz
{
    public sealed class UTMPhoneLookup
    {
        public static string CorporatePhoneSwapFor(Page page)
        {
            var geoCity = "us";

            var phone = string.Empty;

            if (page.UserLocationData() != null && page.UserLocationData().CountryCode.ToLower() == "ca") { geoCity = "canada"; }
            var currentUtm = string.IsNullOrEmpty(page.QueryStringFromRawUrl("utm_campaign")) ? CookieManager.UtmCampaign : page.QueryStringFromRawUrl("utm_campaign");

            if (!string.IsNullOrEmpty(currentUtm))
            {
                var result = SiteManager.GetCorpTFNPhoneLookup();
                var tfnPhone = result.Where(x => (x.Country.ToLower() == geoCity && x.URLVariable.ToLower() == currentUtm.ToLower())).FirstOrDefault();
                if (tfnPhone != null)
                {
                    phone = tfnPhone.Phone;
                }
                else
                {
                    tfnPhone = result.Where(x => (x.Country.ToLower() == geoCity && x.URLVariable.ToLower() == "default")).FirstOrDefault();
                    if (tfnPhone != null)
                         phone = tfnPhone.Phone;
                }                
            }

            else
            {
                var result = SiteManager.GetCorpTFNPhoneLookup();
                var tfnPhone = result.Where(x => (x.Country.ToLower() == geoCity && x.URLVariable.ToLower() == "default")).FirstOrDefault();
                if (tfnPhone != null)
                    phone = tfnPhone.Phone;
            }

            return RegexUtil.FormatUSPhone(phone);
        }
    }
}
