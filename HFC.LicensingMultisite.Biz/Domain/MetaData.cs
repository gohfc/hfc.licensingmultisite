﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    public class MetaData
    {
        public static MetaData Load(long id)
        {
            var metadata = EktronUtil.Content.GetContent(id, true).MetaData;
            if (metadata == null)
                return new MetaData();
            return new MetaData
            {
                Title = metadata.Where(x => x.Name == "title").Select(x => x.Text).FirstOrDefault(),
                Description = metadata.Where(x => x.Name == "description").Select(x => x.Text).FirstOrDefault(),
                Keywords = metadata.Where(x => x.Name == "keywords").Select(x => x.Text).FirstOrDefault(),
                Canonical = metadata.Where(x => x.Name == "canonical").Select(x => x.Text).FirstOrDefault(),
                Sitemappriority = metadata.Where(x => x.Name == "sitemappriority").Select(x => x.Text).FirstOrDefault()
            };
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string Canonical { get; set; }
        public string Sitemappriority { get; set; }



    }
}
