using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    public class SiteManager
    {
        public static IList<CorpTFNPhoneLookup> GetCorpTFNPhoneLookup()
        {
            return new SmartFormService().GetCorpTFNPhoneLookup();
        }
    }
}
