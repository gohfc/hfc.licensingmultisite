﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;
using HFC.LicensingMultisite.Biz.Extensions;

namespace HFC.LicensingMultisite.Biz.Data
{
    public sealed class ConsultationFormRepo
    {
        public void Insert(ConsultFormDTO obj)
        {
            using (var context = new HFCFormQueueEntities())
            {
                ObjectParameter new_identity = new ObjectParameter("new_identity", typeof(int?));
                DateTime? firstvisitutc = null;

                if (!string.IsNullOrEmpty(obj.FirstVisitDateUTC))
                    firstvisitutc = Convert.ToDateTime(obj.FirstVisitDateUTC);

                context.CCLicensingMultisite_RequestFormInsert(obj.FirstName, obj.LastName, obj.PostalCode, obj.Referrer, obj.Phone.RemoveAllNonNumericCharacters(),
                obj.Email, obj.LeadSource, obj.Country, obj.RequestUrl, obj.RequestType, obj.UtmCampaign, obj.IP, firstvisitutc, new_identity);

                int newLeadID = (int)new_identity.Value;
                if (newLeadID > 0)
                {
                    context.FormQueueContactOptInExtInsert(3, 1, "CCLicensingMultisite_RequestForm", newLeadID, true);
                }
            }
        }
    }
}
