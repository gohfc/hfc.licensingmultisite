﻿using HFC.LicensingMultisite.Biz.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    public sealed class NewsletterRepo
    {
        public void Insert(EnewsletterDTO obj)
        {
            using (var context = new CampMailerEntities())
            {
                context.SubscriptionUpsert(obj.Email, obj.Concept, obj.IsUnsub, System.DateTime.Now, System.DateTime.Now, obj.Referrer, obj.RequestUrl, obj.ZipCode);
            }
        }
    }
}
