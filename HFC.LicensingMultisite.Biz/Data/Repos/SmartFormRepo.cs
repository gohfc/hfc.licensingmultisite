﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    public sealed class SmartFormRepo
    {
        public IList<CorpTFNPhoneLookup> GetCorpTFNPhoneLookup()
        {
            return EktronUtil.Serialization.Deserialize<EKCorpTFNPhoneLookup.root>(getSmartFormData("Configuration", SmartFormConfig.CorpTFNPhoneLookup)).ToCorpTFN();
        }

        private global::Ektron.Cms.ContentData getSmartFormData(string dirName, string smartformName)
        {
            return EktronUtil.Content.SmartFormFor(dirName, smartformName);
        }
    }
}
