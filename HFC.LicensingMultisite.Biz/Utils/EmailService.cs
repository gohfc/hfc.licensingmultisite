﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HFC.LicensingMultisite.Biz.Extensions;

namespace HFC.LicensingMultisite.Biz
{
    public static class EmailService
    {
        public static void SendENewsletterThankyouEmail(string email)
        {
            string template = "";
            MailMessage msg = new MailMessage();

            template = File.ReadAllText(ConfigurationManager.AppSettings["email_newsletter_template"]);
            msg.Subject = ConfigurationManager.AppSettings["email_newsletter_subject"];

            msg.From = new MailAddress(ConfigurationManager.AppSettings["email_newsletter_from_address"], ConfigurationManager.AppSettings["email_newsletter_from_displayname"]);
            msg.To.Add(email);

            msg.IsBodyHtml = true;
            msg.Body = template;

            msg.Send();
        }
    }
}
