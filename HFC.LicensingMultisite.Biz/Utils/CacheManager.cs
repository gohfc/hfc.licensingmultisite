﻿using System;
using System.Runtime.Caching;

namespace HFC.LicensingMultisite.Biz.Util
{
    public class CacheManager
    {
        public static T GetCache<T>(string name)
        {
            var cache = MemoryCache.Default;
            //if (cache[name] != null)
            //    return CastObject<T>(cache[name]);
            //else
                return default(T);
        }

        private static void _SetCache(string name, object value, DateTime? absoluteExpire = null, TimeSpan? slidingExpire = null)
        {
            var cache = MemoryCache.Default;
            if (value == null)
            {
                cache.Remove(name);
            }
            else
            {
                var policy = new CacheItemPolicy();
                policy.Priority = CacheItemPriority.Default;
                if (slidingExpire.HasValue)
                {
                    policy.SlidingExpiration = slidingExpire.Value;
                    policy.AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration;
                }
                else
                {
                    if (!absoluteExpire.HasValue)
                        absoluteExpire = DateTime.UtcNow.AddHours(10);
                    policy.AbsoluteExpiration = absoluteExpire.Value;
                    policy.SlidingExpiration = ObjectCache.NoSlidingExpiration;
                }

                if (cache[name] != null)
                    cache.Set(name, value, policy);
                else
                    cache.Add(name, value, policy);
            }
        }

        /// <summary>
        /// Sets cache with default of 10 hour absolute expire
        /// </summary>
        public static void SetCache(string name, object value)
        {
            _SetCache(name, value);
        }

        public static void RemoveCache(string name)
        {
            _SetCache(name, null);
        }

        public static void SetCache(string name, object value, DateTime absoluteExpire)
        {
            _SetCache(name, value, absoluteExpire);
        }

        public static void SetCache(string name, object value, TimeSpan slidingExpire)
        {
            _SetCache(name, value, null, slidingExpire);
        }


        private object CastObject(object objToCast, Type type)
        {
            //Check if type is Nullable
            if (type.IsGenericType &&
                type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                //If the type is Nullable and the value is null
                //Just return null
                if (objToCast == null)
                {
                    return null;
                }
                //Type is Nullable and we have a value, override conversion type to underlying
                //type for the Nullable to avoid exception in Convert.ChangeType
                var nullableConverter = new System.ComponentModel.NullableConverter(type);
                type = nullableConverter.UnderlyingType;
            }

            if (type == typeof(string) || type == typeof(String))
            {
                if (objToCast != null)
                    return objToCast.ToString();
                else
                    return string.Empty;
            }
            else if (type == typeof(int) || type == typeof(Int32))
            {
                int num = 0;
                if (objToCast != null)
                    int.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(byte) || type == typeof(Byte))
            {
                byte num = 0;
                if (objToCast != null)
                    byte.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(short) || type == typeof(Int16))
            {
                short num = 0;
                if (objToCast != null)
                    short.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(double) || type == typeof(Double))
            {
                double num = 0;
                if (objToCast != null)
                    double.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(decimal) || type == typeof(Decimal))
            {
                decimal num = 0;
                if (objToCast != null)
                    decimal.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(float))
            {
                float num = 0;
                if (objToCast != null)
                    float.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(long) || type == typeof(Int64))
            {
                long num = 0;
                if (objToCast != null)
                    long.TryParse(objToCast.ToString(), out num);
                return num;
            }
            else if (type == typeof(Guid))
            {
                Guid id = new Guid();
                if (objToCast != null)
                    Guid.TryParse(objToCast.ToString(), out id);
                return id;
            }
            else if (type == typeof(DateTime))
            {
                DateTime date = new DateTime();
                if (objToCast != null)
                    DateTime.TryParse(objToCast.ToString(), out date);
                return date;
            }
            else if (type == typeof(bool) || type == typeof(Boolean))
            {
                bool val = false;
                if (objToCast != null)
                {
                    //if not successful, try with number instead
                    if (!bool.TryParse(objToCast.ToString().ToLower(), out val))
                    {
                        int num = 0;
                        int.TryParse(objToCast.ToString(), out num);
                        if (num > 0)
                            val = true;
                    }
                }
                return val;
            }
            else
                return objToCast;
        }
    }
}
