﻿using Ektron.Cms.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace HFC.LicensingMultisite.Biz
{
    public class SessionManager
    {
        public static NavigationMenu MenuFor(Page page, string name)
        {
            var session = page.Session;
            if (session[name] == null)
            {
                session[name] = NavigationMenu.Make(EktronUtil.Menu.GetMenuItemsByMenuName(name));
            }
            return session[name] as NavigationMenu;
        }

        public static string CorporatePhoneFor(Page page)
        {
            var session = page.Session;
            if (session["CorporatePhone"] == null)
            {
                session["CorporatePhone"] = UTMPhoneLookup.CorporatePhoneSwapFor(page);
            }
            return session["CorporatePhone"].ToString();
        }
    }
}
