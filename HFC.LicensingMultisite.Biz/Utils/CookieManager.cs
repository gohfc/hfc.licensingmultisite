﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    public sealed class CookieManager
    {
        public static string UtmCampaign
        {
            get { return CurrentCookie.Get("utm_campaign"); }
            set { CurrentCookie.Set("utm_campaign", value); }
        }

        public static string HasUtmChanged
        {
            get { return CurrentCookie.Get("HasUtmChanged"); }
            set { CurrentCookie.Set("HasUtmChanged", value); }
        }

        public static string Phone
        {
            get { return CurrentCookie.Get("Phone"); }
            set { CurrentCookie.Set("Phone", value); }
        }

        public static string First_Visit
        {
            get { return CurrentCookie.Get("first_visit"); }
            set { CurrentCookie.Set("first_visit", value); }
        }
    }
}
