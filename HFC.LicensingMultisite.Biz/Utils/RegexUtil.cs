﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    public class RegexUtil
    {

        public enum PhoneFormat { NoFormat, Hyphens, Parenthesis };

        public const string EmailPattern = @"[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}";
        public const string ZipCodePattern = @"^[0-9]{5}[\s-]?([0-9]{4})?$";
        public const string PostalCodePattern = @"^[A-Z]\d[A-Z][\s-]?(\d[A-Z]\d)?$";
        public const string ZipOrPostalCodePattern = @"(^[A-Z]\d[A-Z][\s-]?(\d[A-Z]\d)?$)|(^[0-9]{5}[\s-]?([0-9]{4})?$)";

        /// <summary>
        /// converts a  string to SE (search engine) friendly url name
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public static string ConvertToSEName(string title)
        {
            if (!string.IsNullOrEmpty(title))
            {
                Regex expression = new Regex("[^\\w]|\\s", RegexOptions.IgnoreCase);
                return expression.Replace(title.Trim(), "_").ToLower();
            }
            else return title;
        }

        public static bool IsValidEmail(string email)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(email))
            {
                return Regex.IsMatch(email, EmailPattern);
            }
            return result;
        }

        public static bool IsValidUSPhone(string phone)
        {
            Regex notANumber = new Regex(@"[^\d]", RegexOptions.Multiline | RegexOptions.IgnoreCase);
            phone = notANumber.Replace(phone, "");
            return phone.Length == 10;
        }

        public static bool IsValidZipOrPostal(string zipcode)
        {
            Regex zipreg = new Regex(ZipOrPostalCodePattern, RegexOptions.IgnoreCase);
            return zipreg.IsMatch(zipcode);
        }

        public static string FormatUSPhone(string phone, PhoneFormat format = PhoneFormat.Hyphens)
        {
            if (!string.IsNullOrEmpty(phone))
            {
                //Regex notANumber = new Regex(@"[^\d]", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                //phone = notANumber.Replace(phone, "");
                if (phone.Length == 10)
                {
                    //Regex exp = new Regex(@"(\d{3})(\d{3})(\d{4})");
                    Regex exp = new Regex(@"(\d{3})([a-zA-Z0-9]{3})([a-zA-Z0-9]{4})");
                    if (format == PhoneFormat.Hyphens)
                    {
                        return exp.Replace(phone, "$1-$2-$3");
                    }
                    else if (format == PhoneFormat.Parenthesis)
                    {
                        return exp.Replace(phone, "($1) $2-$3");
                    }
                    else
                        return phone;
                }
                else
                    return phone;
            }
            else
                return phone;
        }

   

    }
}
