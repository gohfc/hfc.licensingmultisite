﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HFC.LicensingMultisite.Biz
{
    public static class CurrentCookie
    {
        public static string Get(string key)
        {
            if (string.IsNullOrEmpty(key) || HttpContext.Current == null || HttpContext.Current.Request == null)
                return null;

            if (HttpContext.Current.Request.Cookies.AllKeys.Contains(key))
                return HttpContext.Current.Request.Cookies[key].Value;
            else
                return null;
        }

        public static bool Set(string key, string value, DateTime? expires = null)
        {
            if (string.IsNullOrEmpty(key) || HttpContext.Current == null || HttpContext.Current.Request == null)
                return false;

            var cookie = HttpContext.Current.Response.Cookies[key];
            if (cookie == null)
            {
                cookie = new HttpCookie(key, value);
                if (expires.HasValue)
                    cookie.Expires = expires.Value;
                else
                    cookie.Expires = DateTime.UtcNow.AddDays(30);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            else
            {
                cookie.Value = value;
                if (expires.HasValue)
                    cookie.Expires = expires.Value;
                else
                    cookie.Expires = DateTime.UtcNow.AddDays(30);
                HttpContext.Current.Response.Cookies.Set(cookie);
            }

            return true;
        }
    }
}
