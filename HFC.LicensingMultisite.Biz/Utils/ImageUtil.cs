﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace HFC.LicensingMultisite.Biz
{
    public class ImageUtil
    {
        public static string GetThumbnailPath(string imgSrc, bool checkExist = false)
        {
            if (!string.IsNullOrEmpty(imgSrc))
            {
                var split = imgSrc.Split('/');
                var last = split[split.Length - 1];
                split = split.Take(split.Length - 1).ToArray();
                var thumbPath = string.Format("{0}/thumb_{1}", string.Join("/", split), last);
                if (checkExist)
                {
                    if (File.Exists(HostingEnvironment.MapPath(thumbPath)))
                        return thumbPath;
                    else
                        return null;
                }
                else
                    return thumbPath;
            }
            else
                return null;
        }

        public static Dimension ScaleThumbImage(string path)
        {
            int maxWidth = 400;
            int maxHeight = 266;
            if (File.Exists(path))
            {
                var image = Image.FromFile(path);

                var ratioX = (double)maxWidth / image.Width;
                var ratioY = (double)maxHeight / image.Height;
                var ratio = Math.Min(ratioX, ratioY);

                var newWidth = (int)(image.Width * ratio);
                var newHeight = (int)(image.Height * ratio);

                return Dimension.Make(newWidth.ToString(), newHeight.ToString());
            }
            return Dimension.Make(maxWidth.ToString(), maxHeight.ToString());
        }

        public static bool IsImagePortrait(string path)
        {
            string fullPath = HttpRuntime.AppDomainAppPath + path;
            if (!File.Exists(fullPath)) return false;

            Image img = System.Drawing.Image.FromFile(fullPath);
            int height = img.Height;
            int width = img.Width;

            if (height > width)
                return true;
            return false;
        }

    }

    public class Dimension
    {
        public static Dimension Make(string width, string height)
        {
            return new Dimension
            {
                Width = width,
                Height = height
            };
        }

        public string Width { get; set; }
        public string Height { get; set; }
    }
}
