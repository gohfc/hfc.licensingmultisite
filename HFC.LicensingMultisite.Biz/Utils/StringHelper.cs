﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    public static class StringHelper
    {
        public static string AddSpacesBetweenCapitalLetters(this string obj)
        {
            return System.Text.RegularExpressions.Regex.Replace(obj, "[A-Z]", " $0").Trim();
        }

        public static string RemoveSpace(this string obj)
        {
            return obj.Replace(" ", string.Empty);
        }

        public static string FirstLetterToUpper(this string input)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;

            return input = input.Substring(0, 1).ToUpper() +
               input.Substring(1, input.Length - 1);
        }
    }
}
