﻿using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Content;
using Ektron.Cms.Framework;
using Ektron.Cms.Framework.Content;
using Ektron.Cms.Framework.Organization;
using Ektron.Cms.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace HFC.LicensingMultisite.Biz
{
    public static class EktronUtil
    {
        public static class Content
        {
            public static ContentData GetContent(long contentid, bool metadata = false)
            {
                if (contentid == 0) return null;
                ContentManager contentManager = new ContentManager();
                return contentManager.GetItem(contentid, metadata);
            }

            public static ContentData SmartFormFor(string folderName, string smartFormName)
            {
                return ContentsFor(folderName).Where(x => x.Title.ToUpper().Contains(smartFormName.ToUpper())).FirstOrDefault();
            }

            public static ContentData SmartFormFor(long folderID, string smartFormName)
            {
                var contentManager = new ContentManager(global::Ektron.Cms.Framework.ApiAccessMode.Admin);
                contentManager.InPreviewMode = true;
                var criteria = new ContentCriteria();
                criteria.PagingInfo = new PagingInfo(999);
                criteria.AddFilter(ContentProperty.FolderId, CriteriaFilterOperator.EqualTo, folderID);
                criteria.AddFilter(ContentProperty.Title, CriteriaFilterOperator.EqualTo, smartFormName);
                var results = contentManager.GetList(criteria);
                return results.SingleOrDefault();
            }


            public static List<ContentData> SmartFormsFor(string folderName)
            {
                var contentManager = new ContentManager(global::Ektron.Cms.Framework.ApiAccessMode.Admin);
                contentManager.InPreviewMode = true;

                var folderID = EktronUtil.Folder.GetFor(folderName).Id;
                var configFolder = EktronUtil.Folder.ChildFoldersFor(folderID).Where(x => x.Name == "Configuration").FirstOrDefault();
                var criteria = new ContentCriteria();

                criteria.PagingInfo = new PagingInfo(999);
                criteria.AddFilter(ContentProperty.FolderId, CriteriaFilterOperator.EqualTo, configFolder.Id);
                var results = contentManager.GetList(criteria);
                return results;
            }

            public static List<ContentData> ContentsFor(string folderName)
            {
                var contentManager = new ContentManager(global::Ektron.Cms.Framework.ApiAccessMode.Admin);

                //contentManager.InPreviewMode = true;

                //string url = HttpContext.Current.Request.Url.Host;
                //if (url.ToLower().Contains("stage"))
                //    contentManager.InPreviewMode = true;

                var criteria = new ContentCriteria();
                criteria.PagingInfo = new PagingInfo(999);
                criteria.AddFilter(ContentProperty.ContentPath, CriteriaFilterOperator.Contains, folderName.ToLower());
                var results = contentManager.GetList(criteria);
                return results;
            }

            public static ContentData ContentsFor(string folderName, string contentName)
            {
                var contentManager = new ContentManager(global::Ektron.Cms.Framework.ApiAccessMode.Admin);
                contentManager.InPreviewMode = true;
                var criteria = new ContentCriteria();
                criteria.PagingInfo = new PagingInfo(999);
                criteria.AddFilter(ContentProperty.ContentPath, CriteriaFilterOperator.Contains, folderName.ToLower());
                criteria.AddFilter(ContentProperty.Title, CriteriaFilterOperator.Contains, contentName);
                var result = contentManager.GetList(criteria).FirstOrDefault();
                return result;
            }

            public static List<ContentData> ContentsByTitle(string contentTitle)
            {
                var contentManager = new ContentManager(global::Ektron.Cms.Framework.ApiAccessMode.Admin);
                contentManager.InPreviewMode = true;
                var criteria = new ContentCriteria();
                criteria.PagingInfo = new PagingInfo(999);
                criteria.AddFilter(ContentProperty.Title, CriteriaFilterOperator.Contains, contentTitle);
                var results = contentManager.GetList(criteria);
                return results;
            }

            public static ContentMetaData ContentsMetaDataFor(string folderName, string contentTitle, string metaDataName)
            {
                var contentManager = new ContentManager(global::Ektron.Cms.Framework.ApiAccessMode.Admin);
                contentManager.InPreviewMode = true;
                var returnMetadata = true;

                var criteria = new ContentCriteria();
                criteria.PagingInfo = new PagingInfo(999);
                criteria.AddFilter(ContentProperty.ContentPath, CriteriaFilterOperator.Contains, folderName);
                criteria.AddFilter(ContentProperty.Title, CriteriaFilterOperator.EqualTo, contentTitle);
                var cid = contentManager.GetList(criteria)[0].Id;
                var metaData = contentManager.GetItem(cid, returnMetadata).MetaData.Where(x => x.Name.ToLower() == metaDataName.ToLower()).FirstOrDefault();
                return metaData;
            }
        }

        public static class Library
        {
            public static List<LibraryData> GetLibraryItemsByTaxonomyId(long taxId)
            {
                if (taxId == 0) return null;
                List<LibraryData> items = new List<LibraryData>();

                LibraryManager m_refLibraryApi = new LibraryManager(ApiAccessMode.Admin);
                TaxonomyItemCriteria criteria = new TaxonomyItemCriteria();

                criteria.AddFilter(TaxonomyItemProperty.TaxonomyId, CriteriaFilterOperator.EqualTo, taxId);
                criteria.OrderByField = TaxonomyItemProperty.TaxonomyItemDisplayOrder;
                TaxonomyItemManager taxonomyItemManager = new TaxonomyItemManager();

                List<TaxonomyItemData> taxonomyItemList = taxonomyItemManager.GetList(criteria);
                if (taxonomyItemList.Count() > 0)
                {
                    foreach (TaxonomyItemData taxItem in taxonomyItemList)
                    {
                        long contentid = taxItem.Id;
                        var imgItem = m_refLibraryApi.GetLibraryItemByContentId(contentid);
                        if (imgItem != null)
                        {
                            var thisitem = m_refLibraryApi.GetItem(imgItem.Id);
                            if (thisitem != null)
                            {
                                imgItem.MetaData = thisitem.MetaData;
                                imgItem.Teaser = thisitem.Teaser;
                                imgItem.Taxonomies = thisitem.Taxonomies;
                                items.Add(imgItem);
                            }
                        }
                    }
                }
                return items;
            }


            public static List<LibraryData> GetLibraryItemsByTaxonomyName(string taxName)
            {
                if (string.IsNullOrEmpty(taxName)) return null;

                //var taxNameWithSpace = System.Text.RegularExpressions.Regex.Replace(taxName, "[A-Z]", " $0");

                var taxonomy = Taxonomy.TaxonomyDataByName(taxName);

                if (taxonomy == null) return null;

                var taxId = taxonomy.Id;

                List<LibraryData> items = new List<LibraryData>();

                LibraryManager m_refLibraryApi = new LibraryManager(ApiAccessMode.Admin);
                TaxonomyItemCriteria criteria = new TaxonomyItemCriteria();
                criteria.AddFilter(TaxonomyItemProperty.TaxonomyId, CriteriaFilterOperator.EqualTo, taxId);
                TaxonomyItemManager taxonomyItemManager = new TaxonomyItemManager();
                List<TaxonomyItemData> taxonomyItemList = taxonomyItemManager.GetList(criteria);
                if (taxonomyItemList.Count() > 0)
                {
                    foreach (TaxonomyItemData taxItem in taxonomyItemList)
                    {
                        long contentid = taxItem.Id;
                        var imgItem = m_refLibraryApi.GetLibraryItemByContentId(contentid);
                        if (imgItem != null)
                        {
                            var thisitem = m_refLibraryApi.GetItem(imgItem.Id);
                            if (thisitem != null)
                            {
                                imgItem.Teaser = thisitem.Teaser;
                                imgItem.Taxonomies = thisitem.Taxonomies;
                                items.Add(imgItem);
                            }
                        }
                    }
                }
                return items;
            }
        }

        public static class Taxonomy
        {
            public static List<TaxonomyData> GetFeImageTaxonomies()
            {
                var taxonomyFranchiseID = long.Parse(ConfigurationManager.AppSettings["FeImageTaxonomy_id"]);
                var list = new List<TaxonomyData>();
                return GetChildrenTaxonomies(taxonomyFranchiseID, list);
            }

            public static List<TaxonomyData> TaxonomyDataByID(long Id)
            {
                var list = new List<TaxonomyData>();
                return GetChildrenTaxonomies(Id, list);
            }


            public static List<TaxonomyData> TaxonomyChildrenDataByName(string taxName)
            {
                var taxonomy = Taxonomy.TaxonomyDataByName(taxName);
                var taxonomyManager = new TaxonomyManager();
                var criteria = new TaxonomyCriteria();
                criteria.AddFilter(TaxonomyProperty.ParentId, CriteriaFilterOperator.EqualTo, taxonomy.Id);
                var taxonomyChildrenData = taxonomyManager.GetList(criteria);
                return taxonomyChildrenData;
            }

            public static List<TaxonomyData> TaxonomyChildrenWithItemsByName(string taxName)
            {
                var taxonomy = Taxonomy.TaxonomyDataByNameAndLevel(taxName, 1);
                var taxonomyManager = new TaxonomyManager();
                var criteria = new TaxonomyCriteria();
                criteria.AddFilter(TaxonomyProperty.ParentId, CriteriaFilterOperator.EqualTo, taxonomy.Id);
                if (string.Equals(taxName.ToLower(), "garage"))
                    criteria.OrderByDirection = EkEnumeration.OrderByDirection.Descending;
                else
                    criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;

                var taxonomyChildrenData = taxonomyManager.GetList(criteria);
                foreach (var item in taxonomyChildrenData)
                {
                    item.TaxonomyItems = Taxonomy.GetTaxonomiesItems(item.Id).ToArray();
                }

                return taxonomyChildrenData;
            }

            public static TaxonomyData TaxonomyDataByName(string taxName, int level)
            {
                var taxonomyManager = new TaxonomyManager();
                TaxonomyCriteria taxonomycriteria = new TaxonomyCriteria();
                taxonomycriteria.AddFilter(TaxonomyProperty.Name, CriteriaFilterOperator.Contains, taxName);
                var taxonomy = taxonomyManager.GetList(taxonomycriteria).FirstOrDefault();

                return taxonomy;
            }



            public static List<TaxonomyItemData> GetTaxonomiesItems(long id)
            {
                var taxonomyManager = new TaxonomyManager();
                TaxonomyCriteria taxonomycriteria = new TaxonomyCriteria();
                taxonomycriteria.AddFilter(TaxonomyProperty.Id, CriteriaFilterOperator.EqualTo, id);
                var taxonomy = taxonomyManager.GetList(taxonomycriteria).FirstOrDefault();

                var taxonomyItemManager = new TaxonomyItemManager();
                var taxonomyItemCriteria = new TaxonomyItemCriteria();
                taxonomyItemCriteria.AddFilter(TaxonomyItemProperty.TaxonomyId, CriteriaFilterOperator.EqualTo, taxonomy.Id);
                taxonomyItemCriteria.AddFilter(TaxonomyItemProperty.TaxonomyItemDisplayOrder, CriteriaFilterOperator.IsNull, false);
                taxonomyItemCriteria.OrderByField = TaxonomyItemProperty.TaxonomyItemDisplayOrder;
                var taxonomyItemList = taxonomyItemManager.GetList(taxonomyItemCriteria);

                if (taxonomyItemList != null)
                    return taxonomyItemList.ToList();
                return new List<TaxonomyItemData>();
            }


            public static TaxonomyData TaxonomyDataByName(string taxName)
            {
                var taxonomyManager = new TaxonomyManager();
                TaxonomyCriteria taxonomycriteria = new TaxonomyCriteria();
                taxonomycriteria.AddFilter(TaxonomyProperty.Name, CriteriaFilterOperator.Contains, taxName);
                taxonomycriteria.AddFilter(TaxonomyProperty.Level, CriteriaFilterOperator.Contains, 1);
                var taxonomy = taxonomyManager.GetList(taxonomycriteria).FirstOrDefault();

                return taxonomy;
            }

            public static TaxonomyData TaxonomyDataByNameAndLevel(string taxName, int level)
            {
                var taxonomyManager = new TaxonomyManager();
                TaxonomyCriteria taxonomycriteria = new TaxonomyCriteria();
                taxonomycriteria.AddFilter(TaxonomyProperty.Name, CriteriaFilterOperator.Contains, taxName);
                taxonomycriteria.AddFilter(TaxonomyProperty.Level, CriteriaFilterOperator.Contains, level);
                var taxonomy = taxonomyManager.GetList(taxonomycriteria).FirstOrDefault();

                return taxonomy;
            }

            public static List<TaxonomyItemData> GetTaxonomiesItems(string name)
            {
                var taxonomyManager = new TaxonomyManager();
                TaxonomyCriteria taxonomycriteria = new TaxonomyCriteria();
                taxonomycriteria.AddFilter(TaxonomyProperty.Name, CriteriaFilterOperator.EqualTo, name);
                var taxonomy = taxonomyManager.GetList(taxonomycriteria).FirstOrDefault();

                var taxonomyItemManager = new TaxonomyItemManager();
                var taxonomyItemCriteria = new TaxonomyItemCriteria();
                taxonomyItemCriteria.AddFilter(TaxonomyItemProperty.TaxonomyId, CriteriaFilterOperator.EqualTo, taxonomy.Id);
                var taxonomyItemList = taxonomyItemManager.GetList(taxonomyItemCriteria);

                if (taxonomyItemList != null)
                    return taxonomyItemList.ToList();
                return new List<TaxonomyItemData>();
            }

            public static List<TaxonomyData> GetChildrenTaxonomies(long taxID, List<TaxonomyData> list)
            {
                var taxlist = list;
                var taxonomyManager = new TaxonomyManager();
                TaxonomyCriteria criteria = new TaxonomyCriteria();

                criteria.AddFilter(TaxonomyProperty.ParentId, CriteriaFilterOperator.EqualTo, taxID);
                criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                var taxArray = taxonomyManager.GetList(criteria);

                foreach (var item in taxArray)
                {
                    if (item.HasChildren)
                    {
                        GetChildrenTaxonomies(item.Id, taxlist);
                    }
                    list.Add(item);
                }
                return list;
            }
        }

        public static class Folder
        {
            public static FolderData GetFor(string folderName)
            {
                var folderManager = new FolderManager(ApiAccessMode.Admin);
                FolderCriteria criteria = new FolderCriteria();
                criteria.AddFilter(FolderProperty.FolderName, CriteriaFilterOperator.EqualTo, folderName);
                return folderManager.GetList(criteria).FirstOrDefault();
            }

            public static IList<FolderData> ChildFoldersFor(long parentFolderId)
            {
                var folderManager = new FolderManager(ApiAccessMode.Admin);
                FolderCriteria criteria = new FolderCriteria();
                criteria.AddFilter(FolderProperty.ParentId, CriteriaFilterOperator.EqualTo, parentFolderId);
                return folderManager.GetList(criteria).ToList();

            }
        }

        public static class Menu
        {
            public static IMenuData GetMenuItemsByMenuID(long menuId)
            {
                if (menuId <= 0) return null;

                MenuManager menuManager = new MenuManager();
                long menuID = Convert.ToInt64(menuId);

                IMenuData menuData = menuManager.GetTree(menuID);
                if (menuData != null)
                {
                    if (menuData.Items.Count > 0)
                        return menuData;
                }
                return null;
            }

            public static IMenuData GetMenuItemsByMenuName(string menuName)
            {
                if (string.IsNullOrEmpty(menuName)) return null;

                MenuManager menuManager = new MenuManager();
                MenuCriteria criteria = new MenuCriteria();

                criteria.AddFilter(MenuProperty.Text, CriteriaFilterOperator.EqualTo, menuName);
                var item = menuManager.GetMenuList(criteria).FirstOrDefault();
                IMenuData menuData = menuManager.GetTree(item.Id);
                if (menuData != null)
                {
                    if (menuData.Items.Count > 0)
                        return menuData;
                }
                return null;
            }
        }

        public static class Serialization
        {
            public static T Deserialize<T>(ContentData data)
            {
                if (data != null)
                    return (T)EkXml.Deserialize(typeof(T), data.Html);
                return default(T);
            }

            public static string Serialize<T>(T obj)
            {
                if (obj != null)
                    return EkXml.Serialize(typeof(T), obj);
                return string.Empty;
            }
        }
    }


    public static partial class ext
    {
        public static string ConvertToString(this List<TaxonomyBaseData> list)
        {
            var stringlist = new List<string>();

            foreach (var item in list)
            {
                stringlist.Add(item.Name);
            }
            return string.Join(",", stringlist);
        }

    }
}
