﻿using HFC.LicensingMultisite.Biz.Data;
using System.ServiceModel.Activation;


namespace HFC.LicensingMultisite.Biz.Svcs
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class EnewsletterService : IEnewsletterService
    {
        public void ProcessRequest(EnewsletterDTO data)
        {
            new NewsletterRepo().Insert(data);
             if (data.IsUnsub == true) return;
             EmailService.SendENewsletterThankyouEmail(data.Email);
        }
    }
}
