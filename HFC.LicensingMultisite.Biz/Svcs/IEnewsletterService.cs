﻿using System.ServiceModel;
using System.ServiceModel.Web;


namespace HFC.LicensingMultisite.Biz.Svcs
{
    [ServiceContract(Namespace = "HFC.LicensingMultisite.Biz.Svcs")]
    public interface IEnewsletterService
    {
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        void ProcessRequest(EnewsletterDTO data);
    }
}
