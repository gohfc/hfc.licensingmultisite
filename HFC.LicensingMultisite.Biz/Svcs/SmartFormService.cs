﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SmartFormService : ISmartFormService
    {
        public IList<CorpTFNPhoneLookup> GetCorpTFNPhoneLookup()
        {
            return new SmartFormRepo().GetCorpTFNPhoneLookup();
        }
    }
}
