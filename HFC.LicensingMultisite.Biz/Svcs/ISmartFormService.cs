﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace HFC.LicensingMultisite.Biz
{
    [ServiceContract(Namespace = "HFC.CC.Biz.Svcs")]
    public interface ISmartFormService
    {
        [WebGet]
        [OperationContract]
        IList<CorpTFNPhoneLookup> GetCorpTFNPhoneLookup();
    }
}
