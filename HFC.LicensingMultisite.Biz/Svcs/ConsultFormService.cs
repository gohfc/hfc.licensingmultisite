﻿using HFC.LicensingMultisite.Biz.Data;
using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;

namespace HFC.LicensingMultisite.Biz.Svcs
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ConsultFormService : IConsultFormService
    {
        public void ProcessRequest(ConsultFormDTO data)
        {
            data.IP = GetClientIPAddress();
           
            new ConsultationFormRepo().Insert(data);
        }

        private string GetClientIPAddress()
        {
            var props = OperationContext.Current.IncomingMessageProperties;
            var endpointProperty = props[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            if (endpointProperty != null)
            {
                if (endpointProperty.Address == "::1" || String.IsNullOrEmpty(endpointProperty.Address))
                    return "127.0.0.1";

                return endpointProperty.Address;
            }

            return String.Empty;
        }
    }
}