﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Oops.aspx.cs" Inherits="Oops" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="content">
        <h1 class="futura-medium">An Error Has Occurred</h1>
        <p class="futura-light">
            An unexpected error occurred on our website, the website administrator has been notified.<br />
            <br />
            <ul>
                <li><a href="/" title="Homepage">Return to the homepage</a></li>
            </ul>
        </p>
        <br />
        <br />
    </div>
</body>
</html>
