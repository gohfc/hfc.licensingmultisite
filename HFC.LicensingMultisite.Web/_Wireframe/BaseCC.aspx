﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_MasterPages/MasterCC.master" AutoEventWireup="true" CodeFile="BaseCC.aspx.cs" Inherits="_Wireframe_BaseCC" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register Src="~/_Ctrls/FooterCC.ascx" TagPrefix="DZ" TagName="FooterCC" %>
<%@ Register Src="~/_Ctrls/FooterCCFloat.ascx" TagPrefix="DZ" TagName="FooterCCFloat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/_Stylesheets/ccBase.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <PH:PageHost ID="primaryPH" runat="server" />
    <div class="home-float-form">
        <DZ:DropZone ID="heroDZ" runat="server"></DZ:DropZone>
        <div class="promo">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <DZ:DropZone ID="PromoDZ" runat="server"></DZ:DropZone>
                    </div>
                    <div class="hidden-xs col-sm-4">
                        <DZ:DropZone ID="FormDZ" runat="server"></DZ:DropZone>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden-lg hidden-md" style="background: #e6e7e8;">
        <DZ:DropZone ID="AdditionalPromoDZ2" runat="server"></DZ:DropZone>
    </div>

    <div class="container">
        <DZ:DropZone ID="primaryDZ" runat="server"></DZ:DropZone>
        <DZ:DropZone ID="footer1DZ" runat="server"></DZ:DropZone>
    </div>
    <div class="ccFooter2">
        <DZ:DropZone ID="footer2DZ" runat="server"></DZ:DropZone>
    </div>
    <DZ:FooterCCFloat runat="server" ID="FooterCCFloat" />
    <div class="ccFooter3">
        <DZ:DropZone ID="footer3DZ" runat="server"></DZ:DropZone>
    </div>
    <DZ:FooterCC runat="server" ID="FooterCC" />
</asp:Content>

