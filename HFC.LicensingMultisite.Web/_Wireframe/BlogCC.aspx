﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_MasterPages/MasterCC.master" AutoEventWireup="true" CodeFile="BlogCC.aspx.cs" Inherits="_Wireframe_BlogCC" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagPrefix="PH" TagName="PageHost" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagPrefix="DZ" TagName="DropZone" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>
<%@ Register Src="~/_Ctrls/FooterCC.ascx" TagPrefix="DZ" TagName="FooterCC" %>
<%@ Register Src="~/_Ctrls/FooterCCFloat.ascx" TagPrefix="DZ" TagName="FooterCCFloat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <PH:PageHost ID="primaryPH" runat="server" />
    <div class="ccTitle">
        <DZ:DropZone ID="DZccTitleContent" runat="server"></DZ:DropZone>
    </div>
    <div class="ccContent">
        <DZ:DropZone ID="DZccContent" runat="server"></DZ:DropZone>
    </div>
    <div class="ccFooter2">
        <DZ:DropZone ID="footer2DZ" runat="server"></DZ:DropZone>
    </div>
    <DZ:FooterCCFloat runat="server" ID="FooterCCFloat" />
    <div class="ccFooter3">
        <DZ:DropZone ID="footer3DZ" runat="server"></DZ:DropZone>
    </div>
    <DZ:FooterCC runat="server" ID="FooterCC" />
</asp:Content>

