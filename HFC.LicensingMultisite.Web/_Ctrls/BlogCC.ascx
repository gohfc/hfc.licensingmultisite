﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlogCC.ascx.cs" Inherits="_Ctrls_BlogCC" %>
<link href="/_Stylesheets/blog.css" rel="stylesheet" />

   <div class="container" style="min-height: 500px">
            <div class="row dvFilterBy">
                <div style="display: inline-block; width: 55px;">Filter by: </div>
                <div id="dvblogcategories" class="btn-group">

                    <ul>

                        <% foreach (var cat in Categories)
                           {%>

                        <li><a title="<%= Server.HtmlEncode(cat) %>" href="/blog/?category=<%=cat %>"><%= cat %></a></li>

                        <%   } %>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-9">
                    <% if (SingleBlog != null)
                       { %>

                    <article>
                        <div style="margin: 50px 0 25px 0;">
                            <h1 style="font-size: 2em;"><span class="text-lg"><%= SingleBlog.Title %></span> </h1>
                        </div>
                        <div>

                            <div class="form-group" style="margin-bottom: 20px;">
                                <%--                        <img title="<%= Server.HtmlEncode(SingleBlog.Title) %>" src="/<%= SingleBlog.Image + (SingleBlog.Image.Contains("?") ? "&" : "?") %>targetTypeID=SmartPhone" class="img-thumbnail pull-right" style="margin-left: 15px" />--%>
                                <div><%= SingleBlog.Html %></div>
                                <%if (!string.IsNullOrEmpty(Author))
                                  { %>
                                <div>
                                    <br />
                                    Author: <%= Author%> | <time class="text-muted text-md" datetime="<%= SingleBlog.DateCreated.ToString("o") %>"><%= SingleBlog.DateCreated.ToString("ddd, MMM dd yyyy") %></time>
                                </div>
                                <%}
                                  else
                                  {
                                %>
                                <div>
                                    <br />
                                    <time class="text-muted text-md" datetime="<%= SingleBlog.DateCreated.ToString("o") %>"><%= SingleBlog.DateCreated.ToString("ddd, MMM dd yyyy") %></time>
                                </div>

                                <% } %>
                            </div>

                            <div class="form-group">
                                <!-- AddThis Button BEGIN -->
                                <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                                    <a class="addthis_button_facebook"></a>
                                    <a class="addthis_button_twitter"></a>
                                    <a class="addthis_button_google_plusone_share"></a>
                                    <a class="addthis_button_compact"></a>
                                    <a class="addthis_counter addthis_bubble_style"></a>
                                </div>
                                <!-- AddThis Button END -->
                            </div>
                        </div>
                    </article>
                    <% }
                       else
                       { %>




                    <div>
                        <% if (Blogs != null && Blogs.Count > 0)
                           {
                               for (int i = 0; i < Blogs.Count; i++)
                               {
                                   var blog = Blogs[i];%>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <a title="<%= Server.HtmlEncode(blog.Title) %>" href="/blog/<%=HFC.LicensingMultisite.Biz.RegexUtil.ConvertToSEName(blog.Title)%>/">
                                    <img src="/<%= blog.Image + (blog.Image.Contains("?") ? "&" : "?") %>targetTypeID=SmartPhone" class="img-responsive" />
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-8">
                                <div>
                                    <a style="font-family: 'Roboto Slab'; font-weight: 700; line-height: 1.1; color: #212121; font-size: 1.5em;" title="<%= Server.HtmlEncode(blog.Title) %>" href="/blog/<%=HFC.LicensingMultisite.Biz.RegexUtil.ConvertToSEName(blog.Title) %>/"><%=blog.Title %></a><br />
                                    <time class="text-muted" datetime="<%= blog.DateCreated.ToString("o") %>"><%= blog.DateCreated.ToString("ddd, MMM dd yyyy") %></time>
                                </div>
                                <% if (blog.Html.Count() < CHARACTERSPERPOSTONLIST)
                                   {  %>
                                <div style="padding-top: 20px;"><%=blog.Html%></div>
                                <% }
                                   else
                                   { %>
                                <div style="padding-top: 20px;"><%= GetBlogDesciption(blog.Html) + "<div style='padding-top: 10px;'><a href='/blog/"+ HFC.LicensingMultisite.Biz.RegexUtil.ConvertToSEName(blog.Title)%><%="'>Read More</a></div>"  %>  </div>
                                <%} %>
                            </div>

                        </div>
                        <% if (i < Blogs.Count - 1)
                           { %>
                        <hr />
                        <% }
                                   }
                               } if (TotalPages > 1)
                           {  %>
                        <ul class="pager">
                            <% if (PageNum <= 1)
                               { %>
                            <li class='disabled'><span>Previous</span></li>
                            <% }
                               else
                               { %>
                            <li><a href="/blog/<%= (PageNum == 2 ? "" + (string.IsNullOrEmpty(Category) ? "" : "?category="+ Category) : "?pageNum="+(PageNum - 1)) + (string.IsNullOrEmpty(Category) ? "" : "&category="+ Category) %>">Previous</a></li>
                            <% } //end prev %>

                            <% if (PageNum == TotalPages)
                               { %>
                            <li class='disabled'><span>Next</span></li>
                            <% }
                               else
                               { %>
                            <li><a href="/blog/<%= ("?pageNum=" + (PageNum + 1))  + (string.IsNullOrEmpty(Category) ? "" : "&category="+ Category)%>">Next</a></li>
                            <% } //end next %>
                        </ul>
                        <% } %>
                    </div>

                    <% } %>
                </div>

            </div>
        </div>

