﻿using System;
using System.Web.UI;
using HFC.LicensingMultisite.Biz;

public partial class _Ctrls_CookiePolicy : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(CurrentCookie.Get("CookieAcceptance")))
        {
            CookiePolicyPanel.Visible = false;
        }
        else
        {
            var content = EktronUtil.Content.ContentsFor("Concrete Craft Licensing/Content/", "Cookie Policy Banner");
            CookiePolicyText.Text = content.Html;
        }
    }
}