﻿using Ektron.Cms.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HFC.LicensingMultisite.Biz;

public partial class _Ctrls_HeaderCC : System.Web.UI.UserControl
{
    public NavigationMenu Navigation { get { return SessionManager.MenuFor(this.Page, "Concrete Craft"); } }
    public string Phone { get { return SessionManager.CorporatePhoneFor(this.Page); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        var x = this.Navigation;
        var y = this.Phone;
    }
}