﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using HFC.LicensingMultisite.Biz;
using Ektron.Cms;
using System.Web.UI.HtmlControls;
using Ektron.Cms.Content;
using Ektron.Cms.Common;
using System.Text.RegularExpressions;


public partial class _Ctrls_BlogCC : System.Web.UI.UserControl
{

    public const int NUMBER_OF_RECENTPOSTS = 5;
    public const int RECORDPERPAGE = 10;
    public const int CHARACTERSPERPOSTONLIST = 425;

    public IEnumerable<string> Categories;
    public List<ContentData> Blogs = new List<ContentData>();
    private Ektron.Cms.API.Content.Blog blogApi = new Ektron.Cms.API.Content.Blog();
    public ContentData SingleBlog { get; set; }
    private bool isSinglePostRequired = false;
    private bool isCategorySelected = false;
    public string Author { get; set; }
    public string Category { get; set; }
    public int TotalPages { get; set; }
    public int PageNum { get; set; }
    Ektron.Cms.Framework.Content.ContentManager contentManager = new Ektron.Cms.Framework.Content.ContentManager(Ektron.Cms.Framework.ApiAccessMode.Admin);

    private bool isThisSingleBlogPost = false;
    private long blogID = 0;
    private long pageID = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        var blogid = 99;
        InitVariablesAccordingToQuerystring();
        Categories = blogApi.GetBlog(blogid, null, 10, 1033).Categories.AsEnumerable();
        SetMainContent();

    }

    private void InitVariablesAccordingToQuerystring()
    {
        Category = PageHelper.QueryStringFromRawUrl(this.Page, "category");
        var contentid = PageHelper.QueryStringFor(this.Page, "contentId");
        if (contentid != null)
        {
            contentid = EktronUtil.Content.ContentsByTitle(contentid.Replace("_", " ")).SingleOrDefault().Id.ToString();
        }
        var pagenum = PageHelper.QueryStringFromRawUrl(this.Page, "pagenum");
        long.TryParse(this.Page.QueryStringFor("pageid"), out pageID);
        isCategorySelected = (!string.IsNullOrEmpty(Category) && Category != "All Articles") ? true : false;

        PageNum = (!string.IsNullOrEmpty(pagenum)) ? Convert.ToInt32(pagenum) : 1;
        PageNum = (PageNum < 1) ? 1 : PageNum;

        if (contentid == null) return;
        blogID = long.Parse(contentid);
        isSinglePostRequired = true;
    }
    private void SetMainContent()
    {
        if (isSinglePostRequired)
        {

            SingleBlog = EktronUtil.Content.GetContent(38, true);


            // contentManager = new ContentManager();

            //SingleBlog =  contentManager.GetItem(38, true);
            if (SingleBlog == null) { throw new HttpException(404, "Not found"); }
            setMataData(SingleBlog.MetaData);

        }
        else
        {
            GenerateBlogListAndSetTotalPages();
            setMataData(MetaData.Load(38));
        }


    }
    private void GenerateBlogListAndSetTotalPages()
    {
        int totalPages = 0;
        int totalCount = 0;
        var result = GetBlogList(out totalPages);

        if (Category == "All Articles" || string.IsNullOrEmpty(Category))
        {
            Blogs = result.ToList();
            TotalPages = totalPages;
        }
        else
        {
            Blogs = GetSelectedCategoryList(result, out totalCount);
            TotalPages = GetSelectedCategoryTotalPages(totalCount);
        }
    }
    private int GetSelectedCategoryTotalPages(int count)
    {
        if (count == 0) return 0;
        return (int)Math.Ceiling((double)count / RECORDPERPAGE);
    }
    private List<ContentData> GetSelectedCategoryList(List<ContentData> result, out int totalCount)
    {
        var list = GetAllBlogsForSelectedCategory(result);
        totalCount = list.Count();
        return (PageNum > 1) ? list.Skip((PageNum - 1) * RECORDPERPAGE).Take(RECORDPERPAGE).ToList() : list.Take(RECORDPERPAGE).ToList();
    }
    private List<ContentData> GetAllBlogsForSelectedCategory(List<ContentData> result)
    {
        List<ContentData> blogs = new List<ContentData>();
        foreach (var post in result)
        {
            var bpd = blogApi.GetBlogPostData(post.Id);
            if (bpd.Categories.Where(x => x.ToLower().Contains(Category.ToLower())).FirstOrDefault() != null)
                blogs.Add(post);
        }
        return blogs;
    }
    private List<ContentData> GetBlogList(out int totalPages)
    {
        var criteria = GetContentCriteria(RECORDPERPAGE, isCategorySelected);
        if (!isCategorySelected && PageNum > 1) { criteria.PagingInfo.CurrentPage = PageNum; }

        var result = contentManager.GetList(criteria);
        if (PageNum > 1 && (result == null || result.Count == 0)) { throw new HttpException(404, "Not found"); }
        totalPages = criteria.PagingInfo.TotalPages;
        return result;
    }
    private ContentCriteria GetContentCriteria(int recordsPerPage = 1, bool hasCategorySelected = false)
    {
        var criterica = new ContentCriteria(ContentProperty.DateModified, EkEnumeration.OrderByDirection.Descending);
        criterica.AddFilter(ContentProperty.FolderId, CriteriaFilterOperator.EqualTo, 99);
        if (!hasCategorySelected)
            criterica.PagingInfo.RecordsPerPage = recordsPerPage;
        return criterica;
    }
    private void setMataData(ContentMetaData[] metadata)
    {
        if (metadata.Count() == 0) return;
        Author = metadata.Where(x => x.Name == "author").Select(x => x.Text).FirstOrDefault();
        Page.Header.Description = metadata.Where(x => x.Name == "description").Select(x => x.Text).FirstOrDefault();
        Page.Header.Title = metadata.Where(x => x.Name == "title").Select(x => x.Text).FirstOrDefault();
        var canonical = metadata.Where(x => x.Name == "canonical").Select(x => x.Text).FirstOrDefault();
        if (string.IsNullOrEmpty(canonical))
            canonical = string.Format("http://{0}{1}", Request.Url.Host, Request.RawUrl);
        HtmlLink link = new HtmlLink();
        link.Attributes.Add("rel", "canonical");
        link.Attributes.Add("href", canonical);
        Page.Header.Controls.Add(link);
    }
    private void setMataData(MetaData result)
    {
        if (result == null) return;
        HtmlLink link = new HtmlLink();
        link.Attributes.Add("rel", "canonical");
        link.Attributes.Add("href", result.Canonical);
        Page.Header.Controls.Add(link);
        Page.Header.Description = result.Description;
        Page.Header.Title = result.Title;
        Page.Header.Keywords = result.Keywords;

    }
    public string GetBlogDesciption(string html)
    {

        var result = string.Empty;
        if (html.Contains("carousel-example-generic"))
            return result;
        string noHTML = Regex.Replace(html, @"<[^>]+>|&nbsp;", "").Trim();
        string noHTMLNormalised = Regex.Replace(noHTML, @"\s{2,}", " ");

        if (noHTMLNormalised.Count() <= CHARACTERSPERPOSTONLIST)
            result = noHTMLNormalised;
        else
            result = noHTMLNormalised.Substring(0, CHARACTERSPERPOSTONLIST);
        return result;
    }


}