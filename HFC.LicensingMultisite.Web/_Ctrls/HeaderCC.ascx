﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderCC.ascx.cs" Inherits="_Ctrls_HeaderCC" %>

<nav id="header" class="navbar navbar-default header navbar-fixed-top" style="z-index: 999; background-color: #e6e7e8;">
    <%-- hamburger menu--%>
    <div class="visable-xs hidden-lg hidden-md hidden-sm">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" style="margin-top: 50px" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <img src="/images/concrete-craft-logo.png" class="img-responsive" alt="" width="70%"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <% foreach (var menu in this.Navigation.Mobile)
                       { %>
                    <%  if (menu.Items.Count == 0)
                        { %>
                    <li><a href="/<%=menu.Href %>"><%=menu.Text %></a></li>
                    <%} %>

                    <%  if (menu.Items.Count > 0)
                        { %>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><%=menu.Text %>  <span class="caret"></span></a>


                        <ul class="dropdown-menu" role="menu">
                            <% foreach (var item in menu.Items)
                               {%>
                            <li><a href="/<%=item.Href %>" title="<%=item.Text %>"><%=item.Text %></a></li>
                            <% } %>
                        </ul>
                        <%} %>
                        <%} %>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
    <div class="row visible-xs" style="color: #fff">
        <div class="col-xs-6" style="background: #575757; min-height: 40px; padding: 3px 0px; text-align: center">
            <a href="tel:+<%=this.Phone %>" style="color: #fff">CALL TODAY!
                              <br />
                <%=this.Phone %></a>
        </div>
        <div class="col-xs-6" style="background: #f7941d; min-height: 40px; color: #fff; padding: 3px 0px; text-align: center">
            <a href="/Request-Information/" style="color: #fff">REQUEST MORE
                    <br />
                INFORMATION
            </a>
        </div>

    </div>
    <%-- desktop menu--%>
    <div class="hidden-xs">
        <div class="logorow">
            <div class="container">
                <div class="row logo">
                    <div class="col-xs-6 col-sm-12 col-md-3 col-lg-3" style="padding-left: 20px; padding-top: 10px; padding-bottom: 0px; text-align: right;" itemscope itemtype="https://schema.org/logo">
                        <div class=" hidden-sm">
                            <a href="/">
                                <img src="/images/concrete-craft-logo.png" class="img-responsive" alt="">
                            </a>
                        </div>
                        <div class="row visable-sm hidden-lg hidden-md">
                            <div class="col-sm-6 ">
                                <a href="/">
                                    <img src="/images/concrete-craft-logo.png" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="col-sm-6 visable-sm">
                                <div class="row pull-right" style="text-align: right; padding-top: 0px">
                                    <div class="col-xs-12 col-sm-12 zip-form input-group pull-right phone" style="font-size: 14px">
                                        CALL TODAY!
                              <br />
                                        <%=this.Phone %>
                                        <br />
                                        <a href="/Request-Information/">
                                            <img src="/images/request-more-information.png" class="img-responsive" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-12 col-md-9 col-lg-9 hidden-xs">
                        <div class="row " style="">
                            <div class=" col-sm-12 col-md-10 menurow">
                                <div class="collapse navbar-collapse" id="navbar">
                                    <ul class="nav navbar-nav">
                                        <% foreach (var menu in this.Navigation.Desktop)
                                           { %>
                                        <%  if (menu.Items.Count == 0)
                                            { %>
                                        <li class="desktop-nav-group"><a href="/<%=menu.Href %>" class="menutopitemlong"><%=menu.Text %></a></li>
                                        <%} %>

                                        <%  if (menu.Items.Count > 0)
                                            { %>
                                        <li class="dropdown desktop-nav-group">
                                            <a href="#" class="dropdown-toggle menutopitem" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><%=menu.Text %> </a>

                                            <div style="text-align: center">
                                                <a href="#" class="dropdown-toggle menutopitem" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></a>
                                            </div>

                                            <ul class="dropdown-menu" role="menu">
                                                <% foreach (var item in menu.Items)
                                                   {%>
                                                <li><a href="/<%=item.Href %>" title="<%=item.Text %>"><%=item.Text %></a></li>
                                                <% } %>
                                            </ul>
                                            <%} %>
                                            <%} %>
                                    </ul>

                                </div>
                            </div>
                            <div class="col-md-2 hidden-sm">
                                <div class="row" style="text-align: right; padding-top: 5px">
                                    <div class="col-xs-12 col-sm-12 zip-form input-group pull-right phone" style="font-size: 14px">
                                        CALL TODAY!
                              <br />
                                        <%=this.Phone %>
                                    </div>
                                </div>
                                <div class="row" style="text-align: right; padding-top: 20px">
                                     <a href="/Request-Information/">
                                        <img src="/images/request-more-information.png" class="img-responsive" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>










