﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CookiePolicy.ascx.cs" Inherits="_Ctrls_CookiePolicy" %>

<style type="text/css">
    #CookiePolicyPanel {
        background-color: #425866;
        color: #FFFFFF;
        line-height: 2;
        padding: 6px 18px;
        position: fixed;
        left: 0;
        right: 0;
        bottom: 0;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    #cookie-policy_text {
        font-size: 12px;
        padding-right: 36px;
        width: 100%;
        font-family: "Nunito Sans", sans-serif;
        color: #fff;
        margin: 0;
    }

    #cookie-policy_text a {
        color: #fff;
        text-decoration: underline;
    }

    #cookie-policy_button {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        margin: 0 0 0 5px;
        -ms-flex-item-align: center;
        align-self: center;
        border: #283f5d 3px solid;
        background-color: #FFFFFF;
        padding: 5px 15px;
        font-family: "Nunito Sans",sans-serif;
        font-weight: 700;
        display: inline-block;
        -webkit-transition: all .15s linear;
        transition: all .15s linear;
        text-align: center;
        text-decoration: none;
        color: #283f5d;
        -webkit-box-shadow: 0 3px 6px rgba(0,0,0,.16);
        box-shadow: 0 3px 6px rgba(0,0,0,.16);
    }

    .cookie-policy_hidden {
        display: none !important;
    }
</style>
<asp:Panel ID="CookiePolicyPanel" ClientIDMode="Static" CssClass="cookie-policy_container" runat="server">
    <asp:Literal ID="CookiePolicyText" runat="server" />
    <button id="cookie-policy_button">Accept</button>
</asp:Panel>
<script type="text/javascript">
    $('#cookie-policy_button').click(function (e) {
        e.preventDefault();
        setCookie('CookieAcceptance', 'true', 30);
        $('#CookiePolicyPanel').addClass('cookie-policy_hidden');
    });
</script>