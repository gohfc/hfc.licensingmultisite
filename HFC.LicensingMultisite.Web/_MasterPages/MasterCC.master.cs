﻿using HFC.LicensingMultisite.Biz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _MasterPages_MasterCC : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        new PageInitializer().InitPage(this.Page);
    }
}
