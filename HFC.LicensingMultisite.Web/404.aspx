﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="404.aspx.cs" Inherits="_404" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-1142324-44', 'auto');
        ga('send', 'pageview');

</script>

</head>
<body>
    <div style="width: 100% !important;">
        <div id="content">
            <h1 class="futura-medium">404 - Page not found</h1>
            <p class="futura-light">
                We're sorry, but that page you've requested cannot be found. The link may have been incorrect or outdated. <strong><a href="/">Return to the Concrete Craft homepage</a></strong> to find the page you were looking for. 
                <br /><br />
                If you think you reached this page in error, send us an email at <a href="mailto:ccinf@budgetblinds.com">ccinfo@concretecraft.com</a>

            </p>
            <br />
            <br />
        </div>
    </div>
</body>
</html>
