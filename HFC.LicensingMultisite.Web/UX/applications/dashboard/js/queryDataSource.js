/*global Ektron,alert,console*/
define([
        'ektronjs',
        './config'
], function ($, config) {
    'use strict';

    function GetAllQueries(success, error) { 
        $.ajax({
            url: config.apiResources.topicsUrl,
            dataType: 'json',
            type: 'GET',
            success: success,
            error: function (jqXHR, status, msg) {
                error('Could not load topics from configured URL with a status of "' + status + '" due to the error "' + msg + '"');
            }
        });
    }

    return {
        all: GetAllQueries,
        saveTopic: SaveTopic,
        deleteTopic: DeleteTopic
    };

    function SaveTopic(success, error, topic) { 
        $.ajax({
            url: config.apiResources.topicsUrl,
            dataType: 'json',
            type: 'POST',
            success: success,
            data: topic,
            error: function (jqXHR, status, msg) {
                error('Could not save topics from configured URL with a status of "' + status + '" due to the error "' + msg + '"');
            }
        });
    }

    function DeleteTopic(success, error, topicId) {
        $.ajax({
            url: config.apiResources.topicsUrl + '?' + $.param({ 'id': topicId }),
            dataType: 'json',
            type: 'DELETE',
            success: GetAllQueries(success, error),
            error: function (jqXHR, status, msg) {
                error('Could not delete topics from configured URL with a status of "' + status + '" due to the error "' + msg + '"');
            }
        }); 
    }
}
);