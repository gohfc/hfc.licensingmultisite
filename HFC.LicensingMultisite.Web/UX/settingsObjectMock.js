﻿{
    "apps": [
        {
            "Name": "UX Toolkit",
            "id": "uxToolkit",
            "Icon": "cssFramework"
        },
        {
            "Name": "Css Framework",
            "id": "cssFramework",
            "Icon": "cssFramework"
        },
        {
            "Name": "Application",
            "id": "app",
            "Icon": "app"
        }
    ],
    "isAuthorized": true,
    "sitePath": "/",
    "baseUrl": "/",
    "uiPath": "/frameworkUI"
}