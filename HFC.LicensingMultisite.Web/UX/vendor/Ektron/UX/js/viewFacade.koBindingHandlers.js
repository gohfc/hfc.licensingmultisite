﻿/*globals define,window*/
/// <reference path="../../../jQuery/jquery.min.js" />
/// <reference path="../../../jQuery/UI/js/jquery-ui-complete.min.js" />
define([
    'ektronjs',
    'Vendor/jQuery/UI/js/jquery-ui-complete.min'
],
    function ($) {
        'use strict';
        function ViewFacade() {
            var me = this;
        }

        // jQuery UI scrollParent can replace this once bug #9057 is fixed
        // http://bugs.jqueryui.com/ticket/9057
        ViewFacade.prototype.scrollParent = function (element) {
            var me = this,
                $element = $(element),
                $parents,
                $scrollParent;

            if (/(relative|static)/.test($element.css('position'))) {
                $parents = $element.parents();
            } else if (/absolute/.test($element.css('position'))) {
                $parents = $element.parents().filter(function () {
                    return /relative/.test($.css(this, 'position'));
                });
            }

            $scrollParent = $parents.filter(function () {
                return /(auto|scroll)/.test($.css(this, 'overflow') + $.css(this, 'overflow-y') + $.css(this, 'overflow-x'));
            }).eq(0);

            if ($scrollParent.length === 0) {
                $scrollParent = $(element.ownerDocument);
            }

            return $scrollParent;
        };

        ViewFacade.prototype.isElementApparent = function (element) {
            var me = this,
                $element = $(element),
                scrollParent = me.scrollParent(element);

            if (scrollParent[0].ownerDocument !== null) {
                return ($element.offset().top - scrollParent.offset().top) < scrollParent[0].offsetHeight;
            } else {
                return $element.position().top > scrollParent.scrollTop();
            }
        };

        ViewFacade.prototype.onElementAppear = function (element, callback) {
            var me = this;

            function checkAppearance() {
                if (me.isElementApparent(element) && $(element).data('apparent') !== true) {
                    $(element).data('apparent', true);
                    callback();
                } else if (!me.isElementApparent(element) && $(element).data('apparent') === true) {
                    $(element).data('apparent', false);
                }
            }

            me.scrollParent(element).on('scroll', checkAppearance);

            $(window).on('resize', checkAppearance);
        };

        return new ViewFacade();
    }
);