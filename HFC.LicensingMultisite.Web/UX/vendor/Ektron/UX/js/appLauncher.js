﻿/*global define*/
define(['ektronjs'], function ($) {
    'use strict';
    var config = {
        launcherVisibleClass: 'ux-launcher-visible',
        launcherContextSelector: 'body'
    };

    return {
        on: function (eventName, unloadCallback) {
            $(window).on('beforeunload', unloadCallback);
        },
        visible: function (value) {
            if (value) {
                $('html').addClass('ektron-ux-bodyNoScroll');
                $(config.launcherContextSelector).addClass(config.launcherVisibleClass);
            } else {
                $('html').removeClass('ektron-ux-bodyNoScroll');
                $(config.launcherContextSelector).removeClass(config.launcherVisibleClass);
            }
        }
    };
});