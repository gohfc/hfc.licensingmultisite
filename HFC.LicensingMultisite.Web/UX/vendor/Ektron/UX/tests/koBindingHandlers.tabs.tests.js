﻿/// <reference path="setup.js" />
/// <reference path="../../../knockout/knockout.js" />
/// <reference path="../../../jQuery/jquery.min.js" />
/// <reference path="../../../jQuery/jquery.migrate.js" />
/// <reference path="../../ektron.js" />
/// <reference path="../../../jQuery/UI/js/jquery-ui-complete.min.js" />
/// <reference path="../js/koBindingHandlers.js" />

var jQmock, valueAccessor, labels;

module('UX Custom Binding Handlers Tests (ektronUXTabs)', {
    setup: function () {
        'use strict';

        valueAccessor = function () {
            return {};
        };

        labels = {
            'close': 'close'
        };
    }
});

test('adds a disposal callback to prevent memory leaks when the binding element is destroyed.', function () {
    'use strict';

    var correctElem = false,
        element = 'the test elem',
        correctTabsAction = false,
        tabsAction = 'destroy',
        addDisposeCallbackCount = 0;

    jQmock = function (elem) {
        correctElem = elem === element;
        var me = {};

        me.tabs = function (input) {
            correctTabsAction = tabsAction === input;
        };

        me.is = function (input) {
            return true;
        };

        return me;
    };

    jQmock.extend = function () { };

    //handle disposal (if KO removes by the template binding)
    ko.utils.domNodeDisposal.addDisposeCallback = function (elem, callback) {
        addDisposeCallbackCount = addDisposeCallbackCount + 1;
        if (elem === element) {
            callback();
        }
    };

    ko.bindingHandlers.ektronUXTabs = undefined;
    window.createModule(ko, jQmock);
    ko.bindingHandlers.ektronUXTabs.init(element, valueAccessor);

    ok(correctTabsAction, 'When the disposal callback is executed by ko we call destroy on the tabs instance.');
    ok(correctElem, 'The correct element was passed to jquery to destroy the tabs.');
    equal(addDisposeCallbackCount, 1, 'AddDisposalCallback was called once.');
});

test('destroy method is only called when object is already tabbed', function () {
    'use strict';

    var correctElem = false,
        element = 'the test elem',
        correctTabsAction = false,
        tabsAction = 'destroy',
        addDisposeCallbackCount = 0;

    jQmock = function (elem) {
        correctElem = elem === element;
        var me = {};

        me.tabs = function (input) {
            correctTabsAction = tabsAction === input;
        };

        me.is = function (input) {
            return false;
        };

        return me;
    };

    jQmock.extend = function () { };
    ko.bindingHandlers.ektronUXTabs = undefined;
    window.createModule(ko, jQmock);
    ko.bindingHandlers.ektronUXTabs.init(element, valueAccessor);

    ok(!correctTabsAction, 'When the disposal callback is executed by ko we don\'t call destroy on uninitialized elements.');
    equal(addDisposeCallbackCount, 0, 'AddDisposalCallback was called once.');
});

test('passes the correct element to jquery', function () {
    'use strict';

    var correctSelector = false,
        rightSelector = '<div/>',
        callCount = 0;

    jQmock = function (selector) {
        correctSelector = selector === rightSelector;
        var me = {};

        me.tabs = function (input) {
            callCount = callCount + 1;
        };

        me.is = function (input) {
            return true;
        };

        return me;
    };
    jQmock.extend = function () { };

    //clean up our handler first.
    ko.bindingHandlers.ektronUXTabs = undefined;
    window.createModule(ko, jQmock, labels);

    ko.bindingHandlers.ektronUXTabs.update(rightSelector, valueAccessor, {});

    equal(callCount, 1, 'Tabs was called only once on update.');
    ok(correctSelector, 'The correct element was passed to JQuery UI to be tabbed.');
});

test('update correctly passes options to the tabs function.', function () {
    'use strict';

    var correctOptions = false,
        Options = {
            collapsible: true
        };

    valueAccessor = function () {
        return {
            collapsible: Options.collapsible
        };
    };

    jQmock = function (selector) {
        var me = {};

        me.tabs = function (options) {
            correctOptions = options.collapsible === Options.collapsible;
        };

        me.is = function (input) {
            return true;
        };

        return me;
    };

    jQmock.extend = function (first, second) { first.collapsible = second.collapsible; return first; };

    //clean up our handler first.
    ko.bindingHandlers.ektronUXTabs = undefined;
    window.createModule(ko, jQmock, labels);
    ko.bindingHandlers.ektronUXTabs.update({}, valueAccessor, {});

    ok(correctOptions, 'The handler sent the correct options to the tabs function.');
});
