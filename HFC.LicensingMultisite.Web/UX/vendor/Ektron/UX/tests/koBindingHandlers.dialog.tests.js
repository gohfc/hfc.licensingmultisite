﻿/// <reference path="setup.js" />
/// <reference path="../../../knockout/knockout.js" />
/// <reference path="../../../jQuery/jquery.min.js" />
/// <reference path="../../../jQuery/jquery.migrate.js" />
/// <reference path="../../ektron.js" />
/// <reference path="../../../jQuery/UI/js/jquery-ui-complete.min.js" />
/// <reference path="../js/koBindingHandlers.js" />

/*global start, asyncTest, expect */

var valueAccessor, originalAddDisposeCallback, element, labels, jQmock, jqObject, extendKey;

module('UX Custom Binding Handlers Tests (ektronUXDialog)', {
    setup: function () {
        'use strict';

        valueAccessor = function () {
            return {};
        };

        originalAddDisposeCallback = ko.utils.domNodeDisposal.addDisposeCallback;

        element = {};

        labels = {
            'close': 'close'
        };

        jqObject = {
            data: function () {
                return true;
            },
            dialog: function () {
                return jqObject;
            }
        };

        jQmock = function (selector) {
            return jqObject;
        };
        jQmock.extend = function (firstObj, secondObj) {
            for (var extendKey in secondObj) {
                firstObj[extendKey] = secondObj[extendKey];
            }
            return firstObj;
        };
    },
    teardown: function () {
        'use strict';

        ko.bindingHandlers = {};
        ko.utils.domNodeDisposal.addDisposeCallback = originalAddDisposeCallback;
    }
});

test('Disposal callback prevents memory leaks when the binding element is destroyed.', function () {
    'use strict';

    var expectedAction = 'destroy',
        foundAction,
        expectedElement = {},
        jQFoundElement,
        koFoundElement,
        foundCallback,
        addDisposeCallbackCount = 0;

    jqObject.dialog = function (action) {
        foundAction = action;
    };

    jQmock = function (elem) {
        jQFoundElement = elem;

        return jqObject;
    };
    jQmock.extend = function () { };

    //handle disposal (if KO removes by the template binding)
    ko.utils.domNodeDisposal.addDisposeCallback = function (elem, callback) {
        addDisposeCallbackCount = addDisposeCallbackCount + 1;
        koFoundElement = elem;
        foundCallback = callback;
    };

    window.createModule(ko, jQmock, labels);

    ko.bindingHandlers.ektronUXDialog.init(expectedElement, valueAccessor);

    equal(addDisposeCallbackCount, 1, 'AddDisposalCallback was called once.');
    equal(koFoundElement, expectedElement, 'The correct element was passed to knockout addDisposeCallback.');

    foundCallback();

    equal(jQFoundElement, expectedElement, 'The correct element was passed as the jQuery selector for the dialog.');
    equal(foundAction, expectedAction, 'When the disposal callback is executed by ko we call destroy on the dialog instance.');
});

test('Dispose callback is only triggered if the element is already a dialog.', function () {
    'use strict';

    var dialogIsCalled = false,
        isDialog = false,
        requestedDataProperty;

    jqObject = {
        data: function (dataProperty) {
            requestedDataProperty = dataProperty;
            return isDialog;
        },
        dialog: function () {
            dialogIsCalled = true;
        }
    };

    //handle disposal (if KO removes by the template binding)
    ko.utils.domNodeDisposal.addDisposeCallback = function (elem, callback) {
        callback();
    };

    window.createModule(ko, jQmock, labels);
    ko.bindingHandlers.ektronUXDialog.init(element, valueAccessor);

    equal(requestedDataProperty, 'uiDialog', 'The requested data property to check is "uiDialog".');
    equal(dialogIsCalled, false, 'Dialog was not called when element is not a dialog');

    isDialog = true;
    ko.bindingHandlers.ektronUXDialog.init(element, valueAccessor);

    equal(dialogIsCalled, true, 'Dialog was called when element is a dialog');
});

asyncTest('Dialog is created/initialized on init with the correct options.', function () {
    'use strict';

    var foundOptions,
        foundOptionsIsDefined,
        expectedOptions = {
            expectedProperty: true
        };

    valueAccessor = function () {
        return expectedOptions;
    };

    jqObject.dialog = function (options) {
        foundOptions = options;
        return jqObject;
    };

    window.createModule(ko, jQmock, labels);
    ko.bindingHandlers.ektronUXDialog.init(element, valueAccessor);

    equal(typeof (foundOptions), 'undefined', 'Found options is undefined when init is called initially.');
    setTimeout(function () {
        foundOptionsIsDefined = ('undefined' !== typeof (foundOptions));
        if (foundOptionsIsDefined) {
            ok(foundOptions, 'foundOptions exists.');
            for (var key in expectedOptions) {
                equal(foundOptions[key], expectedOptions[key], 'The options passed are the options used to initialize the dialog.');
            }
        }
        else {
            ok(foundOptionsIsDefined, 'foundOptions was not defined');
        }
        start();
    }, 10);
});

asyncTest('Options passsed may be observables.', function () {
    'use strict';

    var expectedOptions = { expectedProperty: true },
        foundOptions,
        foundOptionsIsDefined;

    jqObject.dialog = function (options) {
        foundOptions = options;
    };

    valueAccessor = function () {
        return ko.observable(expectedOptions);
    };

    window.createModule(ko, jQmock, labels);
    ko.bindingHandlers.ektronUXDialog.init(element, valueAccessor);

    setTimeout(function () {
        foundOptionsIsDefined = ('undefined' !== typeof (foundOptions));
        if (foundOptionsIsDefined) {
            ok(foundOptions, 'foundOptions exists.');
            for (var key in expectedOptions) {
                equal(foundOptions[key], expectedOptions[key], 'The options passed are the options used to initialize the dialog.');
            }
        }
        else {
            ok(foundOptionsIsDefined, 'foundOptions was not defined');
        }
        start();
    }, 10);
});

test('Default options are not overwritten by init being called with instance options', function () {
    'use strict';

    var expectedOptions;

    window.createModule(ko, jQmock, labels);
    expectedOptions = ko.toJS(ko.bindingHandlers.ektronUXDialog.defaultOptions);
    ko.bindingHandlers.ektronUXDialog.init(element, function () {
        return {
            autoOpen: true,
            appendTo: 'body',
            closeText: 'blah blah'
        };
    });

    deepEqual(ko.bindingHandlers.ektronUXDialog.defaultOptions, expectedOptions, 'The options used are the same as expected.');
});

asyncTest('Testing default options (test expects defaults for autoOpen, appendTo and closeText).', 3, function () {
    'use strict';

    var foundOptions,
        expectedOptions,
        element = {};

    jqObject.dialog = function (options) {
        foundOptions = options;
    };

    window.createModule(ko, jQmock, labels);
    expectedOptions = ko.bindingHandlers.ektronUXDialog.defaultOptions;
    ko.bindingHandlers.ektronUXDialog.init(element, valueAccessor);

    setTimeout(function () {
        var optionsFound = 'undefined' !== typeof foundOptions,
            key;

        if (optionsFound) {
            for (key in expectedOptions) {
                equal(foundOptions[key], expectedOptions[key], 'Correct default for ' + key + ' was found.');
            }
        } else {
            ok(optionsFound, 'Found options');
        }

        start();
    }, 10);
});

test('Update applies options on existing dialog', function () {
    'use strict';
    var expectedCloseText = 'shut it DOWN!',
        defaultOptions,
        foundOptions,
        foundAction,
        valueAccessor = function () {
            return {
                closeText: expectedCloseText
            };
        };

    jqObject.dialog = function (action, options) {
        foundAction = action;
        foundOptions = options;
    };

    window.createModule(ko, jQmock, labels);
    defaultOptions = ko.bindingHandlers.ektronUXDialog.defaultOptions;

    ko.bindingHandlers.ektronUXDialog.update(element, valueAccessor);

    equal(foundAction, 'option', 'dialog called with the "option" action');
    equal(foundOptions.autoOpen, defaultOptions.autoOpen, 'default option values are included in update (tested autoOpen)');
    equal(foundOptions.closeText, expectedCloseText, 'provided option values are passed to update (tested closeText)');
});

test('Default values are intact after update', function () {
    'use strict';

    var expectedOptions;

    window.createModule(ko, jQmock, labels);
    expectedOptions = ko.toJS(ko.bindingHandlers.ektronUXDialog.defaultOptions);
    ko.bindingHandlers.ektronUXDialog.update(element, function () {
        return {
            autoOpen: true,
            appendTo: 'body',
            closeText: 'blah blah'
        };
    });

    deepEqual(ko.bindingHandlers.ektronUXDialog.defaultOptions, expectedOptions, 'The options used are the same as expected.');
});

test('options can be observable on update', function () {
    'use strict';
    var expectedOptions = { onlyInMyTest: true },
        foundOptions,
        foundOptionsIsDefined;

    jqObject.dialog = function (action, options) {
        foundOptions = options;
    };

    valueAccessor = function () {
        return ko.observable(expectedOptions);
    };

    window.createModule(ko, jQmock, labels);
    ko.bindingHandlers.ektronUXDialog.update(element, valueAccessor);

    ok(foundOptions.onlyInMyTest, 'found option value of observable');
});

// TODO: only apply options if element is a dialog
test('Only changes to the dialog options if the element is in fact a dialog', function () {
    'use strict';

    var dialogIsCalled = false,
        isDialog = false,
        requestedDataProperty;

    jqObject = {
        data: function (dataProperty) {
            requestedDataProperty = dataProperty;
            return isDialog;
        },
        dialog: function () {
            dialogIsCalled = true;
        }
    };

    //handle disposal (if KO removes by the template binding)
    ko.utils.domNodeDisposal.addDisposeCallback = function (elem, callback) {
        callback();
    };

    window.createModule(ko, jQmock, labels);
    ko.bindingHandlers.ektronUXDialog.update(element, valueAccessor);

    equal(requestedDataProperty, 'uiDialog', 'The requested data property to check is "uiDialog".');
    equal(dialogIsCalled, false, 'Dialog method was not called when element is not a dialog');

    isDialog = true;
    ko.bindingHandlers.ektronUXDialog.update(element, valueAccessor);

    equal(dialogIsCalled, true, 'Dialog method was called when element is a dialog');
});