﻿/*globals window,module,test,asyncTest,start,$*/
/// <reference path="setup.js" />
/// <reference path="../../../jQuery/jquery.min.js" />
/// <reference path="../../ektron.js" />
/// <reference path="../../../jQuery/UI/js/jquery-ui-complete.min.js" />
/// <reference path="../js/viewFacade.koBindingHandlers.js" />
var view, testNode;

module('ektronUXInfiniteScroll View Façade', {
    setup: function () {
        'use strict';
        view = window.createModule($);
    },
    teardown: function () {
        'use strict';
        $(testNode).remove();
    }
});

function setupHtml(html) {
    'use strict';
    testNode = $(html).appendTo('body')[0];

    return testNode;
}

test('isElementApparent positive test', function () {
    'use strict';
    var html = setupHtml('<div><div id="apparentElement">&nbsp;</div></div>'),
        element = $(html).find('#apparentElement')[0];

    ok(view.isElementApparent(element), 'isElementApparent is true when element is within the bounds of its scroll container');
});

test('isElementApparent negative test', function () {
    'use strict';
    var html = setupHtml('<div style="position: relative; max-height: 100px; overflow: scroll;"><div id="unapparentElement" style="position: absolute; top: 101px;">&nbsp;</div></div>'),
        element = $(html).find('#unapparentElement')[0];

    ok(!view.isElementApparent(element), 'isElementApparent is false when element is outside the bounds of its scroll container');
});

asyncTest('onElementAppear fires when element becomes apparent', 1, function () {
    'use strict';
    var elementTop = 101,
        html = setupHtml('<div id="scroller" style="position: relative; max-height: 100px; overflow: scroll;"><div id="unapparentElement" style="position: absolute; top: ' + elementTop.toString() + 'px;">&nbsp;</div></div>'),
        scroller = $(html)[0],
        element = $(html).find('#unapparentElement')[0],
        called = false;

    view.onElementAppear(element, function () { called = true; });

    $(scroller).scrollTop(elementTop + 1);

    // Defer test execution to allow the the event to propogate
    setTimeout(function () {
        ok(called, 'onElementAppear called when parent was scrolled lower than element top');
        start();
    },
    0);
});

asyncTest('onElementAppear fires once for all events until element is no longer apparent', 1, function () {
    'use strict';
    var elementTop = 101,
        secondScrollPosition = 200,
        html = setupHtml('<div id="scroller" style="position: relative; max-height: 100px; overflow: scroll;"><div id="unapparentElement" style="position: absolute; top: ' + elementTop.toString() + 'px;">&nbsp;</div><div style="position: absolute; top: ' + secondScrollPosition.toString() + 'px;">&nbsp;</div></div>'),
        scroller = $(html)[0],
        element = $(html).find('#unapparentElement')[0],
        callCount = 0;

    view.onElementAppear(element, function () { callCount = callCount + 1; });

    $(scroller).scrollTop(elementTop + 1);

    // Free execution stack to allow the the event to propogate
    setTimeout(function () {
        $(scroller).scrollTop(secondScrollPosition);

        // Free execution stack to allow the the event to propogate
        setTimeout(function () {
            equal(callCount, 1, 'onElementAppear fired only once after appearing despite multiple scroll events');
            start();
        },
        0);
    },
    0);
});

test('isElementApparent not susceptible to jQuery UI bug #9057 in IE', function () {
    'use strict';
    var html = setupHtml('<div id="container" style="position: relative;"><div id="scroller" style="overflow: scroll; height: 5em; margin: 5em;"><div id="satellite" style="position: relative; height: 10em; top: 0;">Do I scroll?</div></div></div>'),
        element = $(html).find('#satellite')[0],
		isApparent = false;

	try {
		isApparent = view.isElementApparent(element);
	} catch (e) {
	}
	
	ok(isApparent, 'element found within its scroll parent');
});