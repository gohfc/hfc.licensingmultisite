﻿/*globals module,test*/
/// <reference path="setup.js" />
/// <reference path="../js/koBindingHandlers.js" />
var ko, $, view, labels, blocker;

module('UX Custom Binding Handlers Tests (ektronUXInfiniteScroll)', {
    setup: function () {
        'use strict';

        ko = {
            bindingHandlers: {
                visible: {
                    update: function () { }
                }
            },
            isObservable: function (value) {
                return value.isObservable === true;
            },
            observable: function (original) {
                var value = original;

                function property(update) {
                    if (arguments.length === 0) {
                        return value;
                    }

                    value = update;
                }

                property.isObservable = true;

                return property;
            },
            toJS: function () {
                return {};
            },
            utils: {
                unwrapObservable: function (observable) {
                    return 'function' === typeof observable ? observable() : observable;
                }
            }

        };

        $ = function () {
            return $;
        };
        $.block = function () { };
        $.extend = function (left, right) {
            for (var i in right) {
                left[i] = right[i];
            }

            return left;
        };

        labels = {};

        view = {
            isElementApparent: function () { },
            onElementAppear: function () { }
        };

        window.createModule(ko, $, labels, view);
    }
});

test('update correctly passes non-option parameters to blocker binding', function () {
    'use strict';
    var foundElement,
        foundAllBindingsAccessor,
        foundViewModel,
        foundBindingContext,
        element = {},
        allBindingsAccessor = {},
        viewModel = {},
        bindingContext = {};

    // Create mock to capture data
    ko.bindingHandlers.ektronUXBlocker.update = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        foundElement = element;
        foundAllBindingsAccessor = allBindingsAccessor;
        foundViewModel = viewModel;
        foundBindingContext = bindingContext;
    };

    // Perform tested action
    ko.bindingHandlers.ektronUXInfiniteScroll.init(element, function () { return {}; }, allBindingsAccessor, viewModel, bindingContext);
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return {}; }, allBindingsAccessor, viewModel, bindingContext);

    // Test captured data
    equal(foundElement, element, 'element passed to blocker binding is same as that which was recieved by infinite scroll');
    equal(foundAllBindingsAccessor, allBindingsAccessor, 'allBindingsAccessor passed to blocker binding is same as that which was recieved by infinite scroll');
    equal(foundViewModel, viewModel, 'viewModel passed to blocker binding is same as that which was recieved by infinite scroll');
    equal(foundBindingContext, bindingContext, 'bindingContext passed to blocker binding is same as that which was recieved by infinite scroll');
});

test('update calls onScroll with callback to repeat until element does not appear', function () {
    'use strict';
    var calls = 2,
        onScroll = function (isThereMoreData) {
            calls = calls - 1;
            isThereMoreData(true);
        },
        options = {
            onScroll: onScroll
        },
        viewModel = {},
        element = {};

    view.isElementApparent = function () {
        return calls > 0;
    };

    // Perform tested action
    window.createModule(ko, $, labels, view);
    ko.bindingHandlers.ektronUXInfiniteScroll.init(element, function () { return options; }, {}, viewModel, {});
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return options; }, {}, viewModel, {});

    // Validate test scenario
    equal(calls, 0, 'onScroll called by update repeatedly until element was not apparent');
});

test('update binds onScroll to element appearance event', function () {
    'use strict';
    var element = {},
        foundElement,
        onScrollCalled = false,
        foundCallback,
        options = {
            onScroll: function () { onScrollCalled = true; }
        },
        viewModel = {};

    // Create mock to capture data
    view.onElementAppear = function (element, callback) {
        foundElement = element;
        foundCallback = callback;
    };

    // Perform tested action
    window.createModule(ko, $, labels, view);
    ko.bindingHandlers.ektronUXInfiniteScroll.init(element, function () { return options; }, {}, viewModel, {});
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return options; }, {}, viewModel, {});
    foundCallback();

    // Test captured data
    equal(foundElement, element, 'correct element bound to appearance event');
    ok(foundCallback, 'onScroll called via callback for appearance event');
});

test('update passes options to blocker binding leaving isBlocked as a default', function () {
    'use strict';
    var foundIsBlocked,
        viewModel = {},
        element = {};

    ko.bindingHandlers.ektronUXBlocker.update = function (element, valueAccessor) {
        foundIsBlocked = valueAccessor().isBlocked;
    };

    ko.bindingHandlers.ektronUXInfiniteScroll.init(element, function () { return {}; }, {}, viewModel);
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return {}; }, {}, viewModel);

    equal(typeof foundIsBlocked, 'undefined', 'isBlocked is not overridden by infinite scroll');
});

test('update stops calling onScroll when isThereMoreData callback is passed false', function () {
    'use strict';
    // Mock data and functions to avoid onScroll when initializing, and counting calls by onElementAppear
    var foundCallback,
        hasMoreData = true,
        callCount = 0,
        onScroll = function (isThereMoreData) {
            callCount = callCount + 1;
            isThereMoreData(hasMoreData);
        },
        options = {
            onScroll: onScroll
        },
        viewModel = {},
        i,
        element = {};

    view.isElementApparent = function () {
        return false;
    };

    view.onElementAppear = function (element, callback) {
        foundCallback = callback;
    };

    window.createModule(ko, $, labels, view);
    ko.bindingHandlers.ektronUXInfiniteScroll.init(element, function () { return options; }, {}, viewModel, {});
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return options; }, {}, viewModel, {});

    // Perform tested action - triggering event repeatedly, but with more to scroll only the first time
    for (i = 0; i < 5; i = i + 1) {
        foundCallback();
        hasMoreData = false;
    }

    // Validate test scenario
    equal(callCount, 2, 'onScroll called for initialization and while isThereMoreData callback is passed true');
});

test('update does not call onScroll on an element while a previous call is in progress', function () {
    'use strict';
    var callCount = 0,
        onScroll = function () {
            callCount = callCount + 1;
        },
        options = {
            onScroll: onScroll
        },
        element = {},
        viewModel = {};

    view.isElementApparent = function (target) {
        return target === element;
    };

    window.createModule(ko, $, labels, view);

    // Perform tested actions
    ko.bindingHandlers.ektronUXInfiniteScroll.init(element, function () { return options; }, {}, viewModel, {});
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return options; }, {}, viewModel, {});
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return options; }, {}, viewModel, {});

    // Validate test scenario
    equal(callCount, 1, 'onScroll called once for multiple uncompleted updates');
});

test('onScroll is not called for onElementAppear events while one is already in progress', function () {
    'use strict';
    var foundCallback,
        foundMoreDataCallback,
        callCounter = 0,
        options = {
            onScroll: function (isThereMoreData) {
                callCounter = callCounter + 1;
                foundMoreDataCallback = isThereMoreData;
            }
        },
        element = {};

    view.onElementAppear = function (element, callback) {
        foundCallback = callback;
    };

    window.createModule(ko, $, labels, view);
    ko.bindingHandlers.ektronUXInfiniteScroll.init(element, function () { return options; });
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return options; });

    foundCallback();
    foundCallback();

    equal(callCounter, 1, 'onScroll not called before earlier call was completed');

    foundMoreDataCallback(true);
    foundCallback();

    equal(callCounter, 2, 'onScroll called again after earlier call was completed');
});

test('update passes parameters and observable option to visible', function () {
    'use strict';
    var foundElement,
        foundValue,
        foundAllBindingsAccessor,
        foundViewModel,
        foundBindingContext,
        element = {},
        allBindingsAccessor = {},
        viewModel = {},
        bindingContext = {};

    window.createModule(ko, $, labels, view);
    ko.bindingHandlers.visible.update = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        foundElement = element;
        foundValue = valueAccessor();
        foundAllBindingsAccessor = allBindingsAccessor;
        foundViewModel = viewModel;
        foundBindingContext = bindingContext;
    };

    ko.bindingHandlers.ektronUXInfiniteScroll.init(element, function () { return {}; }, allBindingsAccessor, viewModel, bindingContext);
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return { }; }, allBindingsAccessor, viewModel, bindingContext);

    equal(foundElement, element, 'infinite scroll passed element to visible binding');

    //don't think this test is valid... need to confirm why he felt it was needed to pass a function to return an observable
    //ok(ko.isObservable(foundValue), 'infinite scroll passed observable value to visible binding');
    equal(typeof ko.utils.unwrapObservable(foundValue), 'boolean', 'observable value is type of boolean');
    equal(foundAllBindingsAccessor, allBindingsAccessor, 'infinite scroll passed allBindingsAccessor to visible binding');
    equal(foundViewModel, viewModel, 'infinite scroll passed viewModel to visible binding');
    equal(foundBindingContext, bindingContext, 'infinite scroll passed bindingContext to visible binding');
});

test('visible observable becomes false when isThereMoreData callback is passed false', function () {
    'use strict';
    var simiulateInit = true,
        foundValue,
        foundMoreDataCallback,
        foundAppearCallback,
        onScroll = function (isThereMoreData) {
            foundMoreDataCallback = isThereMoreData;
        },
        options = {
            onScroll: onScroll
        },
        element = {},
        viewModel = {};

    view.isElementApparent = function () {
        return simiulateInit;
    };

    view.onElementAppear = function (element, callback) {
        foundAppearCallback = callback;
    };

    ko.bindingHandlers.visible.update = function (element, valueAccessor) {
        foundValue = valueAccessor();
    };

    // Validate test scenario for initialization
    ko.bindingHandlers.ektronUXInfiniteScroll.init(element, function () { return options; }, {}, viewModel);
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return options; }, {}, viewModel);

    foundMoreDataCallback(true);
    ok(foundValue(), 'visible observable is true after initialization passed true to isThereMoreData callback');

    foundMoreDataCallback(false);
    ok(foundValue() === false, 'visible observable is false after initialization passed true to isThereMoreData callback');

    // Setup test scenario for appear event
    simiulateInit = false;
    foundMoreDataCallback(true);
    foundAppearCallback();

    foundMoreDataCallback(false);
    ok(foundValue() === false, 'visible observable is false after appear event passed true to isThereMoreData callback');

    foundMoreDataCallback(true);
    ok(foundValue(), 'visible observable is true after appear event passed true to isThereMoreData callback');
});

test('update does not repeatedly call over multiple binding calls for the same element', function () {
    'use strict';
    var callCount = 0,
        onScroll = function (isThereMoreData) {
            callCount = callCount + 1;
            isThereMoreData(false);
        },
        options = {
            onScroll: onScroll
        },
        element = {},
        viewModel = {},
        i;


    view.isElementApparent = function () {
        return true;
    };

    window.createModule(ko, $, labels, view);
    ko.bindingHandlers.ektronUXInfiniteScroll.init(element, function () { return options; }, {}, viewModel);
    ko.bindingHandlers.ektronUXInfiniteScroll.update(element, function () { return options; }, {}, viewModel);

    callCount = 1;

    // Perform tested action - triggering event repeatedly, but with more to scroll only the first time
    for (i = 0; i < 5; i = i + 1) {
        ko.bindingHandlers.ektronUXInfiniteScroll.update(element,
            function () { return options; },  //ignore jslint doesn't like function declared in loop
            {},
            viewModel);
    }

    // Validate test scenario
    equal(callCount, 1, 'onScroll called for initialization and only until isThereMoreData callback is passed false');
});