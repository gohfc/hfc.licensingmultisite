﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestForm.aspx.cs" Inherits="TestForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script src="/_Scripts/lib/jquery-2.1.4.js"></script>

    <script src="/_Scripts/lib/jquery-2.1.4.min.js"></script>

    <link href="/_Scripts/lib/bootstrap.css" rel="stylesheet" />
    <link href="/_Stylesheets/ccBase.css" rel="stylesheet" />
    <script src="/_Scripts/lib/bootstrap.js"></script>
    <script src="/_Scripts/lib/jquery.maskedinput.min.js"></script>
    <script src="/_Scripts/utils/browserChecker.js"></script>
    <script src="/_Scripts/utils/pageHelper.js"></script>

    <script src="/_Scripts/utils/validator.js"></script>
    <script type="text/javascript" src="http://l2.io/ip.js?var=ip"></script>
    <script src="/_Scripts/widgets/eNewsletter.js"></script>
    <script src="/_Scripts/widgets/consultationForm.js"></script>


</head>

<body>

    <form runat="server">
        <div style="background-size: 100%; background-color: rgba(18, 96, 134, 1); overflow: hidden">
            <div class="form-header">
                Complete the form                                                                                                                                                                                  
        <br />
                to learn more    
            </div>
            <div class="consult_form_outerdiv">
                <div role="form" class="consult-form" style="position: relative; margin-bottom: 5px; margin-left: 10px; margin-right: 10px;">
                    <div class="form-group">
                        <input class="form-control input-sm" id="ihfName"
                            name="FirstName" maxlength="100" placeholder="First Name" data-toggle="tooltip" data-trigger="manual" title="Please enter your full name" />
                    </div>
                    <div class="form-group">
                        <input class="form-control input-sm" id="ihlName"
                            name="LastName" maxlength="100" placeholder="Last Name" data-toggle="tooltip" data-trigger="manual" title="Please enter your full name" />
                    </div>
                    <div class="form-group">
                        <input class="form-control input-sm" id="ihEmail"
                            name="Email" maxlength="256" placeholder="Email@domain.com" data-toggle="tooltip" data-trigger="manual" title="Please enter a valid email address" type="email" />
                    </div>
                    <div class="form-group">
                        <input class="form-control input-sm" id="ihPhone"
                            name="Phone" maxlength="15" placeholder="Mobile Phone" data-toggle="tooltip" data-trigger="manual" title="Please enter a valid phone" type="tel" />
                    </div>


                   <%-- <div class="form-group">
                        <select id="ihCountry" name="Country">
                            <option value="USA">USA</option>
                            <option value="CANADA">CANADA</option>
                            <option value="OTHER">OTHER</option>
                        </select>
                    </div>--%>


                    <div class="form-group">
                        <input class="form-control input-sm" name="PostalCode"
                            id="ihZip" maxlength="10" placeholder="Zip/Postal Code" data-toggle="tooltip" data-trigger="manual" title="Please enter a valid code" />
                    </div>
                    <div>
                        <div class="form-group">
                            <input class="form-control input-sm"
                                name="LeadSource" id="ihLeadSource" maxlength="500" placeholder="How did you hear about us?" data-toggle="tooltip" data-trigger="manual" title="Please enter your description" />
                        </div>
                        <div>
                            <button type="button" id="ihsubmit" class="btn btn-primary btnIH" onclick="consultationForm.submitConsultForm(this)">SUBMIT</button>
                        </div>
                        <div style="padding: 0; margin-top: 0px; font-size: .80em; color: #ffffff">All fields required</div>
                        <div class="alert alert-danger hidden"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
