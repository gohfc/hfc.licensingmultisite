﻿$(document).ready(function () {
    consultationForm.setPhoneAndPostalMask();

    $(".consult-form input").keypress(function (e) {
        var code = e.keyCode ? e.keyCode : e.which;
        if (code == 13) {
            var btn = $(this).closest(".consult-form").find('.btn');
            submitConsultForm(btn[0]);
            return false;
        }
    })

    $(".consult-form input").focus(function () { $(this).tooltip("hide"); });
});

var consultationForm = (function () {
    this.submitConsultForm = submitConsultForm;
    this.setPhoneAndPostalMask = setPhoneAndPostalMask;
    this.resetToolTip = resetToolTip;
    this.pressEnterForSubmit = pressEnterForSubmit;
    return this;

    function submitConsultForm(saveBtn) {
        var requestType = "InHome";
        var slicedArgs = Array.prototype.slice.call(arguments, 1);
        if (slicedArgs[0] === 'dgform') {
            requestType = "DB";
        }

        var form = $(saveBtn).closest(".consult-form");
        var helper = PageHelper;

        var firstnameBox = helper.getCtrlInForm("FirstName", form),
            lastnameBox = helper.getCtrlInForm("LastName", form),
            emailBox = helper.getCtrlInForm("Email", form),
            phoneBox = helper.getCtrlInForm("Phone", form),
            postalCodeBox = helper.getCtrlInForm("PostalCode", form),
            leadSourceBox = helper.getCtrlInForm("LeadSource", form)
            
            //countryBox =  $("select[name='Country']", form)
        
        var firstVisitDateUTC = getCookie("first_visit");
        
        var data = {
            FirstName: helper.htmlEncode(firstnameBox.val()),
            LastName: helper.htmlEncode(lastnameBox.val()),
            PostalCode: helper.htmlEncode(postalCodeBox.val()),
            Referrer: document.referrer,
            Phone: helper.htmlEncode(phoneBox.val()),
            Email: helper.htmlEncode(emailBox.val()),
            LeadSource: helper.htmlEncode(leadSourceBox.val()),
            //Country: helper.htmlEncode(countryBox.val()),
            RequestUrl: window.location.href,
            RequestType: requestType,
            UtmCampaign: helper.queryStringOrCookieFor("utm_campaign") != null ? helper.queryStringOrCookieFor("utm_campaign") : "OrganicCC",           
            FirstVisitDateUTC: firstVisitDateUTC
        }

        if (validate(firstnameBox, lastnameBox, emailBox, phoneBox, postalCodeBox, countryBox)) {
            save(data);
        }
    }

    function validate(firstnameBox, lastnameBox, emailBox, phoneBox, postalCodeBox, countryBox) {
        var isValid = true;
        if (!Validator.isNotEmpty(firstnameBox.val())) { firstnameBox.tooltip("show"); isValid = false; }
        if (!Validator.isNotEmpty(lastnameBox.val())) { lastnameBox.tooltip("show"); isValid = false; }
        if (!Validator.isValidEmail(emailBox.val())) { emailBox.tooltip("show"); isValid = false; }
        if (!Validator.isValidPhone(phoneBox.val())) { phoneBox.tooltip("show"); isValid = false; }
        if (!Validator.isValidZip(postalCodeBox.val())) { postalCodeBox.tooltip("show"); isValid = false; }
        if (!Validator.isNotEmpty(countryBox.val())) { countryBox.tooltip("show"); isValid = false; }
        return isValid;
    }

    function save(data) {
        $.ajax({
            type: 'post',
            url: '/_Svcs/ConsultFormService.svc/ProcessRequest',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ data: data }),
            success: function (result) {
                window.location.href = "/thankyou/";
            },
            error: function (xhr, textStatus, reasonPhrase) {
            }
        })
    }

    function getCookie(name) {
        var re = new RegExp(name + "=([^;]+)");
        var value = re.exec(document.cookie);
        return (value != null) ? unescape(value[1]) : null;
    }

    function setPhoneAndPostalMask() {
        if (!Browser.isMobile()) {
            $("input[name='Phone']").mask("(999) 999-9999");
            $(".postal-mask").mask("a9a? 9a9");
        }
    }

    function resetToolTip() { $(".consult-form input").focus(function () { $(this).tooltip("hide"); }); }

    $(".consult-form input").focus(function () {
        $(this).tooltip("hide");
    });


    function pressEnterForSubmit() {
        $(".consult-form input").keypress(function (e) {
            var code = e.keyCode ? e.keyCode : e.which;
            if (code == 13) {
                var btn = $(this).closest(".consult-form").find('.btn');
                submitConsultForm(btn[0]);
                return false;
            }
        })
    }
})();