﻿$(document).ready(function () {
    $(".newsletter-form input").focus(function () { $(this).tooltip("hide"); });
    //eNewsletter.resetToolTip();
});

var eNewsletter = (function () {

    this.submitNewsletterForm = submitNewsletterForm;
    this.resetToolTip = resetToolTip;
    return this;

    function submitNewsletterForm(saveBtn, isUnsub) {
        var form = $(saveBtn).closest(".newsletter-form");
        var helper = PageHelper;
        var emailBox = helper.getCtrlInForm("Email", form), zipBox = helper.getCtrlInForm("ZipCode", form);

        var data = {
            Concept: campMailerConceptKey,
            Email: PageHelper.htmlEncode(emailBox.val()),
            ZipCode: PageHelper.htmlEncode(zipBox.val()),
            IsUnsub: isUnsub,
            Referrer: helper.queryStringOrCookieFor("utm_campaign") != null ? helper.queryStringOrCookieFor("utm_campaign") : "OrganicCC",
            RequestUrl: window.location.href
        }

        if (validate(emailBox, zipBox, isUnsub)) {

            if (isUnsub) { unsubscribe(data); showUnsubText(form); }
            else {

                if ((data.Referrer == "AADCwebsite")) {
                    ga('send', { 'hitType': 'event', 'eventCategory': 'aadcoverlay', 'eventAction': 'email', label: 'signup' });
                    aadcsubscribe(data);
                }
                else {
                    ga('send', 'event', 'formSubmit', 'submit', 'success');
                    subscribe(data);
                }
            }
            emailBox.val(""); zipBox.val("");
            PageHelper.getCtrlInForm("btnSubmit", form).prop("disabled", true);
        }
    }

    function showUnsubText(form) {
        alertBox = $(".alert", form);
        alertBox.removeClass("hidden");
        alertBox.text("Your email has been removed from our mailing list");
    }

    function validate(emailBox, zipBox, isUnsub) {
        var isValid = true;

        if (!Validator.isValidEmail(emailBox.val())) { emailBox.tooltip("show"); isValid = false; }
        if (!Validator.isValidZip(zipBox.val()) && !isUnsub) { zipBox.tooltip("show"); isValid = false; }

        return isValid;
    }

    function resetToolTip() { $(".newsletter-form input").focus(function () { $(this).tooltip("hide"); }); }

    function subscribe(data) {
        $.ajax({
            type: 'post',
            url: '/_Svcs/EnewsletterService.svc/ProcessRequest',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ data: data }),
            success: function (result) {
                window.location.href = "/thankyounewsletter/";
            },
            error: function (xhr, textStatus, reasonPhrase) { }
        })
    }


    function aadcsubscribe(data) {
        $.ajax({
            type: 'post',
            url: '/_Svcs/EnewsletterService.svc/ProcessRequest',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ data: data }),
            success: function (result) {
                parent.location = '/thankyounewsletter';
            },
            error: function (xhr, textStatus, reasonPhrase) { }
        })
    }

    function unsubscribe(data) {
        $.ajax({
            type: 'post',
            url: '/_Svcs/EnewsletterService.svc/ProcessRequest',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ data: data }),
            success: function (result) {
            },
            error: function (xhr, textStatus, reasonPhrase) { }
        })
    }
})();