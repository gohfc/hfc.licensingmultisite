﻿
$(document).ready(function () { stickyFooter(); });

$(window).scroll(function () { stickyFooter(); });

function stickyFooter() {

    if ($(window).scrollTop() + $(window).height() > $(document).height() - $(".ccFooter3").height() - 50) {
        $('#stickyFooterView').hide();
        $('#stickyFooterFloatView').show();
    }
    else { $('#stickyFooterView').show(); $('#stickyFooterFloatView').hide(); }

    $(document.body).css('padding-top', $('#header').height());
}
