﻿$(document).ready(function () {
    $(document.body).css('padding-top', $('#header').height());
    $(window).resize(function () {
        $(document.body).css('padding-top', $('#header').height());
        fadeOutLogoWhenXs();
    });

    var scrollToAnchor = function (id) {
        var stickyHeight = $('#header').height();
        var elem = $("a[name='" + id + "']");
        var isadjust = 0;
        if (typeof (elem.offset()) === "undefined") {
            elem = $("#" + id);
        }
        if (IsIEorFirefoxBrowser()) {
            if (specialHandle(id)) { isadjust = -40; }
            else { isadjust = -20; }
        }
        if (typeof (elem.offset()) !== "undefined") {
            targetHtmlAnchoring(elem.offset().top - stickyHeight + isadjust);
        }
    };

    $("a[href*=#]").click(function (e) {
        var srcHref = $(this).attr('href');
        if (srcHref === "#") return;
        if (srcHref.indexOf("#") === 0) {
            e.preventDefault();
            var href = $(this).attr('href').replace('#', '')
            scrollToAnchor(href);
        }
        else {
            var url = $(this).attr('href');
            var anchorIndex = url.lastIndexOf("#");
            if (anchorIndex > -1) {
                var name = url.substring(anchorIndex + 1);
                scrollToAnchor(name);
            }
        }
    });

    $(window).scroll(function () {
        fadeOutLogoWhenXs();
    });

    function fadeOutLogoWhenXs() {
        if ($(window).width() < 768) {
            if ($(this).scrollTop() > 10) {
                $('.navbar-header').slideUp();
                $("#header").removeClass("navbar");
            }
            else {
                $('.navbar-header').fadeIn();
                $("#header").addClass("navbar header");
            }
        }
    }
});

$(window).on("load", function () {
    scrollToAnchorWhenLoad();
});

function scrollToAnchorWhenLoad() {
    var url = window.location.href;
    var anchorIndex = url.lastIndexOf("#");
    if (anchorIndex === -1) { return; }
    var name = url.substring(anchorIndex + 1);
    loadWithAnchorPoint(name);
}

function loadWithAnchorPoint(id) {
    var elem = $("a[name='" + id + "']");
    if (typeof (elem.offset()) === "undefined") {
        elem = $("#" + id);
    }
    var xsadjustment = 0;

    if (navigator.userAgent.indexOf("Firefox") != -1) {
        if ($(window).width() < 768) {
            xsadjustment = $('.navbar-header').height();
        } else {
            xsadjustment = $('#header').height();
        }

        if (typeof (elem.offset()) !== "undefined") {
            targetHtmlAnchoring(elem.offset().top - xsadjustment - 30);
        }
    }
    else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
        if ($(window).width() < 768) {
            xsadjustment = $('.navbar-header').height();
        } else {
            xsadjustment = $('#header').height();
        }

        if (typeof (elem.offset()) !== "undefined") {
            if (specialHandle(id)) {
                targetHtmlAnchoring(elem.offset().top - xsadjustment -40);
            }
            else {
                targetHtmlAnchoring(elem.offset().top - xsadjustment - 20);
            }
        }
    }
    else {
        if ($(window).width() < 768) {
            xsadjustment = $('.navbar-header').height();
        } else {
            xsadjustment = $('#header').height();
        }

        if (typeof (elem.offset()) !== "undefined") {
            targetHtmlAnchoring(elem.offset().top - xsadjustment);
        }
    }
}

function targetHtmlAnchoring(px) {
    $('html, body').animate({ scrollTop: px }, 1000);
}

function specialHandle(id) {
    if (id == 'processes' || id == 'franchise-testimonials' ||id == 'training' ||
        id == 'marketing' || id == 'the-investment' || id=='customer-testimonials' ||
        id == 'our-process' || id == 'veteran-discounts' || id == 'construction-background' ||
        id == 'independent-contractor' || id == 'competitive-advantage') {
        return true;
    }
    return false;
}

function IsIEorFirefoxBrowser() {
    if (((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) ||
       (navigator.userAgent.indexOf("Firefox") != -1)) {
        return true;
    }
    return false;
}