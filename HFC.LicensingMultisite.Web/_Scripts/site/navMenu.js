﻿$('.dropdown').hover(function () {
    if ($(window).width() > 768) {
        $(this).find('.dropdown-menu').stop(true, true).fadeIn();
    }
}, function () {
    if ($(window).width() > 768) {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
    }
});

$('.dropdown-menu').hover(function () {
    if ($(window).width() > 768 && $(this).parent().attr('id') != 'dvblogcategories') {     
            $(this).stop(true, true);       
    }
}, function () {
    if ($(window).width() > 768 && $(this).parent().attr('id') != 'dvblogcategories') {      
            $(this).stop(true, true).delay(200).fadeOut();      
    }
});

// Add slideDown animation to dropdown
$('.dropdown').on('show.bs.dropdown', function (e) { $(this).find('.dropdown-menu').first().stop(true, true).slideDown(800); });
// Add slideUp animation to dropdown
$('.dropdown').on('hide.bs.dropdown', function (e) { $(this).find('.dropdown-menu').first().stop(true, true).slideUp(0); });