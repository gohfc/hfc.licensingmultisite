﻿function PageHelper() { }

PageHelper.htmlEncode = function (html) {
    if (html)
        return document.createElement('a').appendChild(
            document.createTextNode(html)).parentNode.innerHTML;
    else
        return null;
};

PageHelper.getQueryString = function (name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
           results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

PageHelper.getCookie = function (name) {
    function escape(s) { return s.replace(/([.*+?\^${}()|\[\]\/\\])/g, '\\$1'); };
    var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
    return match ? match[1] : null;
};

PageHelper.queryStringOrCookieFor = function (name) {
    return PageHelper.getQueryString(name).length > 0 ? PageHelper.getQueryString(name) : PageHelper.getCookie(name);
}

PageHelper.getCtrlInForm = function (ctrlName, form) {
    return $("input[name=" + ctrlName + "]", form)
}
