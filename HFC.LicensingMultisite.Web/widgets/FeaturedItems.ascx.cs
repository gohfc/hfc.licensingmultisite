using System.Diagnostics;

namespace Ektron.Cms.Widget.FeaturedItems
{
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Content;
    using Ektron.Cms.Framework.Content;
    using Ektron.Cms.Framework.Organization;
    using Ektron.Cms.Widget;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.UI.WebControls;

    public partial class widgets_FeaturedItems : System.Web.UI.UserControl, IWidget
    {

        #region properties

        private long _CollectionId;
        private long DynamicId;
        private bool _Teaser = true;
        private bool _EnablePaging;
        private int _PageSize;
        private bool _IncludeIcons;
        private string _addText;
        private long _selTaxonomyID;
        private bool _displaySelectedContent;
        private string FeaturedItemTitle = "Featured Items";

        [WidgetDataMember(0)]
        public long CollectionId
        {
            get { return _CollectionId; }
            set { _CollectionId = value; }
        }

        [WidgetDataMember(true)]
        public bool Teaser
        {
            get { return _Teaser; }
            set { _Teaser = value; }
        }

        [WidgetDataMember(false)]
        public bool EnablePaging
        {
            get { return _EnablePaging; }
            set { _EnablePaging = value; }
        }

        [WidgetDataMember(10)]
        public int PageSize
        {
            get { return _PageSize; }
            set { _PageSize = value; }
        }

        [WidgetDataMember(false)]
        public bool IncludeIcons
        {
            get { return _IncludeIcons; }
            set { _IncludeIcons = value; }
        }

        [WidgetDataMember("Add Item")]
        public string AddText
        {
            get { return _addText; }
            set { _addText = value; }
        }

        [WidgetDataMember("Featured Items")]
        public string HeaderText { get; set; }

        [WidgetDataMember(0)]
        public long SelTaxonomyID
        {
            get { return _selTaxonomyID; }
            set { _selTaxonomyID = value; }
        }

        [WidgetDataMember(false)]
        public bool DisplaySelectedContent
        {
            get { return _displaySelectedContent; }
            set { _displaySelectedContent = value; }
        }

        #endregion

        private IWidgetHost _host;

        protected void Page_Init(object sender, EventArgs e)
        {
            CancelButton.Text = "Cancel";
            Button1.Text = "Save";

            _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
            _host.Title = uxEditorMenu.Title = FeaturedItemTitle;
            if (!string.IsNullOrEmpty(HeaderText) && !string.IsNullOrWhiteSpace(HeaderText))
            {
                FeaturedItemTitle = HeaderText;
            }
            _host.Edit += new EditDelegate(EditEvent);
            _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
            _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
            _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
            _host.ExpandOptions = Expandable.ExpandOnEdit;
            Load +=
                new EventHandler(
                    delegate(object PreRenderSender, EventArgs Evt)
                    {
                        if (ViewSet.GetActiveView() != Edit) SetOutput();
                    });
            ViewSet.SetActiveView(View);
        }

        protected void SetOutput()
        {
            //Header Text
            if (!string.IsNullOrEmpty(HeaderText))
            {
                uxHeaderTitle.Text = HeaderText;
            }
            else
            {
                uxHeaderTitle.Text = GetLocalResourceObject("featuredItems") != null && !string.IsNullOrEmpty(GetLocalResourceObject("featuredItems").ToString()) ? GetLocalResourceObject("featuredItems").ToString() : FeaturedItemTitle;
            }

            if (CollectionId > 0)
            {
                // Editors Menu section
                uxEditorMenu.Visible = true;
                uxEditorMenu.Title = FeaturedItemTitle;
                uxEditorMenu.ObjectId = CollectionId;
                uxEditorMenu.EktronOnBeforeDataBind += uxEditorMenu_EktronOnBeforeDataBind;

                //Get the Items
                var cm = new ContentManager();
                var contentCollectionCriteria = new ContentCollectionCriteria(ContentProperty.DateModified,
                    EkEnumeration.OrderByDirection.Descending);
                contentCollectionCriteria.AddFilter(ContentCollectionProperty.Id, CriteriaFilterOperator.EqualTo,
                    CollectionId);

                contentCollectionCriteria.PagingInfo = new PagingInfo(5);
                if (PageSize > 0)
                {
                    contentCollectionCriteria.PagingInfo = new PagingInfo(PageSize);
                }

                List<ContentData> collectionItemList = cm.GetList(contentCollectionCriteria);

                if (collectionItemList != null && collectionItemList.Any())
                {
                    uxContentlist.DataSource = collectionItemList;
                    uxContentlist.DataBind();
                }

                Text.Visible = false;
            }
            else
            {
                Text.Visible = true;
                uxEditorMenu.Visible = false;
            }
        }

        void uxEditorMenu_EktronOnBeforeDataBind(object sender, ToolBar.ToolbarData e)
        {
            //Filter Only properties and Add Items.
            if (uxEditorMenu.ToolbarData != null && uxEditorMenu.ToolbarData.Items.Count > 0)
            {
                uxEditorMenu.ToolbarData.Name = FeaturedItemTitle;

                var addItem = uxEditorMenu.ToolbarData.Items.Find(item => item.Href.ToLower().Contains("workarea/collections.aspx"));
                var propertiesItem = uxEditorMenu.ToolbarData.Items.Find(item => item.Href.Contains("tab=properties"));

                if (addItem != null || propertiesItem != null)
                {
                    uxEditorMenu.ToolbarData.Items.Clear();

                    if (addItem != null)
                    {
                        addItem.Name = AddText;
                        uxEditorMenu.ToolbarData.Items.Add(addItem);
                    }

                    if (propertiesItem != null)
                    {
                        uxEditorMenu.ToolbarData.Items.Add(propertiesItem);
                    }
                }
            }
        }


        private void EditEvent(string settings)
        {
            try
            {
                pagesize.Text = PageSize.ToString();
                TeaserCheckBox.Checked = Teaser;
                AddTextTextBox.Text = AddText;
                uxHeaderText.Text = HeaderText;

                //get list of collections from CMS
                collectionlist.Items.Clear();
                var collectionManager = new CollectionManager();
                var criteria = new CollectionCriteria(ContentCollectionProperty.Title,
                    EkEnumeration.OrderByDirection.Ascending);
                criteria.AddFilter(ContentCollectionProperty.Id, CriteriaFilterOperator.GreaterThan, 0);

                var list = collectionManager.GetList(criteria);

                if (list == null || list.Count == 0)
                {
                    ListItem li = new ListItem();
                    li.Text = "No collections defined";
                    li.Value = "0";
                    li.Attributes.Add("description", "No collections defined");
                    collectionlist.Items.Add(li);
                }
                else
                {
                    foreach (var item in list)
                    {
                        ListItem li = new ListItem();
                        li.Text = item.Id.ToString() + ": " + item.Title;
                        li.Value = item.Id.ToString();
                        li.Selected = (CollectionId == item.Id);
                        li.Attributes.Add("description", item.Description.Replace("\"", "'"));
                        collectionlist.Items.Add(li);
                    }
                }
                if (collectionlist.SelectedItem == null) collectionlist.SelectedIndex = 0;

                description.InnerHtml = collectionlist.Items[0].Attributes["description"];
                string script = "$ektron('#" + collectionlist.ClientID + "').change(function(){";
                script += "var desc = $ektron(this).find(':selected');";
                script += "desc = ('undefined' === typeof(desc.attr('description')))? '' : desc.attr('description');";
                script += "$ektron(this).parents('.ekColEditView').find('.ekcoldescription').html(desc);";
                script += "});";
                Ektron.Cms.API.JS.RegisterJSBlock(this, script, "collectionedit" + this.ClientID);

                ViewSet.SetActiveView(Edit);
            }
            catch (Exception e)
            {
                string error = e.Message;
                // errorLb.Text = e.Message + e.Data + e.StackTrace + e.Source + e.ToString();
                ViewSet.SetActiveView(Edit);
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            SetOutput();
            ViewSet.SetActiveView(View);
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            // Save the variables
            if (!long.TryParse(Request.Form[collectionlist.UniqueID], out _CollectionId)) _CollectionId = 0;
            if (!int.TryParse(pagesize.Text, out _PageSize)) _PageSize = 2;
            Teaser = TeaserCheckBox.Checked;
            AddText = (AddTextTextBox.Text.Length > 1) ? AddTextTextBox.Text : "Add Item";
            HeaderText = (uxHeaderText.Text.Length > 1) ? uxHeaderText.Text : string.Empty;
            _host.SaveWidgetDataMembers();
            SetOutput();
            ViewSet.SetActiveView(View);
        }


    }
    public static class StringExtensions
    {
        /// <summary>
        /// Returns part of a string up to the specified number of characters, while maintaining full words
        /// </summary>
        /// <param name="summary">string</param>
        /// <param name="length">Maximum characters to be returned</param>
        /// <returns>String</returns>
        public static string TrimSummary(this string summary, int length)
        {
            var modifiedSummary = string.Empty;

            try
            {
                if (!String.IsNullOrEmpty(summary))
                {
                    summary = WebUtility.HtmlDecode(Regex.Replace(summary, "<[^>]*(>|$)", string.Empty));

                    if (!string.IsNullOrEmpty(summary))
                    {
                        var words = summary.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if (words[0].Length > length)
                            return words[0];
                        var sb = new StringBuilder();

                        foreach (var word in words)
                        {
                            if ((sb + word).Length > length)
                                return string.Format("{0}..", sb.ToString().TrimEnd(' '));
                            sb.Append(word + " ");
                        }

                        modifiedSummary = string.Format("{0}..", sb.ToString().TrimEnd(' '));
                    }
                }
            }
            catch (Exception ex)
            {
                EkException.WriteToEventLog(" Featured Items Widget -TrimSummary method - " + ex, EventLogEntryType.Error);
            }

            return modifiedSummary;
        }
    }
}
