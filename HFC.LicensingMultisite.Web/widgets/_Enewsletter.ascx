﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_Enewsletter.ascx.cs" Inherits="widgets_Enewsletter" %>
<asp:MultiView ID="ViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
        <div class="row" style="padding: 10px 10px 10px 20px;">
            <div role="form" class="newsletter-form col-md-12 col-sm-12 hidden-xs" align="left" style="width:400px">
                <div>
                    <div class="row">
                        <div class="form-group col-sm-12" style="padding:2px">
                            <input type="email" class="input-sm emailinput" name="Email" placeholder="Email Address" maxlength="256" 
                                style="width: 130px; color: #000; background: #fff;border-radius:0; margin: 0px; padding: 5px 2ox; border-right: 1px solid #fff; border-top: 1px solid #fff; border-bottom: 1px solid #fff; border-left: 1px solid #fff; font-size:.75em"
                                data-toggle="tooltip" data-trigger="manual" title="Please enter a valid email address" />
                            <% if (!IsUnsubscribeForm)
                               { %>
                            <input type="text" class="input-sm newsletter-zip-input zipinput" name="ZipCode" placeholder="Zip/Postal Code" maxlength="10" 
                                style="width: 130px; color: #000; background: #fff;border-radius:0; margin: 0px; padding: 5px 2ox; border-right: 1px solid #fff; border-top: 1px solid #fff; border-bottom: 1px solid #fff; border-left: 1px solid #fff; font-size:.75em"
                                data-toggle="tooltip" data-trigger="manual" title="Please enter a Zip/Postal Code" />
                            <% } %>
                            <span class="">
                                <button type="button" id="newlettersignup" onclick="eNewsletter.submitNewsletterForm(this, <%= IsUnsubscribeForm ? 1 : 0 %>)"
                                    class="btn btn-sm newsbtnSubmit" style="background: #58595b; border: 1px solid #000; display: inline-block; color: #fff; margin: 0px; border-radius: 0px;">
                                  SUBMIT
                                </button>
                            </span>
                        </div>
                        <div class="alert alert-info hidden"></div>
                    </div>
                    <br />
                </div>
            </div>
            <div role="form" class="newsletter-form col-xs-12 visible-xs" align="left" style="width:300px;padding:0">
                <table border="0">
                    <tr>
                        <td>
                            <input type="email" class="input-sm emailinput" name="Email" placeholder="Email Address" maxlength="256" style="width: 105px; color: #000; background: #fff;border-radius:0; margin: 0px; padding: 5px 2ox; border-right: 1px solid #fff;; border-top: 1px solid #fff; border-bottom: 1px solid #fff; border-left: 1px solid #fff; font-size:.75em"
                                data-toggle="tooltip" data-trigger="manual" title="Please enter a valid email address" />
                            <% if (!IsUnsubscribeForm)
                               { %>
                            <input type="text" class="input-sm newsletter-zip-input zipinput" name="ZipCode" placeholder="Zip/Postal Code" maxlength="10"  style="width: 105px; border-radius:0; color: #000; background: #fff; margin: 0px; padding: 5px 2ox; border-top: 1px solid #fff; border-bottom: 1px solid #fff;font-size:.75em"
                                data-toggle="tooltip" data-trigger="manual" title="Please enter a Zip/Postal Code" />
                            <% } %>
                            <button type="button" id="btnSubmit" onclick="eNewsletter.submitNewsletterForm(this, <%= IsUnsubscribeForm ? 1 : 0 %>)"
                                class="btn btn-sm newsbtnSubmit" style="background: #58595b; border: 1px solid #000; display: inline-block; color: #fff; margin: 0px; border-radius: 0px;">
                                SUBMIT
                            </button>
                        </td>
                    </tr>
                </table>
                <div class="alert alert-info hidden" style="font-size: 10px"></div>
            </div>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div class="form-group">
            <asp:CheckBox runat="server" ID="isUnsubscribeFormChk" Text="Is Unsubscribe form" />
            <span>If this is checked, then this newsletter will unsubscribe user's email, otherwise it will subscribe them.</span>
        </div>
        <div class="form-group">
            <asp:Label ID="errorLabel" runat="server" CssClass="label label-danger"></asp:Label><br />
            <asp:Button ID="cancelBtn" runat="server" Text="Cancel" CssClass="btn btn-default" OnClick="cancelBtn_Click" />
            <asp:Button ID="saveBtn" runat="server" UseSubmitBehavior="true" Text="Save" CssClass="btn btn-primary" OnClick="saveBtn_Click" />
        </div>
    </asp:View>
</asp:MultiView>
<script>
    $(document).ready(function () {
        $(".newsbtnSubmit :last").parent().css("border", "1px solid #fff");
        $(".emailinput").last().css("border", "1px solid #fff");
        $(".zipinput").last().css("border", "1px solid #fff");
    });
</script>