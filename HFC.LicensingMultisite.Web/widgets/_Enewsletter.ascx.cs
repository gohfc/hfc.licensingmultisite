﻿using Ektron.Cms.Widget;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class widgets_Enewsletter : System.Web.UI.UserControl, IWidget
    
   
{
    [WidgetDataMember(false)]
    public bool IsUnsubscribeForm { get; set; }

    public string subscribeText { get; set; }

    IWidgetHost _host;

    protected void Page_Init(object sender, EventArgs e)
    {
        _host = Ektron.Cms.Widget.WidgetHost.GetHost(this);
        _host.Title = "Newsletter Widget";
        _host.Edit += new EditDelegate(EditEvent);
        _host.ExpandOptions = Expandable.ExpandOnEdit;
        _host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        _host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        _host.Create += new CreateDelegate(delegate() { EditEvent(""); });
        PreRender += new EventHandler(delegate(object PreRenderSender, EventArgs Evt) { SetOutput(); });
        ViewSet.SetActiveView(View);
    }

    void EditEvent(string settings)
    {
        isUnsubscribeFormChk.Checked = IsUnsubscribeForm;

        ViewSet.SetActiveView(Edit);
    }

    /// <summary>
    /// We don't need this method
    /// </summary>
    protected void SetOutput()
    {
        subscribeText = "subscribe";
        if (IsUnsubscribeForm)
            subscribeText = "unsubscribe";
    }

    protected void saveBtn_Click(object sender, EventArgs e)
    {
        try
        {
            IsUnsubscribeForm = isUnsubscribeFormChk.Checked;
            _host.SaveWidgetDataMembers();
            SetOutput();
            ViewSet.SetActiveView(View);
        }
        catch (Exception ex)
        {
            errorLabel.Text = ex.Message;
        }
    }

    protected void cancelBtn_Click(object sender, EventArgs e)
    {
        ViewSet.SetActiveView(View);
    }
}