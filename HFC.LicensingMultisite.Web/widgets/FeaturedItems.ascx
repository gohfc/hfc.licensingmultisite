<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FeaturedItems.ascx.cs" Inherits="Ektron.Cms.Widget.FeaturedItems.widgets_FeaturedItems" %>
<%@ Import Namespace="Ektron.Cms.Widget.FeaturedItems" %>
<asp:MultiView ID="ViewSet" runat="server">
    <asp:View ID="View" runat="server">
        <div class="panel panel-default">
            <div class="panel-heading">
                <asp:Label runat="server" ID="uxHeaderTitle"></asp:Label>
            </div>
            <asp:Label ID="Text" runat="server" Visible="false">Select a Collection</asp:Label>
            <ektron:EditorsMenu runat="server" DisplayType="Collection" ID="uxEditorMenu">
				<div class="panel-body">                                        
				   <asp:ListView ID="uxContentlist" runat="server">
						<EmptyDataTemplate>
							<h4>There are currently no featured Items to display.</h4>
						</EmptyDataTemplate>
						<ItemTemplate>
							<div class="media-body" style="margin-left: 3%; margin-right: 3%">
								<h5><strong><%# Eval("Title")%></strong></h5>
								<section style='<%# (Boolean.Parse(Teaser.ToString())) ?  "": "display:none;" %>'>
									<span class="pull-left" style="font-size: 12px; margin-right: 3%"><%# (Boolean.Parse(Teaser.ToString())) ? Eval("Teaser").ToString().TrimSummary(50): "" %>
										<a class="pull-right" href="<%# Eval("Quicklink")%>">
											<asp:Label ID="uxReadMoreLabel" runat="server" meta:resourcekey="uxReadMoreLabel" />&hellip;</a></span>
								</section>
							</div>
						</ItemTemplate>
					</asp:ListView>
			     </div>   
            </ektron:EditorsMenu>
        </div>
    </asp:View>
    <asp:View ID="Edit" runat="server">
        <div id="<%=ClientID%>_edit" class="LSWidget">
            <table style="width: 95%;" class="ekColEditView">
                <tr>
                    <td>Collection Id:
                    </td>
                    <td>
                        <asp:DropDownList ID="collectionlist" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr style="font-size: 80%; color: #888;">
                    <td>Description:
                    </td>
                    <td>
                        <span class="ekcoldescription" id="description" runat="server"></span>
                    </td>
                </tr>
                <tr>
                    <td>Page Size:
                    </td>
                    <td>
                        <asp:TextBox ID="pagesize" runat="server" Style="width: 95%;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Teaser:
                    </td>
                    <td>
                        <asp:CheckBox ID="TeaserCheckBox" runat="server" Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td>AddText:
                    </td>
                    <td>
                        <asp:TextBox ID="AddTextTextBox" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Header Text:
                    </td>
                    <td>
                        <asp:TextBox ID="uxHeaderText" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="CancelButton" CssClass="LSCancel" runat="server" Text="Cancel" UseSubmitBehavior="false" OnClick="CancelButton_Click" />
                        <asp:Button ID="Button1" runat="server" UseSubmitBehavior="false" OnClick="SaveButton_Click" Text="Save" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:View>
</asp:MultiView>
