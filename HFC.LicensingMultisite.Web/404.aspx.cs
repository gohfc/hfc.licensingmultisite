﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _404 : System.Web.UI.Page
{

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request.QueryString == null)
            Response.StatusCode = 404;
        if (Request.QueryString["aspxerrorpath"] == null)
            Response.Redirect("~/404.aspx?aspxerrorpath=" + Request.RawUrl);


    }


    protected void Page_Load(object sender, EventArgs e)
    {

    }
}