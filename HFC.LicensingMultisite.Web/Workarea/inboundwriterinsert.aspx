﻿<%@ Page Language="C#" Debug="true" AutoEventWireup="true" CodeFile="inboundwriterinsert.aspx.cs"
    Inherits="Workarea_inbound_embedinsert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        #iwContentLab
        {
            height: 380px !important;
            width:100% !important;
        }
        span.iwHelpIcon {
            background-image: url("https://app.inboundwriter.com/iw/assets/images/sidebar_bottom_sprite.png");
            background-position: -16px 0;
            background-repeat: no-repeat;
            cursor: pointer;
            display: inline-block;
            height: 14px;
            width: 14px;
        }
    </style>
</head>

<body onload="iwOpenDocument()" style="">
    <form id="form1" runat="server">
        <div class="ektron-aloha-embed-header">
            <span class="ektron-aloha-embed-header"></span>
        </div>
        <div>
            <div style="color: #000000;font-weight: bold;left: 100px;position: fixed;top: 10px;">Help and tools will be displayed below when clicking help icons: <span id="sideBarTopPanelHelpIcon" class="iwHelpIcon" title="Help" /></div>
            <div id="iwContentLab" style="width:100%"></div>
				<input style="display:none;" id="documentId" type="text" value="2"/>
				<div style="display:none;" id="recentDocumentsList"></div>
        <script id="iwCustomerScript" type="text/javascript" src="https://app.inboundwriter.com/iw/customers/demo-widget/v1/customer.js" async="async" defer="defer"></script>

		<script type="text/javascript">

		    function iwOpenDocument() {
		        var username = "api.inboundwriter@ektron.com";
		        var apiKey = '<%=ApiKey%>';
		        var title = parent.Ektron.inboundwriter.title;
		        var content = parent.Ektron.inboundwriter.content;
		        var documentId = '<%=InBoundKey%>' + parent.Ektron.inboundwriter.title;
		        var customerId = '<%=CustomerId %>';

		        // show the ContentLab display
		        document.getElementById("iwContentLab").style.display = "block";

		        InboundWriter.openDocument(username, apiKey, documentId, title, content, customerId);

		        // demo code - keep track of the recent document ids
		        addRecentDocument(documentId);
		        document.getElementById("iwContentLab").style.width = "100%";
		        document.getElementById("iwContentLab").style.height = "100%";
		    }


		    // Demo code - load recent document ids from local storage
		    var recentDocuments = [];
		    loadRecentDocuments();

		    function loadRecentDocuments() {
		        if (localStorage) {
		            var ids = localStorage.getItem("iwRecentDocumentIds");
		            if (ids) {
		                recentDocuments = JSON.parse(ids);
		                for (var i = 0; i < recentDocuments.length; i++) {
		                    addRecentDocumentLink(recentDocuments[i]);
		                }
		            }
		        }
		        if (recentDocuments.length > 0) {
		            setDocumentId(recentDocuments[0]);
		        } else {
		            var randomId = "" + Math.round(Math.random() * 10000000);
		            setDocumentId(randomId);
		        }
		    }

		    function addRecentDocumentLink(docId) {
		        if (docId) {
		            var link = document.createElement("a");
		            link.href = "javascript:setDocumentId(\"" + docId + "\");";
		            link.innerHTML = "" + docId;
		            link.title = "Open recent document";
		            document.getElementById("recentDocumentsList").appendChild(link);
		        }
		    }

		    function addRecentDocument(docId) {
		        if (recentDocuments.indexOf(docId) == -1) {
		            addRecentDocumentLink(docId);
		            recentDocuments.unshift(docId);
		            if (recentDocuments.length > 10) {
		                recentDocuments = recentDocuments.slice(0, 10);
		            }
		            if (localStorage) {
		                localStorage.setItem("iwRecentDocumentIds", JSON.stringify(recentDocuments));
		            }
		        }
		    }

		    function setDocumentId(docId) {
		        if (docId) {
		            document.getElementById("documentId").value = docId;
		        }
		    }

		</script>
        </div>
    </form>
</body>
</html>
