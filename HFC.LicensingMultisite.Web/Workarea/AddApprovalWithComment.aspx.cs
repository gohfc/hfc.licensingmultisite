﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.BusinessObjects.Organization;
using Ektron.Cms.Common;
using Ektron.Cms.ContentWorkflow;
using Ektron.Cms.Contracts.Common;
using Ektron.Cms.Contracts.ContentWorkflow;
using Ektron.Cms.Framework;
using Ektron.Cms.Framework.Content;
using Ektron.Cms.Framework.ContentWorkflow;
using Ektron.Cms.Instrumentation;

public partial class Workarea_AddApprovalWithComment : System.Web.UI.Page
{
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected EkMessageHelper m_refMsg;
    protected long m_contentId = -1;
    protected long m_folderId = -1;
    protected int m_contentLanguage = -1;
    protected string m_title = "";
    protected bool m_isApprovalList;

    protected void Page_Load(object sender, EventArgs e)
    {
        SetQueryStringParm();
        RegisterResources();
        if (!IsPostBack)
        {
            txtApprovalComment.Text = "Add comment";
            m_refMsg = m_refContentApi.EkMsgRef;

            btnApprovalCommentClose.Text = m_refMsg.GetMessage("btn cancel");

            btnApprovalCommentAccept.Focus();
            if (m_contentId != -1)
            {
                btnApprovalCommentAccept.Text = m_refMsg.GetMessage("btn approve");
                lblApprovalQuestion.Text = string.Format(m_refMsg.GetMessage("lblContentWorkflowApproveQuestion"), m_title);
            }
            else
            {
                btnApprovalCommentAccept.Text = m_refMsg.GetMessage("btn approve all");
                lblApprovalQuestion.Text = m_refMsg.GetMessage("lblContentWorkflowApproveAllQuestion");
                txtApprovalComment.Text = m_refMsg.GetMessage("lblContentWorkflowApproveAddComment");
            }
        }
    }

    private void RegisterResources()
    {

        Ektron.Cms.Framework.UI.Packages.EktronCoreJS.Register(this);
        Ektron.Cms.Framework.UI.Packages.Ektron.Namespace.Register(this);
        Ektron.Cms.Framework.UI.Packages.Ektron.Workarea.Core.Register(this);
        Ektron.Cms.Framework.UI.Packages.Ektron.JSON.Register(this);

        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronWorkareaHelperJS);
        //Ektron.Cms.API.Css.RegisterCss(this, workareaPath + "csslib/ektron.workarea.approvalworkflow.css", "EktronWorkareaApprovalWorkFlowCSS");
    }

    protected void btnApprovalCommentAccept_click(object sender, EventArgs e)
    {
        long workflowdefinitionid = -1;
        Ektron.Cms.Content.EkContent m_refContent;
        try
        {
            if (!m_isApprovalList)
            {
                uxCommentPanel.Visible = false;
                m_refContent = m_refContentApi.EkContentRef;
                workflowdefinitionid = IsAdvancedWorkflowActive(m_contentId, m_folderId, m_contentLanguage);
                if (workflowdefinitionid > 0)
                {
                    Dictionary<string, object> workflowInputs = new Dictionary<string, object>();
                    workflowInputs.Add("WorkflowDefinitionId", workflowdefinitionid);
                    workflowInputs.Add("ContentId", m_contentId);
                    workflowInputs.Add("Comment", txtApprovalComment.Text);
                    workflowInputs.Add("RequestInformation", m_refContentApi.EkContentRef.RequestInformation);
                    ContentWorkflowManager.Approve(workflowInputs);
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "closeDialog", "closeDialog(false);", true);
            }
            else
            {
                string comment = txtApprovalComment.Text.Replace("'", "\\'");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "closeDialog", "closeDialog('" + comment + "','true');", true);
            }
        }
        catch (Exception ex)
        {
            Log.WriteError(ex);
            m_refMsg = m_refContentApi.EkMsgRef;
            btnCloseErrorDialog.Text = m_refMsg.GetMessage("btn ok");
            uxErrorMessage.Visible = true;
            uxCommentPanel.Visible = false;
            uxAddCommentErrorMessage.Text = ex.Message;
        }
    }

    protected void btnCloseErrorDialog_Click(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "closeErrorDialog", "closeDialog('',false);", true);
    }

    private long IsAdvancedWorkflowActive(long contentId, long folderId, int languageId)
    {
        UserAPI _UserApi = new UserAPI();
        Ektron.Cms.BusinessObjects.ContentWorkflow.ContentWorkflowUtilities cwfutilties = new Ektron.Cms.BusinessObjects.ContentWorkflow.ContentWorkflowUtilities(_UserApi.RequestInformationRef);
        return cwfutilties.GetInheritedWorkflowDefinitionId(contentId, folderId, languageId);
    }

    private void SetQueryStringParm()
    {
        if (Request.QueryString["id"] != null)
        {
            long.TryParse(Request.QueryString["id"], out m_contentId);
            if (m_contentId != -1)
            {
                ContentManager contentMgr = new ContentManager(ApiAccessMode.Admin);
                contentMgr.RequestInformation.PreviewMode = true;
                ContentData content_data = contentMgr.GetItem(m_contentId);
                m_folderId = content_data.FolderId;
                m_title = content_data.Title;
            }
        }

        if ((Request.QueryString["LangType"] != null) && (Request.QueryString["LangType"] != ""))
        {
            int.TryParse(Request.QueryString["LangType"], out m_contentLanguage);
            m_refContentApi.SetCookieValue("LastValidLanguageID", m_contentLanguage.ToString());

        }
        else if (m_refContentApi.GetCookieValue("LastValidLanguageID") != "")
        {
            int.TryParse(m_refContentApi.GetCookieValue("LastValidLanguageID"), out m_contentLanguage);
        }

        if (Request.QueryString["list"] != null)
        {
            bool.TryParse(Request.QueryString["list"], out m_isApprovalList);
        }
        else
        {
            m_isApprovalList = false;
        }

    }


}