﻿namespace Ektron.Workarea.DxH
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Settings.DxH;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Settings.DxH;
    using Ektron.DxH.Client;
    using Ektron.DxH.Client.Sharepoint;
    using Ektron.DxH.Common.Contracts;
    using Ektron.DxH.Common.Connectors;
    using Ektron.DxH.Common.Objects;
    using System.Linq;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Instrumentation;

    public class AddSharePointErrorArgs : EventArgs
    {
        public Exception ex { get; set; }
    }


    /// <summary>
    /// Add SharePoint Content Class
    /// </summary>
    public partial class AddSharePointContent : System.Web.UI.UserControl
    {


        #region Member Variables
        public ContentAPI _ContentApi;
        public EkMessageHelper _MessageHelper;
        protected long m_intId = 0;
        protected FolderData folder_data;
        public bool IsVerified
        {
            get
            {
                var rtnVal = ViewState["ConnectionVerified"] as bool?;
                if (rtnVal == null)
                {
                    ViewState["ConnectionVerified"] = false;
                    return false;
                }
                else
                    return (bool)rtnVal;
            }
            set
            {
                ViewState["ConnectionVerified"] = value;
            }
        }

        List<DxHConnectionData> connectionList = new List<DxHConnectionData>();

        public string sharePointConnectionId
        {
            get
            {

                return (ViewState["ConnName"] != null ? ViewState["ConnName"] as string : string.Empty);

            }
            set
            {

                ViewState["ConnName"] = value;
            }
        }
        #endregion

        #region Page Events
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
            Package contextbusControlPackage = new Package()
            {
                Components = new List<Component>()
                    {
                        Packages.Ektron.CssFrameworkBase,
                        Css.Create(cmsContextService.WorkareaPath + "/dxh/css/ektron.workarea.dxh.css")
                    }
            };
            contextbusControlPackage.Register(this);

            if (!(Request.QueryString["id"] == null))
            {
                m_intId = Convert.ToInt64(Request.QueryString["id"]);
            }

            _ContentApi = new ContentAPI();
            _MessageHelper = _ContentApi.EkMsgRef;

            folder_data = _ContentApi.GetFolderById(m_intId);
            SetResourceString();
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                bool doesInBound = DoesInBoundExist();
                if (!doesInBound)
                {
                    if (_ContentApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.DxHConnectionAdmin))
                    {
                        string here = GetLocalResourceObject("here").ToString();
                        string hereTitle = GetLocalResourceObject("click here").ToString();
                        string clickHere = "<a href='ViewDXHConnections.aspx?AdapterName=Ektron' title='" + hereTitle + "'>" + here + "</a>";
                        msgConnectionError.Text = string.Format(GetLocalResourceObject("addAddInBoundConnection").ToString(), clickHere);

                        this.RaiseOnError(new AddSharePointErrorArgs() { ex = new Exception(string.Format(GetLocalResourceObject("addAddInBoundConnection").ToString(), clickHere)) });
                    }
                    else
                    {
                        msgConnectionError.Text = GetLocalResourceObject("addInBoundConnection").ToString();

                        this.RaiseOnError(new AddSharePointErrorArgs() { ex = new Exception(GetLocalResourceObject("addInBoundConnection").ToString()) });
                    }
                    ltrConnectionError.Text = GetLocalResourceObject("noInBoundExists").ToString();

                    ltrTryAgain.Text = string.Empty;
                    //uxErrorDialog.AutoOpen = true;
                    //uxErrorDialog.Visible = true;

                    return;
                }

                if (!DoesSharePointExist())
                {
                    if (_ContentApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.DxHConnectionAdmin))
                    {
                        string here = GetLocalResourceObject("here").ToString();
                        string hereTitle = GetLocalResourceObject("click here").ToString();
                        string clickHere = "Please click <a href='ViewDXHConnections.aspx?AdapterName=SharePoint' title='" + hereTitle + "'>" + here + "</a> to add a connection.";
                        msgConnectionError.Text = string.Format(GetLocalResourceObject("msgConnectionError").ToString(), clickHere);

                        this.RaiseOnError(new AddSharePointErrorArgs() { ex = new Exception(string.Format(GetLocalResourceObject("msgConnectionError").ToString(), clickHere)) });
                    }
                    else
                    {
                        msgConnectionError.Text = GetLocalResourceObject("msgConnectionError").ToString();
                        this.RaiseOnError(new AddSharePointErrorArgs() { ex = new Exception(GetLocalResourceObject("msgConnectionError").ToString()) });
                    }
                    ltrConnectionError.Text = GetLocalResourceObject("ltrDxHError").ToString();

                    ltrTryAgain.Text = string.Empty;
                    //uxErrorDialog.AutoOpen = true;
                    //uxErrorDialog.Visible = true;



                    return;
                }
            }
            //get list of sharepoint connections
            try
            {
                DxHUtils dxhUtils = new DxHUtils();

                connectionList = dxhUtils.GetDxHConnectionList();
                connectionList = (from con in connectionList where con.AdapterName == "SharePoint" select con).ToList();
            }
            catch (Exception ex)
            {
                this.RaiseOnError(new AddSharePointErrorArgs()
                {
                    ex = new Exception("There has been an error retrieving the sharepoint connection.  This has been logged.", ex)
                });
                Log.WriteError(ex);
            }

            if (Request.Form[sharePointConnectionList.UniqueID] != null)
            {
                sharePointConnectionId = Request.Form[sharePointConnectionList.UniqueID].ToString();

            }
            if (Page.IsPostBack)
            {
                if (connectionList.Count == 1)
                {
                    sharePointConnectionId = connectionList[0].ConnectionName;
                }
            }
            else
            {
                if (connectionList != null)
                {
                    // Display select connection dialog or item tree or error dialog depending upon the number of connection list.
                    BindConnectionListDropDown();


                }
            }

            base.OnLoad(e);
        }
        #endregion

        # region String Localization
        protected void SetResourceString()
        {
            string addSharePointContentTitle = string.Format(GetLocalResourceObject("uxAddSharePointContentDialog").ToString(), folder_data.Name.ToString());

            uxErrorDialog.Title = addSharePointContentTitle;

            ltrConnectionError.Text = GetLocalResourceObject("ltrDxHError").ToString();
            msgConnectionError.Text = string.Format(GetLocalResourceObject("msgConnectionError").ToString(), Ektron.Cms.Content.ExternalTypeId.DxHSharePoint.ToString(), sharePointConnectionId);
        }
        # endregion

        #region Bind SharePoint List and Verify Connection code
        protected void BindConnectionListDropDown()
        {
            switch (connectionList.Count)
            {
                case 0:

                    // No connection exist error dialog.
                    //uxErrorDialog.AutoOpen = true;
                    //uxErrorDialog.Visible = true;

                    this.RaiseOnError(new AddSharePointErrorArgs() { ex = new Exception(GetLocalResourceObject("msgConnectionError").ToString()) });

                    break;

                default:
                    // More than one connection exist, so give user option to select one of them.
                    Session["SelectedSharePointConnection"] = sharePointConnectionId;
                    sharePointConnectionList.DataSource = connectionList;
                    sharePointConnectionList.DataBind();
                    sharePointConnectionList.SelectedValue = sharePointConnectionId;
                    break;
            }
        }

        private bool VerifyConnection()
        {
            ConnectionManagerClient conMgr = new ConnectionManagerClient();
            try
            {
                List<ConnectionParam> spParams = conMgr.LoadConnection(sharePointConnectionId, "SharePoint").ToList();
                return conMgr.TestConnection(spParams, "SharePoint");
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region EventHandlers

        public event EventHandler<AddSharePointErrorArgs> OnError;

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("content.aspx?action=ViewContentByCategory&id=" + this.folder_data.Id + "&treeViewId = 0");
        }
        protected void sharePointConnectionList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            sharePointConnectionList.Items.Clear();
            if (!VerifyConnection())
            {


                ltrConnectionError.Text = GetLocalResourceObject("ltrDxHError").ToString();
                msgConnectionError.Text = GetLocalResourceObject("unableToConnect").ToString();
                //uxErrorDialog.AutoOpen = true;
                //uxErrorDialog.Visible = true;

                this.RaiseOnError(new AddSharePointErrorArgs() { ex = new Exception(GetLocalResourceObject("unableToConnect").ToString()) });

                uxAddSharePointContentDialog.Visible = false;
            }
            else
            {
                IsVerified = true;
                //btnNext.Enabled = true;
                BindConnectionListDropDown();
                sharePointConnectionId = sharePointConnectionList.SelectedValue;
                var hiddenConnectionControl = Page.FindControl("uxConnectionName") as HiddenField;
                if (hiddenConnectionControl != null)
                {
                    hiddenConnectionControl.Value = sharePointConnectionId;
                }
            }
        }

        public void addToFolderCancel_click(object sender, EventArgs e)
        {
            if (connectionList.Count > 1)
            {
                BindConnectionListDropDown();
                uxAddSharePointContentDialog.Visible = true;
            }
        }
        #endregion

        protected bool DoesInBoundExist()
        {
            DxHUtils dxhUtils = new DxHUtils();
            Connection conn = dxhUtils.GetDxhConnection("", "Ektron");
            if (conn == null)
                return false;
            else
                return true;
        }

        protected bool DoesSharePointExist()
        {
            DxHUtils dxhUtils = new DxHUtils();
            Connection conn = dxhUtils.GetDxhConnection("", "sharepoint");
            if (conn != null)
                return true;
            else
                return false;
        }

        private void RaiseOnError(AddSharePointErrorArgs e)
        {
            if (this.OnError != null)
            {
                this.OnError(this, e);
            }
        }

    }

}