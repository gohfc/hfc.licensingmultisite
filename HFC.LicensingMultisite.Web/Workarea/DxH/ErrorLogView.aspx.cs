﻿namespace Workarea.DxH
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.DxH.Common;
    using Ektron.DxH.Common.Exceptions;
    using Ektron.DxH.Client;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Interfaces.Context;
    using System.Globalization;
    using System.Text;

    public partial class ErrorLogView : Ektron.Cms.Workarea.Page
    {
        protected System.Globalization.CultureInfo userCi =null;
        protected int LangId = 1033;
        private PagingToken<List<ExceptionLogItem>> res = null;
        private PagingToken<List<ExceptionLogItem>> pagingInfo = null;
        private bool isResultEmpty = false;

        int itemsPerPage = 10;
        Ektron.Cms.PagingInfo GridViewPagerInfo = null;

        private ExceptionManagerClient cbClient = null;
        private ContextBusClient cbServiceClient = null;
        private FilterBounds fBounds = null;
        private ExceptionLogCriteria filterCriteria = null;

        /// <summary>
        /// Private StyleHelper reference
        /// </summary>
        private StyleHelper refStyle = new StyleHelper();

        protected override void OnInit(EventArgs e)
        {
            


            ////ask DxH what the available values to filter against are:
            //FilterBounds f = exMgr.GetFilterBounds();
            //DateTime minDate = f.MinDate;                               //the earliest timestamp in the ExceptionLog
            //DateTime maxDate = f.MaxDate;                               //the "Max" date available at the time of this request
            //List<MessageSeverity> availableSeverities = f.Severities;   //list of severities that may be in db
            //List<string> availableSources = f.Sources;                  //list of sources that may be in db

            base.OnInit(e);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            
            EkGrid.PageSize = itemsPerPage;
            Ektron.Cms.SiteAPI _siteApi = new Ektron.Cms.SiteAPI();
            if (!Utilities.ValidateUserLogin())
            {
                Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
            }
            if (!_siteApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.DxHConnectionAdmin) && !_siteApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.SyncAdmin) &&
                !_siteApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.SyncUser))
            {
                Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
            }
            cbClient = new ExceptionManagerClient();
            cbServiceClient = new ContextBusClient();
            if (!cbServiceClient.TestDxhConnection(cbServiceClient.ServiceUrl))
            {
                Response.Redirect("ViewDXHConnections.aspx", true);
                return;
            }

            RegisterResources();
            GetHelpButton();

            //set timepicker inFieldLabel text
            DateTime midnight = DateTime.Today;

            fromTimepickerInfieldLabel.Text = midnight.ToLongTimeString();
            toTimepickerInfieldLabel.Text = midnight.ToLongTimeString();
            ltrTimeZoneText.Text = TimeZoneInfo.Local.DisplayName;


            userCi = Ektron.Cms.Common.EkFunctions.GetCultureInfo( _siteApi.RequestInformationRef.UserCulture.ToString());
            LangId=_siteApi.RequestInformationRef.ContentLanguage;
            TotalPages = 0;
            CurrentPageIndex = 0;

            aspDateRangeDropDown.Attributes.Add("onChange", "DisplayFromTo();");

            if (IsPostBack)
            {
                if (string.IsNullOrEmpty(this.EkGrid.EktronUIOrderByFieldText))
                {
                    this.EkGrid.EktronUIOrderByFieldText = "LogRowID";
                }
            }
            
        }

        protected void GetHelpButton()
        {
            litHelp.Text = this.refStyle.GetHelpButton("dxh_errorlog", string.Empty);
        }
        protected void RegisterResources()
        {
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
            Package timepickerResources = new Package()
            {
                Components = new List<Component>
                {
                    Packages.EktronCoreJS,
                    Packages.jQuery.jQueryUI.Datepicker,
                    Packages.jQuery.jQueryUI.Slider,
                    Packages.jQuery.Plugins.InfieldLabels,
                    Css.Create(cmsContextService.UIPath + "/css/jQuery/Plugins/ektron-ui-timepicker-addon.css"),
                    JavaScript.Create(cmsContextService.UIPath + "/js/jQuery/Plugins/ektron-ui-timepicker-addon.js")
                }
            };
            Package resources = new Package()
            {
                Components = new List<Component>
                {
                    Packages.Ektron.Namespace,
                    Packages.Ektron.CssFrameworkBase,
                    Packages.Ektron.Workarea.Core,
                    Css.Create(cmsContextService.WorkareaPath + "/DxH/css/Ektron.Workarea.DxH.ErrorLog.css"),
                    timepickerResources
                }
            };
            resources.Register(this);
        }

        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //create a list of filter criteria to apply to executionLog:
            filterCriteria = new ExceptionLogCriteria();
            //filterCriteria.Filters.Add(new ExceptionLogFilter() { FilterPropertyName = ExceptionLogProperty.Date, Operator = ExceptionLogOperator.Between, Value = "TEST" });

            fBounds = cbClient.GetFilterBounds();
            
            

            if (!Page.IsPostBack)
            {
                aspSourceList.DataSource = fBounds.Sources;
                aspSourceList.DataBind();

                aspSeverityCheckBoxList.DataSource = fBounds.Severities;
                aspSeverityCheckBoxList.DataBind();

                aspEventIDList.DataSource = fBounds.EventIDs;
                aspEventIDList.DataBind();

                uxFromDatePicker.Text = fBounds.MinDate.ToString();
                uxToDatePicker.Text = fBounds.MaxDate.ToString();

                PopulateEventLogGrid();
            }
            else 
            {
                //uxToDatePicker.OverrideDefaultCulture
                //implement filtering of the results
               
                
            }

        }
            //pagingInfo = new PagingToken<List<ExceptionLogItem>>(EkGrid.PageSize);

        protected void PopulateEventLogGrid()
        {
            pagingInfo = new PagingToken<List<ExceptionLogItem>>(EkGrid.PageSize);
            pagingInfo.CurrentPage = 0;

            res = cbClient.GetExceptionLogItems(pagingInfo, filterCriteria);
            TotalPages = res.EstimatedPageCount;
            CurrentPageIndex = res.CurrentPage;

            EkGrid.DataSource = res.PageData;
            GridViewPagerInfo = new Ektron.Cms.PagingInfo();
            GridViewPagerInfo.CurrentPage = CurrentPageIndex;
            GridViewPagerInfo.TotalPages = TotalPages;
            GridViewPagerInfo.TotalRecords = res.TotalNumberOfItems;
            GridViewPagerInfo.RecordsPerPage = itemsPerPage;
            EkGrid.EktronUIPagingInfo = GridViewPagerInfo;
            if(!Page.IsPostBack)
                EkGrid.EktronUIOrderByFieldText = "LogRowID"; 

            if (res.PageData.Count == 0)
                isResultEmpty = true;
            
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if(res!=null)
                res.PageData.ForEach(x => x.Message = HttpUtility.HtmlEncode(x.Message));
            if (!isResultEmpty)
            {
                EkGrid.Visible = true;
                EkGrid.DataBind();
            }
            else
            {
                EkGrid.Visible = false;
                msgNoRecords.Visible = true;
            }
        }

        #region Pager Events
        private int TotalPages
        {
            get
            {
                if (ViewState["VW_pager_totalPage"] != null)
                {
                    return (int)ViewState["VW_pager_totalPage"];
                }
                return 0;
            }
            set
            {
                ViewState["VW_pager_totalPage"] = value;
            }
        }
        private int CurrentPageIndex
        {
            get
            {
                if (ViewState["VW_pager_CurrentPageIndex"] != null)
                {
                    return (int)ViewState["VW_pager_CurrentPageIndex"];
                }
                return 0;
            }
            set
            {
                ViewState["VW_pager_CurrentPageIndex"] = value;
            }
        }

        private void ChangePage(string Action, int GoToNum)
        {
            pagingInfo = new PagingToken<List<ExceptionLogItem>>(EkGrid.PageSize);
            switch (Action)
            {
                case "first":
                    pagingInfo.CurrentPage = 0;
                    break;
                case "previous":
                    if (CurrentPageIndex > 1)
                        CurrentPageIndex = CurrentPageIndex-2;
                    pagingInfo.CurrentPage = CurrentPageIndex;
                    break;
                case "next":
                     if (CurrentPageIndex < TotalPages)
                        pagingInfo.CurrentPage = CurrentPageIndex;
                    break;
                case "last":
                    pagingInfo.CurrentPage = TotalPages - 1;
                    break;
                case "goto":
                    if (GoToNum > 0 && GoToNum <= TotalPages)
                        pagingInfo.CurrentPage = GoToNum-1;
                    break;
            }

            PopulateFilterCriteria();
            //if (EkGrid.EktronUIOrderByDirection == Ektron.Cms.Common.EkEnumeration.OrderByDirection.Ascending)
            //    filterCriteria.SortOrder = ExceptionLogSortOrder.Ascending;
            //else
            //    filterCriteria.SortOrder = ExceptionLogSortOrder.Descending;

            ExceptionLogProperty outVal = ExceptionLogProperty.LogID;
            Enum.TryParse<ExceptionLogProperty>(EkGrid.EktronUIOrderByFieldText, out outVal);
            filterCriteria.SortPropertyName = outVal;
            if (EkGrid.EktronUIOrderByDirection == Ektron.Cms.Common.EkEnumeration.OrderByDirection.Ascending)
            {
                filterCriteria.SortOrder = ExceptionLogSortOrder.Ascending;

            }
            else
            {
                filterCriteria.SortOrder = ExceptionLogSortOrder.Descending;

            }


            res = cbClient.GetExceptionLogItems(pagingInfo, filterCriteria);
            
            TotalPages = res.EstimatedPageCount;
            CurrentPageIndex = res.CurrentPage;

            GridViewPagerInfo = new Ektron.Cms.PagingInfo();
            GridViewPagerInfo.CurrentPage = CurrentPageIndex;
            GridViewPagerInfo.TotalPages = TotalPages;
            GridViewPagerInfo.TotalRecords = res.TotalNumberOfItems;
            GridViewPagerInfo.RecordsPerPage = itemsPerPage;
            EkGrid.EktronUIPagingInfo = GridViewPagerInfo;
            EkGrid.DataSource = res.PageData;
        }

        #endregion

        protected void EkGrid_EktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {

            ChangePage("goto", e.PagingInfo.CurrentPage);
        }

        protected void EkGrid_EktronUIThemeSortChanged(object sender, GridViewEktronUIThemeSortChangedEventArgs e)
        {
            string s = e.OrderByFieldText;
            PopulateFilterCriteria();
            filterCriteria.SortPropertyName = (ExceptionLogProperty)Enum.Parse(typeof(ExceptionLogProperty), e.OrderByFieldText, true);
            if (e.OrderByDirection == Ektron.Cms.Common.EkEnumeration.OrderByDirection.Ascending)
            {
                filterCriteria.SortOrder = ExceptionLogSortOrder.Ascending;
                
            }
            else
            {
                filterCriteria.SortOrder = ExceptionLogSortOrder.Descending;
                
            }
            PopulateEventLogGrid();           

        
        }

        protected void PopulateFilterCriteria()
        {
            List<string> inValsEventIDs = new List<string>();
            List<string> inValsSource = new List<string>();
            List<string> inValsSeverity = new List<string>();
            ExceptionLogFilter DTFilter = new ExceptionLogFilter();

            DTFilter.FilterPropertyName = ExceptionLogProperty.Date;
            DTFilter.Operator = ExceptionLogOperator.Between;

            string dateLoggedIndex = aspDateRangeDropDown.SelectedIndex.ToString();

            switch (dateLoggedIndex)
            {
                // Anytime (default)
                case "0":
                    DTFilter.Value = new DateTime[] { System.Data.SqlTypes.SqlDateTime.MinValue.Value, System.Data.SqlTypes.SqlDateTime.MaxValue.Value };
                    break;
                // Last hour
                case "1":
                    DTFilter.Value = new DateTime[] { DateTime.Now.AddHours(-1.00).ToUniversalTime(), DateTime.Now.ToUniversalTime() };
                    break;
                // Last 12 hours
                case "2":
                    DTFilter.Value = new DateTime[] { DateTime.Now.AddHours(-12.00).ToUniversalTime(), DateTime.Now.ToUniversalTime() };
                    break;
                // Last 24 hours
                case "3":
                    DTFilter.Value = new DateTime[] { DateTime.Now.AddHours(-24.00).ToUniversalTime(), DateTime.Now.ToUniversalTime() };
                    break;
                // Last 7 days
                case "4":
                    DTFilter.Value = new DateTime[] { DateTime.Now.AddDays(-7).ToUniversalTime(), DateTime.Now.ToUniversalTime() };
                    break;
                // Last 30 days
                case "5":
                    DTFilter.Value = new DateTime[] { DateTime.Now.AddDays(-30).ToUniversalTime(), DateTime.Now.ToUniversalTime() };
                    break;
                // Custom range...
                case "6":
                    string startDate = uxFromDatePicker.Text;
                    string endDate = uxToDatePicker.Text;
                    DateTime objStartDate = Convert.ToDateTime(startDate).ToUniversalTime();
                    DateTime objEndDate = Convert.ToDateTime(endDate).ToUniversalTime();
                    DTFilter.Value = new DateTime[] { objStartDate, objEndDate };
                    break;
            }

            for (int i = 0; i < aspEventIDList.Items.Count; i++)
            {
                if (aspEventIDList.Items[i].Selected)
                {
                    inValsEventIDs.Add(aspEventIDList.Items[i].Value);
                }
            }

            for (int i = 0; i < aspSourceList.Items.Count; i++)
            {
                if (aspSourceList.Items[i].Selected)
                {
                    inValsSource.Add(aspSourceList.Items[i].Value);
                }
            }

            for (int i = 0; i < aspSeverityCheckBoxList.Items.Count; i++)
            {
                if (aspSeverityCheckBoxList.Items[i].Selected)
                {
                    Ektron.DxH.Logging.LogSeverity tmpServ = (Ektron.DxH.Logging.LogSeverity)Enum.Parse(typeof(Ektron.DxH.Logging.LogSeverity), aspSeverityCheckBoxList.Items[i].Value, true);
                    inValsSeverity.Add(((int)tmpServ).ToString());
                }
            }

            filterCriteria = new ExceptionLogCriteria();

            if (inValsSeverity.Count > 0)
            {
                filterCriteria.Filters.Add(new ExceptionLogFilter() { FilterPropertyName = ExceptionLogProperty.Severity, Operator = ExceptionLogOperator.In, Value = inValsSeverity });
            }
            if (inValsEventIDs.Count > 0)
            {
                filterCriteria.Filters.Add(new ExceptionLogFilter() { FilterPropertyName = ExceptionLogProperty.EventID, Operator = ExceptionLogOperator.In, Value = inValsEventIDs });
            }
            if (inValsSource.Count > 0)
            {
                filterCriteria.Filters.Add(new ExceptionLogFilter() { FilterPropertyName = ExceptionLogProperty.Source, Operator = ExceptionLogOperator.In, Value = inValsSource });
            }

            filterCriteria.Filters.Add(DTFilter);

            //return filterCriteria;
            
        }
        protected void uxFilterResults_Click(object sender, EventArgs e)
        {

            ExceptionLogCriteria filterCriteria = new ExceptionLogCriteria();
            PopulateFilterCriteria();
            PopulateEventLogGrid();
            
        }
        protected void uxClear_Click(object sender, EventArgs e)
        {
            filterCriteria = new ExceptionLogCriteria();
            PopulateEventLogGrid();
        }
        protected void exportLog_Click(object sender, EventArgs e) 
        {
            //Page size<0 = All items
            pagingInfo = new PagingToken<List<ExceptionLogItem>>(-1);
            PopulateFilterCriteria();
            res = cbClient.GetExceptionLogItems(pagingInfo, filterCriteria);
            getCSVFile(res);
        }

        void getCSVFile(List<ExceptionLogItem> lst)
        {
            
            List<string> propList = null;
            StringBuilder sb = new StringBuilder();
            List<string> tempArray = new List<string>();
            Type tEI = typeof(ExceptionLogItem);

            propList = new List<string>();

            foreach (System.Reflection.PropertyInfo pi in typeof(ExceptionLogItem).GetProperties())
            {
                propList.Add(pi.Name);
                sb.Append(pi.Name);
                sb.Append(",");
            }

            sb.Append("\r\n");

            foreach (ExceptionLogItem li in lst)
            {
                foreach (string pName in propList)
                {
                    tempArray.Add(tEI.GetProperty(pName).GetValue(li, null).ToString().Replace("\r\n", " ").Replace(",", ";"));
                }

                sb.Append(string.Join(",", tempArray.ToArray()));
                sb.Append("\r\n");
                tempArray.Clear();
            }

            string fileName = "DXH_Log_" + DateTime.Now;
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename="+fileName+".csv");
            Response.ContentType = "Application/x-msexcel";
            Response.Write(sb.ToString());
            Response.End();

        }


    }
}