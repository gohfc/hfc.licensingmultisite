﻿namespace Workarea.DxH.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class RequiredField : System.Web.UI.UserControl
    {
        public short TabIndex
        {
            get
            {
                return tfRequired.TabIndex;
            }
            set
            {
                tfRequired.TabIndex = value;
            }
        }

        public string Value
        {
            get
            {
                return tfRequired.Value;
            }
            set
            {
                tfRequired.Text = value;
            }
        }
        public string CssClass
        {
            get
            {
                return tfRequired.CssClass;
            }
            set
            {
                tfRequired.CssClass = value;
            }
        }
    }
}