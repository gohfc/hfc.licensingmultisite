﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjectDefMappingUtility.ascx.cs"
    Inherits="Ektron.DxH.Client.Objects.ObjectDefMappingUtility" %>

    <ektronUI:GridView AllowPaging="false" AllowSorting="false" AutoGenerateColumns="false" ID="ekGrid" runat="server" CssClass="uxMappingGrid">
            <Columns>

             <asp:TemplateField HeaderText="*" HeaderStyle-CssClass="ektron-ui-required">
                    <ItemStyle CssClass="ekton-ui-alignCenter" />
                    <ItemTemplate>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%# DataBinder.Eval(Container.DataItem, "IsRequired") %>'>
                            <span class="ektron-ui-required">*</span>
                        </asp:PlaceHolder>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                
                <HeaderTemplate>
                    <asp:Literal runat="server" ID="ltrTargetMeta" Text='<%# this.TargetFieldHeaderText %>' />
                </HeaderTemplate>
                    <ItemTemplate>
                        <p><%#  DataBinder.Eval(Container.DataItem, "Source.DisplayName")%></p>
                        <input type="hidden" name='<%# DataBinder.Eval(Container.DataItem,"Source.Id") %>' value='<%# DataBinder.Eval(Container.DataItem,"Source.Id") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
               
                <ektronUI:SelectListField ID="SelectList">
                    <SelectListFieldColumnStyle Visible="true" runat="server" />
                    <SelectListFieldHeaderStyle HeaderText='<%# this.SourceFieldHeaderText %>' runat="server" />
                    

                    <KeyFields>
                        <ektronUI:KeyField DataField="Id" />
                        <ektronUI:KeyField DataField="SourceID" />
                    </KeyFields>

                    <ValueFields>
                        <ektronUI:ValueField Key="MappedSourceId" DataField="Source.Id" />
                        <ektronUI:ValueField Key="ObjectDefinitionId" DataField="ObjectDefinitionId" />
                    </ValueFields>


                    <Options DataField="Target" OptionTextDataField="DisplayName" OptionValueDataField="Id"   />
                </ektronUI:SelectListField>
                
            </Columns>
        </ektronUI:GridView>
