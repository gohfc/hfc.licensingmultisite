﻿namespace Workarea.DxH.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;


    public partial class RequiredURLField : System.Web.UI.UserControl
{

    public short TabIndex
        {
            get
            {
                return tfRequiredURL.TabIndex;
            }
            set
            {
                tfRequiredURL.TabIndex = value;
            }
        }

        public string CssClass
        {
            get
            {
                return this.tfRequiredURL.CssClass;
            }
            set
            {
                this.tfRequiredURL.CssClass = value;
            }
        }
    }

}