﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImportSPListOrItem.ascx.cs"
    Inherits="Ektron.Workarea.DxH.ImportSPListOrItem" %>
<div class="select-list-options">
    <strong>
        <asp:Literal runat="server" ID="ltrOptionQuestion" /></strong>
    <asp:RadioButtonList ID="aspSharePointContentOption" CssClass="unorderedListMargin"
        runat="server" RepeatLayout="UnorderedList" AutoPostBack="true">
        <asp:ListItem Text="<% $Resources:listRadio  %>" Value="ByList" Selected="True" />
        <asp:ListItem Text="<% $Resources:itemRadio  %>" Value="ByItem" />
    </asp:RadioButtonList>
    <br />
    <strong><asp:Literal ID="ltrLanguageOption" runat="server" Text = "<% $Resources:ltrLanguageOption  %>" /></strong>
    <br />
    <asp:DropDownList ID="selectlanguageContent" CssClass="dropdownListmargin" runat="server" AutoPostBack="true" 
        OnSelectedIndexChanged="selectlanguageContent_SelectedIndexChanged" />
    <br />
    <br />
    <strong><asp:Literal ID="ltrDeleteQuestion" runat="server" Text = "<% $Resources:ltrDeleteQuestion  %>" /></strong>
    <asp:RadioButtonList ID="radioDeleteContent" CssClass="unorderedListMargin" runat="server"
        RepeatLayout="UnorderedList" AutoPostBack="true" OnSelectedIndexChanged="radioDeleteContent_SelectedIndexChanged">
        <asp:ListItem Text="<% $Resources:ltrDeleteDoNot  %>" Value="False" Selected="false" />
        <asp:ListItem Text="<% $Resources:ltrDeleteYes  %>" Value="True" Selected="false" />
    </asp:RadioButtonList>  
</div>
