﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImportSPGridTreeList.ascx.cs"
    Inherits="Ektron.Workarea.DxH.ImportSPGridTreeList" %>

<div>
    <p><asp:Literal runat="server" ID="ltrAddSharePointContentMsg" /></p>
</div>
    
<div class="allLists-select-grid">
    <ektronUI:GridView ID="uxSelectListGrid" runat="server" AutoGenerateColumns="false">
        <Columns>
            <ektronUI:CheckBoxField ID="uxSelectByListField" OnCheckedChanged="uxSelectListGrid_uxSelectByListField_OnCheckedChanged">
                <CheckBoxFieldColumnStyle />
                <KeyFields>
                    <ektronUI:KeyField DataField="IdHash" />
                </KeyFields>
                <ValueFields>
                    <ektronUI:ValueField Key="Id" DataField="Id" />
                    <ektronUI:ValueField Key="List" DataField="List" />
                    <ektronUI:ValueField Key="ObjectDefinitionId" DataField="ObjectDefinitionId" />
                </ValueFields>
            </ektronUI:CheckBoxField>
            <ektronUI:RadioButtonField ID="uxSelectByItemField" OnSelectedIndexChanged="uxSelectListGrid_uxSelectByItemField_SelectedIndexChanged">
                <RadioButtonFieldColumnStyle />
                <KeyFields>
                    <ektronUI:KeyField DataField="IdHash" />
                </KeyFields>
                <ValueFields>
                    <ektronUI:ValueField Key="Id" DataField="Id" />
                    <ektronUI:ValueField Key="List" DataField="List" />
                    <ektronUI:ValueField Key="ObjectDefinitionId" DataField="ObjectDefinitionId" />
                </ValueFields>
            </ektronUI:RadioButtonField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Literal runat="server" ID="ltrListTitle" Text="<%# this.SelectListGridHeaderText %>" />
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("List") %>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </ektronUI:GridView>
</div>
<div class="ektron-ui-clearfix">
    <asp:Panel ID="aspItemListTreeWrapper" CssClass="itemList-select-tree ektron-ui-floatLeft"
        runat="server" Visible="false">
        <h2 id="uxTreeHeader" class="treeHeader" runat="server" /><ektronUI:Tree ID="uxTree" runat="server" SelectionMode="SingleForAll" OnSelectionEvent="OnSelectionHandler"
            AutoPostBackOnSelectionChanged="true" CssClass="ektron-ui-tree-sharePoint" BeforeAjax="Ektron.DxH.Tree.ModifyTree" >
        </ektronUI:Tree>
        <ektronUI:JavaScriptBlock ID="uxTreeOverlayBindings" runat="server">
            <ScriptTemplate>
                var selector = ".ektron-ui-tree"; $ektron(selector).on("afterAjaxCallback", function
                (event, tree, obj) { dxhOverlay(false); }); $ektron(selector).on("beforeAjaxCallback",
                function (event, tree, obj) { dxhOverlay(); }); $ektron(selector).on("postback",
                function (event, tree, obj) { dxhOverlay(); });
               
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
         <ektronUI:JavaScriptBlock ID="uxTreeCallBackOverride" runat="server" ExecutionMode="OnEktronReady">
            <ScriptTemplate>
                if(!Ektron.Namespace.Exists("Ektron.DxH.Tree.ModifyTree"))
                {
                    Ektron.Namespace.Register("Ektron.DxH.Tree.ModifyTree");
                    Ektron.DxH.Tree.ModifyTree = function(callBackData){
                        var connName = $ektron('#uxConnectionName').val(),
                            connIdentifier = "ConnectionName:";
                       callBackData.data = connIdentifier + connName + "_ConDelim_" + callBackData.data;
                     };
                }

               
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
    </asp:Panel>
    <asp:Panel ID="aspItemListGridWrapper" CssClass="itemList-select-grid" runat="server">
        <ektronUI:GridView ID="uxSelectItemsGrid" runat="server" Visible="false" Caption="<%# this.SelectItemsGridHeaderText %>"
            AutoGenerateColumns="false" OnEktronUIPageChanged="uxSelectItemsGrid_EktronUIThemePageChanged">
            <Columns>
                <ektronUI:CheckBoxField ID="uxSelectItemField" OnCheckedChanged="uxSelectItemsGrid_uxSelectItemField_SelectedIndexChanged">
                    <CheckBoxFieldHeaderStyle HeaderCheckBoxVisible="<%# SelectItemsGrid_SelectItemField_IsCheckboxVisible %>" />
                    <CheckBoxFieldItemStyle Visible="<%# this.SelectItemsGrid_SelectItemField_IsCheckboxVisible %>" />
                    <KeyFields>
                        <ektronUI:KeyField DataField="IdHash" />
                    </KeyFields>
                    <ValueFields>
                        <ektronUI:ValueField Key="Id" DataField="Id" />
                        <ektronUI:ValueField Key="List" DataField="List" />
                        <ektronUI:ValueField Key="Name" DataField="Name" />
                        <ektronUI:ValueField Key="ObjectDefinitionId" DataField="ObjectDefinitionId" />
                    </ValueFields>
                </ektronUI:CheckBoxField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Literal ID="Literal1" runat="server" Text='<%# this.SelectItemsGridHeader %>' />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Literal runat="server" ID="ltrDisplayName" Text='<%# Bind("Name") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </ektronUI:GridView>
        <ektronUI:GridView ID="uxSelectedItemsGrid" runat="server" Caption="<%# this.SelectItemsGridHeaderText %>"
            AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField ItemStyle-Width="25px">
                    <ItemTemplate>
                        <asp:ImageButton ID="uxRemoveSelectedItem" runat="server" ImageUrl="~/Workarea/FrameworkUI/images/silk/icons/delete.png"
                            ToolTip="<% $Resources:Remove %>" OnCommand="SelectedItemsGrid_RemoveItem" CommandName="Remove"
                            CommandArgument="<%# this.GetSelectedItemsGrid_RemoveItem_CommandArument((Ektron.Workarea.DxH.DataListObject)Container.DataItem) %>"
                            Visible="<%# this.SelectedItemsGrid_SelectItemField_IsRemoveButtonVisible %>"
                            
                            />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<% $Resources:ItemOrDocument %>">
                    <ItemTemplate>
                        <asp:Literal runat="server" ID="aspSelectedItemDisplayName" Text='<%# Bind("Name") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<% $Resources:List %>">
                    <ItemTemplate>
                        <%# Eval("List") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </ektronUI:GridView>
       
    </asp:Panel>
</div>
<ektronUI:Dialog ID="uxErrorDialog" runat="server" Modal="True" Width="640" Resizable="False"
    Visible="false" EnableViewState="False" AutoOpen="False" Draggable="True" Enabled="False"
    Stack="True">
    <ContentTemplate>
        <table>
            <tr>
                <td style="text-align: left; vertical-align: middle; width: 25%;">
                    <asp:Image ID="aspDXHLogo" ImageUrl="~/Workarea/DxH/images/dxh_16x16_blue.png" runat="server"
                        ToolTip="<% $Resources: dxhlogo %>" />
                </td>
                <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em;">
                    <asp:Literal ID="ltrConnectionError" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
        <ektronUI:Message ID="msgConnectionError" runat="server" DisplayMode="Error" />
        <asp:Literal ID="ltrTryAgain" runat="server" Text="<%$Resources:ltrTryAgain %>"></asp:Literal>
    </ContentTemplate>
    <Buttons>
        <ektronUI:DialogButton runat="server" ID="btnClose" CloseDialog="True" CausesValidation="True"
            Text="<%$Resources: close %>" ToolTip="<%$Resources: close %>" />
    </Buttons>
</ektronUI:Dialog>

<asp:HiddenField ID="uxHiddenSelectedItems" runat="server" />
<asp:HiddenField ID="uxSelectListGridDirtyFields" runat="server" />