﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequiredURLField.ascx.cs" Inherits="Workarea.DxH.Controls.RequiredURLField" %>
<ektronUI:TextField runat="server" ID="tfRequiredURL">
    <ValidationRules>
        <ektronUI:RequiredRule />
        <ektronUI:UrlRule />
    </ValidationRules>
</ektronUI:TextField>