﻿(function ($) {
    $.widget("ektron.tree", {
        options: {
            dataField: {},
            json: '[]',
            autoOpen: false
        },
        _create: function (options) {
            this.dataField = $(this.element).parent().prev();
            this._setup.self = this;
            this._markupData = this.options.json;
            this._treeContainer = this._setup.self.element;
            this._treeContainerId = this._treeContainer.attr("id");
            this._treeUniqueId = this._treeContainer.find("#" + this._treeContainerId + "_controlUniqueId").val();
            this._treeSelectionsField = this._treeContainer.find("#" + this._treeContainerId + "_selections");
            this._treeExpandedNodesField = this._treeContainer.find("#" + this._treeContainerId + "_expandedNodes");
            this._dataField = $("#" + this._treeContainerId + "_serialized_data");
            this._setup.setMarkup();
            this._setup.bindEvents();
            this._setup.getData();
        },
        _setup: {
            self: {},
            bindEvents: function () {
                var tree = this.self;
            },
            getData: function () {
                //if (this.self.dataField.val() != undefined) {
                //    this.self.data = Ektron.JSON.parse(this.self.dataField.val());
                //}
            },
            setMarkup: function () {
                var tree = this.self;
                var containerId = tree._treeContainerId;
                var treeRootElement = tree._treeContainer.find("#" + tree._treeContainerId + "_TreeRootElement");

                tree._applyJsonToTemplate(tree, treeRootElement, tree._markupData)
                tree._bindToggleableParentNodes(tree, tree._treeContainer);
                tree._bindParentNodesWithDefferredLoad(tree, tree._treeContainer);
                tree._bindPagingNodes(tree, tree._treeContainer);
                tree._bindSelectionActivity(tree, tree._treeContainer);
            }
        },
        _bindToggleableParentNodes: function (tree, container) {
            container.find(".expandable, collapsable").click(tree.toggle);
        },
        _bindParentNodesWithDefferredLoad: function (tree, container) {
            container.find("[data-ektron-action=\"LoadChildren\"]").click(function () {
                var obj = $ektron(this);
                obj.unbind("click");
                var dataId = obj.attr("data-ektron-id");
                var path = obj.attr("data-ektron-path");
                tree._ServerCallback(tree, tree._treeUniqueId, 'loadChildren:' + path, 'loadChildren^' + tree._treeContainerId + "^" + dataId);
            });
        },
        _bindPagingNodes: function (tree, container) {
            container.find("[data-ektron-action=\"GetMore\"]").click(function () {
                var obj = $ektron(this);
                obj.unbind("click");
                var dataId = obj.attr("data-ektron-id");
                var path = obj.attr("data-ektron-path");
                tree._ServerCallback(tree, tree._treeUniqueId, 'getMore:' + path, 'getMore^' + tree._treeContainerId + "^" + dataId);
            });
        },
        _bindSelectionActivity: function (tree, container) {

            // bind checkboxes, track selections in hidden field for server
            container.find("[data-ektron-selectionmode=\"MultiForAll\"] input[type=\"checkbox\"], [data-ektron-selectionmode=\"MultiForItem\"] input[type=\"checkbox\"]").click(function () {
                var src = $ektron(this);
                var dataId = src.closest("li").attr("data-ektron-id");
                var selections = tree._treeSelectionsField.val();
                selections = tree._getTargetNodeIds(selections, dataId, src.is(":checked"), "^");
                tree._treeSelectionsField.val(selections);
            });

            // bind radio buttons
            // TODO: ...

            // bind postbacks
            // TODO: ...

            // bind callbacks
            // TODO: ...

        },
        _applyJsonToTemplate: function (tree, targetElement, data) {
            var itemTemplate = tree._treeContainer.find("#" + tree._treeContainerId + "_ItemTemplate");
            var jsonData = Ektron.JSON.parse(data);
            $(itemTemplate).tmpl(jsonData).appendTo(targetElement);
        },
        _setData: function () {
            //TODO: ... this._dataField.val(Ektron.JSON.stringify(this.data));
        },
        _setOption: function (key, value) {
            switch (key) {
                case "clear":
                    break;
            }
            $.Widget.prototype._setOption.apply(this, arguments);
            this._super("_setOption", key, value);
        },
        toggle: function (e) {
            var obj = $ektron(this).eq(0);
            var childContainer = $("[data-ektron-id='" + obj.attr("data-ektron-id") + "_childContainer']");
            var parentListItem = childContainer.closest("li");

            if (e != null && e.target != null) {
                // dont hijack active links
                var target = $ektron(e.target);
                if (target.is("input")) { return; }
                if (target.is("a")) {
                    var href = target.attr("href");
                    var onClick = target.attr("onclick");
                    if (("undefined" != typeof href && href != null && href.length > 0)
                    || ("undefined" != typeof onClick && onClick != null && onClick.length > 0)) {
                        return;
                    }
                }
            }
            if (obj == null) { return; }
            if (!obj.is("li")) { obj = obj.closest("li"); }
            if (obj == null) { return; }

            if (parentListItem.is(".collapsable, .expandable")) {
                parentListItem.toggleClass("expandable").toggleClass("collapsable");
            }

            var tree = obj.closest(".ektron-ui-control").data().tree;
            if (tree == null) { return false; }

            // track expanded/non-expanded state in hidden field for server
            var dataId = obj.attr("data-ektron-id");
            var expandedNodes = tree._treeExpandedNodesField.val();
            expandedNodes = tree._getTargetNodeIds(expandedNodes, dataId, obj.hasClass("collapsable"), "^");
            tree._treeExpandedNodesField.val(expandedNodes);

            return false;
        },
        _getTargetNodeIds: function (targetNodes, dataId, addId, delimiter) {
            var targetNodeArray = targetNodes.split(delimiter);
            var targetNode = "";
            var len = targetNodeArray.length;
            if (addId) {
                var found = false;
                for (var i = 0; i < len; i++) {
                    targetNode = targetNodeArray[i];
                    if (found = (targetNode == dataId)) {
                        break;
                    }
                }
                if (!found) {
                    if (targetNodes.length > 0) { targetNodes += delimiter; }
                    targetNodes += dataId;
                }
            } else {
                var delim = targetNodes = "";
                for (var i = 0; i < len; i++) {
                    targetNode = targetNodeArray[i];
                    if (targetNode != dataId) {
                        targetNodes += delim + targetNode;
                        delim = delimiter;
                    }
                }
            }
            return targetNodes;
        },
        _ServerCallback: function (tree, callbackUniqueId, args, context) {
            var outData = "&__CALLBACKID=" + callbackUniqueId; // Must use ASP.NET UniqueID as that's what's expected on server side when control is used with master pages.
            outData += "&__CALLBACKPARAM=" + escape(args);
            outData += "&__VIEWSTATE="; // + encodeURIComponent($ektron("#__VIEWSTATE").attr("value"));
            var ajaxUrl = window.location.href.replace(window.location.hash, ""); // IE/Windows will block callbacks with a pound/hash symbol in the URL.
            $ektron.ajax({
                type: "POST",
                url: ajaxUrl,
                data: outData,
                dataType: "html",
                error: function (inData) { console.log("inData: " + inData + ", context: " + context); },
                success: function (inData) {
                    var PACKET_START = "EKTRON_PACKET_START|";
                    var payload = String(inData);
                    var idx = payload.indexOf(PACKET_START);
                    if (idx >= 0) {
                        payload = payload.substring(idx + PACKET_START.length);
                    }

                    // Load Children
                    if (context != null && context.indexOf("loadChildren^") >= 0) {
                        var dataId = context.split("^")[2];
                        var targetElement = tree._treeContainer.find("ul[data-ektron-id='" + dataId + "_childContainer']");
                        tree._applyJsonToTemplate(tree, targetElement, payload)
                        tree._bindToggleableParentNodes(tree, targetElement);
                        tree._bindParentNodesWithDefferredLoad(tree, targetElement);
                        tree._bindSelectionActivity(tree, targetElement);
                    }
                }
            });

            return false;
        },
        destroy: function () {
            $.Widget.prototype.destroy.call(this);
        }
    });
} ($ektron));