﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using Ektron.Cms.Framework.Settings.DxH;
//using Ektron.Cms.Framework.UI;
//using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
//using Ektron.Cms.Interfaces.Context;
//using Ektron.Cms.Settings.DxH;
//using Ektron.DxH.Client;
//using Ektron.DxH.Client.Sharepoint;
//using Ektron.DxH.Common.Contracts;
//using Ektron.DxH.Common.Connectors;
//using Ektron.DxH.Common.Objects;


//public partial class SharePointTree : System.Web.UI.UserControl
//{
//    #region members
//    private ICmsContextService _iCmsContextService;
//    public event EventHandler<TreeSelectionEventArgs> SelectionEvent;

//    #endregion

//    #region properties
//    public Object DataSource
//    {
//        get
//        {
//            return uxSharePointTree.DataSource;
//        }
//        set
//        {
//            uxSharePointTree.DataSource = value;
//        }
//    }

//    public string[] Selections
//    {
//        //get
//        //{
//        //    return uxSharePointTree.Selections;
//        //}
//        //set
//        //{
//        //    uxSharePointTree.Selections = value;
//        //}
//    }
//    protected ICmsContextService ICmsContextService
//    {
//        get { return _iCmsContextService ?? (_iCmsContextService = ServiceFactory.CreateCmsContextService()); }
//    }
//    #endregion

//    protected override void OnInit(EventArgs e)
//    {
//        uxSharePointTree.TemplatePath = ICmsContextService.WorkareaPath + "/DxH/Controls/SharePointTree/SharePointTreeTemplate.ascx";
//        base.OnInit(e);
//    }

//    #region displayTree
//    public void LoadSelectContentDialog(string ConnectionName)
//    {
//        //uxSharePointTree.SelectionEvent += OnSelectionHandler;
//        //uxSharePointTree.DataSource = GetSharepointData_TwoLevels(ConnectionName, "SharePoint");
//        //uxSharePointTree.DataBind();
//    }
//    #endregion

//    #region folderTreeBindingCode

//    public void OnSelectionHandler(Object sender, TreeSelectionEventArgs argument)
//    {
//        if (SelectionEvent != null)
//        {
//            SelectionEvent(this, argument);
//        }
//    }

//    protected Ektron.Cms.Framework.UI.Controls.EktronUI.ITreeNodeCollection GetSharepointData_TwoLevels(string connectionName, string adapterName)
//    {
//        var list = new Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNodeCollection();
//        var rootId = connectionName + "_" + adapterName;

//        ContextBusClient cb = new ContextBusClient();
//        List<ConnectionParam> spParams = cb.LoadConnection(connectionName, adapterName);
//        cb.Login(spParams, adapterName);
//        List<FlyweightObjectDefinition> metaObjectList = cb.GetObjectDefinitionNameList(adapterName);
//        List<ObjectDefinition> objectdefinitionlist = cb.GetObjectDefinitionList(metaObjectList, adapterName);

//        foreach (ObjectDefinition objDef in objectdefinitionlist)
//        {
//            List<ObjectInstance> typedList = cb.GetObjectInstanceList(objDef, null, adapterName);
//            if (typedList != null && typedList.Count > 0)
//            {
//                var folderItem = MakeNode(objDef.Id, objDef.DisplayName, 0, "", rootId);
//                list.Add(folderItem);
//                foreach (ObjectInstance obj in typedList)
//                {
//                    string id = "";
//                    string name = "";
//                    foreach (Field fld in obj.Fields)
//                    {
//                        if (fld.DisplayName == "ID")
//                        {
//                            id = fld.Value.ToString() as string;
//                        }
//                        if (fld.DisplayName == "Name")
//                        {
//                            name = fld.Value.ToString() as string;
//                        }
//                        if (fld.DisplayName == "Title" && string.IsNullOrEmpty(name))
//                        {
//                            if (fld.Value != null)
//                            {
//                                name = fld.Value.ToString();
//                            }
//                            if (string.IsNullOrEmpty(name))
//                            {
//                                name = "(no title)";
//                            }
//                        }
//                    }
//                    var item = MakeNode(id, name, 1, objDef.Id, rootId);
//                    if (folderItem.Items == null)
//                    {
//                        folderItem.Items = new Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNodeCollection();
//                    }
//                    folderItem.Items.Add(item);
//                    folderItem.HasChildren = true;
//                    folderItem.Type = "folder";
//                }
//            }
//        }
//        return list;
//    }

//    protected Ektron.Cms.Framework.UI.Controls.EktronUI.TreeItem MakeNode(string id, string name, int depth, string parentId, string rootId)
//    {
//        var item = new Ektron.Cms.Framework.UI.Controls.EktronUI.TreeItem();
//        item.Id = parentId + "$" + id;
//        item.Text = name;
//        item.ParentId = parentId;
//        item.RootId = rootId;
//        item.Type = "item";
//        item.SubType = "";
//        item.Depth = depth;
//        return item;
//    }
//    #endregion
//}