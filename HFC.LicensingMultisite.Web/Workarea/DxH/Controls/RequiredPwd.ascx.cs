﻿namespace Workarea.DxH.Controls
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class RequiredPwd : System.Web.UI.UserControl
    {
        public short TabIndex
        {
            get
            {
                return TFPwd.TabIndex;
            }
            set
            {
                TFPwd.TabIndex = value;
            }
        }

        public string Value
        {
            get
            {
                return this.TFPwd.Value;
            }
            set
            {
                this.TFPwd.Text = value;
            }
        }
        public string CssClass
        {
            get
            {
                return TFPwd.CssClass;
            }
            set
            {
                TFPwd.CssClass = value;
            }
        }
    }
}