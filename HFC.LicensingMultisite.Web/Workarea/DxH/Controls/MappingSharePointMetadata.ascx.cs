﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Content;
using Ektron.Cms.Framework.Content;
using Ektron.Cms.Framework.UI.Controls.EktronUI;

public partial class User_Controls_MappingSharePointMetadata : System.Web.UI.UserControl
{
    private bool IsSelected = false;
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.LoadTabControls();
    }

    protected void LoadTabControls()
    {

        foreach (var item in this.GetSelectedList())
        {
            UserControl uc = (UserControl)Page.LoadControl("Workarea/DxH/Controls/MappingGridView.ascx");

            var tab = new Tab();
            tab.Text = item;
            tab.AddControl(uc);
            
            tab.IsSelected = true;
            uxMappingAccordian.AddTab(tab);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    // Testing data
    private List<string> GetSelectedList()
    {
        var selectedList = new List<string>();
        selectedList.Add("List item 1");
        selectedList.Add("List item 2");
        selectedList.Add("List item 3");
        selectedList.Add("List item 4");
        selectedList.Add("List item 5");
        selectedList.Add("List item 6");
        return selectedList;
    }
}


