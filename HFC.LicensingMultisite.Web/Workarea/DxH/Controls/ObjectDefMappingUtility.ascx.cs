﻿namespace Ektron.DxH.Client.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.DxH.Common.Objects;
    using Ektron.Cms.Instrumentation;
    using Ektron.Cms.Framework.UI;
    using System.Web.UI.HtmlControls;
    using Ektron.DxH.Tasks;
    using System.Web.Script.Serialization;
    using System.ComponentModel;
    using System.Runtime.Serialization;
    using Ektron.Newtonsoft.Json;
    using Ektron.Cms.Framework.Content;
    using Ektron.Cms.Content;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Framework.Context;
    using Ektron.Cms.Framework.Organization;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Localization;

    public enum ObjectStoreLocation
    {
        UseViewState,
        UseHiddenField,
        UseSession
    }

    public class ObjectDefMappingUtilityCreateMapEventArgs : EventArgs
    {
        public MappingTask Map { get; set; }
        public List<string> NotAssignedFieldDefinitionIds { get; set; }
    }

    public class ObjectDefMappingUtilityErrorArgs : EventArgs
    {
        public Exception ex { get; set; }
    }

    public class ObjectDefMappingUtilityWarningArgs : EventArgs
    {
        public WarningException ex { get; set; }
    }


    public partial class ObjectDefMappingUtility : System.Web.UI.UserControl
    {
        #region private members
        private const string CLASS_NAME = "Ektron.DxH.Client.Objects.ObjectDefMappingUtility";
        private const string BeginLogMsg = "-Begin {0}.{1}";
        private const string FinishLogMsg = "+Finish {0}.{1}";
        private const string ErrorLogMsg = "{0}.{1} failed: {2}";

        private EkMessageHelper ekMessageHelper = new EkMessageHelper(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
        private LocaleManager localManager = new LocaleManager(Cms.Framework.ApiAccessMode.Admin);

        private FolderData _FolderData = null;
        private Ektron.Cms.FolderData FolderData
        {
            get
            {

                if (this._FolderData == null && this.FolderId != null)
                {
                    this._FolderData = new FolderManager().GetItem((long)this.FolderId);
                }
                return this._FolderData;

            }

        }

        protected string SourceFieldHeaderText
        {
            get
            {
                return ViewState["SourceFieldHeaderText"] as string;
            }
            set
            {
                ViewState["SourceFieldHeaderText"] = value;
            }
        }


        protected string TargetFieldHeaderText
        {
            get
            {
                return ViewState["TargetFieldHeaderText"] as string;
            }
            set
            {

                ViewState["TargetFieldHeaderText"] = value;
            }
        }



        #endregion

        #region public props

        public ObjectDefinition SourceDefinition
        {
            get
            {
                if (string.IsNullOrEmpty(TargetStoreName))
                {
                    throw new NullReferenceException("TargetStoreName has not been set.");
                }
                return GetValue<ObjectDefinition>(SourceStoreName);

            }
            set
            {
                if (string.IsNullOrEmpty(TargetStoreName))
                {
                    throw new NullReferenceException("TargetStoreName has not been set.");
                }
                SetValue<ObjectDefinition>(value, SourceStoreName);
            }
        }

        public ObjectDefinition TargetDefinition
        {
            get
            {
                if (string.IsNullOrEmpty(TargetStoreName))
                {
                    throw new NullReferenceException("TargetStoreName has not been set.");
                }
                return GetValue<ObjectDefinition>(TargetStoreName);

            }
            set
            {
                if (string.IsNullOrEmpty(TargetStoreName))
                {

                    throw new NullReferenceException("TargetStoreName has not been set.");
                }
                SetValue<ObjectDefinition>(value, TargetStoreName);
            }
        }

        public long? FolderId
        {
            get
            {
                return ViewState["MappingUtilityFolderId"] as long?;


            }
            set
            {
                ViewState["MappingUtilityFolderId"] = value;
            }
        }



        /// <summary>
        /// This property will allow for the control to know where to look for the Source/Target Object Definitions.
        /// Using UseHiddenField, the hidden field must contain a ObjectDefintion that has been serialized to json.
        /// </summary>
        public ObjectStoreLocation DataStoreLocations { get; set; }

        /// <summary>
        /// The name used to look for the target store based on the DataStoreLocations enum.
        /// </summary>
        /// <example>Using viewstate, the control will look for ViewState[TargetStoreName] for the object.</example>
        public string TargetStoreName
        {
            get
            {

                return (string)ViewState["TargetStoreNameValue"];

            }
            set { ViewState["TargetStoreNameValue"] = value; }
        }

        /// <summary>
        /// The name used to look for the target store based on the DataStoreLocations enum.
        /// </summary>
        /// <example>Using viewstate, the control will look for ViewState[SourceStoreName] for the object.</example>
        public string SourceStoreName
        {
            get
            {
                if (string.IsNullOrEmpty((string)ViewState["SourceStoreNameValue"]))
                {
                    throw new NullReferenceException("SourceStoreName has not been set.");
                }
                return (string)ViewState["SourceStoreNameValue"];

            }
            set { ViewState["SourceStoreNameValue"] = value; }
        }

        /// <summary>
        /// The name the control will use when saving the DxH MappingTask object when saving to the datastore location.
        /// </summary>
        public string MappingStoreName
        {
            get
            {
                if (string.IsNullOrEmpty((string)ViewState["MappingStoreNameValue"]))
                {
                    throw new NullReferenceException("MappingStoreName has not been set.");
                }
                return (string)ViewState["MappingStoreNameValue"];

            }
            set { ViewState["MappingStoreNameValue"] = value; }
        }

        #endregion

        #region ctors

        public ObjectDefMappingUtility()
        {

        }


        #endregion

        #region Events
        protected override void OnInit(EventArgs e)
        {




            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            if (this.ekGrid == null)
                this.ekGrid = new Cms.Framework.UI.Controls.EktronUI.GridView();
            this.ekGrid.EktronUIPagingInfo = new Cms.PagingInfo();
            RegisterResources();
            base.OnLoad(e);
        }



        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);

        }



        protected override void OnPreRender(EventArgs e)
        {

            if (!string.IsNullOrEmpty(this.TargetStoreName) && this.SourceDefinition != null)
            {
                this.SourceFieldHeaderText = string.Format(GetLocalResourceObject("TargetFieldHeaderTextSharepoint").ToString(), this.SourceDefinition.DisplayName);
            }
            if (!string.IsNullOrEmpty(this.TargetStoreName) && this.FolderData != null)
            {
                this.TargetFieldHeaderText = string.Format(GetLocalResourceObject("TargetFieldHeaderTextEktron").ToString(), this.FolderData.Name);
            }
            this.DataBindGridViewModel();
            base.OnPreRender(e);
        }

        #endregion

        #region public methods/events

        public event EventHandler<ObjectDefMappingUtilityCreateMapEventArgs> OnAfterMapCreated;
        public event EventHandler<ObjectDefMappingUtilityErrorArgs> OnError;
        public event EventHandler<ObjectDefMappingUtilityWarningArgs> OnWarning;

        /// <summary>
        /// Call this method to create the DxH MappingTask and save it to the appropriate DataStoreLocation (ViewState, Session, HiddenField)
        /// If UseHiddenField is selected, the hidden field will contain a MappingTask converted to json.
        /// </summary>
        public void CreateMapping()
        {
            Log.WriteVerbose(string.Format(BeginLogMsg, CLASS_NAME, "CreateMapping  Void"));
            try
            {
                MappingTask map = CreateMap();

                SetValue<MappingTask>(map, MappingStoreName);

            }
            catch (Exception ex)
            {
                string msg = string.Format(ErrorLogMsg, CLASS_NAME, "CreateMapping", ex.Message);
                Log.WriteError(ex);
                throw;
            }
            Log.WriteVerbose(string.Format(FinishLogMsg, CLASS_NAME, "CreateMapping Void"));
        }

        public void DataBindGridViewModel()
        {
            try
            {

                if (this.SourceDefinition == null)
                    throw new ArgumentNullException("SourceDefinition");

                if (this.TargetDefinition == null)
                    throw new ArgumentNullException("TargetDefinition");

                if (this.FolderId == null)
                    throw new ArgumentNullException("FolderId");

                Ektron.Cms.Framework.Organization.FolderManager folderManager = new Cms.Framework.Organization.FolderManager();
                int languageid = 0;
                int.TryParse((Page.FindControl("LanguageID") as HiddenField).Value, out languageid);
                if (languageid > 0)
                    folderManager.RequestInformation.ContentLanguage = languageid;
                Ektron.Cms.FolderData folder = folderManager.GetItem((long)this.FolderId, true);

                if (folder.FolderMetadata == null && !folder.IsMetaInherited)
                {
                    var languageName = localManager.GetItem(languageid).NativeName;
                    var msg = string.Format(ekMessageHelper.GetMessage("lbl dxh no folder metadata"), languageName, folder.Name);
                    throw new WarningException(msg);
                }

                if (folder.FolderMetadata == null)
                    folder = folderManager.GetItem(folder.MetaInheritedFrom, true);

                if (folder.FolderMetadata == null)
                {
                    var languageName = localManager.GetItem(languageid).NativeName;
                    var msg = string.Format(ekMessageHelper.GetMessage("lbl dxh no folder metadata"), languageName, folder.Name);
                    throw new WarningException(msg);
                }
                if (!this.SourceDefinition.Fields.Any(field => field.DisplayName.ToLower().Trim() == "not mapped"))
                    this.SourceDefinition.Fields.Insert(0, (new FieldDefinition("Not_Mapped", "Not Mapped", new FieldType(typeof(string)), false, false)));


                if (folder.FolderMetadata != null)
                {

                    var ContentModel =
                        TargetDefinition.Fields
                        .Where(field =>
                            field.Id.ToLower().Contains("metadata") && folder.FolderMetadata.Any(meta => meta.Id.ToString().ToLower().Trim() == field.Id.Split('|').Last().ToLower().Trim()))
                        .Select(field => new
                        {
                            Id = this.SourceStoreName.GetHashCode(),
                            Source = field,
                            SourceID = field.Id.GetHashCode(),
                            ObjectDefinitionId = this.SourceDefinition.Id,
                            IsRequired = folder.FolderMetadata.First(req => req.Id.ToString() == field.Id.Split('|').Last()).Required,
                            Target = SourceDefinition.Fields.Where(f => CanBeConverted(Type.GetType(f.DataType.Type), Type.GetType(field.DataType.Type)) || f.Id == "Not_Mapped")
                        }
                               )
                        .ToList();

                    this.ekGrid.EktronUIPagingInfo = new Cms.PagingInfo();
                    this.ekGrid.EktronUIPagingInfo.CurrentPage = 1;
                    this.ekGrid.EktronUIPagingInfo.RecordsPerPage = 100;
                    this.ekGrid.EktronUIPagingInfo.TotalRecords = ContentModel.Count;

                    this.ekGrid.DataSource = ContentModel;
                    this.ekGrid.DataBind();
                    this.IsDataBound = true;
                }
                else
                {
                    var ContentModel =
                           TargetDefinition.Fields
                           .Where(field =>
                               field.Id.ToLower().Contains("metadata"))
                           .Select(field => new
                           {
                               Id = this.SourceStoreName.GetHashCode(),
                               Source = field,
                               SourceID = field.Id.GetHashCode(),
                               ObjectDefinitionId = this.SourceDefinition.Id,
                               Target = SourceDefinition.Fields.Where(f => CanBeConverted(Type.GetType(f.DataType.Type), Type.GetType(field.DataType.Type)) || f.Id == "Not_Mapped")
                           }
                                    )
                            .ToList();

                    this.ekGrid.EktronUIPagingInfo = new Cms.PagingInfo();
                    this.ekGrid.EktronUIPagingInfo.CurrentPage = 1;
                    this.ekGrid.EktronUIPagingInfo.RecordsPerPage = 100;
                    this.ekGrid.EktronUIPagingInfo.TotalRecords = ContentModel.Count;
                    this.ekGrid.DataSource = ContentModel;
                    this.ekGrid.DataBind();
                    this.IsDataBound = true;
                }

                SourceDefinition.Fields.RemoveAll(match => match.Id.ToLowerInvariant() == "not_mapped");
                TargetDefinition.Fields.RemoveAll(match => match.Id.ToLowerInvariant() == "not_mapped");
            }
            catch (WarningException ex)
            {
                this.RaiseOnWarning(new ObjectDefMappingUtilityWarningArgs() { ex = ex });
            }
            catch (Exception ex)
            {
                this.RaiseOnError(new ObjectDefMappingUtilityErrorArgs() { ex = ex });
            }

        }

        public MappingTask CreateMap()
        {
            Log.WriteVerbose(string.Format(BeginLogMsg, CLASS_NAME, "CreateMap  return:MappingTask"));
            try
            {
                TaskManagerClient taskClient = new TaskManagerClient();

                //check for the added field of not mapped and remove it
                SourceDefinition.Fields.RemoveAll(match => match.Id.ToLowerInvariant() == "not_mapped");
                TargetDefinition.Fields.RemoveAll(match => match.Id.ToLowerInvariant() == "not_mapped");

                MappingTask map = taskClient.CreateMappingTask(SourceDefinition,
                    TargetDefinition);


                List<string> NotAssignedFieldDefinitionIds = new List<string>();
                foreach (MappableField targetField in map.TargetFields)
                {

                    try
                    {
                        targetField.Mapping = new MappedValue();
                        targetField.Mapping.MappingType = MappingType.FieldMapping;

                        //this is where we call to the grid to find out what needs to be mapped.
                        GridViewDirtyField dirtiedField = null;
                        foreach (var dirtyfield in ekGrid.EktronUIColumns["SelectList"].DirtyFields)
                        {

                            if (dirtyfield.ValueFields.Any(vf => vf.Value.ToLower().Trim() == targetField.Id.ToLower().Trim()) &&
                                dirtyfield.ValueFields.Any(v => v.Value.ToLower().Trim() == map.SourceObject.Id.ToLower().Trim()))
                            {
                                dirtiedField = dirtyfield;
                            }
                        }

                        if (dirtiedField != null)
                        {
                            targetField.Mapping.Value = dirtiedField.DirtyState;
                        }
                        else if (map.SourceObject.Fields.FirstOrDefault(f => f.Id == targetField.Id) != null)
                        {
                            targetField.Mapping.Value = map.SourceObject.Fields.Single(f => f.Id == targetField.Id).Id;
                        }

                        if (targetField.Mapping.Value == null)
                        {
                            targetField.Mapping.MappingType = MappingType.NotAssigned;
                            NotAssignedFieldDefinitionIds.Add(targetField.Id);
                        }
                    }
                    catch (Exception ex)
                    {
                        string msg = string.Format(@"{0}.CreateMapping return:MappingTask, failed for MappableField {1} with the error:{2}", CLASS_NAME, targetField.Id, ex.Message);

                        Log.WriteError(msg);
                        throw;
                    }
                }

                RaiseOnAfterMapCreated(new ObjectDefMappingUtilityCreateMapEventArgs() { Map = map, NotAssignedFieldDefinitionIds = NotAssignedFieldDefinitionIds });

                return map;
            }
            catch (Exception ex)
            {
                RaiseOnError(new ObjectDefMappingUtilityErrorArgs() { ex = ex });

            }
            Log.WriteVerbose(string.Format(FinishLogMsg, CLASS_NAME, "CreateMap return:MappingTask"));
            return null;
        }

        #endregion


        #region private methods

        private bool CanBeConverted(Type SourceType, Type TargetType)
        {
            TypeConverter tc = System.ComponentModel.TypeDescriptor.GetConverter(SourceType);

            return (SourceType == TargetType || tc.CanConvertTo(TargetType));

        }

        private void SetValue<T>(T Object, string Name)
        {
            try
            {
                switch (DataStoreLocations)
                {
                    case ObjectStoreLocation.UseHiddenField:
                        var hiddenField = Page.FindControl(Name) as HiddenField;
                        if (hiddenField == null)
                        {
                            throw new NullReferenceException("Cannot find hidden field control with id: " + Name);
                        }
                        hiddenField.Value = JsonConvert.SerializeObject(Object);
                        break;
                    case ObjectStoreLocation.UseSession:
                        HttpContext.Current.Session[Name] = Object;
                        break;
                    case ObjectStoreLocation.UseViewState:
                        ViewState[Name] = Object;
                        break;
                    default:
                        throw new NullReferenceException("ObjectStoreLocation");

                }
            }
            catch (Exception ex)
            {
                string msg = string.Format(ErrorLogMsg, CLASS_NAME, "SetValue", ex.Message);
                Log.WriteError(msg);
                throw;
            }
        }

        private T GetValue<T>(string Name)
        {
            T rtnVal = default(T);
            try
            {
                switch (DataStoreLocations)
                {
                    case ObjectStoreLocation.UseHiddenField:
                        var hiddenField = Page.FindControl(Name) as HiddenField;
                        if (hiddenField != null)
                            rtnVal = JsonConvert.DeserializeObject<T>(hiddenField.Value);
                        break;
                    case ObjectStoreLocation.UseSession:
                        rtnVal = (T)HttpContext.Current.Session[Name];
                        break;
                    case ObjectStoreLocation.UseViewState:
                        rtnVal = (T)ViewState[Name];
                        break;
                    default:
                        throw new NullReferenceException("ObjectStoreLocation");

                }
            }
            catch (Exception ex)
            {
                string msg = string.Format(ErrorLogMsg, CLASS_NAME, "GetGetValue", ex.Message);
                Log.WriteError(msg);
                throw;
            }
            return rtnVal;
        }

        private bool isDataBound = false;
        private bool IsDataBound
        {
            get
            {
                return this.isDataBound;
            }
            set
            {
                this.isDataBound = value;
            }
        }

        public override void DataBind()
        {
            base.DataBind();
            if (!this.IsDataBound)
            {
                this.DataBindGridViewModel();
            }
        }


        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        private void RaiseOnAfterMapCreated(ObjectDefMappingUtilityCreateMapEventArgs e)
        {
            if (this.OnAfterMapCreated != null)
            {
                this.OnAfterMapCreated(this, e);
            }
        }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        private void RaiseOnError(ObjectDefMappingUtilityErrorArgs e)
        {
            if (this.OnError != null)
            {
                this.OnError(this, e);
            }
        }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        private void RaiseOnWarning(ObjectDefMappingUtilityWarningArgs e)
        {
            if (this.OnWarning != null)
            {
                this.OnWarning(this, e);
            }
        }
        private void RegisterResources()
        {
            Css.Register(this, CmsContextService.Current.WorkareaPath + "/DxH/Controls/CSS/ObjectDefMappingUtility.css");
        }

        #endregion

    }
}