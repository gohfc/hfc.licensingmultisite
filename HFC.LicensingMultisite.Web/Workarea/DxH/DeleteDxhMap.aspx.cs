﻿namespace Workarea.DxH
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.UI;
    using Ektron.DxH.Client;
    using System.Text;

    public partial class DeleteDxhMap : Ektron.Cms.Workarea.Page
    {
        protected EkMessageHelper m_refMsg;
        protected Ektron.Cms.ContentAPI m_refContentApi = new Ektron.Cms.ContentAPI();
        private ContextBusClient contextbus;
        private Ektron.Cms.Interfaces.Context.ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
        private Ektron.Cms.Settings.DxH.DxHMappingData map;
        long formId = 0;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            contextbus = new ContextBusClient();

            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Package contextbusControlPackage = new Package()
            {
                Components = new List<Component>()
                {
                    Packages.Ektron.CssFrameworkBase,
                    Css.Create(cmsContextService.WorkareaPath + "/dxh/css/ektron.workarea.dxh.css"),
                    Packages.Ektron.JSON
                }
            };
            contextbusControlPackage.Register(this);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.ValidateUserLogin();

            if (!(Request.QueryString["formid"] == null))
            {
                formId = Convert.ToInt64(Request.QueryString["formid"]);
            }

            string action = "deletemap";
            if (!(Request.QueryString["action"] == null))
            {
                action = Request.QueryString["action"].ToLower();
            }

            m_refMsg = m_refContentApi.EkMsgRef;
            map = DxHUtils.GetMapping(EkEnumeration.CMSObjectTypes.Content, formId, m_refContentApi.RequestInformationRef.ContentLanguage);
            if (!Page.IsPostBack && map != null && map.Adapter != null)
            {
                string mapName = Server.HtmlEncode(map.Title); 
                Ektron.Cms.FormData form_data = m_refContentApi.GetFormById(formId);
                string formTitle = Server.HtmlEncode(form_data.Title);

                switch (action)
                {
                    case "deleteform":
                        PageHeader.Text = String.Format(GetLocalResourceObject("DeleteDxHFormHeader").ToString(), formTitle);
                        //DxhDescription.Text = String.Format(GetLocalResourceObject("DeleteDxhFormMsg1").ToString(), formTitle, mapName, map.Adapter, map.TargetObjectDefinitionId); 
                        DxhDescription.Text = String.Format(GetLocalResourceObject("DeleteDxhFormMsg1").ToString(), formTitle, mapName); 
                        //lblDeleteMsg2.Text = String.Format(GetLocalResourceObject("DeleteDxhFormMsg2").ToString(), formTitle);
                        lblDeleteMsg1.Visible = false;
                        break;
                    case "deletemap":
                    default:
                        PageHeader.Text = String.Format(GetLocalResourceObject("DeleteDxHMappingHeader").ToString(), mapName);
                        DxhDescription.Text = String.Format(GetLocalResourceObject("DeleteDxhMappingMsg1").ToString(), formTitle, map.Adapter, map.TargetObjectDefinitionId);
                        lblDeleteMsg1.Text = String.Format(GetLocalResourceObject("DeleteDxhMappingMsg2").ToString(), formTitle, map.Adapter);
                        //lblDeleteMsg2.Text = String.Format(GetLocalResourceObject("DeleteDxhMappingMsg3").ToString(), mapName);
                        break;
                }
            }
            pnlExceptions.Visible=errMsg.Visible = false;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool bSuccess = false;
            pnlExceptions.Visible = errMsg.Visible = false;
            try
            {
                DxHUtils.DeleteMapping(map.Id);
                bSuccess = true;
            }
            catch (Exception ex)  
            {
                // display error message
                errMsg.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                errMsg.Text = GetLocalResourceObject("DeleteError").ToString();
                ltrExcText.Text = ex.Message;
                pnlExceptions.Visible = errMsg.Visible = true;
                btnDlgDelete.Enabled = false;
            }

            if (bSuccess)
            {
                if (string.IsNullOrEmpty(Request.QueryString["trigger"]))
                {
                    StringBuilder script = new StringBuilder();
                    script.Append("<script type=\"text/javascript\">").AppendLine();
                    script.Append("setTimeout(\'CompleteDeleteAction(\"delete\");\',1000);").AppendLine();
                    script.Append("</script>").AppendLine();
                    litScript.Text = script.ToString();
                }
                else
                {//DMSMenu
                    StringBuilder script = new StringBuilder();
                    script.Append("<script type=\"text/javascript\">").AppendLine();
                    script.Append("setTimeout(\'CompleteDeleteAction(\"delete\"," + m_refContentApi.RequestInformationRef.ContentLanguage.ToString() + "," + formId.ToString() + ");\',1000);").AppendLine();
                    script.Append("</script>").AppendLine();
                    litScript.Text = script.ToString();
                }
            }
        }
    }
}