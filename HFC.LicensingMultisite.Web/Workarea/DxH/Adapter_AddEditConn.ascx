﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Adapter_AddEditConn.ascx.cs"
    Inherits="Workarea.DxH.Adapter_AddEditConn" %>
<asp:ScriptManager runat="server" ID="ScriptManager1">
</asp:ScriptManager>
<ektronUI:CssBlock ID="uxCloudDeploymentScheduleStyles" runat="server">
    <CssTemplate>
        .editTDLeft { text-align: right; padding: 5px; } .editTDRight { padding: 5px; }
        .editCtrls input{width:300px!important;}
        .ektron-ui-messageBody{word-wrap:break-word;}
    </CssTemplate>
</ektronUI:CssBlock>
<ektronUI:JavaScriptBlock ID="uxOpenDialogScript" runat="server" ExecutionMode="OnParse">
    <ScriptTemplate>
        function openDialog(){ $ektron("<%= diagAddEditConn.Selector%>").dialog("open");
        } function closeDialog(){ $ektron("<%= diagAddEditConn.Selector%>").dialog("close");
        }
    </ScriptTemplate>
</ektronUI:JavaScriptBlock>

<ektronUI:JavaScriptBlock ID="submitFormHandler" runat="server" ExecutionMode="OnEktronReady">
    <ScriptTemplate>
        (function(){
                $ektron('.diagAddEditConn input:not([type="checkbox"])').keypress(function (e) {
                  if (e.which == 13) {
                    <%= GetSavePostBack() %>;
                    e.preventDefault();
                    return false;
                  }
                });
        }());
    </ScriptTemplate>
</ektronUI:JavaScriptBlock>


<ektronUI:Dialog runat="server" ID="diagAddEditConn" CssClass="diagAddEditConn" AutoOpen="true" Width="640" Height="600"
    Resizable="True" Modal="true">
    <ContentTemplate>
        <asp:UpdatePanel runat="server" ID="uPnlTestBtn">
            <ContentTemplate>
             <div style=" padding-right: 15px;">
                        <table style="width:100%;">
                            <tr>
                                <td style="text-align: left; vertical-align: middle; width: 64px;">
                                    <img src="images/dxh_logo_60x60.png" alt="Digital Experience Hub" />
                                </td>
                                <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em;
                                    padding-left: 5px;">
                                    <asp:Literal ID="ltrPageHeadign" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>

                <asp:Panel runat="server" ID="pnlEditArea">
                   
                    <div id="instrArea" style="margin-bottom:10px;">
                        <asp:Literal runat="server" ID="ltrDescText" />
                    </div>
                    <div style="font-weight: bold; font-size:x-small;margin-bottom:10px;">
                        <asp:Label runat="server" ID="lblRequired" Text="<% $Resources: lblRequired %>"></asp:Label> <span class="ektron-ui-required">*</span>
                    </div>
                    <asp:UpdateProgress runat="server" ID="UProgress">
                        <ProgressTemplate>
                            <ektronUI:Message runat="server" ID="msgProgress" DisplayMode="Working" Text="<%$Resources:msgProgress%>" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <ektronUI:Message runat="server" ID="msgBox" EnableViewState="false" Visible="false" />
                    
                    <div style="margin-bottom:10px;">
                        <asp:Literal runat="server" ID="ltrConnName" />
                    </div>
                    <asp:Panel runat="server" ID="pnlConnNameArea">
                        <div style="padding-left:15px; margin-bottom:10px;">
                            <span style="font-weight:bold;"><asp:Literal runat="server" ID="ltrTBConnName" Text="<%$Resources:tbConnName %>" /></span>
                        
                            <span class="ektron-ui-required">*</span>
                            <ektronUI:TextField runat="server" ID="tfConnName" CssClass="editCtrls">
                                <ValidationRules>
                                    <ektronUI:RequiredRule />
                                    <ektronUI:CustomRule JavascriptFunctionName="NOTagValidation" />
                                </ValidationRules>
                            </ektronUI:TextField>
                        </div>
                        <asp:Literal runat="server" ID="ltrConnNameReadOnly" />
                        <br />
                    </asp:Panel>
                    
                    
                    <div id="divInstruction" style=" margin-bottom:10px; margin-top:10px;">
                        <asp:Literal runat="server" ID="ltrInstruction" />
                    </div>
                    <asp:Panel runat="server" ID="pnlControlHolder" style="padding-left:25px;">
                    </asp:Panel>
                    <div style="text-align: right; margin-bottom:10px;">
                        <ektronUI:Button runat="server" ID="btnTestRetry" Text="<%$Resources:btnTestRetry %>"
                            OnClick="btnTestRetry_Click" Visible="false" ValidationGroup="NoGroup"></ektronUI:Button>
                        <ektronUI:Button runat="server" ID="btnTest" Text="<%$Resources:btnTestConn %>" OnClick="btnTest_Click"></ektronUI:Button>
                    </div>
                    
                    
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlConfirmArea" Visible="false" CssClass="ektronTopSpace">
                    <ektronUI:Message runat="server" ID="msgConfirm">
                    </ektronUI:Message>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlExceptions" Visible="false">
                    <ektronUI:Message runat="server" ID="msgException">
                        <ContentTemplate>
                            <asp:Literal runat="server" ID="ltrException" Text="<%$Resources:strExceptionText %>"/>
                        </ContentTemplate>
                    </ektronUI:Message>
                    <a runat="server" id="ancShowDetail" href="#" class="details" title='<%$Resources:strExceptionShowDetail %>'> <asp:Literal runat="server" ID="ltrExMore" Text="<%$Resources:strExceptionShowDetail %>"/></a>
                    <a runat="server" id="ancHideDetail" href="#" class="details ektron-ui-hidden" title='<%$Resources:strExceptionHideDetail %>'> <asp:Literal runat="server" ID="Literal1" Text="<%$Resources:strExceptionHideDetail %>"/></a>
                            <p class="details ektron-ui-hidden">
                                <asp:Literal runat="server" ID="ltrExcText" />        
                            </p>
                            <ektronUI:JavaScriptControlGroup ID="uxControlGroup" AssociatedTriggerIDs="<%# ancShowDetail.ClientID+','+ ancHideDetail.ClientID %>"
                                AssociatedTargetCssClasses="details"
                                AssociatedEvents="click" runat="server">
                                <Modules>
                                    <ektronUI:ToggleModule ID="ToggleModule1" runat="server" />
                                </Modules>
                            </ektronUI:JavaScriptControlGroup>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlButtonArea" Style="margin-top: 30px; text-align: right; margin-bottom:15px;">
                    <ektronUI:Button runat="server" ID="btnSaveConfirm" Text="<%$Resources:btnSave %>"
                        OnClick="btnSaveConfirm_Click" Visible="false" Style="margin-right: 20px;"  />
                    <ektronUI:Button runat="server" ID="btnSave" Text="<%$Resources:btnSave %>" OnClick="btnSave_Click" OnClientClick="$ektron('.diagAddEditConn').css('height', '200px');"
                        Style="margin-right: 20px;" />
                    <ektronUI:Button runat="server" ID="btnCancel" Text="<%$Resources:btnCancel %>" OnClientClick="closeDialog();window.location='ViewDXHConnections.aspx'; return false;" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </ContentTemplate>
</ektronUI:Dialog>
<script type="text/javascript">
    function NOTagValidation(Fieldval, FieldName) {
        var re = /<(.|\n)*?>/g;
        if (re.test(Fieldval)) {
            return false;
        }
        else
            return true;

    }
    </script>