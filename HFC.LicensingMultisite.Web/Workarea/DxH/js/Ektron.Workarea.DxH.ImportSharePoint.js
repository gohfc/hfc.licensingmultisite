﻿function DisplayScheduleAlert() {
    if ($ektron('#uxAddSPContent a').is(".ui-state-disabled")) { return false; }
    else {
        $ektron(".uxNotificationDialog").dialog('open');
        $ektron(".uxNotificationDialog").css("z-index", "999999"); 
        $ektron("#xhr-message, .ui-widget-overlay").css("z-index", "0");
    }
}
function DisplayRotateImage() {
    $ektron(".ui-widget-overlay").css("z-index", "99999");
    $ektron("#xhr-message").css("z-index", "0");
}

function dxhOverlay(show) {
    show = ("undefined" == typeof (show)) ? true : show;
    var overlay = $ektron(".ui-widget-overlay");    
    if (show) {
        overlay.parent().show();
    }
    else {
        overlay.parent().hide();
    }
}
