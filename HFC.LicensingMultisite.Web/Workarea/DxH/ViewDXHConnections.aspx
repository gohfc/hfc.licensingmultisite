﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewDXHConnections.aspx.cs" Inherits="Workarea.DxH.ViewDXHConnections" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body onclick="MenuUtil.hide()">
    <form id="form1" runat="server">
    <ektronUI:JavaScriptBlock ID="uxOpenDialogScript" runat="server" ExecutionMode="OnParse">
        <ScriptTemplate>
            function openDelConfirmDialog(){ $ektron("<%= diagDelConfirm.Selector%>").dialog("open");
            } function closeDelConfirmDialog(){ $ektron("<%= diagDelConfirm.Selector%>").dialog("close");
            }
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
    <ektronUI:CssBlock runat="server" ID="uxCSsBlock">
        <CssTemplate>
            .noBorder { border:none !important;} 
            .errorMessageTR { background: #fbe3e4; border: 2px solid #fbc2c4; color: #8a1f11; margin-bottom: 1.5em; padding: 0.5em;}
            .menuRootItemSelected a {color:#FFFFFF; text;text-decoration:none;}
            .ektron-ui-gridview .JColResizer tbody td {overflow:visible !important;}
                   .dynamic a
       {
            padding-left:15px;
            padding-right:auto;
            padding-top:5px;
            padding-bottom:3px;
       }
        
        a.popout
        {
            padding-right:20px!important;
        }
        a.mnuItem:hover
        {
          background:url(../../WorkArea/images/UI/menu/arrow_over.png);
          color:#FFF;
        }
        .level2
        {
            background:url(../../WorkArea/images/UI/menu/dropdown-background.gif) #ededed repeat-x;
            width:100px;
             border:#bfbfbf 1px solid;
        }
        </CssTemplate>
    </ektronUI:CssBlock>
    <script type="text/javascript">
        function DeleteConfirm(connName, adapterName, connId) {
            $(".hidConnName").html(connName);
            $(".hidAdpName").html(adapterName);
            $("#ekHid_hidConnId").val(connId);
            $("#ekHid_hidConnName").val(connName);
            $("#ekHid_hidAdpName").val(adapterName);
            $ektron("#diagDelConfirm_Dialog_uxDialogControl")[0].title = "Test";
            openDelConfirmDialog();
        }
        //function PopUpWindow(url, hWind, nWidth, nHeight, nScroll, nResize) {
        //    var cToolBar = "toolbar=0,location=0,directories=0,status=" + nResize + ",menubar=0,scrollbars=" + nScroll + ",resizable=" + nResize + ",width=" + nWidth + ",height=" + nHeight;
        //    var popupwin = window.open(url, hWind, cToolBar);
        //    return popupwin;
        //}
    </script>
    <input type="hidden" id="ekHid_hidConnId" name="ekHid_hidConnId" />
    <input type="hidden" id="ekHid_hidConnName" name="ekHid_hidConnName" />
    <input type="hidden" id="ekHid_hidAdpName" name="ekHid_hidAdpName" />
    <div class="ui-widget-overlay" style="z-index: 1001; top: 205px; height: 850px;"
        runat="server" id="divNoDxHMask" visible="false">
    </div>
    <div>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="ltrPageTitle" runat="server" Text="<%$Resources: PageTitle%>" /></span>
            </div>
            <div class="ektronToolbar" id="htmToolBar" runat="server">
                <asp:Literal runat="server" ID="ltrMenuHtml" />
            </div>
        </div>
        <div class="ektronPageContainer">
            <div style="border: #d4d4d4 1px solid; margin-left: 5px; margin-right: 5px; margin-top: 5px;">
                <table class="ektronGrid" runat="server" id="divDxHURL">
                    <tr runat="server" id="trDXHConnection">
                        <td>
                            DXH Connection
                        </td>
                        <td>
                            URL: <a href="DxHSetup.aspx">
                                <asp:Literal runat="server" ID="ltrConnURL"></asp:Literal></a>
                        </td>
                    </tr>
                    <tr class="errorMessageTR" runat="server" id="trError">
                        <td>
                            <ektronUI:Message CssClass="noBorder" runat="server" ID="msgNoDxHConn" DisplayMode="Error">
                            </ektronUI:Message>
                        </td>
                        <td>
                            <ektronUI:Button runat="server" ID="uxRetry" Text="<%$Resources: uxRetry %>" OnClick="RetryClick_Click"></ektronUI:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="ektronPageContainer" style="padding: 10px; margin-left: 5px; margin-right: 5px;">
                <ektronUI:Message CssClass="noBorder" runat="server" ID="uxInformation" Text="Below are the Broken Connections: "
                    DisplayMode="Information">
                </ektronUI:Message>
                <ektronUI:GridView runat="server" ID="EkGrid" AutoGenerateColumns="false" EnableEktronUITheme="true"
                    OnEktronUISortChanged="EkGrid_EktronUIThemeSortChanged" OnEktronUIPageChanged="EkGrid_EktronUIThemePageChanged"
                    Width="99%">
                    <Columns>
                        <asp:TemplateField HeaderText="<%$Resources: strHeaderAdpName%>" ItemStyle-Width="25%"
                            SortExpression="ConnectionName" ControlStyle-CssClass="overflow-visible">
                            <ItemTemplate>
                                <asp:Menu runat="server" ID="itemCM" OnMenuItemClick="mnu_MenuItemClick" Orientation="Vertical"
                                    DisappearAfter="0">
                                    <DynamicMenuItemStyle CssClass="mnuItem" />
                                    <StaticMenuStyle CssClass="mnuTopItem" />
                                </asp:Menu>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$Resources: strHeaderAdpType%>" SortExpression="AdapterName">
                            <ItemTemplate>
                                <%# getAdapterDisplayName(Eval("AdapterName").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </ektronUI:GridView>
                <hr />
                <ektronUI:Button runat="server" ID="btnDelete" Text="Delete Selected Items" OnClientClick="openDelConfirmDialog();return false;"
                    Visible="false" />
            </div>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlCtrlHolder">
    </asp:Panel>
    <asp:Literal runat="server" ID="ltrList"></asp:Literal>
    <ektronUI:Dialog runat="server" ID="diagDelConfirm" AutoOpen="false" Title="<%$Resources: strDelConfirm_Dlg_Title%>"
        Width="640" Resizable="false" Modal="true">
        <ContentTemplate>
            <table>
                <tr>
                    <td style="text-align: left; vertical-align: middle; width: 17%;">
                        <img src="images/dxh_logo_60x60.png" alt="Digital Experience Hub" />
                    </td>
                    <td style="text-align: left; vertical-align: middle; font-weight: bold; font-size: 1.5em;
                        padding-left: 5px;">
                        <asp:Literal ID="ltrDeleteHeader_1" runat="server" Text="<%$Resources: strDelConfirm_header_1%>" />
                        <span class="hidAdpName"></span>
                        <asp:Literal ID="ltrDeleteHeader_2" runat="server" Text="<%$Resources: strDelConfirm_header_2%>" />
                        "<span class="hidConnName"></span>"?
                    </td>
                </tr>
            </table>
            <asp:Literal runat="server" ID="ltrDelConfirm_Desc" Text="<%$Resources: strDelConfirm_Desc%>" /><br />
            <br />
            <asp:Literal runat="server" ID="ltrDelConfirm_1" Text="<%$Resources: strDelConfirm_1%>" />
            <span class="hidAdpName"></span>
            <asp:Literal runat="server" ID="ltrDelConfirm_2" Text="<%$Resources: strDelConfirm_2%>" />
            "<span class="hidConnName" style="font-weight: bold"></span>"?
        </ContentTemplate>
        <Buttons>
            <ektronUI:DialogButton runat="server" ID="btnDelConfirm" OnClick="btnDelConfim_Click"
                ValidationGroup="NoValiGrp" Text="<%$Resources: btnDelete_confirm%>" CloseDialog="false" />
            <ektronUI:DialogButton runat="server" ID="DialogButton1" CloseDialog="true" Text="Cancel" />
        </Buttons>
    </ektronUI:Dialog>
    <ektronUI:Dialog runat="server" ID="diagException" AutoOpen="false" Width="640" Resizable="false"
        Title="<%$Resources:strExceptionTitle %>" Modal="true">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlExceptions">
                <ektronUI:Message runat="server" ID="msgException">
                    <ContentTemplate>
                        <asp:Literal runat="server" ID="ltrException" Text="<%$Resources:strExceptionText %>" />
                    </ContentTemplate>
                </ektronUI:Message>
                <a runat="server" id="ancShowDetail" href="#" class="details" title='<%$Resources:strExceptionShowDetail %>'>
                    <asp:Literal runat="server" ID="ltrExMore" Text="<%$Resources:strExceptionShowDetail %>" /></a>
                <a runat="server" id="ancHideDetail" href="#" class="details ektron-ui-hidden" title='<%$Resources:strExceptionHideDetail %>'>
                    <asp:Literal runat="server" ID="Literal1" Text="<%$Resources:strExceptionHideDetail %>" /></a>
                <p class="details ektron-ui-hidden">
                    <asp:Literal runat="server" ID="ltrExcText" />
                </p>
                <ektronUI:JavaScriptControlGroup ID="uxControlGroup" AssociatedTriggerIDs="<%# ancShowDetail.ClientID+','+ ancHideDetail.ClientID %>"
                    AssociatedTargetCssClasses="details" AssociatedEvents="click" runat="server">
                    <Modules>
                        <ektronUI:ToggleModule ID="ToggleModule1" runat="server" />
                    </Modules>
                </ektronUI:JavaScriptControlGroup>
            </asp:Panel>
        </ContentTemplate>
        <Buttons>
            <ektronUI:DialogButton runat="server" ID="btnCloseExceptonDiag" CloseDialog="true"
                Text="Close" />
        </Buttons>
    </ektronUI:Dialog>
    </form>
</body>
<%	if (m_blnRefreshFrame == true)
    {%>
<script type="text/javascript">
	<!--    //--><![CDATA[//><!--
    var frmNavBottom;
    frmNavBottom = window.parent.frames["ek_nav_bottom"];
    if (("object" == typeof (frmNavBottom)) && (frmNavBottom != null)) {
        frmNavBottom.ReloadTrees('smartdesktop');
        frmNavBottom.ReloadTrees('admin');
    }
    //--><!]]>



</script>
<%}%>
</html>
