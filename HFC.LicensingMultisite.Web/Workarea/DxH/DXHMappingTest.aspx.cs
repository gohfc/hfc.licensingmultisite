﻿namespace Workarea.DxH
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.Content;
    using Ektron.Cms;
    using Ektron.DxH.Client;
    using Ektron.Cms.Settings.DxH;
    using Ektron.DxH.Tasks;
    using Ektron.DxH.Common.Contracts;
    using System.Text;
    using Ektron.DxH.Common.Objects;
    using Ektron.Cms.Common;
    

    

    public partial class DXHMappingTest : Ektron.Cms.Workarea.Page
    {
        //Props/Fields
        private List<Control> editCtrls = null;
        private TaskManagerClient tClient=null;
        private ContextBusClient cbClient=null;
        private ConnectionManagerClient connMgr;
        private List<string> MappedField = null;
        private string labelPrefix = "lblEditCtrls_";
        private string controlPrefix = "ctrlEdits_";
        private long objectId
        {
            get
            {
                long ret=-1;
                if (!string.IsNullOrEmpty(Request.QueryString["objectid"]))
                {
                    if(long.TryParse(Request.QueryString["objectid"],out ret))
                        return ret;
                }
                return ret;
            }
        }
        private int langId
        {
            get
            {
                int ret = System.Threading.Thread.CurrentThread.CurrentUICulture.LCID;
                if (!string.IsNullOrEmpty(Request.QueryString["lang"]))
                {
                    if (int.TryParse(Request.QueryString["lang"], out ret))
                        return ret;
                }
                return ret;
            }
        }
        
        private DxHMappingData mapData=null;
        private MappingTask mapTask = null;
        //Page events
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            try
            {
                Ektron.Cms.SiteAPI _siteApi = new Ektron.Cms.SiteAPI();
                tClient = new TaskManagerClient();
                cbClient = new ContextBusClient();
                connMgr = new ConnectionManagerClient();

                mapData = DxHUtils.GetMapping(Ektron.Cms.Common.EkEnumeration.CMSObjectTypes.Content, objectId, langId);
                mapTask = tClient.GetTask(mapData.MappingTaskId) as MappingTask;

                if (!Utilities.ValidateUserLogin())
                {
                    Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
                }
                if (!_siteApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.SyncAdmin) &&
                    !_siteApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.SyncUser) &&
                    !_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.DxHConnectionAdmin) &&
                    !_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.DxHConnectionUser))
                {
                    Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
                }
                Packages.Ektron.Namespace.Register(this);
                Packages.Ektron.CssFrameworkBase.Register(this);
                Packages.Ektron.Workarea.Core.Register(this);

                if (!cbClient.TestDxhConnection(cbClient.ServiceUrl))
                {
                    //throw new Exception(GetLocalResourceObject("Err_NoDxHConn").ToString());
                    DisplayMsg(GetLocalResourceObject("Err_NoDxHConn").ToString(), false, Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error);
                    return;
                }

                if (!connMgr.TestConnection(connMgr.LoadConnection(mapData.Connection, mapData.Adapter).ToList(), mapData.Adapter))
                {
                    DisplayMsg(GetLocalResourceObject("Err_NoAdapterConn").ToString(), false, Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error);
                    //throw new Exception(GetLocalResourceObject("Err_NoAdapterConn").ToString());
                }

                setUIText();
                if (objectId < 0)
                {
                    DisplayMsg(GetLocalResourceObject("Err_GeneralException").ToString(), false, Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error);
                    btnTest.Visible = true;
                    pnlControlHolder.Visible = false;
                }
                else
                {

                    buildChildControls();
                    pnlControlHolder.Visible = true;
                    msgBox.ContentTemplate = new MessageTemplate();
                    MappedField = new List<string>();
                }
            }
            catch (Exception ex)
            {
                ltrPageHeadign.Text = GetLocalResourceObject("PageTitle").ToString();
                btnTest.Visible = false;
                btnCancelNoConfirm.Visible = true;
                btnCancel.Visible = false;
                string err = GetLocalResourceObject("Err_GeneralException").ToString();
                //DisplayMsg(err + "<br/>" + ex.Message, false, Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error);
                DisplayMsg(err , false, Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error);
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
        protected void btnTestClick(object sender, EventArgs e)
        {

            try 
            { 

                //Send payload to external
                ObjectInstance payload = new ObjectInstance(mapTask.SourceObject);
                foreach (Field f in payload.Fields)
                {
                    string cId = controlPrefix + f.Definition.Id;
                    Control c = editCtrls.Find(x => x.ID == cId);
                    if (c != null)
                    {
                        f.Value = c.GetType().GetProperty("Text").GetValue(c, null).ToString();
                    }
                }
				
				if (mapData.Adapter.ToLower() == "hubspot")
                {
                    if (!string.IsNullOrEmpty(this.hubspotusertoken.Value))
                        payload.Fields.Single(p => p.Id.ToLower().Equals("dxhhubspotusertoken")).Value = this.hubspotusertoken.Value;
                    else
                        payload.Fields.Single(p => p.Id.ToLower().Equals("dxhhubspotusertoken")).Value = "";
                }
                
                List<WorkflowExecutionMessage> retMsgs= DxHUtils.TestFormSubmit(mapData.Adapter, mapData.Connection, mapTask, payload);

                msgBox.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Success;
                msgBox.Text = GetLocalResourceObject("success").ToString();
                StringBuilder sb = new StringBuilder();
                sb.Append(GetLocalResourceObject("ExtMSGHeading").ToString() + ":<br/>");
                foreach (WorkflowExecutionMessage wfmsg in retMsgs)
                {
                    switch ((int)wfmsg.Severity) {
                        case (int)MessageSeverity.Error:
                            sb.Append("<font color=\"red\">");
                            break;
                        case (int)MessageSeverity.Warning:
                            sb.Append("<font color=\"yellow\">");
                            break;
                        case (int)MessageSeverity.Success:
                            sb.Append("<font color=\"green\">");
                            break;
                        case (int)MessageSeverity.Info:
                            sb.Append("<font color=\"blue\">");
                            break;
                        default:
                            sb.Append("<font>");
                            break;
                    }


                    sb.Append(GetLocalResourceObject("MessageSeverity_" + ((int)wfmsg.Severity).ToString()).ToString() + ":" + wfmsg.Message);
                    sb.Append("</font><br/>");
                }

                

                //Get message from external objects
                FieldDefinition IdField = mapTask.TargetObject.Fields.Find(x => x.IsKey);
                SaveMessage SavMsg = retMsgs.Find(x => x.GetType() == typeof(SaveMessage)) as SaveMessage;
                Dictionary<string, object> keyDics = new Dictionary<string, object>();
                if (SavMsg != null)
                {                   
                foreach (Field k in SavMsg.Keys)
                {
                        if (k.Value != null)
                    {
                            keyDics.Add(k.Id, k.Value);
                        }                        
                    }
                }
                if (keyDics.Any())
                {
                    try
                    {

                        cbClient.Login(mapData.Connection, mapData.Adapter);
                        ObjectInstance retOI = cbClient.GetObjectInstance(mapTask.TargetObject, keyDics, mapData.Adapter);
                        cbClient.Logout(mapData.Adapter);
                        List<MappableField> MappedFieldSet = mapTask.TargetFields.FindAll(x => x.Mapping.MappingType == MappingType.FieldMapping);

                        sb.Append(string.Format(GetLocalResourceObject("str_TestCompleted_heading").ToString(), mapTask.TargetObject.DisplayName, mapData.Adapter));
                        sb.Append("<br/>");
                        sb.Append("<UL>");
                        foreach (MappableField f in MappedFieldSet)
                        {
                            Field rF = retOI.Fields.Find(x => x.Id == f.Id);
                            sb.Append("<LI>");
                            sb.Append(rF.DisplayName + ":" + rF.Value);
                            sb.Append("</LI>");
                        }
                        sb.Append("</UL>");
                    }
                    catch (Exception ex)
                    {
                        var st = "Error when retriving values from external system.";
                        msgBox.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Warning;
                        sb.Append(st);
                        msgBox.Text = st;
                    }
                }
                else
                {
                    //No ID returned, error out.
                    msgBox.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
   //                 msgBox.Text = GetLocalResourceObject("errorOccured").ToString();
                }
                
                msgBox.Visible = true;
               // msgBox.ContentTemplate = new MessageTemplate(sb.ToString());
                msgBox.Text = (sb.ToString());
                //Warp up

                btnTest.Visible = true;  //!KeyFieldReturned;
                btnCancelNoConfirm.Visible = true;
                btnCancel.Visible = false;
                btnEditMapping.Visible = true;
            }
            catch (Exception ex)
            {
                msgBox.Visible = true;
                msgBox.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                //msgBox.ContentTemplate = new MessageTemplate(GetLocalResourceObject("Err_GeneralException").ToString());
                msgBox.ContentTemplate = new MessageTemplate(ex.Message + "<br/>" + ex.StackTrace);
                msgBox.Text = ex.Message;
            }

           
            //List<MappableField> fieldList = mapTask.TargetFields.FindAll(x => x.Mapping.MappingType == MappingType.FieldMapping);

            //DxHUtils.TestFormSubmit("", "", mapTask, new Ektron.DxH.Common.Objects.ObjectInstance());
            msgBox.Visible = true;

        }


        //Private methods
        private void setUIText()
        {
            ltrPageInstr.Text = string.Format(GetLocalResourceObject("PageHeadingText").ToString(), mapData.Adapter);
            ltrPageHeadign.Text = GetLocalResourceObject("PageTitle").ToString();
            ltr_CloseConfirmHeading.Text = GetLocalResourceObject("str_CloseConfirmText_heading").ToString();
            ltr_CloseConfirmBody.Text = string.Format(GetLocalResourceObject("str_CloseConfirmText").ToString(), mapData.Adapter);
        }
        private void buildChildControls()
        {
           
            editCtrls = new List<Control>();
            FormManager formMgr = new FormManager();
            FormFieldList currentFieldLst = formMgr.GetFormFieldList(objectId);

            List<MappableField> fieldList = mapTask.TargetFields.FindAll(x => x.Mapping.MappingType == MappingType.FieldMapping);

            List<FormFieldDefinition> formList = currentFieldLst.Fields.ToList();



            Table tbl = new Table();
            tbl.ID = "tblEditBody";

            foreach (FormFieldDefinition Def in formList)
            {

                bool fieldEnabled = false;
                if (fieldList.Count(m => m.Mapping.Value.ToString() == Def.FieldName) > 0)
                    fieldEnabled = true;
                //FormFieldDefinition Def=formList.Find(x=>x.FieldName== m.Mapping.Value.ToString());
                //if(Def==null)
                    //continue;
                TableRow tr = new TableRow();

                TableCell tdLeft = new TableCell();
                TableCell tdRight = new TableCell();

                tdLeft.CssClass = "editTDLeft";
                tdRight.CssClass = "editTDRight";

                tdLeft.Style.Add("font-weight", "bold");

                Literal ltr = new Literal();
                ltr.ID = labelPrefix + Def.FieldName;
                ltr.Text = Def.DisplayName ;
                //if (p.IsRequired)
                //{
                //    ltr.Text += " <span class=\"ektron-ui-required\">*</span>";
                //}
                Control c = LoadControl(typeof(Ektron.Cms.Framework.UI.Controls.EktronUI.TextField), null);

                c.GetType().GetProperty("Enabled").SetValue(c, fieldEnabled, null);
                if (!fieldEnabled)
                {
                    c.GetType().GetProperty("Text").SetValue(c, GetLocalResourceObject("str_FieldNotMapped"), null);
                }
                c.ID = controlPrefix + Def.FieldName;
                
                //editCtrls.Add(ltr);

                if (fieldEnabled)
                    editCtrls.Add(c);

                tdLeft.Controls.Add(ltr);
                tdRight.Controls.Add(c);


                tr.Cells.Add(tdLeft);
                tr.Cells.Add(tdRight);

                tbl.Rows.Add(tr);
            }
            this.pnlControlHolder.Controls.Add(tbl);
            string editDxHMappingURL="dxhmapping.aspx?formid=" + objectId + "&lang=" + langId;
            btnEditMapping.NavigateUrl = editDxHMappingURL;
        }
        private void DisplayMsg(string msg, bool useTemplate, Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes mode)
        {
            msgBox.DisplayMode = mode;
            msgBox.Visible = true;
            if (useTemplate)
            {
                msgBox.ContentTemplate = new MessageTemplate(msg);
            }
            else
            {
                msgBox.Text = msg;
            }
        }
        internal class MessageTemplate:ITemplate
        {
            public MessageTemplate() { }
            public MessageTemplate(string MsgText)
            {
                MessageTest = MsgText;
            }
            public string MessageTest { get; set; }
            public void InstantiateIn(Control container)
            {
                Literal ltr = new Literal();
                ltr.ID = "lterMsgText";
                ltr.Text = MessageTest;
                container.Controls.Add(ltr);
            }
        }
    }
}