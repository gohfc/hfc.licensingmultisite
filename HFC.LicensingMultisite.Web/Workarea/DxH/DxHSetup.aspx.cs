namespace Workarea.DxH
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.DxH.Client;
    using Ektron.Cms.Common;
    using Ektron.Cms.Core;
    public partial class DxHSetup : Ektron.Cms.Workarea.Page
    {
        private ContextBusClient cbClient = null;
        private ConnectionManagerClient connMgr;
        private bool isInitialSetup = false;
        /// <summary>
        /// Private StyleHelper reference
        /// </summary>
        private StyleHelper refStyle = new StyleHelper();
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Ektron.Cms.SiteAPI _siteApi = new Ektron.Cms.SiteAPI();
            if (!Utilities.ValidateUserLogin())
            {
                Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
            }
            if (!_siteApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.SyncAdmin) &&
                !_siteApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.SyncUser) && !_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.DxHConnectionAdmin))
            {
                Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
            }
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            cbClient = new ContextBusClient();
            connMgr = new ConnectionManagerClient();
            setUIText();
            GetHelpButton();

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!Page.IsPostBack)
            {
                this.tbURL.Text = cbClient.ServiceUrl;
            }
            if (tbURL.Text == "") btnDelete.Enabled = false;
        }
        protected void GetHelpButton() 
        {
            litHelp.Text = this.refStyle.GetHelpButton("dxh_aliasconnect", string.Empty);
        }
        protected void btnTestConn_Click(object sender, EventArgs e)
        {
            DxHUtils.ContextBusEndpoint = this.tbURL.Text;
            if (cbClient.TestDxhConnection(this.tbURL.Text))
            {
                msgError.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Success;
                msgError.Text = GetLocalResourceObject("strTestSucess").ToString();
            }
            else
            {
                msgError.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                msgError.Text = GetLocalResourceObject("strTestFail").ToString();
            }
            msgError.Visible = true;
        }

        protected void btnConnectDXH_Click(object sender, EventArgs e)
        {
            //ensure that the new binding address is set
            DxHUtils.ContextBusEndpoint = this.tbURL.Text;
            if (cbClient.TestDxhConnection(this.tbURL.Text))
            {
                cbClient.SetDxhConnection(this.tbURL.Text);
                diagSaveOK.AutoOpen = true;

                if (isInitialSetup)
                {
                    SiteSetting settings = new SiteSetting();
                    SiteSettingData settingData = settings.GetItem((long)EkEnumeration.SiteSetting.DxHInboundConnection);
                    if ( string.IsNullOrEmpty(settingData.Value) )
                        btnSaveOK.OnClientClick = " window.location='ViewDXHConnections.aspx?refreshMenu=true&AdapterName=Ektron';";
                    else
                        btnSaveOK.OnClientClick = " window.location='ViewDXHConnections.aspx?refreshMenu=true';";
                }
                else
                {
                    btnSaveOK.OnClientClick = " window.location='ViewDXHConnections.aspx?refreshMenu=true';";
                }
                btnDelete.Enabled = true;
            }
            else
            {
                diagSaveResult.AutoOpen = true;
                diagSaveResult.Title = GetLocalResourceObject("PageTitle_Error").ToString();
            }
            
        }
        protected void btnSaveConfirm_Click(object sender, EventArgs e)
        {
            btnDelete.Enabled = true;
            cbClient.SetDxhConnection(this.tbURL.Text);
            //Redirect to the list view
            this.Page.Response.Redirect("ViewDXHConnections.aspx?refreshMenu");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            diagDelConfirm.AutoOpen = true;
        }
        protected void btnDeleteConfim_Click(object sender, EventArgs e) {
            //These properties are static in the DxHUtils class so we have to reset them
            DxHUtils.ContextBusEndpoint = "";
            SiteSetting settings = new SiteSetting();
            settings.Delete((long)EkEnumeration.SiteSetting.DxHConnection);
            tbURL.Text = "";
            btnDelete.Enabled = false;
        }

        private void setUIText()
        {
            if (string.IsNullOrEmpty(cbClient.ServiceUrl))
            {
                ltrInstruction.Text = GetLocalResourceObject("strInstructions").ToString();
                ltrPageTitle.Text=ltrPageHeadign.Text = GetLocalResourceObject("PageTitle").ToString();
                //btnConnectDXH.Text = GetLocalResourceObject("btnConnDXH").ToString();
                isInitialSetup = true;
                tdCancelButton.Visible = false;
            }
            else
            {
                ltrInstruction.Text = GetLocalResourceObject("strInstructions_Edit").ToString();
                ltrPageTitle.Text = ltrPageHeadign.Text = GetLocalResourceObject("PageTitle_Edit").ToString();
                //btnConnectDXH.Text = GetLocalResourceObject("btnConnDXH_Edit").ToString();
                tdCancelButton.Visible = true;
            }
        }
    }
}