<%@ Page Language="C#" AutoEventWireup="true" CodeFile="productsearch.aspx.cs" Inherits="Workarea_productsearch" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
 </head>
<body id="body" runat="server">
    <%=StyleSheetJS%>
    <%=SearchStyleSheet%>
    <%=SearchJScript%>
    <form id="form1" runat="server">
        <div id="dhtmltooltip"></div>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server"></div>
            <div class="ektronToolbar" id="divToolBar" runat="server"></div>
        </div>
        
        <div class="workareaProductSearch">
            <ektron:ProductSearchInputView ID="ProductSearchView1" ControllerID="ProductSearchController1" runat="server">
            </ektron:ProductSearchInputView>
            
            <div class="ektron-ui-control ektron-ui-search ektron-ui-search-results ektron-ui-search-results-products">
                <ektron:ProductSearchResultsView ID="ProductSearchResultsView1" ControllerID="ProductSearchController1" runat="server">
                    <ItemTemplate>
                        <div class="section no-results">
                            <asp:Label ID="aspNoResults" runat="server"
                                Visible='<%# (Ektron.Cms.Framework.UI.SearchState)Eval("State") == Ektron.Cms.Framework.UI.SearchState.NoResults %>' 
                                Text='<%# m_refMsg.GetMessage("generic no search results found") %>' 
                                ></asp:Label>
                        </div>

                        <div class="section results">
                            <div Id="divResultsHeader" class="resultsHeader"
                                    Visible='<%# (Ektron.Cms.Framework.UI.SearchState)Eval("State") == Ektron.Cms.Framework.UI.SearchState.SearchResults %>' 
                                    runat="server">
                                    <asp:Label ID="aspResultsHeader" runat="server" 
                                        Text='<%# String.Format(m_refMsg.GetMessage("msg workarea search result header"), 
                                                Eval("PageInfo.StartCount"),
                                                Eval("PageInfo.EndCount"),
                                                Eval("PageInfo.ResultCount"),
                                                Eval("QueryText"),
                                        ((TimeSpan)Eval("ElapsedTime")).TotalSeconds.ToString())  %>' >
                                    </asp:Label>
                            </div>

                            <asp:ListView ID="aspResults" DataSource='<%# Eval("Results") %>' ItemPlaceholderID="aspPlaceholder" 
                                OnItemDataBound="searchResults_OnItemDataBound" runat="server">
                                <LayoutTemplate>
                                <ul class="results">
                                    <asp:PlaceHolder ID="aspPlaceholder" runat="server"></asp:PlaceHolder>
                                </ul>
                                </LayoutTemplate>
                                    <ItemTemplate>
                                    <li class="result ektron-ui-clearfix">
                                        <div class="avatar">
                                            <a href="<%# Eval("Url") %>"><asp:Image ID="aspAvatar" runat="server" AlternateText='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>' ImageUrl='<%# Eval("ImageUrl") %>' Visible='<%# !String.IsNullOrEmpty(DataBinder.Eval(Container.DataItem, "ImageUrl").ToString()) %>' /></a>
                                        </div>
                                        <div class="resultsInfo">
                                            <h3 class="title"><a href="<%# Eval("Url") %>"><%# Eval("Title") %></a></h3>
                                            <div class="summary"><%# Eval("Summary") %></div>
                                            <ul class="ektron-ui-listStyleNone">
                                                <li class="sku ektron-ui-quiet comment">
                                                    <asp:Literal ID="aspLegendText" runat="server" Text='<%# m_refMsg.GetMessage("lbl calatog entry sku") %>' />: <%# Eval("SKU") %>
                                                </li>
                                                <li class="catalogNumber ektron-ui-quiet">
                                                    <asp:Literal ID="aspCatalogNumber" runat="server" Text='<%# m_refMsg.GetMessage("lbl catalog number label") %>' />: <%# Eval("CatalogNumber") %>
                                                    <asp:Literal ID="aspProductId" runat="server" Text='<%# m_refMsg.GetMessage("lbl product label") %>' />: <%# Eval("ProductID")%>
                                                </li>
                                            </ul>
                                        </div>
                                        <ul class="prices">
                                            <li><asp:label ID="aspListPrice" CssClass="listPriceLabel" Text='<%# m_refMsg.GetMessage("lbl list price label") %>' runat="server" />: <span runat="server" id="listPrice" class="listPrice"></span></li>
                                            <li><asp:label ID="aspSalePrice" CssClass="salePriceLabel" Text='<%# m_refMsg.GetMessage("lbl sale price label") %>' runat="server" />: <span runat="server" id="salePrice" class="salePrice"></span></li>
                                        </ul>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                       </div> 
                    </ItemTemplate> 
                </ektron:ProductSearchResultsView>
            </div> 
        </div> 
        <ektron:ProductSearchController ID="ProductSearchController1" runat="server" />
        <ektronUI:Pager ID="Pager1" PageableControlID="ProductSearchController1" ResultsPerPage="5" ControllerID="ProductSearchController1"
            runat="server">
        </ektronUI:Pager>

    </form>
</body>
</html>

