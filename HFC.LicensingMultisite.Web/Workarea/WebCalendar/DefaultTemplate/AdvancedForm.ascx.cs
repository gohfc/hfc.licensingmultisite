﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Ektron.Cms.API;
using Ektron.Cms;
using Ektron.Cms.Controls.CalendarProvider;
using System.Collections.Specialized;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using Ektron.Cms.Content.Calendar;
using Ektron.Cms.Common.Calendar;
using System.Globalization;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Settings.UrlAliasing;
using Ektron.Cms.Settings.UrlAliasing.DataObjects;

namespace SchedulerTemplatesCS
{
    /// <summary>
    /// Specifies the advanced form mode.
    /// </summary>
    public enum AdvancedFormMode
    {
        Insert, Edit
    }

    public partial class AdvancedForm : Ektron.Cms.Controls.WebCalendarForms.AdvancedFormBase
    {
        #region Private members
        private static readonly string[] DayOrdinalValues = { "1", "2", "3", "4", "-1" };
        private static readonly string[] DayMaskValues = { 
            ((int) RecurrenceDay.EveryDay).ToString(),
            ((int) RecurrenceDay.WeekDays).ToString(),
            ((int) RecurrenceDay.WeekendDays).ToString(),
            ((int) RecurrenceDay.Sunday).ToString(),
            ((int) RecurrenceDay.Monday).ToString(),
            ((int) RecurrenceDay.Tuesday).ToString(),
            ((int) RecurrenceDay.Wednesday).ToString(),
            ((int) RecurrenceDay.Thursday).ToString(),
            ((int) RecurrenceDay.Friday).ToString(),
            ((int) RecurrenceDay.Saturday).ToString() };
        private string[] DayOrdinalDescriptions;
        private string[] DayMaskDescriptions;
        private readonly string[] InvariantMonthNames;
        private bool FormInitialized
        {
            get
            {
                return Request.Form["AddEventFormDisplay"] == "true";
            }
        }
        private AdvancedFormMode mode = AdvancedFormMode.Insert;
        private ContentAPI _contentApi;
        private SiteAPI _siteApi;
        protected EkMessageHelper _msgRef;
        private long _FolderID = 0;
        private long _SelectedTaxId = 0;
        private bool _IsEventSearchable = true;
        private long SelectedTaxID
        {
            get
            {
                return _SelectedTaxId;
            }

            set
            {
                _SelectedTaxId = value;
            }
        }
        private WebEventData _eventData = null;
        private bool _eventDataFetched = false;
        private CultureInfo _clientCulture = null;
        protected StyleHelper m_refStyle = new StyleHelper();
        private string m_SelectedEditControl = string.Empty;

        #endregion

        #region FormBase Properties
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override string Title
        {
            get
            {
                return txtTitle.Text;
            }
            set
            {
                txtTitle.Text = value;
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override string Location
        {
            get
            {
                return txtLocation.Text;
            }
            set
            {
                txtLocation.Text = value;
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override string Subject
        {
            get
            {
                switch (m_SelectedEditControl)
                {
                    case "Aloha":
                        return "<p>" + AlohaEditor.Content.Replace("&lt;", "<").Replace("&gt;", ">") + "</p>";
                    case "ContentDesigner":
                    default:
                        return ContentDesigner.Content;
                }
            }

            set
            {
                switch (m_SelectedEditControl)
                {
                    case "Aloha":
                        AlohaEditor.Content = System.Net.WebUtility.HtmlEncode(value);
                        break;
                    case "ContentDesigner":
                    default:
                        ContentDesigner.Content = value;
                        break;
                }

            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override long Folder
        {
            get
            {
                _FolderID = 0;
                if (sourceSelector != null && sourceSelector.SelectedValue != null && sourceSelector.SelectedValue != "")
                {
                    long.TryParse(sourceSelector.SelectedValue.Split('|')[0], out _FolderID);
                }
                return _FolderID;
            }
            set
            {
                _FolderID = value;
                if (sourceSelector.Items.FindByValue(value.ToString() + "|" + _SelectedTaxId) != null)
                {
                    sourceSelector.SelectedValue = _FolderID.ToString() + "|" + _SelectedTaxId.ToString();
                }
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override bool AllDay
        {
            get
            {
                return AllDayEvent.Checked;
            }
            set
            {
                AllDayEvent.Checked = value;
                if (value)
                {
                    EndTime.Style.Add("display", "none");
                    EndDate.Style.Add("display", "none");
                    StartTime.Style.Add("display", "none");
                    lblEndDate.Style.Add("display", "none");
                }
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override string TaxonomyIDs
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                TaxonomySelector.SelectedTaxonomies.ForEach(new Action<long>(delegate(long a) { sb.Append(a.ToString() + ","); }));
                if (sb.Length > 0) sb = sb.Remove(sb.Length - 1, 1);
                return sb.ToString();
            }
            set
            {
                List<string> ids = new List<string>();
                ids.AddRange(value.Split(','));
                List<long> tids = ids.ConvertAll<long>(new Converter<string, long>(delegate(string i) { return long.Parse(i); }));
                TaxonomySelector.SelectedTaxonomies.AddRange(tids);
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override DateTime OriginalStartDateTime
        {
            get
            {
                DateTime retval;
                IFormatProvider ci = new CultureInfo(1033);
                if (!DateTime.TryParseExact(hdnOriginalStartDateTime.Value, "yyyy-MM-dd-T-HH:mm:ss", ci, DateTimeStyles.AllowWhiteSpaces, out retval))
                {
                    retval = DateTime.MinValue;
                }
                return retval;
            }
            set
            {
                hdnOriginalStartDateTime.Value = value.ToString("yyyy-MM-dd-T-HH:mm:ss", ClientCulture.DateTimeFormat);
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override DateTime Start
        {
            get
            {
                DateTime timeToday = DateTime.Today;
                DateTime.TryParse(StartTime.Text, ClientCulture.DateTimeFormat, DateTimeStyles.AssumeLocal, out timeToday);

                return StartDate.Value.Add(timeToday - DateTime.Today);
            }
            set
            {
                StartDate.Value = value;
                StartTime.Text = value.ToString(GetTimeFormatForCulture(ClientCulture), ClientCulture.DateTimeFormat);
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override RecurrencePattern Pattern
        {
            get
            {
                if (!RecurrentAppointment.Checked)
                {
                    return null;
                }

                RecurrencePattern submittedPattern = new RecurrencePattern();
                submittedPattern.Frequency = Frequency;
                submittedPattern.Interval = Interval;
                submittedPattern.DaysOfWeekMask = DaysOfWeekMask;
                submittedPattern.DayOfMonth = DayOfMonth;
                submittedPattern.DayOrdinal = DayOrdinal;
                submittedPattern.Month = Month;

                if (submittedPattern.Frequency == RecurrenceFrequency.Weekly)
                {
                    submittedPattern.FirstDayOfWeek = Owner.FirstDayOfWeek;
                }

                return submittedPattern;
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override RecurrenceRange Range
        {
            get
            {
                DateTime startDate = Start;
                DateTime endDate = EndDateTime;

                if (AllDay)
                {
                    startDate = startDate.Date;
                    endDate = startDate.Date.AddDays(1);
                }

                RecurrenceRange range = new RecurrenceRange();
                range.Start = startDate;
                range.EventDuration = endDate - startDate;
                range.MaxOccurrences = 0;
                range.RecursUntil = DateTime.MaxValue;

                if (Owner.RecurrenceSupport)
                {
                    if (RepeatGivenOccurrences.Checked)
                    {
                        int maxOccurrences;
                        int.TryParse(RangeOccurrences.Text, out maxOccurrences);
                        range.MaxOccurrences = maxOccurrences;
                    }

                    if (RepeatUntilGivenDate.Checked && RangeEndDateTime != DateTime.MinValue)
                    {
                        range.RecursUntil = RangeEndDateTime;
                    }
                }

                return range;
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override string Metadata
        {
            get
            {
                StringBuilder metaXML = new StringBuilder();
                foreach (object key in MetadataSelector.Metadata.Keys)
                {
                    metaXML.Append("<meta id=\"");
                    metaXML.Append(((object[])MetadataSelector.Metadata[key])[0]);
                    metaXML.Append("\">");
                    metaXML.Append(EkFunctions.HtmlEncode(((object[])MetadataSelector.Metadata[key])[2].ToString()));
                    metaXML.Append("</meta>");
                }
                return "<metadata>" + metaXML.ToString() + "</metadata>";
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override string Image
        {
            get
            {
                return Request.Form["content_image"];
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override string ManualAliasName
        {
            get
            {
                return ((Workarea_controls_content_editaliasesTab)uxAliasTabContent).alias;
            }
            set { }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override string ManualAliasExtension
        {
            get
            {
                return ((Workarea_controls_content_editaliasesTab)uxAliasTabContent).extension;
            }
            set
            {
            }
        }
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public override bool IsEventSearchable
        {
            get
            {
                return _IsEventSearchable;
            }
            set
            {
                _IsEventSearchable = value;
            }
        }
        public AdvancedFormMode Mode
        {
            get
            {
                return mode;
            }
            set
            {
                mode = value;
            }
        }

        public string RecurrenceRuleText
        {
            get
            {
                if (Owner.RecurrenceSupport)
                {
                    RecurrenceRule rrule = RecurrenceRule.FromPatternAndRange(Pattern, Range);

                    if (rrule == null)
                    {
                        return string.Empty;
                    }

                    RecurrenceRule originalRule;
                    if (RecurrenceRule.TryParse(OriginalRecurrenceRule.Value, out originalRule))
                    {
                        rrule.Exceptions = originalRule.Exceptions;
                    }

                    return rrule.ToString();
                }

                return string.Empty;
            }

            set
            {
                OriginalRecurrenceRule.Value = value;
            }
        }
        public DateTime EndDateTime
        {
            get
            {
                if (AllDay)
                {
                    return Start.Date.AddDays(1);
                }

                DateTime timeToday = DateTime.Today;
                DateTime.TryParse(EndTime.Text, ClientCulture.DateTimeFormat, DateTimeStyles.AssumeLocal, out timeToday);

                return EndDate.Value.Add(timeToday - DateTime.Today);
            }
            set
            {
                EndDate.Value = value;
                EndTime.Text = value.ToString(GetTimeFormatForCulture(ClientCulture), ClientCulture.DateTimeFormat);
            }
        }
        public DateTime RangeEndDateTime
        {
            get
            {
                DateTime timeToday = DateTime.Today;
                DateTime.TryParse(RangeEndTime.Text, ClientCulture.DateTimeFormat, DateTimeStyles.AssumeLocal, out timeToday);

                return RangeEndDate.Value.Add(timeToday - DateTime.Today);
            }
            set
            {
                RangeEndDate.Value = value;
                RangeEndTime.Text = value.ToString(GetTimeFormatForCulture(ClientCulture), ClientCulture.DateTimeFormat);
            }
        }
        public WebEventData EventData
        {
            get
            {
                if (_eventDataFetched == false)
                {
                    _eventData = GetEventfromAppointment();
                    _eventDataFetched = true;
                }
                return _eventData;
            }
        }
        #endregion

        #region Protected properties
        protected ContentAPI ContentApi
        {
            get { if (_contentApi == null) _contentApi = new ContentAPI(); return _contentApi; }
        }
        protected RadScheduler Owner
        {
            get
            {
                return Appointment.Owner;
            }
        }
        protected EventRadScheduleProvider Provider
        {
            get
            {
                return (Owner.Provider as EventRadScheduleProvider);
            }
        }
        protected Appointment Appointment
        {
            get
            {
                SchedulerFormContainer container = (SchedulerFormContainer)BindingContainer;
                return container.Appointment;
            }
        }

        protected RecurrenceFrequency Frequency
        {
            get
            {
                if (RecurrentAppointment != null && RecurrentAppointment.Checked)
                {
                    if (RepeatFrequencyDaily != null && RepeatFrequencyDaily.Checked)
                    {
                        return RecurrenceFrequency.Daily;
                    }

                    if (RepeatFrequencyWeekly != null && RepeatFrequencyWeekly.Checked)
                    {
                        return RecurrenceFrequency.Weekly;
                    }

                    if (RepeatFrequencyMonthly != null && RepeatFrequencyMonthly.Checked)
                    {
                        return RecurrenceFrequency.Monthly;
                    }

                    if (RepeatFrequencyYearly != null && RepeatFrequencyYearly.Checked)
                    {
                        return RecurrenceFrequency.Yearly;
                    }
                }

                return RecurrenceFrequency.None;
            }
        }
        protected int Interval
        {
            get
            {
                switch (Frequency)
                {
                    case RecurrenceFrequency.Daily:
                        if (RepeatEveryNthDay.Checked)
                        {
                            return int.Parse(DailyRepeatInterval.Text);
                        }
                        break;

                    case RecurrenceFrequency.Weekly:
                        return int.Parse(WeeklyRepeatInterval.Text);

                    case RecurrenceFrequency.Monthly:
                        if (RepeatEveryNthMonthOnDate.Checked)
                        {
                            return int.Parse(MonthlyRepeatIntervalForDate.Text);
                        }

                        return int.Parse(MonthlyRepeatIntervalForGivenDay.Text);
                }

                return 0;
            }
        }
        protected RecurrenceDay DaysOfWeekMask
        {
            get
            {
                switch (Frequency)
                {
                    case RecurrenceFrequency.Daily:
                        return (RepeatEveryWeekday.Checked) ? RecurrenceDay.WeekDays : RecurrenceDay.EveryDay;

                    case RecurrenceFrequency.Weekly:
                        RecurrenceDay finalMask = RecurrenceDay.None;
                        finalMask |= WeeklyWeekDayMonday.Checked ? RecurrenceDay.Monday : finalMask;
                        finalMask |= WeeklyWeekDayTuesday.Checked ? RecurrenceDay.Tuesday : finalMask;
                        finalMask |= WeeklyWeekDayWednesday.Checked ? RecurrenceDay.Wednesday : finalMask;
                        finalMask |= WeeklyWeekDayThursday.Checked ? RecurrenceDay.Thursday : finalMask;
                        finalMask |= WeeklyWeekDayFriday.Checked ? RecurrenceDay.Friday : finalMask;
                        finalMask |= WeeklyWeekDaySaturday.Checked ? RecurrenceDay.Saturday : finalMask;
                        finalMask |= WeeklyWeekDaySunday.Checked ? RecurrenceDay.Sunday : finalMask;

                        return finalMask;

                    case RecurrenceFrequency.Monthly:
                        if (RepeatEveryNthMonthOnGivenDay.Checked)
                        {
                            return (RecurrenceDay)Enum.Parse(typeof(RecurrenceDay), MonthlyDayMaskDropDown.SelectedValue);
                        }
                        break;

                    case RecurrenceFrequency.Yearly:
                        if (RepeatEveryYearOnGivenDay.Checked)
                        {
                            return (RecurrenceDay)Enum.Parse(typeof(RecurrenceDay), YearlyDayMaskDropDown.SelectedValue);
                        }
                        break;
                }

                return RecurrenceDay.None;
            }
        }
        protected int DayOfMonth
        {
            get
            {
                switch (Frequency)
                {
                    case RecurrenceFrequency.Monthly:
                        return (RepeatEveryNthMonthOnDate.Checked ? int.Parse(MonthlyRepeatDate.Text) : 0);

                    case RecurrenceFrequency.Yearly:
                        return (RepeatEveryYearOnDate.Checked ? int.Parse(YearlyRepeatDate.Text) : 0);
                }

                return 0;
            }
        }
        protected int DayOrdinal
        {
            get
            {
                switch (Frequency)
                {
                    case RecurrenceFrequency.Monthly:
                        if (RepeatEveryNthMonthOnGivenDay.Checked)
                        {
                            return int.Parse(MonthlyDayOrdinalDropDown.SelectedValue);
                        }
                        break;

                    case RecurrenceFrequency.Yearly:
                        if (RepeatEveryYearOnGivenDay.Checked)
                        {
                            return int.Parse(YearlyDayOrdinalDropDown.SelectedValue);
                        }
                        break;
                }

                return 0;
            }
        }
        protected RecurrenceMonth Month
        {
            get
            {
                if (Frequency == RecurrenceFrequency.Yearly)
                {
                    string selectedMonth;

                    if (RepeatEveryYearOnDate.Checked)
                    {
                        selectedMonth = YearlyRepeatMonthForDate.SelectedValue;
                    }
                    else
                    {
                        selectedMonth = YearlyRepeatMonthForGivenDay.SelectedValue;
                    }

                    return (RecurrenceMonth)Enum.Parse(typeof(RecurrenceMonth), selectedMonth);
                }

                return RecurrenceMonth.None;
            }
        }
        protected CultureInfo ClientCulture
        {
            get
            {
                if (_clientCulture == null)
                {
                    try
                    {
                        _clientCulture = new CultureInfo(ContentApi.RequestInformationRef.UserCulture);
                    }
                    catch
                    {
                        _clientCulture = new CultureInfo(ContentApi.RequestInformationRef.DefaultContentLanguage);
                    }
                }
                return _clientCulture;
            }
        }
        #endregion

        public AdvancedForm()
        {
            InvariantMonthNames = new string[12];
            Array.Copy(Enum.GetNames(typeof(RecurrenceMonth)), 1, InvariantMonthNames, 0, 12);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            _msgRef = ContentApi.EkMsgRef;
            m_SelectedEditControl = Utilities.GetEditorPreference(Request);
            switch (m_SelectedEditControl)
            {
                case "Aloha":
                    AlohaEditor.Visible = true;
                    break;
                case "ContentDesigner":
                default:
                    ContentDesigner.Visible = true;
                    break;
            }
            if (Provider.DataSources.Count > 0)
            {
                try
                {
                    EventRadScheduleProvider.CalendarData mysrc;
                    mysrc = Provider.DataSources.Find(new Predicate<EventRadScheduleProvider.CalendarData>(
                        delegate(EventRadScheduleProvider.CalendarData cd)
                        {
                            return cd.PermissionData.CanEdit;
                        }));
                    if (mysrc != null)
                    {
                        string content_stylesheet = this.ContentApi.GetStyleSheetByFolderID(mysrc.FolderID);
                        if ("ContentDesigner" == m_SelectedEditControl)
                        {
                            ContentDesigner.Stylesheet = GetFullyQualifiedURL(ContentApi.SitePath + content_stylesheet);
                        }
                    }
					
					mysrc = Provider.DataSources.Find(x => x.PermissionData.IsReadOnlyLib);
                    if (mysrc != null)
                    {
                        ContentDesigner.SetPermissions(mysrc.PermissionData);
                    }
                }
                catch (Exception ex)
                {
                    string _error = ex.Message;
                }
            }
            string path = "";
            string alohaToolbar = "";
            if (ContentApi.RequestInformationRef.IsMembershipUser == 1)
            {
                path = ContentApi.RequestInformationRef.ApplicationPath + "WebCalendar/DefaultTemplate/MembershipInterface.xml";
                alohaToolbar = "CalendarEntryMembershipUser";
            }
            else
            {
                path = ContentApi.RequestInformationRef.ApplicationPath + "WebCalendar/DefaultTemplate/CMSUserInterface.xml";
                alohaToolbar = "CalendarEntryCMSUser";
            }
            if ("ContentDesigner" == m_SelectedEditControl)
            {
                ContentDesigner.ToolsFile = GetFullyQualifiedURL(path);
            }
            else //Aloha
            {
                AlohaEditor.ToolbarConfig = alohaToolbar;
            }

            InitializeStrings();
            PopulateDescriptions();
            InitializeMonthlyRecurrenceControls();
            InitializeYearlyRecurrenceControls();
        }

        protected string GetFullyQualifiedURL(string s)
        {
            Uri Result = new Uri(this.Page.Request.Url, s);
            return Result.ToString();
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (MetadataSelector.MetadataRequired && phMetadata.Visible == false)
            {
                phMetadata.Visible = MetadataSelector.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Packages.jQuery.jQueryUI.ThemeRoller.Register(this);
            Packages.jQuery.jQueryUI.Tabs.Register(this);
            Packages.jQuery.jQueryUI.Datepicker.Register(this);
            Packages.jQuery.Plugins.Globalization.Register(this);

            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
            JS.RegisterJSInclude(this, ContentApi.AppPath + "WebCalendar/DefaultTemplate/timeselector/jquery.ptTimeSelect.js", "WebCalendarAdvancedFormTimePickerJS");
            JS.RegisterJSInclude(this, ContentApi.AppPath + "WebCalendar/defaulttemplate/advancedform.js", "WebCalendarAdvancedFormJS");

            labelfortitle.Text = _msgRef.GetMessage("generic title label");
            labelforlocation.Text = _msgRef.GetMessage("generic location") + ":";
            labelfortitle.ToolTip = _msgRef.GetMessage("generic title label");
            labelforlocation.ToolTip = _msgRef.GetMessage("generic location") + ":";
            sourceSelector.ToolTip = _msgRef.GetMessage("lbl select calendar from drop down menu");
            StartDate.ToolTip = _msgRef.GetMessage("lbl start date");
            StartTime.ToolTip = _msgRef.GetMessage("lbl sync start time");
            EndDate.ToolTip = _msgRef.GetMessage("lbl enter end date here");
            EndTime.ToolTip = _msgRef.GetMessage("lbl enter end time here");
            AllDayEvent.Text = _msgRef.GetMessage("lbl all day");
            RecurrentAppointment.Text = _msgRef.GetMessage("lbl recurrence");
            UpdateButton.ToolTip = _msgRef.GetMessage("btn save");
            UpdateButton.Text = _msgRef.GetMessage("btn save");
            CancelButton.ToolTip = _msgRef.GetMessage("btn cancel");
            CancelButton.Text = _msgRef.GetMessage("btn cancel");
            labelfordescription.Text = _msgRef.GetMessage("lbl description");

            StartDate.OverrideDefaultCulture = new CultureInfo(ContentAPI.Current.RequestInformationRef.UserCulture);
            EndDate.OverrideDefaultCulture = new CultureInfo(ContentAPI.Current.RequestInformationRef.UserCulture);
            RangeEndDate.OverrideDefaultCulture = new CultureInfo(ContentAPI.Current.RequestInformationRef.UserCulture);

            UpdateButton.ValidationGroup = Owner.ValidationGroup;
            UpdateButton.CommandName = Mode == AdvancedFormMode.Edit ? "Update" : "Insert";
            UpdateButton.Command += new CommandEventHandler(CommandHandler);
            CancelButton.Command += new CommandEventHandler(CancelButton_Command);
            AdvancedEditCloseButton.Command += new CommandEventHandler(CancelButton_Command);

            AdvCalendarSelect.Visible = (Provider.DataSources.Count > 1);
            long lastselectedfolder = Folder;

            if (lastselectedfolder == 0 && uxLastSelectedFolderHidden.Value != "0")
                lastselectedfolder = long.Parse(uxLastSelectedFolderHidden.Value.Split('|')[0]);

            sourceSelector.Items.Clear();

            foreach (EventRadScheduleProvider.CalendarData cd in Provider.DataSources)
            {
                if (cd.PermissionData.CanAdd)
                {
                    FolderData fd = ContentApi.GetFolderById(cd.FolderID, false, false);
                    IsEventSearchable = fd.IscontentSearchable;
                    Ektron.Cms.API.User.User uapi = new Ektron.Cms.API.User.User();
                    Ektron.Cms.API.Community.CommunityGroup cgapi = new Ektron.Cms.API.Community.CommunityGroup();
                    if (fd != null)
                    {
                        string name = "";
                        switch (cd.sourceType)
                        {
                            case Ektron.Cms.Controls.SourceType.SystemCalendar:
                                name = "System Calendar: " + fd.Name + " (ID: " + cd.FolderID + ")";
                                break;
                            case Ektron.Cms.Controls.SourceType.GroupCalendar:
                                CommunityGroupData cgd = cgapi.GetCommunityGroupByID(cd.defaultId);
                                name = "Group Calendar: " + cgd.GroupName + " (Group ID: " + cd.defaultId + ")";
                                break;
                            case Ektron.Cms.Controls.SourceType.UserCalendar:
                                UserData thisUser = uapi.GetUser(cd.defaultId, false, true);
                                if (cd.defaultId == 0 || cd.defaultId == ContentApi.UserId)
                                {
                                    name = "My Calendar (" + thisUser.DisplayUserName + ")";
                                }
                                else
                                {
                                    name = "User Calendar: " + thisUser.DisplayUserName + " (User ID: " + cd.defaultId + ")";
                                }
                                break;
                        }
                        sourceSelector.Items.Add(new ListItem(name, cd.FolderID.ToString() + "|" + cd.SelectedTaxID.ToString()));
                    }
                }
            }
            if (Provider.DataSources.Count == 1)
            {
                SelectedTaxID = Provider.DataSources[0].SelectedTaxID;
                Folder = Provider.DataSources[0].FolderID;
            }
            if (sourceSelector.SelectedValue == string.Empty && sourceSelector.Items.Count > 0)
            {
                sourceSelector.Items[0].Selected = true;
            }
            if (lastselectedfolder != 0)
            {
                Folder = lastselectedfolder;
            }
            if (EventData != null)
            {
                Folder = EventData.FolderId;
                sourceSelector.Enabled = false;
            }
            if (Folder > 0)
            {
                TaxonomySelector.FolderID = Folder;
                MetadataSelector.FolderID = Folder;
                if (Ektron.Cms.DataIO.LicenseManager.LicenseManager.IsFeatureEnable(ContentApi.RequestInformationRef, Ektron.Cms.DataIO.LicenseManager.Feature.UrlAliasing, false))
                {
                    AliasSettings _aliasSettings = ObjectFactory.GetAliasSettingsManager(ObjectFactory.GetRequestInfoProvider().GetRequestInformation()).Get();
                    FolderData fd = ContentApi.GetFolderById(Folder, false, false);
                    if (_aliasSettings.IsAliasingEnabled && _aliasSettings.IsManualAliasingEnabled)
                    {
                        if (ContentApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.EditAlias))
                        {
                            phAliases.Visible = phAliasTab.Visible = true;
                            aliasRequired.InnerText = fd.AliasRequired.ToString().ToLower();
                        }
                    }

                    //-------------------DisplayTabs Based on selected options from Folder properties----------------------------------
                    if (((fd.DisplaySettings & (int)EkEnumeration.FolderTabDisplaySettings.AllTabs) == (int)EkEnumeration.FolderTabDisplaySettings.AllTabs) && fd.DisplaySettings != 0)
                    {

                        if ((fd.DisplaySettings & (int)EkEnumeration.FolderTabDisplaySettings.MetaData) == (int)EkEnumeration.FolderTabDisplaySettings.MetaData)
                        {
                            phMetadata.Visible = MetadataSelector.Visible = true;
                        }
                        else
                        {
                            if (!MetadataSelector.MetadataRequired)
                                phMetadata.Visible = MetadataSelector.Visible = false;
                        }
                        if (_aliasSettings.IsAliasingEnabled && (_aliasSettings.IsManualAliasingEnabled || _aliasSettings.IsFolderAliasingEnabled || _aliasSettings.IsTaxonomyAliasingEnabled) && _contentApi.IsARoleMember(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.EditAlias)) //And Not (m_bIsBlog)
                        {
                            if ((fd.DisplaySettings & (int)EkEnumeration.FolderTabDisplaySettings.Aliasing) == (int)EkEnumeration.FolderTabDisplaySettings.Aliasing)
                            { phAliases.Visible = phAliasTab.Visible = phAliases.Visible = true; }
                            else
                            {
                                if (!fd.AliasRequired)
                                    phAliases.Visible = phAliasTab.Visible = false;
                            }
                        }

                        if ((fd.DisplaySettings & (int)EkEnumeration.FolderTabDisplaySettings.Taxonomy) == (int)EkEnumeration.FolderTabDisplaySettings.Taxonomy)
                        { phTaxonomyTab.Visible = phTaxonomy.Visible = true; }
                        else
                        {
                            if (!fd.IsCategoryRequired)
                                phTaxonomyTab.Visible = phTaxonomy.Visible = false;
                        }

                    }
                    //-------------------DisplayTabs Based on selected options from Folder properties End-----------------------------
                }
            }

            if (!FormInitialized)
            {
                initHiddenData();
                PrefillEventControls();
                PrefillRecurrenceControls();
                UpdateResetExceptionsVisibility();
                bool failed = false;
                string initform = String.Format("Ektron.WebCalendar.AdvancedForm.init(\"{0}\", \"{1}\", {2});", Owner.ClientID, ContentApi.AppPath, ContentApi.RequestInformationRef.WorkAreaOperationMode.ToString().ToLower());
                try
                {
                    JavaScript.RegisterJavaScriptBlock(this, initform);
                }
                catch
                {
                    failed = true;
                }
                if (failed || Controls.IsReadOnly)
                {
                    //we're apparently in a full postback which doesn't care for registerjsblock
                    extrascript.Text = "<script type=\"text/javascript\" defer=\"defer\"> window.setTimeout(function(){" + initform + "}, 1500); </script>";
                    extrascript.Visible = true;
                }
            }
            else
            {
                extrascript.Visible = false;
            }
            btnHelp.Text = "<li class=\"actionbarDivider\">&nbsp;</li>" + m_refStyle.GetHelpButton("editevent", "");
        }

        void CancelButton_Command(object sender, CommandEventArgs e)
        {
            //called on cancel
            if (Request["showAddEventForm"] != null)
            {
                Response.Redirect(Request.Url.PathAndQuery.Replace("&showAddEventForm=true", ""), true);
            }
            RaiseBubbleEvent(this, (EventArgs)e);
        }

        void CommandHandler(object sender, CommandEventArgs e)
        {
            //called on save
            if (Request["showAddEventForm"] != null)
            {
                Response.Redirect(Request.Url.PathAndQuery.Replace("&showAddEventForm=true", ""), false);
            }
            try
            {
                RaiseBubbleEvent(this, (EventArgs)e);
            }
            catch (Exception ex)
            {
                Response.Redirect("reterror.aspx?info=" + Server.UrlEncode(ex.Message));
            }
        }

        protected void BasicControlsPanel_DataBinding(object sender, EventArgs e)
        {
            Start = Appointment.Start;
            OriginalStartDateTime = Appointment.Start;
            EndDateTime = Appointment.End;
            if (Appointment.Start == Appointment.Start.Date && Appointment.Duration == new TimeSpan(1, 0, 0, 0) && Appointment.ID == null)
            {
                AllDay = true;
            }
        }

        protected void PrefillEventControls()
        {
            if (EventData != null)
            {
                Title = EventData.DisplayTitle;
                Location = EventData.Location;
                Subject = EventData.Description;
                AllDay = EventData.IsAllDay;

                MetadataSelector.ContentID = EventData.Id;
                MetadataSelector.MetaUpdateString = "update";

                TaxonomyBaseData[] tbd = _contentApi.ReadAllAssignedCategory(EventData.Id);
                TaxonomySelector.SelectedTaxonomies.Clear();
                foreach (TaxonomyBaseData t in tbd)
                {
                    TaxonomySelector.PreselectedTaxonomies.Add(t.Id);
                }
                TaxonomySelector.ForceFill();

                IAliasManager aliasManager = ObjectFactory.GetAliasManager(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
                AliasData event_alias = aliasManager.GetAlias(EventData.Id, EventData.LanguageId, EkEnumeration.TargetType.Content);
                if (event_alias != null && !String.IsNullOrEmpty(event_alias.Alias) && event_alias.Alias.Contains("."))
                {
                    ManualAliasExtension = event_alias.Alias.Split('.')[1];
                    ManualAliasName = event_alias.Alias.Split('.')[0];
                }
            }
            else if (Provider.DataSources.Count > 0)
            {
                TaxonomySelector.defaultTaxID = Provider.DataSources[0].SelectedTaxID;
                TaxonomySelector.ForceFill();
            }
        }

        protected void initHiddenData()
        {
            initUserCulture.Text = ClientCulture.Name;
            initTimeDisplayFormat.Text = ClientCulture.DateTimeFormat.ShortTimePattern;
            initErrTitleRequired.Text = _msgRef.GetMessage("event title required");
            initErrStartRequired.Text = _msgRef.GetMessage("event start date time invalid");
            initErrEndRequired.Text = _msgRef.GetMessage("event end date time invalid");
            initErrMetaDataRequired.Text = _msgRef.GetMessage("event metadata required");
            initErrTaxonomyRequired.Text = _msgRef.GetMessage("event taxonomy required");
            initErrAliasRequired.Text = _msgRef.GetMessage("js manual alias req");
            initErrStartBeforeEnd.Text = _msgRef.GetMessage("event start datetime before end datetime");
            initLocationMaxLength.Text = _msgRef.GetMessage("lbl location max length");
            initTitleMaxLength.Text = _msgRef.GetMessage("lbl title max length");
            initInvalidCharTitle.Text = _msgRef.GetMessage("lbl invalid chars in title");
            initInvalidCharLocation.Text = _msgRef.GetMessage("lbl invalid chars in location");
            initMsgSpecifyNumberOfRecurOccurences.Text = _msgRef.GetMessage("specify number of recur occurences");
            initMsgSpecifyNumberOfOccurencesToEndAfter.Text = _msgRef.GetMessage("specify number of occurences to end after");
            initMsgSpecifyEndDateForRecurrence.Text = _msgRef.GetMessage("specify end date for recurrence");
            initMsgSpecifyEndTimeForRecurrence.Text = _msgRef.GetMessage("specify end time for recurrence");
            initMsgEnterAValidEndTime.Text = _msgRef.GetMessage("enter a valid end time");
            initMsgEnterAValidEndDate.Text = _msgRef.GetMessage("enter a valid end date");

            DateTime morning = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 00, 00);
            initTime8AM.Text = morning.ToString(ClientCulture.DateTimeFormat.ShortTimePattern, ClientCulture.DateTimeFormat);
            initTime9AM.Text = morning.AddHours(1).ToString(ClientCulture.DateTimeFormat.ShortTimePattern, ClientCulture.DateTimeFormat);
            initTimeDayStart.Text = morning.Date.ToString(ClientCulture.DateTimeFormat.ShortTimePattern, ClientCulture.DateTimeFormat);
            initCalendarButton.Text = ContentApi.ApplicationPath + "images/UI/Icons/calendar.png";
            initCalendarButtonAlt.Text = _msgRef.GetMessage("dtselect: date");
            initTimePickButton.Text = ContentApi.ApplicationPath + "images/UI/Icons/clock.png";
            initTimePickButtonAlt.Text = _msgRef.GetMessage("dtselect: time");
            initErrorIcon.Text = ContentApi.ApplicationPath + "images/UI/Icons/error.png";

            if (!TaxonomySelector.HasFolderTaxonomyChoices && Provider.DataSources.Count < 2)
            {
                phTaxonomyTab.Visible = false;
                phTaxonomy.Visible = false;
            }
        }

        protected WebEventData GetEventfromAppointment()
        {
            long folderid = 0;
            long eventid = 0;
            int langid = 0;
            bool isvariance = false;

            WebEventData eventdata = null;
            object apptid = (Appointment.ID == null) ? Appointment.RecurrenceParentID : Appointment.ID;
            if (Appointment.RecurrenceParentID != null) isvariance = true;
            if (apptid != null && apptid.ToString() != "" &&
                EventRadScheduleProvider.EventInfo.ExtractContentID(apptid.ToString(), out folderid, out eventid, out langid))
            {
                WebCalendar w = new WebCalendar(_contentApi.RequestInformationRef);
                w.RequestInformation.ContentLanguage = langid;
                if (isvariance)
                {
                    WebEventVarianceDictionary dic = w.WebEventManager.GetVarianceEventList(folderid, eventid);
                    if (dic != null && dic.ContainsKey(eventid))
                    {
                        eventdata = dic[eventid].Find(delegate(WebEventData wed) { return wed.EventStart == Appointment.Start && wed.IsCancelled == false; });
                    }
                }
                if (eventdata == null)
                {
                    eventdata = w.WebEventManager.GetItem(eventid, langid);
                }
            }
            return eventdata;
        }

        protected void RecurrencePatternPanel_DataBinding(object sender, EventArgs e)
        {
            Telerik.Web.UI.SchedulerFormContainer container = this.Parent as Telerik.Web.UI.SchedulerFormContainer;
            if (container != null && container.Appointment.RecurrenceState == RecurrenceState.Exception ||
                container.Appointment.RecurrenceState == RecurrenceState.Occurrence)
            {
                RecurrentAppointment.Checked = false;
                RecurrentAppointment.Enabled = false;
            }

            RecurrenceRule rrule;
            if (!RecurrenceRule.TryParse(OriginalRecurrenceRule.Value, out rrule))
            {
                RecurrentAppointment.Checked = false;
                return;
            }

            RecurrentAppointment.Checked = true;
            RecurrencePanel.Attributes.Remove("style");

            string interval = rrule.Pattern.Interval.ToString();
            int mask = (int)rrule.Pattern.DaysOfWeekMask;

            switch (rrule.Pattern.Frequency)
            {
                case RecurrenceFrequency.Daily:
                    RepeatFrequencyDaily.Checked = true;
                    RecurrencePatternDailyPanel.Style.Clear();

                    if (rrule.Pattern.DaysOfWeekMask == RecurrenceDay.WeekDays)
                    {
                        RepeatEveryWeekday.Checked = true;
                        RepeatEveryNthDay.Checked = false;
                    }
                    else
                    {
                        RepeatEveryWeekday.Checked = false;
                        RepeatEveryNthDay.Checked = true;
                        DailyRepeatInterval.Text = interval;
                    }
                    break;

                case RecurrenceFrequency.Weekly:
                    RepeatFrequencyWeekly.Checked = true;
                    RecurrencePatternWeeklyPanel.Style.Clear();

                    WeeklyRepeatInterval.Text = interval;

                    WeeklyWeekDayMonday.Checked = (RecurrenceDay.Monday & rrule.Pattern.DaysOfWeekMask) == RecurrenceDay.Monday;
                    WeeklyWeekDayTuesday.Checked = (RecurrenceDay.Tuesday & rrule.Pattern.DaysOfWeekMask) == RecurrenceDay.Tuesday;
                    WeeklyWeekDayWednesday.Checked = (RecurrenceDay.Wednesday & rrule.Pattern.DaysOfWeekMask) == RecurrenceDay.Wednesday;
                    WeeklyWeekDayThursday.Checked = (RecurrenceDay.Thursday & rrule.Pattern.DaysOfWeekMask) == RecurrenceDay.Thursday;
                    WeeklyWeekDayFriday.Checked = (RecurrenceDay.Friday & rrule.Pattern.DaysOfWeekMask) == RecurrenceDay.Friday;
                    WeeklyWeekDaySaturday.Checked = (RecurrenceDay.Saturday & rrule.Pattern.DaysOfWeekMask) == RecurrenceDay.Saturday;
                    WeeklyWeekDaySunday.Checked = (RecurrenceDay.Sunday & rrule.Pattern.DaysOfWeekMask) == RecurrenceDay.Sunday;
                    break;

                case RecurrenceFrequency.Monthly:
                    RepeatFrequencyMonthly.Checked = true;
                    RecurrencePatternMonthlyPanel.Style.Clear();

                    if (0 < rrule.Pattern.DayOfMonth)
                    {
                        RepeatEveryNthMonthOnDate.Checked = true;
                        RepeatEveryNthMonthOnGivenDay.Checked = false;
                        MonthlyRepeatDate.Text = rrule.Pattern.DayOfMonth.ToString();
                        MonthlyRepeatIntervalForDate.Text = interval;
                    }
                    else
                    {
                        RepeatEveryNthMonthOnDate.Checked = false;
                        RepeatEveryNthMonthOnGivenDay.Checked = true;
                        ListItem val = MonthlyDayOrdinalDropDown.Items.FindByValue(rrule.Pattern.DayOrdinal.ToString());
                        if (val != null) MonthlyDayOrdinalDropDown.SelectedValue = rrule.Pattern.DayOrdinal.ToString();
                        MonthlyDayMaskDropDown.SelectedIndex = Array.IndexOf(DayMaskValues, (mask).ToString());
                        MonthlyRepeatIntervalForGivenDay.Text = interval;
                    }
                    break;

                case RecurrenceFrequency.Yearly:
                    RepeatFrequencyYearly.Checked = true;
                    RecurrencePatternYearlyPanel.Style.Clear();

                    if (0 < rrule.Pattern.DayOfMonth)
                    {
                        RepeatEveryYearOnDate.Checked = true;
                        RepeatEveryYearOnGivenDay.Checked = false;
                        YearlyRepeatDate.Text = rrule.Pattern.DayOfMonth.ToString();
                        YearlyRepeatMonthForDate.SelectedIndex = ((int)rrule.Pattern.Month) - 1;
                    }
                    else
                    {
                        RepeatEveryYearOnDate.Checked = false;
                        RepeatEveryYearOnGivenDay.Checked = true;
                        YearlyDayOrdinalDropDown.SelectedValue = rrule.Pattern.DayOrdinal.ToString();
                        YearlyDayMaskDropDown.SelectedIndex = Array.IndexOf(DayMaskValues, (mask).ToString());
                        YearlyRepeatMonthForGivenDay.SelectedIndex = ((int)rrule.Pattern.Month) - 1;
                    }
                    break;
            }
        }

        protected void RecurrenceRangePanel_DataBinding(object sender, EventArgs e)
        {
            RecurrenceRule rrule;
            if (!RecurrenceRule.TryParse(OriginalRecurrenceRule.Value, out rrule))
            {
                return;
            }

            bool occurrencesLimit = (rrule.Range.MaxOccurrences != int.MaxValue);
            bool timeLimit = (rrule.Range.RecursUntil.Date != DateTime.MaxValue.Date);

            if (!occurrencesLimit && !timeLimit)
            {
                RepeatIndefinitely.Checked = true;
                RepeatGivenOccurrences.Checked = false;
                RepeatUntilGivenDate.Checked = false;
            }
            else
                if (occurrencesLimit)
                {
                    RepeatIndefinitely.Checked = false;
                    RepeatGivenOccurrences.Checked = true;
                    RepeatUntilGivenDate.Checked = false;

                    RangeOccurrences.Text = rrule.Range.MaxOccurrences.ToString();
                }
                else
                {
                    RepeatIndefinitely.Checked = false;
                    RepeatGivenOccurrences.Checked = false;
                    RepeatUntilGivenDate.Checked = true;

                    RangeEndDateTime = ConvertUTCtoLocal(rrule.Range.RecursUntil);
                }
        }

        /// <summary>
        /// Convert UTC to Local TimeZone
        /// </summary>
        /// <param name="uTC">DateTime in UTC</param>
        /// <returns>Local Datetime</returns>
        private DateTime ConvertUTCtoLocal(DateTime uTC)
        {
            var converteddatetime = uTC.ToLocalTime();
            return converteddatetime;
        }

        protected void ResetExceptions_OnClick(object sender, EventArgs e)
        {
            Owner.RemoveRecurrenceExceptions(Appointment);
            OriginalRecurrenceRule.Value = Appointment.RecurrenceRule;
            ResetExceptions.Text = _msgRef.GetMessage("status done"); 
        }

        protected string GetTimeFormatForCulture(CultureInfo cultureInfo)
        {
            string result;
            if (cultureInfo != null
                && !string.IsNullOrEmpty(cultureInfo.DateTimeFormat.AMDesignator)
                && !string.IsNullOrEmpty(cultureInfo.DateTimeFormat.PMDesignator))
            {
                result = "hh:mm tt";
            }
            else
            {
                result = "HH:mm";
            }

            return result;
        }

        #region Private methods

        private void InitializeStrings()
        {
            if ("ContentDesigner" == m_SelectedEditControl)
            {
                ContentDesigner.Validator.ValidationGroup = Owner.ValidationGroup;
                ContentDesigner.Validator.ErrorMessage = _msgRef.GetMessage("advanced subject required");
                ContentDesigner.Validator.Enabled = false;
            }
            AllDayEvent.Text = _msgRef.GetMessage("lbl all day");
            RecurrentAppointment.Text = _msgRef.GetMessage("lbl recurrence");
            ResetExceptions.Text = _msgRef.GetMessage("res_mem_reset");
            RepeatFrequencyDaily.Text = _msgRef.GetMessage("lbl sync daily");
            RepeatFrequencyWeekly.Text = _msgRef.GetMessage("lbl sync weekly");
            RepeatFrequencyMonthly.Text = _msgRef.GetMessage("lbl sync monthly");
            RepeatFrequencyYearly.Text = _msgRef.GetMessage("lbl yearly");
            RepeatEveryNthDay.Text = _msgRef.GetMessage("lbl every");
            RepeatEveryWeekday.Text = _msgRef.GetMessage("lbl weekday");
            RepeatEveryNthMonthOnDate.Text = _msgRef.GetMessage("day");
            RepeatEveryNthMonthOnGivenDay.Text =  _msgRef.GetMessage("lbl the"); 
            RepeatEveryYearOnDate.Text = _msgRef.GetMessage("lbl every"); 
            RepeatEveryYearOnGivenDay.Text = _msgRef.GetMessage("lbl the"); 
            RepeatIndefinitely.Text = _msgRef.GetMessage("lbl no end day");
            RepeatGivenOccurrences.Text = _msgRef.GetMessage("lbl end after");
            RepeatUntilGivenDate.Text = _msgRef.GetMessage("lbl end by"); 
            WeeklyWeekDayMonday.Text = _msgRef.GetMessage("generic monday");
            WeeklyWeekDayTuesday.Text = _msgRef.GetMessage("generic tuesday");
            WeeklyWeekDayWednesday.Text = _msgRef.GetMessage("generic wednesday");
            WeeklyWeekDayThursday.Text = _msgRef.GetMessage("generic thursday");
            WeeklyWeekDayFriday.Text = _msgRef.GetMessage("generic friday");
            WeeklyWeekDaySaturday.Text = _msgRef.GetMessage("generic saturday");
            WeeklyWeekDaySunday.Text = _msgRef.GetMessage("generic sunday");
        }

        private void PopulateDescriptions()
        {
            DayOrdinalDescriptions = new string[5];
            DayOrdinalDescriptions[0] = _msgRef.GetMessage("lbl first");
            DayOrdinalDescriptions[1] = _msgRef.GetMessage("lbl second");
            DayOrdinalDescriptions[2] = _msgRef.GetMessage("lbl third");
            DayOrdinalDescriptions[3] = _msgRef.GetMessage("lbl fourth");
            DayOrdinalDescriptions[4] = _msgRef.GetMessage("lbl last"); 

            DayMaskDescriptions = new string[10];
            DayMaskDescriptions[0] = _msgRef.GetMessage("day"); 
            DayMaskDescriptions[1] = _msgRef.GetMessage("lbl weekday"); 
            DayMaskDescriptions[2] = _msgRef.GetMessage("lbl weekend day"); 
            Array.Copy(Owner.Culture.DateTimeFormat.DayNames, 0, DayMaskDescriptions, 3, 7);
        }

        private void InitializeMonthlyRecurrenceControls()
        {
            MonthlyDayOrdinalDropDown.Items.AddRange(CreateComboBoxItemArray(DayOrdinalDescriptions, DayOrdinalValues));
            MonthlyDayMaskDropDown.Items.AddRange(CreateComboBoxItemArray(DayMaskDescriptions, DayMaskValues));
        }

        private void InitializeYearlyRecurrenceControls()
        {
            string[] monthNames = new string[12];
            Array.Copy(Owner.Culture.DateTimeFormat.MonthNames, monthNames, 12);

            YearlyRepeatMonthForDate.Items.AddRange(CreateComboBoxItemArray(monthNames, InvariantMonthNames));

            YearlyDayOrdinalDropDown.Items.AddRange(CreateComboBoxItemArray(DayOrdinalDescriptions, DayOrdinalValues));
            YearlyDayMaskDropDown.Items.AddRange(CreateComboBoxItemArray(DayMaskDescriptions, DayMaskValues));

            YearlyRepeatMonthForGivenDay.Items.AddRange(CreateComboBoxItemArray(monthNames, InvariantMonthNames));
        }

        private void PrefillRecurrenceControls()
        {
            DateTime start = Appointment.Start;

            switch (start.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    WeeklyWeekDaySunday.Checked = true;
                    break;

                case DayOfWeek.Monday:
                    WeeklyWeekDayMonday.Checked = true;
                    break;

                case DayOfWeek.Tuesday:
                    WeeklyWeekDayTuesday.Checked = true;
                    break;

                case DayOfWeek.Wednesday:
                    WeeklyWeekDayWednesday.Checked = true;
                    break;

                case DayOfWeek.Thursday:
                    WeeklyWeekDayThursday.Checked = true;
                    break;

                case DayOfWeek.Friday:
                    WeeklyWeekDayFriday.Checked = true;
                    break;

                case DayOfWeek.Saturday:
                    WeeklyWeekDaySaturday.Checked = true;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            MonthlyRepeatDate.Text = start.Day.ToString();

            YearlyRepeatMonthForDate.SelectedValue = InvariantMonthNames[start.Month - 1];
            YearlyRepeatMonthForGivenDay.SelectedValue = YearlyRepeatMonthForDate.SelectedValue;
            YearlyRepeatDate.Text = start.Day.ToString();
            OriginalRecurrenceRule.Value = Appointment.RecurrenceRule.ToString();
            DailyRepeatInterval.Text = "1";
            WeeklyRepeatInterval.Text = "1";
            MonthlyRepeatIntervalForDate.Text = "1";
            MonthlyRepeatIntervalForGivenDay.Text = "1";
        }

        private void UpdateResetExceptionsVisibility()
        {
            ResetExceptions.Visible = false;
            RecurrenceRule rrule;
            if (RecurrenceRule.TryParse(Appointment.RecurrenceRule, out rrule))
            {
                ResetExceptions.Visible = rrule.Exceptions.Count > 0;
            }
        }

        private static ListItem[] CreateComboBoxItemArray(string[] descriptions)
        {
            ListItem[] listItems = new ListItem[descriptions.Length];

            for (int i = 0; i < descriptions.Length; i++)
            {
                listItems[i] = new ListItem(descriptions[i]);
            }

            return listItems;
        }

        private static ListItem[] CreateComboBoxItemArray(string[] descriptions, string[] values)
        {
            if (descriptions.Length != values.Length)
            {
                throw new InvalidOperationException("There must be equal number of values and descriptions.");
            }

            ListItem[] listItems = CreateComboBoxItemArray(descriptions);

            for (int i = 0; i < values.Length; i++)
            {
                listItems[i].Value = values[i];
            }

            return listItems;
        }

        #endregion
    }
}
