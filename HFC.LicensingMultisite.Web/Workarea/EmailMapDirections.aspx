﻿<%@ Page Language="C#" %>
<!DOCTYPE html>
<script runat="server">
    protected Ektron.Cms.Common.EkRequestInformation _reqinfo = Ektron.Cms.ObjectFactory.GetRequestInfoProvider().GetRequestInformation();
    protected Ektron.Cms.ContentAPI _ContentApi = new Ektron.Cms.ContentAPI();
    protected Ektron.Cms.Common.EkMessageHelper _messageHelper;
    protected string source = string.Empty;
    protected string destination = string.Empty;
    protected string directions = string.Empty;
    string key = System.Configuration.ConfigurationManager.AppSettings["BingMapsAPIKey"];
    
    protected Ektron.Cms.Common.EkMessageHelper MessageHelper
    {
        get
        {
            return (_messageHelper ?? (_messageHelper = _ContentApi.EkMsgRef));
        }
    }

    private void SendMail(System.Net.Mail.MailMessage Message)
    {
        System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(_reqinfo.SMTPServerName, _reqinfo.SMTPPort);
        client.EnableSsl = _reqinfo.SMTPEnableSsl;
        client.Credentials = new System.Net.NetworkCredential(_reqinfo.SMTPUsername, _reqinfo.SMTPPassword);
        client.Send(Message);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
            string waypoint = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString["source"]))
            {
                source = Request.QueryString["source"];
                string result = GeocodeAddress(source);
                if (result != "No Results Found")
                {
                    waypoint = result;
                }
                else
                {
                    Response.Write(result);
                    return;
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["destination"]))
            {
                destination = Request.QueryString["destination"];
                string result = GeocodeAddress(destination);
                if (result != "No Results Found")
                {
                    waypoint = waypoint + ";" + result;
                }
                else
                {
                    Response.Write(result);
                    return;
                }
            }

            directions = CreateRoute(waypoint);
            __RouteInfoPane.InnerHtml = directions;
    }

    private String GeocodeAddress(string address)
    {
        string results = "";
        dev.virtualearth.net.webservices.v1.geocode.GeocodeRequest geocodeRequest = new dev.virtualearth.net.webservices.v1.geocode.GeocodeRequest();

        // Set the credentials using a valid Bing Maps key
        geocodeRequest.Credentials = new dev.virtualearth.net.webservices.v1.common.Credentials();
        geocodeRequest.Credentials.ApplicationId = key;

        // Set the full address query
        geocodeRequest.Query = address;

        // Set the options to only return high confidence results 
        dev.virtualearth.net.webservices.v1.geocode.ConfidenceFilter[] filters = new dev.virtualearth.net.webservices.v1.geocode.ConfidenceFilter[1];
        filters[0] = new dev.virtualearth.net.webservices.v1.geocode.ConfidenceFilter();
        filters[0].MinimumConfidence = dev.virtualearth.net.webservices.v1.common.Confidence.High;

        // Add the filters to the options
        dev.virtualearth.net.webservices.v1.geocode.GeocodeOptions geocodeOptions = new dev.virtualearth.net.webservices.v1.geocode.GeocodeOptions();
        geocodeOptions.Filters = filters;
        geocodeRequest.Options = geocodeOptions;

        // Make the geocode request
        GeocodeServiceClient geocodeService = new GeocodeServiceClient("BasicHttpBinding_IGeocodeService");
        dev.virtualearth.net.webservices.v1.geocode.GeocodeResponse geocodeResponse = geocodeService.Geocode(geocodeRequest);

        if (geocodeResponse.Results.Length > 0)
            results = String.Format("{0},{1}",
              geocodeResponse.Results[0].Locations[0].Latitude,
              geocodeResponse.Results[0].Locations[0].Longitude);
        else
            results = "No Results Found";

        return results;
    }

    private string CreateRoute(string waypointString)
    {
        string results = "";

        dev.virtualearth.net.webservices.v1.route.RouteRequest routeRequest = new dev.virtualearth.net.webservices.v1.route.RouteRequest();
        dev.virtualearth.net.webservices.v1.common.UserProfile userprofile = new dev.virtualearth.net.webservices.v1.common.UserProfile();
        userprofile.DistanceUnit = dev.virtualearth.net.webservices.v1.common.DistanceUnit.Mile;
        dev.virtualearth.net.webservices.v1.route.RouteOptions routeoptions = new dev.virtualearth.net.webservices.v1.route.RouteOptions();
        routeoptions.Mode = dev.virtualearth.net.webservices.v1.route.TravelMode.Driving;
        routeoptions.Optimization = dev.virtualearth.net.webservices.v1.route.RouteOptimization.MinimizeDistance;
        routeoptions.RoutePathType = dev.virtualearth.net.webservices.v1.route.RoutePathType.Points;
        routeoptions.TrafficUsage = dev.virtualearth.net.webservices.v1.route.TrafficUsage.TrafficBasedRouteAndTime;

        // Set the credentials using a valid Bing Maps key
        routeRequest.Credentials = new dev.virtualearth.net.webservices.v1.common.Credentials();
        routeRequest.Credentials.ApplicationId = key;
        routeRequest.UserProfile = userprofile;
        //Parse user data to create array of waypoints
        string[] points = waypointString.Split(';');
        dev.virtualearth.net.webservices.v1.route.Waypoint[] waypoints = new dev.virtualearth.net.webservices.v1.route.Waypoint[points.Length];

        int pointIndex = -1;
        foreach (string point in points)
        {
            pointIndex++;
            waypoints[pointIndex] = new dev.virtualearth.net.webservices.v1.route.Waypoint();
            string[] digits = point.Split(',');
            waypoints[pointIndex].Location = new dev.virtualearth.net.webservices.v1.common.Location();
            waypoints[pointIndex].Location.Latitude = double.Parse(digits[0].Trim());
            waypoints[pointIndex].Location.Longitude = double.Parse(digits[1].Trim());
       }

        routeRequest.Waypoints = waypoints;
        // Make the calculate route request
        RouteServiceClient routeService = new RouteServiceClient("BasicHttpBinding_IRouteService");
        try
        {
            dev.virtualearth.net.webservices.v1.route.RouteResponse routeResponse = routeService.CalculateRoute(routeRequest);
            // Iterate through each itinerary item to get the route directions
            StringBuilder directions = new StringBuilder("");

            if (routeResponse.Result.Legs.Length > 0)
            {
                int instructionCount = 0;
                int legCount = 0;
                long duration = 0;
                double distance = 0;
                string header = string.Empty;
                header = "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td><table>";
                header += "<tr><td><b>Start address: </b></td><td>" + source + "</td></tr>";
                header += "<tr><td><b>End address: </b></td><td>" + destination + "</td></tr>";

                foreach (dev.virtualearth.net.webservices.v1.route.RouteLeg leg in routeResponse.Result.Legs)
                {
                    legCount++;
                    distance += leg.Summary.Distance;
                    foreach (dev.virtualearth.net.webservices.v1.route.ItineraryItem item in leg.Itinerary)
                    {
                        instructionCount++;
                        directions.Append(string.Format("<tr><td style=\"background-color: #f6f6f6\">{0}.</td><td>{1} --({2:0.0}) {3}</td></tr>",
                            instructionCount, item.Text, item.Summary.Distance, routeRequest.UserProfile.DistanceUnit.ToString()));
                        duration += item.Summary.TimeInSeconds;
                    }
                }
                directions.Append("</table></td></tr></table>");
                header += "<tr><td><b>Total distance: </b></td><td>" + string.Format("{0:0.00}", distance) + " " + routeRequest.UserProfile.DistanceUnit.ToString() + "(" + convertToHourMinutes(duration) + ")</td></tr>";
                header += "</table></td></tr><tr><td><table>";

                results = header + directions.ToString();
            }
            else
                results = "No Route found";
        }
        catch (Exception)
        {
            results = "No Route found";
        }       

        return results;
    }

    private string convertToHourMinutes(long seconds)
    {
        TimeSpan t = TimeSpan.FromSeconds(seconds);
        int hours = t.Hours;
        hours += 24 * t.Days;
        return string.Format("{0} Hours, {1} Minutes", hours, t.Minutes);
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(_reqinfo.SystemEmail))
        {
            Response.Write(MessageHelper.GetMessage("lbl email required"));
        }
        else if (directions.Length > 0)
        {
            System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage(_reqinfo.SystemEmail, txtemail.Text, "Driving directions", directions);
            try
            {
                Message.IsBodyHtml = true;
                SendMail(Message);
                Response.Write("Mail Sent Successfully!");
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                Ektron.Cms.EkException.WriteToEventLog(ex.Message, System.Diagnostics.EventLogEntryType.Warning);
                Response.Write(MessageHelper.GetMessage("lbl email required"));
            }
            catch (Exception ex)
            {
                Ektron.Cms.EkException.WriteToEventLog(ex.Message, System.Diagnostics.EventLogEntryType.Warning);
                Response.Write("Error: Please try again!");
            }
            finally
            {
                Message = null;
            }
            Response.End();
        }
        else
        {
            Response.Write("Error: Please try again!");
        }
    }   
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Email Step By Step Directions</title>    
    <script type="text/javascript">        
        //validate the email address
        function validate() {
            var reEmailAddr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!reEmailAddr.test(document.getElementById("txtemail").value)) {
                alert("Please enter a valid Email address!");
                return false;
            }
            else {
                return true;
            }
        }
      </script>
</head>
<body>
    <form id="form1" runat="server" >
    <div>        
        <table border="0" style="height:inherit;">
            <tr><td>E-Mail Address:</td><td><asp:TextBox ID="txtemail" runat="server"></asp:TextBox> <asp:Button ID="btnsubmit"   runat="server" text="Send" OnClientClick="javascript:return validate();" OnClick="btnsubmit_Click"/></td></tr>
            <tr><td colspan="2"><div id="__RouteInfoPane" runat="server" style="border:solid 1px; overflow: scroll; width:inherit;"></div></td></tr>
        </table> 
    </div>
    </form>
</body>
</html>
