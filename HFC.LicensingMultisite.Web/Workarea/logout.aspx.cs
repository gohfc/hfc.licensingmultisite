﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Workarea_logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool bRet;
		string refferingPage = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
        if (Ektron.Cms.ContentAPI.Current.IsLoggedIn)
        {
            Ektron.Cms.User.EkUser userObj;
            userObj = Ektron.Cms.ContentAPI.Current.EkUserRef;

            try
            {
                bRet = userObj.LogOutUser(Ektron.Cms.ContentAPI.Current.UserId, userObj.RequestInformation.CookieSite);
				if (HttpContext.Current != null)
                    {
                        if (HttpContext.Current.Session != null)
                        {
                            HttpContext.Current.Session.Abandon();
                            HttpContext.Current.Session.RemoveAll();
                        }

                        HttpContext.Current.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    }
            }
            catch (Exception ex)
            {
                return;
            }
        }
       
        HttpContext.Current.Response.Redirect(refferingPage);
    }
}