﻿namespace Ektron.Workarea.Packaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Framework.Packaging;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Contracts.Packaging;
    using Ektron.Cms.Common;
    using System.Data.SqlClient;
    using Ektron.Cms.Framework.Context;

    public partial class PackageList : Ektron.Cms.Workarea.Page
    {
        #region Local Variables

        private int pageSize = 20;
        private SiteAPI siteAPI = new SiteAPI();
        PackageManager packageManager;
        List<Ektron.Cms.Contracts.Packaging.Package> packages;
        PackageCriteria criteria = new PackageCriteria();

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            if (this.HasViewPermission())
            {
                try
                {
                    base.OnInit(e);
                    this.initWorkareaUI();
                    this.pageSize = siteAPI.RequestInformationRef.PagingSize;
                    packageManager = new PackageManager();
                }
                catch (Exception exc)
                {
                    Utilities.ShowError(exc.Message);
                }
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                getCriteria();
                this.bindData();
            }
        }

        protected void uxPackageListGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "deletepackage")
            {
                Guid packageID = Guid.Empty;
                Guid.TryParse(e.CommandArgument.ToString(), out packageID);
                if (packageID != Guid.Empty)
                {
                    packageManager = new PackageManager();
                    try
                    {
                        packageManager.Delete(packageID);
                        uxMessage.Text = GetLocalResourceObject("packagedDeleted").ToString();
                        uxMessage.Visible = true;
                        uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Success;
                    }
                    catch (SqlException exc)
                    {
                        if (exc.Message.Contains("REFERENCE constraint"))
                        {
                            uxMessage.Text = GetLocalResourceObject("ForeignKeyConstraint").ToString();
                        }
                        else
                        {
                            uxMessage.Text = exc.Message.ToString();
                        }
                        uxMessage.Visible = true;
                        uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    }

                    getCriteria();
                    this.bindData();
                }
            }
        }

        protected void uxPackageListGridView_EktronUIThemePageChanged(object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            try
            {
                this.criteria.PagingInfo = e.PagingInfo;
                this.criteria.OrderByDirection = uxPackageListGridView.EktronUIOrderByDirection;
                this.criteria.OrderByField = (PackageProperty)Enum.Parse(typeof(PackageProperty), uxPackageListGridView.EktronUIOrderByFieldText);

                this.bindData();
            }
            catch (Exception exc)
            {
                Utilities.ShowError(exc.Message);
            }
        }
        #endregion

        #region Private Methods

        private void bindData()
        {
            packages = packageManager.GetList(criteria);
            uxPackageListGridView.EktronUIPagingInfo = this.criteria.PagingInfo;
            uxPackageListGridView.EktronUIOrderByFieldText = this.criteria.OrderByField.ToString();
            uxPackageListGridView.DataSource = packages;
            uxPackageListGridView.DataBind();
        }

        private void getCriteria()
        {
            criteria = new PackageCriteria();
            criteria.AddFilter(PackageProperty.Id, CriteriaFilterOperator.NotEqualTo, Guid.Empty);
            PagingInfo pagingInfo = new PagingInfo();
            pagingInfo.RecordsPerPage = pageSize;
            criteria.OrderByDirection = uxPackageListGridView.EktronUIOrderByDirection;
            if (!string.IsNullOrEmpty(uxPackageListGridView.EktronUIOrderByFieldText))
            {
                criteria.OrderByField = (PackageProperty)Enum.Parse(typeof(PackageProperty), uxPackageListGridView.EktronUIOrderByFieldText);
            }
            else
            {
                uxPackageListGridView.EktronUIOrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                criteria.OrderByDirection = EkEnumeration.OrderByDirection.Ascending;
                criteria.OrderByField = PackageProperty.Id;
            }
            criteria.PagingInfo = pagingInfo;
        }

        private void initWorkareaUI()
        {
            this.RegisterResources();
            StyleHelper styleHelper = new StyleHelper();
            aspHelpButton.Text = styleHelper.GetHelpButton("packaging_list", string.Empty);
            uxMessage.Visible = false;
        }

        private bool HasViewPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                    ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.SyncAdmin), ContentAPI.Current.UserId, false))
                );
        }

        private void RegisterResources()
        {
            Packages.EktronCoreJS.Register(this);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Ektron.Cms.API.JS.RegisterJS(this, "js/Ektron.Workarea.Packaging.js", "PackagingJS");
            Ektron.Cms.Framework.UI.JavaScript.Register(this, CmsContextService.Current.WorkareaPath + "/java/jfunct.js");
        }

        #endregion

    }

}