﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CollectionSelector.ascx.cs"
    Inherits="Ektron.Workarea.Packaging.CollectionSelector" %>
<div class="collectionSelectorWrapper clearfix">
    <ektronUI:Message ID="uxInfoMessage" runat="server" DisplayMode="Information" meta:resourcekey="uxInfoMessageResource1" />
    <asp:UpdatePanel ID="uxUpdatePanelCollection" runat="server">
        <ContentTemplate>
            <ektronUI:JavaScriptBlock ID="uxCollectionJavaScriptBlock" ExecutionMode="OnEktronReady"
                runat="server">
                <ScriptTemplate>
                    Ektron.Workarea.Packaging.syncCollectionSelections();
                </ScriptTemplate>
            </ektronUI:JavaScriptBlock>
            <div class="hdnFields">
                <asp:Button ID="uxTriggerGetCollection" runat="server" CssClass="uxTriggerGetCollection"
                    Style="display: none;" />
            </div>
            <div class="collectionFilters">
                <span style="float: right;">
                    <ektronUI:Label ID="uxVisibleItems" runat="server" Visible="false" meta:resourcekey="uxVisibleItems"></ektronUI:Label>
                    <ektronUI:Button ID="uxSelectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.collectionselectAllSummary();"
                        Visible="false" meta:resourcekey="uxSelectAllButton"></ektronUI:Button>
                    <ektronUI:Button ID="uxDeselectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.collectiondeselectAllSummary();"
                        Visible="false" meta:resourcekey="uxDeselectAllButton"></ektronUI:Button>
                </span>
            </div>
            <div class="uxCollectionLists">
                <ektronUI:GridView ID="uxCollectionListsGrid" runat="server" AutoGenerateColumns="false"
                    EktronUIAllowResizableColumns="false" OnEktronUIPageChanged="uxCollectionListsGrid_GridViewEktronUIThemePageChanged"
                    Visible="true">
                    <Columns>
                        <ektronUI:CheckBoxField ID="uxSelectColumn">
                            <CheckBoxFieldHeaderStyle HeaderCheckBoxVisible="false" />
                            <CheckBoxFieldColumnStyle Visible="false" />
                        </ektronUI:CheckBoxField>
                        <asp:TemplateField meta:resourceKey="uxIncludeResource">
                            <ItemTemplate>
                                <input type="checkbox" runat="server" name="uxCollectionitem" id="uxCollectionitem"
                                    class="uxCollectionitem" value='<%# Eval("Id") %>' data-ektron-id='<%# GetRowDataCollectionId((long) Eval("Id")) %>'
                                    data-ektron-title='<%#  Eval("Title") %>' data-ektron-lang="-1" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="uxCollectionName">
                            <ItemTemplate>
                                <asp:Literal ID="uxItemID" runat="server" Text='<%# Eval("Title") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="columnHeaderContentLanguage">
                            <ItemTemplate>
                                <img src='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(-1))%>'
                                    alt="collectionlanguageimage" title="All Language" />
                                <input type="hidden" id='uximgsrc_<%# Eval("Id") %>_-1' class="uximgsrchidden" value='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(-1))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </ektronUI:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
