﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PackageDefinitionSelector.ascx.cs"
    Inherits="Ektron.Workarea.Packaging.PackageDefinitionSelector" %>
<div class="packageDefinitionSelectorWrapper clearfix">
    <ektronUI:Message ID="uxInfoMessage" runat="server" DisplayMode="Information" meta:resourcekey="uxInfoMessageResource1" />
    <div class="packageFilters">
        <span style="float: right;">
            <ektronUI:Label ID="uxVisibleItems" runat="server" Visible="false" meta:resourcekey="uxVisibleItems"></ektronUI:Label>
            <ektronUI:Button ID="uxSelectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.packageselectAllSummary();"
                Visible="false" meta:resourcekey="uxSelectAllButton"></ektronUI:Button>
            <ektronUI:Button ID="uxDeselectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.packagedeselectAllSummary();"
                Visible="false" meta:resourcekey="uxDeselectAllButton"></ektronUI:Button>
        </span>
    </div>
    <div class="uxPackageList">
        <ektronUI:GridView ID="uxPackageList" runat="server" AutoGenerateColumns="false"
            EktronUIAllowResizableColumns="false" OnEktronUIPageChanged="uxPackageList_GridViewEktronUIThemePageChanged" Visible="true">
            <Columns>
                <ektronUI:CheckBoxField ID="uxSelectColumn">
                    <CheckBoxFieldHeaderStyle HeaderCheckBoxVisible="false" />
                    <CheckBoxFieldColumnStyle Visible="false" />
                </ektronUI:CheckBoxField>
                <asp:TemplateField meta:resourceKey="uxIncludeResource1">
                    <ItemTemplate>
                        <input type="checkbox" runat="server" name="packagedefcheck" id="packagedefcheck"
                            class="itemPackageDef" value='<%# Eval("Id") %>' data-ektron-id='P_<%# Eval("Id") %>'
                            data-ektron-title='<%#  Eval("Name") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField meta:resourceKey="uxPackageIDResource1">
                    <ItemTemplate>
                        <asp:Literal ID="uxItemID" runat="server" Text='<%# Eval("Id") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField meta:resourceKey="uxNameResource1">
                    <ItemTemplate>
                        <asp:Literal ID="uxItemName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </ektronUI:GridView>
    </div>
    <input type="hidden" id="uxcurrentPackageID" runat="server" class="uxcurrentPackageID" />
</div>
