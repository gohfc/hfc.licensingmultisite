﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FolderSystemTree.ascx.cs"
    Inherits="Ektron.Workarea.Packaging.FolderSystemTree" %>
<ul class="EktronFiletree EktronTreeview" style="overflow: auto; white-space: nowrap;">
    <li class="closed">
        <div id="uxwebsiterootExpand" class="hitarea closed-hitarea expandable-hitarea">
        </div>
        <span id="uxWebsiteroot" class="folder hover">
            <asp:Label ID="uxWebsiterootlabel" runat="server" meta:resourcekey="uxWebsiterootlabelResource1"></asp:Label>
        </span>
        <ul data-ektron-path="\\">
            <ul id="<%= ClientID %>" class="EktronFiletree EktronTreeview">
            </ul>
    </li>
</ul>
<script type="text/javascript">
<!--

function DirectoryToHtml(directory)
{
    var html = "";
    for(var subdirectory in directory.subdirectories)
    {
        html += "<li class=\"closed\"><span class=\"folder\">" +
            directory.subdirectories[subdirectory].substring(directory.subdirectories[subdirectory].lastIndexOf("\\")+1) +
            "</span><ul data-ektron-path=\"" + directory.subdirectories[subdirectory] + "\"></ul></li>";
    }
    return html;
}

$ektron("#uxWebsiteroot").click(function() {
GetDirectoryInfo();
$ektron("#uxwebsiterootExpand").attr("class", "hitarea closed-hitarea collapsable-hitarea");
});

$ektron("#uxwebsiterootExpand").click(function() {
GetDirectoryInfo();
$ektron("#uxwebsiterootExpand").attr("class", "hitarea closed-hitarea collapsable-hitarea");
});

function GetDirectoryInfo()
{$ektron.ajax(
{
    type: "POST",
    cache: false,
    async: false,
    url: "<%= new Ektron.Cms.SiteAPI().ApplicationPath %>Packaging/js/FileSelectorTree.ashx",
    data: {"path" : "\\"
           <%= (Filter != "" ? ", \"filter\" : \"" + Filter + "\"" : "") %>},
    success: function(msg)
    {
        var directory = eval("(" + msg + ")");
        $ektron("#<%= ClientID %>").html("<ul>" + DirectoryToHtml(directory) + "</ul>");
        $ektron("#<%= ClientID %>").treeview(
        {
            toggle : function(index, element)
            {
                var $element = $ektron(element);

                if($element.html() == "")
                {
                    $ektron.ajax(
                    {
                        type: "POST",
                        cache: false,
                        async: false,
                        url: "<%= new Ektron.Cms.SiteAPI().ApplicationPath %>Packaging/js/FileSelectorTree.ashx",
                        data: {"path" : "\\" + $element.attr("data-ektron-path") + "\\"
                               <%= (Filter != "" ? ", \"filter\" : \"" + Filter + "\"" : "") %>},
                        success: function(msg)
                        {
                        if(msg != ""){
                            var directory = eval("(" + msg + ")");
                            var el = $ektron(DirectoryToHtml(directory));
                            $element.append(el);
                            $ektron("#<%= ClientID %>").treeview({add: el});
                            }
                        }
                    });
                }
            }
        });
    }
});
}
-->
</script>
