﻿namespace Ektron.Workarea.Packaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Contracts.Packaging;
    using Ektron.Cms;
    using Ektron.Cms.Content;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Content;
    using Ektron.Cms.Framework.Organization;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Contracts.Packaging.Entities;
    using Ektron.Cms.Framework.Packaging;
    using Ektron.Cms.Organization;

    public partial class PackageSummary : System.Web.UI.UserControl
    {
        private int selectedLanguageID = 1033;
        protected LocalizationAPI objLocalizationApi = new LocalizationAPI();
        private SiteAPI siteAPI = new SiteAPI();
        private ContentAPI _ContentApi;
        private List<long> listofAllLangauageTaxIds = new List<long>();
        private List<long> listofAllLangauageMenuIds = new List<long>();
        public Package package { get; set; }
        public List<OrphanModelItem> orphanlist = new List<OrphanModelItem>();
        private PagingInfo PgInfo = new PagingInfo()
            {
                RecordsPerPage = 100000
            };

        protected void Page_Load(object sender, EventArgs e)
        {
            _ContentApi = new ContentAPI();
            if (!Page.IsPostBack)
            {
                this.bindData();
            }
        }

        /// <summary>
        /// Get Content Related Image Icon
        /// </summary>
        /// <param name="cdata"></param>
        /// <returns></returns>
        protected string GetImageByContentType(ContentData cdata)
        {
            var image = string.Empty;
            if (cdata.ContType.GetHashCode() == Ektron.Cms.Common.EkEnumeration.CMSContentType.Content.GetHashCode() || cdata.ContType.GetHashCode() == Ektron.Cms.Common.EkEnumeration.CMSContentType.Archive_Content.GetHashCode())
            {
                if (cdata.SubType.GetHashCode() == Ektron.Cms.Common.EkEnumeration.CMSContentSubtype.PageBuilderData.GetHashCode())
                {
                    image = "<img title=\"" + Ektron.Workarea.Packaging.PackageItemType.Content + "\" src=\"" + _ContentApi.AppImgPath + "layout_content.png" + "\" />";
                }
                else if (cdata.SubType.GetHashCode() == Ektron.Cms.Common.EkEnumeration.CMSContentSubtype.PageBuilderMasterData.GetHashCode())
                {
                    image = "<img title=\"" + Ektron.Workarea.Packaging.PackageItemType.Content + "\" src=\"" + _ContentApi.AppImgPath + "layout_content.png" + "\"  />";
                }

                else
                {
                    if (cdata.ContType.GetHashCode() == Ektron.Cms.Common.EkEnumeration.CMSContentType.Archive_Content.GetHashCode())
                    {
                        image = "<img title=\"" + Ektron.Workarea.Packaging.PackageItemType.Content + "\" src=\"" + _ContentApi.ApplicationPath + "Images/ui/icons/contentArchived.png\" />";
                    }
                    else
                    {
                        image = "<img title=\"" + Ektron.Workarea.Packaging.PackageItemType.Content + "\" src=\"" + _ContentApi.ApplicationPath + "images/ui/icons/contentHtml.png\"  />";
                    }
                }
            }
            else
            {
                image = "<span>" + cdata.AssetData.Icon + "</span>";
            }

            return image;
        }

        #region private methods

        private void bindData()
        {
            if (this.package != null)
            {
                if (package.Content != null && package.Content.Count > 0)
                {
                    ContentCriteria criteria = this.translateContentIdentifiersToCriteria(package.Content);
                    List<ContentData> data = this.getContentData(criteria);
                    if (data != null && data.Any())
                    {
                        if (package.Content.Count == data.Count)
                        {
                            orphanlist.RemoveAll(x => x.Type == PackageItemType.Content);
                        }
                        else
                        {
                            data.ForEach(s => orphanlist.RemoveAll(x => ((long)x.Id == s.Id && x.Type == PackageItemType.Content)));
                        }

                        uxPackageSummaryContent.DataSource = data;
                        uxPackageSummaryContent.DataBind();
                    }
                }

                if (package.Folders != null && package.Folders.Count > 0)
                {
                    FolderCriteria criteria = this.translateFolderIdentifiersToCriteria(package.Folders);
                    List<FolderData> data = this.getFolderData(criteria);
                    if (data != null && data.Any())
                    {
                        if (package.Content.Count == data.Count)
                        {
                            orphanlist.RemoveAll(x => x.Type == PackageItemType.Folder);
                        }
                        else
                        {
                            data.ForEach(s => orphanlist.RemoveAll(x => ((long)x.Id == s.Id && x.Type == PackageItemType.Folder)));
                        }

                        uxPackageSummaryFolder.DataSource = data;
                        uxPackageSummaryFolder.DataBind();
                    }
                }

                if (package.LibraryFiles != null && package.LibraryFiles.Count > 0)
                {
                    LibraryCriteria criteria = this.translateLibraryIdentifiersToCriteria(package.LibraryFiles);
                    List<LibraryData> data = this.getLibraryData(criteria);
                    if (data != null && data.Any())
                    {
                        if (package.Content.Count == data.Count)
                        {
                            orphanlist.RemoveAll(x => x.Type == PackageItemType.Library);
                        }
                        else
                        {
                            data.ForEach(s => orphanlist.RemoveAll(x => ((long)x.Id == s.ContentId && x.Type == PackageItemType.Library)));
                        }

                        uxPackageSummaryLibrary.DataSource = data;
                        uxPackageSummaryLibrary.DataBind();
                    }
                }

                if (package.Files != null && package.Files.Any())
                {
                    uxPackageFileSystem.DataSource = package.Files;
                    uxPackageFileSystem.DataBind();
                }

                if (package.Taxonomy != null && package.Taxonomy.Any())
                {
                    TaxonomyCriteria criteria = this.translateTaxonomyIdentifiersToCriteria(package.Taxonomy);
                    List<TaxonomyData> data = this.getTaxonomyData(criteria);
                    if (data != null && data.Any())
                    {
                        if (listofAllLangauageTaxIds != null && !listofAllLangauageTaxIds.Any() && package.Content.Count == data.Count)
                        {
                            orphanlist.RemoveAll(x => x.Type == PackageItemType.Taxonomy);
                        }
                        else
                        {
                            data.ForEach(s => orphanlist.RemoveAll(x => ((long)x.Id == s.Id) && x.Type == PackageItemType.Taxonomy));
                        }

                        uxPackageSummaryTaxonomy.DataSource = this.ModifyTaxonomySummaryData(data);
                        uxPackageSummaryTaxonomy.DataBind();
                    }
                }

                if (package.Menu != null && package.Menu.Any())
                {
                    var criteria = this.translateMenuIdentifiersToCriteria(package.Menu);
                    var data = this.getMenuData(criteria);
                    if (data != null && data.Any())
                    {
                        data = CleanMenuList(data);
                        data.ForEach(s => orphanlist.RemoveAll(x => ((long)x.Id == s.Id && x.Type == PackageItemType.Menu)));

                        uxPackageSummaryMenu.DataSource = this.ModifyMenuSummaryData(data);
                        uxPackageSummaryMenu.DataBind();
                    }
                }

                if (package.Collection != null && package.Collection.Any())
                {
                    var criteria = this.translateCollectionIdentifiersToCriteria(package.Collection);
                    var data = this.getCollectionData(criteria);
                    if (data != null && data.Any())
                    {
                        data.ForEach(s => orphanlist.RemoveAll(x => ((long)x.Id == s.Id && x.Type == PackageItemType.Collection)));

                        uxPackageSummaryCollection.DataSource = data;
                        uxPackageSummaryCollection.DataBind();
                    }
                }

                if (package.PackageDefinitions != null && package.PackageDefinitions.Any())
                {
                    PackageCriteria criteria = this.translatePackageIdentifiersToCriteria(package.PackageDefinitions);
                    List<Package> data = this.getPackageData(criteria);
                    if (data != null && data.Any())
                    {
                        data.ForEach(s => orphanlist.RemoveAll(x => x.Id.ToString().ToLower() == s.Id.ToString().ToLower() && x.Type == PackageItemType.LocalPackage));

                        uxPackageDefinition.DataSource = data;
                        uxPackageDefinition.DataBind();
                    }
                }

                if (orphanlist != null && orphanlist.Any())
                {
                    uxViewOrphanListButton.Visible = true;
                    uxOrphansection.Visible = true;
                    uxOrphanList.DataSource = orphanlist;
                    uxOrphanList.DataBind();
                }
            }
        }

        private List<TaxonomyData> ModifyTaxonomySummaryData(List<TaxonomyData> list)
        {
            if (listofAllLangauageTaxIds != null && listofAllLangauageTaxIds.Any())
            {
                foreach (TaxonomyData item in list)
                {
                    if (listofAllLangauageTaxIds.Contains(item.Id))
                    {
                        item.LanguageId = -1;
                    }
                }
                return list;
            }
            else
                return list;
        }

        private List<Ektron.Cms.Organization.MenuData> CleanMenuList(List<Ektron.Cms.Organization.MenuData> list)
        {
            // Remove Duplicate Menu's.
            var filteredmenulist = new List<Ektron.Cms.Organization.MenuData>();
            filteredmenulist = list.GroupBy(x => x.Id).Select(z => z.First()).ToList();

            return filteredmenulist;
        }

        private List<MenuModelItem> ModifyMenuSummaryData(List<Ektron.Cms.Organization.MenuData> list)
        {
            var listofMenus = new List<MenuModelItem>();
            var menumodelitems = new MenuModelItem();
            if (listofAllLangauageMenuIds != null && listofAllLangauageMenuIds.Any())
            {
                foreach (Ektron.Cms.Organization.MenuData item in list)
                {
                    menumodelitems = new MenuModelItem()
                    {
                        MenuID = item.Id,
                        LanguageID = -1,
                        Title = item.Text
                    };
                    if (listofAllLangauageMenuIds.Contains(item.Id))
                    {
                        menumodelitems.LanguageID = -1;
                    }
                    listofMenus.Add(menumodelitems);
                }
                return listofMenus;
            }
            else
            {
                foreach (Ektron.Cms.Organization.MenuData item in list)
                {
                    menumodelitems = new MenuModelItem()
                    {
                        MenuID = item.Id,
                        LanguageID = -1,
                        Title = item.Text
                    };
                    listofMenus.Add(menumodelitems);
                }
                return listofMenus;
            }
        }

        private List<ContentData> getContentData(ContentCriteria criteria)
        {
            List<ContentData> contentData = new List<ContentData>();
            ContentManager contentManager = new ContentManager();
            criteria.PagingInfo = this.PgInfo;
            contentData = contentManager.GetList(criteria);
            return contentData;
        }

        private List<FolderData> getFolderData(FolderCriteria criteria)
        {

            List<FolderData> folderData = new List<FolderData>();
            FolderManager folderManager = new FolderManager();
            criteria.PagingInfo = this.PgInfo;
            folderData = folderManager.GetList(criteria);
            return folderData;
        }

        private List<LibraryData> getLibraryData(LibraryCriteria criteria)
        {
            List<LibraryData> libraylist = new List<LibraryData>();
            LibraryManager libraryManager = new LibraryManager();
            criteria.PagingInfo = this.PgInfo;
            libraylist = libraryManager.GetList(criteria);
            return libraylist;
        }

        private List<TaxonomyData> getTaxonomyData(TaxonomyCriteria criteria)
        {
            List<TaxonomyData> list = new List<TaxonomyData>();
            TaxonomyManager taxonomyManager = new TaxonomyManager();
            criteria.PagingInfo = this.PgInfo;
            list = taxonomyManager.GetList(criteria);
            return list;
        }

        private List<Ektron.Cms.Organization.MenuData> getMenuData(MenuCriteria criteria)
        {
            var list = new List<Ektron.Cms.Organization.MenuData>();
            var menumanager = new MenuManager();
            criteria.PagingInfo = this.PgInfo;
            list = menumanager.GetMenuList(criteria);
            return list;
        }

        private List<ContentCollectionData> getCollectionData(CollectionCriteria criteria)
        {
            var list = new List<ContentCollectionData>();
            var collectionmanager = new CollectionManager();
            criteria.PagingInfo = this.PgInfo;
            list = collectionmanager.GetList(criteria);
            return list;
        }

        private List<Package> getPackageData(PackageCriteria criteria)
        {
            List<Package> packagelist = new List<Package>();
            PackageManager packageManager = new PackageManager();
            criteria.PagingInfo = this.PgInfo;
            packagelist = packageManager.GetList(criteria);
            return packagelist;
        }

        private ContentCriteria translateContentIdentifiersToCriteria(List<ContentDataIdentifier> contentItems)
        {
            ContentCriteria criteria = new ContentCriteria();
            ////criteria.PagingInfo = new PagingInfo(1000, 1);
            criteria.OrderByField = Cms.Common.ContentProperty.Title;
            criteria.OrderByDirection = Cms.Common.EkEnumeration.OrderByDirection.Ascending;
            criteria.Condition = Cms.Common.LogicalOperation.Or;

            foreach (ContentDataIdentifier contentId in contentItems)
            {
                orphanlist.Add(new OrphanModelItem() { Id = contentId.ContentID, Type = PackageItemType.Content, LanguageId = contentId.LanguageID });
                CriteriaFilterGroup<ContentProperty> group = new CriteriaFilterGroup<ContentProperty>();
                group.Condition = LogicalOperation.And;
                group.AddFilter(ContentProperty.Id, CriteriaFilterOperator.EqualTo, contentId.ContentID);
                group.AddFilter(ContentProperty.LanguageId, CriteriaFilterOperator.EqualTo, contentId.LanguageID);
                criteria.FilterGroups.Add(group);
            }
            return criteria;
        }

        private FolderCriteria translateFolderIdentifiersToCriteria(List<FolderDataIdentifier> folderItems)
        {
            FolderCriteria criteria = new FolderCriteria();
            criteria.PagingInfo = new PagingInfo(1000, 1);
            criteria.OrderByField = Cms.Common.FolderProperty.FolderName;
            criteria.OrderByDirection = Cms.Common.EkEnumeration.OrderByDirection.Ascending;
            criteria.Condition = Cms.Common.LogicalOperation.Or;

            foreach (FolderDataIdentifier folderID in folderItems)
            {
                orphanlist.Add(new OrphanModelItem() { Id = folderID.ID, Type = PackageItemType.Folder });
                CriteriaFilterGroup<FolderProperty> group = new CriteriaFilterGroup<FolderProperty>();
                group.Condition = LogicalOperation.And;
                group.AddFilter(FolderProperty.Id, CriteriaFilterOperator.EqualTo, folderID.ID);
                criteria.FilterGroups.Add(group);
            }
            return criteria;
        }

        private LibraryCriteria translateLibraryIdentifiersToCriteria(List<LibraryContentIdentifier> libraryItems)
        {
            LibraryCriteria criteria = new LibraryCriteria();
            criteria.PagingInfo = new PagingInfo(1000, 1);
            criteria.OrderByField = LibraryProperty.ContentId;
            criteria.OrderByDirection = Cms.Common.EkEnumeration.OrderByDirection.Ascending;
            criteria.Condition = Cms.Common.LogicalOperation.Or;

            foreach (LibraryContentIdentifier librarydata in libraryItems)
            {
                orphanlist.Add(new OrphanModelItem() { Id = librarydata.ContentID, Type = PackageItemType.Library, LanguageId = librarydata.LanguageID });
                CriteriaFilterGroup<LibraryProperty> group = new CriteriaFilterGroup<LibraryProperty>();
                group.Condition = LogicalOperation.And;
                group.AddFilter(LibraryProperty.ContentId, CriteriaFilterOperator.EqualTo, librarydata.ContentID);
                criteria.FilterGroups.Add(group);
            }
            return criteria;
        }

        private PackageCriteria translatePackageIdentifiersToCriteria(List<PackageDataIdentifier> PackageItems)
        {
            PackageCriteria criteria = new PackageCriteria();
            criteria.PagingInfo = new PagingInfo(1000, 1);
            criteria.Condition = Cms.Common.LogicalOperation.Or;

            foreach (PackageDataIdentifier item in PackageItems)
            {
                if (item.PackageID != package.Id)
                    orphanlist.Add(new OrphanModelItem() { Id = item.PackageID, Type = PackageItemType.LocalPackage });

                CriteriaFilterGroup<PackageProperty> group = new CriteriaFilterGroup<PackageProperty>();
                group.Condition = LogicalOperation.And;
                group.AddFilter(PackageProperty.Id, CriteriaFilterOperator.EqualTo, item.PackageID);
                group.AddFilter(PackageProperty.Id, CriteriaFilterOperator.NotEqualTo, package.Id);
                criteria.FilterGroups.Add(group);
            }

            return criteria;
        }

        private TaxonomyCriteria translateTaxonomyIdentifiersToCriteria(List<TaxonomyIdentifier> items)
        {
            TaxonomyCriteria criteria = new TaxonomyCriteria();
            criteria.PagingInfo = new PagingInfo(1000, 1);
            criteria.OrderByField = TaxonomyProperty.Name;
            criteria.OrderByDirection = Cms.Common.EkEnumeration.OrderByDirection.Ascending;
            criteria.Condition = Cms.Common.LogicalOperation.Or;

            foreach (TaxonomyIdentifier item in items)
            {
                orphanlist.Add(new OrphanModelItem() { Id = item.TaxonomyID, Type = PackageItemType.Taxonomy, LanguageId = item.LanguageID });
                CriteriaFilterGroup<TaxonomyProperty> group = new CriteriaFilterGroup<TaxonomyProperty>();
                group.Condition = LogicalOperation.And;
                group.AddFilter(TaxonomyProperty.Id, CriteriaFilterOperator.EqualTo, item.TaxonomyID);
                group.AddFilter(TaxonomyProperty.Type, CriteriaFilterOperator.EqualTo, 0);
                if (item.LanguageID != -1)
                    group.AddFilter(TaxonomyProperty.LanguageId, CriteriaFilterOperator.EqualTo, item.LanguageID);
                else
                {
                    listofAllLangauageTaxIds.Add(item.TaxonomyID);
                    group.AddFilter(TaxonomyProperty.LanguageId, CriteriaFilterOperator.EqualTo, CommonApi.Current.ContentLanguage);
                }
                criteria.FilterGroups.Add(group);
            }
            return criteria;
        }

        private MenuCriteria translateMenuIdentifiersToCriteria(List<MenuIdentifier> items)
        {
            MenuCriteria criteria = new MenuCriteria();
            criteria.PagingInfo = new PagingInfo(1000, 1);
            criteria.OrderByField = MenuProperty.LanguageId;
            criteria.OrderByDirection = Cms.Common.EkEnumeration.OrderByDirection.Ascending;
            criteria.Condition = Cms.Common.LogicalOperation.Or;

            foreach (MenuIdentifier item in items)
            {
                orphanlist.Add(new OrphanModelItem() { Id = item.MenuID, Type = PackageItemType.Menu, LanguageId = item.LanguageID });
                CriteriaFilterGroup<MenuProperty> group = new CriteriaFilterGroup<MenuProperty>();
                group.Condition = LogicalOperation.And;
                group.AddFilter(MenuProperty.Id, CriteriaFilterOperator.EqualTo, item.MenuID);
                group.AddFilter(MenuProperty.Type, CriteriaFilterOperator.EqualTo, 0);
                if (item.LanguageID != -1)
                    group.AddFilter(MenuProperty.LanguageId, CriteriaFilterOperator.EqualTo, item.LanguageID);
                else
                {
                    listofAllLangauageMenuIds.Add(item.MenuID);
                    //group.AddFilter(MenuProperty.LanguageId, CriteriaFilterOperator.EqualTo, CommonApi.Current.ContentLanguage);
                    group.AddFilter(MenuProperty.LanguageId, CriteriaFilterOperator.GreaterThan, 0);
                }
                criteria.FilterGroups.Add(group);
            }
            return criteria;
        }

        private CollectionCriteria translateCollectionIdentifiersToCriteria(List<CollectionIdentifier> items)
        {
            CollectionCriteria criteria = new CollectionCriteria();
            criteria.PagingInfo = new PagingInfo(1000, 1);
            criteria.OrderByField = ContentCollectionProperty.Title;
            criteria.OrderByDirection = Cms.Common.EkEnumeration.OrderByDirection.Ascending;
            criteria.Condition = Cms.Common.LogicalOperation.Or;

            foreach (CollectionIdentifier item in items)
            {
                orphanlist.Add(new OrphanModelItem() { Id = item.CollectionID, Type = PackageItemType.Collection, LanguageId = item.LanguageID });
                CriteriaFilterGroup<ContentCollectionProperty> group = new CriteriaFilterGroup<ContentCollectionProperty>();
                group.Condition = LogicalOperation.And;
                group.AddFilter(ContentCollectionProperty.Id, CriteriaFilterOperator.EqualTo, item.CollectionID);

                criteria.FilterGroups.Add(group);
            }
            return criteria;
        }

        #endregion

        #region Public Methods

        public string GetFileName(string filepath)
        {
            if (!string.IsNullOrEmpty(filepath))
                return filepath.Substring(filepath.LastIndexOf('/') + 1);
            return string.Empty;
        }

        public string GetValidFilePath(string filepath)
        {
            if (!string.IsNullOrEmpty(filepath))
                return filepath.Replace(".", "_").Replace(" ", "_").Replace("/", "-").Replace("&", "-").ToLower();

            return string.Empty;
        }

        public string GetTypeImage(PackageItemType package)
        {
            string imagesrc = "../images/UI/icons/content.png";
            switch (package)
            {
                case PackageItemType.Folder:
                    imagesrc = "../images/UI/icons/folder.png";
                    break;
                case PackageItemType.Content:
                    imagesrc = "../images/UI/icons/content.png";
                    break;
                case PackageItemType.Collection:
                    imagesrc = "../images/UI/icons/collection.png";
                    break;
                case PackageItemType.Menu:
                    imagesrc = "../images/UI/icons/menu.png";
                    break;
                case PackageItemType.Library:
                    imagesrc = "../images/UI/icons/bookGreen.png";
                    break;
                case PackageItemType.Files:
                    imagesrc = "../images/UI/icons/contentStack.png";
                    break;
                case PackageItemType.LocalPackage:
                    imagesrc = "../images/UI/icons/package.png";
                    break;
                case PackageItemType.Taxonomy:
                    imagesrc = "../images/UI/icons/taxonomy.png";
                    break;
            }

            return imagesrc;
        }

        public string GetLanguageImage(int languageID)
        {
            if (languageID > 0 || languageID == -1)
                return "<img src=\"" + objLocalizationApi.GetFlagUrlByLanguageID(languageID) + "\" />";

            return "";
        }

        #endregion
    }


    public class MenuModelItem
    {
        public long MenuID { get; set; }
        public int LanguageID { get; set; }
        public string Title { get; set; }
    }

    public class OrphanModelItem
    {
        public object Id { get; set; }
        public PackageItemType Type { get; set; }
        public int? LanguageId { get; set; }
    }

    public enum PackageItemType
    {
        Content = 1,
        Folder = 2,
        Library = 3,
        Menu = 4,
        Collection = 5,
        Taxonomy = 6,
        LocalPackage = 7,
        Files = 8
    }
}