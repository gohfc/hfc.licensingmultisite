﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuSelector.ascx.cs"
    Inherits="Ektron.Workarea.Packaging.MenuSelector" %>
<div class="menuSelectorWrapper clearfix">
    <ektronUI:Message ID="uxInfoMessage" runat="server" DisplayMode="Information" meta:resourcekey="uxInfoMessageResource1" />
    <asp:UpdatePanel ID="uxUpdatePanelMenu" runat="server">
        <ContentTemplate>
            <ektronUI:JavaScriptBlock ID="uxMenuJavaScriptBlock" ExecutionMode="OnEktronReady"
                runat="server">
                <ScriptTemplate>
                    Ektron.Workarea.Packaging.syncMenuSelections();
                </ScriptTemplate>
            </ektronUI:JavaScriptBlock>
            <div class="hdnFields">
                <input type="hidden" id="uxOldLanguage" runat="server" class="hdnOldLanguage" />
                <asp:Button ID="uxTriggerGetMenu" runat="server" CssClass="uxTriggerGetMenu" Style="display: none;" />
            </div>
            <div class="menuFilters">
                <asp:DropDownList ID="uxLanguageList" runat="server" CssClass="uxLanguageList" AutoPostBack="true"
                    Visible="false" OnSelectedIndexChanged="uxLanguageList_Change" />
                <span style="float: right;">
                    <ektronUI:Label ID="uxVisibleItems" runat="server" Visible="false" meta:resourcekey="uxVisibleItems"></ektronUI:Label>
                    <ektronUI:Button ID="uxSelectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.menuselectAllSummary();"
                        Visible="false" meta:resourcekey="uxSelectAllButton"></ektronUI:Button>
                    <ektronUI:Button ID="uxDeselectAllButton" runat="server" OnClientClick="return Ektron.Workarea.Packaging.menudeselectAllSummary();"
                        Visible="false" meta:resourcekey="uxDeselectAllButton"></ektronUI:Button>
                </span>
            </div>
            <div class="uxMenuLists">
                <ektronUI:GridView ID="uxMenuListsGrid" runat="server" AutoGenerateColumns="false"
                    EktronUIAllowResizableColumns="false" OnEktronUIPageChanged="uxMenuListsGrid_GridViewEktronUIThemePageChanged" Visible="true">
                    <Columns>
                        <ektronUI:CheckBoxField ID="uxSelectColumn">
                            <CheckBoxFieldHeaderStyle HeaderCheckBoxVisible="false" />
                            <CheckBoxFieldColumnStyle Visible="false" />
                        </ektronUI:CheckBoxField>
                        <asp:TemplateField meta:resourceKey="uxIncludeResource">
                            <ItemTemplate>
                                <input type="checkbox" runat="server" name="uxMenuitem" id="uxMenuitem" class="uxMenuitem"
                                    value='<%# Eval("MenuID") %>' data-ektron-id='<%# GetRowDataMenuId((long) Eval("MenuID"),(int)Eval("LanguageID")) %>'
                                    data-ektron-title='<%#  Eval("Title") %>' data-ektron-lang='<%# Eval("LanguageID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="uxMenuName">
                            <ItemTemplate>
                                <asp:Literal ID="uxItemID" runat="server" Text='<%# Eval("Title") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField meta:resourceKey="columnHeaderContentLanguage">
                            <ItemTemplate>
                                <img src='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageID")))%>'
                                    alt='<%# Eval("LanguageID") %>' title="All Language" />
                                <input type="hidden" id='uximgsrc_<%# Eval("MenuID") %>_<%# Eval("LanguageID") %>'
                                    class="uximgsrchidden" value='<%#objLocalizationApi.GetFlagUrlByLanguageID(System.Convert.ToInt32(Eval("LanguageID")))%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </ektronUI:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
