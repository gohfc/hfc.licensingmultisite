﻿namespace Ektron.Workarea.Packaging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Framework.Packaging;
    using Ektron.Cms.Contracts.Packaging;
    using Ektron.Newtonsoft.Json;
    using Ektron.Cms.Common;
    using Ektron.Cms.Contracts.Packaging.Entities;
    using Ektron.Cms.Framework.Context;

    public partial class AddEditViewPackage : Ektron.Cms.Workarea.Page
    {

        #region Local Variables

        private DisplayMode displayMode = DisplayMode.Create;
        private int pageSize = 20;
        private SiteAPI siteAPI = new SiteAPI();
        PackageManager packageManager;
        Package package;
        Guid packageId = Guid.Empty;
        #endregion
        #region Events

        protected override void OnInit(EventArgs e)
        {
            if (this.HasViewPermission())
            {
                try
                {
                    base.OnInit(e);
                    this.initWorkareaUI();
                    this.pageSize = siteAPI.RequestInformationRef.PagingSize;
                    packageManager = new PackageManager();
                    if (Request.QueryString["id"] != null)
                    {
                        Guid.TryParse(Request.QueryString["id"].ToString(), out packageId);
                        displayMode = DisplayMode.Edit;
                        package = packageManager.GetItem(packageId);
                        uxPackageSummary.package = package;
                    }
                    if (displayMode == DisplayMode.Edit)
                    {
                        uxAddHeaderLiteral.Visible = false;
                        uxEditHeaderLiteral.Visible = true;
                    }
                    else
                    {
                        uxAddHeaderLiteral.Visible = true;
                        uxEditHeaderLiteral.Visible = false;
                    }
                }
                catch (Exception exc)
                {
                    Utilities.ShowError(exc.Message);
                }
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (displayMode == DisplayMode.Create)
                {
                    package = new Package();
                    package.Content = new List<ContentDataIdentifier>();
                    package.LibraryFiles = new List<LibraryContentIdentifier>();
                    package.Files = new List<TemplateFileIdentifier>();
                    package.Taxonomy = new List<TaxonomyIdentifier>();
                    package.Menu = new List<MenuIdentifier>();
                    package.PackageDefinitions = new List<PackageDataIdentifier>();
                }
                this.bindData();
            }
        }

        protected void uxSaveButton_Click(object sender, EventArgs e)
        {
            if (package == null)
            {
                package = new Package();
            }
            package = JsonConvert.DeserializeObject<Package>(uxPackageJSON.Value);
            package = CleanPackage(package);
            package.Name = uxPackageNameValue.Text;
            package.Description = uxPackageDescriptionValue.Text;

            if (package.Id == Guid.Empty)
            {
                PackageCriteria criteria = new PackageCriteria();
                criteria.AddFilter(PackageProperty.Name, CriteriaFilterOperator.EqualTo, package.Name);
                if (this.packageManager.GetList(criteria).Count > 0)
                {
                    this.uxMessage.Visible = true;
                    this.uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    this.uxMessage.Text = GetLocalResourceObject("MessageDuplicateName").ToString();
                    return;
                }
                package = packageManager.Add(package);
            }
            else
            {
                PackageCriteria criteria = new PackageCriteria();
                criteria.Condition = LogicalOperation.And;
                criteria.AddFilter(PackageProperty.Name, CriteriaFilterOperator.EqualTo, package.Name);
                criteria.AddFilter(PackageProperty.Id, CriteriaFilterOperator.NotEqualTo, package.Id);
                if (this.packageManager.GetList(criteria).Count > 0)
                {
                    this.uxMessage.Visible = true;
                    this.uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    this.uxMessage.Text = GetLocalResourceObject("MessageDuplicateName").ToString();
                    return;
                }
                package = packageManager.Update(package);
            }
            Response.Redirect("PackageList.aspx", true);
        }

        #endregion

        #region private methods

        private Package CleanPackage(Package package)
        {
            // Clean Taxonomy Data when JSON deserialize returns incorrect data. (CASE: when OLD javascript file is cached on the Browser on Upgrade.)
            if (package.Taxonomy != null && package.Taxonomy.Any())
            {
                package.Taxonomy.Where(s => (string.IsNullOrEmpty(s.LanguageID.ToString()) || s.LanguageID != -1)).ToList().ForEach(x => x.LanguageID = -1);
            }

            // Clean Menu Data when JSON deserialize returns incorrect data.
            if (package.Menu != null && package.Menu.Any())
            {
                package.Menu.Where(s => (string.IsNullOrEmpty(s.LanguageID.ToString()) || s.LanguageID != -1)).ToList().ForEach(x => x.LanguageID = -1);
            }

            // Clean Collection Data when JSON deserialize returns incorrect data.
            if (package.Collection != null && package.Collection.Any())
            {
                package.Collection.Where(s => (string.IsNullOrEmpty(s.LanguageID.ToString()) || s.LanguageID != -1)).ToList().ForEach(x => x.LanguageID = -1);
            }

            return package;
        }

        private void bindData()
        {
            uxPackageNameValue.Text = package.Name;
            uxPackageDescriptionValue.Text = package.Description;
            uxPackageJSON.Value = JsonConvert.SerializeObject(package);
        }

        private void initWorkareaUI()
        {
            this.RegisterResources();
            ContentAPI refContentApi = new ContentAPI();
            EkMessageHelper msgHelper = refContentApi.EkMsgRef;
            StyleHelper styleHelper = new StyleHelper();
            string returnpath = refContentApi.RequestInformationRef.ApplicationPath + "Packaging/PackageList.aspx";
            uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
            aspHelpButton.Text = styleHelper.GetHelpButton("packaging_detail", string.Empty);
            uxMessage.Visible = false;
        }

        private bool HasViewPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                    ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.SyncAdmin), ContentAPI.Current.UserId, false))
                );
        }

        private void RegisterResources()
        {
            Ektron.Cms.Framework.UI.Packages.EktronCoreJS.Register(this);
            Ektron.Cms.Framework.UI.Packages.Ektron.Namespace.Register(this);
            Ektron.Cms.Framework.UI.Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.Framework.UI.Packages.Ektron.JSON.Register(this);
            Ektron.Cms.API.JS.RegisterJS(this, "js/Ektron.Workarea.Packaging.js", "PackagingJS");
            Ektron.Cms.Framework.UI.JavaScript.Register(this, CmsContextService.Current.WorkareaPath + "/java/jfunct.js");
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Ektron.Cms.API.Css.RegisterCss(this, "css/Ektron.Workarea.Packaging.css", "PackagingCSS");
        }

        #endregion
    }

    public enum DisplayMode
    {
        None = 0,
        Edit = 1,
        Create = 2
    }
}