<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetTray.ascx.cs" Inherits="pagebuilder_PlaceHolder_WidgetTray" %>
<asp:MultiView ID="uxUXSwitch" runat="server">
    <asp:View ID="uxOriginalView" runat="server">
        <asp:Repeater ID="repWidgetTypes" runat="server">
            <HeaderTemplate>
                <a href="#" class="scrollLeft" onclick="Ektron.PageBuilder.WidgetTray.scrollLeft(); return false;">&nbsp;</a>
                <a href="#" class="scrollRight" onclick="Ektron.PageBuilder.WidgetTray.scrollRight(); return false;">&nbsp;</a>
                <div id="widgetlistWrapper">
                    <ul class="ektronPersonalizationWidgetList widgetList">
            </HeaderTemplate>
            <ItemTemplate>
                    <li id="<%# (Container.DataItem as Ektron.Cms.Widget.WidgetTypeData).ID %>-Widget" title="<%#(Container.DataItem as Ektron.Cms.Widget.WidgetTypeData).Title%>" class="widgetToken">
                        <img src="<%# RequestInformation.WidgetsPath + (Container.DataItem as Ektron.Cms.Widget.WidgetTypeData).ControlURL %>.jpg" title="<%#GetWidgetDescription((Container.DataItem as Ektron.Cms.Widget.WidgetTypeData))%>" alt="<%#GetWidgetDescription((Container.DataItem as Ektron.Cms.Widget.WidgetTypeData))%>" />
                        <span><%#(Container.DataItem as Ektron.Cms.Widget.WidgetTypeData).Title%></span>
                        <input type="hidden" value="<%# (Container.DataItem as Ektron.Cms.Widget.WidgetTypeData).ID %>" />
                    </li>
            </ItemTemplate>
            <FooterTemplate>
                    </ul>
                </div>
            </FooterTemplate>
        </asp:Repeater>
    </asp:View>
    <asp:View ID="uxUXView" runat="server">
        <input type="hidden" data-ektron-pagebuilder-widget-data="true" data-ektron-widgets-path="<%= this.WidgetsPath %>" value='<%= this.WidgetData %>' />
    </asp:View>
</asp:MultiView>
