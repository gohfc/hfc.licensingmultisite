<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DropZone.ascx.cs" Inherits="Ektron.Cms.PageBuilder.Controls.UCDropZone" %>
<%@ Register Src="WidgetHost.ascx" TagPrefix="EktronUC" TagName="WidgetHost" %>

<asp:MultiView ID="uxUXSwitch" runat="server">
    <asp:View ID="uxOriginalView" runat="server">
        <asp:Literal ID="blockuiCall" runat="server" Visible="false" EnableViewState="false">
        <script type="text/javascript">
            <!--//--><![CDATA[//><!--
    Ektron.ready(function () {
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(Ektron.PageBuilder.WidgetHost.BlockUI);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(Ektron.PageBuilder.WidgetHost.unBlockUI);
    });
    //--><!]]>
        </script>
        </asp:Literal>

        <asp:UpdatePanel ID="updatepanel" runat="server" OnLoad="DropZonePanelLoad" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="dropzone PBClear" id="dzcontainer" runat="server">
                    <div class="PBDZHeader" id="dzheader" runat="server" visible="false" enableviewstate="false">
                        <a href="#" title="Lock - Set as Layout Zone" onclick="Ektron.PageBuilder.WidgetHost.setMasterDropZone(this);return false;" id="masterzoneselect" runat="server" class="masterzoneselect" visible="false">
                            <img style="border:none;" alt="" id="imgsetmasterzone" runat="server" class="PBMasterbutton PB-UI-icon" src="#" />
                        </a>
                        <asp:ImageButton ID="AddColumn" runat="server" CssClass="PBAddColumn" OnClick="AddColumn_click" />
                    </div>
                    <asp:Repeater ID="columnDisplay" runat="server">
                        <ItemTemplate>
                            <div class="PBColumn" id="zone" runat="server">
                                <ul id="column" class="columnwidgetlist" runat="server">
                                    <li class="header" id="headerItem" runat="server" enableviewstate="false">
                                        <a href="#" title="Resize Column" class="resizeColumn" onclick="Ektron.PageBuilder.WidgetHost.resizeColumn(this);return false;" runat="server" id="lbResizeColumn">
                                            <img alt="" id="imgresizecolumn" runat="server" class="PBeditbutton PB-UI-icon" src="#" />
                                        </a>
                                        <a href="#" title="Remove Column" class="remColumn" onclick="Ektron.PageBuilder.WidgetHost.RemoveColumn(this);return false;" id="lbDeleteColumn" runat="server">
                                            <img alt="" id="imgremcolumn" runat="server" class="PBclosebutton PB-UI-icon" src="#" />
                                        </a>
                                    </li>
                                    <asp:Repeater ID="controlcolumn" runat="server">
                                        <ItemTemplate>
                                            <li class="PBItem">
                                                <EktronUC:WidgetHost ID="WidgetHost" runat="server" />
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <div class="PBDropzoneError" id="PBDropZoneError" runat="server" visible="false" enableviewstate="false">

        </div>
    </asp:View>
    <asp:View ID="uxUXView" runat="server">
        <asp:UpdatePanel ID="uxUpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="uxDropZone" runat="server" data-ux-pagebuilder="DropZone">
                    <asp:Repeater ID="uxColumnDisplay" runat="server">
                        <ItemTemplate>
                            <div data-ux-pagebuilder="Column"<%# GetColumnStyle(Container) %><%# GetColumnData(Container) %>>
                                <asp:PlaceHolder Visible="<%# this.isZoneEditable %>" runat="server">
                                    <div class="ektron-ux-reset columnHeader" data-bind="event: {mouseover: mouseoverHeader, mouseout: mouseoutHeader }">
                                        <h3 class="ux-app-siteApp-columnHeader" data-bind="click: select">
                                            <span class="columnLabel">
                                                <label data-bind="text: label, attr: {title: label}"></label>
                                                <select style="display:none;" data-bind="options: positionOptions, optionsText: 'text', optionsValue: 'index', value: Index, visible: (positionOptions()).length > 1" ></select>
                                            </span>
                                            <span class="actions" data-bind="foreach: Actions">
                                                <!-- ko ifnot:Action.toLowerCase() === "movecolumn" -->
                                                <a data-bind="attr: {href: Href, title: Title, 'class': Action.toLowerCase()}, html: icon, click: fireAction">
                                                    
                                                </a>
                                                <!-- /ko -->
                                            </span>
                                        </h3>
                                    </div>
                                </asp:PlaceHolder>
                                <ul>
                                    <asp:Repeater ID="uxControlColumn" runat="server">
                                        <ItemTemplate>
                                            <li>
                                                <EktronUC:WidgetHost ID="uxWidgetHost" runat="server" />
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:View>
</asp:MultiView>