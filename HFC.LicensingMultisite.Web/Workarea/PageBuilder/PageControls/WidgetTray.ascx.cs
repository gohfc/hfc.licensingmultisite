using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Ektron.Cms;
using Ektron.Cms.Widget;
using Ektron.Cms.PageBuilder;
using Ektron.Cms.Common;
using System.Web.Script.Serialization;
using System.Web.Configuration;
using System.Collections.Specialized;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.User;


public partial class pagebuilder_PlaceHolder_WidgetTray : System.Web.UI.UserControl
{
    private WidgetTypeData[] widgetData;
    private ICmsContextService cmsContext;
    private IUser iUser;

    protected string WidgetData
    {
        get
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            return this.widgetData == null ? "[]" : json.Serialize(this.widgetData);
        }
    }
    protected bool IsUXEnabled
    {
        get
        {
            bool isUXEnabled = false;
            if (this.cmsContext == null)
            {
                this.cmsContext = ServiceFactory.CreateCmsContextService();
            }
            if (this.iUser == null)
            {
                this.iUser = ObjectFactory.GetUser();
            }
            if (this.cmsContext.IsDeviceHTML5 && cmsContext.IsToolbarEnabledForTemplate && iUser.IsLoggedIn && iUser.IsCmsUser)
            {
                isUXEnabled = true;
            }
            return isUXEnabled;
        }
    }
    protected string WidgetsPath
    {
        get
        {
            return this.RequestInformation.WidgetsPath;
        }
    }
    protected EkRequestInformation requestInformation = null;
    protected EkRequestInformation RequestInformation
    {
        get
        {
            if (requestInformation == null)
            {
                requestInformation = ObjectFactory.GetRequestInfoProvider().GetRequestInformation();
            }
            return requestInformation;
        }
    }
    //private SiteAPI __siteapi = null;
    //protected SiteAPI _siteApi
    //{
    //    get
    //    {
    //        if (__siteapi == null) __siteapi = new SiteAPI();
    //        return __siteapi;
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        long wireframeid = 0;
        PageBuilder page = (Page as PageBuilder);
        if (page != null && page.Pagedata != null && page.Pagedata.pageID != 0 && page.Status != Ektron.Cms.PageBuilder.Mode.AnonViewing)
        {
            IWireframeModel wfm = ObjectFactory.GetWireframeModel();
            WireframeData wfd = wfm.FindByPageID(page.Pagedata.pageID);
            if (wfd != null && wfd.ID != 0)
            {
                wireframeid = wfd.ID;
                WidgetTypeData[] wtd = wfm.GetAssociatedWidgetTypes(wireframeid);
                if (wtd != null && wtd.Length > 0)
                {
                    if (this.IsUXEnabled)
                    {
                        this.widgetData = GetWidgetList(wtd);
                    }
                    else
                    {
                        repWidgetTypes.DataSource = GetWidgetList(wtd);
                        repWidgetTypes.DataBind();
                    }
                }
            }
        }
    }
    protected override void OnPreRender(EventArgs e)
    {
        this.uxUXSwitch.SetActiveView(this.IsUXEnabled ? uxUXView : uxOriginalView);
        base.OnPreRender(e);
    }

    protected string GetWidgetName(Ektron.Cms.Widget.WidgetTypeData widget)
    {
        return GetWidgetName(widget.Title, widget.LocalizedName);
    }
    protected string GetWidgetDescription(Ektron.Cms.Widget.WidgetTypeData widget)
    {
        return GetWidgetDescription(widget.Title, widget.LocalizedDescription);
    }
    private string GetWidgetName(string widgetTitle, string nameResource)
    {
        string widgetName = widgetTitle;
        if (!string.IsNullOrEmpty(nameResource))
        {
            widgetName = nameResource;
        }
        return widgetName;
    }
    private string GetWidgetDescription(string widgetTitle, string descResource)
    {
        string widgetDesc = widgetTitle;
        if (!string.IsNullOrEmpty(descResource))
        {
            widgetDesc = descResource;
        }
        return widgetDesc;
    }
    private WidgetTypeData[] GetWidgetList(WidgetTypeData[] wireframeWidgets)
    {
        List<WidgetTypeData> widgetList = new List<WidgetTypeData>();
        widgetList.AddRange(wireframeWidgets);
        widgetList.ForEach(x => x.Title = GetWidgetName(x));
        widgetList.Sort(delegate(WidgetTypeData w1, WidgetTypeData w2) { return w1.Title.CompareTo(w2.Title); });
        return widgetList.ToArray();
    }
}
