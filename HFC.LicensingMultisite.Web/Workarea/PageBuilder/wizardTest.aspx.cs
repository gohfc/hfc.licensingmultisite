using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Framework.UI;


public partial class Workarea_PageBuilder_wizardTest : Ektron.Cms.Workarea.Page
{
    #region Protected Variables ===========================

    protected string AppImgPath = "";
    protected int ContentLanguage = -1;
    protected long CurrentUserID = -1;
    protected SiteAPI m_refSiteApi = new SiteAPI();
    protected StyleHelper m_refStyle = new StyleHelper();
    protected Ektron.Cms.Common.EkMessageHelper m_refMsg;

    #endregion

    protected void Page_Load(object sender, System.EventArgs e)
    {
        m_refMsg = m_refSiteApi.EkMsgRef;
		Utilities.ValidateUserLogin();
        if (m_refSiteApi.RequestInformationRef.IsMembershipUser == 1 || m_refSiteApi.RequestInformationRef.UserId == 0)
        {
            Response.Redirect(m_refSiteApi.ApplicationPath + "reterror.aspx?info=" + Server.UrlEncode(m_refMsg.GetMessage("msg login cms user")), false);
            return;
        }
        // Register JS
        Packages.EktronCoreJS.Register(this);
        Packages.EktronCoreJS.Register(this);
        Packages.Ektron.Xml.Register(this);
        Packages.jQuery.jQueryUI.Dialog.Register(this);
        Packages.jQuery.jQueryUI.Draggable.Register(this);

        JS.RegisterJS(this, m_refSiteApi.AppPath + "PageBuilder/Wizards/js/ektron.pagebuilder.wizards.js", "EktronPageBuilderWizardsJS");
        JS.RegisterJS(this, m_refSiteApi.AppPath + "PageBuilder/Wizards/js/wizardResources.aspx", "EktronPageBuilderWizardResourcesJS");

        // register necessary CSS
        Packages.jQuery.jQueryUI.ThemeRoller.Register(this);
        //Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronModalCss);
        Ektron.Cms.API.Css.RegisterCss(this, m_refSiteApi.AppPath + "PageBuilder/Wizards/css/ektron.pagebuilder.wizards.css", "EktronPageBuilderWizardsCSS");
    }
}


