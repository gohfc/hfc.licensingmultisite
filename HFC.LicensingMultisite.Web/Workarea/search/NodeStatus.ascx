﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeStatus.ascx.cs" Inherits="NodeStatus" %>

<ektronUI:Tabs id="uxSolrNodeStatusTabs" runat="server">

     <ektronUI:Tab ID="uxNodeCrawlStatus" runat="server">
        <ContentTemplate>
            <div id="divErrorMessages" class="divErrorMessages" runat="server">
                <table class="ektronGrid errorinfo">
                    <tr class="title-header">
                        <th><%=this.Sites.EkMsgRef.GetMessage("generic error")%></th>
                        <th><%=this.Sites.EkMsgRef.GetMessage("generic error information msg")%></th>
                    </tr>
                    <tr>
                        <td class="errormessage"><asp:Literal ID="ucErrorMessage" runat="server"></asp:Literal></td>
                        <td class="errordescription"><asp:Literal ID="ucErrorDescription" runat="server"></asp:Literal>
                        <span>
                            <a title="<%=this.Sites.EkMsgRef.GetMessage("alt click here for additional details")%>" href="#" onclick="ToggleErrorDetails(); return false;">
                                <img class="toggleerrordetails" id="searchStatusDetails" alt="<%=this.Sites.EkMsgRef.GetMessage("alt click here for additional details")%>" 
                                src="../images/UI/Icons/information.png" />
                            </a>
                        </span>
                        <div class="errordetails divHidden"><asp:Literal ID="ucErrorDetails" runat="server"></asp:Literal></div></td>
                    </tr>
                </table>
            </div>

            <div id="divStatusInfo" runat="server">
                <table class="ektronGrid crawlTypes">
                    <tr class="title-header">
                        <th><ektronUI:Infotip ID="infoCrawlStatus" PositionMy="left center" PositionAt="right center" runat="server"></ektronUI:Infotip></th>
                        <th id="contentColumn" runat="server"><%=this.Sites.EkMsgRef.GetMessage("generic content")%></th>
                        <th id="usersColumn" runat="server"><%=this.Sites.EkMsgRef.GetMessage("generic users")%></th>
                        <th id="cgroupsColumn" runat="server"><%=this.Sites.EkMsgRef.GetMessage("lbl community groups")%></th>
                        <th><%=this.Sites.EkMsgRef.GetMessage("lbl description")%></th>
                    </tr>
                    <tr>
                        <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl crawl types")%></td>
                        <td class="value"><asp:Literal ID="ucManifoldStatus" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucManifoldStatusUsers" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucManifoldStatusGroups" runat="server"></asp:Literal></td>
                        <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl manifold status description")%></td>
                    </tr>
                    <tr>
                        <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Current Action")%></td>
                        <td class="value"><asp:Literal ID="ucCurrentAction" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucCurrentActionUsers" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucCurrentActionGroups" runat="server"></asp:Literal></td>
                        <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl indexing request")%></td>
                    </tr>
                    <tr>
                        <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Next Action")%></td>
                        <td class="value"><asp:Literal ID="ucPendingAction" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucPendingActionUsers" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucPendingActionGroups" runat="server"></asp:Literal></td>
                        <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl indexing completion")%></td>
                    </tr>
                    <tr>
                        <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Crawl Start Time")%></td>
                        <td class="value"><asp:Literal ID="ucCrawlStartTime" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucCrawlStartTimeUsers" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucCrawlStartTimeGroups" runat="server"></asp:Literal></td>
                        <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl start recent crawl")%></td>
                    </tr>
                    <tr>
                        <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Crawl End Time")%></td>
                        <td class="value"><asp:Literal ID="ucCrawlEndTime" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucCrawlEndTimeUsers" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucCrawlEndTimeGroups" runat="server"></asp:Literal></td>
                        <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl end recent crawl")%></td>
                    </tr>
                    <tr>
                        <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Crawl Duration")%></td>
                        <td class="value"><asp:Literal ID="ucCrawlDuration" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucCrawlDurationUsers" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucCrawlDurationGroups" runat="server"></asp:Literal></td>
                        <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl duration recent crawl")%></td>
                    </tr>
                    <tr>
                        <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Incremental Crawl Request Pending")%></td>
                        <td class="value"><asp:Literal ID="ucIsCrawlSchedule" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucIsCrawlScheduleUsers" runat="server"></asp:Literal></td>
                        <td class="value"><asp:Literal ID="ucIsCrawlScheduleGroups" runat="server"></asp:Literal></td>
                        <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl incremental crawl submit")%></td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </ektronUI:Tab>

    <ektronUI:Tab ID="uxNodeProcessStatus" runat="server">
        <ContentTemplate>
            <div id="divProcessStatusInfo" class="processStatusInfo" runat="server">

                <div id="divProcessStatusErrorMessages" class="divErrorMessages" runat="server">
                    <table class="ektronGrid errorinfo">
                        <tr class="title-header">
                            <th><%=this.Sites.EkMsgRef.GetMessage("generic error")%></th>
                            <th><%=this.Sites.EkMsgRef.GetMessage("generic error information msg")%></th>
                        </tr>
                        <tr>
                            <td class="errormessage"><asp:Literal ID="ucProcessStatusErrorMessage" runat="server"></asp:Literal></td>
                            <td class="errordescription"><asp:Literal ID="ucProcessStatusErrorDescription" runat="server"></asp:Literal>
                            <span>
                                <a title="<%=this.Sites.EkMsgRef.GetMessage("alt click here for additional details")%>" href="#" onclick="ToggleErrorDetails(); return false;">
                                    <img class="toggleerrordetails" id="Img1" alt="<%=this.Sites.EkMsgRef.GetMessage("alt click here for additional details")%>" 
                                    src="../images/UI/Icons/information.png" />
                                </a>
                            </span>
                            <div class="errordetails divHidden"><asp:Literal ID="ucProcessStatusErrorDetails" runat="server"></asp:Literal></div></td>
                        </tr>
                    </table>
                </div>

                <div id="divProcessInformation" runat="server">
                    <asp:ListView ID="lstProcessState" runat="server" 
                                    OnDataBound="processState_OnDataBound"  
                                    OnItemDataBound="processState_OnItemDataBound" 
                                    ItemPlaceholderID="aspPlaceholder">
                        <LayoutTemplate>
                            <table class="ektronGrid crawlTypes">
                                <tr class="title-header">
                                    <th><ektronUI:Infotip ID="infoProcessStatus" PositionMy="left center" PositionAt="right center" runat="server"></ektronUI:Infotip></th>
                                    <th><asp:Literal ID="ucActivityLabel" runat="server"></asp:Literal></th>
                                    <th><asp:Literal ID="ucIsRunningLabel" runat="server"></asp:Literal></th>
                                </tr>
                                <asp:PlaceHolder ID="aspPlaceholder" runat="server"></asp:PlaceHolder>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="header label"><asp:Literal ID="ucName" runat="server"></asp:Literal></td>
                                <td class="value"><asp:Literal ID="ucActivity" runat="server"></asp:Literal></td>
                                <td class="value"><span id="spanIsRunning" runat="server"></span></td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>
        </ContentTemplate>
    </ektronUI:Tab>
</ektronUI:Tabs>