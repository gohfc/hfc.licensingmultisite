﻿using System;
using System.Web.UI.HtmlControls;

using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Search;
using Ektron.Cms.Search.Proxies.SearchRequestService;
using Ektron.Cms.Search.Contracts.Solr;
using Ektron.Cms.Search.Contracts;
using Ektron.Cms.Framework.UI.Controls.EktronUI;
using System.Web.UI.WebControls;

public partial class NodeStatus : System.Web.UI.UserControl
{
    private SiteAPI _site;
    private StyleHelper _style;

    private readonly DateTime MinDate = new DateTime(1899, 12, 30, 0, 0, 0);
    
    public SiteAPI Sites
    {
        get
        {
            if (_site == null) _site = new SiteAPI();
            return _site;
        }
    }

    public SolrNodeStatus SolrNodeStatus { get; set; }

    public SearchSettingsData SearchSettingsData { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        divErrorMessages.Visible = false;

        uxNodeProcessStatus.Text = this.Sites.EkMsgRef.GetMessage("lbl process status"); 
        uxNodeCrawlStatus.Text = this.Sites.EkMsgRef.GetMessage("lbl crawl types");
        uxSolrNodeStatusTabs.SetActiveTab(0);

        DisplayCrawlStatus();

        DisplayProcessStatus();

    }

    private void DisplayCrawlStatus()
    {
        SolrNodeState coreState = SolrNodeStatus.SolrNodeState;

        if (coreState != null && coreState.SolrNodeStatusResponse.Success)
        {
            //Content Crawl
            if (coreState.ContentJobState.CrawlStartTime <= DateTime.MinValue)
                ucCrawlStartTime.Text = "-";
            else
                ucCrawlStartTime.Text = GetTimeString(coreState.ContentJobState.CrawlStartTime);
            if (coreState.ContentJobState.CrawlEndTime <= DateTime.MinValue)
                ucCrawlEndTime.Text = "-";
            else
                ucCrawlEndTime.Text = GetTimeString(coreState.ContentJobState.CrawlEndTime);

            if (IsValidCrawlDuration(coreState.ContentJobState.CrawlDuration))
                ucCrawlDuration.Text = coreState.ContentJobState.CrawlDuration.ToString("hh\\:mm\\:ss");
            else
                ucCrawlDuration.Text = "-";

            ucIsCrawlSchedule.Text = coreState.ContentJobState.IsCrawlScheduled ? this.Sites.EkMsgRef.GetMessage("generic yes") : this.Sites.EkMsgRef.GetMessage("generic no");
            ucManifoldStatus.Text = GetLocalizedString(coreState.ContentJobState.JobStatus);
            ucCurrentAction.Text = GetLocalizedString(coreState.ContentJobState.CurrentAction);
            ucPendingAction.Text = GetLocalizedString(coreState.ContentJobState.PendingAction);


            //User Crawl
            ucCrawlStartTimeUsers.Text = GetTimeString(coreState.UserJobState.CrawlStartTime);
            if (coreState.UserJobState.CrawlStartTime <= DateTime.MinValue)
                ucCrawlStartTimeUsers.Text = "-";
            else
                ucCrawlStartTimeUsers.Text = GetTimeString(coreState.UserJobState.CrawlStartTime);
            if (coreState.UserJobState.CrawlEndTime <= DateTime.MinValue)
                ucCrawlEndTimeUsers.Text = "-";
            else
                ucCrawlEndTimeUsers.Text = GetTimeString(coreState.UserJobState.CrawlEndTime);

            if (IsValidCrawlDuration(coreState.UserJobState.CrawlDuration))
                ucCrawlDurationUsers.Text = coreState.UserJobState.CrawlDuration.ToString("hh\\:mm\\:ss");
            else
                ucCrawlDurationUsers.Text = "-";

            ucIsCrawlScheduleUsers.Text = coreState.UserJobState.IsCrawlScheduled ? this.Sites.EkMsgRef.GetMessage("generic yes") : this.Sites.EkMsgRef.GetMessage("generic no");
            ucManifoldStatusUsers.Text = GetLocalizedString(coreState.UserJobState.JobStatus);
            ucCurrentActionUsers.Text = GetLocalizedString(coreState.UserJobState.CurrentAction);
            ucPendingActionUsers.Text = GetLocalizedString(coreState.UserJobState.PendingAction);


            //Community Groups Crawl
            if (coreState.CommunityGroupJobState.CrawlStartTime <= DateTime.MinValue)
                ucCrawlStartTimeGroups.Text = "-";
            else
                ucCrawlStartTimeGroups.Text = GetTimeString(coreState.CommunityGroupJobState.CrawlStartTime);
            if (coreState.CommunityGroupJobState.CrawlEndTime <= DateTime.MinValue)
                ucCrawlEndTimeGroups.Text = "-";
            else
                ucCrawlEndTimeGroups.Text = GetTimeString(coreState.CommunityGroupJobState.CrawlEndTime);

            if (IsValidCrawlDuration(coreState.CommunityGroupJobState.CrawlDuration))
                ucCrawlDurationGroups.Text = coreState.CommunityGroupJobState.CrawlDuration.ToString("hh\\:mm\\:ss");
            else
                ucCrawlDurationGroups.Text = "-";

            ucIsCrawlScheduleGroups.Text = coreState.CommunityGroupJobState.IsCrawlScheduled ? this.Sites.EkMsgRef.GetMessage("generic yes") : this.Sites.EkMsgRef.GetMessage("generic no");
            ucManifoldStatusGroups.Text = GetLocalizedString(coreState.CommunityGroupJobState.JobStatus);
            ucCurrentActionGroups.Text = GetLocalizedString(coreState.CommunityGroupJobState.CurrentAction);
            ucPendingActionGroups.Text = GetLocalizedString(coreState.CommunityGroupJobState.PendingAction);

            contentColumn.Attributes.Add("title", this.Sites.EkMsgRef.GetMessage("lbl solr content job desc"));
            usersColumn.Attributes.Add("title", this.Sites.EkMsgRef.GetMessage("lbl solr users job desc"));
            cgroupsColumn.Attributes.Add("title", this.Sites.EkMsgRef.GetMessage("lbl solr cgroups job desc"));

            infoCrawlStatus.Text = String.Format(this.Sites.EkMsgRef.GetMessage("lbl solr crawlstatus info"),
                                                coreState.Host,
                                                SolrNodeStatus.SolrNodeState.ManifoldEndpoint);  
        }
        else
        {
            divErrorMessages.Visible = true;
            divStatusInfo.Visible = false;

            if (coreState == null) 
            {
                ucErrorMessage.Text = this.Sites.EkMsgRef.GetMessage("lbl solr corestatus null response");
                string adminServiceUrl = (SearchSettingsData != null ? SearchSettingsData.AdminService : String.Empty);
                ucErrorDescription.Text = String.Format(this.Sites.EkMsgRef.GetMessage("lbl solr corestatus null response desc"), adminServiceUrl);
                ucErrorDetails.Text = this.Sites.EkMsgRef.GetMessage("lbl solr getstatus null response details");
            }
            else
            {
                ucErrorMessage.Text = this.Sites.EkMsgRef.GetMessage("lbl solr corestatus error");
                ucErrorDescription.Text = GetErrorMessage(coreState.SolrNodeStatusResponse.ErrorCode);
                foreach (string s in coreState.SolrNodeStatusResponse.Details)
                    ucErrorDetails.Text += s + "<br />";
            }
        }
    }

    private void DisplayProcessStatus()
    {
        if (SolrNodeStatus.SolrProcessState != null &&
            SolrNodeStatus.SolrProcessState.SolrProcessStatusResponse != null &&
            SolrNodeStatus.SolrProcessState.SolrProcessStatusResponse.Success &&
            SolrNodeStatus.SolrProcessState.ProcessState != null &&
            SolrNodeStatus.SolrProcessState.ProcessState.Count > 0)
        {
            lstProcessState.DataSource = SolrNodeStatus.SolrProcessState.ProcessState;
            lstProcessState.DataBind();

            divProcessStatusErrorMessages.Visible = false;
        }
        else
        {
            lstProcessState.Visible = false;
            divProcessStatusErrorMessages.Visible = true;

            if (SolrNodeStatus.SolrProcessState.SolrProcessStatusResponse != null &&
                SolrNodeStatus.SolrProcessState.SolrProcessStatusResponse.Success == false &&
                !String.IsNullOrWhiteSpace(SolrNodeStatus.SolrProcessState.SolrProcessStatusResponse.ErrorCode))
            {
                ucProcessStatusErrorMessage.Text = this.Sites.EkMsgRef.GetMessage("lbl solr getprocessstatus error");
                ucProcessStatusErrorDescription.Text =
                    String.Format(GetProcessStatusErrorMessage(SolrNodeStatus.SolrProcessState.SolrProcessStatusResponse.ErrorCode), SolrNodeStatus.SolrProcessState.Endpoint);
                foreach (string s in SolrNodeStatus.SolrProcessState.SolrProcessStatusResponse.Details)
                    ucProcessStatusErrorDetails.Text += s + "<br />";
            }
            else
            {
                ucProcessStatusErrorMessage.Text = this.Sites.EkMsgRef.GetMessage("lbl solr processstatus no response");
                string processEndpoint = (!String.IsNullOrWhiteSpace(SolrNodeStatus.SolrProcessState.Endpoint) ? SolrNodeStatus.SolrProcessState.Endpoint : String.Empty);
                ucProcessStatusErrorDescription.Text = String.Format(this.Sites.EkMsgRef.GetMessage("lbl solr processstatus no response desc"), processEndpoint);
                ucProcessStatusErrorDetails.Text = this.Sites.EkMsgRef.GetMessage("lbl solr processstatus no response details");
                
            }
        }
    }

    private string GetLocalizedString(JobStatus status)
    {
        string resource_prefix = "lbl manifold jobstatus ";
        string result = status.ToString();
        switch (status)
        {
            case JobStatus.Unknown:
            case JobStatus.RunningNoConnector:
            case JobStatus.Restarting:
            case JobStatus.Cleaningup:
            case JobStatus.StartingUp:
            case JobStatus.NotYetRun:
                result = this.Sites.EkMsgRef.GetMessage(resource_prefix + status.ToString().ToLower());
                break;
            default:
                result = status.ToString();
                break;
        }
        return result;
    }

    private string GetLocalizedString(JobAction action)
    {
        string result = action.ToString();
        switch (action)
        {
            case JobAction.IncrementalCrawl:
                result = this.Sites.EkMsgRef.GetMessage("lbl Incremental Crawl");
                break;

            case JobAction.FullCrawl:
                result = this.Sites.EkMsgRef.GetMessage("lbl Full Crawl");
                break;
            default:
                result = action.ToString();
                break;
        }
        return result;
    }

    private bool IsValidCrawlDuration(TimeSpan span)
    {
        TimeSpan none = new TimeSpan();
        return (span > none);
    }


    private string GetTimeString(DateTime input)
    {
        string timeString;
        if (input.Equals(MinDate))
        {
            timeString = this.Sites.EkMsgRef.GetMessage("generic NA");
        }
        else
        {
            timeString = input.ToString();
        }

        return timeString;
    }

    private string GetErrorMessage(string errorCode)
    {
        string result = String.Empty;
        string resource_prefix = "lbl solr admin svc ex ";
        switch (errorCode)
        {
            case SolrNodeStatusResponseCode.ConfigurationException:
            case SolrNodeStatusResponseCode.ConfigurationNotFoundException:
            case SolrNodeStatusResponseCode.SettingsNotFoundException:
            case SolrNodeStatusResponseCode.JobNotFoundException:
            case SolrNodeStatusResponseCode.ManifoldResponseException:
            case SolrNodeStatusResponseCode.GenericException:
            case SolrNodeStatusResponseCode.PeerAdminService_NoResponseException:
            case SolrNodeStatusResponseCode.PeerAdminService_NotFoundException:
            case SolrNodeStatusResponseCode.PeerAdminService_RequestTimeout:
            case SolrNodeStatusResponseCode.PeerAdminService_UnauthorizedException:
            case SolrNodeStatusResponseCode.PeerAdminService_UnknownException:
                result = this.Sites.EkMsgRef.GetMessage(resource_prefix + errorCode.ToLower());
                break;
            default:
                result = this.Sites.EkMsgRef.GetMessage("lbl solr admin svc ex unhandled_error");
                break;
        }
        return result;
    }

    private string GetProcessStatusErrorMessage(string errorCode)
    {
        string result = String.Empty;
        string resource_prefix = "lbl solr process status ex ";
        switch (errorCode)
        {
            case SolrProcessStatusResponseCode.NoResponseException:
            case SolrProcessStatusResponseCode.NotFoundException:
            case SolrProcessStatusResponseCode.RequestTimeout:
            case SolrProcessStatusResponseCode.UnauthorizedException:
            case SolrProcessStatusResponseCode.UnknownException: 
                result = this.Sites.EkMsgRef.GetMessage(resource_prefix + errorCode.ToLower());
                break;
            default:
                result = this.Sites.EkMsgRef.GetMessage("lbl solr process status ex unhandled_error");
                break;
        }
        return result;
    }

    protected void processState_OnDataBound(object sender, EventArgs e)
    {
        Literal l = (lstProcessState.FindControl("ucActivityLabel") as Literal);
        if (l != null) 
            l.Text = this.Sites.EkMsgRef.GetMessage("lbl solr process activity");

        l = (lstProcessState.FindControl("ucIsRunningLabel") as Literal);
        if (l != null) 
            l.Text = this.Sites.EkMsgRef.GetMessage("lbl solr process isrunning");

        Infotip i = (lstProcessState.FindControl("infoProcessStatus") as Infotip);
        if (i != null)
            i.Text = String.Format(this.Sites.EkMsgRef.GetMessage("lbl solr processstatus info"),
                SolrNodeStatus.SolrProcessState.Endpoint);  

    }

    protected void processState_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ProcessState item = e.Item.DataItem as ProcessState;
            if (item != null)
            {
                Literal label = e.Item.FindControl("ucName") as Literal;
                if (label != null)
                {
                    if (item.Name.ToLower().Contains("hsqldb"))
                        label.Text = this.Sites.EkMsgRef.GetMessage("lbl " + item.Name.Trim().ToLower());
                    else
                        label.Text = item.Name;
                }

                label = e.Item.FindControl("ucActivity") as Literal;
                if (label != null)
                    label.Text = item.Activity;

                HtmlGenericControl span = e.Item.FindControl("spanIsRunning") as HtmlGenericControl;
                if (span != null)
                {
                    span.InnerText = (item.IsRunning ? 
                                this.Sites.EkMsgRef.GetMessage("lbl yes") : this.Sites.EkMsgRef.GetMessage("lbl no"));
                    if (!item.IsRunning)
                        span.Attributes.Add("class", "errormessage");
                }

            }
        }
    }
}