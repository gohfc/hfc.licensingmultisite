﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Linq; 

using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Search;
using Ektron.Cms.Search.Proxies.SearchRequestService;
using Ektron.Cms.Search.Contracts.Solr;
using Ektron.Cms.Search.Contracts;
using Ektron.Cms.Framework.UI.Controls.EktronUI;

public partial class Workarea_solrsearch_status : Ektron.Cms.Workarea.Page
{
    private ISearchSettings searchSettingsProvider;
    private SearchSettingsData searchSettingsData;

    private SiteAPI _site;
    private StyleHelper _style;

    public SiteAPI Sites
    {
        get
        {
            if (_site == null) _site = new SiteAPI();
            return _site;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (HasPermission())
        {
            _style = new StyleHelper();

            JS.RegisterJS(this, JS.ManagedScript.EktronStyleHelperJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronJFunctJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaHelperJS);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaIeCss, Ektron.Cms.API.Css.BrowserTarget.LessThanEqualToIE8);

            styleHelper.Text = _style.GetClientScript();
            JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaContextMenusJS);
    
            RenderToolbar();
            RenderTitleBar();

            try
            {
                searchSettingsProvider = ObjectFactory.GetSearchSettings();
                if (searchSettingsProvider == null)
                    throw new Exception(Sites.EkMsgRef.GetMessage("msg solr search status connection error"));

                searchSettingsData = searchSettingsProvider.GetItem();

                if (searchSettingsData == null)
                    throw new Exception(Sites.EkMsgRef.GetMessage("msg solr search status connection error"));
                
                RenderSettings();
            }
            catch (NullReferenceException)
            {
                Utilities.ShowError(Sites.EkMsgRef.GetMessage("msg solr search status connection error"));
            }
            catch (Exception ex)
            {
                Utilities.ShowError(ex.Message);
            }
        }
        else
        {
            Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
        }
    }

    private void RenderSettings()
    {
        Uri adminUri = new Uri(searchSettingsData.AdminService);
        ucSolrServerHost.Text = adminUri.Host;

        ucSolrAdminUrl.Text = searchSettingsData.AdminService;
        ucSolrQueryUrl.Text = searchSettingsData.QueryService;
        anchorSolrQueryUrl.HRef = searchSettingsData.QueryService;
        ucCoreName.Text = searchSettingsData.ContentSource;

        string username = string.Empty;
        if (!string.IsNullOrWhiteSpace(searchSettingsData.Provider.AdminCredentials.Domain))
        {
            ucUsername.Text = searchSettingsData.Provider.AdminCredentials.Domain + "\\";
        }

        ucUsername.Text += searchSettingsData.Provider.AdminCredentials.Username;

        ucCrawlInterval.Text = searchSettingsData.Interval.ToString();

        ucIncludeHtmlContent.Text = GetFilterString(searchSettingsData.Filter.IncludeHtmlContent);
        ucIncludeDocumentContent.Text = GetFilterString(searchSettingsData.Filter.IncludeDocuments);
        ucIncludeForumContent.Text = GetFilterString(searchSettingsData.Filter.IncludeForums);
        ucIncludeProductContent.Text = GetFilterString(searchSettingsData.Filter.IncludeProducts);
        ucIncludeCommunityMembers.Text = GetFilterString(searchSettingsData.Filter.IncludeCommunityMembers);
        ucIncludeCommunityContent.Text = GetFilterString(searchSettingsData.Filter.IncludeCommunityContent);
    }

    private void RenderTitleBar()
    {
        txtTitleBar.InnerHtml = _style.GetTitleBar(this.Sites.EkMsgRef.GetMessage("lbl search config"));
    }

    private void RenderToolbar()
    {
        tdSearchStatusHelpButton.InnerHtml = _style.GetHelpButton("search_config", "");
    }

    public string GetFilterString(bool input)
    {
        return input ? this.Sites.EkMsgRef.GetMessage("generic Included") : this.Sites.EkMsgRef.GetMessage("generic Excluded");
    }

    /// <summary>
    /// Returns true if the current user has sufficient permissions to access
    /// the functionality on this page, false otherwise.
    /// </summary>
    /// <returns>True if permissions are sufficient, false otherwise</returns>
    private static bool HasPermission()
    {
        return
            !((Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) ||
            ContentAPI.Current.RequestInformationRef.UserId == 0 ||
            !ContentAPI.Current.EkUserRef.IsAllowed(ContentAPI.Current.UserId, 0, "users", "IsAdmin", 0)) &&
            !(ContentAPI.Current.IsARoleMember((long)EkEnumeration.CmsRoleIds.SearchAdmin, ContentAPI.Current.UserId, false)));
    }
}
