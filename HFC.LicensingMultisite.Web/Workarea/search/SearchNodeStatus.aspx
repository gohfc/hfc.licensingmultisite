﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchNodeStatus.aspx.cs" Inherits="Workarea_SearchNodeStatus" %>
<%@ Reference Control="NodeStatus.ascx" %>

<%@ Import Namespace="Ektron.Cms.Framework.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Status</title>
    <asp:Literal ID="styleHelper" runat="server"></asp:Literal>

    <style type="text/css">
        div.solrstatus .divVisible {display:block;}
        div.solrstatus .divHidden {display:none;}

        div.sectionHeader { font-weight: bold; color: #1d5987; }
        table.ektronGrid { margin-bottom: 15px; }
        
        td.header.label {width: 150px; text-align: left;}
        td.value { width: 200px; }
        .description { color: #888888; text-align: left; font-weight: normal;}

        div.divErrorMessages td.errormessage, div.processStatusInfo span.errormessage { width: 30%; text-align: left; color:#DD2222; font-weight: bold;}
        div.divErrorMessages td.errordescription { width: 70%; }
        div.divErrorMessages div.errordetails { color: #D97272; }
        
        table.crawlTypes td.header.label {width: 10%; text-align: left;}
        table.crawlTypes td.description {width: 30%; text-align: left; color: #888888; text-align: left; font-weight: normal;}
        table.crawlTypes td.value {width: 20%;}

        div.sectionFooter { margin-bottom: 15px; }
    </style>

    <script type="text/javascript">

        function crawlIncremental() {
            var result = confirm('<asp:Literal ID="ltrincremental" runat="server"></asp:Literal>');
            if (result) {
                alert('<asp:Literal ID="ltrincrementalRequest" runat="server"></asp:Literal>');
                document.location = "SearchNodeStatus.aspx?action=incremental";
            }
        }

        function crawlFull() {
            var result = confirm('<asp:Literal ID="ltrFullC" runat="server"></asp:Literal>');
            if (result) {
                alert('<asp:Literal ID="ltrFullRequest" runat="server"></asp:Literal>');
                document.location = "SearchNodeStatus.aspx?action=full";
            }
        }

        function ToggleErrorDetails() {
            var divDetails = $ektron("div.errordetails");
            if (divDetails.length > 0) 
            {
                if (divDetails.hasClass("divHidden"))
                {
                    divDetails.removeClass("divHidden");
                    divDetails.addClass("divVisible");
                }
                else
                {
                    divDetails.removeClass("divVisible");
                    divDetails.addClass("divHidden");
                }
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="solrstatus">
            <div id="dhtmltooltip"></div>
            <div class="ektronPageHeader">
                <div class="ektronTitlebar" id="txtTitleBar" runat="server"></div>
                <div class="ektronToolbar" id="htmToolBar" runat="server">
                    <table>
                        <tr>
                            <td id="tdIncrementalCrawlButton" runat="server"></td>
                            <td id="tdFullCrawlButton" runat="server"></td>
                            <td id="tdRefreshButton" runat="server"></td>
						    <%=StyleHelper.ActionBarDivider %>
						    <td id="tdSearchStatusHelpButton" runat="server"></td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div id="divSearchStatus" runat="server">
                <div class="ektronPageInfo">
                    <div id="divErrorMessages" class="divErrorMessages" runat="server">
                        <div class="sectionHeader">
                            <div class="title"><%=this.Sites.EkMsgRef.GetMessage("lbl Status Information")%></div>
                            <div class="description">
                            <%=this.Sites.EkMsgRef.GetMessage("lbl status desc")%>
                            </div>
                        </div>
                        <table class="ektronGrid errorinfo">
                            <tr class="title-header">
                                <th><%=this.Sites.EkMsgRef.GetMessage("lbl solr error section header")%></th>
                                <th><%=this.Sites.EkMsgRef.GetMessage("lbl solr error section header desc")%></th>
                            </tr>
                            <tr>
                                <td class="errormessage"><asp:Literal ID="ucErrorMessage" runat="server"></asp:Literal></td>
                                <td class="errordescription"><asp:Literal ID="ucErrorDescription" runat="server"></asp:Literal>
                                <span>
                                    <a title="<%=this.Sites.EkMsgRef.GetMessage("alt click here for additional details")%>" href="#" onclick="ToggleErrorDetails(); return false;">
                                        <img class="toggleerrordetails" id="Img1" alt="<%=this.Sites.EkMsgRef.GetMessage("alt click here for additional details")%>" 
                                        src="../images/UI/Icons/information.png" />
                                    </a>
                                </span>
                                <div class="errordetails divHidden"><asp:Literal ID="ucErrorDetails" runat="server"></asp:Literal></div></td>
                            </tr>
                        </table>
                    </div>

                     <div id="divMultiNodeUI" class="sectionFooter multiNodeStatus" runat="server">
                        <ektronUI:Accordion ID="uxNodes" runat="server" ViewStateMode="Enabled" >
                        </ektronUI:Accordion>
                     </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
