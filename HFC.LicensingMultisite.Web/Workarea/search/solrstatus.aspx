﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="solrstatus.aspx.cs" Inherits="Workarea_solrsearch_status" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Status</title>
    <asp:Literal ID="styleHelper" runat="server"></asp:Literal>

    <style type="text/css">
        div.solrstatus .divVisible {display:block;}
        div.solrstatus .divHidden {display:none;}

        div.sectionHeader { font-weight: bold; color: #1d5987; }
        table.ektronGrid { margin-bottom: 15px; }
        
        td.header.label {width: 150px; text-align: left;}
        td.value { width: 200px; }
        .description { color: #888888; text-align: left; font-weight: normal;}
        
        div.divErrorMessages td.errormessage { width: 30%; text-align: left; color:#DD2222; font-weight: bold;}
        div.divErrorMessages td.errordescription { width: 70%; }
        div.divErrorMessages div.errordetails { color: #D97272; }
        
        table.crawlTypes td.header.label {width: 10%; text-align: left;}
        table.crawlTypes td.description {width: 30%; text-align: left; color: #888888; text-align: left; font-weight: normal;}
        table.crawlTypes td.value {width: 20%;}

        div.sectionFooter { margin-bottom: 15px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="solrstatus">
            <div id="dhtmltooltip"></div>
            <div class="ektronPageHeader">
                <div class="ektronTitlebar" id="txtTitleBar" runat="server"></div>
                <div class="ektronToolbar" id="htmToolBar" runat="server">
                    <table>
                        <tr>
						    <td id="tdSearchStatusHelpButton" runat="server"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="searchstatus" runat="server">
                <div class="ektronPageInfo">
                    <div id="divStatusInfo" runat="server">
                   
                        <div class="sectionHeader">
                            <div class="title"><%=this.Sites.EkMsgRef.GetMessage("lbl solr search config")%></div>
                            <div class="description">
                            <%=this.Sites.EkMsgRef.GetMessage("lbl solr search config desc")%>
                            </div>
                        </div>
                   
                        <table class="ektronGrid">
                            <tr class="title-header">
                                <th></th>
                                <th><%=this.Sites.EkMsgRef.GetMessage("lbl value")%></th>
                                <th><%=this.Sites.EkMsgRef.GetMessage("lbl description")%></th>
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl solr server hostname")%></td>
                                <td class="value"><asp:Literal ID="ucSolrServerHost" runat="server"></asp:Literal></td>
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl solr server hostname desc")%></td>
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl solr server admin url")%></td>
                                <td class="value"><asp:Literal ID="ucSolrAdminUrl" runat="server"></asp:Literal></td>
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl solr server admin url desc")%></td>
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl solr query service url")%></td>
                                <td class="value"><a runat="server" id="anchorSolrQueryUrl" target="_blank"><asp:Literal ID="ucSolrQueryUrl" runat="server"></asp:Literal></a></td>
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl solr query service url desc")%></td>
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl core name")%></td>
                                <td class="value"><asp:Literal ID="ucCoreName" runat="server"></asp:Literal></td>
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl core associated site")%></td>
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Query Credentials")%></td>
                                <td class="value"><asp:Literal ID="ucUsername" runat="server"></asp:Literal></td>
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl Windows user authorized")%></td>
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Incremental Crawl Interval")%></td>
                                <td class="value"><asp:Literal ID="ucCrawlInterval" runat="server"></asp:Literal></td>
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl Indicates frequency")%></td>
                            </tr>
                        </table>

                        <div class="sectionHeader">
                            <div class="title"><%=this.Sites.EkMsgRef.GetMessage("lbl Crawl Filters")%></div>
                            <div class="description">
                            <%=this.Sites.EkMsgRef.GetMessage("lbl following filters")%>
                            </div>
                        </div>

                        <table class="ektronGrid">
                            <tr class="title-header">
                                <th></th>
                                <th><%=this.Sites.EkMsgRef.GetMessage("btn filter")%></th>
                                <th><%=this.Sites.EkMsgRef.GetMessage("generic description")%></th>
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl html content")%></td>
                                <td class="value"><asp:Literal ID="ucIncludeHtmlContent" runat="server"></asp:Literal></td>    
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl cms contents")%></td>                            
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Document Content")%></td>
                                <td class="value"><asp:Literal ID="ucIncludeDocumentContent" runat="server"></asp:Literal></td>
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl dms contents")%></td>
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Forum Content")%></td>
                                <td class="value"><asp:Literal ID="ucIncludeForumContent" runat="server"></asp:Literal></td>
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl forum contents")%></td>
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Product Content")%></td>                                    
                                <td class="value"><asp:Literal ID="ucIncludeProductContent" runat="server"></asp:Literal></td>    
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl product contents")%></td>
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Community Members")%></td>                                    
                                <td class="value"><asp:Literal ID="ucIncludeCommunityMembers" runat="server"></asp:Literal></td>   
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl Community etc")%></td>                                 
                            </tr>
                            <tr>
                                <td class="header label"><%=this.Sites.EkMsgRef.GetMessage("lbl Community Content")%></td>                                    
                                <td class="value"><asp:Literal ID="ucIncludeCommunityContent" runat="server"></asp:Literal></td>                                    
                                <td class="description"><%=this.Sites.EkMsgRef.GetMessage("lbl Community Content etc")%></td>
                            </tr>
                        </table>

                    </div>
                </div>
    
            </div>
        </div>
    </form>
</body>
</html>
