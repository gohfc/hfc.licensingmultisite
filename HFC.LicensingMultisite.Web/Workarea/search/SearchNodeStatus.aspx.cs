﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Linq; 

using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Search;
using Ektron.Cms.Search.Proxies.SearchRequestService;
using Ektron.Cms.Search.Contracts.Solr;
using Ektron.Cms.Search.Contracts;
using Ektron.Cms.Framework.UI.Controls.EktronUI;

public partial class Workarea_SearchNodeStatus : Ektron.Cms.Workarea.Page
{
    private const string ActionParameter = "action";
    private const string IncrementalCrawlAction = "incremental";
    private const string FullCrawlAction = "full";
    private readonly DateTime MinDate = new DateTime(1899, 12, 30, 0, 0, 0);
    private ISearchSettings searchSettingsProvider;
    private SearchSettingsData searchSettingsData;
    private NodeStatus nodeStatusControl;
    private SolrClusterState solrSearchState;

    private SiteAPI _site;
    private StyleHelper _style;
    private SolrCrawler _crawler;

    public SiteAPI Sites
    {
        get
        {
            if (_site == null) _site = new SiteAPI();
            return _site;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ltrincremental.Text = this.Sites.EkMsgRef.GetMessage("js incremental crawl");
        ltrincrementalRequest.Text = this.Sites.EkMsgRef.GetMessage("js incremental crawl request");
        ltrFullC.Text = this.Sites.EkMsgRef.GetMessage("js full crawl");
        ltrFullRequest.Text = this.Sites.EkMsgRef.GetMessage("js full crawl request");

        if (HasPermission())
        {
            _style = new StyleHelper();
            _crawler = new SolrCrawler();

            JS.RegisterJS(this, JS.ManagedScript.EktronStyleHelperJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronJFunctJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaJS);
            JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaHelperJS);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaIeCss, Ektron.Cms.API.Css.BrowserTarget.LessThanEqualToIE8);

            styleHelper.Text = _style.GetClientScript();
            JS.RegisterJS(this, JS.ManagedScript.EktronWorkareaContextMenusJS);
    
            RenderToolbar();
            RenderTitleBar();

            try
            {

                if (!string.IsNullOrEmpty(Request.QueryString[ActionParameter]) &&
                    (Request.QueryString[ActionParameter].ToString().ToLower() == IncrementalCrawlAction ||
                        Request.QueryString[ActionParameter].ToString().ToLower() == FullCrawlAction))
                {
                    switch (Request.QueryString[ActionParameter].ToLower())
                    {
                        case IncrementalCrawlAction:
                            {
                                _crawler.StartIncrementalCrawl(false, CrawlType.All, false);
                                break;
                            }

                        case FullCrawlAction:
                            {
                                _crawler.StartFullCrawl(CrawlType.All, false);
                                break;
                            }
                    }
                }
                else
                {
                    searchSettingsProvider = ObjectFactory.GetSearchSettings();
                    if (searchSettingsProvider == null)
                        throw new Exception(Sites.EkMsgRef.GetMessage("msg solr search status connection error"));

                    searchSettingsData = searchSettingsProvider.GetItem();

                    if (searchSettingsData == null)
                        throw new Exception(Sites.EkMsgRef.GetMessage("msg solr search status connection error"));

                    RenderState();
                }
            }
            catch (NullReferenceException)
            {
                Utilities.ShowError(Sites.EkMsgRef.GetMessage("msg solr search status connection error"));
            }
            catch (Exception ex)
            {
                Utilities.ShowError(ex.Message);
            }

            if (!string.IsNullOrEmpty(Request.QueryString[ActionParameter]) &&
                (Request.QueryString[ActionParameter].ToString().ToLower() == IncrementalCrawlAction ||
                    Request.QueryString[ActionParameter].ToString().ToLower() == FullCrawlAction))
            {
                Response.Redirect("SearchNodeStatus.aspx");
            }
        }
        else
        {
            Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
        }
    }

    private string GetErrorMessage(string errorCode)
    {
        string result = String.Empty;
        string resource_prefix = "lbl solr admin svc ex ";
        switch (errorCode)
        {
            case SolrNodeStatusResponseCode.ConfigurationException:
            case SolrNodeStatusResponseCode.ConfigurationNotFoundException:
            case SolrNodeStatusResponseCode.SettingsNotFoundException:
            case SolrNodeStatusResponseCode.JobNotFoundException:
            case SolrNodeStatusResponseCode.ManifoldResponseException:
            case SolrNodeStatusResponseCode.GenericException:
                result = this.Sites.EkMsgRef.GetMessage(resource_prefix + errorCode.ToLower());
                break;
            default:
                result = this.Sites.EkMsgRef.GetMessage("lbl solr admin svc ex unhandled_error");
                break;
        }
        return result;
    }

    private string GetLocalizedString(JobAction action)
    {
        string result = action.ToString();
        switch (action)
        {
            case JobAction.IncrementalCrawl:
                result = this.Sites.EkMsgRef.GetMessage("lbl Incremental Crawl");
                break;

            case JobAction.FullCrawl:
                result = this.Sites.EkMsgRef.GetMessage("lbl Full Crawl");
                break;
            default:
                result = action.ToString();
                break;
        }
        return result;
    }

    private bool IsValidCrawlDuration(TimeSpan span)
    {
        TimeSpan none = new TimeSpan();
        return (span > none);
    }

    private void RenderState()
    {
        divErrorMessages.Visible = false;
        divMultiNodeUI.Visible = false;

        solrSearchState = _crawler.GetState();

        // If a null response is received from Admin service display the error and do no further processing.
        if (solrSearchState == null)
        {
            divErrorMessages.Visible = true;

            string adminServiceUrl = (searchSettingsData != null ? searchSettingsData.AdminService : String.Empty);
            ucErrorMessage.Text = String.Format(this.Sites.EkMsgRef.GetMessage("lbl solr getstatus null response"), adminServiceUrl);

            ucErrorDescription.Text = String.Format(this.Sites.EkMsgRef.GetMessage("lbl solr getstatus null response desc"), adminServiceUrl);
            ucErrorDetails.Text = this.Sites.EkMsgRef.GetMessage("lbl solr getstatus null response details");
            return;
        }

        if (solrSearchState.SolrClusterStatusResponse != null &&
            solrSearchState.SolrClusterStatusResponse.Success == false &&
            (!String.IsNullOrWhiteSpace(solrSearchState.SolrClusterStatusResponse.ErrorCode)))
        {
            divErrorMessages.Visible = true;
            ucErrorMessage.Text = this.Sites.EkMsgRef.GetMessage("lbl solr getstatus error");
            ucErrorDescription.Text = GetErrorMessage(solrSearchState.SolrClusterStatusResponse.ErrorCode);
            foreach (string s in solrSearchState.SolrClusterStatusResponse.Details)
                ucErrorDetails.Text += s + "<br />";
            return;
        }

        // If SolrNodeSearchState is null/empty display the error and do no further processing.
        if (solrSearchState.SolrNodeSearchState == null || solrSearchState.SolrNodeSearchState.Count <= 0)
        {
            divErrorMessages.Visible = true;
            ucErrorMessage.Text = this.Sites.EkMsgRef.GetMessage("lbl solr getstatus no status");
            string adminServiceUrl = (searchSettingsData != null ? searchSettingsData.AdminService : String.Empty);
            ucErrorDescription.Text = String.Format(this.Sites.EkMsgRef.GetMessage("lbl solr getstatus null solrnodesearchstate"), adminServiceUrl);
            ucErrorDetails.Text = this.Sites.EkMsgRef.GetMessage("lbl solr getstatus null solrnodesearchstate details"); 
            return;
        }

        DisplaySearchStatus();
    }

    private void DisplaySearchStatus()
    {
        divMultiNodeUI.Visible = true;
        int index = 0;

        foreach (string host in solrSearchState.SolrNodeSearchState.Keys)
        {
            //For standalone setups do not display Zookeeper Process status

            try
            {
                if (solrSearchState.IsStandalone &&
                    solrSearchState.SolrNodeSearchState != null &&
                    solrSearchState.SolrNodeSearchState.Count > 0 &&
                    solrSearchState.SolrNodeSearchState.First().Value != null)
                {
                    SolrNodeStatus standaloneProcessState = solrSearchState.SolrNodeSearchState.First().Value;
                    ProcessState zkState = standaloneProcessState.SolrProcessState.ProcessState.Find(x => x.Name.ToLower().Contains("zookeeper"));
                    if (zkState != null)
                        standaloneProcessState.SolrProcessState.ProcessState.Remove(zkState);
                }
            }
            catch { //Ignore exception, no zookeeper information was found to remove for StandAlone 
            }

            NodeStatus uc = (NodeStatus)(LoadControl("NodeStatus.ascx"));

            uc.SolrNodeStatus = solrSearchState.SolrNodeSearchState[host];
            uc.SearchSettingsData = searchSettingsData;


            Tab tab = new Tab();
            if (!(String.IsNullOrWhiteSpace(host)))
            {
                tab.Text = uc.SolrNodeStatus.SolrNodeState.Host;
            }
            else
            {
                tab.Text = this.Sites.EkMsgRef.GetMessage("lbl solr unknown host");
            }

            tab.AddControl(uc);

            uxNodes.AddTab(tab);
                
            //Set the Leader Tab
            if (!solrSearchState.IsStandalone && uc.SolrNodeStatus != null && uc.SolrNodeStatus.SolrNodeState != null && uc.SolrNodeStatus.SolrNodeState.IsLeader)
            {
                tab.Text += " " + this.Sites.EkMsgRef.GetMessage("lbl solr node leader");
                uxNodes.SetActiveTab(index);
            }

            index++;
        }
    }

    private void RenderTitleBar()
    {
        txtTitleBar.InnerHtml = _style.GetTitleBar(this.Sites.EkMsgRef.GetMessage("lbl solr search page title"));
    }

    private void RenderToolbar()
    {
        tdIncrementalCrawlButton.InnerHtml = _style.GetButtonEventsWCaption(
            this.Sites.AppImgPath + "../UI/Icons/controlRepeat.png", 
            "javascript:crawlIncremental();",
            this.Sites.EkMsgRef.GetMessage("msg search incremental crawl button"),
            this.Sites.EkMsgRef.GetMessage("msg search incremental crawl button"),
            "",
			StyleHelper.CrawlIncrementalButtonCssClass);

        tdFullCrawlButton.InnerHtml = _style.GetButtonEventsWCaption(
            this.Sites.AppImgPath + "../UI/Icons/controlRepeatBlue.png",
            "javascript:crawlFull();",
            this.Sites.EkMsgRef.GetMessage("msg search full crawl button"),
            this.Sites.EkMsgRef.GetMessage("msg search full crawl button"),
            "",
			StyleHelper.CrawlFullButtonCssClass);

        tdRefreshButton.InnerHtml = _style.GetButtonEventsWCaption(
            this.Sites.AppImgPath + "../UI/Icons/refresh.png",
            "SearchNodeStatus.aspx",
            this.Sites.EkMsgRef.GetMessage("generic refresh"),
            this.Sites.EkMsgRef.GetMessage("generic refresh"),
            "",
			StyleHelper.RefreshButtonCssClass);

        tdSearchStatusHelpButton.InnerHtml = _style.GetHelpButton("search_node", "");
    }

    /// <summary>
    /// Returns true if the current user has sufficient permissions to access
    /// the functionality on this page, false otherwise.
    /// </summary>
    /// <returns>True if permissions are sufficient, false otherwise</returns>
    private static bool HasPermission()
    {
        return
            !((Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) ||
            ContentAPI.Current.RequestInformationRef.UserId == 0 ||
            !ContentAPI.Current.EkUserRef.IsAllowed(ContentAPI.Current.UserId, 0, "users", "IsAdmin", 0)) &&
            !(ContentAPI.Current.IsARoleMember((long)EkEnumeration.CmsRoleIds.SearchAdmin, ContentAPI.Current.UserId, false)));
    }
}
