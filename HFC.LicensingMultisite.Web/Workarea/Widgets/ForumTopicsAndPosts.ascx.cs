using System;
using System.Collections;
using System.Text;
using Ektron.Cms.Widget;
using Ektron.Cms.Common;

public partial class Workarea_Widgets_ForumTopicsAndPosts : WorkareaWidgetBaseControl, IWidget
{
    ArrayList _data;
    private long _bID;
    [WidgetDataMember(0)]
    public long bID { get { return _bID; } set { _bID = value; } }

    protected void Page_Init(object sender, EventArgs e)
    {
        base.Host.Edit += new EditDelegate(EditEvent);
        base.Host.Maximize += new MaximizeDelegate(delegate() { Visible = true; });
        base.Host.Minimize += new MinimizeDelegate(delegate() { Visible = false; });
        base.Host.Create += new CreateDelegate(delegate() { EditEvent(""); });
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (bID.ToString() != BoardID.Text && BoardID.Text != "")
            bID = Convert.ToInt64(BoardID.Text);

        _data = EkContentRef.GetModeratedTotals(bID, true);
        if (_data.Count > 0)
        {
            lblNoRecords.Visible = false;
            LoadData(bID);
        }
        else
        {
            ltr_forum_mod.Text = "";
            lblNoRecords.Visible = true;
            string title = GetMessage("lbl forummod smrtdesk") + " (0)";
            SetTitle(title);
        }

        ForumTopicsAndPostsViews.SetActiveView(DisplayView);
        ltrlNoRecords.Text = GetMessage("lbl no topics replies awaiting moderation");    
    }

    #region Edit
    /// <summary>
    /// Edit Event
    /// </summary>
    /// <param name="settings">Settings Data</param>
    protected void EditEvent(string settings)
    {
        BoardID.Text = bID.ToString();

        ForumTopicsAndPostsViews.SetActiveView(EditView);
    }
    #endregion

    #region Save Data
    /// <summary>
    /// Save Data
    /// </summary>
    /// <param name="sender">Sender Object</param>
    /// <param name="e">Event Args</param>
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        int boardId;

        int.TryParse(BoardID.Text, out boardId);
        bID = boardId;
        Host.SaveWidgetDataMembers();
        ForumTopicsAndPostsViews.SetActiveView(DisplayView);
    }
    #endregion


    #region Cancel Event
    /// <summary>
    /// Cancel Event
    /// </summary>
    /// <param name="sender">Sender Object</param>
    /// <param name="e">Event Args</param>
    protected void CancelButton_Click(object sender, EventArgs e)
    {
        ForumTopicsAndPostsViews.SetActiveView(DisplayView);
    }
    #endregion


    private void LoadData(long bID = 0)
    {
        int itotal = 0;
        long boardID = bID;
        long forumID = 0;
        int alltotal = 0;

        StringBuilder sbForum = new StringBuilder();

        ArrayList alTmp = new ArrayList();


        for (int i = 0; i <= _data.Count - 1; i++)
        {        
            
                alTmp = (ArrayList)_data[i];
                itotal = Convert.ToInt32(alTmp[0]);

                boardID = Convert.ToInt64(alTmp[1]);
                forumID = Convert.ToInt64(alTmp[3]);

                if (boardID == bID || bID == 0)
                {
                    if (boardID != 0)
                    {
                        sbForum.Append("<img src=\"images/UI/Icons/folderBoard.png\" style=\"vertical-align:middle\"> ");
                        sbForum.Append(alTmp[2]);
                        sbForum.Append("<br />");
                        sbForum.Append(Environment.NewLine);
                    }


                    string foldercsvpath = EkContentRef.GetFolderParentFolderIdRecursive(forumID);

                    if (forumID != 0)
                    {
                        //  a new forum
                        sbForum.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                        sbForum.Append("<img src=\"images/UI/Icons/folderBoard.png\" style=\"vertical-align:middle\"> ");
                        sbForum.Append("<a href=\"#\" onclick=\"top.showContentInWorkarea('" + EkFunctions.UrlEncode("content.aspx?action=ViewContentByCategory&id=" + forumID.ToString() + "&from=dashboard") + "', 'Content', '" + foldercsvpath + "')\">");
                        sbForum.Append(alTmp[4]);
                        sbForum.Append("</a>");
                        sbForum.Append("<br />");
                        sbForum.Append(Environment.NewLine);
                    }
                    sbForum.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    sbForum.Append("<img src=\"images/UI/Icons/folderBoard.png\" style=\"vertical-align:middle\">");
                    sbForum.Append("&nbsp;");
                    sbForum.Append("<a href=\"#\" onclick=\"top.showContentInWorkarea('" + EkFunctions.UrlEncode("content.aspx?id=" + forumID.ToString() + "&action=ViewContentByCategory&LangType=1033&ContType=13&contentid=" + alTmp[5].ToString() + "&from=dashboard") + "', 'Content', '" + foldercsvpath + "')\">");
                    sbForum.Append(alTmp[6] + " " + (alTmp[7].ToString().ToUpper() == "I" ? "" : "(" + itotal.ToString() + ")"));
                    sbForum.Append("</a>");
                    sbForum.Append("<br />");
                    sbForum.Append(Environment.NewLine);
                    alltotal = (alltotal + itotal);
                }
        }

        ltr_forum_mod.Text = sbForum.ToString();

        string title = GetMessage("lbl forummod smrtdesk") + " (" + alltotal.ToString() + ")";
        SetTitle(title);
    }
}
