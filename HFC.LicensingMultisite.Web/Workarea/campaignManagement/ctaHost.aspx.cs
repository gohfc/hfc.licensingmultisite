﻿using System;
using Ektron.Cms;
using Ektron.Cms.Common;

public partial class Workarea_campaign_management_ctaHost : System.Web.UI.Page
{
    public long CTAId = 0;
    public string LangId;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ctaid"] != null &&
                !String.IsNullOrEmpty(Request.QueryString["ctaid"].ToString()))
        {
            long.TryParse(Request.QueryString["ctaid"], out CTAId);
        }

        if (Request.QueryString["langid"] != null &&
                !String.IsNullOrEmpty(Request.QueryString["langid"].ToString()))
        {
            LangId = Request.QueryString["langid"].ToString();
        }

        if (CTAId > 0)
        {
            var manager = ObjectFactory.GetCTA(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
            manager.RequestInformation.CallerId = EkConstants.InternalAdmin;
            manager.RequestInformation.UserId = EkConstants.InternalAdmin;

            uxCTa.Text = manager.GetRawHtml(CTAId, LangId, true);
        }
    }

}