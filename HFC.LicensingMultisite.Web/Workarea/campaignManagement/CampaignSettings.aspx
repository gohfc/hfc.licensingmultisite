﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CampaignSettings.aspx.cs" Inherits="Ektron.Workarea.CampaignManagement.CampaignSettings" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Campaign Management Settings</title>
    <meta http-equiv="Cache-control" content="no-cache" />
    <script type="text/javascript" language="javascript">
        function RollOver(MyObj) {
            $ektron(MyObj).parent().addClass("button-over");
            $ektron(MyObj).parent().removeClass("button");
        }

        function RollOut(MyObj) {
            $ektron(MyObj).parent().addClass("button");
            $ektron(MyObj).parent().removeClass("button-over");
        }
    </script>
</head>
<body>
    <form id="form2" runat="server">
        <ektronUI:Css ID="campaignCSS" Path="CSS/Ektron.Workarea.CampaignManagement.css" runat="server">
        </ektronUI:Css>
        <ektronUI:JavaScript ID="campaignJS" runat="server" Path="js/ektron.workarea.campaignManagement.js" />
        <ektronUI:JavaScriptBlock ID="campaignJSLauncher" runat="server">
            <ScriptTemplate>
                Ektron.Workarea.CampaignManagement.initSettings();
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
        <!-- Header Section -->
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
                </span><span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                        <asp:Literal ID="uxBackButton" runat="server"></asp:Literal>
                        <td class="column-PrimaryButton">
                            <asp:LinkButton runat="server" ID="uxEditButton" class="primary editButton" OnClick="EditButton_Click"
                                meta:resourcekey="uxedittxt_labelBase" />
                            <asp:LinkButton runat="server" ID="uxSaveButton" Visible="false" meta:resourcekey="uxSavetxt_labelBase"
                                ValidationGroup="saveSettings" OnClientClick="return Ektron.Workarea.CampaignManagement.SaveSettings();"
                                class="primary saveButton" OnClick="SaveButton_Click" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="ektronPageContainer ektronPageInfo">
            <ektronUI:Message ID="uxMessage" runat="server" Visible='<%# this.ShowMessage %>'>
            </ektronUI:Message>
            <div class="ektronTopSpace">
            </div>
            <div class="ektrongridtwo">
                <asp:Panel ID="CampaignManagementAdvanceSettings" runat="server">
                    <table class="ektronForm campaignSettings">
                        <tr>
                            <td class="label">
                                <asp:Label ID="uxLandingPageAliasPrefixLabel" runat="server" meta:resourcekey="uxLandingPageAliasPrefixLabel" />
                            </td>
                            <td class="value">
                                <div class="lpAliasPrefix">
                                    <asp:TextBox runat="server" ID="uxLandingPagePrefix" Enabled='<%# this.IsEditable %>'></asp:TextBox>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="uxCampaignMediaLabel" runat="server" meta:resourcekey="uxCampaignMediaLabel" />
                            </td>
                            <td class="value">
                                <div class="MediaListBlock">
                                    <asp:ListBox ID="uxCampaignMediumList" runat="server"></asp:ListBox>
                                    <input type="hidden" id="uxMediasHidden" runat="server" />
                                </div>
                                <asp:PlaceHolder ID="uxMediasec" runat="server" Visible="false">
                                    <div class="MediaControlBlock">
                                        <asp:Button ID="btnRemoveExt" runat="server" Text="Remove" Enabled='<%# this.IsEditable %>' /><br />
                                        <p>
                                        </p>
                                        <asp:TextBox ID="tbNewExt" runat="server" Enabled='<%# this.IsEditable %>'
                                            EnableViewState="false"></asp:TextBox>
                                        <asp:Button ID="btnAdd" runat="server" Text="Add" Enabled='<%# this.IsEditable %>' />
                                    </div>
                                </asp:PlaceHolder>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
