﻿/*
* Ektron Workarea Campaign Management
*
* Copyright 2014
*
* Depends:
*	jQuery
*   Ektron.Namespace.js
*/

Ektron.Namespace.Register("Ektron.Workarea.CampaignManagement");
Ektron.Workarea.CampaignManagement = {
    initSettings: function () {
        $ektron('div.MediaControlBlock #btnRemoveExt').click(function () {
            Ektron.Workarea.CampaignManagement.RemoveMedia();
            return false;
        });
        $ektron('div.MediaControlBlock #btnAdd').click(function () {
            Ektron.Workarea.CampaignManagement.AddMedia();
            return false;
        });
    },
    RemoveMedia: function () {
        var enabled = $ektron('div.MediaControlBlock #btnRemoveExt').is('[readonly = "readonly"]');
        if (!enabled) {
            var currentExt = $ektron('div.MediaListBlock #uxCampaignMediumList option:selected').text();
            if (!currentExt.match(/\/|website/)) {
                $ektron('div.MediaListBlock #uxCampaignMediumList option:selected').remove();
            } else {
                alert('This default Media cannot be removed.');
            }
        }
    },

    AddMedia: function () {
        var enabled = $ektron('div.MediaControlBlock #btnAdd').is('[readonly = "readonly"]');
        if (!enabled) {
            var isExist = false;
            var newExt = $ektron('div.MediaControlBlock #tbNewExt').val().toLowerCase();
            if (newExt) {
                if (Ektron.Workarea.CampaignManagement.mediumValidation(newExt)) {
                    if (!newExt.match(/\/|website/)) {
                        $ektron('#uxCampaignMediumList option:contains("' + newExt + '")').each(function () {
                            if ($ektron(this).text() == newExt) {
                                $ektron('div.MediaControlBlock #tbNewExt').val('');
                                isExist = true;
                            }
                        });

                        if (!isExist) {
                            $ektron('div.MediaListBlock #uxCampaignMediumList')
                .append($("<option></option>")
                            //                .attr("value", newExt)
                .text(newExt));
                            $ektron('div.MediaControlBlock #tbNewExt').val('');
                        } else {
                            alert('This Media already exists');
                        }
                    } else {
                        $ektron('div.MediaControlBlock #tbNewExt').val('');
                        alert('This Media already exists');
                    }
                }
                else {
                    alert('Please enter Valid Media');
                }

            } else {
                alert('Please enter an Media');
            }

        }
    },
    RefreshTreeAfterSettings: function () {
        top.TreeNavigation("AdminTree", "campaign management\\settings");
    },
    HideMessage: function () {
        $ektron('#uxMessage').delay(3000).hide('200');
    },
    mediumValidation: function (value) {
        var badChars = ['"', ',', '\\', '<', '>', ' ', ':', '|', '?', "'", '^', '%', '$', '!', '*', '#', ']', '@', '~'];
        for (var i = 0; i < badChars.length; i++) {
            if (value.indexOf(badChars[i]) != -1) {
                return false;
            }
        }

        return true;
    },
    SaveSettings: function () {
        var prefix = $ektron('div.lpAliasPrefix #uxLandingPagePrefix').val().toLowerCase();
        if (prefix) {
            if (!Ektron.Workarea.CampaignManagement.mediumValidation(prefix)) {
                alert('Please enter Valid Landing Page Alias Prefix.');
                return false;
            }
        }
        var items = $ektron("#uxCampaignMediumList > option").map(function () {
            var arr = [];
            arr.push($ektron(this).text());
            return arr;
        }).get();
        $ektron('#uxMediasHidden').val(Ektron.JSON.stringify(items));
        return true;
    },
};