﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ctaHost.aspx.cs" Inherits="Workarea_campaign_management_ctaHost" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .ux-cta
        {
            font-size: 75px;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <span id="cta" class="ux-cta">
            <asp:Literal runat="server" ID="uxCTa"></asp:Literal>
        </span>
    </form>
</body>
</html>
