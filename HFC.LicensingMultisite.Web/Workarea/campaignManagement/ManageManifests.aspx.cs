﻿

using System.Threading.Tasks;
using Ektron.Cms.Instrumentation;

namespace Ektron.Workarea.CampaignManagement
{
    using Ektron.Cms;
    using Ektron.Cms.CampaignManagement;
    using Ektron.Cms.Common;
    using Ektron.Cms.Core;
    using Ektron.Cms.Framework.UI;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Web.Script.Serialization;
    using System.Xml.Serialization;
    using System.Linq;
    using com.sun.org.apache.bcel.@internal.generic;
    using Ektron.Cms.BusinessObjects.CampaignManagement;
    using Ektron.Cms.Contracts.CampaignManagement;
    using Ektron.Cms.Framework.CampaignManagement;
    using Ektron.Cms.PageBuilder;

    public partial class ManageManifests : System.Web.UI.Page
    {
        #region Variables

        private EkMessageHelper msgHelper;
        private int cb;
        private ContentAPI refContentApi;
        private CampaignManagementSettingsData campaignManagementData;
        private SiteSetting siteSettingManager;
        private List<ManifestData> manifestList;
        [DefaultValue(false)]
        public bool ShowMessage { get; set; }
        [DefaultValue(false)]
        public bool IsEditable { get; set; }
        [DefaultValue(false)]
        public bool IsReplaceManifest { get; set; }

        public bool campaignManagementInstalled = true;
        private List<string> _defaultManifestList = new List<string>() { "Form Left", "Form Right", "Form Center", "Result Page", "Menu with Form" };

        private ILandingPageManager LandingPageManager;
        #endregion

        #region Events

        /// <summary>
        /// Use this event to read or initialize control properties
        /// </summary>
        /// <param name="e"> EventArgs e</param>
        protected override void OnInit(EventArgs e)
        {
            if (this.HasPermission())
            {
                base.OnInit(e);
                refContentApi = new ContentAPI();
                siteSettingManager = new SiteSetting();
                LandingPageManager = ObjectFactory.GetLandingPageManager(refContentApi.GetRequestInformation());
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        /// <summary>
        /// Use the OnLoad event method to set properties in controls.
        /// </summary>
        /// <param name="e">EventArgs e</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.msgHelper = this.refContentApi.EkMsgRef;
            this.RegisterResources();
            var styleHelper = new StyleHelper();
            aspHelpButton.Text = styleHelper.GetCustomHelpButton("http://documentation.ektron.com/cms400/v9.10/CMP/Web/CMP.htm#CMP/CMPAdmin.htm#Manifest", string.Empty);
            string returnpath = refContentApi.RequestInformationRef.ApplicationPath +
                                "CampaignManagement/ManageManifests.aspx?action=view";
            uxBackButton.Text =
                styleHelper.GetButtonEventsWCaption(
                    refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath,
                    msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "",
                    StyleHelper.BackButtonCssClass, true);

            // CMP validation.
            try
            {
                ValidateCampaignManagement();
            }
            catch (Exception ex)
            {
                uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                this.ShowMessage = true;
                uxMessage.Text = ex.Message;
                return;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!IsPostBack)
            {
                if (campaignManagementInstalled)
                {
                    try
                    {
                        this.ViewManifests();
                    }
                    catch (Exception exc)
                    {
                        Utilities.ShowError(exc.Message);
                    }
                }
            }
        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            ResetValues();
            this.uxSaveButton.Visible = this.IsEditable = true;
            // this.uxCampaignMediumList.Attributes.Remove("disabled");
            this.DataBind();
            this.uxEditButton.Visible = false;
            UpsertManifest.Visible = true;
            this.uxBackButton.Visible = true;
            this.uxMessage.Visible = false;
            uxManifestList.Visible = false;
        }

        private void ValidateCampaignManagement()
        {
            // Check whether CampaignManagement is installed.
            string landingpagePath = "~/LandingPages/";
            try
            {
                var mappath = Utility.MapPath(landingpagePath);
                if (!Directory.Exists(mappath))
                {
                    campaignManagementInstalled = false;
                    throw new CmsException("Campaign Management Platform is not installed.");
                }
            }
            catch (Exception ex)
            {
                campaignManagementInstalled = false;
                throw new CmsException("Campaign Management Platform is not installed.");
            }
        }

        private void ResetValues()
        {
            uxManifestName.Text = "";
            uxPageId.Text = "0";
        }
        protected void ReplaceButton_Click(object sender, EventArgs e)
        {
            this.IsReplaceManifest = true;
            ProcessSave();
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            ProcessSave();
        }

        private void ProcessSave()
        {
            if (IsPostBack)
            {
                UpsertManifest.Visible = false;
                this.DataBind();

                try
                {
                    this.GenerateCustomManifest();
                    //  this.uxCampaignMediumList.Attributes.Add("disabled", "");
                    GetManifests();
                    this.ShowMessage = true;
                    uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Success;
                    var localResourceObject = GetLocalResourceObject("manifestSaved");
                    if (localResourceObject != null)
                        uxMessage.Text = localResourceObject.ToString();
                    JavaScriptResolver.RegisterJavaScriptBlock(this, "Ektron.Workarea.CampaignManagement.HideMessage();Ektron.Workarea.CampaignManagement.RefreshTreeAfterSettings();");
                    this.ViewManifests();
                }
                catch (Exception exc)
                {
                    uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    this.ShowMessage = true;
                    uxMessage.Text = exc.Message;
                    UpsertManifest.Visible = true;
                    this.DataBind();
                    return;
                }
            }
        }
        #endregion

        #region Display

        /// <summary>
        /// Display Manifests.
        /// </summary>
        protected void ViewManifests()
        {
            this.uxReplaceButton.Visible = this.IsEditable = false;
            this.uxEditButton.Visible = true;
            this.uxSaveButton.Visible = false;
            this.uxBackButton.Visible = false;
            uxManifestList.Visible = true;
            this.IsReplaceManifest = false;
            GetManifests();
            ResetValues();
            this.DataBind();
        }

        private void GetManifests()
        {
            manifestList = LandingPageManager.GetManifestList();
            if (manifestList != null && manifestList.Any())
            {
                uxManifestList.EktronUIPagingInfo = new PagingInfo(1000) { TotalRecords = manifestList.Count };
                uxManifestList.DataSource = manifestList;
            }
        }

        /// <summary>
        /// Add Custom Manifest.
        /// </summary>
        protected void GenerateCustomManifest()
        {
            long pageId = 0;
            if (string.IsNullOrEmpty(uxManifestName.Text) && string.IsNullOrWhiteSpace(uxManifestName.Text))
            {
                throw new CmsException("Manifest Name is Required.");
            }

            // Default manifests cannot be replaced.
            var anyManifests = _defaultManifestList.Where(s => (String.Equals(uxManifestName.Text, s, StringComparison.CurrentCultureIgnoreCase)) || (String.Equals(uxManifestName.Text, s.Replace(" ", ""), StringComparison.CurrentCultureIgnoreCase)));
            if (anyManifests != null && anyManifests.Any())
            {
                throw new CmsException("Default Manifests cannot be replaced. Please rename manifest Name.");
            }

            bool nameAlreadyExist = false;
            GetManifests();
            if (manifestList != null && manifestList.Any() && !IsReplaceManifest)
            {
                Parallel.For(0, manifestList.Count, (i, state) =>
                {
                    if (uxManifestName.Text.ToLower() == (manifestList[i].Name.ToLower()))
                    {
                        nameAlreadyExist = true;
                        state.Stop();
                    }
                });
            }

            if (nameAlreadyExist)
            {
                this.uxReplaceButton.Visible = true;
                this.uxSaveButton.Visible = false;
                throw new CmsException("Manifest Name already Exists. Want To Replace existing Manifest? If yes, Please click Replace Manifest.");
            }

            if (!string.IsNullOrEmpty(uxPageId.Text) && long.TryParse(uxPageId.Text, out pageId) && pageId > 0)
            {
                var contentManager = ObjectFactory.GetContent(LandingPageManager.RequestInformation);
                var pageInfo = contentManager.GetItem(pageId);
                if (pageInfo != null && pageInfo.Id > 0 && pageInfo.SubType == EkEnumeration.CMSContentSubtype.PageBuilderData)
                {
                    var manifestManager = new ManifestManager(LandingPageManager.RequestInformation);
                    var manifestData = manifestManager.PackPage(pageId);
                    manifestData.Name = uxManifestName.Text;

                    if (manifestList != null)
                    {
                        Parallel.For(0, manifestList.Count, (i, state) =>
                        {
                            if (uxManifestName.Text.ToLower() == (manifestList[i].Name.ToLower()))
                            {
                                manifestData.Id = manifestList[i].Id;
                                manifestData.Thumbnail = manifestList[i].Thumbnail;
                                manifestData.IsResultPage = manifestList[i].IsResultPage;
                            }
                        });
                    }
                    manifestManager.SaveManifest(manifestData);
                }
                else
                {
                    throw new CmsException("Pagebuilder Id is not valid Id.");
                }
            }
            else
            {
                throw new CmsException("Pagebuilder Id is required for Manifest.");
            }
        }

        /// <summary>
        /// Returns true if the current user has sufficient permissions to access
        /// the functionality on this page, false otherwise.
        /// </summary>
        /// <returns>True if permissions are sufficient, false otherwise</returns>
        private bool HasPermission()
        {
            return
            (
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0
                ) &&
                (
                    ContentAPI.Current.EkUserRef.IsAllowed(ContentAPI.Current.UserId, 0, "users", "IsAdmin", 0)
                    || ContentAPI.Current.EkUserRef.IsARoleMember((long)EkEnumeration.CmsRoleIds.CampaignManagementAdmin, ContentAPI.Current.UserId, false)
                )
            );
        }

        #endregion

        #region JavaScript/CSS

        /// <summary>
        /// Register Javascripts and CSS packages
        /// </summary>
        private void RegisterResources()
        {
            Packages.EktronCoreJS.Register(this);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Packages.Ektron.JSON.Register(this);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
        }

        #endregion
    }
}
