﻿using System.Linq;

namespace Ektron.Workarea.CampaignManagement
{
    using Ektron.Cms;
    using Ektron.Cms.CampaignManagement;
    using Ektron.Cms.Common;
    using Ektron.Cms.Core;
    using Ektron.Cms.Framework.UI;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Web.Script.Serialization;
    using System.Xml.Serialization;

    /// <summary>
    /// CampaignManagement Settings
    /// </summary>
    public partial class CampaignSettings : Ektron.Cms.Workarea.Page
    {
        #region Variables

        private EkMessageHelper msgHelper;
        private int cb;
        private ContentAPI refContentApi;
        private CampaignManagementSettingsData campaignManagementData;
        private SiteSetting siteSettingManager;

        [DefaultValue(false)]
        public bool ShowMessage { get; set; }
        [DefaultValue(false)]
        public bool IsEditable { get; set; }
        [DefaultValue(false)]
        public bool IsAliasEditable { get; set; }

        #endregion

        #region Events

        /// <summary>
        /// Use this event to read or initialize control properties
        /// </summary>
        /// <param name="e"> EventArgs e</param>
        protected override void OnInit(EventArgs e)
        {
            if (this.HasPermission())
            {
                base.OnInit(e);
                refContentApi = new ContentAPI();
                siteSettingManager = new SiteSetting();
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        /// <summary>
        /// Use the OnLoad event method to set properties in controls.
        /// </summary>
        /// <param name="e">EventArgs e</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.msgHelper = this.refContentApi.EkMsgRef;
            this.RegisterResources();
            var styleHelper = new StyleHelper();
            aspHelpButton.Text = styleHelper.GetCustomHelpButton("http://documentation.ektron.com/cms400/v9.10/CMP/Web/CMP.htm#CMP/CMPAdmin.htm", string.Empty);
            string returnpath = refContentApi.RequestInformationRef.ApplicationPath + "CampaignManagement/CampaignSettings.aspx?action=view";
            uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!IsPostBack)
            {
                try
                {
                    this.uxCampaignMediumList.Attributes.Add("disabled", "");
                    this.ViewCampaignManagementSettings();
                }
                catch (Exception exc)
                {
                    Utilities.ShowError(exc.Message);
                }
            }
        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            this.IsEditable = true;
            this.uxCampaignMediumList.Attributes.Remove("disabled");
            this.DataBind();
            this.uxEditButton.Visible = false;
            this.uxSaveButton.Visible = this.uxMediasec.Visible = true;
            this.uxBackButton.Visible = true;
            this.uxMessage.Visible = false;

        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            if (IsPostBack)
            {

                this.IsEditable = this.uxMediasec.Visible = false;
                this.IsAliasEditable = false;
                this.DataBind();

                try
                {
                    this.UpdateCampaignManagementSettings();
                    this.uxCampaignMediumList.Attributes.Add("disabled", "");
                    GetCampaignManagementSettings();
                    this.ShowMessage = true;
                    uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Success;
                    var localResourceObject = GetLocalResourceObject("SettingsSaved");
                    if (localResourceObject != null)
                        uxMessage.Text = localResourceObject.ToString();
                    JavaScriptResolver.RegisterJavaScriptBlock(this, "Ektron.Workarea.CampaignManagement.HideMessage();Ektron.Workarea.CampaignManagement.RefreshTreeAfterSettings();");
                    this.ViewCampaignManagementSettings();
                }
                catch (Exception exc)
                {
                    uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    this.ShowMessage = true;
                    uxMessage.Text = exc.Message;
                    this.IsEditable = this.uxMediasec.Visible = true;
                    this.IsAliasEditable = true;
                    this.DataBind();
                    return;
                }
            }
        }


        #endregion

        #region Display

        /// <summary>
        /// Display View Campaign Management Settings
        /// </summary>
        protected void ViewCampaignManagementSettings()
        {
            this.IsEditable = false;
            this.uxEditButton.Visible = true;
            this.uxSaveButton.Visible = false;
            this.uxBackButton.Visible = false;
            GetCampaignManagementSettings();

            // View Advance Campaign Management Settings.
            var managementData = this.campaignManagementData;
            if (managementData != null)
            {
                uxCampaignMediumList.DataSource = managementData.CampaignMediaList;
                uxLandingPagePrefix.Text = managementData.LandingPageAliasPrefix;
            }

            this.DataBind();
        }

        private void GetCampaignManagementSettings()
        {
            var siteSettings = new SiteSetting();
            var cmpSettings = siteSettings.GetItem(EkEnumeration.SiteSetting.CampaignManagementSettings.GetHashCode(), 0);
            if ((cmpSettings != null) && (cmpSettings.Value != ""))
            {
                var settingsReader = new StringReader(cmpSettings.Value);
                var xml = new XmlSerializer(typeof(CampaignManagementSettingsData));
                campaignManagementData = (CampaignManagementSettingsData)xml.Deserialize(settingsReader);
            }

            if (campaignManagementData != null && campaignManagementData.CampaignMediaList != null && campaignManagementData.CampaignMediaList.Any())
            {
                campaignManagementData.CampaignMediaList = (campaignManagementData.CampaignMediaList.GroupBy(x => x.ToLower()).Select(y => y.First())).ToList();
            }
        }
        /// <summary>
        /// Update Campaign Management Settings
        /// </summary>
        protected void UpdateCampaignManagementSettings()
        {
            try
            {
                var aJsSerializer = new JavaScriptSerializer();
                if (campaignManagementData == null)
                {
                    campaignManagementData = new CampaignManagementSettingsData();
                }
                campaignManagementData.LandingPageAliasPrefix = uxLandingPagePrefix.Text;
                campaignManagementData.CampaignMediaList = aJsSerializer.Deserialize<List<string>>(uxMediasHidden.Value);

                if (campaignManagementData.CampaignMediaList == null || campaignManagementData.CampaignMediaList.Count == 0)
                {
                    campaignManagementData.CampaignMediaList.Add("website");
                }

                campaignManagementData.DateModified = DateTime.Now;
                campaignManagementData.UserId = siteSettingManager.RequestInformation.UserId;

                var xml = new XmlSerializer(campaignManagementData.GetType());
                var stringWriter = new StringWriter();
                xml.Serialize(stringWriter, campaignManagementData);
                siteSettingManager = new SiteSetting();
                var settingData = new SiteSettingData();
                settingData.Id = EkEnumeration.SiteSetting.CampaignManagementSettings.GetHashCode();
                settingData.SiteId = 0;
                settingData.Value = stringWriter.ToString();
                siteSettingManager.Update(settingData);

            }
            catch (Exception ex)
            {
            }
        }

        private void ValidateText(string text)
        {

        }
        /// <summary>
        /// Returns true if the current user has sufficient permissions to access
        /// the functionality on this page, false otherwise.
        /// </summary>
        /// <returns>True if permissions are sufficient, false otherwise</returns>
        private bool HasPermission()
        {
            return
            (
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0
                ) &&
                (
                    ContentAPI.Current.EkUserRef.IsAllowed(ContentAPI.Current.UserId, 0, "users", "IsAdmin", 0)
                    || ContentAPI.Current.EkUserRef.IsARoleMember((long)EkEnumeration.CmsRoleIds.CampaignManagementAdmin, ContentAPI.Current.UserId, false)
                )
            );
        }

        #endregion

        #region JavaScript/CSS

        /// <summary>
        /// Register Javascripts and CSS packages
        /// </summary>
        private void RegisterResources()
        {
            Packages.EktronCoreJS.Register(this);
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Packages.Ektron.JSON.Register(this);
            Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
        }

        #endregion
    }
}