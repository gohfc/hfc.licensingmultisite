﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManageManifests.aspx.cs" Inherits="Ektron.Workarea.CampaignManagement.ManageManifests" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Manage Manifests</title>
    <meta http-equiv="Cache-control" content="no-cache" />
    <script type="text/javascript" language="javascript">
        function RollOver(MyObj) {
            $ektron(MyObj).parent().addClass("button-over");
            $ektron(MyObj).parent().removeClass("button");
        }

        function RollOut(MyObj) {
            $ektron(MyObj).parent().addClass("button");
            $ektron(MyObj).parent().removeClass("button-over");
        }
    </script>
    <style type="text/css">
        .redLabel {
            color: red;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server">
        <ektronUI:Css ID="campaignCSS" Path="CSS/Ektron.Workarea.CampaignManagement.css" runat="server" Aggregate="True" BrowserTarget="All" Media="">
        </ektronUI:Css>
        <ektronUI:JavaScript ID="campaignJS" runat="server" Path="js/ektron.workarea.campaignManagement.js" Aggregate="True" />
        <ektronUI:JavaScriptBlock ID="campaignJSLauncher" runat="server" ExecutionMode="OnEktronReady">
        </ektronUI:JavaScriptBlock>
        <!-- Header Section -->
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
                </span><span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                        <asp:Literal ID="uxBackButton" runat="server" meta:resourcekey="uxBackButtonResource1"></asp:Literal>
                        <td class="column-PrimaryButton">
                            <asp:LinkButton runat="server" ID="uxEditButton" Visible="False" class="primary editButton" OnClick="EditButton_Click"
                                meta:resourcekey="uxedittxt_labelBase" />
                            <asp:LinkButton runat="server" ID="uxSaveButton" Visible="False" meta:resourcekey="uxSavetxt_labelBase"
                                ValidationGroup="saveSettings" class="primary saveButton" OnClick="SaveButton_Click" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="uxReplaceButton" Visible="False" meta:resourcekey="uxReplaceButton"
                                ValidationGroup="saveSettings" class="primary saveButton" OnClick="ReplaceButton_Click" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="ektronPageContainer ektronPageInfo">
            <ektronUI:Message ID="uxMessage" runat="server" Visible='<%# this.ShowMessage %>' DisplayMode="Error" meta:resourcekey="uxMessageResource1">
            </ektronUI:Message>
            <div class="ektronTopSpace">
            </div>
            <div class="ektrongridtwo">

                <asp:Panel runat="server" ID="uxManifestSection">
                    <ektronUI:GridView ID="uxManifestList" runat="server" AutoGenerateColumns="false" EktronUIAllowResizableColumns="false" Visible="true">
                        <Columns>
                            <ektronUI:CheckBoxField ID="uxSelectColumn">
                                <CheckBoxFieldHeaderStyle HeaderCheckBoxVisible="false" />
                                <CheckBoxFieldColumnStyle Visible="false" />
                            </ektronUI:CheckBoxField>
                            <asp:TemplateField meta:resourceKey="HeaderIdText">
                                <ItemTemplate>
                                    <asp:Literal ID="uxFileName" runat="server" Text='<%# Eval("Id") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField meta:resourceKey="NameText">
                                <ItemTemplate>
                                    <asp:Literal ID="uxFileName" runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField meta:resourceKey="IsResultPageText">
                                <ItemTemplate>
                                    <%# Eval("IsResultPage")  %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField meta:resourceKey="ThumbnailpathText">
                                <ItemTemplate>
                                    <img width="180" height="140" src='<%# Eval("Thumbnail") %>' alt="Thumnail" runat="server"></img>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </ektronUI:GridView>
                </asp:Panel>
                <asp:Panel ID="UpsertManifest" runat="server" Visible="False" meta:resourcekey="UpsertManifestResource">
                    <table class="ektronForm campaignSettings">
                        <tr>
                            <td class="label">
                                <asp:Label ID="uxManifestNameLabel" runat="server" meta:resourcekey="uxManifestNameLabel" />
                            </td>
                            <td class="value">
                                <asp:TextBox runat="server" ID="uxManifestName"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="requiredFieldValidator1" runat="server" CssClass="redLabel" ControlToValidate="uxManifestName" ErrorMessage="Please enter a valid Manifest Name"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <asp:Label ID="uxPageBuilderIdLabel" runat="server" meta:resourcekey="uxPageBuilderIdLabel" />
                            </td>
                            <td class="value">
                                <asp:TextBox runat="server" ID="uxPageId" Text="0"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regularExpressionValidator" runat="server" CssClass="redLabel" ControlToValidate="uxPageId" ValidationExpression="[0-9]+" ErrorMessage="Please enter a valid PageId"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="requiredFieldValidator" runat="server" CssClass="redLabel" ControlToValidate="uxPageId" ErrorMessage="Please enter a PageId"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
