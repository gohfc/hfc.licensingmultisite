<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Metadata.ascx.cs" Inherits="Community_DistributionWizard_Metadata" %>
<script type="text/javascript">
 function RemoveContentImage(path) {
			        var elem = null;
			        var elemThumb = null;
			        elem = document.getElementById( 'content_image' );
			        if (elem != null)
			        {
			            elem.value = '';
			        }
			        elemThumb = document.getElementById( 'content_image_thumb' );
			        if ( elemThumb != null )
			        {
			            elemThumb.src = path;
			        }
			    }
 </script>
<asp:Literal ID="ltrEnhancedMetadataArea" runat="server"></asp:Literal>
<asp:Literal ID="ltrMetadataHTML" runat="server"></asp:Literal>
<div class="EnhancedMetadataBasic">
    <input type="hidden" id="inputTitle" runat="server" />
    <input type="hidden" id="inputDescription" runat="server" />
    <input type="hidden" id="inputKeywords" runat="server" />
</div>