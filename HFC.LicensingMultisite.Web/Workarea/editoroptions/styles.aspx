﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="styles.aspx.cs" Inherits="styles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal runat="server" id="litTitle" /></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
</head>
<body>
    <form id="form1" runat="server">
        <ektronUI:Message ID="uxInformationMessage" DisplayMode="Information" runat="server" Visible="false">
            <ContentTemplate>No Style Definition is stored in the system.</ContentTemplate>
        </ektronUI:Message>

        <ektronUI:GridView ID="StylesList" runat="server" AutoGenerateColumns="false" EnableEktronUITheme="true" AllowPaging="true" OnEktronUIPageChanged="OnEktronUIPageChanged" 
             Caption="<%$ Resources:SelectableStyles %>">
            <Columns>
                <ektronUI:CheckBoxField ID="SelectStyle">
                   <%-- <CheckBoxFieldItemStyle CheckedBoundField="IsStyleSelected" />xOnCheckedChanged="SelectStyle_CheckedChanged"--%>
                    <KeyFields>
                        <ektronUI:KeyField DataField="Id" />
                    </KeyFields>
                    <ValueFields>
                        <ektronUI:ValueField Key="Id" DataField="Id" />
                        <ektronUI:ValueField Key="Name" DataField="Name" />
                        <ektronUI:ValueField Key="TypeId" DataField="TypeId" />
                        <ektronUI:ValueField Key="Value" DataField="Value" />
                        <ektronUI:ValueField Key="SampleValue" DataField="SampleValue" />
                        <ektronUI:ValueField Key="Taglist" DataField="Taglist" />
                    </ValueFields>
                </ektronUI:CheckBoxField>
                <asp:TemplateField HeaderText="<%$ Resources:Name %>">
                    <ItemTemplate>
                        <a style="text-decoration:none;" href="<%# DataBinder.Eval(Container, "Id") %>" title="<%# DataBinder.Eval(Container, "Id") %>" onclick="<%# this.EditStyleOnClick(Eval("Id")) %>">
                            <span style="text-decoration:underline;"><%# Eval("Name") %></span>
                        </a> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:Type %>" ItemStyle-Width="18%">
                    <ItemTemplate>
                        <asp:Label ID="StyleType" runat="server" Text='<%# this.GetStyleTypeLabel(Eval("TypeId")) %>' CssClass="ContentId item"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:Sample %>">
                    <ItemTemplate>
                        <asp:Label style='<%# Eval("SampleValue") %>' ID="Sample" runat="server" Text='AaBbCcDd'></asp:Label></span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemStyle Width="250px" />
                    <ItemTemplate>
                        <div style="text-align: center; cursor: pointer;">
                            <ektronUI:Button ID="BtnEdit" runat="server" Text="<%$ Resources:Edit %>" ToolTip="<%$ Resources:Edit %>" OnClientClick='<%# this.EditStyleOnClick(Eval("Id")) %>'></ektronUI:Button>
                            <ektronUI:Button ID="BtnAssign" runat="server" Text="<%$ Resources:Assign %>" ToolTip="<%$ Resources:Assign %>"></ektronUI:Button>
                            <ektronUI:Button ID="BtnDelete" runat="server" Text="<%$ Resources:Delete %>" ToolTip="<%$ Resources:Delete %>" OnClientClick='<%# this.DeleteStyleOnClick(Eval("Id")) %>'></ektronUI:Button>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </ektronUI:GridView>
        <ektronUI:JavaScriptBlock ID="JavaScriptBlock1" runat="server" ExecutionMode="OnParse">
            <ScriptTemplate>
                if ("undefined" == typeof (Ektron)) { Ektron = {}; }
                if ("undefined" == typeof (Ektron.Workarea)) { Ektron.Workarea = {}; }
                if ("undefined" == typeof (Ektron.Workarea.Settings)) { Ektron.Workarea.Settings = {}; }
                Ektron.Workarea.Settings.AuthoringStyles = {
                    confirmDelete: function(id){
                        var jCurr, aId = new Array, val, obj, 
                            multiItemsMsg = '<asp:Literal ID="uxDeleteMultiItemsMsg" runat="server" Text="<%$ Resources:ConfirmDeletePlural %>" />',
                            singleItemMsg = '<asp:Literal ID="uxDeleteSingleItemMsg" runat="server" Text="<%$ Resources:ConfirmDelete %>" />';
                        if ('undefined' == typeof id) {
                            $ektron('.ektron-ui-gridview-checkbox').each(function( index ) { 
                                jCurr = $ektron(this);
                                if (jCurr.is(':checked')) {
                                    val = jCurr.siblings('.ektron-ui-gridview-checkbox-data').val();
                                    obj = $ektron.parseJSON(val);
                                    aId.push(obj.KeyFields.Id);
                                }
                            });
                            id = aId.join(',');
                        }

                        if (id.indexOf(',') > 0) {
                            $ektron("<%# uxDeleteStyleDialog.Selector %>").html(multiItemsMsg);
                        }
                        else {
                            $ektron("<%# uxDeleteStyleDialog.Selector %>").html(singleItemMsg);
                        }

                        if (id.length > 0) {
                            $ektron("<%# uxDeleteStyleDialog.Selector %>").data("style_id", id);
                            $ektron("<%# uxDeleteStyleDialog.Selector %>").dialog("open");
                        }
                    },
                    delete: function(){
                        var id = $ektron("<%# uxDeleteStyleDialog.Selector %>").data("style_id");
                        window.location = "styles.aspx?view=delete&id=" + id;
                    },
                    edit: function(obj){
                     $ektron("<%# uxAddEditStyleDialog.Selector %>").dialog({ title: "<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:strHeadingEditStyle %>" />" });
                     $ektron("#uxAddEditIframe").attr('src', 'Admin_AddEditStyle.aspx?styleid='+ obj);
                     $ektron("<%# uxAddEditStyleDialog.Selector %>").dialog("open");
                    }
                } 
                function OpenAddDialog() {
                     $ektron("#uxAddEditIframe").attr('src', 'Admin_AddEditStyle.aspx');
                     $ektron("<%# uxAddEditStyleDialog.Selector %>").dialog({ title: "<asp:Literal ID="litAddStyle" runat="server" Text="<%$ Resources:strHeadingAddStyle %>" />" });
                     $ektron("<%# uxAddEditStyleDialog.Selector %>").dialog("open");
                }
                //Close Add/Edit modal - reload page if changed
                function CloseAddDialog(flag) { 
                     $ektron("<%# uxAddEditStyleDialog.Selector %>").dialog("close");
                    if (flag) {
                        window.location.reload();
                    }
                }
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
        <ektronUI:Dialog ID="uxDeleteStyleDialog" runat="server" Modal="true" Draggable="true" Resizable="false" AutoOpen="false" Title="<%$ Resources:DeleteStyle %>">
            <Buttons>
                <ektronUI:DialogButton ID="uxDeleteStyleButton" runat="server" OnClientClick="Ektron.Workarea.Settings.AuthoringStyles.delete();return false;" Text="<%$ Resources:Delete %>" />
                <ektronUI:DialogButton ID="DialogButton1" runat="server" Text="<%$ Resources:Cancel %>" CloseDialog="true" />
            </Buttons>
            <ContentTemplate>
                <asp:Literal ID="uxDeleteMsg" runat="server" Text="<%$ Resources:ConfirmDelete %>" />
            </ContentTemplate>
        </ektronUI:Dialog>
        <%--<ektronUI:Button ID="BtnGetByTagList" runat="server" Text="Get By Tag List" OnClick="btnGetListByTagList_Clicked" OnClientClick="return false;"></ektronUI:Button>--%>
        <ektronUI:Dialog ID="uxAddEditStyleDialog" runat="server" Width="620" Height="580" Modal="true" Draggable="true" Resizable="true" AutoOpen="false" Title="<%$ Resources:strHeadingAddStyle %>">
            <ContentTemplate>
                <ektronUI:Iframe runat="server" ID="uxAddEditIframe" Src="../blank.htm" Height="520" Width="580" />
            </ContentTemplate>
        </ektronUI:Dialog>
    </form>
</body>
</html>
