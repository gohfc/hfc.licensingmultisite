﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Settings.Authoring;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using Microsoft.Security.Application;

public partial class styles : Ektron.Cms.Workarea.workareabase
{
    protected string AppPath = "";

    private Ektron.Cms.PagingInfo GridPagingInfo
    {
        get
        {
            if (ViewState["GridPagingInfo"] == null)
            {
                Ektron.Cms.PagingInfo info = new Ektron.Cms.PagingInfo();
                info.RecordsPerPage = 20;
                ViewState["GridPagingInfo"] = info;
            }
            return ViewState["GridPagingInfo"] as Ektron.Cms.PagingInfo;
        }
        set { ViewState["GridPagingInfo"] = value; }
    }

    protected override void Page_Load(object sender, EventArgs e)
    {
        SiteAPI m_refSiteApi = new SiteAPI();
        AppPath = m_refSiteApi.AppPath;
        try
        {
            if (!Utilities.ValidateUserLogin())
            {
                return;
            }
            if (m_refContentApi.UserId == 0 || m_refContentApi.RequestInformationRef.IsMembershipUser > 0)
            {
                Response.Redirect("login.aspx?fromLnkPg=1", false);
                return;
            }

            SetTitleBarToMessage("lbl basic content style");
            AddButton((string)(m_refContentApi.AppPath + "images/UI/Icons/add.png"), "javascript:OpenAddDialog()", (string)(m_refMsg.GetMessage("lbl add style")), (string)(m_refMsg.GetMessage("lbl add style")), "", StyleHelper.AddButtonCssClass, true);
            AddButton((string)(m_refContentApi.AppPath + "images/UI/Icons/add.png"), "javascript:OpenAddDialog()", (string)(m_refMsg.GetMessage("lbl import styles")), (string)(m_refMsg.GetMessage("lbl import styles")), "", StyleHelper.AddButtonCssClass, true);
            AddButton((string)(m_refContentApi.AppPath + "images/UI/Icons/add.png"), "javascript:OpenAddDialog()", (string)(m_refMsg.GetMessage("lbl assign")), (string)(m_refMsg.GetMessage("lbl assign")), "", StyleHelper.AddButtonCssClass, false);
            AddButton((string)(m_refContentApi.AppPath + "images/UI/Icons/delete.png"), "javascript:Ektron.Workarea.Settings.AuthoringStyles.confirmDelete();", (string)(m_refMsg.GetMessage("lbl delete")), (string)(m_refMsg.GetMessage("lbl delete")), "", StyleHelper.AddButtonCssClass, false);
            AddHelpButton("style_viewlist");

            string view = "list";
            if (!String.IsNullOrEmpty(Request.QueryString["view"]))
            {
                view = AntiXss.UrlEncode(Request.QueryString["view"].ToLower());
            }

            if (!this.IsPostBack && view == "list")
            {
                LoadAllStyles();
            }
            else if (view == "delete")
            {
                DeleteStyle();
            }
        }
        catch (Exception ex)
        {
            EkException.ThrowException(ex);
        }

        base.Page_Load(sender, e);
    }

    protected void SelectStyle_CheckedChanged(object sender, EventArgs e) { }

    protected void OnEktronUIPageChanged(Object sender, GridViewEktronUIThemePageChangedEventArgs e)
    {
        GridPagingInfo = e.PagingInfo;
    }

    protected string DeleteStyleOnClick(object Id)
    {
        return "Ektron.Workarea.Settings.AuthoringStyles.confirmDelete('" + Id.ToString() + "'); return false;";
    }

    protected string EditStyleOnClick(object Id)
    {
        return "Ektron.Workarea.Settings.AuthoringStyles.edit('" + Id.ToString() + "'); return false;";
    }

    protected string GetStyleTypeLabel(object TypeId)
    {
        string retTypeName = GetLocalResourceObject("InlineStyle").ToString();
        switch (TypeId.ToString())
        {
            case "2":
                retTypeName = GetLocalResourceObject("IdStyle").ToString();
                break;
            case "1":
                retTypeName = GetLocalResourceObject("ClassStyle").ToString();
                break;
            case "0":
            default:
                break;
        }
        return retTypeName;
    }

    private void DeleteStyle()
    {
        StyleManager sm = new StyleManager();
        string[] aStyle_id = Request.QueryString["id"].Split(',');
        foreach (string id in aStyle_id)
        {
            long style_id = 0;
            Int64.TryParse(AntiXss.UrlEncode(id), out style_id);
            if (style_id > 0)
            {
                sm.Delete(style_id);
            }
        }

        LoadAllStyles();
    }

    //protected void btnGetListByTagList_Clicked(object sender, EventArgs e)
    //{
    //    StyleManager sm = new StyleManager();
    //    StyleCriteria criteria = new StyleCriteria();
    //    criteria.AddFilter(Ektron.Cms.Common.StyleProperty.Taglist, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, "div");

    //    List<StyleData> styleData = sm.GetList(criteria);
    //}

    private void LoadAllStyles()
    {
        StyleManager sm = new StyleManager();
        StyleCriteria criteria = new StyleCriteria();
        criteria.AddFilter(Ektron.Cms.Common.StyleProperty.Id, Ektron.Cms.Common.CriteriaFilterOperator.GreaterThan, 0);
        criteria.PagingInfo = GridPagingInfo;

        List<StyleData> styleData = sm.GetList(criteria);

        if (styleData.Count > 0)
        {
            StylesList.EktronUIPagingInfo = GridPagingInfo;
            StylesList.DataSource = styleData;
            StylesList.DataBind();
        }
        else
        {
            uxInformationMessage.Visible = true;
        }
    }

    private void Util_RegisterResources()
    {
        Packages.EktronCoreJS.Register(this);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaIeCss, Ektron.Cms.API.Css.BrowserTarget.AllIE);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
        
    }
}