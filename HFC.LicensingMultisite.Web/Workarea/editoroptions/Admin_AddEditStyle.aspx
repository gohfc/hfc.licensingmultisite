﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Admin_AddEditStyle.aspx.cs" Inherits="Admin_AddEditStyle" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <style>
            .editCtrls input, .editCtrls textarea {
                width: 420px;
            }

            .editCtrls textarea {
                height: 80px;
            }

            #pnlConnNameArea * td.tdwide {
                width: 80px;
            }
        </style>
        <script type="text/javascript">


        </script>
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <ektronUI:JavaScriptBlock ID="uxOpenDialogScript" runat="server" ExecutionMode="OnParse">
            <ScriptTemplate>              
                function CloseAddDialog(flag){ 
                        parent.CloseAddDialog(flag);
            }

                function OpenHelpDialog(){alert('me');
                $ektron("<%= uxHelpDialog.Selector %>").dialog("open");
            }
            
                function CloseHelpDialog(){
                $ektron("<%= uxHelpDialog.Selector %>").dialog("close");
            }
            //If Class, use sample value to show preview
            function processupdate() {
                var divholder = $('.styleholder');
                divholder.html('');
                var testnode = $('<span>AaBbCc</span>');
                var stylesampleval = $('[name*="tfSampleValue"]').val();
                testnode.attr('style', stylesampleval);
                divholder.append(testnode);
                var Name = $('[name*="tfStyleName"]').val();
                if (Name) {
                    var a = ' - ' + Name + ' (style)';
                    divholder.append($('<span>' + a + '</span>'));
                }
            }
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
        <asp:HiddenField ID="hidStyleAction" runat="server" Value="0" />
        <ektronUI:Dialog ID="uxHelpDialog" runat="server" Title="Creating Styles" Zindex="300" AutoOpen="false" Modal="true">
            <ContentTemplate>
                <p>
                    <asp:Literal runat="server" ID="ltrCreatingStyles" Text="<%$Resources:strCreatingStyles %>" />
                </p>
            </ContentTemplate>
        </ektronUI:Dialog>
        <asp:Panel runat="server" ID="pnlStyleExists" Visible="false">
            <ektronUI:Message runat="server" ID="msgStyleExists">
            </ektronUI:Message>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlConnNameArea">
            <div style="padding-left: 50px;">
                <table>
                    <tbody>
                        <tr>
                            <td  class="tdwide" style="text-align: right; padding-right: 10px;">
                                <asp:Literal runat="server" ID="ltrTBStyleName" Text="<%$Resources:tbStyleName %>" />

                            </td>
                            <td>
                                <ektronUI:TextField runat="server" ID="tfStyleName" CssClass="editCtrls">
                                <ValidationRules>
                                    <ektronUI:RequiredRule />
                                </ValidationRules>
                                </ektronUI:TextField>
                            </td>

                        </tr>
                        <tr>
                            <td style="text-align: right; padding-right: 10px;">
                                <asp:Literal runat="server" ID="ltrTBType" Text="<%$Resources:tbType %>" /></td>
                            <td>
                                <asp:DropDownList ID="uxStyleType" runat="server">
                                    <asp:ListItem>Inline style</asp:ListItem>
                                    <asp:ListItem>Class</asp:ListItem>
                                    <asp:ListItem>ID</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="text-align: right; padding-right: 10px;">
                                <asp:Literal runat="server" ID="ltrTBApplyTo" Text="<%$Resources:tbApplyTo %>" />
                            </td>
                            <td style="vertical-align: middle;">
                                <asp:Literal runat="server" ID="ltrTBCommaSeparated" Text="<%$Resources:tbCommaSeparated %>" /><img id="Img1" runat="server" src="~/Workarea/images/UI/Icons/information.png" title="<%$Resources:strApplyToSample %>" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <ektronUI:TextField runat="server" ID="tfApplyTo" CssClass="editCtrls" TextMode="MultiLine">
                                <ValidationRules>
                                    <ektronUI:RequiredRule />
                                </ValidationRules>
                                </ektronUI:TextField>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; vertical-align: top; padding-right: 10px;">
                                <asp:Literal runat="server" ID="ltrTBValue" Text="<%$Resources:tbValue %>" /></td>
                            <td>
                                <ektronUI:TextField runat="server" ID="tfValue" CssClass="editCtrls" TextMode="MultiLine">
                                <ValidationRules>
                                    <ektronUI:RequiredRule />
                                </ValidationRules>
                                </ektronUI:TextField></td>
                        </tr>
                        <tr>
                            <td style="text-align: right; vertical-align: top; padding-right: 10px;">
                                <asp:Literal runat="server" ID="ltrTBSampleValue" Text="<%$Resources:tbSampleValue %>" /></td>
                            <td>
                                <ektronUI:TextField runat="server" ID="tfSampleValue" CssClass="editCtrls" TextMode="MultiLine">
                                </ektronUI:TextField></td>
                        </tr>
                        <tr>
                            <td style="text-align: right; vertical-align: top; padding-right: 10px;">
                                <asp:Literal runat="server" ID="ltrBTNPreview" Text="<%$Resources:tbPreview %>" /></td>
                            <td>
                                <ektronUI:Button runat="server" ID="btnUPdate" Text="<%$Resources:btnUpdate %>" OnClientClick="processupdate(); return false;" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div style="min-height: 15px;" class="styleholder"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlButtonArea" Style="margin-top: 10px; text-align: right; margin-bottom: 15px;">
            <ektronUI:Button runat="server" ID="btnSave" Text="<%$Resources:btnSave %>" OnClick="btnSave_Click" Style="margin-right: 20px;" />
            <ektronUI:Button runat="server" ID="btnCancel" Text="<%$Resources:btnCancel %>" OnClientClick="CloseAddDialog(false); return false;" />
        </asp:Panel>
    </form>
</body>
</html>
