﻿using Ektron.Cms.Framework.UI;
using Ektron.Cms.Settings.Authoring;
using System;
using System.Collections.Generic;


public partial class Admin_AddEditStyle : System.Web.UI.Page
{

    #region Member Variables - Private
    private StyleManager stylemanager = new StyleManager();
    private long styleId = 0;
    #endregion

    protected long StyleId
    {
        get
        {
            return styleId;
        }
        set
        {
            styleId = value;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        try
        {
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            SetUIText();
        }
        catch (Exception ex)
        {
            //GeneralExceptionHandling(ex);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //if styleId is  0, check before Add, if > 0, update
            if (StyleId == 0)
            {
                StyleCriteria sc = new StyleCriteria();
                sc.AddFilter(Ektron.Cms.Common.StyleProperty.Name, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, tfStyleName.Text.Trim());

                List<StyleData> checkfor = stylemanager.GetList(sc);
                if (checkfor.Count > 0)
                {
                    pnlStyleExists.Visible = true;
                    msgStyleExists.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                    msgStyleExists.Text = GetLocalResourceObject("strStyleExists").ToString();
                }

                else
                {
                    StyleData currentStyle = BuildStyleData(StyleId);
                    stylemanager.Add(currentStyle);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "closeaddedit", "CloseAddDialog(true);", true);
                }
            }
            else
            {
                StyleData currentStyle = BuildStyleData(StyleId);
                stylemanager.Update(currentStyle);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "closeaddedit", "CloseAddDialog(true);", true);
            }
        }

        catch (Exception ex)
        {
            string x = ex.Message;
        }
    }
    //Build modal header
    private void SetUIText()
    {
        string x = Request.QueryString["styleid"];
        if (long.TryParse(x, out styleId))
        {
            PopulateFields(styleId);
            pnlStyleExists.Visible = true;
            msgStyleExists.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
            msgStyleExists.Text = GetLocalResourceObject("strEditStyleMsg").ToString();
        }
        //Can I add help button here?
        string Title = GetLocalResourceObject("strHeadingAdd").ToString();
    }
    //Populate fields for edit
    protected void PopulateFields(long id)
    {

        StyleData sd = stylemanager.GetItem(id);
        tfStyleName.Text = sd.Name;
        uxStyleType.SelectedIndex = sd.TypeId;
        tfApplyTo.Value = sd.Taglist;
        tfValue.Value = sd.Value;
        tfSampleValue.Value = sd.SampleValue;

    }
    //Build StyleData object
    private StyleData BuildStyleData(long id)
    {
        //try
        //{
        StyleData sd = new StyleData();
        sd.Name = tfStyleName.Text.Trim();
        sd.TypeId = uxStyleType.SelectedIndex;
        sd.Taglist = tfApplyTo.Text;
        sd.Value = tfValue.Text;
        sd.SampleValue = tfSampleValue.Text;
        sd.Id = id;
        return sd;
        //}
        //catch
        //{
        //    //GeneralExceptionHandling(ex);
        //}

    }

}