﻿<%@ WebHandler Language="C#" Class="CloudSyncRelationShip" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.IO;
using Ektron.Cms.Sync.Client;
using Ektron.FileSync.Common;
using Ektron.Cloud.Amazon.Deployment;
using Ektron.Cloud.Amazon.Deployment.EC2;
using System.ServiceProcess;

public class CloudSyncRelationShip : IHttpHandler
{
    private const string CLOUDPREMISECERTSDIR = "CLOUDPREMISECERTSDIR";
    private string _remoteServer = string.Empty;
    public void StopStartEktronWindowsService()
    {
        ServiceController controller = new ServiceController();
        controller.ServiceName = "EktronWindowsServices40";
        try
        {
            if (controller.Status == ServiceControllerStatus.Running)
            {
                controller.Stop();
                controller.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 1, 30));
            }
            System.Threading.Thread.Sleep(3000);
            if (controller.Status == ServiceControllerStatus.Stopped)
            {
                controller.Start();
            }
        }
        catch
        {
        }
    }
    public void ProcessRequest(HttpContext context)
    {
        long deploymentRelationshipId = 0;
        if (!string.IsNullOrEmpty(context.Request["id"]) && long.TryParse(context.Request["id"], out deploymentRelationshipId))
        {
            System.Threading.Thread.Sleep(new TimeSpan(0, 0, 30));
            StopStartEktronWindowsService();
            Relationship deploymentRelationship = Relationship.GetRelationship(deploymentRelationshipId);
            if (deploymentRelationship != null)
            {
                SyncProfileActions cloudSyncParams = (new CloudParamWrapper()).GetParams(deploymentRelationship, deploymentRelationshipId);
                cloudSyncParams.ExternalArgs = deploymentRelationship.ExternalArgs;
                ServiceArguments deploymentArguments = Ektron.Cloud.Common.Serializer.Deserialize<ServiceArguments>(cloudSyncParams.ExternalArgs);
                InstanceInformation instanceInfo = new Compute(deploymentArguments.AccessKey, deploymentArguments.SecretKey, deploymentArguments.ComputeArguments.Location).GetMachineId(deploymentArguments.ComputeArguments.Name);
                if (instanceInfo != null && !string.IsNullOrEmpty(instanceInfo.PublicDns))
                {
                    string certDir = Ektron.Cloud.Azure.Installer.PathInfo.SyncCertPath + Path.DirectorySeparatorChar + deploymentArguments.ComputeArguments.Name + "-" + CLOUDPREMISECERTSDIR;
                    string[] remoteCertsFiles = Directory.GetFiles(certDir, "*_SyncClient.cer");
                    if (remoteCertsFiles.Length >= 1)
                    {
                        _remoteServer = Path.GetFileName(remoteCertsFiles[0]).Replace("_SyncClient.cer", "");
                    }
                    List<SiteConfiguration> remoteSites = ServerInformation.GetSites(instanceInfo.PublicDns, _remoteServer);
                    if (remoteSites != null && remoteSites.Count == 1)
                    {
                        try
                        {
                            List<SiteConfiguration> localsites = ServerInformation.GetSites(System.Net.Dns.GetHostName(), System.Net.Dns.GetHostName());
                            SiteConfiguration onpremisesite = null;
                            Ektron.Cms.SiteAPI _siteApi = new Ektron.Cms.SiteAPI();
                            foreach (SiteConfiguration localsite in localsites)
                            {
                                if (_siteApi.RequestInformationRef.ConnectionString == localsite.ConnectionString)
                                {
                                    onpremisesite = localsite;
                                }
                            }
                            Relationship syncRelationShip = Relationship.CreateRelationship(onpremisesite, remoteSites[0], instanceInfo.PublicDns, -1, SyncClientSyncDirection.Upload);
                            if (syncRelationShip != null)
                            {
                                ISyncDataProvider provider = DataProviderFactory.Create();
                                provider.SetExternalArgs(syncRelationShip.Id, "<CloudServiceRequest xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                                            "    <SubscriptionID></SubscriptionID>" +
                                            "    <StorageAccountID>" + deploymentArguments.AccessKey + "</StorageAccountID>" +
                                            "    <StorageAccountKey>" + deploymentArguments.SecretKey + "</StorageAccountKey>" +
                                            "    <CDNEndpoint />" +
                                            "    <ContainerConfigName>amazon</ContainerConfigName>" +
                                            "    <WebSites>" +
                                            "        <SiteInfo>" +
                                            "            <ContainerAddress>" + deploymentArguments.StorageArguments.BucketName + "</ContainerAddress >" +
                                            "            <LocalSitePath>" + syncRelationShip.LocalSite.SitePath + "</LocalSitePath>" +
                                            "        </SiteInfo>" +
                                            "    </WebSites>" +
                                            "</CloudServiceRequest>");

                                Profile profile = syncRelationShip.AddProfile(true);
                                profile.Name = _remoteServer + " Template & Database - Upload";
                                profile.SynchronizeDatabase = true;
                                profile.SynchronizeWorkarea = false;
                                profile.SynchronizeTemplates = true;
                                profile.SynchronizeBinaries = false;
                                profile.ConflictResolution = CmsConflictResolutionPolicy.SourceWins;
                                profile.Direction = SyncClientSyncDirection.Upload;
                                profile.Save();
                                provider.SetExternalArgs(profile.Id, "<CloudServiceRequest xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                                            "    <SubscriptionID></SubscriptionID>" +
                                            "    <StorageAccountID>" + deploymentArguments.AccessKey + "</StorageAccountID>" +
                                            "    <StorageAccountKey>" + deploymentArguments.SecretKey + "</StorageAccountKey>" +
                                            "    <CDNEndpoint />" +
                                            "    <ContainerConfigName>amazon</ContainerConfigName>" +
                                            "    <WebSites>" +
                                            "        <SiteInfo>" +
                                            "            <ContainerAddress>" + deploymentArguments.StorageArguments.BucketName + "</ContainerAddress >" +
                                            "            <LocalSitePath>" + syncRelationShip.LocalSite.SitePath + "</LocalSitePath>" +
                                            "        </SiteInfo>" +
                                            "    </WebSites>" +
                                            "</CloudServiceRequest>");
                            }
                        }
                        catch { }
                    }
                    Ektron.Cms.Sync.Presenters.SyncHandlerController.ResultCode result;
                    Ektron.Cms.Sync.Presenters.SyncHandlerController _controller = new Ektron.Cms.Sync.Presenters.SyncHandlerController();
                    _controller.DeleteRelationship(deploymentRelationshipId, out result);
                }
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write("");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}