<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEditCloudScheduleEventItem.aspx.cs"
    Inherits="Ektron.Workarea.Cloud.Deployment.AddEditCloudScheduleEventItem" %>

<%@ Register TagPrefix="ektronUI" Namespace="Ektron.Cms.Framework.UI.Controls.EktronUI" Assembly="Ektron.Cms.Framework.UI.Controls.EktronUI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <ektronUI:JavaScript ID="uxWorakreaJs" runat="server" Path="{WorkareaPath}/java/ektron.workarea.js" />
    <ektronUI:CssBlock ID="uxCloudDeploymentScheduleStyles" runat="server">
        <CssTemplate>
            .ektron-cloud-event { border: #d4d4d4 1px solid; font-size:13px; padding-bottom:1em; }
                        
            .ektron-cloud-event .ektron-ui-odd { background-color: #fff; vertical-align: top; padding: 1em 0;}
            .ektron-cloud-event .ektron-ui-even { background-color: #E6E6E6; vertical-align: top; padding: 1em 0;}
            .ektron-cloud-event .container-left { text-align: right; width: 15%; display: inline-block; padding-right: 1em; vertical-align: top; position: relative; }
            .ektron-cloud-event .container-right { width: 70%; display: inline-block; padding-left: .5em; vertical-align: top; }
            .ektron-cloud-event .container-left .ektron-ui-required { position: absolute; right: .25em;}

            .ektron-cloud-event input { width:10em; }
            .ektron-cloud-event span.wide input { width:20em; }
            .ektron-cloud-event span.medium-width input { width:13.4em; }
            .ektron-cloud-event select.medium-width { width:14.5em; }
            .ektron-cloud-event span.medium2-width input { width:15.5em; }
            .ektron-cloud-event span.small-width input { width:7em; }
            .ektron-cloud-event textarea.description { width: 90%; height: 3em; }
            .ektron-cloud-event textarea.multiline-text { width: 80%; height: 5em; overflow-y: scroll; }
            .ektron-cloud-event input.smaller-width { width:3em; }
            .ektron-cloud-event .buttons { text-align:right;padding-top:1em;margin-top:1em; }
            .ektron-cloud-event p.header { margin-bottom: .75em; }
            .ektron-cloud-event p.caption { margin: .5em 0 .5em 1.1em; font-size: .88em; color: #676767; border: none; }
            .ektron-cloud-event .ui-spinner-input { width: 15px !important; }
            .ektron-cloud-event .datepicker input { width: 6.5em; margin-right: .75em; }
            
            .ektron-cloud-event .ektron-ui-intro { clear: both; display: block; padding: 0.5em 0;}
            .ektron-cloud-event .required-text { text-align: right; display: inline-block; width: 98%; padding-right: 1em; } 
            .ektron-cloud-event .delete-options-list input { width: 1.5em; }
            
            .ektron-cloud-event .description-text {}
            .ektron-cloud-event .date-time-text { }
            .ektron-cloud-event .email-text { }
            .ektron-cloud-event .delete-confirmation-description { min-height: 10em; }
            .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight { background: none !important; background-color: #ffffff !important; margin-bottom: 0; }
            .delete-confirmation { padding: 0 2.25em; }
            .delete-confirmation .ektron-ui-textField input { width: 40em; }
        </CssTemplate>
    </ektronUI:CssBlock>
        <ektronUI:JavaScriptBlock ID="uxJavaScriptBlock" runat="server">
            <ScriptTemplate>
                var descriptionRequired = "<asp:Literal runat='server' ID='ltrDesc' Text='<%$ Resources: ltrDesc %>' />";
                VerifyFieldVal = function()
                {
                    if($ektron("#aspDescriptionTextBox").val() == "")
                    {
                        alert(descriptionRequired); 
                        return false; 
                    }
                    else
                        return true;
                }
            </ScriptTemplate>
        </ektronUI:JavaScriptBlock>
    <div id="pageWarp">
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="aspHeaderTextId" runat="server" Text="<%$ Resources:strPageHeader %>" />
                </span>
                <span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                        <td>
                            <asp:LinkButton ID="aspBackButton" runat="server" CausesValidation="false" CssClass="primary backButton" PostBackUrl="~/Workarea/cloud/CloudScheduledEvents.aspx" ToolTip="<%$ Resources:btnBack %>" Text="<%$ Resources:btnBack %>" />
                        </td>
                        <td>
                            <div class="actionbarDivider">&nbsp;</div>
                        </td>
                        <td class="button">
                            <asp:LinkButton ID="aspSaveButton" runat="server" Text="<%$ Resources:SaveButtonText %>" OnClick="btnSave_Click" ValidationGroup="AddEditCloudEvtValidators" ToolTip="Save" CssClass="primary saveButton" OnClientClick="if(!VerifyFieldVal()) return false;" />
                        </td>
                        <td>
                            <div class="actionbarDivider">&nbsp;</div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <asp:Panel runat="server" ID="pnlEditArea" CssClass="ektron-cloud-event">
            <div class="ektron-ui-quiet required-text">
                <b><asp:Literal ID="aspRequiredText" runat="server" Text="<%$ Resources:RequiredText%>" /></b><span class="ektron-ui-required"> *</span>
            </div>

            <div class="event-data" ><%--class="ektronGrid"--%>
                <div class="ektron-ui-odd">
                    <div class="container-left ektron-ui-loud description-text">
                        <asp:Literal runat="server" ID="aspDescriptionText" Text ="<%$ Resources:strDescription %>" />: <span class="ektron-ui-required">*</span>
                    </div>
                    <div class="container-right">
                        <asp:TextBox ID="aspDescriptionTextBox" runat="server" ValidationGroup="AddEditCloudEvtValidators" MaxLength="150" Wrap="true" CssClass="description" TextMode="MultiLine" />
                        <ektronUI:ValidationMessage ID="uxDescriptionRequired" runat="server" AssociatedControlID="aspDescriptionTextBox"></ektronUI:ValidationMessage>
                    </div>
                </div>
                <div class="ektron-ui-even">
                    <div class="container-left ektron-ui-loud">
                        <asp:Literal runat="server" ID="aspEventType" Text ="<%$ Resources:strEventType %>" />:
                    </div>
                    <div class="container-right">
                        <div class="event-type-list">
                            <asp:DropDownList ID="ddlEvtType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="EventTypeSelectionChanged">
                                <asp:ListItem Text="<%$ Resources:aspEventTypeEditInstance %>" Value="1" Selected="True" />
                                <asp:ListItem Text="<%$ Resources:aspEventTypeDeploy %>" Value="0" />
                                <asp:ListItem Text="<%$ Resources:aspEventTypeDeleteDeployment %>" Value="3" />
                            </asp:DropDownList>
                        </div>
                        <div class="delete-options-list">
                            <span class="ektron-ui-quiet">
                                <asp:Literal ID="aspDeleteOptionsDescription" runat="server" Text="<%$ Resources:aspDeleteOptionsDescriptionText %>" />
                            </span>
                            <asp:CheckBoxList runat="server" ID="aspDeleteOptions" >
                                <asp:ListItem Text="Relationships" Value="1" />
                                <asp:ListItem Text="<%$ Resources:aspDeleteOptionsService %>" Value="2" />
                                <asp:ListItem Text="<%$ Resources:aspDeleteOptionsStorage %>" Value="8" />
                                <asp:ListItem Text="Database" Value="4" />
                            </asp:CheckBoxList>
                        </div>
                    </div>
                </div>
                <div class="ektron-ui-odd">
                    <div class="container-left ektron-ui-loud">
                        <asp:Literal runat="server" ID="Literal2" Text ="<%$ Resources:strNumberOfIns %>" />:
                    </div>
                    <div class="container-right">
                        <ektronUI:IntegerField runat="server" ID="tbNumOfInst" MinValue="1" CssClass="smaller-width" />
                    </div>
                </div>
                <div class="ektron-ui-even">
                    <div class="container-left ektron-ui-loud date-time-text">
                        <asp:Literal runat="server" ID="aspDateAndTime" Text ="<%$ Resources:strSchDT %>" />: <span class="ektron-ui-required">*</span>
                    </div>
                    <div class="container-right">
                        <ektronUI:Datepicker ID="uxEventDate" runat="server" MinDate="<%# DateTime.Now.ToShortDateString() %>" CssClass="datepicker" ValidationGroup="AddEditCloudEvtValidators">
                            <ValidationRules>
                                <ektronUI:RequiredRule />
                            </ValidationRules>
                        </ektronUI:Datepicker>
                        <asp:DropDownList ID="aspEditHour" runat="server" >
                            <asp:ListItem Text="00" Value="00" Selected="True" />
                            <asp:ListItem Text="01" Value="01" />
                            <asp:ListItem Text="02" Value="02" />
                            <asp:ListItem Text="03" Value="03" />
                            <asp:ListItem Text="04" Value="04" />
                            <asp:ListItem Text="05" Value="05" />
                            <asp:ListItem Text="06" Value="06" />
                            <asp:ListItem Text="07" Value="07" />
                            <asp:ListItem Text="08" Value="08" />
                            <asp:ListItem Text="09" Value="09" />
                            <asp:ListItem Text="10" Value="10" />
                            <asp:ListItem Text="11" Value="11" />
                            <asp:ListItem Text="12" Value="12" />
                            <asp:ListItem Text="13" Value="13" />
                            <asp:ListItem Text="14" Value="14" />
                            <asp:ListItem Text="15" Value="15" />
                            <asp:ListItem Text="16" Value="16" />
                            <asp:ListItem Text="17" Value="17" />
                            <asp:ListItem Text="18" Value="18" />
                            <asp:ListItem Text="19" Value="19" />
                            <asp:ListItem Text="20" Value="20" />
                            <asp:ListItem Text="21" Value="21" />
                            <asp:ListItem Text="22" Value="22" />
                            <asp:ListItem Text="23" Value="23" />
                        </asp:DropDownList>
                        <span>:</span>
                        <asp:DropDownList ID="aspEditMinute" runat="server">
                            <asp:ListItem Text="00" Value="00" Selected="True" />
                            <asp:ListItem Text="01" Value="01" />
                            <asp:ListItem Text="02" Value="02" />
                            <asp:ListItem Text="03" Value="03" />
                            <asp:ListItem Text="04" Value="04" />
                            <asp:ListItem Text="05" Value="05" />
                            <asp:ListItem Text="06" Value="06" />
                            <asp:ListItem Text="07" Value="07" />
                            <asp:ListItem Text="08" Value="08" />
                            <asp:ListItem Text="09" Value="09" />
                            <asp:ListItem Text="10" Value="10" />
                            <asp:ListItem Text="11" Value="11" />
                            <asp:ListItem Text="12" Value="12" />
                            <asp:ListItem Text="13" Value="13" />
                            <asp:ListItem Text="14" Value="14" />
                            <asp:ListItem Text="15" Value="15" />
                            <asp:ListItem Text="16" Value="16" />
                            <asp:ListItem Text="17" Value="17" />
                            <asp:ListItem Text="18" Value="18" />
                            <asp:ListItem Text="19" Value="19" />
                            <asp:ListItem Text="20" Value="20" />
                            <asp:ListItem Text="21" Value="21" />
                            <asp:ListItem Text="22" Value="22" />
                            <asp:ListItem Text="23" Value="23" />
                            <asp:ListItem Text="24" Value="24" />
                            <asp:ListItem Text="25" Value="25" />
                            <asp:ListItem Text="26" Value="26" />
                            <asp:ListItem Text="27" Value="27" />
                            <asp:ListItem Text="28" Value="28" />
                            <asp:ListItem Text="29" Value="29" />
                            <asp:ListItem Text="30" Value="30" />
                            <asp:ListItem Text="31" Value="31" />
                            <asp:ListItem Text="32" Value="32" />
                            <asp:ListItem Text="33" Value="33" />
                            <asp:ListItem Text="34" Value="34" />
                            <asp:ListItem Text="35" Value="35" />
                            <asp:ListItem Text="36" Value="36" />
                            <asp:ListItem Text="37" Value="37" />
                            <asp:ListItem Text="38" Value="38" />
                            <asp:ListItem Text="39" Value="39" />
                            <asp:ListItem Text="40" Value="40" />
                            <asp:ListItem Text="41" Value="41" />
                            <asp:ListItem Text="42" Value="42" />
                            <asp:ListItem Text="43" Value="43" />
                            <asp:ListItem Text="44" Value="44" />
                            <asp:ListItem Text="45" Value="45" />
                            <asp:ListItem Text="46" Value="46" />
                            <asp:ListItem Text="47" Value="47" />
                            <asp:ListItem Text="48" Value="48" />
                            <asp:ListItem Text="49" Value="49" />
                            <asp:ListItem Text="50" Value="50" />
                            <asp:ListItem Text="51" Value="51" />
                            <asp:ListItem Text="52" Value="52" />
                            <asp:ListItem Text="53" Value="53" />
                            <asp:ListItem Text="54" Value="54" />
                            <asp:ListItem Text="55" Value="55" />
                            <asp:ListItem Text="56" Value="56" />
                            <asp:ListItem Text="57" Value="57" />
                            <asp:ListItem Text="58" Value="58" />
                            <asp:ListItem Text="59" Value="59" />
                        </asp:DropDownList>
                        <div class="ektron-ui-quiet">
                            <asp:Literal ID="aspDateAndTimeDescription" runat="server" Text="<%$ Resources:aspDateAndTimeDescriptionText %>" />
                        </div>
                    </div>
                </div>
                <div class="ektron-ui-odd">
                    <div class="container-left ektron-ui-loud email-text">
                        <asp:Literal runat="server" ID="aspEmail" Text ="<%$ Resources:strEmail %>" />:
                    </div>
                    <div class="container-right">
                        <asp:TextBox ID="aspEmailsTextBox" runat="server" CssClass="multiline-text" TextMode="MultiLine"></asp:TextBox>
                        <div class="ektron-ui-quiet">
                            <asp:Literal ID="aspEmailDescription" runat="server" Text="<%$ Resources:aspEmailDescriptionText %>" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <ektronUI:Dialog runat="server" ID="uxDeleteConfirmDialog" Width="730" Resizable="false" Modal="true" Title="<%$ Resources:uxDeleteConfirmDialogTitle %>" >
            <ContentTemplate>
                <div class="delete-confirmation-description">
                    <ektronUI:Message runat="server" ID="uxDeleteConfirmText" Text="" DisplayMode="Warning" />
                </div>
                <div class="delete-confirmation">
                    <ektronUI:TextField runat="server" ID="uxDeleteConfirm" ValidationGroup="valDeleteConfirm">
                        <ValidationRules>
                            <ektronUI:RequiredRule />
                        </ValidationRules>
                    </ektronUI:TextField>
                </div>
            </ContentTemplate>
            <Buttons>
                <ektronUI:DialogButton ID="uxDeleteButton" CloseDialog="false" Text="<%$ Resources:uxDeleteButtonText %>" OnClick="btnDeleteConfirm_Click" ValidationGroup="valDeleteConfirm"/>
                <ektronUI:DialogButton ID="uxCancelButton" CloseDialog="true" Text="<%$ Resources:uxCancelButtonText %>" />
            </Buttons>
        </ektronUI:Dialog>
    </div>
    </form>
</body>
</html>
