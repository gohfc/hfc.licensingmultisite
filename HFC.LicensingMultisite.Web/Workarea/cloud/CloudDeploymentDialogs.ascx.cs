﻿namespace Ektron.Workarea.Cloud
{
    using System;
    using System.Collections.Generic;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Interfaces.Context;

    public partial class DeploymentDialogs : System.Web.UI.UserControl
    {
        public Ektron.FileSync.Common.ProfileType CloudProfileType
        {
            get;
            set;
        }

        public string HandlerPath
        {
            get
            {
                ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
                return cmsContextService.WorkareaPath + "/cloud/CloudDeploymentHandler.ashx";
            }
        }

        protected override void OnInit(EventArgs e)
        {
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.StringObject.Register(this);
            base.OnInit(e);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.CloudProfileType == Ektron.FileSync.Common.ProfileType.Amazon)
            {
                this.aspConfigureInstanceNote.Text = GetLocalResourceObject("uxConfigureInstanceNoteAmazon").ToString();
                this.uxlblDeleteWarningMessage.Text = GetLocalResourceObject("uxDeleteRelationshipConfirmationMessageTextAmazon").ToString();
            }
            else if (this.CloudProfileType == Ektron.FileSync.Common.ProfileType.Azure)
            {
                this.aspConfigureInstanceNote.Text = GetLocalResourceObject("uxConfigureInstanceNoteAzure").ToString();
                this.uxlblDeleteWarningMessage.Text = GetLocalResourceObject("uxDeleteRelationshipConfirmationMessageTextAzure").ToString();
            }

            // Get help link
            StyleHelper sHelper = new StyleHelper();
            this.ltrHelpCloudLoadBalancing.Text = sHelper.GetHelpButton("cloud_configure_load_balancing", String.Empty);
        }

    }
}