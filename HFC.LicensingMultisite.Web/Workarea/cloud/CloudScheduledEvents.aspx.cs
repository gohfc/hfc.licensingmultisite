﻿namespace Ektron.Workarea.Cloud.Deployment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cloud.Common;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.UI;

    public partial class ScheduledEvents : System.Web.UI.Page
    {
        public static class EnumToString
        {
            public static string GetStringValue(Enum value)
            {
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                System.ComponentModel.DescriptionAttribute[] attrs = fi.GetCustomAttributes
               (
                   typeof(System.ComponentModel.DescriptionAttribute),
                   false
               ) as System.ComponentModel.DescriptionAttribute[];
                return attrs.Length > 0 ? attrs[0].Description : null;
            }
        }

        private long ProfileId = 0;
        private bool dataBind = false;
        private CloudServiceRequest viewData;
        private SiteAPI _siteApi;

        #region properties

        protected CloudServiceRequest ViewData
        {
            get
            {
                if (this.viewData == null)
                {
                    viewData = Ektron.DbSync.Core.Serializer.Deserialize<CloudServiceRequest>(Ektron.Cms.Sync.Client.Relationship.GetExternalArgs(ProfileId));
                }
                return this.viewData;
            }
        }

        #endregion

        #region events

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!Utilities.ValidateUserLogin())
            {
                return;
            }
            _siteApi = new SiteAPI();
            if (!_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) &&
                !_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
            {
                Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
            }
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);

            aspViewScheduledCloudEvents.PageIndexChanging += new GridViewPageEventHandler(aspViewScheduledCloudEvents_PageIndexChanging);
            aspViewScheduledCloudEvents.Sorting += new GridViewSortEventHandler(GridSorting);
            aspViewScheduledCloudEvents.RowCommand += new GridViewCommandEventHandler(aspViewScheduledCloudEvents_RowCommand);
        }

        private void aspViewScheduledCloudEvents_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "GotoPage")
            {
                LinkButton lb = e.CommandSource as LinkButton;
                GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                TextBox tb = (TextBox)gvr.FindControl("tbGotoPage");
                int target = -1;
                if (int.TryParse(tb.Text, out target))
                {
                    if (target > 0 && target <= aspViewScheduledCloudEvents.PageSize)
                        aspViewScheduledCloudEvents.PageIndex = target - 1;
                }
                dataBind = true;
            }
        }

        protected void aspViewScheduledCloudEvents_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex >= 0 && e.NewPageIndex < aspViewScheduledCloudEvents.PageCount)
                aspViewScheduledCloudEvents.PageIndex = e.NewPageIndex;
            else if (e.NewPageIndex < 0)
                aspViewScheduledCloudEvents.PageIndex = 0;
            else
                aspViewScheduledCloudEvents.PageIndex = aspViewScheduledCloudEvents.PageSize - 1;
            dataBind = true;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            StyleHelper styleHelper = new StyleHelper();
            aspHelpButton.Text = styleHelper.GetHelpButton("view_azure_host", string.Empty);

            uxDialog.Visible = false;
            long.TryParse(Request.QueryString["profile-id"], out ProfileId);
            btnAddNew.PostBackUrl = "AddEditCloudScheduleEventItem.aspx?profile-id=" + Request.QueryString["profile-id"];
            aspViewScheduledCloudEvents.DataSource = this.ViewData.DeploymentSchedules;

            if (aspViewScheduledCloudEvents.Rows.Count == 0)
            {
                uxDEventsButton.Visible = false;
            }
            else
            {
                uxDEventsButton.Visible = true;
            }

            if (!Page.IsPostBack)
            {
                ToggleDeleteMode(false);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!Page.IsPostBack || dataBind)
            {
                aspViewScheduledCloudEvents.DataBind();
            }
            base.OnPreRender(e);
        }

        protected override void OnError(EventArgs e)
        {
            uxDialog.Visible = true;
            uxDialogMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
            uxDialogMessage.Text = Server.GetLastError().Message;
            //uxDeleteButton.Visible = false;
            base.OnError(e);
        }

        protected void uxDEventsButton_Click(object sender, EventArgs e)
        {
            ToggleDeleteMode(true);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            List<string> itemsToDelete = new List<string>();
            //((System.Web.UI.DataBoundLiteralControl)(row.Cells[0].Controls[0])).Text.Trim()
            CheckBox cb = null;
            DataBoundLiteralControl hf = null;
            foreach (GridViewRow row in aspViewScheduledCloudEvents.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    cb = row.Cells[0].FindControl("cbDelete") as CheckBox;
                    hf = row.Cells[0].Controls[0] as DataBoundLiteralControl;
                    if (cb != null && hf != null)
                    {
                        if (cb.Checked)
                        {
                            string[] parts = hf.Text.Trim().Split('|');
                            itemsToDelete.Add(parts[1]);
                        }

                    }
                }
            }

            viewData.DeploymentSchedules.RemoveAll(x => itemsToDelete.Contains(x.Id.ToString()));
            Ektron.Cms.Sync.Client.Relationship.SetExternalArgs(ProfileId, Ektron.DbSync.Core.Serializer.Serialize<CloudServiceRequest>(ViewData));
            ToggleDeleteMode(false);
            dataBind = true;
        }
        protected void GridSorting(object sender, GridViewSortEventArgs e)
        {
            string sortCol = "Name";
            string sortDir = "ASC";
            if (ViewState["VS_Sort_Col"] != null)
            {
                sortCol = ViewState["VS_Sort_Col"].ToString();
            }
            if (ViewState["VS_Sort_Dir"] != null)
            {
                sortDir = ViewState["VS_Sort_Dir"].ToString();
            }
            //var source = ViewData.DeploymentSchedules.ToList();
            //var sorted source.OrderBy(x=>x.Name);
            IQueryable<CloudProvisioningSchedule> unsorted = ViewData.DeploymentSchedules.AsQueryable();
            if (sortDir == "ASC")
            {
                //var sorted = unsorted.OrderBy(sortCol);
                //aspViewScheduledCloudEvents.DataSource = sorted.ToList();
                aspViewScheduledCloudEvents.DataSource = ViewData.DeploymentSchedules.OrderBy(sortCol).ToList();

            }
            else
            {
                //var sorted = unsorted.OrderByDescending(sortCol);
                //aspViewScheduledCloudEvents.DataSource = sorted.ToList();
                aspViewScheduledCloudEvents.DataSource = ViewData.DeploymentSchedules.OrderByDescending(sortCol).ToList();
            }
            if (sortCol == e.SortExpression)
            {
                if (sortDir == "ASC")
                {
                    sortDir = "DESC";
                    e.SortDirection = System.Web.UI.WebControls.SortDirection.Descending;
                }
                else
                {
                    sortDir = "ASC";
                    e.SortDirection = System.Web.UI.WebControls.SortDirection.Ascending;
                }
            }
            ViewState["VS_Sort_Dir"] = sortDir;
            ViewState["VS_Sort_Col"] = e.SortExpression;
            dataBind = true;
        }


        #endregion

        void ToggleDeleteMode(bool deleteMode)
        {
            if (deleteMode)
            {
                aspViewScheduledCloudEvents.Columns[0].Visible = true;
                btnAddNew.Visible = false;
                uxDEventsButton.Visible = false;
                btnDelete.Visible = true;
            }
            else
            {
                aspViewScheduledCloudEvents.Columns[0].Visible = false;
                btnAddNew.Visible = true;
                uxDEventsButton.Visible = true;
                btnDelete.Visible = false;
            }
        }
    }
    public static class SortExtension
    {
        public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> source, string property) { return ExecSort<T>(source.AsQueryable(), property, "OrderBy"); }
        public static IEnumerable<T> OrderByDescending<T>(this IEnumerable<T> source, string property) { return ExecSort<T>(source.AsQueryable(), property, "OrderByDescending"); }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string property) { return ExecSort<T>(source, property, "OrderBy"); }
        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string property) { return ExecSort<T>(source, property, "OrderByDescending"); }

        private static IOrderedQueryable<T> ExecSort<T>(IQueryable<T> source, string property, string methodName)
        {
            //custom activation of 
            //public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer);


            //build  expression e.g: x=>x.Name
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg; //x

            //get property w/o case
            var properties = from p in type.GetProperties() where p.Name.ToLower() == property.ToLower() select p;
            PropertyInfo pi = properties.ToList()[0]; //not sure the error handling yet, if passing in a name that cannot be found..

            expr = Expression.Property(expr, pi);
            type = pi.PropertyType;
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);
            //call the method
            MethodInfo mi = typeof(Queryable).GetMethods().Single(method => method.Name == methodName && method.IsGenericMethodDefinition && method.GetGenericArguments().Length == 2 && method.GetParameters().Length == 3).MakeGenericMethod(typeof(T), type);
            object result = mi.Invoke(null, new object[] { source, lambda, Create(typeof(CustomComparator<>), type) });
            return (IOrderedQueryable<T>)result;

        }
        private static object Create(Type name, params Type[] types)
        {
            Type generic = name.MakeGenericType(types);
            return Activator.CreateInstance(generic);
        }

    }
    public class CustomComparator<T> : IComparer<T>, IDisposable
    {
        private enum ChunkType { Alphanumeric, Numeric };

        private bool InChunk(char ch, char otherCh)
        {
            ChunkType type = ChunkType.Alphanumeric;

            if (char.IsDigit(otherCh))
            {
                type = ChunkType.Numeric;
            }

            if ((type == ChunkType.Alphanumeric && char.IsDigit(ch))
                || (type == ChunkType.Numeric && !char.IsDigit(ch)))
            {
                return false;
            }

            return true;
        }

        //Memeber of IComparer
        //Natrual sort , need to detect different types such as datetime....
        public int Compare(T x, T y)
        {
            String s1 = getStringFromObject(x);
            String s2 = getStringFromObject(y);

            if (s1 == null || s2 == null)
            {
                return 0;
            }

            int s1Position = 0, thisNumericChunk = 0;
            int s2Position = 0, thatNumericChunk = 0;

            while ((s1Position < s1.Length) || (s2Position < s2.Length))
            {
                if (s1Position >= s1.Length)
                {
                    return -1;
                }
                else if (s2Position >= s2.Length)
                {
                    return 1;
                }
                char thisCh = s1[s1Position];
                char thatCh = s2[s2Position];

                StringBuilder thisChunk = new StringBuilder();
                StringBuilder thatChunk = new StringBuilder();

                while ((s1Position < s1.Length) && (thisChunk.Length == 0 || InChunk(thisCh, thisChunk[0])))
                {
                    thisChunk.Append(thisCh);
                    s1Position++;

                    if (s1Position < s1.Length)
                    {
                        thisCh = s1[s1Position];
                    }
                }

                while ((s2Position < s2.Length) && (thatChunk.Length == 0 || InChunk(thatCh, thatChunk[0])))
                {
                    thatChunk.Append(thatCh);
                    s2Position++;

                    if (s2Position < s2.Length)
                    {
                        thatCh = s2[s2Position];
                    }
                }

                int result = 0;
                // If both chunks contain numeric characters, sort them numerically
                if (char.IsDigit(thisChunk[0]) && char.IsDigit(thatChunk[0]))
                {
                    thisNumericChunk = Convert.ToInt32(thisChunk.ToString());
                    thatNumericChunk = Convert.ToInt32(thatChunk.ToString());

                    if (thisNumericChunk < thatNumericChunk)
                    {
                        result = -1;
                    }

                    if (thisNumericChunk > thatNumericChunk)
                    {
                        result = 1;
                    }
                }
                else
                {
                    result = thisChunk.ToString().CompareTo(thatChunk.ToString());
                }

                if (result != 0)
                {
                    return result;
                }
            }

            return 0;
        }

        private string getStringFromObject(object src)
        {
            string ret = "";

            if (src.GetType() == typeof(DateTime))
            {
                DateTime d = (DateTime)src;
                ret = d.ToShortDateString() + " " + d.ToShortTimeString();
            }
            else if (src.GetType() == typeof(TimeSpan))
            {
                //do somthing
                //ret = "";
            }
            else
            {
                ret = src as string;
            }
            return ret;
        }
        public void Dispose()
        {
        }
    }

}