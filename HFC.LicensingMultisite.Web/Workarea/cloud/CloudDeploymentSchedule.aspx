﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CloudDeploymentSchedule.aspx.cs" Inherits="Ektron.Workarea.Cloud.Deployment.Schedule" culture="auto" meta:resourcekey="PageResourceKey" uiculture="auto" %>
<!DOCTYPE>
<html>
    <head runat="server">
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <form id="form1" runat="server">
            <ektronUI:JavaScript ID="uxWorakreaJs" runat="server" Path="{WorkareaPath}/java/ektron.workarea.js" />
            <div class="ektron-cloud-deployment-schedule">
                <ektronUI:JavaScriptBlock ID="uxCloudDeploymentJs" runat="server">
                    <ScriptTemplate>
                        Ektron.Namespace.Register("Ektron.Workarea.Cloud.Deployment");
                        Ektron.Workarea.Cloud.Deployment.Schedule = {
                            bindEvents: function(){
                                //toggle instance field based on operation value
                                var operation = $ektron(".ektron-cloud-deployment-schedule .operation-drop-down-list");
                                operation.change(function(){
                                    var value = $ektron(this).find("option:selected").prop("value");
                                    var instance = $ektron(this).closest("tr").find(".instance");
                                    switch(value){
                                        case "DeleteDeployment":
                                            //disable
                                            instance.spinner({disabled: true});
                                            break;
                                        case "ChangeConfig":
                                            //enable
                                            instance.spinner({disabled: false});
                                            break;
                                        case "CreateDeployment":
                                            //enable
                                            instance.spinner({disabled: false});
                                            break;
                                    }
                                    
                                });

                                //toggle notification email based on causes notification checkbox
                                $ektron(".ektron-cloud-deployment-schedule .causes-notification > input").click(function(){
                                    var isChecked = $ektron(this).is(":checked");
                                    var email = $ektron(this).closest("tr").find(".email > input");
                                    if(isChecked){
                                        //enable
                                        email.prop("disabled", false).removeClass("aspNetDisabled").removeClass("ui-state-disabled");
                                    } else {
                                        //disable
                                        email.prop("disabled", true).addClass("aspNetDisabled").addClass("ui-state-disabled").removeClass("ektron-ui-invalid").val("");
                                    }
                                });
                            },
                            init:function(){
                                this.bindEvents();
                            },
                            Validation: {
                                NotificationEmail: {
                                    required: function(value, selector, errorMessage){
                                        var isValid = true;
                                        var emailField = $ektron(selector);
                                        var causesNotificationFieldChecked = emailField.closest("tr").find(".causes-notification input:checkbox").prop("checked");
                                        if (value === "" && causesNotificationFieldChecked === true){
                                            isValid = false;
                                        }
                                        return isValid;
                                    }
                                }
                            }
                        };

                        Ektron.Workarea.Cloud.Deployment.Schedule.init();
                    </ScriptTemplate>
                </ektronUI:JavaScriptBlock>
                <ektronUI:CssBlock ID="uxCloudDeploymentScheduleStyles" runat="server">
                    <CssTemplate>
                        .ektron-cloud-deployment-schedule .no-wrap {white-space:nowrap;}
                        .ektron-cloud-deployment-schedule .ektronPageContainer input {width:6em !important;}
                        .ektron-cloud-deployment-schedule table .column-id {width:5%;}
                        .ektron-cloud-deployment-schedule table .column-name {width:10%;}
                        .ektron-cloud-deployment-schedule table .column-operation {width:10%;}
                        .ektron-cloud-deployment-schedule table .column-instance {width:5%;}
                        .ektron-cloud-deployment-schedule table span.ui-spinner {width:4em !important;}
                        .ektron-cloud-deployment-schedule table .column-type {width:15%;}
                        .ektron-cloud-deployment-schedule table .column-date {width:10%;}
                        .ektron-cloud-deployment-schedule table .column-causes-notification {width:5%;}
                        .ektron-cloud-deployment-schedule table .column-notification-email {width:15%;}
                        .ektron-cloud-deployment-schedule table .column-enabled {width:10%;}
                        .ektron-cloud-deployment-schedule table .column-actions {width:15%;}
                        .ektron-cloud-deployment-schedule table th,
                        .ektron-cloud-deployment-schedule table td {vertical-align:middle;}
                    </CssTemplate>
                </ektronUI:CssBlock>
                <div class="ektronPageHeader">
                    <div class="ektronTitlebar" id="divTitleBar" runat="server">
                        <span id="WorkareaTitleBar"><asp:Literal ID="aspHeaderTextId" runat="server" Text="<%$ Resources:ScheduleCloudDeploymentText %>" /></span>
                        <span id="_WorkareaTitleBar" style="display:none;"></span>
                    </div>
                    <div class="ektronToolbar" id="divToolBar" runat="server">
                        <table>
                            <tr>
                                <td><asp:LinkButton ID="aspBackButton" runat="server" CausesValidation="false" CssClass="primary backButton" PostBackUrl="~/Workarea/cloud/CloudDeployment.aspx" ToolTip="<%$ Resources:BackButtonText %>" Text="<%$ Resources:BackButtonText %>" /></td>
                                <td><div class="actionbarDivider"></div></td>
                                <td><asp:ImageButton ID="aspAddButton" runat="server" ImageUrl="~/Workarea/images/UI/Icons/add.png" OnClick="ShowAddUI" ToolTip="<%$ Resources:AddButtonText %>" Text="<%$ Resources:AddButtonText %>" /></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="ektronPageContainer">
                    <asp:ListView 
                        ID="aspScheduledDeployments"
                        runat="server"
                        ItemPlaceholderID="aspItemPlaceholder" 
                        DataSource="<%# this.ViewData.DeploymentSchedules %>"
                        OnItemInserting="Insert"
                        OnItemUpdating="Update"
                        OnItemCanceling="Cancel" 
                        InsertItemPosition="None"
                        OnItemEditing="ShowUpdateUI"
                        OnItemDeleting="ShowDeleteUI">
                        <LayoutTemplate>
                            <table class="ektronGrid">
                                <colgroup>
                                    <col class="column-id" />
                                    <col class="column-name" />
                                    <col class="column-operation" />
                                    <col class="column-instance" />
                                    <col class="column-type" />
                                    <col class="column-date" />
                                    <col class="column-notification-email" />
                                    <col class="column-enabled" />
                                    <col class="column-actions" />
                                </colgroup>
                                <thead>
                                    <tr class="title-header">
                                        <th><asp:Literal ID="aspHeaderTextId" runat="server" Text="<%$ Resources:HeaderTextId %>" /></th>
                                        <th><asp:Literal ID="aspHeaderTextName" runat="server" Text="<%$ Resources:HeaderTextName %>" /></th>
                                        <th><asp:Literal ID="aspHeaderTextOperation" runat="server" Text="<%$ Resources:HeaderTextOperation %>" /></th>
                                        <th><asp:Literal ID="aspHeaderTextInstance" runat="server" Text="<%$ Resources:HeaderTextInstance %>" /></th>
                                        <th><asp:Literal ID="aspHeaderTextType" runat="server" Text="<%$ Resources:HeaderTextType %>" /></th>
                                        <th><asp:Literal ID="aspHeaderTextDate" runat="server" Text="<%$ Resources:HeaderTextDate %>" /></th>
                                        <th><asp:Literal ID="aspHeaderTextNotificationEmail" runat="server" Text="<%$ Resources:HeaderTextNotificationEmail %>" /></th>
                                        <th><asp:Literal ID="aspHeaderTextEnabled" runat="server" Text="<%$ Resources:HeaderTextEnabled %>" /></th>
                                        <th><asp:Literal ID="aspHeaderTextActions" runat="server" Text="<%$ Resources:HeaderTextActions %>" /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:PlaceHolder ID="aspItemPlaceholder" runat="server" />
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# ViewData.DeploymentSchedules.IndexOf(ViewData.DeploymentSchedules.Find(delegate(Ektron.Cloud.Common.CloudProvisioningSchedule s) { return s.Id.ToString() == Eval("Id").ToString(); })) + 1%></td>
                                <td><%# Eval("Name") %></td>
                                <td><%# Ektron.Workarea.Cloud.Deployment.EnumToString.GetStringValue((Ektron.Cloud.Common.CloudCommandType)Eval("Operation"))%> </td>
                                <td><span id="aspDisplayVisible" runat="server" visible='<%# ((Ektron.Cloud.Common.CloudProvisioningSchedule)(Container.DataItem)).Operation == Ektron.Cloud.Common.CloudCommandType.ChangeConfig | ((Ektron.Cloud.Common.CloudProvisioningSchedule)(Container.DataItem)).Operation == Ektron.Cloud.Common.CloudCommandType.CreateDeployment%>'><%# Eval("Instance") %></span></td>
                                <td><%# Eval("Type") %></td>
                                <td class="no-wrap">
                                    <%# ((System.DateTime)Eval("Date")).ToShortDateString() %>
                                    <%# ((System.DateTime)Eval("Date")).ToString("hh:mm tt") %>
                                </td>
                                <td><span id="aspDisplayNotification" runat="server"><%# Eval("NotificationEmail")%></span></td>
                                <td><asp:CheckBox ID="aspDisplayEnabled" runat="server" Checked='<%# Eval("Enabled") %>' Enabled="false" /></td>
                                <td class="no-wrap">
                                    <ektronUI:Button ID="uxEditButton" runat="server" DisplayMode="Anchor" Enabled='<%# (aspScheduledDeployments.EditIndex == -1) && (aspScheduledDeployments.InsertItemPosition == System.Web.UI.WebControls.InsertItemPosition.None) %>' CausesValidation="false" PrimaryIcon="Pencil" ToolTip="<%$ Resources:EditText %>" CommandName="Edit" CommandArgument="<%# ((Ektron.Cloud.Common.CloudProvisioningSchedule)(Container.DataItem)) %>" />
                                    <ektronUI:Button ID="uxDeleteButton" runat="server" DisplayMode="Anchor" Enabled='<%# (aspScheduledDeployments.EditIndex == -1) && (aspScheduledDeployments.InsertItemPosition == System.Web.UI.WebControls.InsertItemPosition.None) %>' CausesValidation="false" PrimaryIcon="CircleClose" ToolTip="<%$ Resources:DeleteText %>" CommandName="Delete" CommandArgument="<%# ((Ektron.Cloud.Common.CloudProvisioningSchedule)(Container.DataItem)) %>" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="aspEditId" runat="server" Value='<%# Eval("Id") %>' />
                                    <%# ViewData.DeploymentSchedules.IndexOf(ViewData.DeploymentSchedules.Find(delegate(Ektron.Cloud.Common.CloudProvisioningSchedule s) { return s.Id.ToString() == Eval("Id").ToString(); })) + 1%>
                                </td>
                                <td class="no-wrap">
                                    <ektronUI:TextField ID="uxEditName" runat="server" Text='<%# Eval("Name") %>'>
                                        <ValidationRules>
                                            <ektronUI:RequiredRule />
                                            <ektronUI:NotJustWhitespaceRule />
                                        </ValidationRules>
                                    </ektronUI:TextField>
                                    <span class="ektron-ui-required">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="aspEditOperation" runat="server" CssClass="operation-drop-down-list" SelectedValue='<%# Eval("Operation") %>'>
                                        <asp:ListItem Text="<%$ Resources:OperationConfigureText %>" Value="ChangeConfig" />
                                        <asp:ListItem Text="<%$ Resources:OperationCreateText %>" Value="CreateDeployment"  />
                                        <asp:ListItem Text="<%$ Resources:OperationDeleteText %>" Value="DeleteDeployment"  />
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <ektronUI:IntegerField ID="uxEditInstance" runat="server" MinValue="1" Text='<%# Eval("Instance") %>' Enabled='<%# (((Ektron.Cloud.Common.CloudProvisioningSchedule)(Container.DataItem)).Operation == Ektron.Cloud.Common.CloudCommandType.ChangeConfig) %>' CssClass="instance" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="aspEditType" runat="server">
                                        <asp:ListItem Text="<%$ Resources:TypeOneTimeText %>" Value="OneTime" Selected="True" />
                                    </asp:DropDownList>
                                </td>
                                <td class="no-wrap">
                                    <ektronUI:Datepicker ID="uxEditDate" runat="server" MinDate="<%# System.DateTime.Now.ToShortDateString() %>" Text='<%# Eval("Date") %>' />
                                    <asp:DropDownList ID="aspEditHour" runat="server" SelectedValue='<%# ((System.DateTime)Eval("Date")).ToString("HH") %>'>
                                        <asp:ListItem Text="00" Value="00" Selected="True" />
                                        <asp:ListItem Text="01" Value="01" />
                                        <asp:ListItem Text="02" Value="02" />
                                        <asp:ListItem Text="03" Value="03" />
                                        <asp:ListItem Text="04" Value="04" />
                                        <asp:ListItem Text="05" Value="05" />
                                        <asp:ListItem Text="06" Value="06" />
                                        <asp:ListItem Text="07" Value="07" />
                                        <asp:ListItem Text="08" Value="08" />
                                        <asp:ListItem Text="09" Value="09" />
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="11" Value="11" />
                                        <asp:ListItem Text="12" Value="12" />
                                        <asp:ListItem Text="13" Value="13" />
                                        <asp:ListItem Text="14" Value="14" />
                                        <asp:ListItem Text="15" Value="15" />
                                        <asp:ListItem Text="16" Value="16" />
                                        <asp:ListItem Text="17" Value="17" />
                                        <asp:ListItem Text="18" Value="18" />
                                        <asp:ListItem Text="19" Value="19" />
                                        <asp:ListItem Text="20" Value="20" />
                                        <asp:ListItem Text="21" Value="21" />
                                        <asp:ListItem Text="22" Value="22" />
                                        <asp:ListItem Text="23" Value="23" />
                                    </asp:DropDownList>
                                    <span>:</span>
                                    <asp:DropDownList ID="aspEditMinute" runat="server" SelectedValue='<%# ((System.DateTime)Eval("Date")).ToString("mm") %>'>
                                        <asp:ListItem Text="00" Value="00" Selected="True" />
                                        <asp:ListItem Text="01" Value="01" />
                                        <asp:ListItem Text="02" Value="02" />
                                        <asp:ListItem Text="03" Value="03" />
                                        <asp:ListItem Text="04" Value="04" />
                                        <asp:ListItem Text="05" Value="05" />
                                        <asp:ListItem Text="06" Value="06" />
                                        <asp:ListItem Text="07" Value="07" />
                                        <asp:ListItem Text="08" Value="08" />
                                        <asp:ListItem Text="09" Value="09" />
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="11" Value="11" />
                                        <asp:ListItem Text="12" Value="12" />
                                        <asp:ListItem Text="13" Value="13" />
                                        <asp:ListItem Text="14" Value="14" />
                                        <asp:ListItem Text="15" Value="15" />
                                        <asp:ListItem Text="16" Value="16" />
                                        <asp:ListItem Text="17" Value="17" />
                                        <asp:ListItem Text="18" Value="18" />
                                        <asp:ListItem Text="19" Value="19" />
                                        <asp:ListItem Text="20" Value="20" />
                                        <asp:ListItem Text="21" Value="21" />
                                        <asp:ListItem Text="22" Value="22" />
                                        <asp:ListItem Text="23" Value="23" />
                                        <asp:ListItem Text="24" Value="24" />
                                        <asp:ListItem Text="25" Value="25" />
                                        <asp:ListItem Text="26" Value="26" />
                                        <asp:ListItem Text="27" Value="27" />
                                        <asp:ListItem Text="28" Value="28" />
                                        <asp:ListItem Text="29" Value="29" />
                                        <asp:ListItem Text="30" Value="30" />
                                        <asp:ListItem Text="31" Value="31" />
                                        <asp:ListItem Text="32" Value="32" />
                                        <asp:ListItem Text="33" Value="33" />
                                        <asp:ListItem Text="34" Value="34" />
                                        <asp:ListItem Text="35" Value="35" />
                                        <asp:ListItem Text="36" Value="36" />
                                        <asp:ListItem Text="37" Value="37" />
                                        <asp:ListItem Text="38" Value="38" />
                                        <asp:ListItem Text="39" Value="39" />
                                        <asp:ListItem Text="40" Value="40" />
                                        <asp:ListItem Text="41" Value="41" />
                                        <asp:ListItem Text="42" Value="42" />
                                        <asp:ListItem Text="43" Value="43" />
                                        <asp:ListItem Text="44" Value="44" />
                                        <asp:ListItem Text="45" Value="45" />
                                        <asp:ListItem Text="46" Value="46" />
                                        <asp:ListItem Text="47" Value="47" />
                                        <asp:ListItem Text="48" Value="48" />
                                        <asp:ListItem Text="49" Value="49" />
                                        <asp:ListItem Text="50" Value="50" />
                                        <asp:ListItem Text="51" Value="51" />
                                        <asp:ListItem Text="52" Value="52" />
                                        <asp:ListItem Text="53" Value="53" />
                                        <asp:ListItem Text="54" Value="54" />
                                        <asp:ListItem Text="55" Value="55" />
                                        <asp:ListItem Text="56" Value="56" />
                                        <asp:ListItem Text="57" Value="57" />
                                        <asp:ListItem Text="58" Value="58" />
                                        <asp:ListItem Text="59" Value="59" />
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <script type="text/javascript">
                                        uxOkButtonRequired = function (value, selector, errorMessage) {
                                            return Ektron.Workarea.Cloud.Deployment.Schedule.Validation.NotificationEmail.required(value, selector, errorMessage);
                                        }
                                    </script>
                                    <ektronUI:TextField ID="uxEditNotificationEmail" runat="server" Text='<%# Eval("NotificationEmail") %>' CssClass="email">
                                        <ValidationRules>
                                            <ektronUI:CustomRule JavascriptFunctionName="uxOkButtonRequired" ServerValidationEnabled="false" />
                                            <ektronUI:EmailRule />
                                        </ValidationRules>
                                    </ektronUI:TextField>
                                </td>
                                <td>
                                    <asp:CheckBox ID="aspEditEnabled" runat="server" Checked='<%# Eval("Enabled") %>' />
                                </td>
                                <td>
                                    <ektronUI:Button ID="uxOkButton" runat="server" DisplayMode="Anchor" ToolTip="<%$ Resources:OkButtonText %>" CausesValidation="true" CommandName="Update" PrimaryIcon="Check" />
                                    <ektronUI:Button ID="uxCancelButton" runat="server" DisplayMode="Anchor" ToolTip="<%$ Resources:CancelButtonText %>" CausesValidation="false" CommandName="Cancel" PrimaryIcon="Cancel" />
                                </td>
                            </tr>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <tr>
                                <td>-</td>
                                <td class="no-wrap"><ektronUI:TextField ID="uxInsertName" runat="server" Enabled="<%# (aspScheduledDeployments.EditIndex == -1) %>">
                                        <ValidationRules>
                                            <ektronUI:RequiredRule />
                                            <ektronUI:NotJustWhitespaceRule />
                                        </ValidationRules>
                                    </ektronUI:TextField>
                                    <span class="ektron-ui-required">*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="aspInsertOperation" runat="server" CssClass="operation-drop-down-list" Enabled="<%# (aspScheduledDeployments.EditIndex == -1) %>">
                                        <asp:ListItem Text="<%$ Resources:OperationConfigureText %>" Value="ChangeConfig" />
                                        <asp:ListItem Text="<%$ Resources:OperationCreateText %>" Value="CreateDeployment"  />
                                        <asp:ListItem Text="<%$ Resources:OperationDeleteText %>" Value="DeleteDeployment"  />
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <ektronUI:IntegerField ID="uxInsertInstance" runat="server" MinValue="1" CssClass="instance"/>
                                </td>
                                <td>
                                    <asp:DropDownList ID="aspInsertType" runat="server" Enabled="<%# (aspScheduledDeployments.EditIndex == -1) %>">
                                        <asp:ListItem Text="<%$ Resources:TypeOneTimeText %>" Value="OneTime" Selected="True" />
                                    </asp:DropDownList>
                                </td>
                                <td class="no-wrap">
                                    <ektronUI:Datepicker ID="uxInsertDate" runat="server" MinDate="<%# System.DateTime.Now.ToShortDateString() %>" Enabled="<%# (aspScheduledDeployments.EditIndex == -1) %>">
                                        <ValidationRules>
                                            <ektronUI:RequiredRule />
                                        </ValidationRules>
                                    </ektronUI:Datepicker>
                                    <asp:DropDownList ID="aspInsertHour" runat="server" Enabled="<%# (aspScheduledDeployments.EditIndex == -1) %>">
                                        <asp:ListItem Text="00" Value="00" Selected="True" />
                                        <asp:ListItem Text="01" Value="01" />
                                        <asp:ListItem Text="02" Value="02" />
                                        <asp:ListItem Text="03" Value="03" />
                                        <asp:ListItem Text="04" Value="04" />
                                        <asp:ListItem Text="05" Value="05" />
                                        <asp:ListItem Text="06" Value="06" />
                                        <asp:ListItem Text="07" Value="07" />
                                        <asp:ListItem Text="08" Value="08" />
                                        <asp:ListItem Text="09" Value="09" />
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="11" Value="11" />
                                        <asp:ListItem Text="12" Value="12" />
                                        <asp:ListItem Text="13" Value="13" />
                                        <asp:ListItem Text="14" Value="14" />
                                        <asp:ListItem Text="15" Value="15" />
                                        <asp:ListItem Text="16" Value="16" />
                                        <asp:ListItem Text="17" Value="17" />
                                        <asp:ListItem Text="18" Value="18" />
                                        <asp:ListItem Text="19" Value="19" />
                                        <asp:ListItem Text="20" Value="20" />
                                        <asp:ListItem Text="21" Value="21" />
                                        <asp:ListItem Text="22" Value="22" />
                                        <asp:ListItem Text="23" Value="23" />
                                    </asp:DropDownList>
                                    <span>:</span>
                                    <asp:DropDownList ID="aspInsertMinute" runat="server" Enabled="<%# (aspScheduledDeployments.EditIndex == -1) %>">
                                        <asp:ListItem Text="00" Value="00" Selected="True" />
                                        <asp:ListItem Text="01" Value="01" />
                                        <asp:ListItem Text="02" Value="02" />
                                        <asp:ListItem Text="03" Value="03" />
                                        <asp:ListItem Text="04" Value="04" />
                                        <asp:ListItem Text="05" Value="05" />
                                        <asp:ListItem Text="06" Value="06" />
                                        <asp:ListItem Text="07" Value="07" />
                                        <asp:ListItem Text="08" Value="08" />
                                        <asp:ListItem Text="09" Value="09" />
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="11" Value="11" />
                                        <asp:ListItem Text="12" Value="12" />
                                        <asp:ListItem Text="13" Value="13" />
                                        <asp:ListItem Text="14" Value="14" />
                                        <asp:ListItem Text="15" Value="15" />
                                        <asp:ListItem Text="16" Value="16" />
                                        <asp:ListItem Text="17" Value="17" />
                                        <asp:ListItem Text="18" Value="18" />
                                        <asp:ListItem Text="19" Value="19" />
                                        <asp:ListItem Text="20" Value="20" />
                                        <asp:ListItem Text="21" Value="21" />
                                        <asp:ListItem Text="22" Value="22" />
                                        <asp:ListItem Text="23" Value="23" />
                                        <asp:ListItem Text="24" Value="24" />
                                        <asp:ListItem Text="25" Value="25" />
                                        <asp:ListItem Text="26" Value="26" />
                                        <asp:ListItem Text="27" Value="27" />
                                        <asp:ListItem Text="28" Value="28" />
                                        <asp:ListItem Text="29" Value="29" />
                                        <asp:ListItem Text="30" Value="30" />
                                        <asp:ListItem Text="31" Value="31" />
                                        <asp:ListItem Text="32" Value="32" />
                                        <asp:ListItem Text="33" Value="33" />
                                        <asp:ListItem Text="34" Value="34" />
                                        <asp:ListItem Text="35" Value="35" />
                                        <asp:ListItem Text="36" Value="36" />
                                        <asp:ListItem Text="37" Value="37" />
                                        <asp:ListItem Text="38" Value="38" />
                                        <asp:ListItem Text="39" Value="39" />
                                        <asp:ListItem Text="40" Value="40" />
                                        <asp:ListItem Text="41" Value="41" />
                                        <asp:ListItem Text="42" Value="42" />
                                        <asp:ListItem Text="43" Value="43" />
                                        <asp:ListItem Text="44" Value="44" />
                                        <asp:ListItem Text="45" Value="45" />
                                        <asp:ListItem Text="46" Value="46" />
                                        <asp:ListItem Text="47" Value="47" />
                                        <asp:ListItem Text="48" Value="48" />
                                        <asp:ListItem Text="49" Value="49" />
                                        <asp:ListItem Text="50" Value="50" />
                                        <asp:ListItem Text="51" Value="51" />
                                        <asp:ListItem Text="52" Value="52" />
                                        <asp:ListItem Text="53" Value="53" />
                                        <asp:ListItem Text="54" Value="54" />
                                        <asp:ListItem Text="55" Value="55" />
                                        <asp:ListItem Text="56" Value="56" />
                                        <asp:ListItem Text="57" Value="57" />
                                        <asp:ListItem Text="58" Value="58" />
                                        <asp:ListItem Text="59" Value="59" />
                                    </asp:DropDownList>
                                    <span class="ektron-ui-required">*</span>
                                </td>
                                <td>
                                    <script type="text/javascript">
                                        uxInsertButtonRequired = function (value, selector, errorMessage) {
                                            return Ektron.Workarea.Cloud.Deployment.Schedule.Validation.NotificationEmail.required(value, selector, errorMessage);
                                        }
                                    </script>
                                    <ektronUI:TextField ID="uxInsertNotificationEmail" runat="server" Text='<%# Eval("NotificationEmail") %>' CssClass="email" Enabled="false">
                                        <ValidationRules>
                                            <ektronUI:CustomRule JavascriptFunctionName="uxInsertButtonRequired" ServerValidationEnabled="false" />
                                            <ektronUI:EmailRule />
                                        </ValidationRules>
                                    </ektronUI:TextField>
                                </td>
                                <td>
                                    <asp:CheckBox ID="aspInsertEnabled" runat="server" Checked="true" />
                                </td>
                                <td>
                                    <ektronUI:Button ID="uxInsertButton" runat="server" DisplayMode="Anchor" Enabled="<%# (aspScheduledDeployments.EditIndex == -1) %>" ToolTip="<%$ Resources:InsertButtonText %>" CausesValidation="true" CommandName="Insert" PrimaryIcon="PlusThick" />
                                    <ektronUI:Button ID="uxCancelInsertButton" runat="server" DisplayMode="Anchor" ToolTip="<%$ Resources:CancelButtonText %>" CausesValidation="false" CommandName="Cancel" PrimaryIcon="Cancel" />
                                </td>
                            </tr>
                        </InsertItemTemplate>
                        <EmptyDataTemplate>
                            <p>
                                <asp:Literal ID="aspEmptyDataTemplateText" runat="server" Text="<%$ Resources:EmptyDataTemplateText %>" />
                            </p>
                        </EmptyDataTemplate>
                    </asp:ListView>
                    <ektronUI:Dialog ID="uxDialog" runat="server" AutoOpen="true" Visible="false" Draggable="true" Modal="true" Resizable="false" Title="<%$ Resources:DialogTitle %>">
                        <Buttons>
                            <ektronUI:DialogButton ID="uxCancelButton" runat="server" CloseDialog="true" Text="<%$ Resources:CloseDialogButtonText %>" />
                            <ektronUI:DialogButton ID="uxDeleteButton" runat="server" Text="<%$ Resources:DeleteText %>" Visible="false" OnCommand="Delete" />
                        </Buttons>
                        <ContentTemplate>
                            <ektronUI:Message ID="uxDialogMessage" runat="server" DisplayMode="Error" />
                        </ContentTemplate>
                    </ektronUI:Dialog>
                </div>
            </div>
        </form>
    </body>
</html>
