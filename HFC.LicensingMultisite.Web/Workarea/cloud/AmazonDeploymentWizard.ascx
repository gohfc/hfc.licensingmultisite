﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AmazonDeploymentWizard.ascx.cs"
    Inherits="Ektron.Workarea.Cloud.AmazonDeploymentWizard" %>
<%@ Register Assembly="Ektron.Cms.Framework.UI.Controls.EktronUI" Namespace="Ektron.Cms.Framework.UI.Controls.EktronUI"
    TagPrefix="ektronUI" %>
<ektronUI:JavaScript ID="uxWorkareaCloudJs" runat="server" Path="{WorkareaPath}/cloud/js/Ektron.Workarea.Cloud.js" />
<ektronUI:JavaScript ID="uxWorkareaSyncRelationshipJs" Path="{WorkareaPath}/sync/js/Ektron.Workarea.Sync.Relationships.js" />
<ektronUI:JavaScriptBlock ID="uxWorkareaCloudJsBlock" runat="server">
    <scripttemplate> 

        $ektron("#<%= aspStorageSizeLabelExpWeb.ClientID %>").addClass("inFieldLabel").inFieldLabels();
        $ektron("#<%= aspStorageSizeLabelStd.ClientID %>").addClass("inFieldLabel").inFieldLabels(); 
        Ektron.Workarea.Cloud.Dialogs.ServiceSelection.selector = "<%= uxServiceSelection.Selector%>";
        Ektron.Workarea.Cloud.Dialogs.ServiceSelection.amazonserviceradioselector = ".amazonServiceRadio";
        Ektron.Workarea.Cloud.Dialogs.ServiceSelection.windowsazureserviceradio = ".windowsAzureServiceRadio";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step1 = "#<%= pnlStep1.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step2 = "#<%= pnlStep2.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step3 = "#<%= pnlStep3.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step4 = "#<%= pnlStep4.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.awsAccessKey = "#<%= uxAWSKeys.ClientID %> > input";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.awsSecretKey = "#<%= uxAWSSecretKey.ClientID %> > input";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.amazonWizard = "<% = amazonWizard.Selector %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlKeyPair = "#<% = ddlKeyPair.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.keyPairRequired = "#<% = ltrRequiredKeyPair.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBServerName = "#<% = ddlDBServerName.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxNewKeyPair = "#<% = uxNewKeyPair.ClientID %> > input";
        <%--Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxNewKeyPairNameRequired = "#<% = uxNewKeyPairNameRequired.ClientID %>";--%>
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSiteName = "#<% = uxSiteName.ClientID %> > input";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxBucketName = "#<% = uxBucketName.ClientID%> > input";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxVMBucketname = "#<% = uxVMBucketname.ClientID%>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSiteNameRestriction = "#<% = uxSiteNameRestriction.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxDBName = "#<% = uxDBName.ClientID %> > input";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSQLServerUsername = "#<% = uxSQLServerUsername.ClientID %> > input";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSQLServerPassword = ".uxSQLServerPassword > input";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxReenterSQLPassword = ".uxReEnterSQLServerPassword > input";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxServerNamePrefix = "#<% = uxServerNamePrefix.ClientID %> > input";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBInstanceType = "#<% = ddlDBInstanceType.ClientID%>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxRFVDdlDBInstanceType = "#<% = uxRFVDdlDBInstanceType.ClientID%>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBInstanceClass = "#<% = ddlDBInstanceClass.ClientID%>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize = "#<% = uxStorageSize.ClientID %> > input";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSizeLabelExpWeb = "#<% = aspStorageSizeLabelExpWeb.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSizeLabelStd = "#<% = aspStorageSizeLabelStd.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.newSetupItemDB = "#<% = newSetupItemDB.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.pnlDBInstanceType = "#<% = pnlDBInstanceType.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStep1ProgressMsg = "#<% = uxStep1ProgressMsg.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.phErrorMessage = "#<% = phErrorMessage.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStep2ProgressMsg = "#<% = uxStep2ProgressMsg.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.phErrorMessage2 = "#<% = phErrorMessage2.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStep3ProgressMsg = "#<% = uxStep3ProgressMsg.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.phErrorMessage3 = "#<% = phErrorMessage3.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStep4ProgressMsg = "#<% = uxStep4ProgressMsg.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.phStep4ErrorMessage = "#<% = phStep4ErrorMessage.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxAmazonCert = ".amazon-certificate-error";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.profileId = "#<% = profileId.ClientID%>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.divAutoScalingInstances = ".divAutoScalingInstances";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxInstanceCount = "input.minWebInstances";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.vmMinIns = ".vmMinIns";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxAutoScalingMaxInstances = "input.maxWebInstances";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxAutoScalingCheckbox = "#<% = uxAutoScaling.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.encodedVal = "#<%=encodedVal.ClientID %>";

        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlSecurityGroup = "#<% = ddlSecurityGroup.ClientID %>";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.serverValidationErrorMsgStep1 = ".serverValidationErrorMsgStep1";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.serverValidationErrorMsgStep2 = ".serverValidationErrorMsgStep2";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.serverValidationErrorMsgStep3 = ".serverValidationErrorMsgStep3";

    </scripttemplate>
</ektronUI:JavaScriptBlock>
<!-- Select Hosted Service Provider Dialog -->
<ektronUI:Dialog ID="uxServiceSelection" runat="server" Title="<%$ Resources:uxServiceSelectionDialogTitle %>"
    Modal="true" Width="800" AutoOpen="false" CssClass="create-hosted-service-wizard">
    <contenttemplate> 
        <p class="intro"> 
            <asp:Literal ID="ltrTitle" runat="server" Text="<% $Resources: ltrTitle %>" />
            <asp:Literal ID="ltrServiceSelectionHelpButton" runat="server" />
            <asp:Literal runat="server" Text="<% $Resources: ltrServiceQuestion %>" id="ltrServiceQuestion" />
        </p>        
        <div class="amazon-certificate-error">
             <ektronUI:Message ID="uxAmazonCert" runat="server" DisplayMode="Error" 
                    Text="<% $Resources: NoCertificateExists %>" />
        </div>
        <div class="profile-settings-ui">
            <table class="select-cloud-service">
                <tbody>
                    <tr class="amazon-service">
                        <td style="padding-right: 10px;">
                            <input id="amazonServiceRadio" runat="server" type="radio" name="serviceSelectionRadios"
                                checked title="<% $Resources: amazonServiceRadio %>" class="amazonServiceRadio" />
                        </td>
                        <td style="padding: 0 1em 0 1em;">
                            <asp:Image ID="aspAmazonLogo" runat="server" ImageUrl="~/Workarea/cloud/images/Amazon_logo.jpg" 
                             ToolTip="<% $Resources: aspAmazonLogo %>" CssClass="rounded-corner" />
                        </td>
                        <td style="padding-left: 10px;">
                            <asp:Literal ID="ltrAmazonWebServiceDesc" runat="server" Text="<%$ Resources: ltrAmazonWebServiceDesc %>" />
                        </td>
                    </tr>

                    <tr>
                        <td style="padding-right: 10px;">
                            <input id="windowsAzureServiceRadio" runat="server" type="radio" name="serviceSelectionRadios"
                                title="<% $Resources: windowsAzureServiceRadio %>" class="windowsAzureServiceRadio" />
                        </td>
                        <td style="padding: 1em 1em 0 1em">
                            <asp:Image ID="aspWindowsAzureLogo" runat="server" ImageUrl="~/Workarea/cloud/images/windows_azure_logo.jpg"
                                ToolTip="<% $Resources: aspWindowsAzureLogo %>" CssClass="rounded-corner" />
                        </td>
                        <td style="padding-left: 10px;">
                            <asp:Literal ID="ltrWindowsAzureServiceDesc" runat="server" Text="<%$ Resources: ltrWindowsAzureServiceDesc %>" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" runat="server" id="encodedVal" name="encodedVal" />
            <span class="buttonRight" style="margin-top: 2em">
                <ektronUI:Button ID="uxSelectHostedServiceNext" runat="server" OnClientClick="if(!Ektron.Workarea.Cloud.Dialogs.ServiceSelection.next()) return false;"
                   Text="<%$ Resources: uxSelectHostedServiceNext %>" CssClass="primary-amazon-dialog-action" OnClick="uxSelectHostedServiceNext_Click"  DisplayMode="Submit" ToolTip="<%$ Resources: uxSelectHostedServiceNext %>"/>
                <ektronUI:Button ID="uxSelectHostedServiceCancel" runat="server" OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.closeDialog(Ektron.Workarea.Cloud.Dialogs.ServiceSelection.selector);"
                    Text="<%$ Resources: uxDialogCancelButtonText %>" />
            </span>
        </div>
    </contenttemplate>
</ektronUI:Dialog>
<!-- Amazon Cloud UI Starts -->
<ektronUI:Dialog ID="amazonWizard" runat="server" Title="<%$ Resources:AmazonWizardDialogTitle %>"
    Modal="true" Width="700" AutoOpen="false" CssClass="create-amazon-hosted-service-wizard">
    <contenttemplate>

        <asp:UpdatePanel runat="server" ID="aspAmazonUpdatePanel">
            <ContentTemplate>

<!-- Step 1: Amazon Hosted Service -->

                <asp:Panel ID="pnlStep1" CssClass="pnlStep1" runat="server">
                    <div class="ektron-cloud-deployment">
                        <p class="intro">
                            <asp:Literal ID="ltrStep1Title" runat="server" Text="<% $Resources: ltrStep1Title %>" />
                            <asp:Literal ID="ltrStep1DescHelpButton" runat="server" />
                            <asp:Literal ID="ltrStep1Desc" runat="server" Text="<% $Resources: ltrStep1Desc%>" />
                        </p>
                        <ektronUI:Message runat="server" ID="uxStep1ProgressMsg" DisplayMode="Working" Visible="true" CssClass="ektron-ui-hidden" Text="<%$Resources: Progress %>" />

                        <div class="serverValidationErrorMsgStep1">
                        <asp:PlaceHolder ID="phErrorMessage" runat="server"></asp:PlaceHolder>
                        </div>
                        <asp:Panel ID="pnlEditArea" runat="server" CssClass="ektronTopSpace">
                            <span class="ektron-ui-quiet required-text"><b>
                                <asp:Literal ID="ltrRequired" runat="server" Text="<%$ Resources:RequiredText %>" /></b><span
                                    class="ektron-ui-required"> *</span> </span>
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblSiteName" runat="server" Text="<%$ Resources: lblSiteName %>" AssociatedControlID="uxSiteName"></asp:Label>:
                                <span class="ektron-ui-required">*</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxSiteName" runat="server" ValidationGroup="KeyValidation" CssClass="form-text-field">
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources: SiteNameRequired %>" />
                                       <ektronUI:RegexRule ClientValidationEnabled="true" ClientRegex="/^[a-z][a-z0-9]*$/i" ErrorMessage="<%$ Resources: ErrorSitenameValidation %>" />
                                        <ektronUI:MaxLengthRule ClientValidationEnabled="true" MaxLength="128" ErrorMessage="<%$ Resources: ErrorSitenameMaxChar %>" />
                                    </ValidationRules>
                                </ektronUI:TextField>
                                <asp:Literal ID="ltrSiteNameHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                                <ektronUI:ValidationMessage ID="uxVMSitename" runat="server" DisplayMode="Error" AssociatedControlID="uxSiteName" />
                            </div>

                              <div class="sitename-error-message" id="divSiteNameErrorMessage" runat="server" visible="false">
                             <div class="container-left ektron-ui-loud"></div>
                                <div class="container-right">
                                   <ektronUI:Message ID="uxSiteNameRestriction" runat="server" DisplayMode="Error" />
                                </div>
                                  </div>
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblAWSKey" runat="server" Text="<%$ Resources: lblAWSKey %>" AssociatedControlID="uxAWSKeys"></asp:Label>:
                                <span class="ektron-ui-required">*</span>
                            </div>

                            <div class="container-right">
                                <ektronUI:TextField ID="uxAWSKeys" runat="server" ValidationGroup="KeyValidation" CssClass="form-text-field">
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources: AWSRequired %>" />
                                        <ektronUI:NotJustWhitespaceRule ErrorMessage="<% $Resources: NotJustWhitespace %>" />
                                    </ValidationRules>
                                </ektronUI:TextField>
                                <asp:Literal ID="ltrAWSKeysHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                                <ektronUI:ValidationMessage ID="uxAWSKeysMessage" runat="server" DisplayMode="Error"
                                    Text="<% $Resources: SiteNameRequired%>" AssociatedControlID="uxAWSKeys" />
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblAWSSecretKey" runat="server" Text="<%$ Resources: lblAWSSecretKey %>"
                                    AssociatedControlID="uxAWSSecretKey"></asp:Label>: <span class="ektron-ui-required">
                                        *</span>
                            </div>

                            <div class="container-right">
                                <ektronUI:TextField ID="uxAWSSecretKey" runat="server" ValidationGroup="KeyValidation" CssClass="form-text-field">
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources: AWSSecretKeyRequired %>" />
                                        <ektronUI:NotJustWhitespaceRule ErrorMessage="<%$ Resources: lblAWSSecretKey %>" />
                                    </ValidationRules>
                                </ektronUI:TextField>
                                <asp:Literal ID="ltrAWSSecretKeyHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                                <ektronUI:ValidationMessage ID="uxAWSSecretKeyMessage" runat="server" DisplayMode="Error"
                                    Text="<% $Resources: AWSSecretKeyRequired%>" AssociatedControlID="uxAWSSecretKey" />
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblHostedSvcLocation" runat="server" Text="<%$ Resources: lblHostedSvcLocation %>"
                                    AssociatedControlID="ddlHostSvc"></asp:Label>:
                            </div>

                            <div class="container-right">
                                <asp:DropDownList ID="ddlHostSvc" runat="server" CssClass="medium-width">
                                    <asp:ListItem Text="US East (N. Virginia)" Value="us-east-1" Selected="True"/>
                                    <asp:ListItem Text="US West (N. California)" Value="us-west-1" />
                                    <asp:ListItem Text="US West (Oregon)" Value="us-west-2" />
                                    <asp:ListItem Text="EU West (Ireland)" Value="eu-west-1" />
                                    <asp:ListItem Text="Asia Pacific (Singapore)" Value="ap-southeast-1" />
                                    <asp:ListItem Text="Asia Pacific (Sydney)" Value="ap-southeast-2" />
                                    <asp:ListItem Text="Asia Pacific (Tokyo)" Value="ap-northeast-1" />
                                    <asp:ListItem Text="South America (São Paulo)" Value="sa-east-1" />
                                </asp:DropDownList>
                                <asp:Literal ID="ltrHostedSvcHelpButton" runat="server" Visible="false" />
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblAutoScaling" runat="server" Text="<%$ Resources: lblAutoScaling %>"
                                    AssociatedControlID="uxAutoScaling"></asp:Label>:
                            </div>

                            <div class="container-right chkbox">
                              <asp:CheckBox ID="uxAutoScaling" runat="server" OnClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step1AutoScalingCheckedChanged(this);" />
                            </div>
                               
                            
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblWebInstance" runat="server" Text="<%$ Resources: lblWebInstances %>"
                                    AssociatedControlID="uxInstanceCount"></asp:Label>:
                            </div>

                            <div class="container-right">
                                <ektronUI:IntegerField ID="uxInstanceCount" runat="server" MinValue="1" Value="1"
                                    CssClass="smaller-width minWebInstances" ValidationGroup="KeyValidation" >
                                     <ValidationRules>
                                      <ektronUI:CustomRule ErrorMessage="<%$ Resources: uxMaxInstancesLimitError %>" ClientValidationEnabled="true" JavascriptFunctionName="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ValidateNumberOfMinInstances" />
                                     </ValidationRules>
                                </ektronUI:IntegerField>
                                <asp:Literal ID="ltrWebInstancesHelpButton" runat="server" Visible="false" />
                                 <div class="clear"></div>
                                 <ektronUI:ValidationMessage ID="uxVMMinInsCount" CssClass="vmMinIns" runat="server" AssociatedControlID="uxInstanceCount" Text="" />
                            </div>

                             <div class="divAutoScalingInstances" id="divAutoScaling" runat="server" style="display:none">
                  
                                     <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblAutoScalingMaxInstances" runat="server" Text="<%$ Resources: lblAutoScalingMaxInstances %>"
                                    AssociatedControlID="uxAutoScalingMaxInstances"></asp:Label>:
                            </div>

                            <div class="container-right">
                                <ektronUI:IntegerField ID="uxAutoScalingMaxInstances" runat="server" MinValue="1" Value="1"
                                    CssClass="smaller-width maxWebInstances" ValidationGroup="KeyValidation">
                                     <ValidationRules>
                                      <ektronUI:CustomRule ClientValidationEnabled="true" JavascriptFunctionName="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ValidateNumberOfMaxInstances" />
                                     </ValidationRules>
                                </ektronUI:IntegerField>
                            </div>
                            </div>
                             
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblWebInstanceType" runat="server" Text="<%$ Resources: lblWebInstanceType %>"
                                    AssociatedControlID="ddlWebVmSize"></asp:Label>:
                            </div>

                            <div class="container-right">
                               
                                <asp:DropDownList ID="ddlWebVmSize" runat="server" CssClass="medium-width">
                                    <asp:ListItem Enabled="true" Text="c1.medium" Value="c1.medium" Selected="True" />
                                    <asp:ListItem Enabled="true" Text="c1.xlarge" Value="c1.xlarge" />
                                    <asp:ListItem Enabled="true" Text="m1.small" Value="m1.small" />
                                    <asp:ListItem Enabled="true" Text="m1.medium" Value="m1.medium" />
                                    <asp:ListItem Enabled="true" Text="m1.large" Value="m1.large" />
                                    <asp:ListItem Enabled="true" Text="m1.xlarge" Value="m1.xlarge" />
                                    <asp:ListItem Enabled="true" Text="m2.xlarge" Value="m2.xlarge" />
                                    <asp:ListItem Enabled="true" Text="m2.2xlarge" Value="m2.2xlarge" />
                                    <asp:ListItem Enabled="true" Text="m2.4xlarge" Value="m2.4xlarge" />
                                </asp:DropDownList>
                                <span style="cursor: pointer;display:none" onclick="parent.window.open('http://msdn.microsoft.com/en-us/library/windowsazure/ee814754.aspx');">
                                    <asp:Image ID="aspWebVmSizeHelpImage" runat="server" ImageUrl="~/Workarea/images/UI/Icons/help.png"
                                        AlternateText="<%$ Resources:VmSizeHelpText %>" ToolTip="<%$ Resources:VmSizeHelpText %>" />
                                </span>
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblSupportOS" runat="server" Text="<%$ Resources: lblSupportOS %>"></asp:Label>:
                            </div>
                            <div class="container-right">
                                <asp:Literal ID="ltrWindowsOS" runat="server" Text="<% $Resources: ltrWindowsOS %>" />
                                <asp:Literal ID="ltrWindowsOSHelpButton" runat="server" Visible="false" />
                            </div>
                        </asp:Panel>
                    </div>
                    <span class="buttonRight">
                        <ektronUI:Button ID="uxStep1Next" runat="server" Text="<%$ Resources:uxSelectHostedServiceNext %>"
                            ToolTip="<%$ Resources:uxSelectHostedServiceNext %>" ValidationGroup="KeyValidation" OnClick="uxStep1Next_Click" 
                                onclientclick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step1Next(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStep1ProgressMsg, Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.phErrorMessage);" />
                        <ektronUI:Button ID="uxStep1Cancel" runat="server" Text="<%$ Resources:uxDialogCancelButtonText %>"
                            OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.closeDialog(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.amazonWizard);" />
                    </span>
                </asp:Panel>
    
<!-- Step 2: Hosted Service Storage and Security-->
    
                <asp:Panel ID="pnlStep2" CssClass="pnlStep2" runat="server" Visible="false">
                    <div class="ektron-cloud-deployment">
                        <p class="intro">
                <asp:Literal ID="ltrStep2Title" runat="server" Text="<% $Resources: ltrStep2Title %>" />
                <asp:Literal ID="ltrStep2DescHelpButton" runat="server" />
                <asp:Literal ID="ltrStep2Desc" runat="server" Text="<% $Resources: ltrStep2Desc %>" />
            </p>
                        <ektronUI:Message runat="server" ID="uxStep2ProgressMsg" DisplayMode="Working" Visible="true" CssClass="ektron-ui-hidden" Text="<%$Resources: Progress %>" />
                        <div class="serverValidationErrorMsgStep2">
                       <asp:PlaceHolder ID="phErrorMessage2" runat="server"></asp:PlaceHolder>
                       </div>
                        <div class="container-left ektron-ui-loud">
                <asp:Label ID="lblBucketName" runat="server" Text="<%$ Resources: lblBucketName %>"
                    AssociatedControlID="uxBucketName"></asp:Label>: <span class="ektron-ui-required">*</span>
            </div>
                        <div class="container-right">
                <ektronUI:TextField ID="uxBucketName" runat="server" ValidationGroup="BucketValidation" CssClass="form-text-field">
                    <ValidationRules>
                        <ektronUI:RequiredRule ErrorMessage="<% $Resources: BucketNameRequired%>" />
                        <ektronUI:RegexRule ClientValidationEnabled="true" ClientRegex="/^[a-z][a-z0-9]*$/i" ErrorMessage="<%$ Resources: ErrorBucketnameValidation %>" />
                        <ektronUI:MaxLengthRule ClientValidationEnabled="true" MaxLength="128" ErrorMessage="<%$ Resources: ErrorBucketnameMaxChar %>" />
                        <ektronUI:AllLowercaseRule ClientValidationEnabled="true" ErrorMessage="<%$ Resources: ErrorBucketnameLowerCase %>" />
                    </ValidationRules>
                </ektronUI:TextField>
                <asp:Literal ID="ltrBucketNameHelpButton" runat="server" Visible="false"/>
                                <div class="clear"></div>
                <ektronUI:ValidationMessage ID="uxVMBucketname" runat="server" DisplayMode="Error" AssociatedControlID="uxBucketName" />
                  <span class="ektron-ui-required">
                    <asp:Label CssClass="bucketname-required-lbl ektron-ui-invalid" ID="lblRequiredBucketName" runat="server" Text="<% $Resources: BucketNameRequired %>"/>
            </div>

            <div class="container-left ektron-ui-loud">
                <asp:Label ID="lblEnableCDN" runat="server" Text="<%$ Resources: lblEnableCDN %>" AssociatedControlID="uxEnableCDN"></asp:Label>:
            </div>
            <div class="container-right chkbox">
                <asp:CheckBox ID="uxEnableCDN" runat="server" />
            </div>

                        <div class="container-left ektron-ui-loud">
                <asp:Label ID="lblEktLicKey" runat="server" Text="<%$ Resources: lblEktLicKey %>"
                    AssociatedControlID="uxEktLicKey"></asp:Label>:
            </div>
                        <div class="container-right">
                <ektronUI:TextField ID="uxEktLicKey" runat="server" CssClass="form-text-field"></ektronUI:TextField>
                <asp:Literal ID="ltrEktLicKeyHelpButton" runat="server" Visible="false" />

            </div>
                        <div class="container-left ektron-ui-loud">
                <asp:Label ID="lblKeyPair" runat="server" Text="<%$ Resources: lblKeyPair %>" AssociatedControlID="ddlKeyPair"></asp:Label>:
                <span class="ektron-ui-required">*</span>
            </div>
                        <div class="container-right">
                <!-- bind the amazon keys -->
                <asp:DropDownList ID="ddlKeyPair" ValidationGroup="BucketValidation" CssClass="select-key-pair medium-width" runat="server" onchange="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.toggleKeyPairDiv(this);"/>
                <asp:Literal ID="ltrKeyPairHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>

                <span class="ektron-ui-required">
                <asp:RequiredFieldValidator ID="uxRFVDDlKeyPair" runat="server" ValidationGroup="BucketValidation" ControlToValidate="ddlKeyPair" Display="Dynamic" InitialValue="Select Key Pair" ><asp:Label  ID="ltrRequiredKeyPair" runat="server" Text="<% $Resources: ltrRequiredKeyPair %>"/></asp:RequiredFieldValidator>
                </span>
            </div>
            <!-- If Create New Key Pair selected launch the following div-->
                        <div class="new-setup-item-keypair" id="dvNewKeyPair" runat="server">
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblNewKeyPairName" runat="server" Text="<%$ Resources: lblNewKeyPair %>" AssociatedControlID="uxNewKeyPair"></asp:Label>:
                    <span class="ektron-ui-required">*</span> 
                            </div>
                <div class="container-right">
                    <ektronUI:TextField ID="uxNewKeyPair" runat="server" CssClass="form-text-field">
                        <ValidationRules>
                           <ektronUI:RequiredRule ErrorMessage="<%$ Resources: lblNewKeyPairNameRequired %>" />
                           <ektronUI:RegexRule ClientValidationEnabled="true" ClientRegex="/^[a-z][a-z0-9]*$/i" ErrorMessage="<%$ Resources: ErrorNewKeyPairValidation %>" />
                           <ektronUI:MaxLengthRule ClientValidationEnabled="true" MaxLength="128" ErrorMessage="<%$ Resources: ErrorNewKeyPairMaxChar %>" />
                        </ValidationRules>
                    </ektronUI:TextField>  
                                <div class="clear"></div>

                <ektronUI:ValidationMessage ID="uxVMNewKeyPair" runat="server" AssociatedControlID="uxNewKeyPair" Text="" />
                </div>
            </div>
              <div class="container-left ektron-ui-loud">
                <asp:Label ID="lblSecurityGroupSettings" runat="server" Text="<%$ Resources: lblSecurityGroupSettings %>"
                    AssociatedControlID="ddlSecurityGroup"></asp:Label>:
                            <span class="ektron-ui-required">*</span>
            </div>
                        <div class="container-right">
                <asp:DropDownList ID="ddlSecurityGroup" ValidationGroup="BucketValidation" runat="server" onchange="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.VerifySecurityGroupSelection(this);"></asp:DropDownList>
                <asp:Literal ID="ltrDdlNewKeyPairHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                  <span class="ektron-ui-required">
                <asp:RequiredFieldValidator ID="UxRFVDDlSecurityGroup" runat="server" ValidationGroup="BucketValidation" ControlToValidate="ddlSecurityGroup" Display="Dynamic" InitialValue="Select Security Group" ><asp:Label ID="lblRequiredSecurityGroup" runat="server" Text="<% $Resources: ltrRequiredSecurityGroup %>"/></asp:RequiredFieldValidator>
                </span>
            </div>
                    </div>

                    <ektronUI:Button runat="server" ID="uxStep2Previous" CausesValidation="false" ToolTip="<%$Resources:uxPrevious %>" Text="<%$Resources:uxPrevious %>" OnClick="uxStep2Previous_Click" CssClass="buttonLeft" DisplayMode="Anchor" OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step2Previous(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStep2ProgressMsg);" />

                    <span class="buttonRight">
                        
                        <ektronUI:Button runat="server" ID="uxStep2Next" ValidationGroup="BucketValidation" ToolTip="<%$Resources:uxSelectHostedServiceNext %>" DisplayMode="Anchor"
                            Text="<%$Resources:uxSelectHostedServiceNext %>" OnClick="uxStep2Next_Click" OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step2Next(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStep2ProgressMsg, Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.phErrorMessage2);" />

                        <ektronUI:Button runat="server" ID="uxStep2Cancel" ToolTip="<%$Resources:uxDialogCancelButtonText %>"
                            Text="<%$Resources:uxDialogCancelButtonText %>" OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.closeDialog(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.amazonWizard);" />

                    </span>

                </asp:Panel>
    
<!-- Step 3: Hosted Service Storage and Security-->
    
                <asp:Panel ID="pnlStep3" CssClass="pnlStep3" runat="server" Visible="false">
                    <div class="ektron-cloud-deployment">
                        <p class="intro">
                            <asp:Literal ID="ltrStep3Title" runat="server" Text="<% $Resources: ltrStep3Title %>" />
                            <asp:Literal ID="ltrStep3DescHelpButton" runat="server" />
                            <asp:Literal ID="ltrStep3Desc" runat="server" Text="<% $Resources: ltrStep3Desc %>" />
                        </p>
                        <ektronUI:Message runat="server" ID="uxStep3ProgressMsg" DisplayMode="Working" Visible="true" CssClass="ektron-ui-hidden" Text="<%$Resources: Progress %>" />
                         <div class="serverValidationErrorMsgStep3">
                        <asp:PlaceHolder ID="phErrorMessage3" runat="server"></asp:PlaceHolder>
                        </div>
                <!-- DB Name-->
                        <div class="container-left ektron-ui-loud">
                            <asp:Label ID="lblDBName" runat="server" Text="<%$ Resources: lblDBName %>" AssociatedControlID="uxDBName"></asp:Label>:
                            <span class="ektron-ui-required">*</span>
                        </div>
                        <div class="container-right">
                            <ektronUI:TextField ID="uxDBName" runat="server" CssClass="form-text-field" ValidationGroup="DBConnectionValidation">
                                <ValidationRules>
                                     <ektronUI:RequiredRule ErrorMessage="<%$ Resources: uxDBName %>" />
                                     <ektronUI:RegexRule ClientValidationEnabled="true" ClientRegex="/^[a-z][a-z0-9]*$/i" ErrorMessage="<%$ Resources: ErrorDBNameValidation %>" />
                                     <ektronUI:MaxLengthRule ClientValidationEnabled="true" MaxLength="128" ErrorMessage="<%$ Resources: ErrorDBNameMaxChar %>" />
                                </ValidationRules>
                            </ektronUI:TextField>
                            <asp:Literal ID="ltrUXDBNameHelpButton" runat="server" Visible="false" />
                             <div class="clear"></div>
                            <ektronUI:ValidationMessage ID="uxVMDBName" runat="server" AssociatedControlID="uxDBName" Text="" />
                        </div>
                    <!-- DB Server-->
                        <div class="container-left ektron-ui-loud">
                            <asp:Label ID="lblDBServer" runat="server" Text="<%$ Resources: lblDBServer %>" AssociatedControlID="ddlDBServerName"></asp:Label>:
                            <span class="ektron-ui-required">*</span>
                        </div>
                        <div class="container-right">
                            <asp:DropDownList ID="ddlDBServerName" ValidationGroup="DBConnectionValidation" CausesValidation="true" CssClass="select-key-pair medium-width" runat="server" onchange="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.Step3DdlDbServernameChanged(this);" AutoPostBack="false">
                            </asp:DropDownList>
                            <asp:Literal ID="ltrUXDBServerHelpButton" runat="server" Visible="false" />
                            <%--<div class="ektronTopSpace">
                                <ektronUI:Message ID="uxDDServerRequired" runat="server" visible="true" cssclass="ektron-ui-hidden" DisplayMode="Error" Text="<% $Resources: lblDDServerRequired %>" />
                            </div>--%>
                                <div class="clear"></div>
                            <span class="ektron-ui-required">
                <asp:RequiredFieldValidator ID="uxRFVDDlDBServername" runat="server" ControlToValidate="ddlDBServerName" Display="Dynamic" InitialValue="Select a Database Server" ><asp:Label  ID="lblRequiredDbServername" runat="server" Text="<% $Resources: lblDDServerRequired %>"/></asp:RequiredFieldValidator>
                </span>
                        </div>
                        <asp:panel class="new-setup-item-DB" runat="server" id="pnlDBInstanceType" style="display:none;">
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblDBInstanceType" runat="server" Text="<%$ Resources: lblDBInstanceType %>"
                                    AssociatedControlID="ddlDBInstanceType"></asp:Label>: <span class="ektron-ui-required">
                                        *</span>
                            </div>
                            <div class="container-right">
                                <asp:DropDownList ID="ddlDBInstanceType" CssClass="select-key-pair medium-width" runat="server" CausesValidation="true" onchange="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.Step3InstanceTypeChanged(this);" AutoPostBack="false">
                                    <asp:ListItem Text="<% $Resources: TypeOfServerInstance %>" Value="<% $Resources: TypeOfServerInstance %>"
                                        Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Express Edition" Value="sqlserver-ex"></asp:ListItem>
                                    <asp:ListItem Text="Web Edition" Value="sqlserver-web"></asp:ListItem>
                                    <asp:ListItem Text="Standard Edition" Value="sqlserver-se"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Literal ID="ltrDdlDBInstanceTypeHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                                 
                           <span class="ektron-ui-required">
                <asp:RequiredFieldValidator ID="uxRFVDdlDBInstanceType" ValidationGroup="DBConnectionValidation" runat="server" ControlToValidate="ddlDBInstanceType" Display="Dynamic" InitialValue="Select Type of Server Instance"><asp:Label  ID="lblRequiredDbInstanceType" runat="server" Text="<% $Resources: lblDDInstanceTypeRequired %>"/></asp:RequiredFieldValidator>
                </span>
                        
                            </div>
                        </asp:panel>
                    <!-- SQL Server Username-->
                        <div class="credential-division">
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblSQLServerUsername" runat="server" Text="<%$ Resources: lblSQLServerUsername %>"
                                    AssociatedControlID="uxSQLServerUsername"></asp:Label>: <span class="ektron-ui-required">
                                        *</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxSQLServerUsername" runat="server" ValidationGroup="DBConnectionValidation" CssClass="form-text-field">
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources: uxSQLServerUsername %>" />
                                        <ektronUI:RegexRule ClientValidationEnabled="true" ClientRegex="/^[a-z][a-z0-9]*$/i" ErrorMessage="<%$ Resources: ErrorUsernameValidation %>" />
                                        <ektronUI:MaxLengthRule ClientValidationEnabled="true" MaxLength="128" ErrorMessage="<%$ Resources: ErrorUsernameMaxChar %>" />
                                    </ValidationRules>
                                </ektronUI:TextField>
                                <asp:Literal ID="ltrUXSQLServerUsernameHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                                 <ektronUI:ValidationMessage ID="uxVMSQLUsername" runat="server" AssociatedControlID="uxSQLServerUsername" Text="" />
                            </div>
                        <!-- SQL Server Password-->
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblSQLServerPassword" runat="server" Text="<%$ Resources: lblSQLServerPassword %>"
                                    AssociatedControlID="uxSQLServerPassword"></asp:Label>: <span class="ektron-ui-required">
                                        *</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:PasswordField ID="uxSQLServerPassword" CssClass="form-text-field uxSQLServerPassword" runat="server" ValidationGroup="DBConnectionValidation">
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources: uxSQLServerPassword %>" />
                                       <%-- <ektronUI:CustomRule ClientValidationEnabled="true" JavascriptFunctionName="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.checkForillegalChar" />--%>
                                        <ektronUI:RegexRule ClientValidationEnabled="true" ClientRegex="/^[a-z][a-z0-9]*$/i" ErrorMessage="<%$ Resources: ErrorPasswordValidation %>" />
                                        <ektronUI:MaxLengthRule ClientValidationEnabled="true" MaxLength="128" ErrorMessage="<%$ Resources: ErrorPasswordMaxChar %>" />
                                        <ektronUI:MinLengthRule ClientValidationEnabled="true" MinLength="8" ErrorMessage="<%$ Resources: ErrorPasswordMinChar %>" />
                                    </ValidationRules>
                                </ektronUI:PasswordField>
                                <asp:Literal ID="ltrUXSQLServerPasswordHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                                 <ektronUI:ValidationMessage ID="uxVMSQLServerPassword" runat="server" AssociatedControlID="uxSQLServerPassword" Text="" />
                            </div> 
                        </div>
                        
                     <!-- Create a New Database-->
                        <asp:panel class="new-setup-item-DB" runat="server" id="newSetupItemDB" style="display:none;">
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblReEnterSQLPassword" runat="server" Text="<%$ Resources: lblReEnterSQLPassword %>"
                                    AssociatedControlID="uxReenterSQLPassword"></asp:Label>: <span class="ektron-ui-required">
                                        *</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:PasswordField ID="uxReenterSQLPassword" runat="server" CssClass="form-text-field uxReEnterSQLServerPassword" >
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources: uxReenterSQLPassword %>" />
                                        <ektronUI:CustomRule ClientValidationEnabled="true" JavascriptFunctionName="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.VerifyPasswordMatch" ErrorMessage="<% $Resources: uxPWDMatch %>" />
                                    </ValidationRules>
                                </ektronUI:PasswordField>
                                <asp:Literal ID="ltrUXReenterSQLPasswordHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                                 <ektronUI:ValidationMessage ID="uxVMReenterSQLPassword" runat="server" AssociatedControlID="uxReenterSQLPassword" Text="" />
                            </div>
                        <!-- DataBase Instance Class -->
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblDBInstanceClass" runat="server" Text="<%$ Resources: lblDBInstanceClass %>"
                                    AssociatedControlID="ddlDBInstanceClass"></asp:Label>: <span class="ektron-ui-required">
                                        *</span>
                            </div>
                            <div class="container-right">
                                <asp:DropDownList ID="ddlDBInstanceClass" CssClass="select-key-pair medium-width" runat="server"
                                    onchange="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.toggleSetUpDivs(this);">
                                    <asp:ListItem Text="Small" Value="db.m1.small"></asp:ListItem>
                                    <asp:ListItem Text="Large" Value="db.m1.large"></asp:ListItem>
                                    <asp:ListItem Text="Extra Large" Value="db.m2.xlarge"></asp:ListItem>
                                    <asp:ListItem Text="2Xtra Large" Value="db.m2.2xlarge"></asp:ListItem>
                                    <asp:ListItem Text="4Xtra Large" Value="db.m2.4xlarge"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Literal ID="ltrDddlDBInstanceClassHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                            </div>
                        <!-- Server Name Prefix -->
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblServerNamePrefix" runat="server" Text="<%$ Resources: lblServerNamePrefix %>"
                                    AssociatedControlID="uxServerNamePrefix"></asp:Label>: <span class="ektron-ui-required">
                                        *</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxServerNamePrefix" runat="server" CssClass="form-text-field">
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources: uxServerNamePrefix %>" />
                                        <ektronUI:RegexRule ClientValidationEnabled="true" ClientRegex="/^[a-z][a-z0-9]*$/i" ErrorMessage="<%$ Resources: ErrorSQLPrefixValidation %>" />
                                        <ektronUI:MaxLengthRule ClientValidationEnabled="true" MaxLength="128" ErrorMessage="<%$ Resources: ErrorSQLPrefixMaxChar %>" />
                                    </ValidationRules>
                                </ektronUI:TextField>
                                <asp:Literal ID="ltrLblServerNamePrefixHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                                 <ektronUI:ValidationMessage ID="uxVMServerNamePrefix" runat="server" AssociatedControlID="uxServerNamePrefix" Text="" />
                            </div>
                        <!-- Storage Size -->
                            <div class="container-left ektron-ui-loud">
                                <asp:Label ID="lblStorageSize" runat="server" Text="<%$ Resources: lblStorageSize %>"/>: <span class="ektron-ui-required">*</span>
                            </div>
                            <div class="container-right">
                                <asp:Label ID="aspStorageSizeLabelExpWeb" runat="server" AssociatedControlID="uxStorageSize" Text="<%$ Resources: ExpWeb %>" CssClass="lblStorageSize" />
                                <asp:Label ID="aspStorageSizeLabelStd" runat="server" AssociatedControlID="uxStorageSize" Text="<%$ Resources: StandardEnterprise %>" CssClass="lblStorageSize" />
                                <%--<ektronUI:IntegerField id="uxStorageSize" runat="server" maxvalue="1024" />--%>
                                 <ektronUI:TextField ID="uxStorageSize" runat="server" CssClass="form-text-field">
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources: uxStorageSize %>" />
                                        <ektronUI:IntegerRule ErrorMessage="<%$ Resources: uxStorageSizeIntegerOnly %>" ClientValidationEnabled="true" />
                                        <ektronUI:CustomRule ErrorMessage="<%$ Resources: uxStorageSizeLimit %>" ClientValidationEnabled="true" JavascriptFunctionName="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.VerifyStorageSize" />
                                    </ValidationRules>
                                </ektronUI:TextField>
                                <asp:Literal ID="ltrUXStorageSizeHelpButton" runat="server" Visible="false" />
                                <div class="clear"></div>
                                <ektronUI:ValidationMessage ID="uxVMStorageSize" runat="server" AssociatedControlID="uxStorageSize" Text="" />
                            </div>

                            <div class="container-left ektron-ui-loud">
                            </div>
                            <div class="container-right">
                                <ektronUI:Message runat="server" ID="uxStorageSizeWarning" DisplayMode="Warning" Text="<%$Resources: uxStorageSizeWarning %>" />
                            </div>
                        </asp:panel> 
                    </div>
                    <ektronUI:Button ID="uxStep3Previous" CausesValidation="false" runat="server" Text="<%$ Resources:uxPrevious %>" DisplayMode="Anchor" OnClick="uxStep3Previous_Click"
                        OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step3Previous(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStep3ProgressMsg);" />
                    <span class="buttonRight">
                         <ektronUI:Button ID="uxStep3Next" runat="server" Text="<%$ Resources:uxSelectHostedServiceNext %>" ValidationGroup="DBConnectionValidation"
                            CssClass="primary-amazon-dialog-action" OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step3Next(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStep3ProgressMsg, Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.phErrorMessage3);"  OnClick="uxStep3Next_Click" />
                        <ektronUI:Button ID="uxStep3Cancel" runat="server" Text="<%$ Resources:uxDialogCancelButtonText %>"
                            OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.closeDialog(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.amazonWizard);" />
                    </span>
                </asp:Panel>

<!-- Step 4: Hosted Service Storage and Security-->

                <asp:Panel ID="pnlStep4" CssClass="pnlStep4" runat="server" Visible="false">
                    <div class="ektron-cloud-deployment">
                        <p class="intro">
                            <asp:Literal ID="ltrStep4Title" runat="server" Text="<% $Resources: ltrStep4Title %>" />
                            <asp:Literal ID="ltrStep4DescHelpButton" runat="server" />
                            <asp:PlaceHolder ID="phStep4ErrorMessage" runat="server"></asp:PlaceHolder>
                            <ektronUI:Message runat="server" ID="uxStep4ProgressMsg" DisplayMode="Working" Visible="true" CssClass="ektron-ui-hidden" Text="<%$Resources: Progress %>" />
                            <asp:Literal ID="ltrStep4Desc" runat="server" Text="<% $Resources: ltrStep4Desc %>" />
                        </p>
                    </div>
                    <span class="buttonRight">
                        <ektronUI:Button ID="uxAmazonSave" runat="server" Text="<%$ Resources:uxDialogButtonSave %>" 
                            CssClass="primary-amazon-dialog-action" OnClick="uxAmazonSave_Click" ValidationGroup="KeyValidation" onclientclick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step4Save(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStep4ProgressMsg);"  />
                        <ektronUI:Button ID="uxStep4Cancel" runat="server" Text="<%$ Resources:uxDialogCancelButtonText %>" 
                            OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.closeDialog(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.amazonWizard);" />
                    </span>
                </asp:Panel>
<!-- Step 5: Hosted Service Storage and Security-->
                <asp:Panel ID="pnlStep5" CssClass="pnlStep5" runat="server" Visible="false">
                    <div class="ektron-cloud-deployment">
                        <p class="intro">
                            <asp:Literal ID="ltrStep5Title" runat="server" Text="<% $Resources: ltrStep5Title %>" />
                            <asp:Literal ID="ltrStep5DescHelpButton" runat="server" />
                            <asp:Literal ID="ltrStep5Desc" runat="server" Text="<% $Resources: ltrStep5Desc %>" />
                            <input type="hidden" runat="server" id="profileId" name="profileId" />
                        </p>
                    </div>
                    <span class="buttonRight">
                        <ektronUI:Button ID="uxDeploySite" runat="server" Text="<%$ Resources:uxDeploySite %>" OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.deploySite(this);" DisplayMode="Anchor" /> 
                        <ektronUI:Button ID="uxStep5Cancel" runat="server" Text="<%$ Resources:uxDialogCloseButtonText %>" OnClientClick=" Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.closeDialog(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.amazonWizard);" />
                    </span>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </contenttemplate>
</ektronUI:Dialog>
