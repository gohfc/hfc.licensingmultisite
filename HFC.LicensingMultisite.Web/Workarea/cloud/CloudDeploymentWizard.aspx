<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CloudDeploymentWizard.aspx.cs" Inherits="Ektron.Workarea.Cloud.Dialogs.DeploymentWizard" %>

<!DOCTYPE>
<html>
<head id="Head1" runat="server">
    <title>Create Cloud Hosted Service</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="ektron-cloud-deployment">
            <ektronUI:Css ID="uxCloudDialogStyles" runat="server" Path="{WorkareaPath}/cloud/css/ektron.workarea.cloud.css" />
            <ektronUI:JavaScriptBlock ID="uxSetheightJs" runat="server">
                <ScriptTemplate>
                    parent.Ektron.Workarea.Cloud.Dialogs.Deployment.setHeight($ektron("body").outerHeight() + 20);
                        $ektron(".ektron-cloud-deployment table:not(.deployment-package) tr:odd").addClass("ektron-ui-odd");
                        $ektron(".ektron-cloud-deployment table:not(.deployment-package) tr:even").addClass("ektron-ui-even");
                </ScriptTemplate>
            </ektronUI:JavaScriptBlock>
            <p class="header">
                <asp:PlaceHolder ID="aspHeaderText" runat="server">
                    <span class="ektron-ui-loud">
                        <asp:Literal ID="aspWizardHeaderText" runat="server" Text="<%$ Resources:StepLabel %>" />&#160;<%# (aspWizardSteps.ActiveViewIndex + 1) %> of 4: 
                    </span>
                </asp:PlaceHolder>
                <asp:MultiView ID="aspWizardStepTitle" runat="server" ActiveViewIndex="0">
                    <asp:View ID="aspStep1TitleView" runat="server">
                        <span class="ektron-ui-loud">
                            <asp:Literal ID="aspStep1TitleViewHeaderText" runat="server" Text="<%$ Resources:ServicesLegend %>" />
                        </span>
                        <span class="ektron-ui-intro clearfix">
                            <asp:Literal ID="aspStep1IntroText" runat="server" Text="<%$ Resources:Step1IntroText %>"></asp:Literal>
                        </span>
                        <span class="ektron-ui-quiet required-text">
                            <b>
                                <asp:Literal runat="server" Text="<%$ Resources:RequiredText%>" /></b><span class="ektron-ui-required"> *</span>
                        </span>
                    </asp:View>
                    <asp:View ID="aspStep2TitleView" runat="server">
                        <span class="ektron-ui-loud">
                            <asp:Literal ID="aspStep2TitleViewHeaderText" runat="server" Text="<%$ Resources:HostedServiceLegend %>" />
                        </span>
                        <span class="ektron-ui-intro clearfix">
                            <asp:Literal ID="aspStep2IntroText" runat="server" Text="<%$ Resources:Step2IntroText %>"></asp:Literal>
                        </span>
                        <span class="ektron-ui-quiet required-text">
                            <b>
                                <asp:Literal runat="server" Text="<%$ Resources:RequiredText%>" /></b><span class="ektron-ui-required"> *</span>
                        </span>
                    </asp:View>
                    <asp:View ID="aspStep3TitleView" runat="server">
                        <span class="ektron-ui-loud">
                            <asp:Literal ID="aspStep3TitleViewHeaderText" runat="server" Text="<%$ Resources:AzureConnectionsLegend %>" />
                        </span>
                        <span class="ektron-ui-intro clearfix">
                            <asp:Literal ID="aspStep3IntroText" runat="server" Text="<%$ Resources:Step3IntroText %>"></asp:Literal>
                        </span>
                        <span class="ektron-ui-quiet required-text">
                            <b>
                                <asp:Literal runat="server" Text="<%$ Resources:RequiredText%>" /></b><span class="ektron-ui-required"> *</span>
                        </span>
                    </asp:View>
                    <asp:View ID="aspStep4TitleView" runat="server">
                        <span class="ektron-ui-loud">
                            <asp:Literal ID="aspStep4TitleViewHeaderText" runat="server" Text="<%$ Resources:DeploymentPackage %>" />
                        </span>
                        <span class="ektron-ui-intro clearfix">
                            <asp:Literal ID="aspStep4IntroText" runat="server" Text="<%$ Resources:Step4IntroText %>"></asp:Literal>
                        </span>
                    </asp:View>
                    <asp:View ID="aspSyncingTitleView" runat="server">
                        <asp:Literal ID="aspSyncingTitleViewHeaderText" runat="server" Text="<%$ Resources:CreatingDeploymentMessage %>" />
                    </asp:View>
                </asp:MultiView>
            </p>
            <ektronUI:Message ID="uxFormValidationText" runat="server" DisplayMode="Error" Visible="false" />
            <asp:MultiView ID="aspWizard" runat="server" ActiveViewIndex="0">
                <asp:View ID="aspWizardView" runat="server">
                    <ektronUI:JavaScriptBlock ID="uxCancelJs" runat="server" Visible="false">
                        <ScriptTemplate>
                            parent.Ektron.Workarea.Cloud.Dialogs.Deployment.cancel(false);
                        </ScriptTemplate>
                    </ektronUI:JavaScriptBlock>
                    <asp:MultiView ID="aspWizardSteps" runat="server" ActiveViewIndex="0">
                        <asp:View ID="aspStep1" runat="server">
                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspSubscriptionIdLabelB" runat="server" Text="<%$ Resources:SubscriptionId %>" />: <span class="ektron-ui-required">*</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxSubscriptionId" runat="server" CssClass="wide">
                                        <ValidationRules>
                                            <ektronUI:RequiredRule ErrorMessage="<%$ Resources:RequiredMessage %>" />
                                        </ValidationRules>
                                </ektronUI:TextField>
                            </div>

                            <div class="container-left ektron-ui-loud ektron-ui-alignTop">
                                <asp:Literal ID="aspServiceName" runat="server" Text="<%$ Resources:ServiceName %>" />: <span class="ektron-ui-required">*</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxServiceName" runat="server" CssClass="medium-width">
                                        <ValidationRules>
                                            <ektronUI:RequiredRule ErrorMessage="<%$ Resources:RequiredMessage %>"/>
                                            <ektronUI:AllLowercaseRule ErrorMessage="<%$ Resources:AllLowerCaseMessage %>" />
                                        </ValidationRules>                                                    
                                </ektronUI:TextField>
                                <span>.cloudapp.net</span>
                                <ektronUI:ValidationMessage ID="uxServiceNameErrorMessage" runat="server" AssociatedControlID="uxServiceName" />
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Label AssociatedControlID="aspLocationDropDown" ID="aspLocationDropDownLabel" runat="server" Text="<%$ Resources:LocationDropDownLabel %>" />:
                            </div>
                            <div class="container-right">
                                <asp:DropDownList ID="aspLocationDropDown" runat="server">
                                    <asp:ListItem Text="<%$ Resources:HostedService_NorthCentralUS %>" Value="North Central US" Selected="True" />
                                    <asp:ListItem Text="<%$ Resources:HostedService_SouthCentralUS %>" Value="South Central US" />
                                    <asp:ListItem Text="<%$ Resources:HostedService_EastUS %>" Value="East US" />
                                    <asp:ListItem Text="<%$ Resources:HostedService_WestUS %>" Value="West US" />
                                    <asp:ListItem Text="<%$ Resources:HostedService_WestEurope %>" Value="West Europe" />
                                    <asp:ListItem Text="<%$ Resources:HostedService_SoutheastAsia %>" Value="Southeast Asia" />
                                    <asp:ListItem Text="<%$ Resources:HostedService_EastAsia %>" Value="East Asia" />
                                </asp:DropDownList>
                                <asp:Literal ID="aspLocationHelpButton" runat="server" />
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspSlot" runat="server" Text="<%$ Resources:Slot %>" />:
                            </div>
                            <div class="container-right">
                                <asp:DropDownList ID="aspSlots" runat="server">
                                    <asp:ListItem Enabled="true" Text="<%$ Resources:Production %>" Value="Production" Selected="True" />
                                    <asp:ListItem Enabled="true" Text="<%$ Resources:Staging %>" Value="Staging" Selected="False" />
                                </asp:DropDownList>
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspWebVmSizeLabel" runat="server" Text="<%$ Resources:WebRoleSize %>" />:
                            </div>
                            <div class="container-right">
                                <asp:DropDownList ID="aspWebVmSize" runat="server" CssClass="medium-width">
                                    <asp:ListItem Enabled="true" Text="<%$ Resources:VmSmall %>" Value="Small" Selected="False" />
                                    <asp:ListItem Enabled="true" Text="<%$ Resources:VmMedium %>" Value="Medium" Selected="True" />
                                    <asp:ListItem Enabled="true" Text="<%$ Resources:VmLarge %>" Value="Large" Selected="False" />
                                    <asp:ListItem Enabled="true" Text="<%$ Resources:VmExtraLarge %>" Value="ExtraLarge" Selected="False" />
                                </asp:DropDownList>
                                <span style="cursor: pointer; cursor: hand;" onclick="parent.window.open('http://msdn.microsoft.com/en-us/library/windowsazure/ee814754.aspx');">
                                    <asp:Image ID="aspWebVmSizeHelpImage" runat="server" ImageUrl="~/Workarea/images/UI/Icons/help.png" AlternateText="<%$ Resources:VmSizeHelpText %>" ToolTip="<%$ Resources:VmSizeHelpText %>" />
                                </span>
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspInstanceCount" runat="server" Text="<%$ Resources:InstanceCount %>" />: <span class="ektron-ui-required">*</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:IntegerField ID="uxInstanceCount" runat="server" MinValue="1" Value="1" CssClass="smaller-width">
                                        <ValidationRules>
                                            <ektronUI:RequiredRule ErrorMessage="<%$ Resources:RequiredMessage %>"/>
                                        </ValidationRules>
                                </ektronUI:IntegerField>
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspWrVmSizeLabel" runat="server" Text="<%$ Resources:WorkerRoleSize %>" />:
                            </div>
                            <div class="container-right">
                                <asp:DropDownList ID="aspWorkerVmSize" runat="server" CssClass="medium-width">
                                    <asp:ListItem Enabled="true" Text="<%$ Resources:VmSmall %>" Value="Small" Selected="True" />
                                    <asp:ListItem Enabled="true" Text="<%$ Resources:VmMedium %>" Value="Medium" Selected="False" />
                                    <asp:ListItem Enabled="true" Text="<%$ Resources:VmLarge %>" Value="Large" Selected="False" />
                                    <asp:ListItem Enabled="true" Text="<%$ Resources:VmExtraLarge %>" Value="ExtraLarge" Selected="False" />
                                </asp:DropDownList>
                                <span style="cursor: pointer; cursor: hand;" onclick="parent.window.open('http://msdn.microsoft.com/en-us/library/windowsazure/ee814754.aspx');">
                                    <asp:Image ID="aspWorkerVmSizeHelpImage" runat="server" ImageUrl="~/Workarea/images/UI/Icons/help.png" AlternateText="<%$ Resources:VmSizeHelpText %>" ToolTip="<%$ Resources:VmSizeHelpText %>" />
                                </span>
                            </div>
                        </asp:View>
                        <asp:View ID="aspStep2" runat="server">
                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspStorageAccount" runat="server" Text="<%$ Resources:StorageAccount %>" />: <span class="ektron-ui-required">*</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxStorageAccount" runat="server" CssClass="wide">
                                        <ValidationRules>
                                            <ektronUI:RequiredRule ErrorMessage="<%$ Resources:RequiredMessage %>"/>
                                            <ektronUI:AllLowercaseRule ErrorMessage="<%$ Resources:AllLowerCaseMessage %>" />
                                        </ValidationRules>
                                </ektronUI:TextField>
                            </div>
                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspLabel" runat="server" Text="<%$ Resources:CDN %>" />:
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxCDN" runat="server" CssClass="wide" />
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspLicenseKey" runat="server" Text="<%$ Resources:LicenseKey %>" />:
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxLicenseKey" runat="server" CssClass="medium2-width"></ektronUI:TextField>
                            </div>
                            <div class="security-setting-protocol">
                                <ektronUI:CssBlock ID="aspSecurityProtocolSettingStyle" runat="server">
                                    <CssTemplate>
                                        .ektron-cloud-deployment .security-setting-protocol input {width:auto!important;}
                                            .ektron-cloud-deployment .security-setting-protocol table.radio-buttons {width:auto!important;}
                                            .ektron-cloud-deployment .security-setting-protocol {margin-bottom:1.5em;}
                                            .ektron-cloud-deployment .security-setting-protocol .container-left,
                                            .ektron-cloud-deployment .security-setting-protocol .container-right {margin-bottom:0;}
                                            .ektron-cloud-deployment .https-configuration-disabled {opacity:.5;}
                                    </CssTemplate>
                                </ektronUI:CssBlock>
                                <div class="container-left ektron-ui-loud">
                                    <asp:Label ID="aspSecurityProtocolSettingLabel" runat="server" AssociatedControlID="aspSecurityProtocolSetting" Text="<%$ Resources:SecurityProtocolSettingLabel %>" />: <span class="ektron-ui-required">*</span>
                                </div>
                                <div class="container-right">
                                    <asp:RadioButtonList ID="aspSecurityProtocolSetting" runat="server" RepeatDirection="Horizontal" CssClass="radio-buttons" RepeatLayout="Flow">
                                        <asp:ListItem Text="HTTP" Value="HTTP" Selected="True" />
                                        <asp:ListItem Text="HTTPS" Value="HTTPS" />
                                    </asp:RadioButtonList>
                                </div>
                            </div>

                            <div id="uxHttpsConfiguration" runat="server" class="https-configuration">
                                <ektronUI:JavaScriptBlock ID="uxHttpsConfigurationJS" runat="server">
                                    <ScriptTemplate>
                                        Ektron.Namespace.Register("Ektron.Workarea.Cloud.Dialogs.Deployment");
                                            Ektron.Workarea.Cloud.Dialogs.Deployment.HttpConfiguration = {
                                                bindEvents: function(){
                                                    var self = this;
                                                     $ektron("#<%# aspSecurityProtocolSetting.ClientID %> :radio").change(function(){
                                                        self.selection = $ektron(this);
                                                        self.enabled = self.selection.val() === "HTTPS" ? true : false;
                                                        self.toggle();
                                                    });
                                                },
                                                init: function(){
                                                    this.row = $ektron("#<%# uxHttpsConfiguration.ClientID%>");
                                                    this.selection = $ektron("#<%# aspSecurityProtocolSetting.ClientID %> :checked");
                                                    this.enabled = this.selection.val() === "HTTPS" ? true : false;
                                                    this.field = $ektron("#<%# uxSecurityCertificateThumbprint.ClientID %> input");

                                                    this.bindEvents();
                                                    this.toggle();
                                                },
                                                isValid: function(){
                                                    if(this.enabled){
                                                        if (this.field.val() === ""){
                                                            //it's enabled and has no contents - it's invalid
                                                            return false;
                                                        } else {
                                                            //it's enabled and has contents - it's valid
                                                            return true;
                                                        }
                                                    } else {
                                                        //it's valid if it's disabled
                                                        return true;
                                                    }
                                                },
                                                toggle: function(){
                                                    if (this.enabled){
                                                        this.field.prop("disabled", false);
                                                        this.row.removeClass("https-configuration-disabled");
                                                    } else {
                                                        this.field.val("");
                                                        this.field.focus().blur();
                                                        this.field.prop("disabled", true);
                                                        this.row.addClass("https-configuration-disabled");
                                                    }
                                                }
                                            }
                                            Ektron.Workarea.Cloud.Dialogs.Deployment.HttpConfiguration.init();

                                    </ScriptTemplate>
                                </ektronUI:JavaScriptBlock>
                                <div class="container-left ektron-ui-loud ektron-ui-alignTop">
                                    <asp:Label ID="aspSecurityCertificateThumbprintLabel" runat="server" AssociatedControlID="uxSecurityCertificateThumbprint" Text="<%$ Resources:SecurityCertificateThumbprintLabel %>" />: <span class="ektron-ui-required">* </span>
                                </div>
                                <div class="container-right">
                                    <ektronUI:TextField ID="uxSecurityCertificateThumbprint" runat="server" CssClass="wide">
                                            <ValidationRules>
                                                <ektronUI:CustomRule JavascriptFunctionName="Ektron.Workarea.Cloud.Dialogs.Deployment.HttpConfiguration.isValid" />
                                            </ValidationRules>
                                    </ektronUI:TextField>
                                    <a href="http://msdn.microsoft.com/en-us/library/windowsazure/ff795779.aspx" onclick="window.open(this.href);return false;">
                                        <asp:Image ID="uxSecurityCertificateThumbprintHelpText" runat="server" ImageUrl="~/Workarea/images/UI/Icons/help.png" AlternateText="<%$ Resources:SecurityCertificateThumbprintHelpText %>" ToolTip="<%$ Resources:SecurityCertificateThumbprintHelpText %>" />
                                    </a>
                                    <br />
                                    <span class="ektron-ui-text-small ektron-ui-required">Required only if "Security Protocol Setting" is set to "HTTPS"</span>
                                </div>
                            </div>
                            <div class="container-left ektron-ui-loud ektron-ui-alignTop">
                                <asp:Label ID="aspCachingXmlLabel" runat="server" AssociatedControlID="aspCachingXml" Text="<%$ Resources:CachingXmlLabel %>" />: 
                            </div>
                            <div class="container-right caching-xml">
                                <ektronUI:TextField ID="aspCachingXml" TextMode="MultiLine" runat="server" CssClass="wide" />
                                <asp:Literal ID="aspCachingHelpButton" runat="server" />
                            </div>
                        </asp:View>
                        <asp:View ID="aspStep3" runat="server">
                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="ltrDbOption" runat="server" Text="<%$ Resources:DatabaseOption %>" />:
                            </div>
                            <div class="container-right">
                                <asp:DropDownList ID="ddlDbOption" runat="server" CssClass="wide">
                                    <asp:ListItem Enabled="true" Text="<% $Resources:CreateDatabase %>" Value="Yes"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="<% $Resources:DonotCreateDatabase %>" Value="No"></asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <%--<div class="container-left ektron-ui-loud">
                                    <asp:Literal ID="ltrEdition" runat="server" Text="<%$ Resources:DatabaseEdition %>" />:
                                </div>
                                <div class="container-right">
                                    <asp:RadioButtonList ID="rdlDbEdition" runat="server">
                                        <asp:ListItem Enabled="true" Text="<% $Resources:WebEdition %>" Value="Web" Selected="True"></asp:ListItem>
                                        <asp:ListItem Enabled="true" Text="<% $Resources:BusinessEdition %>" Value="Business" Selected="False"></asp:ListItem>
                                    </asp:RadioButtonList>    
                                </div>--%>

                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="ltrDbSize" runat="server" Text="<%$ Resources:DatabaseSize %>" />:
                            </div>
                            <div class="container-right">
                                <asp:DropDownList ID="ddlDbSize" runat="server" CssClass="small-width">
                                    <asp:ListItem Enabled="true" Text="1" Value="1"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="5" Value="5"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="30" Value="30"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="40" Value="40"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="50" Value="50"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="100" Value="100"></asp:ListItem>
                                    <asp:ListItem Enabled="true" Text="150" Value="150"></asp:ListItem>
                                </asp:DropDownList>&#160;GB
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspServer" runat="server" Text="<%$ Resources:Server %>" />: <span class="ektron-ui-required">*</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxServer" runat="server" CssClass="medium2-width">
                                        <ValidationRules>
                                            <ektronUI:RequiredRule ErrorMessage="<%$ Resources:RequiredMessage %>" />
                                        </ValidationRules>
                                </ektronUI:TextField>
                                <span>.database.windows.net</span>
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspDatabase" runat="server" Text="<%$ Resources:Database %>" />: <span class="ektron-ui-required">*</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxDatabase" runat="server" CssClass="medium2-width">
                                        <ValidationRules>
                                            <ektronUI:RequiredRule ErrorMessage="<%$ Resources:RequiredMessage %>" />
                                        </ValidationRules>
                                </ektronUI:TextField>
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspUsername" runat="server" Text="<%$ Resources:Username %>" />: <span class="ektron-ui-required">*</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:TextField ID="uxUsername" runat="server" CssClass="medium2-width">
                                        <ValidationRules>
                                            <ektronUI:RequiredRule ErrorMessage="<%$ Resources:RequiredMessage %>" />
                                        </ValidationRules>
                                </ektronUI:TextField>
                            </div>

                            <div class="container-left ektron-ui-loud">
                                <asp:Literal ID="aspPassword" runat="server" Text="<%$ Resources:Password %>" />: <span class="ektron-ui-required">*</span>
                            </div>
                            <div class="container-right">
                                <ektronUI:PasswordField ID="uxPassword" runat="server" CssClass="medium2-width">
                                        <ValidationRules>
                                            <ektronUI:RequiredRule ErrorMessage="<%$ Resources:RequiredMessage %>" />
                                        </ValidationRules>
                                </ektronUI:PasswordField>
                            </div>
                        </asp:View>
                        <asp:View ID="aspStep4" runat="server">
                            <asp:RadioButtonList ID="aspDeploymentPackageUploadOptions" runat="server" CssClass="deployment-package" AutoPostBack="true" OnSelectedIndexChanged="DeploymentPackage_Changed">
                                <asp:ListItem Enabled="true" Text="<%$ Resources:CreateDeploymentPackage %>" Value="New" Selected="true" />
                                <asp:ListItem Enabled="true" Text="<%$ Resources:UploadExistingDeploymentPackage %>" Value="Existing" Selected="False" />
                            </asp:RadioButtonList>
                            <div id="uxUploadExistingPackage" runat="server" visible="false">
                                <div class="ektron-ui-quiet required-text ui-helper-clearfix">
                                    <b>
                                        <asp:Literal ID="uxLiteralRequiredText" runat="server" Text="<%$ Resources:RequiredText%>" /></b><span class="ektron-ui-required"> *</span>
                                </div>
                                        <ektronUI:Message runat="server" ID="uxMsgSilverlight" DisplayMode="Information" Visible="true" Text="<%$Resources: SilverlightRequired %>" />

                                <div class="uploadDeploymentPackage ui-helper-clearfix">
                                    <div class="uploadPackageFile">
                                        <asp:Literal ID="aspPackageFileLabel" runat="server" Text="<%$ Resources:PackageFile %>" />: <span class="ektron-ui-required">*</span>
                                        <ektronUI:FileUpload ID="uxPackageFile" runat="server" MaxUploads="1" ErrorMessage="<%$ Resources:RequiredUploadMessage %>" IsRequired="true" FileTypeFilter="Ektron Cloud Package|*.cspkg;" Path="<%# this.UploadPath %>" FinishedMessage="<%$ Resources: UploadFinished %>" UploadedFileProcessorType="Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets.RenameFileUploadedProcessor, Ektron.Cms.Framework.UI.Controls.EktronUI" TurnOffFileExistenceValidation="true" />
                                    </div>
                                    
                                    
                                    <div class="uploadConfigurationFile">
                                        <asp:Literal ID="aspConfigurationFileLabel" runat="server" Text="<%$ Resources:ConfigurationFile %>" />: <span class="ektron-ui-required">*</span>
                                        <ektronUI:FileUpload ID="uxConfigurationFile" MaxUploads="1" runat="server" ErrorMessage="<%$ Resources:RequiredUploadMessage %>" IsRequired="true" FileTypeFilter="Ektron Cloud Configuration|*.cscfg" Path="<%# this.UploadPath %>" FinishedMessage="<%$ Resources: UploadFinished %>" UploadedFileProcessorType="Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets.RenameFileUploadedProcessor, Ektron.Cms.Framework.UI.Controls.EktronUI" TurnOffFileExistenceValidation="true" />
                                    </div>
                                    <%--<div class="uploadWebConfig">
                                            <asp:Literal ID="aspWebConfig" runat="server" Text="<%$ Resources:WebConfigFile %>" />:    
                                            <ektronUI:FileUpload ID="uxWebConfigFile" runat="server" MaxUploads="1" FileTypeFilter="Web Configuration|*.config;" Path="<%# this.UploadPath %>" FinishedMessage="<%$ Resources: UploadFinished %>" UploadedFileProcessorType="Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets.RenameFileUploadedProcessor, Ektron.Cms.Framework.UI.Controls.EktronUI" TurnOffFileExistenceValidation="true" />
                                            <span class="ektron-ui-required">*</span>
                                        </div>--%>
                                </div>
                            </div>
                        </asp:View>
                    </asp:MultiView>
                    <div class="buttons">
                        <ektronUI:Button ID="uxNext" runat="server" DisplayMode="Anchor" Text="<%$ Resources:Next %>" OnCommand="WizardButtonClick" CommandArgument="Next" Visible="<%# (aspWizardSteps.ActiveViewIndex == (aspWizardSteps.Views.Count - 1) ? false : true) %>" CssClass="primary-action" />
                        <ektronUI:Button ID="uxCreate" runat="server" DisplayMode="Anchor" Text="<%$ Resources:SaveText %>" OnCommand="WizardButtonClick" CommandArgument="Create" Visible="<%# (aspWizardSteps.ActiveViewIndex == (aspWizardSteps.Views.Count - 1) ? true : false) %>" CssClass="primary-action" />
                        <ektronUI:Button ID="uxPrevious" runat="server" DisplayMode="Anchor" Text="<%$ Resources:Previous %>" ValidationGroup="None" OnCommand="WizardButtonClick" CommandArgument="Previous" Visible="<%# (aspWizardSteps.ActiveViewIndex > 0 ? true : false) %>" CssClass="ektron-ui-floatLeft" />
                        <ektronUI:Button ID="uxCancel" runat="server" DisplayMode="Anchor" Text="<%$ Resources:Cancel %>" CausesValidation="false" OnCommand="WizardButtonClick" CommandArgument="Cancel" />
                    </div>
                </asp:View>
                <asp:View ID="aspSuccessView" runat="server">
                    <ektronUI:JavaScript ID="uxSyncRelationshipsJs" runat="server" Path="{WorkareaPath}/sync/js/Ektron.Workarea.Sync.Relationships.js" />
                    <ektronUI:JavaScriptBlock ID="uxDeploymentUiReadyJs" runat="server">
                        <ScriptTemplate>
                            parent.Ektron.Workarea.Cloud.Dialogs.Deployment.setHeight($ektron("body").outerHeight() + 70);
                                parent.Ektron.Workarea.Cloud.Dialogs.Deployment.progressHandler = "<%= this.ProgressHandlerPath %>";
                                parent.Ektron.Workarea.Cloud.Dialogs.Deployment.errorMessageSelector = "#" + "<%= this.uxError.ClientID %>" + " .ektron-ui-messageBody";
                                parent.Ektron.Workarea.Cloud.Dialogs.Deployment.execute();
                        </ScriptTemplate>
                    </ektronUI:JavaScriptBlock>
                    <ektronUI:JavaScriptBlock ID="uxDeploymentUiParseJs" runat="server" ExecutionMode="OnParse">
                        <ScriptTemplate>
                            Ektron.Namespace.Register("Ektron.Workarea.Cloud.Dialogs.Deployment");
                                Ektron.Workarea.Cloud.Dialogs.Deployment.Create = {
                                    data:'<%= this.DeploymentData %>',
                                    xhrError: function(message){
                                        $ektron("#wizard-progress .syncing").fadeOut(function(){
                                            $ektron("#wizard-progress .error-message .ektron-ui-messageBody").text(message);
                                            $ektron("#wizard-progress .error-message").fadeIn("fast", function(){
                                                $ektron("#wizard-progress .buttons > span.resubmit").fadeIn("fast");
                                                $ektron("#wizard-progress .buttons > span").fadeIn("fast");
                                            });
                                        });
                                    },
                                    serverError: function(message){
                                        $ektron("#wizard-progress .syncing").fadeOut(function(){
                                            $ektron("#wizard-progress .error-message .ektron-ui-messageBody").text(message);
                                            $ektron("#wizard-progress .error-message").fadeIn("fast", function(){
                                                $ektron("#wizard-progress .buttons > span.resubmit").fadeIn("fast");
                                                $ektron("#wizard-progress .buttons > span.close").fadeIn("fast");
                                            });
                                        });
                                    },
                                    success: function(data){
                                        $ektron("#wizard-progress .syncing").fadeOut(function(){
                                            $ektron("#wizard-progress .success-message").fadeIn("fast", function(){
                                                $ektron("p.header").hide();
                                                var deploySiteButton = $ektron("#wizard-progress .buttons > span.deploy-site");
                                                deploySiteButton.fadeIn("fast");
                                                deploySiteButton.find("#uxDeploySiteNow .ui-button-text").attr("rel", data.ProfileId);
                                                $ektron("#wizard-progress .buttons > span.close").fadeIn("fast");
                                            });
                                        });
                                    }
                                };
                        </ScriptTemplate>
                    </ektronUI:JavaScriptBlock>
                    <ektronUI:CssBlock ID="uxDeploymentUiStyle" runat="server">
                        <CssTemplate>
                            .ektron-cloud-deployment .ektron-ui-message {margin-bottom:1em !important;}
                                #wizard-progress { min-height: 8em; }
                        </CssTemplate>
                    </ektronUI:CssBlock>
                    <div id="wizard-progress">
                        <div class="loading-view">
                            <div class="complete-message">
                                <div class="success-message ektron-ui-hidden">
                                    <p>
                                        <asp:Literal ID="aspSaveHostedServiceConfirmationIntro" runat="server" Text="<%$ Resources:SaveHostedServiceConfirmationIntro %>" /></p>
                                    <p>
                                        <asp:Literal ID="aspWizardSuccessMessage" runat="server" Text="<%$ Resources:SuccessMessage %>" /></p>
                                </div>
                                <div class="error-message ektron-ui-hidden">
                                    <ektronUI:Message ID="uxWizardErrorMessage" runat="server" DisplayMode="Error" />
                                </div>
                            </div>
                            <div class="syncing">
                                <asp:Image ID="aspLoadingImage" runat="server" ImageUrl="~/Workarea/sync/images/creating-deployment.gif" AlternateText="<%$ Resources:CreatingDeploymentMessage %>" Height="19px" Width="220px" />
                            </div>
                        </div>
                        <div class="success-view ektron-ui-hidden">
                            <ektronUI:Message ID="uxSuccess" runat="server" DisplayMode="Success" Text="<%$ Resources:SuccessMessage %>" />
                        </div>
                        <div class="error-view ektron-ui-hidden">
                            <ektronUI:Message ID="uxError" runat="server" DisplayMode="Error" />
                        </div>
                        <div class="buttons">
                            <span class="ektron-ui-hidden resubmit">
                                <ektronUI:Button ID="uxResubmit" runat="server" DisplayMode="Anchor" Text="<%$ Resources:Resubmit %>" OnCommand="WizardButtonClick" CommandArgument="Resubmit" />
                            </span>
                            <span class="ektron-ui-hidden deploy-site">
                                <ektronUI:Button ID="uxDeploySiteNow" runat="server" DisplayMode="Anchor" Text="<%$ Resources:DeploySiteNow %>" OnClientClick="parent.Ektron.Workarea.Cloud.Dialogs.Deployment.cancel(false);parent.Ektron.Workarea.Sync.Relationships.Synchronize(this, false);" />
                            </span>
                            <span class="ektron-ui-hidden close">
                                <ektronUI:Button ID="uxClose" runat="server" DisplayMode="Anchor" Text="<%$ Resources:Close %>" OnClientClick="parent.Ektron.Workarea.Cloud.Dialogs.Deployment.close();" />
                            </span>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
    </form>
</body>
</html>
