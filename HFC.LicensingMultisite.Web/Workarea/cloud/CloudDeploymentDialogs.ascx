﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CloudDeploymentDialogs.ascx.cs" Inherits="Ektron.Workarea.Cloud.DeploymentDialogs" %>

<%@ Register Assembly="Ektron.Cms.Framework.UI.Controls.EktronUI" Namespace="Ektron.Cms.Framework.UI.Controls.EktronUI" TagPrefix="ektronUI" %>

<ektronUI:Css ID="uxCloudDialogStyles" runat="server" Path="{WorkareaPath}/cloud/css/ektron.workarea.cloud.css" />
<ektronUI:JavaScript ID="uxWorkareaCloudJs" runat="server" Path="{WorkareaPath}/cloud/js/Ektron.Workarea.Cloud.js" />
<ektronUI:JavaScriptBlock ID="uxWorkareaCloudJsBlock" runat="server">
    <scripttemplate>
        Ektron.Workarea.Cloud.handler = "<%= this.HandlerPath %>";

        Ektron.Workarea.Cloud.Dialogs.closeText = "<asp:Literal ID="aspCancelText" runat="server" Text="<%$ Resources:uxDialogCloseButtonText %>" />";

        Ektron.Workarea.Cloud.Dialogs.ChangeStatus.headerFormat = "<asp:Literal ID="aspStatusHeaderFormat" runat="server" Text="<%$ Resources:statusHeaderFormat %>" />";

        Ektron.Workarea.Cloud.Dialogs.ChangeStatus.selector = "<%= uxChangeStatusDialog.Selector%>";
        Ektron.Workarea.Cloud.Dialogs.ChangeStatus.progressImageSelector = "<%= uxChangeStatusDialog.Selector%> div.progress-image";
        Ektron.Workarea.Cloud.Dialogs.ChangeStatus.confirmationMessageSelector = "<%= uxChangeStatusDialog.Selector%> div.confirmation-message";
        Ektron.Workarea.Cloud.Dialogs.ChangeStatus.successMessageSelector = "<%= uxChangeStatusDialog.Selector%> div.success-message";
        Ektron.Workarea.Cloud.Dialogs.ChangeStatus.errorMessageSelector = "<%= uxChangeStatusDialog.Selector%> div.error-message";

        Ektron.Workarea.Cloud.Dialogs.StartSite.headerFormat = "<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:statusHeaderFormat %>" />";

        Ektron.Workarea.Cloud.Dialogs.StartSite.selector = "<%= uxStartSiteConfirmationDialog.Selector%>";
        Ektron.Workarea.Cloud.Dialogs.StartSite.progressImageSelector = "<%= uxStartSiteConfirmationDialog.Selector%> div.progress-image";
        Ektron.Workarea.Cloud.Dialogs.StartSite.confirmationMessageSelector = "<%= uxStartSiteConfirmationDialog.Selector%> div.confirmation-message";
        Ektron.Workarea.Cloud.Dialogs.StartSite.successMessageSelector = "<%= uxStartSiteConfirmationDialog.Selector%> div.success-message";
        Ektron.Workarea.Cloud.Dialogs.StartSite.errorMessageSelector = "<%= uxStartSiteConfirmationDialog.Selector%> div.error-message";

        Ektron.Workarea.Cloud.Dialogs.StopSite.headerFormat = "<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:statusHeaderFormat %>" />";

        Ektron.Workarea.Cloud.Dialogs.StopSite.selector = "<%= uxStopSiteConfirmationDialog.Selector%>";
        Ektron.Workarea.Cloud.Dialogs.StopSite.progressImageSelector = "<%= uxStopSiteConfirmationDialog.Selector%> div.progress-image";
        Ektron.Workarea.Cloud.Dialogs.StopSite.confirmationMessageSelector = "<%= uxStopSiteConfirmationDialog.Selector%> div.confirmation-message";
        Ektron.Workarea.Cloud.Dialogs.StopSite.successMessageSelector = "<%= uxStopSiteConfirmationDialog.Selector%> div.success-message";
        Ektron.Workarea.Cloud.Dialogs.StopSite.errorMessageSelector = "<%= uxStopSiteConfirmationDialog.Selector%> div.error-message";

        Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.selector = "<%= uxConfigureInstanceDialog.Selector%>";
        Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.progressImageSelector = "<%= uxConfigureInstanceDialog.Selector%> div.progress-image";
        Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.confirmationMessageSelector = "<%= uxConfigureInstanceDialog.Selector%> div.confirmation-message";
        Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.successMessageSelector = "<%= uxConfigureInstanceDialog.Selector%> div.success-message";
        Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.errorMessageSelector = "<%= uxConfigureInstanceDialog.Selector%> div.error-message";
        Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.instanceSelector = "<%= uxConfigureInstanceDialog.Selector%> table.instance-data";
        Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.instanceCountSelector = "<%= uxConfigureInstanceDialog.Selector%> .integer-field input";

        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.selector = "<%= uxDeleteRelationshipDialog.Selector%>";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.amazondeleteselector = ".amazon-delete-panel";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.azuredeleteselector = ".azure-delete-panel";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.amazoninstanceselector = ".amazon-instance-panel";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.azureinstanceselector = ".azure-instance-panel";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.progressImageSelector = "<%= uxDeleteRelationshipDialog.Selector%> div.progress-image";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.confirmationMessageSelector = "<%= uxDeleteRelationshipDialog.Selector%> div.confirmation-message";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.successMessageSelector = "<%= uxDeleteRelationshipDialog.Selector%> div.success-message";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.errorMessageSelector = "<%= uxDeleteRelationshipDialog.Selector%> div.error-message";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deleteSelector = "<%= uxDeleteRelationshipDialog.Selector%> div.delete-data";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deleteRelationAndProfiles = "<%= uxDeleteRelationshipDialog.Selector%> .delete-relation-profiles input";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deleteRoles = "<%= uxDeleteRelationshipDialog.Selector%> .delete-roles input";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deleteStorageData = "<%= uxDeleteRelationshipDialog.Selector%> .delete-storage-data input";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deleteAmazonStorageData = "<%= uxDeleteRelationshipDialog.Selector%> .delete-amazon-storage-data input";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deleteRDSAmazonDatabase = "<%= uxDeleteRelationshipDialog.Selector%> .delete-sql-amazon-rds-server input";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deleteSqlAzure = "<%= uxDeleteRelationshipDialog.Selector%> .delete-sql-azure input";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deleteSqlAmazon = "<%= uxDeleteRelationshipDialog.Selector%> .delete-sql-amazon input";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deleteServiceName = "<%= uxDeleteRelationshipDialog.Selector %> .delete-service-name label";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deleteConfirmServiceName = "<%= uxDeleteRelationshipDialog.Selector %> .delete-confirm-service-name input";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.validationMessageSelector = "<%= uxDeleteRelationshipDialog.Selector %> div.validation-message";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.confirmdeletesvcname = ".delete-service-name-label";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.confirmdeletesvcnameinput = ".delete-confirm-svcname-input input";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.confirmdelmessage = ".delete-svcname-required";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.hdndeletewarningmessage = "<%= uxDeleteRelationshipDialog.Selector %> .lbl-hidden-del-confirm-warning";
        Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deletewarningmessage = "<%= uxDeleteRelationshipDialog.Selector %> .delete-rel-confirm-message > .ektron-ui-messageBody";
        
        Ektron.Workarea.Cloud.Dialogs.Deployment.selector = "<%= uxDeploymentDialog.Selector %>";
        Ektron.Workarea.Cloud.Dialogs.Deployment.progressImage = "<%= uxDeploymentDialog.Selector%> div.progress-image";

        Ektron.Workarea.Cloud.Dialogs.SwapDeployment.selector = "<%= uxSwapDeploymentDialog.Selector%>";
        Ektron.Workarea.Cloud.Dialogs.SwapDeployment.progressImageSelector = "<%= uxSwapDeploymentDialog.Selector%> div.progress-image";
        Ektron.Workarea.Cloud.Dialogs.SwapDeployment.confirmationMessageSelector = "<%= uxSwapDeploymentDialog.Selector%> div.confirmation-message";
        Ektron.Workarea.Cloud.Dialogs.SwapDeployment.successMessageSelector = "<%= uxSwapDeploymentDialog.Selector%> div.success-message";
        Ektron.Workarea.Cloud.Dialogs.SwapDeployment.errorMessageSelector = "<%= uxSwapDeploymentDialog.Selector%> div.error-message";
        Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.deployamazondialog = "<%= uxDeployAmazonDialog.Selector %>";

        Ektron.Workarea.Cloud.Dialogs.Deployment.init();

        $ektron("<%= uxChangeStatusDialog.Selector%>").bind("dialogopen", function() {
            $ektron(this)
                .dialog("option", "closeOnEscape", false)
                .closest(".ui-dialog").find(".ui-dialog-titlebar-close").hide();
        });
        $ektron("<%= uxConfigureInstanceDialog.Selector%>").bind("dialogopen", function() {
            $ektron(this)
                .dialog("option", "closeOnEscape", false)
                .closest(".ui-dialog").find(".ui-dialog-titlebar-close").hide();
        });
        $ektron("<%= uxDeleteRelationshipDialog.Selector%>").bind("dialogopen", function() {
            $ektron(this)
                .dialog("option", "closeOnEscape", false)
                .closest(".ui-dialog").find(".ui-dialog-titlebar-close").hide();
                $ektron(Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.deletewarningmessage).html($ektron(Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.hdndeletewarningmessage).html());
        });
        $ektron("<%= uxSwapDeploymentDialog.Selector%>").bind("dialogopen", function() {
            $ektron(this)
                .dialog("option", "closeOnEscape", false)
                .closest(".ui-dialog").find(".ui-dialog-titlebar-close").hide();
        });

        $ektron("<%= uxDeploymentDialog.Selector%>").bind("dialogopen", function() {
            $ektron(this)
                .dialog("option", "closeOnEscape", false)
                //.closest(".ui-dialog").find(".ui-dialog-titlebar-close").hide();
        });

        $ektron("<%= uxStartSiteConfirmationDialog.Selector%>").bind("dialogopen", function() {
            $ektron(this)
                .dialog("option", "closeOnEscape", false)
        });

        $ektron("<%= uxStopSiteConfirmationDialog.Selector%>").bind("dialogopen", function() {
            $ektron(this)
                .dialog("option", "closeOnEscape", false)
        });
        
    </scripttemplate>
</ektronUI:JavaScriptBlock>
<ektronUI:Dialog ID="uxChangeStatusDialog" runat="server" AutoOpen="False" Modal="true" CssClass="cloud-dialog" Title="<%$ Resources:uxChangeStatusDialogTitle %>" Width="400">
    <buttons>
        <ektronUI:DialogButton ID="uxChangeStatusDialogButtonOk" runat="server" Text="<%$ Resources:uxDialogOkButtonText %>" OnClientClick="Ektron.Workarea.Cloud.Dialogs.ChangeStatus.execute();return false;" />
        <ektronUI:DialogButton ID="uxChangeStatusDialogButtonCancel" runat="server" CloseDialog="true" Text="<%$ Resources:uxDialogCancelButtonText %>" />
    </buttons>
    <contenttemplate>
        <div class="confirmation-message">
            <ektronUI:Message ID="uxChangeStatusConfirmationMessage" runat="server" DisplayMode="Warning" Text="<%$ Resources:uxChangeStatusConfirmationMessageText %>" />
        </div>
        <div class="success-message ektron-ui-hidden">
            <ektronUI:Message ID="uxChangeStatusSuccessMessage" runat="server" DisplayMode="Success" Text="<%$ Resources:uxChangeStatusSuccessMessageText %>" />
        </div>
        <div class="error-message ektron-ui-hidden">
            <ektronUI:Message ID="uxChangeStatusErrorMessage" runat="server" DisplayMode="Error" />
        </div>
        <div class="progress-image ektron-ui-hidden">
            <asp:Image ID="uxChangeStatusProgressImage" runat="server" ImageUrl="~/Workarea/sync/images/creating-deployment.gif" AlternateText="<%$ Resources:Loading %>" Height="19px" Width="220px" />
        </div>
    </contenttemplate>
</ektronUI:Dialog>
<ektronUI:Dialog ID="uxStopSiteConfirmationDialog" runat="server" AutoOpen="False" Modal="true" CssClass="cloud-dialog" Title="<%$ Resources:StopSiteDialogTitle %>" Width="400">
    <buttons>
        <ektronUI:DialogButton ID="uxStopSiteConfirmationDialogOkButton" runat="server" Text="<%$ Resources:uxStopSiteConfirmationDialogOkButtonText %>" OnClientClick="Ektron.Workarea.Cloud.Dialogs.StopSite.execute();return false;" />
        <ektronUI:DialogButton ID="uxStopSiteConfirmationDialogCancelButton" runat="server" CloseDialog="true" Text="<%$ Resources:uxDialogCancelButtonText %>" />
    </buttons>
    <contenttemplate>
        <div class="confirmation-message">
            <ektronUI:Message ID="uxStopSiteConfirmationDialogWarningMessage" runat="server" DisplayMode="Warning" Text="<%$ Resources:uxStopSiteConfirmationDialogWarningMessageText %>" />
        </div>
        <div class="success-message ektron-ui-hidden">
            <ektronUI:Message ID="uxStopSiteConfirmationDialogSuccessMessage" runat="server" DisplayMode="Success" Text="<%$ Resources:uxStopSiteConfirmationDialogSuccessMessageText %>" />
        </div>
        <div class="error-message ektron-ui-hidden">
            <ektronUI:Message ID="uxStopSiteConfirmationDialogErrorMessage" runat="server" DisplayMode="Error" />
        </div>
        <div class="progress-image ektron-ui-hidden">
            <asp:Image ID="uxStopSiteConfirmationDialogProgressImage" runat="server" ImageUrl="~/Workarea/sync/images/creating-deployment.gif" AlternateText="<%$ Resources:Loading %>" Height="19px" Width="220px" />
        </div>
    </contenttemplate>
</ektronUI:Dialog>

<ektronUI:Dialog ID="uxStartSiteConfirmationDialog" runat="server" AutoOpen="False" Modal="true" CssClass="cloud-dialog" Title="<%$ Resources:StartSiteDialogTitle %>" Width="400">
    <buttons>
        <ektronUI:DialogButton ID="uxStartSiteConfirmationDialogOkButton" runat="server" Text="<%$ Resources:uxStartSiteConfirmationDialogOkButtonText %>" OnClientClick="Ektron.Workarea.Cloud.Dialogs.StartSite.execute();return false;" />
        <ektronUI:DialogButton ID="uxStartSiteConfirmationDialogCancelButton" runat="server" CloseDialog="true" Text="<%$ Resources:uxDialogCancelButtonText %>" />
    </buttons>
    <contenttemplate>
        <div class="confirmation-message">
            <ektronUI:Message ID="uxStartSiteConfirmationDialogWarningMessage" runat="server" DisplayMode="Warning" Text="<%$ Resources:uxStartSiteConfirmationDialogWarningMessageText %>" />
        </div>
        <div class="success-message ektron-ui-hidden">
            <ektronUI:Message ID="uxStartSiteConfirmationDialogSuccessMessage" runat="server" DisplayMode="Success" Text="<%$ Resources:uxStartSiteConfirmationDialogSuccessMessageText %>" />
        </div>
        <div class="error-message ektron-ui-hidden">
            <ektronUI:Message ID="uxStartSiteConfirmationDialogErrorMessage" runat="server" DisplayMode="Error" />
        </div>
        <div class="progress-image ektron-ui-hidden">
            <asp:Image ID="uxStartSiteConfirmationDialogProgressImage" runat="server" ImageUrl="~/Workarea/sync/images/creating-deployment.gif" AlternateText="<%$ Resources:Loading %>" Height="19px" Width="220px" />
        </div>
    </contenttemplate>
</ektronUI:Dialog>

<ektronUI:Dialog ID="uxSwapDeploymentDialog" runat="server" AutoOpen="false" Modal="true" CssClass="cloud-dialog" Title="<%$ Resources:uxSwapDeploymentDialogTitle %>" Width="400">
    <buttons>
        <ektronUI:DialogButton ID="uxSwapDeploymentDialogButtonOk" runat="server" Text="<%$ Resources:uxDialogOkButtonText %>" OnClientClick="Ektron.Workarea.Cloud.Dialogs.SwapDeployment.execute();return false;" />
        <ektronUI:DialogButton ID="uxSwapDeploymentDialogButtonCancel" runat="server" CloseDialog="true" Text="<%$ Resources:uxDialogCancelButtonText %>" />
    </buttons>
    <contenttemplate>
        <div class="confirmation-message">
            <ektronUI:Message ID="uxSwapDeploymentConfirmationMessage" runat="server" DisplayMode="Warning" Text="<%$ Resources:uxSwapDeploymentConfirmationMessageText %>" />
        </div>
        <div class="success-message ektron-ui-hidden">
            <ektronUI:Message ID="uxSwapDeploymentSuccessMessage" runat="server" DisplayMode="Success" Text="<%$ Resources:uxSwapDeploymentSuccessMessageText %>" />
        </div>
        <div class="error-message ektron-ui-hidden">
            <ektronUI:Message ID="uxSwapDeploymentErrorMessage" runat="server" DisplayMode="Error" />
        </div>
        <div class="progress-image ektron-ui-hidden">
            <asp:Image ID="uxSwapDeploymentProgressImage" runat="server" ImageUrl="~/Workarea/sync/images/creating-deployment.gif" AlternateText="<%$ Resources:Loading %>" Height="19px" Width="220px" />
        </div>
    </contenttemplate>
</ektronUI:Dialog>

<ektronUI:Dialog ID="uxDeleteRelationshipDialog" runat="server" AutoOpen="false" Modal="true" CssClass="cloud-dialog delete-hosted-service" Title="<%$ Resources:uxDeleteRelationshipDialogTitle %>" Width="600">
    <contenttemplate>
    <div style="display:none">
     <ektronUI:Label ID="uxlblDeleteWarningMessage" runat="server" CssClass="lbl-hidden-del-confirm-warning" Text="" />
    </div>
        <div class="confirmation-message">
            <ektronUI:Message ID="uxDeleteRelationshipConfirmationMessage" CssClass="delete-rel-confirm-message" runat="server" DisplayMode="Warning" Text="" />
        </div>
        <div class="success-message ektron-ui-hidden">
            <ektronUI:Message ID="uxDeleteRelationshipSuccessMessage" runat="server" DisplayMode="Success" Text="<%$ Resources:uxDeleteRelationshipSuccessMessageText %>" />
        </div>
        <div class="error-message ektron-ui-hidden">
            <ektronUI:Message ID="uxDeleteRelationshipErrorMessage" runat="server" DisplayMode="Error" />
        </div>
        <div class="validation-message ektron-ui-hidden">
            <ektronUI:Message ID="uxDeleteRelationshipValidationMessage" Text="<%$ Resources:uxDeleteRelationshipValidationMessage %>" runat="server" DisplayMode="Error" />
        </div>
        <div class="progress-image ektron-ui-hidden">
            <asp:Image ID="uxDeleteRelationshipProgressImage" runat="server" ImageUrl="~/Workarea/sync/images/creating-deployment.gif" AlternateText="<%$ Resources:Loading %>" Height="19px" Width="220px" />
        </div>
        <div class="delete-data">
            <asp:Literal ID="aspSelectionDescription" runat="server" Text="<%$ Resources:DeleteRelationshipCheckboxDescription %>" />
            <asp:Panel runat="server" ID="pnlAzureDelete" CssClass="azure-delete-panel">
                <table>
                <tbody>
                    <tr>
                        <th class="delete-relation-profiles">
                            <asp:CheckBox ID="chkRelationAndProfiles" runat="server" Checked="true" Enabled="false" />
                        </th>
                        <td>
                            <ektronUI:Label ID="uxRelationshipAndProfiles" runat="server" Text="<%$ Resources:uxRelationshipAndProfiles %>" AssociatedControlID="chkRelationAndProfiles" />
                        </td>
                    </tr>
                    <tr>
                        <th class="delete-roles">
                            <asp:CheckBox ID="chkDeleteRoles" runat="server" />
                        </th>
                        <td>
                            <ektronUI:Label ID="uxDeleteRoles" runat="server" Text="<%$ Resources:uxDeleteRoles %>" AssociatedControlID="chkDeleteRoles" />
                        </td>
                    </tr>
                    <tr>
                        <th class="delete-storage-data">
                            <asp:CheckBox ID="chkStorageData" runat="server" />
                        </th>
                        <td>
                            <ektronUI:Label ID="uxStorageData" runat="server" Text="<%$ Resources:uxStorageData %>" AssociatedControlID="chkStorageData" />
                        </td>
                    </tr>
                    <tr>
                        <th class="delete-sql-azure">
                            <asp:CheckBox ID="chkSqlAzure" runat="server" />
                        </th>
                        <td>
                            <ektronUI:Label ID="uxSqlAzure" runat="server" Text="<%$ Resources:uxSqlAzure %>" AssociatedControlID="chkSqlAzure" />
                        </td>
                    </tr>
                </tbody>
            </table>
            </asp:Panel>
            <asp:Panel ID="pnlAmazonDelete" runat="server" CssClass="amazon-delete-panel">
                <table>
                <tbody>
                    <tr>
                        <th class="delete-relation-profiles">
                            <asp:CheckBox ID="aspAmazonRelationAndProfiles" runat="server" Checked="true" Enabled="false" />
                        </th>
                        <td>
                            <ektronUI:Label ID="uxAmazonRelationshipAndProfiles" runat="server" Text="<%$ Resources:uxAmazonRelationshipAndProfiles %>" AssociatedControlID="aspAmazonRelationAndProfiles" />, 
                            <ektronUI:Label ID="uxAmazonVirtualMachines" runat="server" Text="<%$ Resources:uxAmazonVirtualMachines %>" />
                        </td>
                    </tr>
                    <tr>
                        <th class="delete-amazon-storage-data">
                            <asp:CheckBox ID="aspAmazonDataInStorage" runat="server" />
                        </th>
                        <td>
                            <ektronUI:Label ID="uxAmazonDataInStorage" runat="server" Text="<%$ Resources:uxStorageData %>" AssociatedControlID="aspAmazonDataInStorage" />
                        </td>
                    </tr>
                    <tr>
                        <th class="delete-sql-amazon">
                            <asp:CheckBox ID="aspAmazonDatabase" runat="server" />
                        </th>
                        <td>
                            <ektronUI:Label ID="uxAmazonDatabase" runat="server" Text="<%$ Resources:uxAmazonDatabase %>" AssociatedControlID="aspAmazonDatabase" />
                        </td>
                    </tr>
                    <tr class="rds-server">
                        <th class="delete-sql-amazon-rds-server">
                            <asp:CheckBox ID="aspAmazonRDSServer" runat="server" />
                        </th>
                        <td>
                            <ektronUI:Label ID="uxAmazonRDSServer" runat="server" Text="<%$ Resources:uxAmazonRDSServer %>" AssociatedControlID="aspAmazonRDSServer" />
                        </td>
                    </tr>
                </tbody>
            </table>
            </asp:Panel>
            <table>
                <tbody>
                    <tr>
                        <td>
                            <div class="confirm-service-name">
                                <ektronUI:Label ID="uxConfirmServiceName" runat="server" Text="<%$ Resources:uxConfirmServiceName %>" />
                                <strong><asp:Label id="lblConfirmServiceNameLabel" runat="server" CssClass="delete-service-name-label"/></strong>
                                <span class="delete-service-name">
                                    <ektronUI:Label ID="uxLblServiceName" runat="server" Text="" CssClass="" AssociatedControlID="uxServiceName" /> 
                                </span>
                            </div>
                            <div class="delete-confirm-service-name">
                                <ektronUI:TextField ID="uxServiceName" runat="server">
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="Enter Service Name" />
                                    </ValidationRules>
                                </ektronUI:TextField>
								<span class="ektron-ui-required">*</span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </contenttemplate>
    <buttons>
        <ektronUI:DialogButton ID="uxDeleteRelationshipDialogButtonDelete" runat="server" Text="<%$ Resources:uxDialogDeleteButtonText %>" OnClientClick="Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.execute(); return false;" />
        <ektronUI:DialogButton ID="uxDeleteRelationshipDialogButtonCancel" runat="server" CloseDialog="true" Text="<%$ Resources:uxDialogCancelButtonText %>" />
    </buttons>
</ektronUI:Dialog>

<ektronUI:Dialog ID="uxConfigureInstanceDialog" runat="server" AutoOpen="false" Modal="true" CssClass="cloud-dialog configure-instance" Title="<%$ Resources:uxConfigureInstanceDialogTitle %>" Width="530">
    <buttons>
        <ektronUI:DialogButton ID="uxConfigureInstanceDialogButtonOk" runat="server" OnClientClick="Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.execute();return false;" Text="<%$ Resources:uxDialogButtonSave %>" />
        <ektronUI:DialogButton ID="uxConfigureInstanceDialogButtonCancel" runat="server" CloseDialog="true" Text="<%$ Resources:uxDialogCancelButtonText %>" />
    </buttons>
    <contenttemplate>
        <div class="confirmation-message">
            <asp:Literal ID="aspConfigureInstanceConfirmationMessage" runat="server" Text="<%$ Resources:uxConfigureInstanceConfirmationMessage %>" />
        </div>
        <div class="success-message ektron-ui-hidden">
            <ektronUI:Message ID="uxConfigureInstanceSuccessMessage" runat="server" DisplayMode="Success" Text="<%$ Resources:uxConfigureInstanceSuccessMessage %>" />
        </div>
        <div class="error-message ektron-ui-hidden">
            <ektronUI:Message ID="uxConfigureInstanceErrorMessage" runat="server" DisplayMode="Error" />
        </div>
        <div class="progress-image ektron-ui-hidden">
            <asp:Image ID="uxConfigureInstanceProgressImage" runat="server" ImageUrl="~/Workarea/sync/images/creating-deployment.gif" AlternateText="<%$ Resources:Loading %>" Height="19px" Width="220px" />
        </div>
        <table class="instance-data">
            <tbody>
                <tr>
                    <td colspan="2">
                        <ektronUI:Message ID="uxWebInstanceMessage" DisplayMode="Warning" runat="server" Text="<%$ Resources:uxWebInstanceMessageText %>"/>            
                    </td>
                </tr>
                <tr>
                    <th>
                        <ektronUI:Label ID="uxConfigureInstanceCountLabel" runat="server" Text="<%$ Resources:uxConfigureInstanceCountLabel %>" AssociatedControlID="uxChangeInstanceCount" />            
                    </th>
                    <td class="integer-field">
                        <ektronUI:IntegerField ID="uxChangeInstanceCount" runat="server" MinValue="1">
                            <ValidationRules>
                                <ektronUI:CustomRule ClientValidationEnabled="true" JavascriptFunctionName="Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.VerifyWebInstanceCount" ErrorMessage="<%$ Resources:uxWebInstanceValidationErrorText %>" />
                            </ValidationRules>
                        </ektronUI:IntegerField>    
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                    <span class="ektron-ui-required">
                   <ektronUI:ValidationMessage ID="uxVMWebInstanceCount" runat="server" AssociatedControlID="uxChangeInstanceCount" Text="" />

                    </span>
                        </td>
                </tr>
            </tbody>
        </table>
        <div class="configure-instance-note">
            <asp:Literal ID="aspConfigureInstanceNote" runat="server" Text="<%$ Resources:uxConfigureInstanceNoteAzure %>" />
            <asp:Literal ID="ltrHelpCloudLoadBalancing" runat="server" />
        </div>
    </contenttemplate>
</ektronUI:Dialog>

<ektronUI:Dialog ID="uxDeploymentDialog" runat="server" Title="<%$ Resources:uxCloudDeploymentWizardDialogTitle %>" Modal="true" Width="600" AutoOpen="false" CssClass="create-hosted-service-wizard">
    <contenttemplate>
        <iframe src="" width="100%" frameborder="0"></iframe>
    </contenttemplate>
</ektronUI:Dialog>

<ektronUI:Dialog ID="uxDeployAmazonDialog" runat="server" Title="<%$ Resources:DeploymentDialogTitle %>" Modal="true" Width="600" AutoOpen="false">
    <contenttemplate>
        <p>
            <asp:Literal runat="server" ID="aspDeployDesc" Text="<% $Resources: aspDeployDesc %>" />
        </p>
    </contenttemplate>
    <buttons>
        <ektronUI:DialogButton ID="uxDeployAmazonButton" runat="server" OnClientClick="Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.deploySite();" Text="<%$ Resources:uxDeploySite %>" />
        <ektronUI:DialogButton ID="uxCancelAmazonButton" runat="server" CloseDialog="true" Text="<%$ Resources:uxDialogCancelButtonText %>" />
    </buttons>
</ektronUI:Dialog>


