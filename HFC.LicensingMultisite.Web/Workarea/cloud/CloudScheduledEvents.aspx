﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CloudScheduledEvents.aspx.cs" Inherits="Ektron.Workarea.Cloud.Deployment.ScheduledEvents" culture="auto" meta:resourcekey="PageResourceKey" uiculture="auto" %>
<!DOCTYPE>
<html>
    <head runat="server">
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <form id="form1" runat="server">
            <ektronUI:JavaScript ID="uxWorakreaJs" runat="server" Path="{WorkareaPath}/java/ektron.workarea.js" />
            <div class="ektron-scheduled-cloud-events">
                <ektronUI:CssBlock ID="uxCloudDeploymentScheduleStyles" runat="server">
                    <CssTemplate>
                        .ektron-view-scheduled-cloud-events { width: 100%; }
                        .ektron-scheduled-cloud-events .no-wrap {white-space:nowrap;}

                        .ektron-scheduled-cloud-events table .column-delete { width: 3em; }
                        .ektron-scheduled-cloud-events table .column-description {width:20em;}
                        .ektron-scheduled-cloud-events table .column-date {width:15em;}
                        .ektron-scheduled-cloud-events table .column-status {width:8em;}
                        .ektron-scheduled-cloud-events table .column-event-type {width:9em;}
                        .ektron-scheduled-cloud-events table .column-web-instances {width:9em;text-align:center;}
                        .ektron-scheduled-cloud-events table .column-email-notifications { /*min-width: 25em;*/ }
                        
                        .ektron-scheduled-cloud-events table th,
                        table.ektron-view-scheduled-cloud-events td {vertical-align:top; padding: 1em;}
                        .ektron-scheduled-cloud-events table th { color: #787878; font-size: 95%; }
                        .ektron-ui-even {background-color: #fff;}
                        .ektron-sort-ascending {}
                        .ektron-sort-descending {}
                        .dialog-confirmation-message { margin: 1em; margin-bottom: 2em; }
                        
                        .deleteEventsButton { font-weight: bold; } 
                    </CssTemplate>
                </ektronUI:CssBlock>
                <ektronUI:JavaScriptBlock ID="uxOpenDialogScript" runat="server" ExecutionMode="OnParse">
                    <ScriptTemplate>
                        function openDialog(){
                            $ektron("<%= diagDeleteConfirm.Selector%>").dialog("open");
                        }
            
                        function closeDialog(){
                            $ektron("<%= diagDeleteConfirm.Selector%>").dialog("close");
                        }
                    </ScriptTemplate>
                </ektronUI:JavaScriptBlock>
                <div class="ektronPageHeader">
                    <div class="ektronTitlebar" id="divTitleBar" runat="server">
                        <span id="WorkareaTitleBar"><asp:Literal ID="aspHeaderTextId" runat="server" Text="<%$ Resources:ViewScheduledCloudEventsText %>" /></span>
                        <span id="_WorkareaTitleBar" style="display:none;"></span>
                    </div>
                    <div class="ektronToolbar" id="divToolBar" runat="server">
                        <table>
                            <tr>
                                <td><asp:LinkButton ID="aspBackButton" runat="server" CausesValidation="false" CssClass="primary backButton" PostBackUrl="~/Workarea/cloud/CloudDeployment.aspx" ToolTip="<%$ Resources:BackButtonText %>" Text="<%$ Resources:BackButtonText %>" /></td>
                                <td><div class="actionbarDivider"></div></td>
                                <td class="button"><asp:LinkButton ID="btnAddNew" runat="server" Text="<%$Resources:AddButtonText %>" CssClass="primary scheduleEventButton" Visible="true" /></td>
                                <td><asp:LinkButton ID="btnDelete" runat="server" Text="<%$Resources:DeleteCloudEventsButtonText %>" OnClientClick="openDialog(); return false;" CssClass="primary deleteEventsButton" Visible="false" /></td>
                                <td><asp:LinkButton ID="uxDEventsButton" runat="server" Text="X" ToolTip="<%$ Resources:DeleteCloudEventsButtonText %>" OnClick="uxDEventsButton_Click" CssClass="deleteEventsButton" Visible="false" /></td>
                                <td><div class="actionbarDivider"></div></td>
                                <td><asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" /></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="ektronPageContainer">
                    <%--GridView control will use EktronUI GridView when the control is available--%>
                    <asp:GridView ID="aspViewScheduledCloudEvents" AllowSorting="false" AllowPaging="false"  AutoGenerateColumns="false" runat="server" CssClass="ektron-view-scheduled-cloud-events" SortedAscendingHeaderStyle-CssClass="ektron-sort-ascending" SortedDescendingHeaderStyle-CssClass="ektron-sort-descending" SelectedRowStyle-CssClass="ektron-selected-row" >
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources:HeaderTextDelete %>" SortExpression="Delete" HeaderStyle-CssClass="column-delete">
                                <ItemTemplate>
                                    <span style="display:none">|<%# Eval("Id")%>|</span>
                                    <asp:CheckBox runat="server" ID="cbDelete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="<%$ Resources:HeaderTextDescription %>" DataField="name" SortExpression="name" ReadOnly="true" HeaderStyle-CssClass="column-description" Visible="false" />
                            <asp:TemplateField HeaderText="<%$ Resources:HeaderTextDescription %>" SortExpression="Name" HeaderStyle-CssClass="column-description">
                                <ItemTemplate>
                                 <a href="<%# String.Format("AddEditCloudScheduleEventItem.aspx?profile-id={0}&guid={1}",this.Request["profile-id"],Eval("Id")) %>" >
                                     <%# Eval("name") %>
                                 </a>        
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="<%$ Resources:HeaderTextDateAndTime %>" DataField="date" SortExpression="date" ReadOnly="true" HeaderStyle-CssClass="column-date" /><%--defaultsort--%>
                            <asp:TemplateField HeaderText="<%$ Resources:HeaderTextStatus %>" SortExpression="DeploymentStatus" HeaderStyle-CssClass="column-status" >
                                <ItemTemplate>
                                    <%# Eval("DeploymentStatus") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:HeaderTextEventType %>" SortExpression="operation" HeaderStyle-CssClass="column-event-type" >
                                <ItemTemplate>
                                    <%# Ektron.Workarea.Cloud.Deployment.ScheduledEvents.EnumToString.GetStringValue((Ektron.Cloud.Common.CloudCommandType)Eval("operation"))%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="<%$ Resources:HeaderTextWebInstances %>" DataField="instance" SortExpression="instance" ReadOnly="true" HeaderStyle-CssClass="column-web-instances" ItemStyle-CssClass="column-web-instances" />
                            <asp:BoundField HeaderText="<%$ Resources:HeaderTextEmailNotifications %>" DataField="NotificationEmail" SortExpression="NotificationEmail" ReadOnly="true" HeaderStyle-CssClass="column-email-notifications" />
                        </Columns>
                        <RowStyle CssClass="ektron-ui-odd" />
                        <AlternatingRowStyle CssClass="ektron-ui-even" />
                        <EmptyDataTemplate>
                            <p>
                                <asp:Literal ID="aspEmptyDataTemplateText" runat="server" Text="<%$ Resources:EmptyDataTemplateText %>" />
                            </p>
                        </EmptyDataTemplate>

                          <%--<PagerStyle ForeColor="Black" HorizontalAlign="Right" BackColor="#C6C3C6"></PagerStyle>--%>
                        <PagerTemplate>
                        <table style=" width:100%">
                        <tr>
                        <td style="width:20%; text-align:left;">
                            <%--<asp:LinkButton CommandName="Page" CommandArgument="First" ID="LinkButton1" runat="server"  Text="« First" />--%>
                            <asp:ImageButton  CommandName="Page" CommandArgument="First" ID="imgBtn1" runat="server" ImageUrl="~/WorkArea/images/ui/icons/arrowheadFirst.png" CssClass="pagerIcon"/>
                            <asp:ImageButton  CommandName="Page" CommandArgument="Prev" ID="ImageButton1" runat="server" ImageUrl="/WorkArea/images/ui/icons/arrowheadLeft.png" CssClass="pagerIcon" />
                            <asp:ImageButton  CommandName="Page" CommandArgument="Next" ID="ImageButton2" runat="server" ImageUrl="~/WorkArea/images/ui/icons/arrowheadRight.png" CssClass="pagerIcon"/>
                            <asp:ImageButton  CommandName="Page" CommandArgument="Last" ID="ImageButton3" runat="server" ImageUrl="~/WorkArea/images/ui/icons/arrowheadLast.png" CssClass="pagerIcon" />
                            </td>
                            <td style="width:80%; text-align:right;">
                            Go To Page <asp:TextBox runat="server" ID="tbGotoPage" Width="15px" Text="<%# aspViewScheduledCloudEvents.PageIndex+1 %>" /> of | <%= aspViewScheduledCloudEvents.PageCount %>
                            <asp:LinkButton CommandName="GotoPage" runat="server" ID="btnGotoPage" Text="Go" />
                            </td>
                            </tr>
                            </table>

                        </PagerTemplate>
                    </asp:GridView>

                    <ektronUI:Dialog ID="uxDialog" runat="server" AutoOpen="true" Visible="false" Draggable="true" Modal="true" Resizable="false" Title="<%$ Resources:DialogTitle %>">
                        <Buttons>
                            <%--<ektronUI:DialogButton ID="uxDeleteButton" runat="server" Text="<%$ Resources:DeleteText %>" Visible="false" OnCommand="Delete" />--%>
                            <ektronUI:DialogButton ID="uxCancelButton" runat="server" CloseDialog="true" Text="<%$ Resources:CloseDialogButtonText %>" />
                        </Buttons>
                        <ContentTemplate>
                            <ektronUI:Message ID="uxDialogMessage" runat="server" DisplayMode="Error" />
                        </ContentTemplate>
                    </ektronUI:Dialog>

                    <%--Delete Item Confirmation Dialog--%>
                    <ektronUI:Dialog runat="server" ID="diagDeleteConfirm" Modal="true" Title="<%$Resources:DeleteItemConfirmationDialogTitle %>" Width="576" >
                        <Buttons>
                            <ektronUI:DialogButton ID="uxDeleteButton" runat="server" OnClick="btnDelete_Click" Text="<%$Resources:DeleteText %>"></ektronUI:DialogButton>
                            <ektronUI:DialogButton ID="uxCancelDeletionButton" runat="server" CloseDialog="true" Text="<%$Resources:CancelButtonText %>"></ektronUI:DialogButton>
                        </Buttons>
                        <ContentTemplate>
                            <div class="dialog-confirmation-message">
                                <asp:Literal ID="aspDeleteItemConfirmationMessage" runat="server" Text="<%$Resources:DeleteItemConfirmationMessageText %>" />
                            </div>
                        </ContentTemplate>
                    </ektronUI:Dialog>
                </div>
            </div>
        </form>
    </body>
</html>
