﻿namespace Ektron.Workarea.Cloud
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Script.Serialization;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cloud.Amazon.Deployment.EC2;
    using Ektron.Cloud.Amazon.Deployment.RDS;
    using Ektron.Cloud.Amazon.Storage;
    using Ektron.Cms;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Sync.Client;
    using Ektron.Cms.Sync.Presenters;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core.Validation;
    using System.Text.RegularExpressions;
    using Ektron.Cloud.Amazon.Deployment;

    /// <summary>
    /// Class that holds values of the fields in the wizard so that it can be serialized in 
    /// stored in ViewState as user progresses in the process of adding hosted service.
    /// </summary>
    [Serializable()]
    class ViewStateData
    {
        // Step 1:
        public string CloudSiteName { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string HostedSvcLocation { get; set; }
        public string WebInstances { get; set; }
        public string AutoScalingWebInstancesMaxCount { get; set; }
        public string WebInstanceType { get; set; }
        public string SupportOSInstance { get; set; }

        // Step 2:
        public string BucketName { get; set; }
        public string KeyPairName { get; set; }
        public string KeyPairTextBoxVal { get; set; }
        public string SGSettings { get; set; }
        public bool CreateKeyPair = false;
        public bool CreateSecurityGroup { get; set; }
        public string LicenseKey { get; set; }
        public bool isItNewKeyPair = false;
        public bool enableCDN { get; set; }

        //Step 3:
        public string DatabaseEndPoint { get; set; }
        public string DatabaseName { get; set; }
        public string DBServer { get; set; }
        public string SQLUsername { get; set; }
        public string SQLPassword { get; set; }
        public string DBInstanceType { get; set; }
        public string DBInstanceClass { get; set; }
        public string ServerNamePrefix { get; set; }
        public int StorageSize { get; set; }
        public bool CreateServer { get; set; }
        public string Identifier { get; set; }
        public bool isNewDB = false;

        // Step 4:
        public long ProfileID { get; set; }
    }

    public partial class AmazonDeploymentWizard : System.Web.UI.UserControl
    {
        #region Variables
        public StyleHelper styleHelper;
        protected ContentAPI contAPI = new ContentAPI();
        private string _ApplicationPath;
        Dictionary<string, string> amazonJsonData = new Dictionary<string, string>();
        private JavaScriptSerializer jsonSerializer;
        List<string> bindVal = new List<string>();
        private bool isNewKeyPair = false;
        private bool isNewDB = false;
        protected List<DBInstance> instances;
        SettingsData settings_data;
        SiteAPI m_refSiteApi = new SiteAPI();
        private static readonly char[] SpecialChars = "!@#$%^&*()<>?/{}[]|\\,:;\"".ToCharArray();
        private static readonly char[] SpecialCharsCloudSiteName = "!_@#$%^&*()<>?/{}[]|\\,:;\"".ToCharArray();
        private static readonly string RegexDoubleByte = "[^\\u0000-\\u00ff]";
        private static readonly string RegexStartsWithNumber = "^\\d+";
        private static readonly string RegexHasEmptySpace = "\\s+";

        private string accessKeyVal { get; set; }
        private string secretKeyVal { get; set; }
        private string bucketNameVal { get; set; }
        private string keyPairNameVal { get; set; }
        private string securityGroupNameVal { get; set; }
        private bool validate { get; set; }
        private string newKeyPair { get; set; }

        private ViewStateData formData { get; set; }
        #endregion

        #region Properties and Construction
        /// <summary>
        /// Constructor
        /// </summary>  
        public AmazonDeploymentWizard()
        {
            contAPI = new ContentAPI();
            styleHelper = new StyleHelper();
            this.jsonSerializer = new JavaScriptSerializer();
        }
        /// <summary>
        /// WorkArea path
        /// </summary>
        private string ApplicationPath
        {
            get
            {
                return contAPI.ApplicationPath;
            }
        }
        #endregion

        #region Page Events

        protected override void OnInit(EventArgs e)
        {
            PopulateHelpButtons();
            RegisterResources();
            settings_data = m_refSiteApi.GetSiteVariables(m_refSiteApi.UserId, true);
            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            encodedVal.Value = System.Configuration.ConfigurationManager.AppSettings["encodedValue"];
        }
        #endregion

        #region Button Events

        /// <summary>
        /// Step 1 next click. Validated the AWS Key, AWS Secret key 
        /// to move forward to step 2 of the wizard.
        /// </summary
        protected void uxStep1Next_Click(object sender, EventArgs e)
        {
            this.accessKeyVal = uxAWSKeys.Text;
            this.secretKeyVal = uxAWSSecretKey.Text;
            this.validate = false;

            try
            {
                SubscriptionValidator subscriptionValidator = new SubscriptionValidator(accessKeyVal, secretKeyVal);
                if (subscriptionValidator.Validate().IsValid)
                {
                    if ((ViewStateData)ViewState["formData"] == null)
                    {
                        ViewState.Add("formData", new ViewStateData()
                        {
                            CloudSiteName = uxSiteName.Text,
                            AccessKey = this.accessKeyVal,
                            SecretKey = this.secretKeyVal,
                            HostedSvcLocation = ddlHostSvc.SelectedValue,
                            WebInstances = uxInstanceCount.Value.ToString(),
                            AutoScalingWebInstancesMaxCount = uxAutoScaling.Checked ? uxAutoScalingMaxInstances.Value.ToString() : "0",
                            WebInstanceType = ddlWebVmSize.SelectedValue
                        });
                    }

                    this.formData = (ViewStateData)ViewState["formData"];

                    uxBucketName.Value = uxSiteName.Text.ToLower();
                    uxEktLicKey.Text = settings_data.LicenseKey;

                    this.formData.LicenseKey = settings_data.LicenseKey;

                    Compute c = new Compute(accessKeyVal, secretKeyVal, this.formData.HostedSvcLocation);
                    List<KeyPair> list = c.GetKeyPair();

                    bindVal.Add(GetLocalResourceObject("SelectKeyPair").ToString());

                    list.ForEach(delegate(KeyPair keyPair)
                    {
                        bindVal.Add(keyPair.KeyName);
                    });
                    bindVal.Add(GetLocalResourceObject("CreateNewKeyPair").ToString());
                    ddlKeyPair.DataSource = bindVal;
                    ddlKeyPair.DataBind();

                    List<SecurityGroup> sg = c.GetSecurityGroups();
                    bindVal = new List<string>();
                    bindVal.Add(GetLocalResourceObject("SelectSGSetting").ToString());
                    sg.ForEach(delegate(SecurityGroup sgVal)
                    {
                        bindVal.Add(sgVal.GroupName);
                    });
                    bindVal.Add(GetLocalResourceObject("CreateNewSecurityGroup").ToString());
                    ddlSecurityGroup.DataSource = bindVal;
                    ddlSecurityGroup.DataBind();

                    ViewState.Add("formData", this.formData);

                    pnlStep1.Attributes.Add("style", "display:none");
                    pnlStep3.Attributes.Add("style", "display:none");
                    pnlStep4.Attributes.Add("style", "display:none");
                    pnlStep5.Attributes.Add("style", "display:none");
                    pnlStep2.Attributes.Add("style", "display:block");

                    // Display the values if the navigation is from step 3 to step 2 to step 1 to step 2

                    if (this.formData.KeyPairName == "Create New Key Pair")
                    {
                        uxNewKeyPair.Text = this.formData.KeyPairTextBoxVal;
                        dvNewKeyPair.Attributes.Add("style", "display:block");
                    }
                    else
                    {
                        uxNewKeyPair.Text = string.Empty;
                        dvNewKeyPair.Attributes.Add("style", "display:none");
                    }

                    ddlSecurityGroup.SelectedValue = this.formData.SGSettings;
                    ddlKeyPair.SelectedValue = this.formData.KeyPairName;

                    pnlStep2.Visible = true;
                }
                else
                {
                    Ektron.Cms.Framework.UI.Controls.EktronUI.Message message = new Cms.Framework.UI.Controls.EktronUI.Message();
                    message.Text = subscriptionValidator.Validate().Message;
                    phErrorMessage.Controls.Add(message);

                    pnlStep1.Attributes.Add("style", "display:block;");
                    pnlStep2.Attributes.Add("style", "display:none;");
                }
            }
            catch (Exception ex)
            {
                Ektron.Cms.Framework.UI.Controls.EktronUI.Message message = new Cms.Framework.UI.Controls.EktronUI.Message();
                message.Text = ex.Message.ToString();
                phErrorMessage.Controls.Add(message);
            }
        }

        /// <summary>
        /// Go back to step 1 where user can modify the access key, 
        /// access secret key, cloud site name, hosted service location etc...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxStep2Previous_Click(object sender, EventArgs e)
        {
            this.formData = (ViewStateData)ViewState["formData"];
            this.formData.SGSettings = ddlSecurityGroup.SelectedValue;
            this.formData.KeyPairName = ddlKeyPair.SelectedValue;
            if (this.uxAutoScaling.Checked)
            {
                this.divAutoScaling.Attributes.Add("style", "display:block");
            }
            else
            {
                this.divAutoScaling.Attributes.Add("style", "display:none");
            }

            if (ddlKeyPair.SelectedValue == "Create New Key Pair")
            {
                this.formData.KeyPairTextBoxVal = uxNewKeyPair.Text;
            }
            else this.formData.KeyPairTextBoxVal = string.Empty;

            pnlStep2.Attributes.Add("style", "display:none");
            pnlStep2.Visible = false;

            pnlStep1.Attributes.Add("style", "display:block");
            pnlStep1.Visible = true;

            pnlStep3.Attributes.Add("style", "display:none");
            pnlStep3.Visible = false;
        }

        /// <summary>
        /// Move forward in the wizard once bucket name and keypair value is entered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxStep2Next_Click(object sender, EventArgs e)
        {
            this.formData = (ViewStateData)ViewState["formData"];

            this.formData.BucketName = uxBucketName.Text.ToLower();
            this.formData.LicenseKey = uxEktLicKey.Text.Trim() != string.Empty ? uxEktLicKey.Text : settings_data.LicenseKey;
            this.formData.enableCDN = uxEnableCDN.Checked;
            S3Validator s3Validator = new S3Validator(uxBucketName.Text.ToLower(), formData.AccessKey, formData.SecretKey);
            List<Ektron.Cms.Framework.UI.Controls.EktronUI.Message> messageList = new List<Ektron.Cms.Framework.UI.Controls.EktronUI.Message>();
            Ektron.Cms.Framework.UI.Controls.EktronUI.Message message = null;
            bool error = false;
            if (s3Validator.Validate().IsValid && ddlKeyPair.SelectedValue != "Select Key Pair")
            {
                formData.SGSettings = ddlSecurityGroup.SelectedValue;
                if (ddlKeyPair.SelectedValue == "Create New Key Pair")
                {
                    this.isNewKeyPair = true;
                    this.formData.KeyPairName = uxNewKeyPair.Text;
                    this.formData.KeyPairTextBoxVal = uxNewKeyPair.Text;
                    this.formData.CreateKeyPair = true;
                    this.formData.isItNewKeyPair = true;
                    Ektron.Cloud.Amazon.Deployment.EC2.KeyPairValidator kpValid = new KeyPairValidator(formData.AccessKey, formData.SecretKey, uxNewKeyPair.Text, formData.HostedSvcLocation);
                    if (!kpValid.Validate().IsValid)
                    {
                        message = new Ektron.Cms.Framework.UI.Controls.EktronUI.Message();
                        message.ID = "errMsgNewKeyValPair";
                        message.Text = kpValid.Validate().Message;
                        messageList.Add(message);
                        error = true;
                        this.dvNewKeyPair.Attributes.Add("style", "display:block");
                    }
                }
                else
                {
                    this.formData.isItNewKeyPair = false;
                    this.formData.CreateKeyPair = false;
                    this.formData.KeyPairName = ddlKeyPair.SelectedValue;
                }

                if (ddlSecurityGroup.SelectedValue == "Create New Security Group using Existing Defaults")
                {
                    this.formData.CreateSecurityGroup = true;
                }
                else
                {
                    this.formData.CreateSecurityGroup = false;
                    this.formData.SGSettings = ddlSecurityGroup.SelectedValue;
                }
                Database db = new Database(formData.AccessKey, formData.SecretKey, formData.HostedSvcLocation);
                this.instances = db.GetDBInstances();

                this.bindVal = new List<string>();
                bindVal.Add(GetLocalResourceObject("SelectDServer").ToString());

                uxDBName.Value = string.IsNullOrEmpty(formData.DatabaseName) ? formData.CloudSiteName : formData.DatabaseName;

                instances.ForEach(delegate(DBInstance dbInstance)
                {
                    this.bindVal.Add(dbInstance.DBInstanceIdentifier);
                });
                this.bindVal.Add(GetLocalResourceObject("CreateNewDBServer").ToString());

                ViewState.Add("formData", this.formData);

                ddlDBServerName.DataSource = this.bindVal;
                ddlDBServerName.DataBind();

                if (!String.IsNullOrEmpty(this.formData.DBServer)) ddlDBServerName.SelectedValue = this.formData.DBServer;
                if (ddlDBServerName.SelectedIndex == ddlDBServerName.Items.Count - 1)
                {
                    newSetupItemDB.Attributes.Add("style", "display:block");
                    pnlDBInstanceType.Attributes.Add("style", "display:block");
                }
                else
                {
                    newSetupItemDB.Attributes.Add("style", "display:none");
                    pnlDBInstanceType.Attributes.Add("style", "display:none");
                }

                if (!String.IsNullOrEmpty(this.formData.DBInstanceType)) ddlDBInstanceType.SelectedValue = this.formData.DBInstanceType;
                switch (ddlDBInstanceType.SelectedIndex)
                {
                    case 0:
                    case 1:
                        ddlDBInstanceClass.Attributes.Add("disabled", "disabled");
                        break;
                    case 2:
                    case 3:
                        ddlDBInstanceClass.Attributes.Remove("disabled");
                        break;
                }
            }
            else
            {
                message = new Ektron.Cms.Framework.UI.Controls.EktronUI.Message();
                message.ID = "errMsgSelKeyValPair";
                message.Text = ddlKeyPair.SelectedValue == "Select Key Pair" ? "Please select Keypair" : s3Validator.Validate().Message;
                messageList.Add(message);
                error = true;
            }

            if (ddlSecurityGroup.SelectedValue == "Select Security Group")
            {
                message = new Ektron.Cms.Framework.UI.Controls.EktronUI.Message();
                message.ID = "errMsgSelSecGrp";
                message.Text = "Please select Security Group";
                messageList.Add(message);
                error = true;
            }
            if (error)
            {
                foreach (Ektron.Cms.Framework.UI.Controls.EktronUI.Message msg in messageList)
                {
                    phErrorMessage2.Controls.Add(msg);
                }
            }
            else
            {
                pnlStep2.Attributes.Add("style", "display:none");
                pnlStep3.Attributes.Add("style", "display:block");
                pnlStep3.Visible = true;
            }
        }

        /// <summary>
        /// Step 3 previous to go back to specify bucket name, key pair value.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxStep3Previous_Click(object sender, EventArgs e)
        {
            this.formData = (ViewStateData)ViewState["formData"];
            this.formData.DatabaseName = uxDBName.Text;
            this.formData.DBServer = ddlDBServerName.SelectedValue;
            this.formData.DBInstanceType = ddlDBInstanceType.SelectedValue;
            if (this.formData.isItNewKeyPair)
            {
                uxNewKeyPair.Text = this.formData.KeyPairTextBoxVal;
                dvNewKeyPair.Attributes.Add("style", "display:block");
            }
            else
            {
                uxNewKeyPair.Text = string.Empty;
                dvNewKeyPair.Attributes.Add("style", "display:none");
            }

            pnlStep3.Attributes.Add("style", "display:none");
            pnlStep3.Visible = false;

            pnlStep2.Attributes.Add("style", "display:block");
            pnlStep2.Visible = true;

        }

        /// <summary>
        /// Step 3 of wizard, select or create new database for service.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxStep3Next_Click(object sender, EventArgs e)
        {
            DBInstance selectedDBInstance = new DBInstance();

            this.formData = (ViewStateData)ViewState["formData"];

            if (ddlDBServerName.SelectedValue.ToLower() != "select a database server" && ddlDBServerName.SelectedValue.ToLower() != "create new database server")
            {
                Database db = new Database(formData.AccessKey, formData.SecretKey, formData.HostedSvcLocation);
                this.instances = db.GetDBInstances();
                selectedDBInstance = instances.Find(x => x.DBInstanceIdentifier == ddlDBServerName.SelectedValue);
                if (selectedDBInstance != null && selectedDBInstance.MasterUsername == uxSQLServerUsername.Text)
                {
                    DBNameValidator dbnv = new DBNameValidator(uxDBName.Text, uxSQLServerPassword.Text, selectedDBInstance);
                    if (dbnv.Validate().IsValid)
                    {
                        this.formData.DatabaseName = uxDBName.Text;
                        this.formData.DBServer = selectedDBInstance.DBInstanceIdentifier;
                        this.formData.SQLUsername = selectedDBInstance.MasterUsername;
                        this.formData.SQLPassword = uxSQLServerPassword.Text;
                        this.formData.Identifier = ddlDBServerName.SelectedValue;
                        this.formData.DatabaseEndPoint = selectedDBInstance.Endpoint.Address;
                        pnlStep3.Attributes.Add("style", "display:none");
                        pnlStep4.Attributes.Add("style", "display:block");
                        pnlStep4.Visible = true;
                    }
                    else
                    {
                        Ektron.Cms.Framework.UI.Controls.EktronUI.Message message = new Cms.Framework.UI.Controls.EktronUI.Message();
                        message.Text = dbnv.Validate().Message.ToLower().Contains("login failed for user") ? "Incorrect Password" : dbnv.Validate().Message;
                        phErrorMessage3.Controls.Add(message);
                    }
                }
                else
                {
                    Ektron.Cms.Framework.UI.Controls.EktronUI.Message message = new Cms.Framework.UI.Controls.EktronUI.Message();
                    message.Text = GetLocalResourceObject("UsernameIncorrect").ToString();
                    phErrorMessage3.Controls.Add(message);
                }
            }
            else if (ddlDBServerName.SelectedValue.ToLower() == "create new database server")
            {
                DBServerValidator serverValidator = new DBServerValidator(formData.AccessKey, formData.SecretKey, uxServerNamePrefix.Text, formData.HostedSvcLocation);
                if (serverValidator.Validate().IsValid)
                {
                    this.formData.CreateServer = true;
                    this.formData.DatabaseName = uxDBName.Text;
                    this.formData.DBInstanceType = ddlDBInstanceType.SelectedValue;
                    this.formData.SQLUsername = uxSQLServerUsername.Text;
                    this.formData.SQLPassword = uxSQLServerPassword.Text;
                    this.formData.DBInstanceClass = ddlDBInstanceClass.SelectedValue;
                    this.formData.ServerNamePrefix = uxServerNamePrefix.Text;
                    ////this.formData.StorageSize = uxStorageSize.Value;
                    this.formData.StorageSize = Convert.ToInt32(uxStorageSize.Value);
                    this.formData.isNewDB = true;
                    pnlStep3.Attributes.Add("style", "display:none");
                    pnlStep4.Attributes.Add("style", "display:block");
                    pnlStep4.Visible = true;
                }
                else
                {
                    Ektron.Cms.Framework.UI.Controls.EktronUI.Message message = new Cms.Framework.UI.Controls.EktronUI.Message();
                    message.Text = serverValidator.Validate().Message;
                    phErrorMessage3.Controls.Add(message);
                }
            }
            else
            {
                Ektron.Cms.Framework.UI.Controls.EktronUI.Message message = new Cms.Framework.UI.Controls.EktronUI.Message();
                message.Text = GetLocalResourceObject("selectDBServer").ToString();
                phErrorMessage3.Controls.Add(message);
            }
        }


        /// <summary>
        /// Deploy site once the user is through the wizard and hosted service is saved.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxDeploySite_Click(object sender, EventArgs e)
        {
            pnlStep5.Visible = false;
            pnlStep5.Attributes.Add("style", "display:block");
            Ektron.Cloud.Amazon.Deployment.ServiceArguments args = new Ektron.Cloud.Amazon.Deployment.ServiceArguments();
        }

        /// <summary>
        /// Display the add amazon hosted service wizard if the encoded value key 
        /// in the web.config exists.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxSelectHostedServiceNext_Click(object sender, EventArgs e)
        {
            string connectionString;
            connectionString = System.Configuration.ConfigurationManager.AppSettings["encodedValue"];
            if (String.IsNullOrEmpty(connectionString))
            {
                uxAmazonCert.Visible = true;
                uxServiceSelection.AutoOpen = true;
            }
            else
            {
                uxServiceSelection.AutoOpen = false;
                amazonWizard.AutoOpen = true;
            }
        }

        /// <summary>
        /// Save Amazon Hosted Service button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxAmazonSave_Click(object sender, EventArgs e)
        {
            this.formData = (ViewStateData)ViewState["formData"];

            Ektron.Cloud.Amazon.Deployment.ServiceArguments args = new Ektron.Cloud.Amazon.Deployment.ServiceArguments();
            args.AccessKey = this.formData.AccessKey;
            args.SecretKey = this.formData.SecretKey;

            args.ComputeArguments = new ComputeArguments();
            args.ComputeArguments.Name = this.formData.CloudSiteName;
            args.ComputeArguments.Location = this.formData.HostedSvcLocation;
            args.ComputeArguments.InstanceCount = Convert.ToInt32(this.formData.WebInstances);
            args.ComputeArguments.AutoScalingInstanceMaxCount = Convert.ToInt32(this.formData.AutoScalingWebInstancesMaxCount);
            args.ComputeArguments.IntanceType = this.formData.WebInstanceType;
            args.ComputeArguments.InstanceOS = this.formData.SupportOSInstance;
            args.ComputeArguments.LicenseKey = this.formData.LicenseKey;

            args.ComputeArguments.SecurityGroup = new SecurityGroup();
            args.ComputeArguments.CreateSecurityGroup = this.formData.CreateSecurityGroup;

            if (this.formData.CreateSecurityGroup == false)
                args.ComputeArguments.SecurityGroup.GroupName = this.formData.SGSettings;

            args.ComputeArguments.KeyPair = new KeyPair();
            args.ComputeArguments.CreateKeyPair = this.formData.CreateKeyPair;
            args.ComputeArguments.KeyPair.KeyName = this.formData.KeyPairName;
            args.ComputeArguments.LicenseKey = this.formData.LicenseKey;
            args.LocalSitePath = @System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath.TrimEnd('\\');
            args.DatabaseArguments = new DatabaseArguments();

            args.DatabaseArguments.CreateServer = this.formData.CreateServer;
            args.DatabaseArguments.DatabaseName = this.formData.DatabaseName;
            args.DatabaseArguments.MasterUsername = this.formData.SQLUsername;
            args.DatabaseArguments.MasterUserPassword = this.formData.SQLPassword;
            args.DatabaseArguments.Port = 1433;
            if (this.formData.CreateServer)
            {
                args.DatabaseArguments.Engine = this.formData.DBInstanceType;
                args.DatabaseArguments.EngineVersion = "10.50.2789.0.v1";
                args.DatabaseArguments.DBInstanceClass = this.formData.DBInstanceClass;
                args.DatabaseArguments.AllocatedStorage = Convert.ToInt16(this.formData.StorageSize);
                args.DatabaseArguments.DBInstanceIdentifier = this.formData.ServerNamePrefix;
            }
            else
            {
                args.DatabaseArguments.DBInstanceIdentifier = this.formData.Identifier;
                args.DatabaseArguments.EndPoint = this.formData.DatabaseEndPoint;
            }

            // CDN properties
            args.CDNArguments = new CDNArguments();
            args.CDNArguments.EnableCDN = this.formData.enableCDN;
            args.CDNArguments.CreateCloudFrontDistribution = args.CDNArguments.EnableCDN;
            args.CDNArguments.CDNURL = "";

            args.StorageArguments = new StorageArguments();
            args.StorageArguments.BucketName = this.formData.BucketName;

            CommonApi _siteApi = new SiteAPI();
            SyncHandlerController.CreateRelationshipResult result;
            SyncHandlerController _controller = new SyncHandlerController();
            ConnectionInfo LocalConnection = new ConnectionInfo(_siteApi.RequestInformationRef.ConnectionString);
            Relationship relationship = _controller.CreateAmazonRelationship(LocalConnection.DatabaseName, LocalConnection.ServerName, _siteApi.RequestInformationRef.SitePath, -1, "", "", args.ComputeArguments.Name, args.StorageArguments.BucketName, "amazonaws.com", "", Ektron.FileSync.Common.SyncClientSyncDirection.Upload, out result);

            if (result != SyncHandlerController.CreateRelationshipResult.Success)
            {
                uxStep4ProgressMsg.Attributes.Add("style", "display:none");
                Ektron.Cms.Framework.UI.Controls.EktronUI.Message errorMsg = new Cms.Framework.UI.Controls.EktronUI.Message();
                errorMsg.Text = GetLocalResourceObject("uxStep4ErrorMsg").ToString();
                phStep4ErrorMessage.Controls.Add(errorMsg);
                uxStep4ProgressMsg.Visible = true;
                //throw new Exception("Unable to create relationship. Make sure Ektron Windows Service is running with secure certificate installed.");
            }
            else
            {
                args.RelationShipId = relationship.Id;
                ISyncDataProvider provider = DataProviderFactory.Create();
                //provider.SetExternalArgs(relationship.Id, Ektron.DbSync.Core.Serializer.Serialize<Ektron.Cloud.Amazon.Deployment.ServiceArguments>(args));
                Ektron.Cloud.Common.CloudServiceRequest request = new Ektron.Cloud.Common.CloudServiceRequest();
                request.SubscriptionID = string.Empty;
                request.StorageAccountID = args.AccessKey;
                request.StorageAccountKey = args.SecretKey;
                request.ContainerConfigName = "amazon";
                request.WebSites.Add(new Ektron.Cloud.Common.SiteInfo()
                {
                    ContainerAddress = args.StorageArguments.BucketName,
                    LocalSitePath = @System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath.TrimEnd('\\')
                });
                request.ServiceArguments = new Ektron.Cloud.Amazon.Deployment.ServiceArguments();
                request.ServiceArguments = args;
                provider.SetExternalArgs(relationship.Id, Ektron.DbSync.Core.Serializer.Serialize<Ektron.Cloud.Common.CloudServiceRequest>(request));

                Profile profile = relationship.AddProfile(true);
                profile.Name = "Template & Database - Upload";
                profile.SynchronizeDatabase = true;
                profile.SynchronizeWorkarea = false;
                profile.SynchronizeTemplates = true;
                profile.SynchronizeAssets = true;
                profile.SynchronizePrivateAssets = true;
                profile.SynchronizeUploadedFiles = true;
                profile.SynchronizeUploadedImages = true;
                profile.SynchronizeBinaries = true;
                profile.ConflictResolution = Ektron.FileSync.Common.CmsConflictResolutionPolicy.SourceWins;
                profile.Direction = Ektron.FileSync.Common.SyncClientSyncDirection.Upload;
                List<FileSync.Common.SyncDBScope> scope = new List<FileSync.Common.SyncDBScope>();
                scope.Add(FileSync.Common.SyncDBScope.ektron);
                profile.Scope = scope;
                profile.Save();
                provider.SetExternalArgs(profile.Id, Ektron.DbSync.Core.Serializer.Serialize<Ektron.Cloud.Common.CloudServiceRequest>(request));

                this.formData = (ViewStateData)ViewState["formData"];
                uxDeploySite.Attributes.Add("rel", relationship.Id.ToString());

                profileId.Value = relationship.Id.ToString();
                this.formData.ProfileID = relationship.Id;
                this.formData.LicenseKey = settings_data.LicenseKey;
                pnlStep4.Attributes.Add("style", "display:none");
                pnlStep5.Visible = true;
                pnlStep5.Attributes.Add("style", "display:block");
            }
        }

        #endregion

        #region UtilitiesFunctions
        /// <summary>
        /// Register JavaScript and CSS Resources
        /// </summary>
        protected void RegisterResources()
        {
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();

            Package resources = new Package()
            {
                Components = new List<Component>()
                {
                    Packages.jQuery.Plugins.InfieldLabels,
                    Ektron.Cms.Framework.UI.Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-dateField.css"),
                    JavaScript.Create(cmsContextService.WorkareaPath + "/sync/js/Ektron.Workarea.Sync.Relationships.js")
                }
            };
            resources.Register(this);
        }

        /// <summary>
        /// Populate Help Buttons for every text box in Amazon Service Dialogs.
        /// </summary>
        protected void PopulateHelpButtons()
        {
            ltrServiceSelectionHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_ServiceSelection", String.Empty);

            // Step 1: Help Buttons
            ltrStep1DescHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step1Desc", String.Empty);

            ltrSiteNameHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_SiteName", String.Empty);
            ltrAWSKeysHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_AWSKeys", String.Empty);
            ltrAWSSecretKeyHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_AWSSecretKey", String.Empty);
            ltrHostedSvcHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_HostedService", String.Empty);
            ltrWebInstancesHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_WebInstances", String.Empty);
            ltrWindowsOSHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_WindowsOS", String.Empty);

            // Step 2: Help buttons
            ltrStep2DescHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step2Desc", String.Empty);

            ltrBucketNameHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_BucketName", String.Empty);
            ltrEktLicKeyHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_EktLicense", String.Empty);
            ltrKeyPairHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_KeyPair", String.Empty);
            ltrDdlNewKeyPairHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_KeyPairDDL", String.Empty);

            // Step 3: Help Buttons

            ltrStep3DescHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step3Desc", String.Empty);

            ltrUXDBNameHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step3_DBName", String.Empty);
            ltrUXDBServerHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step3_DBServer", String.Empty);
            ltrUXSQLServerUsernameHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step3_UserName", String.Empty);
            ltrUXSQLServerPasswordHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step3_Password", String.Empty);

            ltrUXReenterSQLPasswordHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step3_ReenterPassword", String.Empty);
            ltrDdlDBInstanceTypeHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step3_DBInstanceType", String.Empty);
            ltrDddlDBInstanceClassHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step3_DBInstanceClass", String.Empty);
            ltrLblServerNamePrefixHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step3_ServerNamePrefix", String.Empty);
            ltrUXStorageSizeHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step3_StorageSize", String.Empty);

            // Step 4: Help Buttons
            ltrStep4DescHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step4Desc", String.Empty);

            // Step 5: Help Buttons
            ltrStep5DescHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Amazon_Step5Desc", String.Empty);
        }

        #endregion
    }
}