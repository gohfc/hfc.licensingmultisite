﻿<%@ WebHandler Language="C#" Class="CloudDeploymentHandler" %>
using System;
using System.Web;
using System.Collections.Specialized;
using Ektron.Cloud.Common;
using Ektron.Cms;
using Ektron.Cms.Sync.Presenters;
using Ektron.Cms.Sync.Client;
using System.Web.Script.Serialization;
using Ektron.FileSync.Common;
using System.Collections.Generic;
public class CloudDeploymentHandler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        ResponseData responseData = new ResponseData();
        responseData.ErrorMessage = String.Empty;
        responseData.Status = "Ok";

        context.Response.AppendHeader("Pragma", "no-cache");
        context.Response.AppendHeader("Cache-Control", "no-cache");

        context.Response.CacheControl = "no-cache";
        context.Response.Expires = -1;

        context.Response.ExpiresAbsolute = new DateTime(1900, 1, 1);
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

        try
        {
            context.Server.ScriptTimeout = 3600;
            CloudServiceRequest csrequest = null;
            long relationshipId = 0;
            if (!string.IsNullOrEmpty(context.Request.QueryString["relationship-id"]))
            {
                relationshipId = Convert.ToInt64(context.Request.QueryString["relationship-id"]);
            }
            if (relationshipId > 0)
            {
                csrequest = Ektron.Cloud.Common.Serializer.Deserialize<Ektron.Cloud.Common.CloudServiceRequest>(Relationship.GetExternalArgs(relationshipId));
                if (context.Request.QueryString["instance"] != null)
                {
                    if (context.Request.QueryString["service-type"] == ProfileType.Azure.ToString())
                    {
                        csrequest.InstanceCount = Convert.ToInt32(context.Request.QueryString["instance"]) == 0 ? 1 : Convert.ToInt32(context.Request.QueryString["instance"]);                                             
                    }
                    else
                    {
                        csrequest.InstanceCount = csrequest.ServiceArguments.ComputeArguments.InstanceCount = Convert.ToInt32(context.Request.QueryString["instance"]) == 0 ? 1 : Convert.ToInt32(context.Request.QueryString["instance"]);                     
                    }
                }
            }
            else
            {
                csrequest = Ektron.Cloud.Common.CloudServiceHelper.CreateCloudServiceRequest(context.Request.QueryString);
            }
            CloudCommandType commandType = (CloudCommandType)Enum.Parse(typeof(CloudCommandType), (string)context.Request.QueryString["command"], true);
            EktronServiceProxy proxy = new EktronServiceProxy();
            SyncProfileActions syncParams = null;
            switch (commandType)
            {
                case CloudCommandType.CreateDeployment:
                    responseData.ProfileId = CreateRelationship(csrequest);
                    break;
                case CloudCommandType.DeleteDeployment:
                    if (context.Request.QueryString["service-type"] == ProfileType.Azure.ToString())
                    {
                        if ((string)context.Request.QueryString["delwebworkerroles"] == "true")
                            csrequest.DeleteOptions = csrequest.DeleteOptions | DeleteOption.Service;
                        if ((string)context.Request.QueryString["delstoragedata"] == "true")
                            csrequest.DeleteOptions = csrequest.DeleteOptions | DeleteOption.Storage;
                        if ((string)context.Request.QueryString["delsqlazure"] == "true")
                            csrequest.DeleteOptions = csrequest.DeleteOptions | DeleteOption.Database;
                        syncParams = new CloudParamWrapper().GetParam(csrequest.RelationshipID);
                        syncParams.ExternalArgs = Ektron.Cloud.Common.Serializer.Serialize<Ektron.Cloud.Common.CloudServiceRequest>(csrequest);
                        proxy.RunAzureServiceCommand(syncParams, commandType);
                        DeleteRelationship(csrequest.RelationshipID);
                    }
                    else
                    {
                        if ((string)context.Request.QueryString["delrelationprofiles"] == "true")
                        {
                            csrequest.DeleteOptions = csrequest.DeleteOptions | DeleteOption.Service;
                            csrequest.DeleteOptions = csrequest.DeleteOptions | DeleteOption.Relation;
                        }
                        if ((string)context.Request.QueryString["deleteAmazonStorageData"] == "true")
                            csrequest.DeleteOptions = csrequest.DeleteOptions | DeleteOption.Storage;
                        if ((string)context.Request.QueryString["deleteSqlAmazon"] == "true")
                            csrequest.DeleteOptions = csrequest.DeleteOptions | DeleteOption.Database;
                        if ((string)context.Request.QueryString["deleteRDSAmazonDatabase"] == "true")
                            csrequest.DeleteOptions = csrequest.DeleteOptions | DeleteOption.DatabaseServer;

                        syncParams = new CloudParamWrapper().GetParam(csrequest.ServiceArguments.RelationShipId);
                        syncParams.ExternalArgs = Ektron.Cloud.Common.Serializer.Serialize<Ektron.Cloud.Common.CloudServiceRequest>(csrequest);
                        proxy.CloudDeployment(syncParams, CloudCommandType.DeleteDeployment, ProfileType.Amazon);
                        DeleteRelationship(csrequest.ServiceArguments.RelationShipId);
                    }
                    break;
                default:
                    if (context.Request.QueryString["service-type"] == ProfileType.Amazon.ToString())
                    {
                        syncParams = new CloudParamWrapper().GetParam(csrequest.ServiceArguments.RelationShipId);
                        syncParams.ExternalArgs = Ektron.Cloud.Common.Serializer.Serialize<Ektron.Cloud.Common.CloudServiceRequest>(csrequest);
                        switch (commandType)
                        {
                            case CloudCommandType.ChangeStatus:
                                proxy.CloudDeployment(syncParams, CloudCommandType.ChangeStatus, ProfileType.Amazon);
                                break;
                            case CloudCommandType.ChangeConfig:
                                proxy.CloudDeployment(syncParams, CloudCommandType.ChangeConfig, ProfileType.Amazon);
                                break;
                        }
                    }
                    else
                    {
                        syncParams = new CloudParamWrapper().GetParam(csrequest.RelationshipID);
                        syncParams.ExternalArgs = Ektron.Cloud.Common.Serializer.Serialize<Ektron.Cloud.Common.CloudServiceRequest>(csrequest);
                        proxy.RunAzureServiceCommand(syncParams, commandType);
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            responseData.ErrorMessage = ex.Message;
            responseData.Status = "Error";
        }

        context.Response.ContentType = "application/json";
        context.Response.Write(jsonSerializer.Serialize(responseData));
    }

    void DeleteRelationship(long relationshipId)
    {
        SyncHandlerController.ResultCode result;
        SyncHandlerController _controller = new SyncHandlerController();
        _controller.DeleteRelationship(relationshipId, out result);
        if (result != SyncHandlerController.ResultCode.Success)
        {
            throw new Exception("Unable to delete relationship.  Check your windows services running with secure certificate installed.");
        }
    }
    long CreateRelationship(CloudServiceRequest csrequest)
    {
        CommonApi _siteApi = new SiteAPI();
        SyncHandlerController.CreateRelationshipResult result;
        SyncHandlerController _controller = new SyncHandlerController();
        ConnectionInfo LocalConnection = new ConnectionInfo(_siteApi.RequestInformationRef.ConnectionString);

        Relationship relationship = _controller.CreateCloudRelationship(
            LocalConnection.DatabaseName, LocalConnection.ServerName, _siteApi.RequestInformationRef.SitePath, -1,
            csrequest.WebSites[0].ConnectionString, "", csrequest.SubscriptionID,
            csrequest.StorageAccountID, csrequest.StorageAccountKey,
            csrequest.CloudServiceName, "", Ektron.FileSync.Common.SyncClientSyncDirection.Upload, out result);
        if (result != SyncHandlerController.CreateRelationshipResult.Success)
        {
            throw new Exception("Unable to create relationship. Make sure Ektron Windows Service is running with secure certificate installed.");
        }
        csrequest.RelationshipID = relationship.Id;
        Relationship.SetExternalArgs(relationship.Id, Ektron.DbSync.Core.Serializer.Serialize<Ektron.Cloud.Common.CloudServiceRequest>(csrequest));
        return relationship.Id;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}

public class ResponseData
{
    private string status;
    public string Status
    {
        get
        {
            return this.status;
        }
        set
        {
            if ((string)value != "Ok" && (string)value != "Error")
            {
                throw (new ArgumentException("Status must be set to either \"Ok\" or \"Error\""));
            }
            this.status = value;
        }
    }
    public long ProfileId { get; set; }

    public string ErrorMessage { get; set; }
}