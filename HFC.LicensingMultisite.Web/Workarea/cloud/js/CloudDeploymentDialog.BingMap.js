﻿/*
* Ektron.Workarea.Cloud.BingMap
*
* Copyright 2011
*
* Depends:
*	Ektron.Namespace.js
*   jQuery
*/

Ektron.Namespace.Register("Ektron.Workarea.Cloud");

Ektron.Workarea.Cloud.BingMap = {
    // properties
    map: null,
    pins: [],
    pinClick: null, // stub to allow user to set a pin click callback
    displayInfobox: false, // indicates wether or not to display the Infobox for each pin on hover

    // classes
    InfoBox: function (title, description) {
        this.div;  // Container div element
        this.html = '<div class="ui-dialog ui-widget ui-widget-content ui-corner-all"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span class="ui-dialog-title">' + title + '</span></div><div id="dialog" class="ui-dialog-content ui-widget-content">' + description + '</div></div>';
    },

    Pin: function (latitude, longitude, title, description) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title; // the title that appears in the associated InfoBox
        this.description = description; // the description that appears in the associated InfoBox
    },

    // methods
    createPin: function (pin, pinText) {
        var pushPin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(pin.latitude, pin.longitude), {
            text: pinText
        });
        pushPin.title = pin.title;
        pushPin.description = pin.description;
        if (this.displayInfobox === true) {
            pushPin.setInfoBox(new InfoBox(pin.title, pin.description));
        }
        if (this.pinClick) {
            Microsoft.Maps.Events.addHandler(pushPin, 'click', this.pinClick);
        }
        // Add this pushpin to the map using global map variable
        this.map.entities.push(pushPin);
    },

    init: function (credentials) {
        // ensure that BingMaps API has loaded
        var bingMapLoaded = false;
        do {
            if (window.Microsoft) {
                this._getMap(credentials);
                bingMapLoaded = true;
            }
        }
        while (bingMapLoaded == false);

        // extend the Infobox's prototype
        if (this.displayInfobox) {
            this._extendInfobox();
        }
        this._extendPushpin();
    },

    mapMouseMove: function (e) {
        // get the HTML DOM Element that represents the Map
        var mapElem = Ektron.Workarea.Cloud.BingMap.map.getRootElement();
        if (e.targetType === "map") {
            // Mouse is over Map
        }
        else {
            // Mouse is over Pushpin, Polyline, Polygon
            mapElem.style.cursor = "pointer";
        }
    },

    // helpers
    _extendInfobox: function () {
        // extend Infobox prototype with show & hide methods
        Ektron.Workarea.Cloud.BingMap.InfoBox.prototype.show = function (e) {
            if (this.div == undefined) {
                // Create the container div.
                this.div = document.createElement("div");
                this.div.className = "infobox";
                this.div.innerHTML = this.html;
                var mapDiv = document.getElementById('mapDiv');
                mapDiv.appendChild(this.div);
            }

            // Calculate the pixel position of the pushpin relative to the map control
            var pinLocation = map.tryLocationToPixel(e.target.getLocation(), Microsoft.Maps.PixelReference.control);

            // Display the infobox at the correct pixel coordinates
            this.div.style.left = pinLocation.x + "px";
            this.div.style.top = pinLocation.y + "px";
            this.div.style.visibility = "visible";
        };

        Ektron.Workarea.Cloud.BingMap.InfoBox.prototype.hide = function (e) {
            if (this.div != undefined) {
                this.div.style.visibility = "hidden";
            }
        };
    },
    _extendPushpin: function () {
        // Extend the Pushpin class to add an InfoBox object
        Microsoft.Maps.Pushpin.prototype.setInfoBox = function (infoBox) {
            if (typeof this.infoBox != undefined && this.infoBox != undefined && this.infoBox != null) {
                this.removeInfoBox();
            }
            // Assign the infobox to this pushpin
            this.infoBox = infoBox;

            // Add handlers for mouse events
            this.mouseoverHandler = Microsoft.Maps.Events.addHandler(this, 'mouseover',
                    function (e) { infoBox.show(e); }
                );
            this.mouseoutHander = Microsoft.Maps.Events.addHandler(this, 'mouseout',
                    function (e) { infoBox.hide(e); }
                );
        };

        // Extend the Pushpin class to remove an existing InfoBox object
        Microsoft.Maps.Pushpin.prototype.removeInfoBox = function () {
            this.infoBox = null;

            // Remove handlers for mouse events
            Microsoft.Maps.Events.removeHandler(this.mouseoverHandler);
            Microsoft.Maps.Events.removeHandler(this.mouseoutHander);
        };

        Microsoft.Maps.Pushpin.prototype.title = null;
        Microsoft.Maps.Pushpin.prototype.description = null;
    },

    _getMap: function (key) {
        var mapCenter = new Microsoft.Maps.Location(37.09024, -95.71289100000001)
        this.map = new Microsoft.Maps.Map(document.getElementById("mapDiv"),
            {
                credentials: key,
                center: mapCenter,
                mapTypeId: Microsoft.Maps.MapTypeId.road,
                enableClickableLogo: false,
                enableSearchLogo: false,
                showCopyright: false,
                showDashboard: false,
                zoom: 1
            }
        );

        // add handler to alter the cursor
        Microsoft.Maps.Events.addHandler(this.map, "mousemove", this.mapMouseMove); // handles cursor changes

        // Create the pushpins on the maps from the pins array
        for (var pinIndex = 0; pinIndex < this.pins.length; pinIndex++) {
            this.createPin(this.pins[pinIndex], (pinIndex + 1) + "");
        }
    }
};
