/*
* Depends:
*   jQuery	
*   Ektron.Namespace.js
*   Ektron.String.js
*/

Ektron.Namespace.Register("Ektron.Workarea.Cloud");
Ektron.Workarea.Cloud = {
    handler: "",  //set in CloudDeploymentDialogs.ascx
    Ajax: {
        error: function (ui, errorMessage) {
            //$ektron(ui.progressImageSelector).fadeOut("fast");
            $ektron(ui.progressImageSelector).hide();
            $ektron(ui.errorMessageSelector).find(".ektron-ui-messageBody").text(errorMessage);
            //$ektron(ui.errorMessageSelector).slideDown("fast");
            $ektron(ui.errorMessageSelector).show();
            $ektron(ui.selector).closest(".ui-dialog").find(".ui-dialog-buttonpane button:first").hide();
            $ektron(ui.selector).closest(".ui-dialog").find(".ui-dialog-buttonpane button:last span").text(Ektron.Workarea.Cloud.Dialogs.closeText);
            $ektron(ui.selector).closest(".ui-dialog").find(".ui-dialog-buttonpane button:last").click(function () {
                window.location.reload();
            });
            //$ektron(ui.selector).closest(".ui-dialog").find(".ui-dialog-buttonpane").slideDown("fast");
            $ektron(ui.selector).closest(".ui-dialog").find(".ui-dialog-buttonpane").show();
        },
        execute: function (ui, xhrData) {
            var ui = ui;
            var xhrData = xhrData;
            // if we're going to display the status dialog, hide the current dialog
            if (ui.showStatus) {
                $ektron(ui.selector).closest(".ui-dialog").hide();
            }
            $ektron.ajax({
                url: Ektron.Workarea.Cloud.handler,
                data: xhrData,
                cache: false,
                dataType: "json",
                error: function (jqXHR, textStatus, errorThrown) {
                    Ektron.Workarea.Cloud.Ajax.error(ui, errorThrown);
                },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Status) {
                        case "Ok":
                            Ektron.Workarea.Cloud.Ajax.success(ui, xhrData);
                            break;
                        case "Error":
                            Ektron.Workarea.Cloud.Ajax.error(ui, data.ErrorMessage);
                            break;
                    }
                }
            });
        },
        success: function (ui, xhrData) {
            if (ui.showStatus) {
                Ektron.Workarea.Sync.Relationships.ShowCloudSyncStatus(ui, xhrData);
            }
            else {
                $ektron(ui.progressImageSelector).hide();
                $ektron(ui.successMessageSelector).show();
                $ektron(ui.selector).closest(".ui-dialog").find(".ui-dialog-buttonpane button:first").hide();
                $ektron(ui.selector).closest(".ui-dialog").find(".ui-dialog-buttonpane button:last span").text(Ektron.Workarea.Cloud.Dialogs.closeText);
                $ektron(ui.selector).closest(".ui-dialog").find(".ui-dialog-buttonpane button:last").click(function () {
                    window.location.reload();
                });
                $ektron(ui.selector).closest(".ui-dialog").find(".ui-dialog-buttonpane").show();
            }
        }
    },
    Dialogs: {
        closeText: "", //set in CloudDeploymentDialogs.ascx
        CloseModal: function () {
            location.reload(false);
        },
        ChangeStatus: {
            execute: function () {
                //set vars
                var selector = this.selector;
                var progressImageSelector = this.progressImageSelector;
                var errorMessageSelector = this.errorMessageSelector;
                var successMessageSelector = this.successMessageSelector;
                var confirmationMessageSelector = this.confirmationMessageSelector;

                //set ui
                $ektron(confirmationMessageSelector).fadeOut("fast", function () {
                    var ui = {
                        "selector": selector,
                        "progressImageSelector": progressImageSelector,
                        "successMessageSelector": successMessageSelector,
                        "errorMessageSelector": errorMessageSelector,
                        "showStatus": true
                    };

                    var xhrData = eval("(" + Ektron.Workarea.Cloud.Dialogs.ChangeStatus.data + ")");
                    xhrData.command = "ChangeStatus";

                    Ektron.Workarea.Cloud.Ajax.execute(ui, xhrData);
                });
            },
            data: "{}",
            progressImage: "", //set in CloudDeploymentDialogs.ascx
            selector: "", //set in CloudDeploymentDialogs.ascx
            errorMessageSelector: "", //set in CloudDeploymentDialogs.ascx
            confirmationMessageSelector: "", //set in CloudDeploymentDialogs.ascx
            successMessageSelector: "", //set in CloudDeploymentDialogs.ascx
            open: function (data) {
                this.data = data;
                $ektron(this.selector).dialog("open");
            }
        },
        ConfigureInstance: {
            execute: function () {
                //set vars
                var selector = this.selector;
                var progressImageSelector = this.progressImageSelector;
                var errorMessageSelector = this.errorMessageSelector;
                var successMessageSelector = this.successMessageSelector;
                var confirmationMessageSelector = this.confirmationMessageSelector;
                var instanceCountSelector = this.instanceCountSelector;
                var instanceSelector = this.instanceSelector;

                //set ui
                $ektron(instanceSelector).fadeOut("fast");
                $ektron(confirmationMessageSelector).fadeOut("fast", function () {
                    var ui = {
                        "selector": selector,
                        "progressImageSelector": progressImageSelector,
                        "successMessageSelector": successMessageSelector,
                        "errorMessageSelector": errorMessageSelector,
                        "showStatus": true
                    };

                    var xhrData = eval("(" + Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.data + ")");
                    xhrData.command = "ChangeConfig";
                    xhrData.instance = $ektron(instanceCountSelector).val();

                    Ektron.Workarea.Cloud.Ajax.execute(ui, xhrData);
                });
            },
            VerifyWebInstanceCount: function (value, element, errorMessage, contextInfo) {
                var isValid = true;
                if (value < 1) {
                    isValid = false;
                }
                return isValid;
            },
            data: "",
            progressImage: "", //set in CloudDeploymentDialogs.ascx
            selector: "", //set in CloudDeploymentDialogs.ascx
            errorMessage: "", //set in CloudDeploymentDialogs.ascx
            instanceSelector: "", //set in CloudDeploymentDialogs.ascx
            instanceCountSelector: "", //set in CloudDeploymentDialogs.ascx
            confirmationMessage: "", //set in CloudDeploymentDialogs.ascx
            successMessage: "", //set in CloudDeploymentDialogs.ascx
            open: function (data) {
                this.data = data;
                var deldata = eval("(" + this.data + ")");
                $ektron(this.selector).dialog("open");

                // Prepopulate with the current number of instances for the depolyment
                var numInstances = deldata["number-of-instances"];
                var instanceCountSelector = this.instanceCountSelector;
                $ektron(instanceCountSelector).val(numInstances);

                if (deldata["service-type"] == "Amazon") {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.amazoninstanceselector).css("display", "block");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.azureinstanceselector).css("display", "none");
                }
                else {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.azureinstanceselector).css("display", "block");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.amazoninstanceselector).css("display", "none");
                }
            }
        },
        DeleteRelationship: {
            close: function () {
                // return validation messages
                $ektron(this.validationMessageSelector).hide();
            },
            execute: function () {
                //set vars
                var selector = this.selector;
                var progressImageSelector = this.progressImageSelector;
                var errorMessageSelector = this.errorMessageSelector;
                var successMessageSelector = this.successMessageSelector;
                var confirmationMessageSelector = this.confirmationMessageSelector;
                var deleteSelector = this.deleteSelector;
                var deleteRelationAndProfiles = this.deleteRelationAndProfiles;
                var deleteRoles = this.deleteRoles;
                var deleteStorageData = this.deleteStorageData;
                var deleteAmazonStorageData = this.deleteAmazonStorageData;
                var deleteSqlAzure = this.deleteSqlAzure;
                var deleteSqlAmazon = this.deleteSqlAmazon;
                var deleteConfirmServiceName = this.deleteConfirmServiceName;
                var validationMessageSelector = this.validationMessageSelector;
                var deleteRDSAmazonDatabase = this.deleteRDSAmazonDatabase;

                var xhrData = eval("(" + Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.data + ")");
                var delServiceName = xhrData['service-name'];
                var delServiceType = xhrData['service-type'];

                var confirmDelServiceName = $ektron(deleteConfirmServiceName).val();
                if (delServiceName != confirmDelServiceName) {
                    $ektron(validationMessageSelector).slideDown("fast");
                    return false;
                }
                //set ui
                $ektron(deleteSelector).fadeOut("fast");
                $ektron(validationMessageSelector).fadeOut("fast");
                $ektron(confirmationMessageSelector).fadeOut("fast", function () {
                    var ui = {
                        "selector": selector,
                        "progressImageSelector": progressImageSelector,
                        "successMessageSelector": successMessageSelector,
                        "errorMessageSelector": errorMessageSelector,
                        "showStatus": true
                    };

                    xhrData.command = "DeleteDeployment";
                    xhrData.delrelationprofiles = $ektron(deleteRelationAndProfiles).is(':checked');

                    if (delServiceType == "Azure") {
                        xhrData.delwebworkerroles = $ektron(deleteRoles).is(':checked');
                        xhrData.delstoragedata = $ektron(deleteStorageData).is(':checked');
                        xhrData.delsqlazure = $ektron(deleteSqlAzure).is(':checked');
                    }
                    else {
                        xhrData.deleteAmazonStorageData = $ektron(deleteAmazonStorageData).is(':checked');
                        xhrData.deleteSqlAmazon = $ektron(deleteSqlAmazon).is(':checked');
                        xhrData.deleteRDSAmazonDatabase = $ektron(deleteRDSAmazonDatabase).is(':checked');
                    }
                    Ektron.Workarea.Cloud.Ajax.execute(ui, xhrData);
                });
            },
            data: "",
            progressImage: "", //set in CloudDeploymentDialogs.ascx
            selector: "", //set in CloudDeploymentDialogs.ascx
            errorMessage: "", //set in CloudDeploymentDialogs.ascx
            confirmationMessage: "", //set in CloudDeploymentDialogs.ascx
            successMessage: "", //set in CloudDeploymentDialogs.ascx
            open: function (data) {
                this.data = data;
                var deldata = eval("(" + this.data + ")");
                $ektron(this.selector).dialog("open");
                if (deldata["service-type"] == "Amazon") {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.amazondeleteselector).css("display", "block");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.azuredeleteselector).css("display", "none");
                }
                else {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.azuredeleteselector).css("display", "block");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.amazondeleteselector).css("display", "none");
                }
                $ektron(this.deleteServiceName).text(deldata['service-name']);
            }
        },
        Deployment: {
            selector: "", //set in CloudDeploymentDialogs.ascx
            progressHandler: "", //set in CloudDeploymentWizard.ascx
            errorMessageSelector: "", //set in CloudDeploymentWizard.ascx
            init: function () {
                this.reset();
                $ektron(".CreateHostedService").click(function () {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.ServiceSelection.selector).dialog('open');
                    $ektron(".ui-dialog-titlebar-close").bind("click", function () {
                        Ektron.Workarea.Cloud.Dialogs.CloseModal();
                    });
                });
            },
            cancel: function (reload) {
                this.reset();
                $ektron(this.selector).dialog("close");
                if (reload) {
                    window.location.reload();
                }
            },
            close: function () {
                this.cancel(true);
            },
            execute: function () {
                var json;

                if ("undefined" !== typeof ($ektron(Ektron.Workarea.Cloud.Dialogs.Deployment.selector + " iframe")[0].contentWindow.Ektron.Workarea.Cloud.Dialogs.Deployment.Create.data)) {
                    json = $ektron(Ektron.Workarea.Cloud.Dialogs.Deployment.selector + " iframe")[0].contentWindow.Ektron.Workarea.Cloud.Dialogs.Deployment.Create.data;
                    json = eval("(" + json + ")");
                }

                if ("undefined" !== typeof ($ektron(Ektron.Workarea.Cloud.Dialogs.Deployment.selector + " iframe")[0].contentWindow.Ektron.Workarea.Cloud.Dialogs.Deployment.Create.success)) {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.Deployment.selector + " iframe")[0].contentWindow.Ektron.Workarea.Cloud.Dialogs.Deployment.Create.success(json);
                }

            },
            reset: function () {
                function init() {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.Deployment.selector + " iframe").attr("src", "").attr("src", "./CloudDeploymentWizard.aspx");
                }
                var isDialog = $ektron(Ektron.Workarea.Cloud.Dialogs.Deployment.selector).is(".ui-dialog-content");
                if (isDialog) {
                    init();
                } else {
                    var key = setInterval(function () {
                        isDialog = $ektron(Ektron.Workarea.Cloud.Dialogs.Deployment.selector).is(".ui-dialog-content");
                        if (isDialog) {
                            init();
                            clearInterval(key);
                        }
                    }, 9);
                }
            },
            setHeight: function (height) {
                height = parseInt(height, 10) + 25;
                $ektron(this.selector + " iframe").css("height", height + "px");
                $ektron(this.selector).css("height", height + "px");
            }
        },
        ProfileSettings: {
            selector: "", //set in CloudDeploymentDialogs.ascx
            open: function (data) {
                var profileData = eval("(" + data + ")");
                $ektron(this.selector).find(".profileId").val(profileData["relationship-id"]);
                $ektron(this.selector).dialog("open");
            }
        },
        ServiceSelection: {
            selector: "",
            amazonserviceradioselector: "",
            windowsazureserviceradio: "",
            uxstep1dialog: "",
            next: function () {
                var retVal = true;
                if ($ektron(Ektron.Workarea.Cloud.Dialogs.ServiceSelection.windowsazureserviceradio)[0].checked) {
                    var cloudDialog = $ektron(Ektron.Workarea.Cloud.Dialogs.Deployment.selector);
                    var dialogIframe = cloudDialog.find("iframe");
                    var iframeBody = dialogIframe.contents().find("body");
                    cloudDialog.dialog("option", "position", ["center", 50]).dialog("open");
                    dialogIframe.height(iframeBody.outerHeight() + "px");
                    cloudDialog.dialog("option", "height", iframeBody.outerHeight() + 50);
                    $ektron(Ektron.Workarea.Cloud.Dialogs.ServiceSelection.selector).dialog('close');
                    retVal = false;
                }
                else if ($ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.encodedVal).val() != "") {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.ServiceSelection.selector).dialog('close');
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.amazonWizard).dialog('open');
                    $ektron(".ui-dialog-titlebar-close").bind("click", function () {
                        Ektron.Workarea.Cloud.Dialogs.CloseModal();
                    });
                }
                else {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxAmazonCert).css("display", "block");
                    retVal = false;
                }
                return retVal;
            }
        },
        AmazonServiceWizard: {
            closeDialog: function (dialogId) {
                $ektron(dialogId).dialog('close');
                location.reload(false);
                return false;
            },
            step1: "",
            step2: "",
            step3: "",
            step4: "",
            amazonProfileId: "",
            objAmazonProfile: "",
            step1Next: function (progressId, errorId) {
                var serverErrorMsg = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.serverValidationErrorMsgStep1);
                if ($ektron.trim(serverErrorMsg.html()).length) {
                    serverErrorMsg.css("display", "none");
                }
                var accessKey = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.awsAccessKey).val();
                var secretKey = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.awsSecretKey).val();
                var cloudSiteName = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSiteName).val();
                var numMinInstances = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxInstanceCount).val();
                var numMaxInstances = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxAutoScalingMaxInstances).val();
                var isValid = true;
                if ($ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxAutoScalingCheckbox).is(':checked') && (numMinInstances > numMaxInstances)) {
                    isValid = false;
                }
                if (accessKey != "" && secretKey != "" && cloudSiteName != "" && isValid) {
                    $ektron(progressId).show();
                    $ektron(errorId).hide();
                }
            },
            step1AutoScalingCheckedChanged: function (object) {
                var autoScalingChkBox = $ektron(object);
                if (autoScalingChkBox.is(':checked')) {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.divAutoScalingInstances).attr("style", "display:block");
                }
                else {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.divAutoScalingInstances).attr("style", "display:none");
                }
            },
            ValidateNumberOfMinInstances: function () {
               
                var isValid = true;
                var numMinInstances = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxInstanceCount);
                var numMaxInstances = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxAutoScalingMaxInstances);
                if ($ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxAutoScalingCheckbox).is(':checked') && (numMinInstances.val() > numMaxInstances.val())) {
                    isValid = false;
                }
                if (isValid) {
                    if (numMaxInstances.hasClass('ektron-ui-invalid')) {
                        numMaxInstances.removeClass('ektron-ui-invalid');
                    }
                }
                return isValid;
            },
            ValidateNumberOfMaxInstances: function () {
               
                var isValid = true;
                var numMinInstances = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxInstanceCount);
                var numMaxInstances = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxAutoScalingMaxInstances);
                if ($ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxAutoScalingCheckbox).is(':checked') && (numMinInstances.val() > numMaxInstances.val())) {
                    isValid = false;
                }
                if (isValid) {
                    if (numMinInstances.hasClass('ektron-ui-invalid')) {
                        numMinInstances.removeClass('ektron-ui-invalid');
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.vmMinIns).hide();
                    }
                }
                return isValid;
            },

            step2Next: function (progressId, errorId) {
                var serverErrorMsg = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.serverValidationErrorMsgStep2);
                if ($ektron.trim(serverErrorMsg.html()).length) {
                    serverErrorMsg.css("display", "none");
                }
                $ektron(progressId).hide();
                var returnVal = true;
                var bucketname = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxBucketName).val();
                var newkeypairname = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxNewKeyPair).val();
                var keyPairVal = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlKeyPair)[0];
                var securityGroup = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlSecurityGroup)[0];

                if (keyPairVal.selectedIndex == 0 || securityGroup.selectedIndex == 0 || bucketname == "" || (keyPairVal.selectedIndex == keyPairVal.length - 1 && newkeypairname == "")) {
                    returnVal = false;
                }

                if (returnVal) {
                    $ektron(progressId).show();
                    $ektron(errorId).hide();
                }
                else {
                    $ektron(progressId).hide();
                }
            },

            step2Previous: function (progressId) {
                var serverErrorMsg = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.serverValidationErrorMsgStep2);
                if ($ektron.trim(serverErrorMsg.html()).length) {
                    serverErrorMsg.css("display", "none");
                }
                $ektron(progressId).show();
            },

            step3Next: function (progressId, errorId) {
                var serverErrorMsg = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.serverValidationErrorMsgStep3);
                if ($ektron.trim(serverErrorMsg.html()).length) {
                    serverErrorMsg.css("display", "none");
                }
                $ektron(progressId).hide();
                ////isValid = Page_ClientValidate("");
                var dbName = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxDBName).val();
                var sqlusername = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSQLServerUsername).val();
                var sqlPassword = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSQLServerPassword).val();
                var storageSize = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).val();
                var sqlprefix = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxServerNamePrefix).val();
                var returnVal = true;

                var ddlDBSerName = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBServerName)[0];
                var ddlDBInstanceType = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBInstanceType)[0];
                if (dbName == "" || ddlDBSerName.selectedIndex == 0 || (ddlDBSerName.selectedIndex != 0 && (sqlusername == "" || sqlPassword == ""))) {
                    returnVal = false;
                }
                if (ddlDBSerName.selectedIndex == ddlDBSerName.length - 1) {
                    if (ddlDBInstanceType.selectedIndex == 0)
                        ValidatorEnable($ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxRFVDdlDBInstanceType)[0], true); // enable the required field validator for instance type dropdown
                    else
                        ValidatorEnable($ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxRFVDdlDBInstanceType)[0], false); // disable the required field validator for instance type dropdown

                    var reSQLPassword = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxReenterSQLPassword).val();

                    if (sqlPassword == "" || reSQLPassword == "" || ddlDBInstanceType.selectedIndex == 0) {
                        $ektron(".db-required-parameters").show();
                        returnVal = false;
                    }
                    if (sqlPassword != reSQLPassword || sqlprefix == "") {
                        returnVal = false;
                    }

                    var size = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).val();
                    var sizeInt = parseInt($ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).val());
                    switch (ddlDBInstanceType.selectedIndex) {
                        case 0:
                            returnVal = false;
                            break;
                        case 1:
                        case 2:
                            if (size == "" || sizeInt < 30 || sizeInt > 1024)
                                returnVal = false;
                            break;

                        case 3:
                        case 4:
                            if (size == "" || sizeInt < 200 || sizeInt > 1024)
                                returnVal = false;
                            break;
                    }

                }
                else {
                    ValidatorEnable($ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxRFVDdlDBInstanceType)[0], false); // disable the required field validator for instance type dropdown
                }

                if (returnVal) {
                    $ektron(progressId).show();
                    $ektron(errorId).hide();
                }
                else {
                    $ektron(progressId).hide();
                }
            },

            step3Previous: function (progressId) {
                var serverErrorMsg = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.serverValidationErrorMsgStep3);
                if ($ektron.trim(serverErrorMsg.html()).length) {
                    serverErrorMsg.css("display", "none");
                }
                $ektron(progressId).show();
                var keyPairOptions = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlKeyPair)[0];
                if (keyPairOptions.selectedIndex == keyPairOptions.length - 1) $ektron(".new-setup-item-keypair").css("display", "block");

            },
            step4Save: function (progressId) {
                $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.phStep4ErrorMessage).hide();
                $ektron(progressId).show();
            },
            step4Previous: function () {
                $ektron(".pnlStep4").css("display", "none");
                $ektron(".pnlStep3").css("display", "block");
                return false;
            },
            deploySite: function (obj) {
                if (obj != undefined) {
                    var profileId = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.profileId).val();
                    $ektron(obj).attr('rel', profileId);
                    Ektron.Workarea.Sync.Relationships.Synchronize(obj, false);
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.amazonWizard).dialog('close');
                }
                else {
                    var profileId = Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.amazonProfileId;
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.objAmazonProfile).attr('rel', profileId);
                    Ektron.Workarea.Sync.Relationships.Synchronize(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.objAmazonProfile, false);
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.deployamazondialog).dialog('close');
                }
            },
            alertDeploy: function (obj) {
                $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.deployamazondialog).dialog('open');
                Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.amazonProfileId = $ektron(obj).attr('rel');
                Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.objAmazonProfile = obj;
            },
            toggleKeyPairDiv: function (obj) {
                // if create new key pair is selected
                if (obj.selectedIndex != obj.length - 1) {
                    $ektron(".new-setup-item-keypair").css("display", "none");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxNewKeyPair).removeAttr("data-ektron-validationgroup");
                }
                else {
                    $ektron(".new-setup-item-keypair").css("display", "block");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxNewKeyPair).attr("data-ektron-validationgroup", "BucketValidation");
                }

                // Display or hide validation error message based on the selection
                if (obj.selectedIndex == 0) $ektron(".key-pair-required-lbl").css("display", "block");
                else $ektron(".key-pair-required-lbl").css("display", "none");
            },
            VerifySecurityGroupSelection: function (obj) {
                // Display or hide validation error message based on the selection
                if (obj.selectedIndex == 0) $ektron(".security-group-required-lbl").css("display", "block");
                else $ektron(".security-group-required-lbl").css("display", "none");
            },
            toggleDBSetUpDivs: function (temp) {
                if (temp.selectedIndex == temp.length - 1) {
                    //$ektron(".credential-division").css("display", "none");
                    $ektron(".new-setup-item-DB").css("display", "block");
                }
                else {
                    $ektron(".credential-division").css("display", "block");
                    $ektron(".new-setup-item-DB").css("display", "none");
                }
            },
            Step3DdlDbServernameChanged: function (obj) {
                if (obj.selectedIndex == obj.length - 1) {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.newSetupItemDB).css("display", "block");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.pnlDBInstanceType).css("display", "block");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxReenterSQLPassword).attr("data-ektron-validationgroup", "DBConnectionValidation");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxServerNamePrefix).attr("data-ektron-validationgroup", "DBConnectionValidation");
                    ////$ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).attr("data-ektron-validationgroup", "DBConnectionValidation");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step3).attr("style", "height: 450px; overflow-y: scroll");
                    var ddlDBInstanceType = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBInstanceType)[0];

                    // storage size input is disabled by default (enabled only when an Instance type is selected)
                    if (ddlDBInstanceType.selectedIndex == 0) {
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).prop("disabled", "disabled");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).removeAttr("data-ektron-validationgroup");
                    }
                    else {
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).removeProp("disabled");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).attr("data-ektron-validationgroup", "DBConnectionValidation");
                    }
                }
                else {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.newSetupItemDB).css("display", "none");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.pnlDBInstanceType).css("display", "none");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxReenterSQLPassword).removeAttr("data-ektron-validationgroup");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxServerNamePrefix).removeAttr("data-ektron-validationgroup");
                    ////$ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).removeAttr("data-ektron-validationgroup");
                    ValidatorEnable($ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxRFVDdlDBInstanceType)[0], false); // disable the required field validator for instance type dropdown
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.step3).attr("style", "height: auto; overflow-y: none");
                }
            },
            Step3InstanceTypeChanged: function (obj) {
                var selectedOption = obj.selectedIndex;
                switch (selectedOption) {
                    case 0:
                        ValidatorEnable($ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxRFVDdlDBInstanceType)[0], true); // enable the required field validator for instance type dropdown
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBInstanceClass).prop("disabled", "disabled");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).prop("disabled", "disabled");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).removeAttr("data-ektron-validationgroup");
                        break;
                    case 1:
                        // Instance type is "Express edition", so disable the DB instance classes dropdown. This will ensure that "Small" is the option selected.
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBInstanceClass).prop("disabled", "disabled");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).removeProp("disabled");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSizeLabelExpWeb).css("display", "block");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSizeLabelStd).css("display", "none");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.newSetupItemDB).css("display", "block");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).attr("data-ektron-validationgroup", "DBConnectionValidation");
                        break;
                    case 2:
                        // Instance type is "Web edition"
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBInstanceClass).removeProp("disabled");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).removeProp("disabled");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSizeLabelExpWeb).css("display", "block");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSizeLabelStd).css("display", "none");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.newSetupItemDB).css("display", "block");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).attr("data-ektron-validationgroup", "DBConnectionValidation");
                        break;
                    case 3:
                        // Instance type is "Standard edition"
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBInstanceClass).removeProp("disabled");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).removeProp("disabled");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSizeLabelExpWeb).css("display", "none");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSizeLabelStd).css("display", "block");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.newSetupItemDB).css("display", "block");
                        $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxStorageSize).attr("data-ektron-validationgroup", "DBConnectionValidation");
                        break;
                }
            },
            checkForillegalChar: function (value, element, errorMessage, contextInfo) {
                var doubleByteCharCheck = /[^\\u0000-\\u00ff]/;
                var firstCharAlphaCheck = /^[a-zA-Z]+$/;
                var firstCharWhiteSpaceCheck = /^\s+/;
                if ((value.indexOf("\\") >= 0) ||
                        (value.indexOf("/") >= 0) ||
                        (value.indexOf(":") >= 0) ||
                        (value.indexOf("*") >= 0) ||
                        (value.indexOf("?") >= 0) ||
                        (value.indexOf("\"") >= 0) ||
                        (value.indexOf("#") >= 0) ||
                        (value.indexOf("$") >= 0) ||
                        (value.indexOf("[") >= 0) ||
                        (value.indexOf("]") >= 0) ||
                        (value.indexOf("^") >= 0) ||
                        (value.indexOf("<") >= 0) ||
                        (value.indexOf(">") >= 0) ||
                        (value.indexOf("|") >= 0) ||
                        (value.indexOf("&") >= 0) ||
                        (value.indexOf("\'") >= 0) ||
                        (value.indexOf("%") >= 0) ||
                        (value.indexOf("+") >= 0) ||
                        (value.length == 1) ||
                        (value.indexOf(" ") >= 0) ||
                        (value.indexOf("@") >= 0) ||
                        (value.indexOf("!") >= 0) ||
                        (value.indexOf("(") >= 0) ||
                        (value.indexOf(")") >= 0) ||
                        (value.length > 128) ||
                        !(firstCharAlphaCheck.test(value.charAt(0))) ||
                        (firstCharWhiteSpaceCheck.test(value.charAt(0))) ||
                        (value.indexOf(",") >= 0)) {
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSiteNameRestriction).css("display", "block");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSiteNameRestriction).css("visible", "true");
                    $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSiteNameRestriction).css("visible", "visible");
                    return false;
                }
                else
                    return true;
            },
            CheckForValidChars: function (value, element, errorMessage, contextInfo) {
                var regexAllowedChars = /^[a-z][a-z0-9]*$/i;
                return regexAllowedChars.test(value);
            },
            CheckForWhiteSpacesAndEmpty: function (value, element, errorMessage, contextInfo) {
                var whiteSpaceCheck = /^$|^\s+$/;
                return whiteSpaceCheck.test(value);
            },
            VerifyPasswordMatch: function (value, element, errorMessage, contextInfo) {
                var isValid = true;
                var sqlPassword = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.uxSQLServerPassword).val();
                if (value != sqlPassword)
                    isValid = false;
                return isValid;
            },
            VerifyStorageSize: function (value, element, errorMessage, contextInfo) {
                var isValid = true;
                var size = parseInt(value);
                var dbInstanceType = $ektron(Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.ddlDBInstanceType)[0];
                switch (dbInstanceType.selectedIndex) {
                    case 0:
                        isValid = false;
                        break;
                    case 1:
                    case 2:
                        if (size < 30 || size > 1024)
                            isValid = false;
                        break;

                    case 3:
                    case 4:
                        if (size < 200 || size > 1024)
                            isValid = false;
                        break;
                }

                return isValid;
            }
        },
        StartSite: {
            execute: function () {
                //set vars
                var selector = this.selector;
                var progressImageSelector = this.progressImageSelector;
                var errorMessageSelector = this.errorMessageSelector;
                var successMessageSelector = this.successMessageSelector;
                var confirmationMessageSelector = this.confirmationMessageSelector;

                //set ui
                $ektron(confirmationMessageSelector).fadeOut("fast", function () {
                    var ui = {
                        "selector": selector,
                        "progressImageSelector": progressImageSelector,
                        "successMessageSelector": successMessageSelector,
                        "errorMessageSelector": errorMessageSelector,
                        "showStatus": true
                    };

                    var xhrData = eval("(" + Ektron.Workarea.Cloud.Dialogs.StartSite.data + ")");
                    xhrData.command = "ChangeStatus";

                    Ektron.Workarea.Cloud.Ajax.execute(ui, xhrData);
                });
            },
            data: "",
            progressImage: "", //set in CloudDeploymentDialogs.ascx
            selector: "", //set in CloudDeploymentDialogs.ascx
            errorMessageSelector: "", //set in CloudDeploymentDialogs.ascx
            confirmationMessageSelector: "", //set in CloudDeploymentDialogs.ascx
            successMessageSelector: "", //set in CloudDeploymentDialogs.ascx
            open: function (data) {
                this.data = data;
                $ektron(this.selector).dialog("open");
            }
        },
        StopSite: {
            execute: function () {
                //set vars
                var selector = this.selector;
                var progressImageSelector = this.progressImageSelector;
                var errorMessageSelector = this.errorMessageSelector;
                var successMessageSelector = this.successMessageSelector;
                var confirmationMessageSelector = this.confirmationMessageSelector;

                //set ui
                $ektron(confirmationMessageSelector).fadeOut("fast", function () {
                    var ui = {
                        "selector": selector,
                        "progressImageSelector": progressImageSelector,
                        "successMessageSelector": successMessageSelector,
                        "errorMessageSelector": errorMessageSelector,
                        "showStatus": true
                    };

                    var xhrData = eval("(" + Ektron.Workarea.Cloud.Dialogs.StopSite.data + ")");
                    xhrData.command = "ChangeStatus";

                    Ektron.Workarea.Cloud.Ajax.execute(ui, xhrData);
                });
            },
            data: "{}",
            progressImage: "", //set in CloudDeploymentDialogs.ascx
            selector: "", //set in CloudDeploymentDialogs.ascx
            errorMessageSelector: "", //set in CloudDeploymentDialogs.ascx
            confirmationMessageSelector: "", //set in CloudDeploymentDialogs.ascx
            successMessageSelector: "", //set in CloudDeploymentDialogs.ascx
            open: function (data) {
                this.data = data;
                $ektron(this.selector).dialog("open");
            }
        },
        SwapDeployment: {
            execute: function () {
                //set vars
                var selector = this.selector;
                var progressImageSelector = this.progressImageSelector;
                var errorMessageSelector = this.errorMessageSelector;
                var successMessageSelector = this.successMessageSelector;
                var confirmationMessageSelector = this.confirmationMessageSelector;

                //set ui
                $ektron(confirmationMessageSelector).fadeOut("fast", function () {
                    $ektron(selector).closest(".ui-dialog").find(".ui-dialog-buttonpane").slideUp("fast", function () {
                        $ektron(progressImageSelector).slideDown("fast", function () {
                            var ui = {
                                "selector": selector,
                                "progressImageSelector": progressImageSelector,
                                "successMessageSelector": successMessageSelector,
                                "errorMessageSelector": errorMessageSelector
                            };

                            var xhrData = eval("(" + Ektron.Workarea.Cloud.Dialogs.SwapDeployment.data + ")");
                            xhrData.command = "SwapDeployment";

                            Ektron.Workarea.Cloud.Ajax.execute(ui, xhrData);
                        });
                    });
                });
            },
            data: "",
            progressImage: "", //set in CloudDeploymentDialogs.ascx
            selector: "", //set in CloudDeploymentDialogs.ascx
            errorMessage: "", //set in CloudDeploymentDialogs.ascx
            confirmationMessage: "", //set in CloudDeploymentDialogs.ascx
            successMessage: "", //set in CloudDeploymentDialogs.ascx
            open: function (data) {
                this.data = data;
                $ektron(this.selector).dialog("open");
            }
        },
        DeployAmazon: {
            amazonlaunchdeploydialog: "",
            open: function () {
                $ektron(this.amazonlaunchdeploydialog).dialog("open");
            }
        }
    }
};