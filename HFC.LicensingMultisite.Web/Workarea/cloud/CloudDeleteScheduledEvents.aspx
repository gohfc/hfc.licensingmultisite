﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CloudDeleteScheduledEvents.aspx.cs" Inherits="Ektron.Workarea.Cloud.Deployment.DeleteScheduledEvents" culture="auto" meta:resourcekey="PageResourceKey" uiculture="auto" %>
<!DOCTYPE>
<html>
    <head runat="server">
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <form id="form1" runat="server">
            <ektronUI:JavaScript ID="uxWorakreaJs" runat="server" Path="{WorkareaPath}/java/ektron.workarea.js" />
            <div class="ektron-scheduled-cloud-events">
                <ektronUI:CssBlock ID="uxCloudDeploymentScheduleStyles" runat="server">
                    <CssTemplate>
                        .ektron-scheduled-cloud-events .no-wrap {white-space:nowrap;}
                        .ektron-scheduled-cloud-events .ektronPageContainer input {width:6em !important;}
                        .ektron-scheduled-cloud-events table .column-delete {width:5%;}
                        .ektron-scheduled-cloud-events table .column-description {width:15%;}
                        .ektron-scheduled-cloud-events table .column-date {width:10%;}
                        .ektron-scheduled-cloud-events table .column-status {width:10%;}
                        .ektron-scheduled-cloud-events table .column-event-type {width:5%;}
                        .ektron-scheduled-cloud-events table .column-web-instances {width:10%;text-align:center;}
                        .ektron-scheduled-cloud-events table .column-email-notifications {width:15%;}
                        .ektron-scheduled-cloud-events table th,
                        table.ektron-view-scheduled-cloud-events td {vertical-align:top; padding: 1em;}
                        .ektron-ui-even {background-color: #fff;}
                        .ektron-sort-ascending {}
                        .ektron-sort-descending {}
                    </CssTemplate>
                </ektronUI:CssBlock>
                <div class="ektronPageHeader">
                    <div class="ektronTitlebar" id="divTitleBar" runat="server">
                        <span id="WorkareaTitleBar"><asp:Literal ID="aspHeaderTextId" runat="server" Text="<%$ Resources:ViewScheduledCloudEventsText %>" /></span>
                        <span id="_WorkareaTitleBar" style="display:none;"></span>
                    </div>
                    <div class="ektronToolbar" id="divToolBar" runat="server">
                        <table>
                            <tr>
                                <td><asp:LinkButton ID="aspBackButton" runat="server" CausesValidation="false" CssClass="primary backButton" PostBackUrl="~/Workarea/cloud/CloudDeployment.aspx" ToolTip="<%$ Resources:BackButtonText %>" Text="<%$ Resources:BackButtonText %>" /></td>
                                <td><div class="actionbarDivider"></div></td>
                                <td>
                                    <ektronUI:Button ID="uxAddButton" runat="server" DisplayMode="Submit" OnClick="ShowAddUI" Text="<%$ Resources:DeleteEventButtonText %>"></ektronUI:Button><%--ToolTip="<%$ Resources:AddButtonText %>" --%>
                                </td>
                                <td><div class="actionbarDivider"></div></td>
                                <td>
                                    <%--<asp:ImageButton ID="aspContextualHelpButton" runat="server" ImageUrl="/WorkArea/images/UI/Icons/help.png" OnClick="PopUpWindow('http://documentation.ektron.com/cms400/v8.50/webhelpindex.html?alias=NoCollectiontopiclist.xml', 'SitePreview', 600, 500, 1, 1);return false;" ToolTip="<%$ Resources:AddButtonText %>" Text="<%$ Resources:AddButtonText %>" />--%>
                                    <ektronUI:Button ID="uxContextualHelpButton" runat="server" CssClass="contextualHelpButton" DisplayMode="Anchor"><img id="DeskTopHelp" onclick="PopUpWindow('http://documentation.ektron.com/cms400/v8.50/webhelpindex.html?alias=NoCollectiontopiclist.xml', 'SitePreview', 600, 500, 1, 1);return false;" src="/cms400min/WorkArea/images/UI/Icons/help.png" tt="Click here to get help " /></ektronUI:Button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="ektronPageContainer">
                    <%--GridView control will use EktronUI GridView when the control is available--%>
                    <asp:GridView ID="aspViewScheduledCloudEvents" AllowSorting="true" AllowPaging="true" PageSize="10" AutoGenerateColumns="false" runat="server" CssClass="ektron-view-scheduled-cloud-events" SortedAscendingHeaderStyle-CssClass="ektron-sort-ascending" SortedDescendingHeaderStyle-CssClass="ektron-sort-descending" SelectedRowStyle-CssClass="ektron-selected-row">
                        <Columns>
                            <%--<asp:CheckBoxField AccessibleHeaderText="string" DataField="string" FooterText="string" HeaderImageUrl="uri" HeaderText="string" InsertVisible="true" ReadOnly="false" ShowHeader="true" SortExpression="string" Text="string" Visible="true"></asp:CheckBoxField>--%>
                            <asp:CheckBoxField HeaderText="<%$ Resources:HeaderTextDelete %>" SortExpression="name" ReadOnly="false" HeaderStyle-CssClass="column-delete" Visible="true" />
                            <asp:BoundField HeaderText="<%$ Resources:HeaderTextDescription %>" DataField="name" SortExpression="name" ReadOnly="true" HeaderStyle-CssClass="column-description" />
                            <asp:BoundField HeaderText="<%$ Resources:HeaderTextDateAndTime %>" DataField="date" SortExpression="date" ReadOnly="true" HeaderStyle-CssClass="column-date" /><%--defaultsort--%>
                            <asp:BoundField HeaderText="<%$ Resources:HeaderTextStatus %>" DataField="operation" SortExpression="operation" ReadOnly="true" HeaderStyle-CssClass="column-status" />
                            <asp:BoundField HeaderText="<%$ Resources:HeaderTextEventType %>" DataField="type" SortExpression="type" ReadOnly="true" HeaderStyle-CssClass="column-event-type" />
                            <asp:BoundField HeaderText="<%$ Resources:HeaderTextWebInstances %>" DataField="instance" SortExpression="instance" ReadOnly="true" HeaderStyle-CssClass="column-web-instances" ItemStyle-CssClass="column-web-instances" />
                            <asp:BoundField HeaderText="<%$ Resources:HeaderTextEmailNotifications %>" DataField="NotificationEmail" SortExpression="NotificationEmail" ReadOnly="true" HeaderStyle-CssClass="column-email-notifications" />
                        </Columns>
                        <RowStyle CssClass="ektron-ui-odd" />
                        <AlternatingRowStyle CssClass="ektron-ui-even" />
                        <EmptyDataTemplate>
                            <p>
                                <asp:Literal ID="aspEmptyDataTemplateText" runat="server" Text="<%$ Resources:EmptyDataTemplateText %>" />
                            </p>
                        </EmptyDataTemplate>
                    </asp:GridView>

                    <ektronUI:Dialog ID="uxDialog" runat="server" AutoOpen="true" Visible="false" Draggable="true" Modal="true" Resizable="false" Title="<%$ Resources:DialogTitle %>">
                        <Buttons>
                            <ektronUI:DialogButton ID="uxDeleteButton" runat="server" Text="<%$ Resources:DeleteText %>" Visible="false" OnCommand="Delete" />
                            <ektronUI:DialogButton ID="uxCancelButton" runat="server" CloseDialog="true" Text="<%$ Resources:CloseDialogButtonText %>" />
                        </Buttons>
                        <ContentTemplate>
                            <ektronUI:Message ID="uxDialogMessage" runat="server" DisplayMode="Error" />
                        </ContentTemplate>
                    </ektronUI:Dialog>
                </div>
            </div>
        </form>
    </body>
</html>
