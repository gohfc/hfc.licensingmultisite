﻿namespace Ektron.Workarea.Cloud.Deployment
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cloud.Common;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core.Validation;

    public partial class AddEditCloudScheduleEventItem : System.Web.UI.Page
    {
        #region properties

        private long profileId = -1;
        private long ProfileId
        {
            get
            {
                if(long.TryParse(Request.QueryString["profile-id"], out profileId))
                {
                    return profileId;
                }
                else
                {
                    return 0;
                }
            }
        }

        private Ektron.Cms.Sync.Client.Relationship Relationship
        {
            get;
            set;
        }

        private string GUID
        {
            get
            {
                return Request.QueryString["guid"];
            }
        }

        private bool dataBind = true;
        private CloudServiceRequest viewData;
        private SiteAPI _siteApi;
        private readonly StyleHelper _styleHelper;
        private bool IsEdit = false;
        protected CloudServiceRequest ViewData
        {
            get
            {
                if (this.viewData == null)
                {
                    viewData = Ektron.DbSync.Core.Serializer.Deserialize<CloudServiceRequest>(Ektron.Cms.Sync.Client.Relationship.GetExternalArgs(ProfileId));
                }
                return this.viewData;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public AddEditCloudScheduleEventItem()
        {
            _siteApi = new SiteAPI();
            _styleHelper = new StyleHelper();
        }
        #endregion

        #region Page Events
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!Utilities.ValidateUserLogin())
            {
                return;
            }
            if (!_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) &&
                !_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
            {
                Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
            }
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ValidationAPI.AddRule(
                aspDescriptionTextBox,
                new RequiredRule("this field is required!")
            );
            int c = ViewData.DeploymentSchedules.Count;
            if (!string.IsNullOrEmpty(GUID))
            {
                IsEdit = true;
            }

            if (this.viewData != null && this.viewData.ServiceArguments != null && this.viewData.ServiceArguments.RelationShipId != null)
            {
                this.Relationship = this.GetRelationship(this.ViewData.ServiceArguments.RelationShipId);
                if (this.Relationship != null)
                {
                    if (this.Relationship.Type == FileSync.Common.ProfileType.Amazon)
                    {
                        this.uxDeleteConfirmText.Text = GetLocalResourceObject("strDeleteConfirmAmazon").ToString();
                    }
                    else if (this.Relationship.Type == FileSync.Common.ProfileType.Azure)
                    {
                        this.uxDeleteConfirmText.Text = GetLocalResourceObject("strDeleteConfirmAzure").ToString();
                    }
                }
            }
            else
            {
                this.uxDeleteConfirmText.Text = GetLocalResourceObject("strDeleteConfirmAzure").ToString();
            }

            if (!Page.IsPostBack)
            {
                this.initControls();
                if (IsEdit)
                    populateItemDataToPage(GUID);
            }
            aspBackButton.PostBackUrl = "CloudScheduledEvents.aspx?profile-id=" + Request.QueryString["profile-id"];

            StyleHelper styleHelper = new StyleHelper();
            aspHelpButton.Text = styleHelper.GetHelpButton("view_azure_host", string.Empty);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.DataBind();
        }
        #endregion

        protected void btnDeleteConfirm_Click(object sender, EventArgs e)
        {
            if (uxDeleteConfirm.Text == "CONFIRM")
            {
                DoAddEdit();
            }
            else
            {
                uxDeleteConfirmDialog.AutoOpen = true;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool redirect = true;

            if (CheckDeleteSelection())
            {
                uxDeleteConfirmDialog.AutoOpen = true;
                return;
            }
            else
            {
                uxDeleteConfirmDialog.AutoOpen = false;
            }
            DoAddEdit();
        }

        private void DoAddEdit()
        {
            if (!IsEdit)
                AddItem();
            else
            {
                EditItem(GUID);
            }
            Response.Redirect("CloudScheduledEvents.aspx?profile-id=" + profileId.ToString(), true);
        }

        protected void EventTypeSelectionChanged(object sender, EventArgs e)
        {
            aspDeleteOptions.Visible = ddlEvtType.SelectedItem.Value == ((int)CloudCommandType.DeleteDeployment).ToString();
            aspDeleteOptionsDescription.Visible = ddlEvtType.SelectedItem.Value == ((int)CloudCommandType.DeleteDeployment).ToString();
            tbNumOfInst.Enabled = true;
            // Show labels based on profile type
            if (ddlEvtType.SelectedValue == ((int)CloudCommandType.DeleteDeployment).ToString())
            {
                tbNumOfInst.Enabled = false;
                if (this.Relationship != null)
                {
                    ListItem liRelationships = aspDeleteOptions.Items.FindByValue("1");
                    ListItem liDatabase = aspDeleteOptions.Items.FindByValue("4");

                    if (this.Relationship.Type == FileSync.Common.ProfileType.Azure)
                    {
                        liRelationships.Text = GetLocalResourceObject("aspDeleteOptionsRelationshipsAzure").ToString();
                        liDatabase.Text = GetLocalResourceObject("aspDeleteOptionsDatabaseAzure").ToString();
                    }
                    else if (this.Relationship.Type == FileSync.Common.ProfileType.Amazon)
                    {
                        ListItem litmp = aspDeleteOptions.Items.FindByValue("2");
                        if (litmp != null)
                        {
                            aspDeleteOptions.Items.Remove(litmp);
                        }
                        liRelationships.Text = GetLocalResourceObject("aspDeleteOptionsRelationshipsAmazon").ToString();
                        liDatabase.Text = GetLocalResourceObject("aspDeleteOptionsDatabaseAmazon").ToString();
                    }
                }
            }
        }

        private void initControls()
        {
            Type operationTypes = typeof(Ektron.Cloud.Common.CloudCommandType);

            //Event Type item values:
            //  <option value="0">CreateDeployment</option>
            //  <option value="1">ChangeConfig</option>
            //  <option value="2">SwapDeployment</option>
            //  <option value="3" selected="selected">DeleteDeployment</option>
            //  <option value="4">ChangeStatus</option>
            //  <option value="5">GetStatus</option>
            //ddlEvtType.Items.Clear();
            foreach (int value in Enum.GetValues(operationTypes))
            {
                string name = Enum.GetName(operationTypes, value);
                //ddlEvtType.Items.Add(new ListItem(name, value.ToString()));
            }
            //aspDeleteOption.Items.Clear();

            Type tDeleteOptions = typeof(DeleteOption);
            foreach (int value in Enum.GetValues(tDeleteOptions))
            {
                string name = Enum.GetName(tDeleteOptions, value);
                //aspDeleteOption.Items.Add(new ListItem(name, value.ToString()));
            } 

            //TimeSpan dayhour = TimeSpan.FromDays(1);
            //TimeSpan hourmin = TimeSpan.FromHours(1);
            //aspEditHour.Items.Clear();
            //aspEditMinute.Items.Clear();
            //for (int i = 0; i < dayhour.TotalHours; i++)
            //{
            //    this.aspEditHour.Items.Add(new ListItem(i.ToString(), i.ToString()));
            //}
            //for (int i = 0; i < hourmin.TotalMinutes; i++)
            //{
            //    this.aspEditMinute.Items.Add(new ListItem(i.ToString(), i.ToString()));
            //}

            aspDeleteOptions.Visible = false;
            aspDeleteOptionsDescription.Visible = false;
        }
        
        private void AddItem()
        {
            if (ViewData.DeploymentSchedules == null && ViewData.DeploymentSchedules.Count == 0)
            {
                ViewData.DeploymentSchedules = new List<CloudProvisioningSchedule>();
            }
            CloudProvisioningSchedule item = new CloudProvisioningSchedule();
            item.Id = Guid.NewGuid();
            populateDataFromPage(ref item);
            uxDeleteConfirmDialog.AutoOpen = false;
            ViewData.DeploymentSchedules.Add(item);
            Ektron.Cms.Sync.Client.Relationship.SetExternalArgs(ProfileId, Ektron.DbSync.Core.Serializer.Serialize<CloudServiceRequest>(ViewData));
        }

        private void EditItem(string strGUID)
        {
            CloudProvisioningSchedule item = ViewData.DeploymentSchedules.Find(x => x.Id.ToString() == strGUID);
            populateDataFromPage(ref item);
            Ektron.Cms.Sync.Client.Relationship.SetExternalArgs(ProfileId, Ektron.DbSync.Core.Serializer.Serialize<CloudServiceRequest>(ViewData));
            uxDeleteConfirmDialog.AutoOpen = false;
        }

        private bool CheckDeleteSelection()
        {
            foreach (ListItem li in aspDeleteOptions.Items)
            {
                int itemVal = int.Parse(li.Value);
                switch (itemVal)
                {
                    case 1:
                        if (li.Selected)
                            viewData.DeleteOptions = viewData.DeleteOptions | DeleteOption.Relation;
                        break;
                    case 2:
                        if (li.Selected)
                            viewData.DeleteOptions = viewData.DeleteOptions | DeleteOption.Service;
                        break;
                    case 4:
                        if (li.Selected)
                            viewData.DeleteOptions = viewData.DeleteOptions | DeleteOption.Database;
                        break;
                    case 8:
                        if (li.Selected)
                            viewData.DeleteOptions = viewData.DeleteOptions | DeleteOption.Storage;
                        break;
                }
            }
            if ((CloudCommandType)int.Parse(ddlEvtType.SelectedValue) == CloudCommandType.DeleteDeployment)
                return true;
            else
                return false;
        }

        private void populateDataFromPage(ref CloudProvisioningSchedule item)
        {
            item.Name = aspDescriptionTextBox.Text;
            item.Operation = (CloudCommandType)int.Parse(ddlEvtType.SelectedValue);
            item.Instance = tbNumOfInst.Value;
            item.Recurrence = CloudProvisioningSchedule.RecurrenceType.OneTime;//there only one type
            DateTime date = this.uxEventDate.Value;
            string hour = aspEditHour.SelectedValue;
            string minute = aspEditMinute.SelectedValue;
            item.Date = (DateTime)(date + new TimeSpan(Int32.Parse(hour), Int32.Parse(minute), 0));
            item.NotificationEmail = aspEmailsTextBox.Text;
            item.Enabled = false;
            //item.CauseNotification = false;
            if (item.Operation == CloudCommandType.DeleteDeployment)
            {

                //public enum DeleteOptions
                //{
                //    Relation = 1,
                //    Service = 2,
                //    Database = 4,
                //    Storage = 8
                //}
                viewData.DeleteOptions = DeleteOption.Relation;
                viewData.DeleteOptions = viewData.DeleteOptions ^ DeleteOption.Relation;
                foreach (ListItem li in aspDeleteOptions.Items)
                {
                    int itemVal = int.Parse(li.Value);
                    switch (itemVal)
                    {
                        case 1:
                            if (li.Selected)
                                viewData.DeleteOptions = viewData.DeleteOptions | DeleteOption.Relation;
                            break;
                        case 2:
                            if (li.Selected)
                                viewData.DeleteOptions = viewData.DeleteOptions | DeleteOption.Service;
                            break;
                        case 4:
                            if (li.Selected)
                                viewData.DeleteOptions = viewData.DeleteOptions | DeleteOption.Database;
                            break;
                        case 8:
                            if (li.Selected)
                                viewData.DeleteOptions = viewData.DeleteOptions | DeleteOption.Storage;
                            break;
                    }
                }
            }
        }

        private void populateItemDataToPage(string strGUID)
        {
            CloudProvisioningSchedule item = ViewData.DeploymentSchedules.Find(x => x.Id.ToString() == strGUID);
            tbNumOfInst.Enabled = true;
            aspDescriptionTextBox.Text = item.Name;
            ddlEvtType.SelectedItem.Selected = false;
            ddlEvtType.Items.FindByValue(((int)item.Operation).ToString()).Selected = true;

            tbNumOfInst.Text = item.Instance.ToString();

            this.uxEventDate.Value = item.Date.Date;
            
            aspEditHour.SelectedItem.Selected = false;
            if (item.Date.Hour.ToString().Length == 1)
            {
                aspEditHour.Items.FindByValue("0" + item.Date.Hour.ToString()).Selected = true;
            }
            else
            {
                aspEditHour.Items.FindByValue(item.Date.Hour.ToString()).Selected = true;
            }

            aspEditMinute.SelectedItem.Selected = false;
            if (item.Date.Minute.ToString().Length == 1)
            {
                aspEditMinute.Items.FindByValue("0" + item.Date.Minute.ToString()).Selected = true;
            }
            else
            {
                aspEditMinute.Items.FindByValue(item.Date.Minute.ToString()).Selected = true;
            }

            aspEmailsTextBox.Text = item.NotificationEmail;

            if (item.Operation == CloudCommandType.DeleteDeployment)
            {
                tbNumOfInst.Enabled = false;
                aspDeleteOptions.Visible = true;
                aspDeleteOptionsDescription.Visible = true;

                if (this.viewData != null && this.viewData.ServiceArguments != null && this.viewData.ServiceArguments.RelationShipId != null)
                {
                    Ektron.Cms.Sync.Client.Relationship relationship = Ektron.Cms.Sync.Client.Relationship.GetRelationship(viewData.ServiceArguments.RelationShipId);
                    if (relationship != null)
                    {
                        ListItem liRelationships = aspDeleteOptions.Items.FindByValue("1");
                        ListItem liDatabase = aspDeleteOptions.Items.FindByValue("4");

                        if (relationship.Type == FileSync.Common.ProfileType.Azure)
                        {
                            liRelationships.Text = GetLocalResourceObject("aspDeleteOptionsRelationshipsAzure").ToString();
                            liDatabase.Text = GetLocalResourceObject("aspDeleteOptionsDatabaseAzure").ToString();
                        }
                        else if (relationship.Type == FileSync.Common.ProfileType.Amazon)
                        {
                            ListItem litmp = aspDeleteOptions.Items.FindByValue("2");
                            if (litmp != null)
                            {
                                aspDeleteOptions.Items.Remove(litmp);
                            }
                            liRelationships.Text = GetLocalResourceObject("aspDeleteOptionsRelationshipsAmazon").ToString();
                            liDatabase.Text = GetLocalResourceObject("aspDeleteOptionsDatabaseAmazon").ToString();
                        }
                    }
                }
            }
            else
            {
                aspDeleteOptions.Visible = false;
                aspDeleteOptionsDescription.Visible = false;
            }

            foreach (ListItem li in aspDeleteOptions.Items)
            {
                int itemVal = int.Parse(li.Value);
                switch (itemVal)
                {
                    case 1:
                        li.Selected = (viewData.DeleteOptions & DeleteOption.Relation) == DeleteOption.Relation;
                        break;
                    case 2:
                        li.Selected = (viewData.DeleteOptions & DeleteOption.Service) == DeleteOption.Service;
                        break;
                    case 4:
                        li.Selected = (viewData.DeleteOptions & DeleteOption.Database) == DeleteOption.Database;
                        break;
                    case 8:
                        li.Selected = (viewData.DeleteOptions & DeleteOption.Storage) == DeleteOption.Storage;
                        break;
                }
            }
        }

        private Ektron.Cms.Sync.Client.Relationship GetRelationship(long relationshipid)
        {
            return Ektron.Cms.Sync.Client.Relationship.GetRelationship(relationshipid);
        }
    }
}
