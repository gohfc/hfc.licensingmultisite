namespace Ektron.Workarea.Cloud.Dialogs
{
    using System;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Web.UI.WebControls;
    using System.Xml;
    using Ektron.Cloud.Azure.Validator;
    using Ektron.Cloud.Common;
    using Ektron.Cms;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Sync.Client;
    using Ektron.Cms.Sync.Presenters;

    public partial class DeploymentWizard : System.Web.UI.Page
    {
        public const string AzureDeploymentPath = "~/azuredeploy/{0}/{1}/";

        #region members, enums, constants

        private IValidator validator;
        private ValidationResult validation;
        private NameValueCollection deploymentParams;

        #endregion

        #region properties

        public string DeploymentData { get; set; }
        public string UploadPath
        {
            get
            {
                return String.Format(AzureDeploymentPath, uxSubscriptionId.Text, uxServiceName.Text);
            }
        }
        public string ProgressHandlerPath
        {
            get
            {
                ICmsContextService cmsContext = ServiceFactory.CreateCmsContextService();
                return cmsContext.WorkareaPath + "/cloud/CloudDeploymentHandler.ashx";
            }
        }

        #endregion

        #region events

        protected override void OnInit(EventArgs e)
        {
            Server.ScriptTimeout = 36000;
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.RegisterCss(this);
            Packages.Ektron.Namespace.Register(this);
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            //to support help links
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronEmpJSFuncJS);
            this.DataBindChildren();
            base.OnPreRender(e);
            uxPassword.Attributes.Add("autocomplete", "off");
            uxUsername.Attributes.Add("autocomplete", "off");

            //set help button
            StyleHelper styleHelper = new StyleHelper();
            aspLocationHelpButton.Text = styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Location_Choices", String.Empty);
            aspCachingHelpButton.Text = @"<span style=""vertical-align:top"">" + styleHelper.GetHelpButton("Cloud_Deployment_Wizard_Caching_Xml", String.Empty) + @"</span>";
        }

        protected void WizardButtonClick(object sender, CommandEventArgs e)
        {
            aspHeaderText.Visible = true;
            switch (e.CommandArgument.ToString().ToLower())
            {
                case "cancel":
                    this.validation = new ValidationResult();
                    this.validation.IsValid = true;
                    aspWizardSteps.ActiveViewIndex = aspWizardStepTitle.ActiveViewIndex = 0;
                    uxCancelJs.Visible = true;
                    break;
                case "next":
                    ValidateParam();
                    if (this.validation.IsValid)
                    {
                        aspWizardSteps.ActiveViewIndex = aspWizardStepTitle.ActiveViewIndex = aspWizardSteps.ActiveViewIndex + 1;
                    }
                    break;
                case "previous":
                    this.validation = new ValidationResult();
                    this.validation.IsValid = true;
                    aspWizardSteps.ActiveViewIndex = aspWizardStepTitle.ActiveViewIndex = aspWizardSteps.ActiveViewIndex - 1;
                    break;
                case "create":
                    ValidateParam();
                    if (this.validation.IsValid)
                    {
                        #region collect form values

                        this.deploymentParams = new NameValueCollection();

                        //step 1
                        this.deploymentParams.Add("subscription-id", uxSubscriptionId.Text);
                        this.deploymentParams.Add("service-name", uxServiceName.Text);
                        this.deploymentParams.Add("location", aspLocationDropDown.SelectedValue);
                        this.deploymentParams.Add("slot", aspSlots.SelectedValue);
                        this.deploymentParams.Add("web-vm-size", aspWebVmSize.SelectedValue);
                        this.deploymentParams.Add("worker-vm-size", aspWorkerVmSize.SelectedValue);
                        this.deploymentParams.Add("instance", uxInstanceCount.Text);

                        //step 2
                        this.deploymentParams.Add("storage-account", uxStorageAccount.Text);
                        char[] arr = new char[] { 'h', 't', 't', 'p', ':', '/', '/' };
                        this.deploymentParams.Add("cdn", !string.IsNullOrEmpty(uxCDN.Text) ? "http://" + uxCDN.Text.TrimStart(arr).TrimEnd('/') + "/" : string.Empty);
                        this.deploymentParams.Add("licensekey", uxLicenseKey.Text);
                        this.deploymentParams.Add("https", aspSecurityProtocolSetting.SelectedValue == "HTTPS" ? "1" : "0");
                        this.deploymentParams.Add("ssl-thumbprint", uxSecurityCertificateThumbprint.Value);
                        this.deploymentParams.Add("caching-xml", aspCachingXml.Text);

                        //step 3
                        this.deploymentParams.Add("dboption", ddlDbOption.SelectedValue);
                        //this.deploymentParams.Add("dbedition", rdlDbEdition.SelectedValue);
                        this.deploymentParams.Add("dbsize", ddlDbSize.SelectedValue);
                        this.deploymentParams.Add("dbserver", uxServer.Value);
                        this.deploymentParams.Add("dbname", uxDatabase.Value);
                        this.deploymentParams.Add("dbuser", uxUsername.Value);
                        this.deploymentParams.Add("dbpassword", uxPassword.Value);
                        //step 4
                        this.deploymentParams.Add("deployment-package", aspDeploymentPackageUploadOptions.SelectedValue.ToLower());
                        var packageUploadedFile = uxPackageFile.UploadedFiles.FirstOrDefault();
                        if (packageUploadedFile != null)
                        {
                            this.deploymentParams.Add("package-file", packageUploadedFile.OriginalFileName);
                        }

                        var configurationUploadedFile = uxConfigurationFile.UploadedFiles.FirstOrDefault();
                        if (configurationUploadedFile != null)
                        {
                            this.deploymentParams.Add("configruation-file", configurationUploadedFile.OriginalFileName);
                        }

                        //other required data
                        this.deploymentParams.Add("command", "CreateDeployment");
                        #endregion

                        //set deployment data
                        //this.DeploymentData = @"{""ProfileId"": """ + Save().ToString() + @""",""command"": ""CreateDeployment""}";// data.ToString();
                        this.DeploymentData = @"{""ProfileId"": """ + Save().ToString() + @"""}";// data.ToString();

                        //set wizard to response status
                        aspWizard.SetActiveView(aspSuccessView);
                        aspWizardStepTitle.SetActiveView(aspSyncingTitleView);
                        aspHeaderText.Visible = false;
                    }
                    break;
                case "resubmit":
                    this.validation = new ValidationResult();
                    this.validation.IsValid = true;

                    //set wizard to last step prior to create
                    aspWizard.SetActiveView(aspWizardView);
                    aspWizardSteps.SetActiveView(aspStep3);
                    aspWizardStepTitle.SetActiveView(aspStep4TitleView);
                    break;
            }

            if (this.validation.IsValid)
            {
                uxFormValidationText.Visible = false;
            }
            else
            {
                uxFormValidationText.Visible = true;
                uxFormValidationText.Text = this.validation.Message;
            }
        }

        protected void ValidateParam()
        {
            switch (aspWizardSteps.ActiveViewIndex)
            {
                case 0:
                    //validate hosted service credentials
                    this.validator = new SubscriptionValidator(uxSubscriptionId.Value);
                    this.validation = this.validator.Validate();
                    if (this.validation.IsValid)
                    {
                        this.validator = new HostedServiceValidator(uxSubscriptionId.Value, uxServiceName.Value, (DeploymentSlot)Enum.Parse(typeof(DeploymentSlot), aspSlots.SelectedValue, true));
                        this.validation = this.validator.Validate();
                    }
                    break;
                case 1:
                    //validate srtorage account
                    this.validator = new StorageValidator(uxSubscriptionId.Value, uxStorageAccount.Value);
                    this.validation = this.validator.Validate();

                    //validate caching xml
                    XmlDocument xml = new XmlDocument();
                    try
                    {
                        xml.LoadXml(aspCachingXml.Text);
                        this.validation.IsValid = true;
                    }
                    catch (XmlException)
                    {
                        try
                        {
                            string validateXml = "<ektron>" + aspCachingXml.Text + "</ektron>";
                            xml.LoadXml(validateXml);
                            this.validation.IsValid = true;
                        }
                        catch (XmlException)
                        {
                            aspCachingXml.Text = String.Empty;
                            this.validation.IsValid = false;
                            this.validation.Message = this.GetLocalResourceObject("CannotParseCachingXml").ToString();
                        }
                    }
                    break;
                case 2:
                    //sql database credentials
                    this.validator = new SqlAzureValidator(uxServer.Value, uxUsername.Value, uxPassword.Value, uxDatabase.Value, ddlDbOption.SelectedValue.ToLower() == "yes" ? true : false);
                    this.validation = validator.Validate();
                    break;
                case 3:
                    //package deployment
                    this.validation = new ValidationResult();
                    this.validation.IsValid = true;  //always has a valid value
                    break;
            }
        }

        protected void DeploymentPackage_Changed(object sender, EventArgs e)
        {
            if ((aspDeploymentPackageUploadOptions.SelectedValue == "New") || (aspDeploymentPackageUploadOptions.SelectedIndex == 0))
            {
                uxUploadExistingPackage.Visible = false;
            }
            else
            {
                uxUploadExistingPackage.Visible = true;
            }
        }

        #endregion
        public long Save()
        {
            long relationshipId = 0;
            try
            {
                CloudServiceRequest csrequest = null;
                csrequest = Ektron.Cloud.Common.CloudServiceHelper.CreateCloudServiceRequest(this.deploymentParams);

                CloudCommandType commandType = (CloudCommandType)Enum.Parse(typeof(CloudCommandType), this.deploymentParams["command"], true);
                EktronServiceProxy proxy = new EktronServiceProxy();
                switch (commandType)
                {
                    case CloudCommandType.CreateDeployment:
                        relationshipId = CreateRelationship(csrequest);
                        break;

                    default:
                        throw new Exception(string.Format(@"invalid switch:""{0}""", commandType));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return relationshipId;
        }
        long CreateRelationship(CloudServiceRequest csrequest)
        {
            CommonApi _siteApi = new SiteAPI();
            SyncHandlerController.CreateRelationshipResult result;
            SyncHandlerController _controller = new SyncHandlerController();
            ConnectionInfo LocalConnection = new ConnectionInfo(_siteApi.RequestInformationRef.ConnectionString);

            Relationship relationship = _controller.CreateCloudRelationship(
                LocalConnection.DatabaseName, LocalConnection.ServerName, _siteApi.RequestInformationRef.SitePath, -1,
                csrequest.WebSites[0].ConnectionString, "", csrequest.SubscriptionID,
                csrequest.StorageAccountID, csrequest.StorageAccountKey,
                csrequest.CloudServiceName, "", Ektron.FileSync.Common.SyncClientSyncDirection.Upload, out result);
            if (result != SyncHandlerController.CreateRelationshipResult.Success)
            {
                throw new Exception("Unable to create relationship. Make sure Ektron Windows Service is running with secure certificate installed.");
            }
            csrequest.RelationshipID = relationship.Id;
            Relationship.SetExternalArgs(relationship.Id, Ektron.DbSync.Core.Serializer.Serialize<Ektron.Cloud.Common.CloudServiceRequest>(csrequest));
            return relationship.Id;
        }
    }
}
