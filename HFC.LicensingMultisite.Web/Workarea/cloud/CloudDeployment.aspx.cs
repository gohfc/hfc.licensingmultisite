using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Linq.Expressions;
using System.Linq;
using Ektron.Cloud.Azure.ServiceManagement;
using Ektron.Cloud.Common;
using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.UI.StringExtensions;
using Ektron.Cms.Sync.Client;
using Ektron.Cms.Sync.Presenters;
using Ektron.Cms.Sync.Web;
using Ektron.Cms.Sync.Web.Parameters;
using Ektron.FileSync.Common;

public partial class Workarea_CloudDeployment : Ektron.Cms.Workarea.Page, ISyncRelationshipsListView
{
    private const string ProfileUrlFormat = "../sync/SyncProfile.aspx?server_type={0}&action={1}&id={2}&mode=cloud";
    private const string ProfileViewAction = "view";
    private const string AttributeRel = "rel";
    private const string AttributeClass = "class";
    private const string AttributeOnClick = "onclick";
    private CloudServiceRequest csrequest = null;
    private readonly SyncRelationshipsPresenter _presenter;
    private readonly CommonApi _commonApi;
    private readonly SiteAPI _siteApi;
    private readonly StyleHelper _styleHelper;

    private JavaScriptSerializer jsonSerializer;

    private RelationshipsParameters _parameters;

    private Ektron.Cloud.Amazon.Deployment.EC2.Compute compute;

    /// <summary>
    /// Constructor
    /// </summary>
    public Workarea_CloudDeployment()
    {
        _presenter = new SyncRelationshipsPresenter(this);
        _commonApi = new CommonApi();
        _siteApi = new SiteAPI();
        _styleHelper = new StyleHelper();
        this.jsonSerializer = new JavaScriptSerializer();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!_siteApi.EkContentRef.IsAllowed(0, 0, "users", "IsLoggedIn", 0))
        {
            Response.Redirect("../reterror.aspx?info=" + HttpUtility.UrlEncode(_siteApi.EkMsgRef.GetMessage("sync logged out message")));
        }

        if (!_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) &&
            !_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
        {
            Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
        }

        _parameters = new RelationshipsParameters(Request);

        RegisterResources();
    }

    /// <summary>
    /// Handles the Page's 'Load' event, initializing the view
    /// for display.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        _presenter.InitializeView(-1);
    }

    #region ISyncRelationshipsListView Members

    /// <summary>
    /// Gets or sets the collection of relationships to be displayed
    /// on this page.
    /// </summary>
    public List<Relationship> Relationships { get; set; }

    /// <summary>
    /// Binds the specified relationship data for display.
    /// </summary>
    public void Bind()
    {
        RenderHeader();

        if (Relationships != null)
        {
            List<Relationship> activeRelationships = new List<Relationship>();

            foreach (Relationship relationship in Relationships)
            {
                if (relationship.Status == ProfileStatus.Active &&
                    relationship.LocalSite.Address.ToLower() == Environment.MachineName.ToLower())
                {
                    activeRelationships.Add(relationship);
                }
            }

            if (activeRelationships.Count > 0)
            {
                var activeRelationshipsList = from element in activeRelationships
                                              orderby element.Name
                                              select element;

                rptRelationshipList.DataSource = activeRelationshipsList;
                rptRelationshipList.DataBind();
            }
            else
            {
                uxNoRelationshipsMessage.Visible = true;
            }
        }
    }

    #endregion

    #region ISyncView Members

    /// <summary>
    /// Displays the specified error message to the user.
    /// </summary>
    /// <param name="message"></param>
    public void DisplayError(string message)
    {
        Response.Write("**** " + message + " ****");
    }

    #endregion

    private void RenderHeader()
    {
        //divTitleBar.InnerHtml = _styleHelper.GetTitleBar("View All Cloud Deployment");
        divTitleBar.InnerHtml = _styleHelper.GetTitleBar(GetLocalResourceObject("divTitleBarText").ToString());

        bool primaryCssApplied = false;
        bool utilityDividerAdded = false;

        if (_presenter.IsValidVersion)
        {
            if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
            {
                // Add button
                HtmlTableCell cellAddButton = new HtmlTableCell();
                cellAddButton.InnerHtml = _styleHelper.GetButtonEventsWCaption(
                    _siteApi.AppPath + "images/ui/Icons/add.png",
                   "#" + "create hosted service".Replace(" ", ""),
                    GetLocalResourceObject("CreateHostedServiceText").ToString(),
                    GetLocalResourceObject("CreateHostedServiceText").ToString(),
                    String.Empty,
                    StyleHelper.MergeCssClasses(new string[] { "CreateHostedService", StyleHelper.AddButtonCssClass }),
                    !primaryCssApplied);

                primaryCssApplied = true;

                rowToolbarButtons.Cells.Add(cellAddButton);

                rowToolbarButtons.Cells.Add(StyleHelper.ActionBarDividerCell);
                utilityDividerAdded = true;

            }

            // Log Button
            HtmlTableCell cellLogButton = new HtmlTableCell();
            cellLogButton.InnerHtml = _styleHelper.GetButtonEventsWCaption(
                _siteApi.AppPath + "images/ui/icons/chartBar.png",
                "../sync/SyncHistory.aspx?referrer=Sync.aspx",
                _siteApi.EkMsgRef.GetMessage("sync log history"),
                _siteApi.EkMsgRef.GetMessage("sync log history"),
                string.Empty,
                StyleHelper.LogHistoryButtonCssClass,
                !primaryCssApplied);

            primaryCssApplied = true;

            rowToolbarButtons.Cells.Add(cellLogButton);
        }

        if (_presenter.EnableSyncConflictResolution)
        {
            if (!utilityDividerAdded)
            {
                rowToolbarButtons.Cells.Add(StyleHelper.ActionBarDividerCell);
                utilityDividerAdded = true;
            }

            // Sync Conflict Resolution
            HtmlTableCell cellConflictButton = new HtmlTableCell();
            cellConflictButton.InnerHtml = _styleHelper.GetButtonEventsWCaption(
                _siteApi.AppImgPath + "id_card_force.png",
                "#ResolveSynchronizationCollisions",
                "Resolve Synchronization Collisions",
                "Resolve Synchronization Collisions",
                String.Empty,
                StyleHelper.MergeCssClasses(new string[] { "launchResolveSyncCollisionsButton", StyleHelper.ResolveConflictButtonCssClass }),
                !primaryCssApplied);

            primaryCssApplied = true;

            rowToolbarButtons.Cells.Add(cellConflictButton);
        }

        if (_presenter.EnableSyncConflictReview)
        {
            if (!utilityDividerAdded)
            {
                rowToolbarButtons.Cells.Add(StyleHelper.ActionBarDividerCell);
                utilityDividerAdded = true;
            }

            // Sync Review
            HtmlTableCell cellReviewButton = new HtmlTableCell();
            cellReviewButton.InnerHtml = _styleHelper.GetButtonEventsWCaption(
                _siteApi.AppImgPath + "id_card_warning_22x22.gif",
                "../sync/SyncReview.aspx",
                _siteApi.EkMsgRef.GetMessage("lbl sync review resolved conflicts"),
                _siteApi.EkMsgRef.GetMessage("lbl sync review resolved conflicts"),
                String.Empty,
                StyleHelper.MergeCssClasses(new string[] { "reviewConflictsButton", StyleHelper.ReviewConflictButtonCssClass }),
                !primaryCssApplied);

            primaryCssApplied = true;

            rowToolbarButtons.Cells.Add(cellReviewButton);
        }

        if (_presenter.isCustomConfigAvailable())
        {
            // Sync Custom Config
            HtmlTableCell cellCustomConfigButton = new HtmlTableCell();
            cellCustomConfigButton.InnerHtml = _styleHelper.GetButtonEventsWCaption(
                _siteApi.AppPath + "images/ui/Icons/wrenchOrange.png",
                "../sync/SyncCustomConfig.aspx",
                _siteApi.EkMsgRef.GetMessage("synclblmanagecustomconfig"),
                _siteApi.EkMsgRef.GetMessage("synclblmanagecustomconfig"),
                string.Empty);

            rowToolbarButtons.Cells.Add(cellCustomConfigButton);
        }

        rowToolbarButtons.Cells.Add(StyleHelper.ActionBarDividerCell);

        // Help Button
        HtmlTableCell cellHelpButton = new HtmlTableCell();
        cellHelpButton.InnerHtml = _styleHelper.GetHelpButton("view_azure_host", string.Empty);

        rowToolbarButtons.Cells.Add(cellHelpButton);

        // Hosted service List filter
        //Display the dropdown only if both Amazon and Azure service exist.
        Relationship amazonRelationShipObj = Relationships.Find(delegate(Relationship rel) { return rel.Type == ProfileType.Amazon; });
        Relationship azureRelationShipObj = Relationships.Find(delegate(Relationship rel) { return rel.Type == ProfileType.Azure; });
        if (amazonRelationShipObj != null && azureRelationShipObj != null)
        {
            if (amazonRelationShipObj.Profiles.Count > 0 && azureRelationShipObj.Profiles.Count > 0)
            {
                aspSvFilter.Visible = true;

                if (!(aspSvFilter.Items.Count > 1))
                {
                    aspSvFilter.Items.Insert(0, new ListItem(GetLocalResourceObject("ShowAll").ToString()));
                    aspSvFilter.AppendDataBoundItems = true;
                    var profileList = System.Enum.GetNames(typeof(ProfileType));
                    IEnumerable<String> sortProfileType = from item in profileList
                                                          orderby item
                                                          select item;
                    aspSvFilter.DataSource = sortProfileType;
                    aspSvFilter.DataBind();
                }
            }
        }
    }

    /// <summary>
    /// Handle the ItemDataBound event for the repeater control responsible for rendering
    /// relationship data on the page.
    /// </summary>
    /// <remarks>
    /// When a relationship is bound, another child repeater will be populated with the 
    /// relationship's profiles.
    /// </remarks>
    /// <param name="source">Repeater</param>
    /// <param name="e">Repeater arguments</param>
    protected void HandleRelationshipListItemDataBound(object source, RepeaterItemEventArgs e)
    {
        Relationship relationship = null;
        bool cloudServiceIsAvailable = false;
        string svcDeploymentStatus = string.Empty;
        int numofcurrentInstances = 1;
        try
        {
            relationship = e.Item.DataItem as Relationship;
            if (relationship != null && relationship.Status != ProfileStatus.Deleted)
            {
                RelationshipPresentationServices presentationServices =
                    new RelationshipPresentationServices(relationship);

                CloudServiceRequest svcArgumentObject = Ektron.DbSync.Core.Serializer.Deserialize<Ektron.Cloud.Common.CloudServiceRequest>(relationship.ExternalArgs);

                // Relationship Labels
                if (relationship.Type != ProfileType.Amazon)
                {
                    PopulateTextControl(e.Item.FindControl("labelSubscriptionId"), GetLocalResourceObject("SubscriptionID").ToString());
                    PopulateTextControl(e.Item.FindControl("subscriptionId"), relationship.RemoteSite.SitePath);
                }
                PopulateTextControl(e.Item.FindControl("labelWebInstances"), GetLocalResourceObject("WebInstances").ToString());
                PopulateTextControl(e.Item.FindControl("labelLocalSitePath"), GetLocalResourceObject("LocalSitePath").ToString());
                PopulateTextControl(e.Item.FindControl("labelSqlDatabaseName"), GetLocalResourceObject("SqlDatabaseName").ToString());
                PopulateTextControl(e.Item.FindControl("labelCloudDatabaseName"), GetLocalResourceObject("CloudDatabaseName").ToString());
                PopulateTextControl(e.Item.FindControl("labelRemoteSiteUrl"), GetLocalResourceObject("RemoteSiteUrl").ToString());

                switch (relationship.Type)
                {
                    case ProfileType.Amazon:
                        string accessKey = svcArgumentObject.ServiceArguments.AccessKey.ToString();
                        string secretKey = svcArgumentObject.ServiceArguments.SecretKey.ToString();
                        string location = svcArgumentObject.ServiceArguments.ComputeArguments.Location;

                        compute = new Ektron.Cloud.Amazon.Deployment.EC2.Compute(accessKey, secretKey, location);

                        string amazonSqlDBName = !string.IsNullOrEmpty(svcArgumentObject.ServiceArguments.DatabaseArguments.EndPoint) ? svcArgumentObject.ServiceArguments.DatabaseArguments.EndPoint.ToString() : GetLocalResourceObject("DeploymentPending").ToString();
                        PopulateTextControl(e.Item.FindControl("sqlDatabaseName"), amazonSqlDBName);
                        PopulateTextControl(e.Item.FindControl("cloudDatabaseName"), svcArgumentObject.ServiceArguments.DatabaseArguments.DatabaseName.ToString());
                        PopulateTextControl(e.Item.FindControl("webInstances"), svcArgumentObject.ServiceArguments.ComputeArguments.InstanceCount.ToString());
                        numofcurrentInstances = svcArgumentObject.ServiceArguments.ComputeArguments.InstanceCount;
                        this.uxCloudDeploymentDialogs.CloudProfileType = ProfileType.Amazon;
                        break;
                    case ProfileType.Azure:
                        PopulateTextControl(e.Item.FindControl("sqlDatabaseName"), relationship.RemoteSite.Connection.ServerName);
                        PopulateTextControl(e.Item.FindControl("cloudDatabaseName"), relationship.RemoteSite.Connection.DatabaseName);
                        PopulateTextControl(e.Item.FindControl("webInstances"), svcArgumentObject.InstanceCount.ToString());
                        e.Item.FindControl("labelRemoteSiteUrl").Visible = false;
                        e.Item.FindControl("litRemoteSiteUrl").Visible = false;
                        numofcurrentInstances = svcArgumentObject.InstanceCount;
                        this.uxCloudDeploymentDialogs.CloudProfileType = ProfileType.Azure;
                        break;
                }

                string svcName = relationship.Type == ProfileType.Amazon ? svcArgumentObject.ServiceArguments.ComputeArguments.Name.ToString() : relationship.RemoteSite.Address.Replace(".cloudapp.net", "").ToString();
                PopulateTextControl(e.Item.FindControl("aspHostedService"), svcName);
                PopulateTextControl(e.Item.FindControl("localSitePath"), relationship.LocalSite.SitePath);

                // Profile List Headers
                PopulateTextControl(e.Item.FindControl("litProfileHeader"), GetLocalResourceObject("Profile").ToString());
                PopulateTextControl(e.Item.FindControl("litProfileIdHeader"), GetLocalResourceObject("ProfileId").ToString());
                PopulateTextControl(e.Item.FindControl("litScheduleHeader"), GetLocalResourceObject("Schedule").ToString());
                PopulateTextControl(e.Item.FindControl("litLastRunTimeHeader"), GetLocalResourceObject("LastFullSync").ToString());
                PopulateTextControl(e.Item.FindControl("litButtonsHeader"), GetLocalResourceObject("Actions").ToString());
                PopulateTextControl(e.Item.FindControl("litLastRunResultHeader"), GetLocalResourceObject("LastRunResult").ToString());

                // Relationship Buttons
                HtmlGenericControl divRelationshipButtons =
                    e.Item.FindControl("divRelationshipButtons") as HtmlGenericControl;

                if (divRelationshipButtons != null)
                {
                    if (presentationServices.AllowInitialSync &&
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                    {
                        //Deploy Site to the Cloud button
                        HtmlAnchor linkSyncButton = new HtmlAnchor();
                        string linkSyncButtonOnClick = relationship.Type == ProfileType.Azure ? "Ektron.Workarea.Sync.Relationships.InitialSynchronizeCloud(this)" : "Ektron.Workarea.Cloud.Dialogs.AmazonServiceWizard.alertDeploy(this);";
                        linkSyncButton.HRef = "#";
                        linkSyncButton.Title = GetLocalResourceObject("DeploySiteToCloudButtonText").ToString();// "Deploy to the Cloud";
                        linkSyncButton.Attributes.Add(AttributeRel, relationship.Id.ToString());
                        linkSyncButton.Attributes.Add(AttributeClass, "deployToCloud");
                        linkSyncButton.Attributes.Add(AttributeOnClick, linkSyncButtonOnClick);

                        divRelationshipButtons.Controls.Add(linkSyncButton);
                    }

                    HtmlGenericControl dvRelationShipLogo = e.Item.FindControl("serviceLogo") as HtmlGenericControl;
                    if (dvRelationShipLogo != null)
                    {
                        HtmlImage imgServiceLogo = new HtmlImage();
                        string imgPath = _siteApi.AppPath + "cloud/images/";
                        imgServiceLogo.Src = relationship.Type == ProfileType.Amazon ? imgPath + "Amazon_logo.jpg" : imgPath + "windows_azure_logo.jpg";
                        imgServiceLogo.Alt = relationship.Type == ProfileType.Amazon ? GetLocalResourceObject("AmazonWebService").ToString() : GetLocalResourceObject("WindowsAzureService").ToString();
                        dvRelationShipLogo.Controls.Add(imgServiceLogo);
                    }
                    Dictionary<string, string> actionButtonJsonData = new Dictionary<string, string>();
                    try
                    {
                        bool enableCloudCommand = relationship.Description.ToLower() == "completed" ? true : false;
                        string cloudhost = relationship.Type == ProfileType.Amazon ? svcName : relationship.RemoteSite.Address.Replace(".cloudapp.net", "");
                        csrequest = Serializer.Deserialize<Ektron.Cloud.Common.CloudServiceRequest>(relationship.ExternalArgs);
                        string subscriptionid = csrequest.SubscriptionID;// RemoteSite.SitePath;
                        string storageAccount = csrequest.StorageAccountID;
                        Ektron.Cloud.Common.CloudServiceRequest apiRequest = new Ektron.Cloud.Common.CloudServiceRequest();
                        apiRequest.SubscriptionID = subscriptionid;
                        apiRequest.CloudServiceName = cloudhost;

                        // add minimal data regardless of whether or not it has been deployed
                        actionButtonJsonData.Add("service-name", cloudhost);
                        actionButtonJsonData.Add("storage-account", storageAccount);
                        actionButtonJsonData.Add("subscription-id", subscriptionid);
                        actionButtonJsonData.Add("relationship-id", relationship.Id.ToString());
                        actionButtonJsonData.Add("commandText", "");
                        actionButtonJsonData.Add("service-type", relationship.Type.ToString());
                        actionButtonJsonData.Add("number-of-instances", numofcurrentInstances.ToString());

                        if (relationship.Type == ProfileType.Amazon)
                        {
                            Ektron.Cloud.Amazon.Deployment.EC2.InstanceStatus deploymentStatus = compute.GetHostedServiceStatus(svcArgumentObject.ServiceArguments);
                            svcDeploymentStatus = deploymentStatus.ToString();
                            string stackname = svcArgumentObject.ServiceArguments.ComputeArguments.Name;
                            string remoteMachineName = string.Empty;
                            if (svcDeploymentStatus.ToLower() != "stopped")
                            {
                                try
                                {
                                    remoteMachineName = compute.GetPublicDnsForLoadBalancedSites(stackname);
                                }
                                catch (Exception exc)
                                {
                                    EkException.LogException(exc);
                                }
                            }

                            if (!string.IsNullOrEmpty(remoteMachineName))
                            {
                                PopulateTextControl(e.Item.FindControl("litRemoteSiteUrl"), remoteMachineName);
                            }
                            else
                            {
                                PopulateTextControl(e.Item.FindControl("litRemoteSiteUrl"), GetLocalResourceObject("DeploymentPending").ToString());
                            }
                        }
                        else if (relationship.Type == ProfileType.Azure)
                        {
                            if (enableCloudCommand)
                            {
                                HostedServiceManager hostMgr = new HostedServiceManager(apiRequest);
                                HostedService result = hostMgr.GetProperties();
                                if (result != null)
                                {
                                    svcDeploymentStatus = result.Deployments[0].Status.ToLower();
                                }
                                bool swap = false;
                                Deployment swapslot = result.Deployments.Find(p => p.DeploymentSlot == "staging");
                                if (swapslot != null)
                                {
                                    swap = true;
                                }
                                if (swap)
                                {
                                    //Swap Instance button
                                    HtmlAnchor swapDeploy = new HtmlAnchor();
                                    swapDeploy.HRef = "#";
                                    swapDeploy.Title = GetLocalResourceObject("SwapInstance").ToString(); //Swap Instance (Staging<->Production)
                                    swapDeploy.Attributes.Add(AttributeClass, "swapInstance ");
                                    swapDeploy.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Cloud.Dialogs.SwapDeployment.open('" + this.jsonSerializer.Serialize(actionButtonJsonData) + "');return false;");
                                    divRelationshipButtons.Controls.Add(swapDeploy);
                                }
                            }
                        }
                        ////if (enableCloudCommand)
                        // If enableCloudCommand = true, it need not necessarily mean that the deployment was successful, so changing the condition
                        if (!presentationServices.AllowInitialSync)
                        {
                            //Edit Web Instance Button
                            HtmlAnchor changeConfig = new HtmlAnchor();
                            changeConfig.HRef = "#";
                            changeConfig.Title = GetLocalResourceObject("EditWebInstancesButtonText").ToString();// "Configure Instance";
                            changeConfig.Attributes.Add(AttributeClass, "configureInstance");
                            actionButtonJsonData["commandText"] = changeConfig.Title;
                            changeConfig.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Cloud.Dialogs.ConfigureInstance.open('" + this.jsonSerializer.Serialize(actionButtonJsonData) + "'); return false;");
                            divRelationshipButtons.Controls.Add(changeConfig);
                        }
                        // Schedule Cloud Events Button
                        HtmlAnchor scheduleDeployment = new HtmlAnchor();
                        scheduleDeployment.HRef = "./CloudScheduledEvents.aspx?profile-id=" + relationship.DefaultProfile.Id.ToString();
                        scheduleDeployment.Title = GetLocalResourceObject("ScheduleCloudEventsButtonText").ToString();//Schedule Deployment";
                        scheduleDeployment.Attributes.Add(AttributeClass, "scheduleDeployment");
                        divRelationshipButtons.Controls.Add(scheduleDeployment);

                        HtmlAnchor deployStatus = new HtmlAnchor();
                        deployStatus.HRef = "#";
                        if (enableCloudCommand)
                        {
                            if (svcDeploymentStatus == Ektron.Cloud.Amazon.Deployment.EC2.InstanceStatus.Running.ToString() || svcDeploymentStatus == "running")
                            {
                                cloudServiceIsAvailable = true;
                                deployStatus.Title = GetLocalResourceObject("StopSiteButtonText").ToString();// "Stop Instance";
                                actionButtonJsonData["commandText"] = deployStatus.Title;
                                deployStatus.Attributes.Add(AttributeClass, "stopInstance");
                                actionButtonJsonData.Add("status", "Suspended");
                                deployStatus.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Cloud.Dialogs.StopSite.open('" + this.jsonSerializer.Serialize(actionButtonJsonData) + "');return false;");
                            }
                            else
                            {
                                deployStatus.Title = GetLocalResourceObject("StartSiteButtonText").ToString();// "Start Instance";
                                actionButtonJsonData["commandText"] = deployStatus.Title;
                                deployStatus.Attributes.Add(AttributeClass, "startInstance");
                                actionButtonJsonData.Add("status", "Running");
                                deployStatus.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Cloud.Dialogs.StartSite.open('" + this.jsonSerializer.Serialize(actionButtonJsonData) + "');return false;");
                            }
                            actionButtonJsonData["commandText"] = deployStatus.Title;
                            divRelationshipButtons.Controls.Add(deployStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    if (presentationServices.AllowGetStatus)
                    {
                        HtmlAnchor linkStatusButton = new HtmlAnchor();
                        linkStatusButton.HRef = "#";
                        linkStatusButton.Title = GetLocalResourceObject("GetStatus").ToString();
                        linkStatusButton.Attributes.Add(AttributeRel, relationship.Id.ToString());
                        linkStatusButton.Attributes.Add(AttributeClass, "statusButton");
                        linkStatusButton.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Sync.Relationships.ShowSyncStatus(this);");

                        divRelationshipButtons.Controls.Add(linkStatusButton);
                    }

                    // File restoration is disabled until backend support is implemented.

                    if (presentationServices.AllowDelete && _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                    {
                        //Delete Hosted Service button
                        HtmlAnchor linkDeleteButton = new HtmlAnchor();
                        linkDeleteButton.HRef = "#";
                        linkDeleteButton.Title = GetLocalResourceObject("DeleteHostedServiceButtonText").ToString();// "Delete";
                        linkDeleteButton.Attributes.Add(AttributeRel, relationship.Id.ToString());
                        linkDeleteButton.Attributes.Add(AttributeClass, "deleteButton");

                        actionButtonJsonData.Add("server-type", "cloud");
                        actionButtonJsonData["commandText"] = linkDeleteButton.Title;

                        if (relationship.Type == ProfileType.Amazon)
                            actionButtonJsonData["rds-server"] = svcArgumentObject.ServiceArguments.DatabaseArguments.CreateServer.ToString();

                        linkDeleteButton.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Cloud.Dialogs.DeleteRelationship.open('" + this.jsonSerializer.Serialize(actionButtonJsonData) + "');return false;");
                        divRelationshipButtons.Controls.Add(linkDeleteButton);
                    }
                    if (presentationServices.AllowAddProfile && _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                    {
                        //Profile Settings Button
                        //HtmlAnchor profileSettings = new HtmlAnchor();
                        //profileSettings.HRef = "#";
                        //profileSettings.Title = GetLocalResourceObject("ProfileSettingsButtonText").ToString();
                        //profileSettings.Attributes.Add(AttributeClass, "profileSettings");
                        //profileSettings.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Cloud.Dialogs.ProfileSettings.open('" + this.jsonSerializer.Serialize(actionButtonJsonData) + "'); return false;");

                        //divRelationshipButtons.Controls.Add(profileSettings);

                        //Add Profile button
                        HtmlAnchor linkAddButton = new HtmlAnchor();
                        linkAddButton.HRef = "../sync/SyncProfile.aspx?mode=cloud&server_type=" + (relationship.Type == ProfileType.Amazon ? 2 : 1) + "&action=add&id=" + relationship.Id.ToString();
                        linkAddButton.Title = "Add Profile";
                        linkAddButton.Attributes.Add(AttributeClass, "addButton");

                        divRelationshipButtons.Controls.Add(linkAddButton);
                    }
                }

                // Relationship Information
                HtmlGenericControl liRelationship = e.Item.FindControl("liRelationship") as HtmlGenericControl;
                if (liRelationship != null)
                {
                    string classes = string.Empty;
                    string originalClasses = liRelationship.Attributes[AttributeClass];
                    if (!string.IsNullOrEmpty(originalClasses))
                    {
                        liRelationship.Attributes.Remove(AttributeClass);
                        classes = originalClasses + " ";
                    }

                    switch (relationship.DefaultProfile.Direction)
                    {
                        case SyncClientSyncDirection.Download:
                            classes += "download";
                            break;
                        case SyncClientSyncDirection.Upload:
                            classes += "upload";
                            break;
                    }

                    liRelationship.Attributes.Add(AttributeClass, classes);
                }

                if (relationship.Profiles.Count > 1)
                {
                    // If child profiles exist (other than the default relationship
                    // profile), bind them for display as well.
                    Repeater rptProfileList = e.Item.FindControl("rptProfileList") as Repeater;
                    if (rptProfileList != null)
                    {
                        rptProfileList.DataSource = relationship.Profiles;
                        rptProfileList.DataBind();
                    }
                }
                else
                {
                    // No child profiles exist -- hide the the current list item.
                    HtmlGenericControl profileListItem = e.Item.FindControl("profilesListItem") as HtmlGenericControl;
                    if (profileListItem != null)
                    {
                        profileListItem.Visible = false;
                    }
                }
            }
            else
            {
                // Hide list item for deleted relationships.

                e.Item.Visible = false;
            }
        }
        catch (EndpointNotFoundException)
        {
            Utilities.ShowError(string.Format("Error while processing relation id:{0}", relationship != null ? relationship.Id.ToString() : ""));
        }
    }

    /// <summary>
    /// Handles the ItemDataBound event for the repeater control responsible for rendering
    /// a relationship's profile data on the page.
    /// </summary>
    /// <remarks>
    /// The default profile for a relationship is intentionally hidden.
    /// </remarks>
    /// <param name="source">Repeater</param>
    /// <param name="e">Repeater arguments</param>
    protected void HandleProfileListItemDataBound(object source, RepeaterItemEventArgs e)
    {
        try
        {
            Profile profile = e.Item.DataItem as Profile;
            if (profile != null)
            {
                ProfilePresentationServices profileServices = new ProfilePresentationServices(profile);

                if (profileServices.DisplayProfile)
                {
                    // Render the link to access profile details.
                    HtmlAnchor linkProfileName = e.Item.FindControl("linkProfileName") as HtmlAnchor;
                    if (linkProfileName != null)
                    {
                        string profileLinkHRef = "";
                        profileLinkHRef = string.Format(ProfileUrlFormat, (profile.Type == ProfileType.Amazon ? "2" : "1"), ProfileViewAction, profile.Id);

                        linkProfileName.HRef = profileLinkHRef;
                        linkProfileName.InnerText = profile.Name;

                        string itemsList = string.Empty;
                        if (profile.SynchronizeDatabase)
                        {
                            itemsList = itemsList + "Database, ";
                        }
                        if (profile.SynchronizeTemplates)
                        {
                            itemsList = itemsList + "Templates, ";
                        }
                        if (profile.SynchronizeWorkarea)
                        {
                            itemsList = itemsList + "Workarea, ";
                        }
                        if (profile.SynchronizeBinaries)
                        {
                            itemsList = itemsList + "Bin, ";
                        }
                        if (itemsList.Length > 0)
                        {
                            itemsList = itemsList.Substring(0, itemsList.Length - 2);
                        }

                        linkProfileName.Title = String.Format("Direction: {0}. Including Items: {1}", profile.Direction.ToString(), itemsList);
                    }
                    PopulateTextControl(
                       e.Item.FindControl("litProfileId"), profile.Id.ToString());
                    // Populate the schedule and run time fields.
                    PopulateTextControl(
                        e.Item.FindControl("litSchedule"),
                        PresentationHelper.GetScheduleIntervalString(profile.Schedule.Recurrence));

                    // Don't display the next run time when nothing has been
                    // scheduled.
                    if (profile.Schedule.Recurrence != ScheduleInterval.None)
                    {
                        PopulateTextControl(
                            e.Item.FindControl("litNextRunTime"),
                            "Next Sync Time: " + FormatDateTime(profile.Schedule.NextRunTime));
                    }

                    if (profile.LastRunResult != SyncResult.None)
                    {
                        if (profile.LastRunResult == SyncResult.Success)
                        {
                            PopulateTextControl(
                                e.Item.FindControl("litLastRunResult"),
                                "Success");
                        }
                        else
                        {
                            EwsExceptionParser exceptionParser = new EwsExceptionParser();
                            PopulateTextControl(
                                e.Item.FindControl("litLastRunResult"),
                                exceptionParser.Translate(profile.LastRunMessage, SyncHandlerAction.GetStatus).ToString().FormatUrls("<span class='ektron-ui-forceWrap'>{0}</span>"));
                        }
                    }

                    // Don't display the last run time if the profile
                    // has not yet been executed.
                    if (profile.LastFullSync > DateTime.MinValue)
                    {
                        PopulateTextControl(
                            e.Item.FindControl("litLastRunTime"),
                            FormatDateTime(profile.LastFullSync));
                    }

                    // Render the action buttons for the profile.
                    HtmlGenericControl divProfileButtons =
                        e.Item.FindControl("divProfileButtons") as HtmlGenericControl;

                    if (divProfileButtons != null)
                    {
                        // Add 'Sync' button.
                        if (profileServices.AllowSync)
                        {
                            HtmlAnchor linkSyncButton = new HtmlAnchor();
                            linkSyncButton.HRef = "#";
                            linkSyncButton.Title = "Sync";
                            linkSyncButton.Attributes.Add(AttributeClass, "syncButton");
                            linkSyncButton.Attributes.Add(AttributeRel, profile.Id.ToString());
                            linkSyncButton.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Sync.Relationships.Synchronize(this, true, '" + _siteApi.EkMsgRef.GetMessage("lbl sync confirm synchronization") + "');");

                            divProfileButtons.Controls.Add(linkSyncButton);
                        }

                        // Add 'Get Status' button.
                        if (profileServices.AllowStatusRetrieval)
                        {
                            HtmlAnchor linkStatusButton = new HtmlAnchor();
                            linkStatusButton.HRef = "#";
                            linkStatusButton.Title = "Get Status";
                            linkStatusButton.Attributes.Add(AttributeClass, "statusButton");
                            linkStatusButton.Attributes.Add(AttributeRel, profile.Id.ToString());
                            linkStatusButton.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Sync.Relationships.ShowSyncStatus(this);");

                            divProfileButtons.Controls.Add(linkStatusButton);
                        }

                        // Add 'Sync Preview' button.
                        //syncpreview disabled because blob preview is not ready.
                        //if (profileServices.AllowSyncPreview)
                        //{
                        //    HtmlAnchor linkPreviewButton = new HtmlAnchor();
                        //    linkPreviewButton.HRef = "SyncPreview.aspx?id=" + profile.Id.ToString();
                        //    linkPreviewButton.Title = "Sync Preview";
                        //    linkPreviewButton.Attributes.Add(AttributeClass, "previewButton");
                        //    linkPreviewButton.Attributes.Add(AttributeRel, profile.Id.ToString());

                        //    divProfileButtons.Controls.Add(linkPreviewButton);
                        //}

                        // Add 'Pause/Resume' button.
                        if (profileServices.AllowToggleSchedule &&
                            _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                        {
                            HtmlAnchor linkToggleScheduleButton = new HtmlAnchor();
                            linkToggleScheduleButton.HRef = "#";
                            linkToggleScheduleButton.Title = "Pause/Resume";
                            linkToggleScheduleButton.Attributes.Add(AttributeRel, profile.Id.ToString());
                            linkToggleScheduleButton.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Sync.Relationships.ToggleScheduleStatus(this);");

                            // Display the appropriate button (play/pause) according
                            // to the current status of the profile.
                            if (profile.Status == ProfileStatus.Active)
                            {
                                linkToggleScheduleButton.Attributes.Add(AttributeClass, "toggleScheduleButton pause");
                            }
                            else
                            {
                                linkToggleScheduleButton.Attributes.Add(AttributeClass, "toggleScheduleButton resume");
                            }

                            divProfileButtons.Controls.Add(linkToggleScheduleButton);
                        }

                        // Add 'Delete' button.
                        if (profileServices.AllowDelete &&
                            _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                        {
                            HtmlAnchor linkDeleteButton = new HtmlAnchor();
                            linkDeleteButton.HRef = "#";
                            linkDeleteButton.Title = GetLocalResourceObject("DeleteProfileButtonText").ToString();// "Delete";
                            linkDeleteButton.Attributes.Add(AttributeClass, "deleteButton");
                            linkDeleteButton.Attributes.Add(AttributeRel, profile.Id.ToString());
                            linkDeleteButton.Attributes.Add(AttributeOnClick, "Ektron.Workarea.Sync.Relationships.DeleteCloudProfile(this);");

                            divProfileButtons.Controls.Add(linkDeleteButton);
                        }
                    }
                }
                else
                {
                    // Do not render the default profile or deleted 
                    // profile entries.
                    e.Item.Visible = false;
                }
            }
        }
        catch (EndpointNotFoundException)
        {
            Utilities.ShowError(SiteAPI.Current.EkMsgRef.GetMessage("windows service not running"));
        }
    }

    protected void aspSvFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Relationships != null)
        {
            List<Relationship> activeRelationships = new List<Relationship>();

            foreach (Relationship relationship in Relationships)
            {
                if (relationship.Type.ToString() == aspSvFilter.SelectedValue && relationship.Status == ProfileStatus.Active &&
                    relationship.LocalSite.Address.ToLower() == Environment.MachineName.ToLower())
                    activeRelationships.Add(relationship);
                else if (aspSvFilter.SelectedIndex == 0 && relationship.Status == ProfileStatus.Active &&
                    relationship.LocalSite.Address.ToLower() == Environment.MachineName.ToLower())
                    activeRelationships.Add(relationship);
            }

            if (activeRelationships.Count > 0)
            {
                uxNoRelationshipsMessage.Visible = false;
                var activeRelationshipsList = from element in activeRelationships
                                              orderby element.Name
                                              select element;
                rptRelationshipList.DataSource = activeRelationshipsList;
                rptRelationshipList.DataBind();
                rptRelationshipList.Visible = true;

            }
            else
            {
                uxNoRelationshipsMessage.Visible = true;
                rptRelationshipList.Visible = false;
            }
        }
    }
    /// <summary>
    /// Formats the specified DateTime value for display on the page.
    /// </summary>
    /// <param name="dateTime">DateTime value to format</param>
    /// <returns>Formatted DateTime string</returns>
    private string FormatDateTime(DateTime dateTime)
    {
        string formattedDateTime = string.Empty;
        if (dateTime.ToString() == DateTime.MaxValue.ToString())
        {
            formattedDateTime = "None";
        }
        else
        {
            formattedDateTime = dateTime.ToString();
        }

        return formattedDateTime;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="textControl"></param>
    /// <param name="text"></param>
    private void PopulateTextControl(object control, string text)
    {
        ITextControl textControl = control as ITextControl;
        if (textControl != null)
        {
            textControl.Text = text;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void RegisterResources()
    {
        // JavaScript Resources

        JS.RegisterJS(this, JS.ManagedScript.EktronJS, false);
        JS.RegisterJS(this, JS.ManagedScript.EktronModalJS, false);
        JS.RegisterJS(this, JS.ManagedScript.EktronXmlJS, false);
        JS.RegisterJS(this, JS.ManagedScript.EktronCookieJS, false);
        JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronStringJS, false);
        JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronScrollToJS, false);
        JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronUICoreJS, false);
        JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronUITabsJS, false);
        JS.RegisterJS(this, JS.ManagedScript.EktronJFunctJS, false);

        Ektron.Cms.API.JS.RegisterJS(this, _commonApi.AppPath + "wamenu/includes/com.ektron.ui.menu.js", "EktronComUIMenuJS");

        JS.RegisterJS(this, "../sync/js/Ektron.Workarea.Sync.Relationships.js", "SyncRelationshipsJS", false);
        JS.RegisterJS(this, "../java/internCalendarDisplayFuncs.js", "EktronIntercalendarDisplayFuncs", false);
        JS.RegisterJS(this, "../java/toolbar_roll.js", "EktronToolbarRollJS", false);

        // CSS Resources

        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronWorkareaCss, false);
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronWorkareaIeCss, false);
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronUITabsCss, false);
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronFixedPositionToolbarCss, false);
        Css.RegisterCss(this, Css.ManagedStyleSheet.EktronThickBoxCss, false);
        Css.RegisterCss(this, "../sync/css/ektron.workarea.sync.ie.css", "EktronWorkareaSyncIeCss", Css.BrowserTarget.LessThanEqualToIE7, false);
        Ektron.Cms.Framework.UI.Css.Create("css/ektron.workarea.azure.relationships.css").Register(this);

        // Style Helper Client Script

        ektronClientScript.Text = _styleHelper.GetClientScript();
    }
}