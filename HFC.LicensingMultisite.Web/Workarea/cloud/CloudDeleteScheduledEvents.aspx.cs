﻿namespace Ektron.Workarea.Cloud.Deployment
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using Ektron.Cloud.Common;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.UI;

    public partial class DeleteScheduledEvents : System.Web.UI.Page
    {
        private long ProfileId = 0;
        //private bool dataBind = true;
        private CloudServiceRequest viewData;
        private SiteAPI _siteApi;
        #region properties

        protected CloudServiceRequest ViewData
        {
            get
            {
                if (this.viewData == null)
                {
                    viewData = Ektron.Cloud.Common.Serializer.Deserialize<CloudServiceRequest>(Ektron.Cms.Sync.Client.Relationship.GetExternalArgs(ProfileId));
                }
                return this.viewData;
            }
        }

        #endregion

        #region events

        protected override void OnInit(EventArgs e)
        {
            if (!Utilities.ValidateUserLogin())
            {
                return;
            }
            _siteApi = new SiteAPI();
            if (!_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) &&
                !_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
            {
                Response.Redirect(_siteApi.AppPath + "login.aspx?fromLnkPg=1", true);
            }
            Packages.Ektron.Namespace.Register(this);
            Packages.Ektron.CssFrameworkBase.Register(this);
            Packages.Ektron.Workarea.Core.Register(this);
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            uxDialog.Visible = false;
            long.TryParse(Request.QueryString["profile-id"], out ProfileId);
            base.OnLoad(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            aspViewScheduledCloudEvents.DataSource = this.ViewData.DeploymentSchedules;
            aspViewScheduledCloudEvents.DataBind();
            base.OnPreRender(e);
        }

        protected override void OnError(EventArgs e)
        {
            uxDialog.Visible = true;
            uxDialogMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
            uxDialogMessage.Text = Server.GetLastError().Message;
            uxDeleteButton.Visible = false;
            base.OnError(e);
        }

        protected void Insert(object sender, ListViewInsertEventArgs e)
        {
            try
            {

                if (ViewData.DeploymentSchedules == null && ViewData.DeploymentSchedules.Count == 0)
                {
                    ViewData.DeploymentSchedules = new List<CloudProvisioningSchedule>();
                }
                CloudProvisioningSchedule item = new CloudProvisioningSchedule();
                item.Id = Guid.NewGuid();
                item.Name = ((Ektron.Cms.Framework.UI.Controls.EktronUI.TextField)e.Item.FindControl("uxInsertName")).Value;
                string operation = ((DropDownList)e.Item.FindControl("aspInsertOperation")).SelectedValue;
                item.Operation = (CloudCommandType)(Enum.Parse(typeof(CloudCommandType), operation));
                item.Instance = ((Ektron.Cms.Framework.UI.Controls.EktronUI.IntegerField)e.Item.FindControl("uxInsertInstance")).Value;
                string type = ((DropDownList)e.Item.FindControl("aspInsertType")).SelectedValue;
                item.Recurrence = (CloudProvisioningSchedule.RecurrenceType)(Enum.Parse(typeof(CloudProvisioningSchedule.RecurrenceType), type));
                DateTime date = ((Ektron.Cms.Framework.UI.Controls.EktronUI.Datepicker)e.Item.FindControl("uxInsertDate")).Value;
                string hour = ((DropDownList)e.Item.FindControl("aspInsertHour")).SelectedValue;
                string minute = ((DropDownList)e.Item.FindControl("aspInsertMinute")).SelectedValue;
                item.Date = (DateTime)(date + new TimeSpan(Int32.Parse(hour), Int32.Parse(minute), 0));
                //item.CauseNotification = ((CheckBox)e.Item.FindControl("aspInsertCausesNotification")).Checked;
                item.NotificationEmail = ((Ektron.Cms.Framework.UI.Controls.EktronUI.TextField)e.Item.FindControl("uxInsertNotificationEmail")).Value;
                item.Enabled = ((CheckBox)e.Item.FindControl("aspInsertEnabled")).Checked;
                ViewData.DeploymentSchedules.Insert(ViewData.DeploymentSchedules.Count, item);
                Ektron.Cms.Sync.Client.Relationship.SetExternalArgs(ProfileId, Ektron.Cloud.Common.Serializer.Serialize<CloudServiceRequest>(ViewData));
            }
            catch (Exception ex)
            {
                uxDialog.Visible = true;
                uxDialog.Title = this.GetLocalResourceObject("AddFailedDialogTitleText").ToString();
                uxDialogMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                uxDialogMessage.Text = ex.Message;
                uxDeleteButton.Visible = false;
            }
            finally
            {
                //                aspViewScheduledCloudEvents.InsertItemPosition = System.Web.UI.WebControls.InsertItemPosition.None;
            }
        }
        protected void Update(object sender, ListViewUpdateEventArgs e)
        {
            try
            {
                //e.Cancel = true;
                //CloudProvisioningSchedule item = new CloudProvisioningSchedule();
                //item.Id = Guid.Parse(((HiddenField)aspScheduledDeployments.EditItem.FindControl("aspEditId")).Value);
                //item.Name = ((Ektron.Cms.Framework.UI.Controls.EktronUI.TextField)aspScheduledDeployments.EditItem.FindControl("uxEditName")).Value;
                //string operation = ((DropDownList)aspScheduledDeployments.EditItem.FindControl("aspEditOperation")).SelectedValue;
                //item.Operation = (CloudCommandType)(Enum.Parse(typeof(CloudCommandType), operation));
                //item.Instance = ((Ektron.Cms.Framework.UI.Controls.EktronUI.IntegerField)aspScheduledDeployments.EditItem.FindControl("uxEditInstance")).Value;
                //string type = ((DropDownList)aspScheduledDeployments.EditItem.FindControl("aspEditType")).SelectedValue;
                //item.Type = (CloudProvisioningSchedule.ScheduleType)(Enum.Parse(typeof(CloudProvisioningSchedule.ScheduleType), type));
                //DateTime date = ((Ektron.Cms.Framework.UI.Controls.EktronUI.Datepicker)aspScheduledDeployments.EditItem.FindControl("uxEditDate")).Value;
                //string hour = ((DropDownList)aspScheduledDeployments.EditItem.FindControl("aspEditHour")).SelectedValue;
                //string minute = ((DropDownList)aspScheduledDeployments.EditItem.FindControl("aspEditMinute")).SelectedValue;
                //item.Date = (DateTime)(date + new TimeSpan(Int32.Parse(hour), Int32.Parse(minute), 0));
                //item.CauseNotification = ((CheckBox)aspScheduledDeployments.EditItem.FindControl("aspEditCausesNotification")).Checked;
                //item.NotificationEmail = ((Ektron.Cms.Framework.UI.Controls.EktronUI.TextField)aspScheduledDeployments.EditItem.FindControl("uxEditNotificationEmail")).Value;
                //item.Enabled = ((CheckBox)aspScheduledDeployments.EditItem.FindControl("aspEditEnabled")).Checked;
                //if (ViewData != null && ViewData.DeploymentSchedules != null && ViewData.DeploymentSchedules.Count > 0)
                //{
                //    ViewData.DeploymentSchedules[ViewData.DeploymentSchedules.IndexOf(ViewData.DeploymentSchedules.Find(delegate(CloudProvisioningSchedule s) { return s.Id == item.Id; }))] = item;
                //    Ektron.Cms.Sync.Client.Relationship.SetExternalArgs(ProfileId, Ektron.Cloud.Common.Serializer.Serialize<CloudServiceRequest>(ViewData));
                //}
            }
            catch (Exception ex)
            {
                uxDialog.Visible = true;
                uxDialog.Title = this.GetLocalResourceObject("UpdateFailedDialogTitleText").ToString();
                uxDialogMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                uxDialogMessage.Text = ex.Message;
                uxDeleteButton.Visible = false;

            }
            //            aspScheduledDeployments.EditIndex = -1;
        }
        protected void Delete(object sender, CommandEventArgs e)
        {
            try
            {
                CloudProvisioningSchedule item = new CloudProvisioningSchedule();
                item = ViewData.DeploymentSchedules.Find(delegate(CloudProvisioningSchedule s) { return s.Id.ToString() == e.CommandArgument.ToString(); });
                ViewData.DeploymentSchedules.Remove(item);
                Ektron.Cms.Sync.Client.Relationship.SetExternalArgs(ProfileId, Ektron.Cloud.Common.Serializer.Serialize<CloudServiceRequest>(ViewData));
            }
            catch (Exception ex)
            {
                uxDialog.Visible = true;
                uxDialog.Title = this.GetLocalResourceObject("DeleteFailedDialogTitleText").ToString();
                uxDialogMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                uxDialogMessage.Text = ex.Message;
                uxDeleteButton.Visible = false;
            }
            //            aspScheduledDeployments.EditIndex = -1;
        }

        protected void Cancel(object sender, ListViewCancelEventArgs e)
        {
            //            aspScheduledDeployments.EditIndex = -1;
            //            aspScheduledDeployments.InsertItemPosition = System.Web.UI.WebControls.InsertItemPosition.None;
        }
        protected void ShowAddUI(object sender, EventArgs e)
        {
            //            aspScheduledDeployments.InsertItemPosition = System.Web.UI.WebControls.InsertItemPosition.FirstItem;
        }
        protected void ShowUpdateUI(object sender, ListViewEditEventArgs e)
        {
            uxDeleteButton.Visible = true;
        }
        protected void ShowDeleteUI(object sender, ListViewDeleteEventArgs e)
        {
            uxDialog.Visible = true;
            uxDialog.Title = this.GetLocalResourceObject("DeleteItemConfirmationDialogTitle").ToString();
            uxDialogMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Warning;
            uxDialogMessage.Text = this.GetLocalResourceObject("DeleteItemConfirmationMessageText").ToString();
            uxDeleteButton.Visible = true;
            uxDeleteButton.CommandArgument = this.ViewData.DeploymentSchedules[e.ItemIndex].Id.ToString();
        }

        #endregion
    }
}