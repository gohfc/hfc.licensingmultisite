<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="CloudDeployment.aspx.cs" Inherits="Workarea_CloudDeployment" %>
<%@ Register Src="../sync/SyncDialogs.ascx" TagPrefix="ektron" TagName="SyncDialogs" %>
<%@ Register Src="CloudDeploymentDialogs.ascx" TagPrefix="ektron" TagName="CloudDeploymentDialogs" %>
<%@ Register Src="../sync/SyncResources.ascx" TagPrefix="ektron" TagName="SyncResources" %>
<%@ Register Src="AmazonDeploymentWizard.ascx" TagName="AmazonDeploymentWizard"  TagPrefix="ektron" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Cloud Manager</title>
    
    <script type="text/javascript">
        Ektron.ready(function () {
            Ektron.Workarea.Sync.Relationships.Init();
        });
    </script>
</head>
<body>    
    <!-- Ektron Client Script -->
    <asp:Literal id="ektronClientScript" runat="server"></asp:Literal>
    
    <!-- Sync String Resources -->
    <ektron:SyncResources ID="syncClientResources" runat="server" />
    
    <form id="form1" runat="server">
    <asp:ScriptManager ID="aspScriptManager" runat="server" />
    <ektronUI:Css ID="Css1" runat="server" Path="{WorkareaPath}/csslib/ektron.workarea.css" />
    <ektronUI:Css ID="Css2" runat="server" Path="{WorkareaPath}/csslib/ektron.workarea.css" />
    <ektronUI:Css ID="Css3" runat="server" Path="{WorkareaPath}/sync/css/ektron.workarea.sync.relationships.css" />
    <ektronUI:Css ID="Css4" runat="server" Path="{WorkareaPath}/sync/css/ektron.workarea.sync.dialogs.css" />
    <ektronUI:Css ID="Css5" runat="server" Path="{WorkareaPath}/java/plugins/modal/ektron.modal.css" />
    
        <ektronUI:CssBlock runat="server" ID="uxCloudDeploymentStyle">
                <CssTemplate>
                    .right
                    {
                        position:absolute;
                        right:0px;
                    }
                    .toolbar-display
                    {
                        display:inline;
                    }
                    div.dv-toolbar-display
                    {
                        float:right;
                    }
                    .dv-service-logo img 
                    {
                        border-radius: 0.5em;
                        float:left;
                        margin-right: 1em;
                        padding-top:0.2em
                    }

                </CssTemplate>

        </ektronUI:CssBlock>

    <ektronUI:JavaScriptBlock ID="uxCloudDeploymentLocalizedStrings" runat="server">
        <ScriptTemplate>
            Ektron.Namespace.Register("Ektron.Workarea.Cloud");
            Ektron.Workarea.Cloud.Localization = {
                DeploymentDialog: {
                    title: "<%= (string)this.GetLocalResourceObject("DeploymentDialogTitle")%>",
                    message: "<%= (string)this.GetLocalResourceObject("DeploymentDialogCaption")%>",
                    okButtonText: "<%= (string)this.GetLocalResourceObject("DeploymentDialogOkButtonText") %>"
                }
            };
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
	<ektronUI:JavaScript ID="uxJS1" runat="server" Path="~/Workarea/java/internCalendarDisplayFuncs.js" />
        <ektronUI:JavaScript ID="uxJS2" runat="server" Path="~/Workarea/java/toolbar_roll.js" />
            
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server"></div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <div class="dv-toolbar-display">
                    <asp:DropDownList ID="aspSvFilter" Visible="false" runat="server" OnSelectedIndexChanged="aspSvFilter_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
                <table class="toolbar-display">
                    <tr id="rowToolbarButtons" runat="server"></tr>
                </table>
            </div>
        </div>
        <div class="ektronPageContainer" id="divSyncTabs" runat="server">
            <div class="ektronTopSpace">
                <ektronUI:Message ID="uxNoRelationshipsMessage" runat="server" DisplayMode="Information" Text="<%$Resources: lblCloudNoConfigurations.Text %>" Visible="false"></ektronUI:Message>
                <asp:Repeater ID="rptRelationshipList" runat="server" OnItemDataBound="HandleRelationshipListItemDataBound">
                    <ItemTemplate>
                        <ul class="relationshipList">
                            <li id="liRelationship" runat="server" class="relationship">
                                <div class="relationshipInfo">
                                    <div id="serviceLogo" runat="server" class="dv-service-logo">
                                    </div>
                                    <div class="azureButtons ektron-ui-clearfix" id="divRelationshipButtons" runat="server">
                                    </div>
                                    <h5><asp:Literal ID="aspHostedService" runat="server" /></h5>
                                    <ul class="azureServerDetails azureServerDetailsCol1">
                                        <li><asp:Label ID="labelSubscriptionId" CssClass="relationshipLabel" runat="server" /> <asp:Literal ID="subscriptionId" runat="server" /></li>
                                        <li><asp:Label ID="labelWebInstances" CssClass="relationshipLabel" runat="server" /> <asp:Literal ID="webInstances" runat="server" /></li>
                                        <li><asp:Label CssClass="relationshipLabel" ID="labelLocalSitePath" runat="server"></asp:Label> <asp:Literal ID="localSitePath" runat="server"></asp:Literal></li>
                                        <li><asp:Label CssClass="relationshipLabel" ID="labelRemoteSiteUrl" runat="server"></asp:Label> <asp:Literal ID="litRemoteSiteUrl" runat="server"></asp:Literal></li>
                                    </ul>

                                    <ul class="azureServerDetails azureServerDetailsCol2">
                                        <li><asp:Label CssClass="relationshipLabel" ID="labelSqlDatabaseName" runat="server"></asp:Label> <asp:Literal ID="sqlDatabaseName" runat="server"></asp:Literal></li>
                                        <li><asp:Label CssClass="relationshipLabel" ID="labelCloudDatabaseName" runat="server"></asp:Label> <asp:Literal ID="cloudDatabaseName" runat="server"></asp:Literal></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="profilesListItem" runat="server" class="profiles">
                                <table class="profileList">
                                    <tr>
                                        <th><asp:Literal ID="litProfileHeader" runat="server"></asp:Literal></th>
                                        <th><asp:Literal ID="litProfileIdHeader" runat="server"></asp:Literal></th>
                                        <th><asp:Literal ID="litScheduleHeader" runat="server"></asp:Literal></th>
                                        <th><asp:Literal ID="litLastRunTimeHeader" runat="server"></asp:Literal></th>
                                        <th><asp:Literal ID="litLastRunResultHeader" runat="server"></asp:Literal></th>
                                        <th><asp:Literal ID="litButtonsHeader" runat="server"></asp:Literal></th>
                                    </tr>
                                    <asp:Repeater ID="rptProfileList" runat="server" OnItemDataBound="HandleProfileListItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="profileName">
                                                    <a href="#" id="linkProfileName" runat="server"></a>
                                                </td>
                                                 <td class="profileId">
                                                     <asp:Literal ID="litProfileId" runat="server"></asp:Literal>
                                                </td>
                                                <td class="schedule">
                                                    <asp:Literal ID="litSchedule" runat="server"></asp:Literal>
                                                    <p class="nextRunTime"><asp:Literal ID="litNextRunTime" runat="server"></asp:Literal></p>
                                                </td>
                                                <td class="lastRunTime">
                                                    <asp:Literal ID="litLastRunTime" runat="server"></asp:Literal>
                                                </td>
                                                <td class="lastRunResult">
                                                    <asp:Literal ID="litLastRunResult" runat="server"></asp:Literal>
                                                </td>
                                                <td class="profileButtons">
                                                    <div class="syncButtons" id="divProfileButtons" runat="server">
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </li>
                        </ul>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <ektron:SyncDialogs ID="syncDialogs" runat="server" IsCloud="true" />
        <ektron:CloudDeploymentDialogs ID="uxCloudDeploymentDialogs" runat="Server" />
        <ektron:AmazonDeploymentWizard ID="uxAmazonDeploymentWizard" runat="server" />
    </form>
</body>
</html>
