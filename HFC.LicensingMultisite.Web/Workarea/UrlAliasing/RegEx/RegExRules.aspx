﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegExRules.aspx.cs" Inherits="Ektron.Workarea.UrlAliasing.RegExRules" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function RollOver(MyObj) {
            $ektron(MyObj).parent().addClass("button-over");
            $ektron(MyObj).parent().removeClass("button");
        }

        function RollOut(MyObj) {
            $ektron(MyObj).parent().addClass("button");
            $ektron(MyObj).parent().removeClass("button-over");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ektronUI:Css ID="aliasCSS" Path="../CSS/Ektron.Workarea.UrlAliasing.css" runat="server" />
    <div>
        <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar">
                    <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
                </span><span id="_WorkareaTitleBar" style="display: none;"></span>
            </div>
            <div class="ektronToolbar" id="divToolBar" runat="server">
                <table>
                    <tr>
                        <td class="column-AddButton">
                            <div>
                                <asp:HyperLink ID="uxAddButton" runat="server" class="primary editButton" meta:resourcekey="uxAddButton"
                                    NavigateUrl="AddEditViewRegExRule.aspx" />
                            </div>
                        </td>
                    <asp:Literal ID="uxBackButton" runat="server"></asp:Literal>
                        <td class="column-PrimaryButton">
                            <asp:LinkButton runat="server" ID="uxEditButton" class="primary editButton" OnClick="EditButton_Click"
                                meta:resourcekey="uxEditButton" CommandName="Edit" />
                            <asp:LinkButton runat="server" ID="uxSaveButton" Visible="False" meta:resourcekey="uxSaveButton"
                                class="primary saveButton" OnClick="SaveButton_Click" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td class="column-Site">
                            <asp:Label ID="uxSiteLabel" runat="server" meta:resourcekey="uxSiteLabel" />
                        </td>
                        <td class="column-Site">
                            <asp:DropDownList ID="uxSiteList" runat="server" meta:resourcekey="uxSiteList" AutoPostBack="true"
                                OnSelectedIndexChanged="filterChange" AppendDataBoundItems="true" />
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td class="column-Search">
                        </td>
                        <td class="column-Search">
                            <asp:TextBox ID="uxSearchTextBox" runat="server" meta:resourcekey="uxSearchTextBox"
                                Width="100px" onfocus="if(this.value=='Search')this.value=''" onblur="if(this.value=='')this.value='Search'" />
                        </td>
                        <td class="column-Search">
                            <asp:ImageButton runat="server" ID="uxSearchButton" meta:resourcekey="uxSearchButton"
                                class="deleteButton" OnClick="filterChange" ImageUrl="../../images/ui/icons/magnifier.png" />
                            <asp:HyperLink ID="uxSearchReset" runat="server" NavigateUrl="regexrules.aspx"
                                CssClass="resetButton" meta:resourcekey="uxSearchReset">
                                <img src="../../images/ui/icons/restore.png" />
                            </asp:HyperLink>
                        </td>
                        <td>
                            <div class="actionbarDivider">
                            </div>
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="ektronPageContainer ektronPageGrid">
            <ektronUI:Message ID="uxMessage" runat="server" Visible="<%# this.showMessage %>">
            </ektronUI:Message>
            <ektronUI:GridView ID="uxRegExList" runat="server" AutoGenerateColumns="false" EktronUIAllowResizableColumns="false" EktronUIEnabled="true"
                OnEktronUISortChanged="uxAutoRulesGridView_EktronUISortChanged" OnEktronUIPageChanged="uxRegExRulesGridView_EktronUIPageChanged">
                <Columns>
                    <ektronUI:CheckBoxField ID="uxDeleteField">
                        <CheckBoxFieldHeaderStyle HeaderText="<%$ Resources:columnHeaderDelete %>" />
                        <CheckBoxFieldColumnStyle Visible="<%# this.IsEditable %>" />
                        <KeyFields>
                            <ektronUI:KeyField DataField="Id" />
                        </KeyFields>
                    </ektronUI:CheckBoxField>
                    <ektronUI:CheckBoxField ID="uxActiveColumn">
                        <CheckBoxFieldHeaderStyle HeaderText="<%$ Resources:uxActiveColumn %>" />
                        <CheckBoxFieldItemStyle Enabled="<%# this.IsEditable %>" CheckedBoundField="IsEnabled" />
                        <KeyFields>
                            <ektronUI:KeyField DataField="Id" />
                        </KeyFields>
                    </ektronUI:CheckBoxField>
                    <asp:BoundField DataField="Priority" meta:resourcekey="uxPriorityColumn" SortExpression="SortOrder" />
                    <asp:HyperLinkField DataTextField="ExpressionName" DataNavigateUrlFormatString="AddEditViewRegExRule.aspx?id={0}"
                        DataNavigateUrlFields="RegExId" meta:resourcekey="uxExpressionColumn" SortExpression="ExpressionName" />
                    <asp:BoundField DataField="TransformedUrl" meta:resourcekey="uxTransformedColumn"
                        SortExpression="TransformedUrl" />
                </Columns>
            </ektronUI:GridView>
        </div>
    </div>
    </form>
</body>
</html>
