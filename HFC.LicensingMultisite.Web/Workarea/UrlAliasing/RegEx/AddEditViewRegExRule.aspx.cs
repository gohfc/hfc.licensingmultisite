﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Settings;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.Settings.UrlAliasing;
using Ektron.Cms.Settings.UrlAliasing.DataObjects;
using System.Data;

public partial class Workarea_UrlAliasing_RegEx_AddEditViewRegExRule : Ektron.Cms.Workarea.Page
{
    #region private members

    SiteAPI siteAPI = new SiteAPI();
    public EkMessageHelper msgHelper { get { return siteAPI.EkMsgRef; } }
    private ISite site;
    private RegExAliasManager manager = new RegExAliasManager();
    public UrlAliasRegExData rule { get; set; }
    public string backurl = "";
    StyleHelper styleHelper = new StyleHelper();

    private const string querystringKey = "id";
    public string sitePath;

    [System.ComponentModel.DefaultValue(false)]
    public bool showMessage { get; set; }
    [System.ComponentModel.DefaultValue(false)]
    public bool IsEditable { get; set; }
    #endregion


    #region page methods

    protected override void OnInit(EventArgs e)
    {

        if (this.HasViewPermission())
        {
            sitePath = siteAPI.SitePath;
            backurl = "UrlAliasing/regex/regexrules.aspx";
            try
            {
                if (!String.IsNullOrEmpty(Request.QueryString[querystringKey]))
                {
                    rule = manager.GetItem(long.Parse(Request.QueryString[querystringKey]));
                    if (rule.RegExId == 0 || rule.Id == 0)
                    {
                        rule = null;
                    }
                }
                this.IsEditable = ((rule != null && Page.IsPostBack) || (rule == null && !Page.IsPostBack));

                this.initWorkareaUI();
                if (!Page.IsPostBack)
                {
                    this.bindUIOptions();
                }
                this.RegisterResources();

                // View Mode
                if (rule != null && !Page.IsPostBack)
                {
                    uxEditButton.Visible = this.HasEditPermission();
                    uxDelete.Visible = HasEditPermission();
                    uxSaveButton.Visible = false;
                    uxSiteList.Enabled = false;
                    uxBackButton.Visible = true;
                    this.bindData(rule);
                }

                // Edit mode
                if (rule != null && Page.IsPostBack)
                {
                    // moved to click event handler
                }

                // Add Mode
                if (rule == null)
                {
                    uxEditButton.Visible = false;
                    uxSaveButton.Visible = true;
                    uxDelete.Visible = false;
                    uxSiteList.Enabled = true;
                    uxBackButton.Visible = true;
                }
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }
        else
        {
            Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
        }
    }

    private void BuildRegexLib()
    {

        System.Collections.Generic.List<RegExSampleData> RegExSampleList;

        Ektron.Cms.Settings.UrlAliasing.UrlAliasRegExBI regexSampleListApi = new Ektron.Cms.Settings.UrlAliasing.UrlAliasRegExBI();
        string escapedExp;

        RegExSampleList = regexSampleListApi.GetRegExSampleList();
        if ((RegExSampleList != null) && RegExSampleList.Count > 0)
        {
            regExPicker.Columns.Add(styleHelper.CreateBoundField("EXPRESSION", "Expression", "title-header", HorizontalAlign.Left, HorizontalAlign.NotSet, Unit.Percentage(25), Unit.Percentage(25), false, false));
            regExPicker.Columns.Add(styleHelper.CreateBoundField("EXPRESSION MAP", "Expression Map", "title-header", HorizontalAlign.Left, HorizontalAlign.NotSet, Unit.Percentage(30), Unit.Percentage(30), false, false));
            regExPicker.Columns.Add(styleHelper.CreateBoundField("TRANSFORMED URL", "Transformed URL", "title-header", HorizontalAlign.Left, HorizontalAlign.NotSet, Unit.Percentage(25), Unit.Percentage(25), false, false));

            DataTable dt = new DataTable();
            DataRow dr;

            dt.Columns.Add(new DataColumn("EXPRESSION", typeof(string)));
            dt.Columns.Add(new DataColumn("EXPRESSION MAP", typeof(string)));
            dt.Columns.Add(new DataColumn("TRANSFORMED URL", typeof(string)));

            for (int i = 0; i <= RegExSampleList.Count - 1; i++)
            {
                dr = dt.NewRow();
                escapedExp = (string)(RegExSampleList[i].Expression.Replace("\\", "\\\\"));
                dr["EXPRESSION"] = "<a href=\"#\" onclick=\"selectExpMap(\'" + RegExSampleList[i].ExpressionMap + "\',\'" + escapedExp + "\',\'" + RegExSampleList[i].TransformedUrl + "\');return false;\">" + RegExSampleList[i].Expression + "</a>"; //"<a href=""urlmanualaliasmaint.aspx?action=view&id=" & maliaslist(i).AliasId & """>" & maliaslist(i).DisplayAlias & "</a>"
                dr["EXPRESSION MAP"] = "<a href=\"#\" onclick=\"selectExpMap(\'" + RegExSampleList[i].ExpressionMap + "\',\'" + escapedExp + "\',\'" + RegExSampleList[i].TransformedUrl + "\');return false;\">" + RegExSampleList[i].ExpressionMap + "</a>";
                dr["TRANSFORMED URL"] = "<a href=\"#\" onclick=\"selectExpMap(\'" + RegExSampleList[i].ExpressionMap + "\',\'" + escapedExp + "\',\'" + RegExSampleList[i].TransformedUrl + "\');return false;\">" + RegExSampleList[i].TransformedUrl + "</a>";
                dt.Rows.Add(dr);
            }
            DataView dv = new DataView(dt);
            regExPicker.DataSource = dv;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        BuildRegexLib();
        closeDialogLink.Text = msgHelper.GetMessage("close title");
        closeDialogLink.ToolTip = closeDialogLink.Text;
        lblExpressionLib.Text = msgHelper.GetMessage("lbl select exp");
        closeDialogLink.NavigateUrl = "#" + System.Text.RegularExpressions.Regex.Replace(msgHelper.GetMessage("close this dialog"), "\\s+", "");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        // for inline databinding
        this.DataBind();
    }

    protected void uxSaveButton_Click(object sender, EventArgs e)
    {
        try
        {
            if (rule == null)
            {
                rule = new UrlAliasRegExData();
            }
            rule.IsEnabled = uxActiveCheckBox.Checked;
            rule.ExpressionName = uxExpressionNameValue.Text;
            rule.Expression = uxExpressionValue.Text;
            rule.ExpressionMap = uxExpressionMapValue.Text;
            rule.TransformedUrl = uxExampleURLValue.Text;
            rule.Priority = (EkEnumeration.RegExPriority)Enum.Parse(typeof(EkEnumeration.RegExPriority), ddlPriority.SelectedValue);
            long siteId;
            if (long.TryParse(uxSiteList.SelectedValue, out siteId))
            {
                rule.SiteId = siteId;
            }
            //rule.Priority 
            if (rule.RegExId > 0)
            {
                manager.Update(rule);
            }
            else
            {
                manager.Add(rule);
            }
        }
        catch (Exception exc)
        {
            //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
            Utilities.ShowError(exc.Message);
        }
        uxEditButton.Visible = true;
        uxSaveButton.Visible = false;
        uxSiteList.Enabled = false;
        uxBackButton.Visible = true;
        uxDelete.Visible = true;
        this.IsEditable = false;
        Response.Redirect("RegExRules.aspx", true);
    }

    protected void uxEditButton_Click(object sender, EventArgs e)
    {
        backurl = "UrlAliasing/regex/AddEditViewRegExRule.aspx?" + querystringKey + "=" + Request.QueryString[querystringKey];
        uxEditButton.Visible = false;
        uxSaveButton.Visible = true;
        uxDelete.Visible = false;
        uxBackButton.Visible = true;
        this.IsEditable = true;
    }

    protected void uxDelete_Click(object sender, EventArgs e)
    {
        try
        {
            manager.Delete(rule.RegExId);
        }
        catch (Exception exc)
        {
            //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
            Utilities.ShowError(exc.Message);
        }
        Response.Redirect("RegExRules.aspx", true);
    }

    #endregion

    #region private methods

    private void initWorkareaUI()
    {
        this.RegisterResources();
        ContentAPI refContentApi = new ContentAPI();
        EkMessageHelper msgHelper = refContentApi.EkMsgRef;
        string returnpath = refContentApi.RequestInformationRef.ApplicationPath + backurl;
        uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
        this.aspHelpButton.Text = styleHelper.GetHelpButton("urlaliasing_rules_regex_detail", string.Empty);
    }

    private void bindUIOptions()
    {
        site = ObjectFactory.GetSite(this.siteAPI.RequestInformationRef);
        bindSiteList();
        bindPriority();
    }
    private void bindPriority()
    {
        ddlPriority.DataSource = Enum.GetNames(typeof(EkEnumeration.RegExPriority));
        ddlPriority.DataBind();
    }
    private void bindData(UrlAliasRegExData rule)
    {
        uxActiveCheckBox.Checked = rule.IsEnabled;
        uxSiteList.SelectedValue = rule.SiteId.ToString();
        uxExpressionNameValue.Text = rule.ExpressionName;
        uxExpressionValue.Text = rule.Expression;
        uxExpressionMapValue.Text = rule.ExpressionMap;
        uxExampleURLValue.Text = rule.TransformedUrl;
        ddlPriority.SelectedValue = rule.Priority.ToString();
    }

    private void bindSiteList()
    {
        Dictionary<long, string> siteList = site.GetSiteList();
        var displaySites = from sites in siteList
                           select new { Label = sites.Value, ID = sites.Key };
        uxSiteList.Items.Clear();
        uxSiteList.DataSource = displaySites;
        uxSiteList.DataTextField = "Label";
        uxSiteList.DataValueField = "ID";
    }

    private bool HasViewPermission()
    {
        return
            (
                !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                ContentAPI.Current.RequestInformationRef.UserId != 0 
            );
    }

    private bool HasEditPermission()
    {
        return
            (
                !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.UrlAliasingAdmin), ContentAPI.Current.UserId, false))
            );
    }


    /// <summary>
    /// Register Javascripts and CSS packages
    /// </summary>
    private void RegisterResources()
    {
        ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
        Package resources = new Package()
        {
            Components = new List<Component>()
                {
                    Packages.EktronCoreJS,
                    Packages.Ektron.Namespace,
                    Packages.Ektron.Workarea.Core, 
                    JavaScript.Create(cmsContextService.WorkareaPath + "/UrlAliasing/js/ektron.workarea.urlaliasing.js"),
                }
        };
        resources.Register(this);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronModalCss);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronModalJS);
    }

    #endregion
}
