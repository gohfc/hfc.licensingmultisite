﻿<%@ WebHandler Language="C#" Class="ektronworkareaurlaliasing" %>

using System;
using System.Web;
using System.Linq;
using Ektron.Cms.Framework.Settings.UrlAliasing;
using Ektron.Cms.Framework;
using Ektron.Newtonsoft.Json;
using System.Collections.Generic;
using Ektron.Cms.Settings.UrlAliasing.DataObjects;
using System.Linq;
using Ektron.Cms;
using Ektron.Cms.Common;

public class AliasRuleRequest
{
    [JsonProperty("Id")]
    public object Id = "";
    [JsonProperty("Option")]
    public object Option = "";
    [JsonProperty("Type")]
    public object Type = "";
}

public class ektronworkareaurlaliasing : IHttpHandler
{
    public AliasRuleRequest aliasruledata;
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            aliasruledata = new AliasRuleRequest();
            string folderid = "0";
            string contentid = "0";
            string response = string.Empty;
            if (context.Request["request"] != null)
            {
                aliasruledata = (AliasRuleRequest)JsonConvert.DeserializeObject(context.Request["request"], typeof(AliasRuleRequest));

                if (!string.IsNullOrEmpty(aliasruledata.Option.ToString()) && aliasruledata.Type.ToString() == "auto")
                {
                    response = ReplaceCharacters(aliasruledata.Option.ToString(), aliasruledata.Id.ToString());
                    context.Response.Write(response);
                }

                if (!string.IsNullOrEmpty(aliasruledata.Option.ToString()) && aliasruledata.Type.ToString() == "manual")
                {
                    if (!string.IsNullOrEmpty(context.Request.QueryString["folderid"]))
                    {
                        folderid = context.Request.QueryString["folderid"].ToString();
                    }
                    if (!string.IsNullOrEmpty(context.Request.QueryString["contentid"]))
                    {
                        contentid = context.Request.QueryString["contentid"].ToString();
                    }
                    response = ValidateAliasName(aliasruledata.Option.ToString(), aliasruledata.Id.ToString(), folderid, contentid);
                    context.Response.Write(response);
                }
            }
        }
        catch (Exception Ex)
        {
            Ektron.Cms.EkException.LogException(Ex);
        }
    }

    /// <summary>
    /// Replace all characters In preview URL
    /// </summary>
    /// <param name="previewurl">URL</param>
    /// <param name="ruleid">Alias ID</param>
    /// <returns>Modified URL</returns>
    private string ReplaceCharacters(string previewurl, string ruleid)
    {
        ReplacementCharacterManager rm = new ReplacementCharacterManager(ApiAccessMode.Admin);
        List<ReplacementCharacter> replacecharacterlist = new List<ReplacementCharacter>();
        if (!string.IsNullOrEmpty(ruleid))
        {
            replacecharacterlist = rm.GetList(long.Parse(ruleid));
        }
        else
        {
            replacecharacterlist = rm.getDefault();
        }

        if (replacecharacterlist != null)
        {
            foreach (var x in replacecharacterlist)
            {
                previewurl = previewurl.Replace(x.MatchCharacter, x.ReplaceCharacter);
            }
        }

        return previewurl;
    }

    /// <summary>
    /// Check Whether Alias Name Already Exists or Not
    /// </summary>
    /// <param name="aliasname"> Alias Name with Extension</param>
    /// <param name="aliasid">Alias Id</param>
    /// <returns>message</returns>
    private string ValidateAliasName(string aliasname, string aliasid = "0", string folderid = "0", string contentid = "0")
    {
        string err = string.Empty;
        long siteid = 0;
        if (!string.IsNullOrEmpty(aliasname))
        {

            AliasManager alasimanager = new AliasManager(Ektron.Cms.Framework.ApiAccessMode.Admin);
            AliasCriteria criteria = new AliasCriteria();
            criteria.AddFilter(AliasProperty.Alias, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, aliasname);
            if (long.Parse(aliasid) != 0)
            {
                criteria.AddFilter(AliasProperty.Id, Ektron.Cms.Common.CriteriaFilterOperator.NotEqualTo, long.Parse(aliasid));
            }
            criteria.AddFilter(AliasProperty.LanguageId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, Ektron.Cms.CommonApi.Current.ContentLanguage);
            if (!string.IsNullOrEmpty(folderid))
            {
                siteid = GetSiteId(Convert.ToInt64(folderid), "folder");
            }
            if (!string.IsNullOrEmpty(contentid) && long.Parse(contentid) > 0)
            {
                siteid = GetSiteId(Convert.ToInt64(contentid), "content");
            }
            if(siteid>0)
                criteria.AddFilter(AliasProperty.SiteId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, siteid);
            else
                criteria.AddFilter(AliasProperty.SiteId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, 0);

            List<AliasData> list = alasimanager.GetList(criteria);

            if (list != null && list.Any())
            {
                err = "error";
            }

            if (!nameValid(aliasname))
            {
                err = "errorInvalidCharacters";
            }
        }
        return err;
    }

    private bool nameValid(string aliasName)
    {
        string invalidchars = @",\<> :|?''^%$!*#";

        foreach (char c in invalidchars.ToCharArray())
        {
            if (aliasName.Contains(c)) return false;
        }

        if (aliasName.EndsWith("."))
        {
            return false;
        }

        if (aliasName.IndexOf("..") > -1)
        {
            return false;
        }

        //Remove any leading slashes to evaluate slashes in between
        string tempAliasName = aliasName.TrimStart(new char[] { '/' });

        if (tempAliasName.IndexOf("//") > -1)
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get the SiteID from FolderId
    /// </summary>
    /// <param name="objectID"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    private long GetSiteId(long objectId, string type = "folder")
    {
        long siteId = 0;
        if (objectId > 0)
        {
            var fm = ObjectFactory.GetFolder(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
            if (type == "folder")
            {
                var folderdata = fm.GetItem(objectId, false);
                if (folderdata != null && folderdata.NameWithPath.ToString().ToLower() != "/")
                {
                    FolderCriteria criteria = new FolderCriteria();
                    criteria.AddFilter(FolderProperty.FolderPath, CriteriaFilterOperator.EqualTo, folderdata.NameWithPath.Split('/')[0].ToString() + "/");

                    var list = fm.GetList(criteria);
                    if (list != null && list.Any())
                    {
                        if (list[0].IsDomainFolder)
                            siteId = list[0].Id;
                    }
                }
            }
            else
            {
                var cm = ObjectFactory.GetContent(ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
                var contentdata = cm.GetItem(objectId, false);

                if (contentdata != null && contentdata.Path.ToString().ToLower() != "/")
                {
                    FolderCriteria criteria = new FolderCriteria();
                    criteria.AddFilter(FolderProperty.FolderPath, CriteriaFilterOperator.EqualTo, contentdata.Path.Split('/')[0].ToString() + "/");

                    var list = fm.GetList(criteria);
                    if (list != null && list.Any())
                    {
                        if (list[0].IsDomainFolder)
                            siteId = list[0].Id;
                    }
                }
            }
        }

        return siteId;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}