﻿/*
* Ektron Workarea URL Aliasing
*
* Copyright 2012
*
* Depends:
*	jQuery
*   Ektron.Namespace.js
*/

Ektron.Namespace.Register("Ektron.Workarea.UrlAliasing");
Ektron.Workarea.UrlAliasing = {
    /* properties */
    SelectedFolderPath: "xyz",

    /* methods */
    initAddAuto: function () {
        $ektron("#uxSourceTypeDropDownList").change(function () {
            var configType = $ektron('#uxSourceTypeDropDownList option:selected').val();
            if (configType != 1) { //taxonomy is the only one with language support
                $ektron('#uxLanguageList').val(-1).attr("disabled", "disabled");
                $ektron('#uxLanguageList option[value="-1"]').removeAttr("disabled");
            } else {
                $ektron('#uxLanguageList option[value="-1"]').attr("disabled", "disabled");
                if ($ektron('#uxLanguageList').val() == "-1") $ektron('#uxLanguageList option[value!="-1"]:first').attr("selected", "selected");
                $ektron('#uxLanguageList').removeAttr("disabled");
            }
        });
        var configType = $ektron('#uxSourceTypeDropDownList option:selected').val();
        if (configType != 1) { //taxonomy is the only one with language support
            $ektron('#uxLanguageList').val(-1).attr("disabled", "disabled");
            $ektron('#uxLanguageList option[value="-1"]').removeAttr("disabled");
        } else {
            $ektron('#uxLanguageList option[value="-1"]').attr("disabled", "disabled");
            if ($ektron('#uxLanguageList').val() == "-1") $ektron('#uxLanguageList option[value!="-1"]:first').attr("selected", "selected");
            $ektron('#uxLanguageList').removeAttr("disabled");
        }
    },
    initAutoConfig: function () {
        var configType = $ektron('#uxSourceTypeDropDownList option:selected').val();
        switch (configType) {
            case "1":
                Ektron.Workarea.UrlAliasing.InitTaxonomyConfig();
                break;
            case "2":
                Ektron.Workarea.UrlAliasing.InitFolderConfig();
                break;
            case "4":
                Ektron.Workarea.UrlAliasing.InitUserConfig();
                break;
            case "5":
                Ektron.Workarea.UrlAliasing.InitGroupConfig();
                break;
        }

        $ektron('.uxAddNewCharacter').click(function () {
            $('#uxTableHead tr:last').after('<tr><td class="urlTypesLabel"><input name="newHiddenCharacterID" type="hidden" id="newHiddenCharacterID" class="uxHiddenCharacterID" value="0"><input name="newHiddenConfigID" type="hidden" id="newHiddenConfigID" class="uxHiddenConfigID" value="0"><input name="newHiddenIsDefault" type="hidden" id="newHiddenIsDefault" class="uxHiddenIsDefault" value="False"><input name="newMatchCharacter" style="width: 20px;" type="text" value="" maxlength="1" id="newMatchCharacter" class="uxMatchCharacter"></td><td class="urlTypesLabel"><input name="newReplacementCharacter" type="text" value="" id="newReplacementCharacter" style="width: 20px;" class="uxReplacementCharacter"></td><td><img src="../Images/ui/icons/delete.png" alt="delete" class="uxDeleteCharacter"></td></tr>');
        });

        $(".uxDeleteCharacter").live("click", function () {
            $(this).closest("tr").remove()
            Ektron.Workarea.UrlAliasing.SerializeCharMap();
            return false;
        })

        $ektron('.uxReplacementCharacter').live('blur', function () {
            var isValid = false;
            var badChars = "[\<>:|?]'%.#* &+";
            var theReplacementChar = $ektron(this).val();
            if (badChars.indexOf(theReplacementChar) != -1) {
                isValid = false;
                alert("Character can not be [\<>:|?]'%.#* &+");
                $ektron(this).val('');
            } else {
                isValid = true;
                Ektron.Workarea.UrlAliasing.SerializeCharMap();
            }
            return isValid;
        });
    },

    InitTaxonomyConfig: function () {
        Ektron.Workarea.UrlAliasing.getTaxonomyExcludePathList();
        $ektron("#uxTaxonomyPath_TextField_aspInput").focus(function () {
            this.blur();
        });
        $ektron("#uxTaxonomyPath_TextField_aspInput").focus(function () {
            Ektron.Workarea.UrlAliasing.getTaxonomyExcludePathList();
        });
        $ektron("#uxExcludeFromTaxPathList").change(function () {
            Ektron.Workarea.UrlAliasing.getExamplePreviewForTax({ selectedfolderpath: "" });
        });
        $ektron("#uxAliasTaxFormatDropDownList").change(function () {
            Ektron.Workarea.UrlAliasing.getExamplePreviewForTax({ selectedfolderpath: "" });
        });
        $ektron("#uxTaxExtensionDropDownList").change(function () {
            Ektron.Workarea.UrlAliasing.getExamplePreviewForTax({ selectedfolderpath: "" });
        });
    },

    InitFolderConfig: function () {
        Ektron.Workarea.UrlAliasing.getFolderExcludePathList();
        $ektron("#uxFolderPath_TextField_aspInput").focus(function () {
            this.blur();
        });
        $ektron("#uxFolderPath_TextField_aspInput").focus(function () {
            Ektron.Workarea.UrlAliasing.getFolderExcludePathList();
        });
        $ektron("#uxExcludeFromPathList").change(function () {
            Ektron.Workarea.UrlAliasing.getExamplePreview({ selectedfolderpath: "" });
        });
        $ektron("#uxAliasFormatDropDownList").change(function () {
            Ektron.Workarea.UrlAliasing.getExamplePreview({ selectedfolderpath: "" });
        });
        $ektron("#uxExtensionDropDownList").change(function () {
            Ektron.Workarea.UrlAliasing.getExamplePreview({ selectedfolderpath: "" });
        });
    },

    IsValidChar: function (replacementString) {
        return true;
    },

    InitUserConfig: function () {
        Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview("user");
        $ektron("#uxExtensionsForUserDropDownList").change(function () {
            Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview("user");
        });
        $ektron("#uxUserAliasPath input").keyup(function () {
            Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview("user");
        });
    },

    InitGroupConfig: function () {
        Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview("group");
        $ektron("#uxExtensionsForGroupDropDownList").change(function () {
            Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview("group");
        });
        $ektron("#uxGroupAliasPath input").keyup(function () {
            Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview("group");
        });
    },

    initSettings: function () {
        $ektron('div.extensionControlBlock #btnRemoveExt').click(function () {
            Ektron.Workarea.UrlAliasing.RemoveExtension();
            return false;
        });
        $ektron('div.extensionControlBlock #btnAdd').click(function () {
            Ektron.Workarea.UrlAliasing.AddExtension();
            return false;
        });
        $ektron('#uxGlobalEnableCheckBox').click(function () {
            Ektron.Workarea.UrlAliasing.ToggleEnableSettings($ektron(this).is(':checked'));
        });
    },

    initAddManual: function () {
        $ektron('#uxLanguageList').change(function () {
            var selectedLanguage = $ektron(this).val();
            $ektron('#uxContentSelectIframe').attr('src', "../QuickLinkSelect.aspx?folderid=0&LangType=" + selectedLanguage + "&formName=frm_urlalias&titleFormElem=uxContentSelector_TextField_aspInput&useQLinkCheck=0&SetBrowserState=1&forcetemplate=1&disAllowAddContent=1");
            $ektron('#uxQuicklinkSelector option').remove();
            $ektron('#uxContentSelector_TextField_aspInput').val("");
            $ektron("#frm_content_id_validation_box_TextField_aspInput").val("");
        });

        $ektron("#uxExtensionDropDownList").change(function () {
            Ektron.Workarea.UrlAliasing.validateAliasName();
        });

        $ektron("#uxAliasName_TextField_aspInput").keyup(function () {
            delay(function () {
                Ektron.Workarea.UrlAliasing.validateAliasName();
            }, 400);

        });

        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
    },

    initRedirect: function () {
        $ektron("#uxRedirectCodeDropDownList").change(function () {
            Ektron.Workarea.UrlAliasing.URLValidation();
        });

        if (typeof String.prototype.startsWith != 'function') {
            String.prototype.startsWith = function (str) {
                return this.indexOf(str) == 0;
            };
        }
    },


    RefreshTreeAfterSettings: function () {
        top.TreeNavigation("AdminTree", "configuration\\url aliasing");
    },

    ToggleEnableSettings: function (enable) {
        if (enable) {
            $ektron('table.urlAliasSettings input').removeAttr("disabled").removeClass("aspNetDisabled").removeClass("ui-state-disabled");
            $ektron('table.urlAliasSettings select').removeAttr("disabled");
        } else {
            $ektron('table.urlAliasSettings input').attr("disabled", "disabled").addClass("aspNetDisabled").addClass("ui-state-disabled");
            $ektron('table.urlAliasSettings select').attr("disabled", "disabled");
        }
    },

    SaveSettings: function () {
        var items = $ektron("#uxFileExtension > option").map(function () {
            var arr = [];
            var extension = new Object();
            if ($ektron(this).val() != $ektron(this).text()) {
                extension.Id = $ektron(this).val();
            }
            extension.Extension = $ektron(this).text();
            arr.push(extension);
            return arr;
        }).get();
        $ektron('#uxExtensionsHidden').val(Ektron.JSON.stringify(items));
    },

    SerializeCharMap: function () {
        var items = $ektron(".replacementlistedit tr[id != 'trHeader']").map(function () {
            var arr = [];
            var row = $ektron(this);
            var replacementCharacter = new Object();
            replacementCharacter.Id = row.find('.uxHiddenCharacterID').val();
            replacementCharacter.MatchCharacter = row.find('.uxMatchCharacter').val();
            replacementCharacter.ReplaceCharacter = row.find('.uxReplacementCharacter').val();
            replacementCharacter.ConfigId = row.find('.uxHiddenConfigID').val();
            replacementCharacter.IsDefault = row.find('.uxHiddenIsDefault').val();
            arr.push(replacementCharacter);
            return arr;
        }).get();
        $ektron('#uxCharacterMapHidden').val(Ektron.JSON.stringify(items));
    },

    UserFolderValidation: function (value, element, errorMessage, contextInfo) {
        var isValid = false;
        var badChars = ['"', ',', '\\', '<', '>', ' ', ':', '|', '?', "'", '^', '%', '$', '!', '*', '#', ']'];
        for (var i = 0; i < badChars.length; i++) {
            if (value.indexOf(badChars[i]) != -1) {
                return false;
            }
        }
        return true;
    },

    GroupFolderValidation: function (value, element, errorMessage, contextInfo) {
        var isValid = false;
        var badChars = ['"', ',', '\\', '<', '>', ' ', ':', '|', '?', "'", '^', '%', '$', '!', '*', '#', ']'];
        for (var i = 0; i < badChars.length; i++) {
            if (value.indexOf(badChars[i]) != -1) {
                return false;
            }
        }
        return true;
    },

    FileExtensionValidation: function (value) {
        var isValid = false;
        var badChars = ['"', ',', '\\', '<', '>', ' ', ':', '|', '?', "'", '^', '%', '$', '!', '*', '#', ']', '@', '~'];
        for (var i = 0; i < badChars.length; i++) {
            if (value.indexOf(badChars[i]) != -1) {
                return false;
            }
        }

        return true;
    },

    URLValidation: function () {
        $ektron("#uxnewurlerrormessage").hide();
        $ektron("#uxORGurlerrormessage").hide();
        if ($ektron(".uxRedirectCodeDropDownList option:selected").val() == "404") {
            $ektron("#optionalmessage").show();
            //ValidatorEnable(uxNewURLRequired, false);
        }
        else {
            $ektron("#optionalmessage").hide();
            //    ValidatorEnable(uxNewURLRequired, true);
        }
    },

    RedirectURLValidation: function () {
        $ektron("#uxnewurlerrormessage").hide();
        $ektron("#uxORGurlerrormessage").hide();
        $ektron("#optionalmessage").hide();
        var isvalid = true;
        if ($ektron(".uxOriginalURL").val() != "" && ($ektron(".uxOriginalURL").val().toString().indexOf("://") != -1)) {
            $ektron("#uxORGurlerrormessage").show();
            return false;
        }

        if ($ektron(".uxNewURL").val() != "" && $ektron(".uxRedirectCodeDropDownList option:selected").val() != "404" && ($ektron(".uxNewURL").val().toString().indexOf("://") != -1)) {
            if ((/^([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test($ektron(".uxNewURL").val()))) {
                $ektron("#uxnewurlerrormessage").hide();
                return isvalid;
            } else {
                $ektron("#uxnewurlerrormessage").show();
                return false;
            }
        }

        if ($ektron(".uxNewURL").val() == "" && $ektron(".uxRedirectCodeDropDownList option:selected").val() != "404") {
            $ektron("#uxnewurlerrormessage").show();
            return false;
        }

        return isvalid;
    },

    HideMessage: function () {
        $ektron('#uxMessage').delay(3000).hide('200');
    },

    openFolderDialog: function () {
        $ektron("#uxSelectFolderDialog_Dialog_uxDialogControl").dialog("open");
    },

    openTaxonomyDialog: function () {
        $ektron("#uxTaxonomyDialog_Dialog_uxDialogControl").dialog("open");
    },

    openContentDialog: function () {
        $ektron("#uxContentSelectorDialog_Dialog_uxDialogControl").dialog("open");
    },

    getExampleUserGroupPreview: function (type) {
        var s = {
            selectedtype: type,
            extensionlist: "",
            alias: "",
            aliasprefix: ""
        };
        $ektron.extend(s, type);
        if (s.selectedtype == "user") {
            s.extensionlist = $ektron("#uxExtensionsForUserDropDownList option:selected").text();
            s.alias = '[DisplayName]';
            s.aliasprefix = $ektron("#uxUserAliasPath input").val();
        }
        else {
            s.extensionlist = $ektron("#uxExtensionsForGroupDropDownList option:selected").text();
            s.alias = '[GroupName]';
            s.aliasprefix = $ektron("#uxGroupAliasPath input").val();
        }
        if (typeof (s.extensionlist) != 'undefined') {
            $ektron("#uxExamplePreview").empty().html(s.aliasprefix + "/" + s.alias + s.extensionlist);
        }
    },

    getExamplePreview: function (folderpath) {
        var s = {
            selectedfolderpath: folderpath,
            aliasnametype: null,
            excludeFromPathList: $ektron("#uxExcludeFromPathList")[0]
        };
        $ektron.extend(s, folderpath);
        if (typeof (s.excludeFromPathList) != 'undefined' && s.excludeFromPathList.length > 0) {
            var selectedpath = s.excludeFromPathList.options[s.excludeFromPathList.selectedIndex].text;
            if (s.selectedfolderpath == "") {
                if (("/" + $ektron("#uxFolderPath_TextField_aspInput").val()) == selectedpath) {
                    s.selectedfolderpath = "";
                }
                else {
                    s.selectedfolderpath = $ektron("#uxFolderPath_TextField_aspInput").val().replace(selectedpath.substring(1, selectedpath.length), "");
                }
            }

            if ("1" == $ektron("#uxAliasFormatDropDownList").val()) {
                s.aliasNameType = "ContentTitle";
            }
            else if ("2" == $ektron("#uxAliasFormatDropDownList").val()) {
                s.aliasNameType = "999";
            }
            else {
                s.aliasNameType = "999/1033";
            }
            var url = s.selectedfolderpath + s.aliasNameType;
            if (url != "") {
                var ruleid = $ektron("#uxRuleId").val();
                var extension = $ektron("#uxExtensionDropDownList option:selected").text();

                Ektron.Workarea.UrlAliasing.replacecharactersInpreviewURL(Ektron.Workarea.UrlAliasing.CreateRequestObj(ruleid, url, "auto"), extension);
            }
        }
    },

    getExamplePreviewForTax: function (folderpath) {
        var s = {
            selectedfolderpath: folderpath,
            aliasnametype: null,
            excludeFromTaxPathList: $ektron("#uxExcludeFromTaxPathList")[0]
        };
        $ektron.extend(s, folderpath);
        if (typeof (s.excludeFromTaxPathList) != 'undefined') {
            var selectedpath = s.excludeFromTaxPathList.options[s.excludeFromTaxPathList.selectedIndex];
            if (typeof selectedpath != 'undefined') {
                var selectedpathString = selectedpath.text;

                if (s.selectedfolderpath == "") {
                    if (($ektron("#uxTaxonomyPath_TextField_aspInput").val().substring(1) + "/") == selectedpathString) {
                        s.selectedfolderpath = "";
                    }
                    else {
                        s.selectedfolderpath = $ektron("#uxTaxonomyPath_TextField_aspInput").val().replace(selectedpathString.substring(1, selectedpathString.length), "");
                    }
                }

                if ("1" == $ektron("#uxAliasTaxFormatDropDownList").val()) {
                    s.aliasNameType = "ContentTitle";
                }
                else if ("2" == $ektron("#uxAliasTaxFormatDropDownList").val()) {
                    s.aliasNameType = "999";
                }
                else {
                    s.aliasNameType = "999/1033";
                }
                var url = s.selectedfolderpath + s.aliasNameType;
                if (url != "") {
                    var ruleid = $ektron("#uxRuleId").val();
                    var extension = $ektron("#uxTaxExtensionDropDownList option:selected").text();

                    Ektron.Workarea.UrlAliasing.replacecharactersInpreviewURL(Ektron.Workarea.UrlAliasing.CreateRequestObj(ruleid, url, "auto"), extension);
                }
            }
        }
    },

    validateManualAliasLocally: function () {
        var aliasname = $ektron("#uxAliasName_TextField_aspInput").val();
        var badChars = [',', '\\', '<', '>', ' ', ':', '|', '?', '\'', '^', '%', '$', '!', '*', '#'];
        for (var i = 0; i < badChars.length; i++) {
            if (aliasname.indexOf(badChars[i]) != -1) {
                return false;
            }
        }
        return true;
    },

    validateAliasName: function () {
        var aliasname = $ektron("#uxAliasName_TextField_aspInput").val();
        $ektron("#uxUrlAliasError").hide();

        if (typeof (aliasname) != "undefined" && aliasname != "") {
            aliasname = aliasname + $ektron("#uxExtensionDropDownList option:selected").val();

            var contentid = $ektron("#frm_content_id").val();

            if (typeof (contentid) != "undefined" && contentid != null) {
            }
            else {
                contentid = "0";
            }
            var args = Ektron.Workarea.UrlAliasing.CreateRequestObj($ektron("#uxaliasId").val(), aliasname, "manual")
            $ektron.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "js/ektronworkareaurlaliasing.ashx?contentid=" + contentid,
                data: { "request": args },
                success: function (msg) {
                    if (msg == "error") {
                        $ektron("#uxUrlAliasError").show();
                    } else {
                        $ektron("#uxUrlAliasError").hide();
                    }
                    if (msg == "errorInvalidCharacters") {
                        //$ektron("#uxUrlAliasErrorInvalid").show();
                    } else {
                        //$ektron("#uxUrlAliasErrorInvalid").hide();
                    }
                }
            });

        }
    },
    replacecharactersInpreviewURL: function (url, extension) {
        $ektron.ajax({
            type: "POST",
            cache: false,
            async: false,
            url: "js/ektronworkareaurlaliasing.ashx",
            data: { "request": url },
            success: function (msg) {
                $ektron("#uxExamplePreview").empty().html(msg + extension);
            }
        });
    },
    CreateRequestObj: function (id, option, type) {
        request = {
            "Id": id,
            "Option": option,
            "Type": type
        };
        return Ektron.JSON.stringify(request);
    },
    getFolderExcludePathList: function () {
        var se = {
            pathlistoption: "",
            selectedfolderpath: $ektron("#uxFolderPath_TextField_aspInput").val()
        };
        if (se.selectedfolderpath != "") {
            se.pathlistoption += "<option>" + $ektron("#uxPleaseSelect").val() + "</option>";
            var nodes = se.selectedfolderpath.split('/');
            var prePath = '/';
            for (var i = 0; i < nodes.length - 1; i++) {
                prePath += nodes[i] + '/';
                if (prePath == $ektron("#uxselectedExcludePath").val()) {
                    se.pathlistoption += "<option selected=\"\">" + prePath + "</option>";
                }
                else {
                    se.pathlistoption += "<option>" + prePath + "</option>";
                }
            }
        }
        $ektron('#uxExcludeFromPathList option').remove();
        $ektron('#uxExcludeFromPathList').append(se.pathlistoption);
    },

    getTaxonomyExcludePathList: function () {
        var se = {
            pathlistoption: "",
            selectedpath: $ektron("#uxTaxonomyPath_TextField_aspInput").val()
        };
        if (se.selectedpath) {
            if (se.selectedpath != "") {
                se.pathlistoption += "<option>" + $ektron("#uxPleaseSelect").val() + "</option>";
                var nodes = se.selectedpath.split('/');
                var prePath = '/';
                for (var i = 0; i < nodes.length - 1; i++) {
                    prePath += nodes[i] + '/';
                    if (prePath == $ektron("#uxselectedExcludePath").val()) {
                        se.pathlistoption += "<option selected=\"\">" + prePath + "</option>";
                    }
                    else {
                        se.pathlistoption += "<option>" + prePath + "</option>";
                    }
                }
            }
        }
        $ektron('#uxExcludeFromTaxPathList option').remove();
        $ektron('#uxExcludeFromTaxPathList').append(se.pathlistoption);
    },

    RemoveExtension: function () {
        var enabled = $ektron('div.extensionControlBlock #btnRemoveExt').is('[readonly = "readonly"]');
        if (!enabled) {
            var currentExt = $ektron('div.extensionListBlock #uxFileExtension option:selected').text();
            if (!currentExt.match(/\/|.aspx/)) {
                $ektron('div.extensionListBlock #uxFileExtension option:selected').remove();
            } else {
                alert('This default extension can not be removed.');
            }
        }
    },

    AddExtension: function () {
        var enabled = $ektron('div.extensionControlBlock #btnAdd').is('[readonly = "readonly"]');
        if (!enabled) {
            var isExist = false;
            var newExt = $ektron('div.extensionControlBlock #tbNewExt').val().toLowerCase();
            if (newExt) {
                if (Ektron.Workarea.UrlAliasing.FileExtensionValidation(newExt)) {
                    if (!newExt.match(/\/|.aspx/)) {
                        if (!newExt.match(/^\./)) {
                            newExt = "." + newExt;
                        }
                        $ektron('#uxFileExtension option:contains("' + newExt + '")').each(function () {
                            if ($ektron(this).text() == newExt) {
                                $ektron('div.extensionControlBlock #tbNewExt').val('');
                                isExist = true;
                            }
                        });

                        if (!isExist) {
                            $ektron('div.extensionListBlock #uxFileExtension')
                .append($("<option></option>")
                            //                .attr("value", newExt)
                .text(newExt));
                            $ektron('div.extensionControlBlock #tbNewExt').val('');
                        } else {
                            alert('This extension already exists');
                        }
                    } else {
                        $ektron('div.extensionControlBlock #tbNewExt').val('');
                        alert('This extension already exists');
                    }
                }
                else {
                    alert('Please enter Valid extension');
                }

            } else {
                alert('Please enter an extension');
            }

        }
    }
};

function ReturnChildValue(folderid, folderpath, targetFolderIsXml) {
    if (folderid == 0 || folderpath == '\\') {
        return false;
    }

    $ektron("#uxSelectFolderDialog_Dialog_uxDialogControl").dialog("close");
    $ektron("#uxTaxonomyDialog_Dialog_uxDialogControl").dialog("close");
    folderpath = folderpath.replace(/\\/g, '/');
    $ektron("#frm_hdnSourceId").val(folderid);
    folderpath = folderpath.substring(1) + '/';
    $ektron("#uxselectedExcludePath").val("");
    $ektron("#uxFolderPath_TextField_aspInput").focus().val(folderpath).focus();
    $ektron('#uxTaxonomyPath_TextField_aspInput').focus().val(folderpath).focus();
    Ektron.Workarea.UrlAliasing.getExamplePreview({ selectedfolderpath: folderpath });
    Ektron.Workarea.UrlAliasing.getExamplePreviewForTax({ selectedfolderpath: folderpath });
}

function SetBrowserState() {
    $ektron("#frm_content_id_validation_box_TextField_aspInput").val($ektron(".frm_content_id").val());
    $ektron("#uxContentSelectorDialog_Dialog_uxDialogControl").dialog("close");
    $ektron.ajax({
        url: "../AJAXbase.aspx?action=getcontenttemplates&id=" + $ektron('.frm_content_id').val() + "&LangType=" + $ektron('.frm_content_langid').val(),
        cache: false,
        success: function (html) {
            $ektron('#uxQuicklinkSelector option').remove();
            $ektron('#uxQuicklinkSelector').append(html);
            return true;
        }
    });
    $ektron("#uxValidationMessageUxContentSelector_Label").removeClass("ektron-ui-invalid").attr("style", "").text("");
    Ektron.Workarea.UrlAliasing.validateAliasName();
    return false;
}