﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UrlAliasingSettings.aspx.cs" Inherits="Ektron.Workarea.UrlAliasing.UrlAliasingSettings" meta:resourcekey="PageResource1" %>
<%@ OutputCache Location="None" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta http-equiv="Cache-control" content="no-cache" />
    <script type="text/javascript" language="javascript">
        function RollOver(MyObj) {
            $ektron(MyObj).parent().addClass("button-over");
            $ektron(MyObj).parent().removeClass("button");
        }

        function RollOut(MyObj) {
            $ektron(MyObj).parent().addClass("button");
            $ektron(MyObj).parent().removeClass("button-over");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ektronUI:Css ID="aliasCSS" Path="CSS/Ektron.Workarea.UrlAliasing.css" runat="server">
    </ektronUI:Css>
    <ektronUI:JavaScript ID="aliasJS" runat="server" Path="js/ektron.workarea.urlaliasing.js" />
    <ektronUI:JavaScriptBlock ID="aliasJSLauncher" runat="server">
        <ScriptTemplate>
            Ektron.Workarea.UrlAliasing.initSettings();
        </ScriptTemplate>
    </ektronUI:JavaScriptBlock>
    <!-- New URL aliasing Module -->
    <div class="ektronPageHeader">
        <div class="ektronTitlebar" id="divTitleBar" runat="server">
            <span id="WorkareaTitleBar">
                <asp:Literal ID="aspHeaderTextId" runat="server" meta:resourcekey="aspHeaderTextId" />
            </span><span id="_WorkareaTitleBar" style="display: none;"></span>
        </div>
        <div class="ektronToolbar" id="divToolBar" runat="server">
            <table>
                <tr>
                    <asp:Literal ID="uxBackButton" runat="server"></asp:Literal>
                    <td class="column-PrimaryButton">
                        <asp:LinkButton runat="server" ID="uxEditButton" class="primary editButton" OnClick="EditButton_Click"
                            meta:resourcekey="uxedittxt_labelBase" />
                        <asp:LinkButton runat="server" ID="uxSaveButton" Visible="false" meta:resourcekey="uxSavetxt_labelBase"
                            ValidationGroup="saveSettings" OnClientClick="Ektron.Workarea.UrlAliasing.SaveSettings();"
                            class="primary saveButton" OnClick="SaveButton_Click" />
                    </td>
                    <td class="column-clearCacheButton">
                        <div>
                            <asp:ImageButton runat="server" ID="clearCacheButton" meta:resourcekey="clearCacheButton"
                                class="deleteButton" OnClick="ClearCacheButton_Click" ImageUrl="../Images/UI/Icons/driveDelete.png" />
                        </div>
                    </td>
                    <td>
                        <div class="actionbarDivider">
                        </div>
                    </td>
                    <td>
                        <asp:Literal runat="server" ID="aspHelpButton" meta:resourcekey="aspHelpButton" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="ektronPageContainer ektronPageInfo">
        <ektronUI:Message ID="uxMessage" runat="server" Visible='<%# this.showMessage %>'>
        </ektronUI:Message>
        <div class="ektrongridone">
            <asp:Panel ID="URLAliasingSettings" runat="server">
                <table class="urlAliasGlobalSettings">
                    <tr>
                        <td class="urlGlobalLabel">
                            <asp:Label ID="uxGlobalAliasLabel" runat="server" meta:resourcekey="uxGlobalAliasLabel" />
                        </td>
                        <td class="urlGlobalInput">
                            <asp:CheckBox ID="uxGlobalEnableCheckBox" runat="server" Enabled='<%#this.IsEditable %>'
                                meta:resourcekey="uxGlobalEnableCheckBox" />
                        </td>
                        <td class="urlGlobalLabel">
                            <asp:Label ID="uxRedirectsLabel" runat="server" meta:resourcekey="uxRedirectsLabel" />
                        </td>
                        <td class="urlGlobalInput">
                            <asp:CheckBox ID="uxRedirectsEnabledCheckBox" runat="server" Enabled='<%#this.IsEditable %>'
                                meta:resourcekey="uxredirectsenabledcheckbox" />
                        </td>
                    </tr>
                </table>
                <table class="ektronGrid urlAliasSettings">
                    <tr class="title-header">
                        <th title="Types" class="column-type urlTypesLabel">
                            <asp:Label ID="uxTypesLabel" runat="server" meta:resourcekey="uxTypesLabel" />
                        </th>
                        <th title="Enabled" class="column-enabled">
                            <asp:Label ID="uxEnabledLabel" runat="server" meta:resourcekey="uxEnabledLabel" />
                        </th>
                    </tr>
                    <tr class="ManualAliasSettings">
                        <td class="urlTypesLabel">
                            <asp:Label ID="uxManualLabel" runat="server" meta:resourcekey="uxManualLabel" />
                        </td>
                        <td>
                            <asp:CheckBox ID="uxManualAliasEnabledCheckBox" runat="server" Enabled='<%#this.IsAliasEditable && this.IsEditable%>'
                                meta:resourcekey="uxManualAliasEnabledCheckBox" />
                        </td>
                    </tr>
                    <tr class="FolderAliasSettings">
                        <td class="urlTypesLabel">
                            <asp:Label ID="uxFolderAliasLabel" runat="server" meta:resourcekey="uxFolderAliasLabel" />
                        </td>
                        <td>
                            <asp:CheckBox ID="uxFolderAliasEnabledCheckBox" runat="server" Enabled='<%#this.IsAliasEditable && this.IsEditable %>'
                                meta:resourcekey="uxFolderAliasEnabledCheckBox" />
                        </td>
                    </tr>
                    <tr class="TaxonomyAliasSettings">
                        <td class="urlTypesLabel">
                            <asp:Label ID="uxTaxonomyAliasLabel" runat="server" meta:resourcekey="uxTaxonomyAliasLabel" />
                        </td>
                        <td>
                            <asp:CheckBox ID="uxTaxonomyAliasEnabledCheckBox" runat="server" Enabled='<%#this.IsAliasEditable && this.IsEditable %>'
                                meta:resourcekey="uxTaxonomyAliasEnabledCheckBox" />
                        </td>
                    </tr>
                    <tr class="UserAliasSettings">
                        <td class="urlTypesLabel">
                            <asp:Label ID="uxUserAliasLabel" runat="server" meta:resourcekey="uxUserAliasLabel" />
                        </td>
                        <td>
                            <asp:CheckBox ID="uxUserAliasEnabledCheckBox" runat="server" Enabled='<%#this.IsAliasEditable && this.IsEditable %>'
                                meta:resourcekey="uxUserAliasEnabledCheckBox" />
                        </td>
                    </tr>
                    <tr class="GroupAliasSettings">
                        <td class="urlTypesLabel">
                            <asp:Label ID="uxGroupAliasLabel" runat="server" meta:resourcekey="uxGroupAliasLabel" />
                        </td>
                        <td>
                            <asp:CheckBox ID="uxGroupAliasEnabledCheckBox" runat="server" Enabled='<%# this.IsAliasEditable && this.IsEditable %>'
                                meta:resourcekey="uxGroupAliasEnabledCheckBox" />
                        </td>
                    </tr>
                    <tr class="RegExAliasSettings">
                        <td class="urlTypesLabel">
                            <asp:Label ID="uxRegExAliasLabel" runat="server" meta:resourcekey="uxRegExAliasLabel" />
                        </td>
                        <td>
                            <asp:CheckBox ID="uxRegExAliasEnabledCheckBox" runat="server" Enabled='<%#this.IsAliasEditable && this.IsEditable %>'
                                meta:resourcekey="uxRegExAliasEnabledCheckBox" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <div class="ektronTopSpace">
        </div>
        <div class="ektrongridtwo">
            <asp:Panel ID="UrlAliasingAdvanceSettings" runat="server">
                <table class="ektronForm urlAliasSettings">
                    <tr>
                        <td class="label">
                            <asp:Label ID="uxExtensionLabel" runat="server" meta:resourcekey="uxExtensionLabel" />
                        </td>
                        <td class="value">
                            <div class="extensionListBlock">
                                <asp:ListBox ID="uxFileExtension" runat="server" DataTextField="Extension" DataValueField="Id">
                                </asp:ListBox>
                                <input type="hidden" id="uxExtensionsHidden" runat="server" />
                            </div>
                            <asp:PlaceHolder ID="uxextensionsec" runat="server" Visible="false">
                                <div class="extensionControlBlock">
                                    <asp:Button ID="btnRemoveExt" runat="server" Text="Remove" Enabled='<%#this.IsAliasEditable && this.IsEditable %>' /><br />
                                    <p>
                                    </p>
                                    <asp:TextBox ID="tbNewExt" runat="server" Enabled='<%#this.IsAliasEditable && this.IsEditable %>'
                                        EnableViewState="false"></asp:TextBox>
                                    <asp:Button ID="btnAdd" runat="server" Text="Add" Enabled='<%#this.IsAliasEditable && this.IsEditable %>' />
                                </div>
                            </asp:PlaceHolder>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="uxOverrideTemplateLabel" runat="server" meta:resourcekey="uxOverrideTemplateLabel" />
                        </td>
                        <td class="value">
                            <asp:CheckBox ID="uxOverrideTemplateCheckBox" runat="server" meta:resourcekey="uxOverrideTemplateCheckBox"
                                Enabled='<%#this.IsAliasEditable && this.IsEditable %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="uxFallBackLangLabel" runat="server" meta:resourcekey="uxFallBackLangLabel" />
                        </td>
                        <td class="value">
                            <asp:CheckBox ID="uxFallBackLangCheckBox" runat="server" meta:resourcekey="uxFallBackLangCheckBox"
                                Enabled='<%#this.IsAliasEditable && this.IsEditable %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="uxDefaultReplacementCharLabel" runat="server" meta:resourcekey="uxDefaultReplacementCharLabel" />
                        </td>
                        <td class="value">
                            <ektronUI:TextField ID="uxDefaultReplacementChar" runat="server" meta:resourcekey="uxDefaultReplacementChar"
                                Enabled='<%#this.IsAliasEditable && this.IsEditable %>' ValidationGroup="saveSettings">
                                    <ValidationRules>
                                        <ektronUI:RequiredRule ErrorMessage="<%$ Resources:CharacterRequired %>" ServerValidationEnabled="false"  />
                                        <ektronUI:MaxLengthRule MaxLength="1" ErrorMessage="<%$ Resources:CharacterSingle %>" ServerValidationEnabled="false" />
                                    </ValidationRules>
                            </ektronUI:TextField>
                            <ektronUI:ValidationMessage ID="uxvalidationMessageRepChar" runat="server" AssociatedControlID="uxDefaultReplacementChar" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="uxQueryStringActionLabel" runat="server" meta:resourcekey="uxQueryStringActionLabel" />
                        </td>
                        <td class="value">
                            <asp:DropDownList ID="uxQueryStringActionList" runat="server" meta:resourcekey="uxQueryStringActionList"
                                Enabled='<%#this.IsAliasEditable && this.IsEditable %>'>
                                <asp:ListItem Text="<%$ Resources:ListNone %>" Value="0" />
                                <asp:ListItem Text="<%$ Resources:ListReplaceAll %>" Value="1" />
                                <asp:ListItem Text="<%$ Resources:ListAppendParameters %>" Value="2" />
                                <asp:ListItem Text="<%$ Resources:ListResolveParameters %>" Value="3" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
    </form>
</body>
</html>
