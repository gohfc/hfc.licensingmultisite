﻿namespace Ektron.Workarea.UrlAliasing
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web.Script.Serialization;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Organization;
    using Ektron.Cms.Framework.Settings.UrlAliasing;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Organization;
    using Ektron.Cms.Settings;
    using Ektron.Cms.Settings.UrlAliasing.DataObjects;
    using Microsoft.Security.Application;

    public partial class AddAutomaticAliasingRule : Ektron.Cms.Workarea.Page
    {
        #region Variables

        SiteAPI siteAPI = new SiteAPI();
        public string disabledString { get { return this.IsEditable ? "" : "disabled='true'"; } }
        private ISite site;
        private AliasRuleManager manager = new AliasRuleManager();
        private JavaScriptSerializer aJsSerializer = new JavaScriptSerializer();
        private const string querystringKey = "id";
        public AliasRuleData rule { get; set; }
        protected DisplayMode displayMode;
        public string excludepath = "";

        [System.ComponentModel.DefaultValue(false)]
        public bool showMessage { get; set; }
        [System.ComponentModel.DefaultValue(false)]
        public bool IsEditable { get; set; }
        public string sitePath;
        AliasSettings urlAliasingSettingsData;
        protected bool addTitleVisible { get { return displayMode == DisplayMode.AddStep1 || displayMode == DisplayMode.AddStep2; } }
        protected bool editTitleVisible { get { return displayMode == DisplayMode.Edit; } }
        protected bool viewTitleVisible { get { return displayMode == DisplayMode.View || displayMode == DisplayMode.None; } }
        #endregion

        #region Page Controls Events


        protected override void OnInit(EventArgs e)
        {

            if (this.HasViewPermission())
            {
                sitePath = siteAPI.SitePath;
                AliasSettingsManager urlAliasSettingsManager = new AliasSettingsManager();
                urlAliasingSettingsData = urlAliasSettingsManager.Get();
                try
                {
                    if (!String.IsNullOrEmpty(Request.QueryString[querystringKey]))
                    {
                        rule = manager.GetItem(long.Parse(Request.QueryString[querystringKey]));
                        if (rule != null && rule.Id == 0)
                        {
                            rule = null;
                        }
                    }

                    this.IsEditable = ((rule != null && Page.IsPostBack) || (rule == null && !Page.IsPostBack));
                    this.initWorkareaUI();
                    this.displayMode = this.getDisplayMode();
                    this.setUI();
                    if (rule != null && rule.Id != 0)
                    {
                        uxExamplePreview.Text = GeneratePreview(rule);
                        uxRuleId.Value = rule.Id.ToString();
                    }

                }
                catch (Exception exc)
                {
                    //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                    Utilities.ShowError(exc.Message);
                }
            }
            else
            {
                Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // for inline databinding
            this.DataBind();
        }

        protected void uxEditButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.displayMode = DisplayMode.Edit;
                this.showTypePanel(rule.Type);
                this.bindData(rule);
                this.setUI();

            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
        }

        protected void uxSaveButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                bool isSuccess = false;
                try
                {
                    FolderManager FManager = new FolderManager(Cms.Framework.ApiAccessMode.Admin);
                    TaxonomyManager tmanager = new TaxonomyManager(Cms.Framework.ApiAccessMode.Admin);

                    if (rule == null)
                    {
                        rule = new AliasRuleData();
                        rule.Type = (EkEnumeration.AliasRuleType)int.Parse(uxSourceTypeDropDownList.SelectedValue);
                        rule.SiteId = long.Parse(uxSiteList.SelectedValue);
                        rule.LanguageId = int.Parse(uxLanguageList.SelectedValue);
                    }

                    string excluderootsitepath = string.Empty;

                    if (rule.SiteId > 0)
                    {
                        var fdata = FManager.GetItem(rule.SiteId);
                        if (fdata != null && fdata.IsDomainFolder)
                            excluderootsitepath = fdata.NameWithPath;
                    }

                    rule.Name = AntiXss.GetSafeHtmlFragment(uxNameTextBox.Text);
                    rule.IsEnabled = uxActiveCheckBox.Checked;
                    rule.ReplacementCharacterMap = aJsSerializer.Deserialize<List<ReplacementCharacter>>(uxCharacterMapHidden.Value);
                    switch (rule.Type)
                    {
                        case EkEnumeration.AliasRuleType.Folder:
                            if (Request["uxExcludeFromPathList"] != null && Request["uxExcludeFromPathList"].Length > 0 && Request["uxExcludeFromPathList"].Contains("/"))
                            {

                                FolderCriteria crit = new FolderCriteria();
                                excluderootsitepath = excluderootsitepath + Request["uxExcludeFromPathList"].TrimStart('/');

                                crit.AddFilter(FolderProperty.FolderPath, CriteriaFilterOperator.EqualTo, excluderootsitepath);
                                List<FolderData> fol = FManager.GetList(crit);
                                if (fol.Count > 0) rule.ExcludedPathId = fol[0].Id;
                            }
                            else
                            {
                                if (Request["uxExcludeFromPathList"] != null && Request["uxExcludeFromPathList"] == uxPleaseSelect.Value.ToString())
                                {
                                    rule.ExcludedPathId = 0;
                                }
                            }

                            rule.SourceId = long.Parse(frm_hdnSourceId.Value);
                            rule.PageTypeId = (EkEnumeration.AutoAliasNameType)int.Parse(uxAliasFormatDropDownList.SelectedValue);
                            rule.FileExtensionId = long.Parse(uxExtensionDropDownList.SelectedValue);
                            rule.TargetTemplateSource = EkEnumeration.TargetTemplateSource.Quicklink;
                            //rule.ExcludedPathId = uxE
                            rule.SourceParamName = uxQueryStringParam.Text;
                            break;
                        case EkEnumeration.AliasRuleType.Taxonomy:
                            if (Request["uxExcludeFromTaxPathList"] != null && Request["uxExcludeFromTaxPathList"].Length > 0 && Request["uxExcludeFromTaxPathList"].Contains("/"))
                            {
                                var path = Request["uxExcludeFromTaxPathList"].TrimEnd('/');
                                path = path.Replace("/", "\\");
                                TaxonomyCriteria crit = new TaxonomyCriteria();
                                crit.AddFilter(TaxonomyProperty.Path, CriteriaFilterOperator.EqualTo, path);
                                List<TaxonomyData> taxlist = tmanager.GetList(crit);
                                if (taxlist.Any()) rule.ExcludedPathId = taxlist[0].Id;
                            }
                            else
                            {
                                if (Request["uxExcludeFromTaxPathList"] != null && Request["uxExcludeFromTaxPathList"] == uxPleaseSelect.Value.ToString())
                                {
                                    rule.ExcludedPathId = 0;
                                }
                            }

                            rule.SourceId = long.Parse(frm_hdnSourceId.Value);
                            rule.PageTypeId = (EkEnumeration.AutoAliasNameType)int.Parse(uxAliasTaxFormatDropDownList.SelectedValue);
                            rule.FileExtensionId = long.Parse(uxTaxExtensionDropDownList.SelectedValue);
                            rule.TargetTemplateSource = (EkEnumeration.TargetTemplateSource)int.Parse(uxAliasTemplateSource.SelectedValue);
                            //rule.ExcludedPathId = 
                            rule.SourceParamName = uxTaxQueryStringParam.Text;
                            break;
                        case EkEnumeration.AliasRuleType.User:
                            rule.Prefix = uxUserAliasPath.Text;
                            rule.FileExtensionId = long.Parse(uxExtensionsForUserDropDownList.SelectedValue);
                            ////rule.SourceParamName = uxQueryStringParamForUser.Text;
                            rule.TargetTemplateSource = EkEnumeration.TargetTemplateSource.Quicklink;
                            break;
                        case EkEnumeration.AliasRuleType.Group:
                            rule.Prefix = uxGroupAliasPath.Text;
                            rule.FileExtensionId = long.Parse(uxExtensionsForGroupDropDownList.SelectedValue);
                            ////rule.SourceParamName = uxQueryStringParamForGroup.Text;
                            rule.TargetTemplateSource = EkEnumeration.TargetTemplateSource.Quicklink;
                            break;
                    }

                    rule.AliasPriority = Convert.ToInt32(this.uxPriorityField.Value);
                    if (rule.Id != 0)
                    {
                        rule = manager.Update(rule);
                    }
                    else
                    {
                        rule = manager.Add(rule);
                    }
                    isSuccess = true;
                }
                catch (Exception exc)
                {
                    isSuccess = false;
                    this.setUI();
                    if (exc as SqlException != null && exc.Message.Contains("2601"))
                    {
                        this.showMessage = true;
                        uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                        uxMessage.Text = GetLocalResourceObject("DuplicateRule").ToString();
                    }
                    else
                    {
                        //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                        //Utilities.ShowError(exc.Message);
                        this.showMessage = true;
                        uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                        uxMessage.Text = exc.Message;
                    }
                    return;
                }

                if (isSuccess)
                {
                    Response.Redirect("AutomaticAliasingRules.aspx");
                }
            }
            else
            {
                this.showMessage = true;
                uxMessage.DisplayMode = Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Error;
                uxMessage.Text = GetLocalResourceObject("SaveFail").ToString();
            }
        }

        protected void uxNextButton_Click(object sender, EventArgs e)
        {
            this.displayMode = DisplayMode.AddStep2;
            this.setUI();
        }

        protected void uxDelete_Click(object sender, EventArgs e)
        {
            try
            {
                manager.Delete(rule.Id);
            }
            catch (Exception exc)
            {
                //Utilities.ShowError(GetLocalResourceObject("ExceptionMessage").ToString());
                Utilities.ShowError(exc.Message);
            }
            Response.Redirect("AutomaticAliasingRules.aspx", true);
        }

        /// <summary>
        /// Build the Example Preview
        /// </summary>
        /// <param name="config"> AliasRuleData </param>
        /// <returns> URL </returns>
        protected string GeneratePreview(AliasRuleData config)
        {
            AliasSettingsManager settingsManager = new AliasSettingsManager();

            ContentAPI _capi = new ContentAPI();
            _capi.ContentLanguage = config.LanguageId;

            string folderpath = "";
            string extension = (config.FileExtensionId != 0) ? settingsManager.GetExtension(config.FileExtensionId).Extension : "";
            string format = "";

            switch (config.Type)
            {
                case EkEnumeration.AliasRuleType.Folder:
                    folderpath = _capi.GetPathByFolderID(config.SourceId);
                    excludepath = _capi.GetPathByFolderID(config.ExcludedPathId);
                    if (config.SiteId > 0)
                    {
                        folderpath = folderpath.TrimStart('\\');
                        if (folderpath.IndexOf('\\') > 0) folderpath = folderpath.Substring(folderpath.IndexOf('\\'));
                        excludepath = excludepath.TrimStart('\\');
                        if (excludepath.IndexOf('\\') > 0) excludepath = excludepath.Substring(excludepath.IndexOf('\\'));
                    }

                    format = getFormatString(config.PageTypeId);
                    uxselectedExcludePath.Value = excludepath.Replace("\\", "/") + "/";
                    break;
                case EkEnumeration.AliasRuleType.Group:
                    format = "GroupName";
                    folderpath = config.Prefix;
                    break;
                case EkEnumeration.AliasRuleType.Taxonomy:
                    folderpath = getTaxonomyPathById(config.SourceId, config.LanguageId);
                    excludepath = getTaxonomyPathById(config.ExcludedPathId, config.LanguageId);
                    format = getFormatString(config.PageTypeId);
                    uxselectedExcludePath.Value = excludepath.Replace("\\", "/") + "/";
                    break;
                case EkEnumeration.AliasRuleType.User:
                    folderpath = config.Prefix;
                    format = "UserName";
                    break;
                default:
                    return "";
            }

            excludepath = excludepath.Trim('/');
            folderpath = folderpath.TrimStart('/');
            if (folderpath.IndexOf(excludepath) >= 0)
                folderpath = folderpath.Substring(excludepath.Length);
            if (!folderpath.EndsWith("/")) folderpath = folderpath + "/";
            folderpath = folderpath.Replace("\\", "/");

            string fullpath = folderpath + format;
            fullpath = fullpath.Replace("//", "/");
            fullpath = fullpath.TrimStart('/');

            ReplacementCharacterManager rm = new ReplacementCharacterManager(Cms.Framework.ApiAccessMode.Admin);
            config.ReplacementCharacterMap = rm.GetList(config.Id);

            if (config.ReplacementCharacterMap != null)
            {
                foreach (var x in config.ReplacementCharacterMap)
                {
                    fullpath = fullpath.Replace(x.MatchCharacter, x.ReplaceCharacter);
                }
            }

            return fullpath + extension;
        }

        #endregion

        #region internal methods

        private string getTaxonomyPathById(long id, int language)
        {
            TaxonomyManager tm = new TaxonomyManager(Cms.Framework.ApiAccessMode.Admin);
            if (language > 0) tm.ContentLanguage = language;
            TaxonomyData td = tm.GetItem(id);
            return td.Path;
        }

        private string getFormatString(EkEnumeration.AutoAliasNameType x)
        {
            switch (x)
            {
                case EkEnumeration.AutoAliasNameType.ContentId:
                    return "999";
                case EkEnumeration.AutoAliasNameType.ContentIdAndLanguage:
                    return "999/1033";
                case EkEnumeration.AutoAliasNameType.ContentTitle:
                    return "Content Title";
            }
            return x.ToDescriptionString();
        }

        private void initWorkareaUI()
        {
            this.RegisterResources();

            ContentAPI refContentApi = new ContentAPI();
            EkMessageHelper msgHelper = refContentApi.EkMsgRef;
            StyleHelper styleHelper = new StyleHelper();
            try
            {
                this.aspHelpButton.Text = styleHelper.GetHelpButton("urlaliasing_rules_automatic_detail", string.Empty);
            }
            catch { }
        }

        private void setBackButton()
        {
            ContentAPI refContentApi = new ContentAPI();
            EkMessageHelper msgHelper = refContentApi.EkMsgRef;
            StyleHelper styleHelper = new StyleHelper();
            string returnpath = refContentApi.RequestInformationRef.ApplicationPath;
            switch (this.displayMode)
            {
                case DisplayMode.AddStep1:
                    returnpath += "UrlAliasing/AutomaticAliasingRules.aspx?mode=auto";
                    break;
                case DisplayMode.AddStep2:
                    returnpath += "UrlAliasing/AddEditViewAutomaticAliasingRule.aspx";
                    break;
                case DisplayMode.Edit:
                    returnpath += "UrlAliasing/AddEditViewAutomaticAliasingRule.aspx?" + querystringKey + "=" + Request.QueryString[querystringKey];
                    break;
                case DisplayMode.None:
                    returnpath = Request.UrlReferrer.OriginalString;
                    break;
                case DisplayMode.View:
                    returnpath += "UrlAliasing/AutomaticAliasingRules.aspx?mode=auto";
                    break;
            }
            uxBackButton.Text = styleHelper.GetButtonEventsWCaption(refContentApi.RequestInformationRef.AppImgPath + "../UI/Icons/back.png", returnpath, msgHelper.GetMessage("alt back button"), msgHelper.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true);
        }

        private void setUI()
        {
            if (!Page.IsPostBack)
            {
                this.bindUIOptions();
            }
            addAutoInitJS.Visible = false;

            // View Mode
            if (displayMode == DisplayMode.View)
            {
                this.IsEditable = false;
                this.showTypePanel(rule.Type);
                this.bindData(rule);
                uxEditButton.Visible = this.HasEditPermission();
                uxDelete.Visible = this.HasEditPermission();
                uxSaveButton.Visible = false;
                uxSourceTypeDropDownList.Enabled = false;
                uxSiteList.Enabled = false;
                langRow.Visible = true;
                uxViewAssociatesButton.Visible = true;
                uxViewAssociatesButton.NavigateUrl = string.Format("Aliases.aspx?id={0}", rule.Id);
                uxBackButton.Visible = true;
                uxNextButton.Visible = false;
                uxReplacementCharListView.Visible = true;
                uxReplacementCharListEdit.Visible = false;
                uxPriorityField.Enabled = false;
            }

            // Edit mode
            if (displayMode == DisplayMode.Edit)
            {
                this.IsEditable = true;
                this.bindData(rule);
                uxEditButton.Visible = false;
                uxSaveButton.Visible = true;
                uxDelete.Visible = false;
                uxNextButton.Visible = false;
                uxBackButton.Visible = true;
                uxViewAssociatesButton.Visible = false;
                langRow.Visible = true;
                uxReplacementCharListView.Visible = false;
                uxReplacementCharListEdit.Visible = true;
                uxPriorityField.Enabled = true;
                uxUserAliasPath.Attributes.Add("onkeyup", "Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview('user');");
                uxUserAliasPath.Attributes.Add("onmouseup", "Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview('user);");
                uxGroupAliasPath.Attributes.Add("onkeyup", "Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview('group');");
                uxGroupAliasPath.Attributes.Add("onmouseup", "Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview('group');");
            }

            // Add Mode
            if (displayMode == DisplayMode.AddStep1)
            {
                this.IsEditable = true;
                this.showTypePanel(EkEnumeration.AliasRuleType.None);
                uxEditButton.Visible = false;
                uxSaveButton.Visible = false;
                uxDelete.Visible = false;
                uxSourceTypeDropDownList.Enabled = true;
                uxSiteList.Enabled = true;
                uxBackButton.Visible = true;
                autoRuleInitJS.Visible = false;
                addAutoInitJS.Visible = true;
                langRow.Visible = true;
                uxLanguageList.Enabled = true;
            }

            if (displayMode == DisplayMode.AddStep2)
            {
                EkEnumeration.AliasRuleType configType = (EkEnumeration.AliasRuleType)int.Parse(uxSourceTypeDropDownList.SelectedValue);
                showTypePanel(configType);
                uxReplacementCharactersPanel.Visible = true;
                List<ReplacementCharacter> defaultChars = new ReplacementCharacterManager().getDefault();
                uxReplacementCharListEdit.DataSource = defaultChars;
                uxCharacterMapHidden.Value = aJsSerializer.Serialize(defaultChars);
                uxReplacementCharListView.Visible = false;
                uxReplacementCharListEdit.Visible = true;
                this.IsEditable = true;
                uxSourceTypeDropDownList.Enabled = false;
                uxSiteList.Enabled = false;
                uxLanguageList.Enabled = false;
                uxSaveButton.Visible = true;
                uxNextButton.Visible = false;
                autoRuleInitJS.Visible = true;
                langRow.Visible = true;
                uxFolderPathSelectIframe.Src = "../urlAutoAliasSourceSelector.aspx?FolderID=" + uxSiteList.SelectedValue.ToString() + "&browser=1&WantXmlInfo=1&noblogfolders=0&mode=Folder";
                string lang = uxLanguageList.SelectedValue;
                if (lang == "-1") lang = ObjectFactory.GetRequestInfoProvider().GetRequestInformation().DefaultContentLanguage.ToString();
                uxTaxonomyPathSelectIframe.Src = "../urlAutoAliasSourceSelector.aspx?FolderID=0&browser=1&WantXmlInfo=1&noblogfolders=1&mode=Taxonomy&LangType=" + lang;
                uxUserAliasPath.Attributes.Add("onkeyup", "Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview('user');");
                uxUserAliasPath.Attributes.Add("onmouseup", "Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview('user);");
                uxGroupAliasPath.Attributes.Add("onkeyup", "Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview('group');");
                uxGroupAliasPath.Attributes.Add("onmouseup", "Ektron.Workarea.UrlAliasing.getExampleUserGroupPreview('group');");
            }
            setBackButton();
        }

        private void bindUIOptions()
        {
            bindLanguageList(siteAPI.DefaultContentLanguage);
            site = ObjectFactory.GetSite(this.siteAPI.RequestInformationRef);
            bindSiteList();
            bindTypeList();
            bindFormatLists();
            bindExtensions();

            uxAliasTemplateSource.Items.Add(new ListItem("Quicklink", Ektron.Cms.Common.EkEnumeration.TargetTemplateSource.Quicklink.GetHashCode().ToString()));
            uxAliasTemplateSource.Items.Add(new ListItem("Taxonomy Template", Ektron.Cms.Common.EkEnumeration.TargetTemplateSource.Taxonomy.GetHashCode().ToString()));
            uxAliasTemplateSource.DataBind();
        }

        private void bindExtensions()
        {
            AliasSettingsManager settingsManager = new AliasSettingsManager();
            List<FileExtension> extensions = settingsManager.GetAllExtensions();
            uxExtensionDropDownList.DataSource = extensions;
            uxExtensionDropDownList.DataValueField = "Id";
            uxExtensionDropDownList.DataTextField = "Extension";
            uxExtensionDropDownList.DataBind();

            uxTaxExtensionDropDownList.DataSource = extensions;
            uxTaxExtensionDropDownList.DataValueField = "Id";
            uxTaxExtensionDropDownList.DataTextField = "Extension";
            uxTaxExtensionDropDownList.DataBind();

            uxExtensionsForUserDropDownList.DataSource = extensions;
            uxExtensionsForUserDropDownList.DataValueField = "Id";
            uxExtensionsForUserDropDownList.DataTextField = "Extension";
            uxExtensionsForUserDropDownList.DataBind();

            uxExtensionsForGroupDropDownList.DataSource = extensions;
            uxExtensionsForGroupDropDownList.DataValueField = "Id";
            uxExtensionsForGroupDropDownList.DataTextField = "Extension";
            uxExtensionsForGroupDropDownList.DataBind();

        }

        private void bindFormatLists()
        {
            uxAliasFormatDropDownList.Items.Add(new ListItem(getLocalPageTypeName(EkEnumeration.AutoAliasNameType.ContentTitle), EkEnumeration.AutoAliasNameType.ContentTitle.GetHashCode().ToString()));
            uxAliasFormatDropDownList.Items.Add(new ListItem(getLocalPageTypeName(EkEnumeration.AutoAliasNameType.ContentId), EkEnumeration.AutoAliasNameType.ContentId.GetHashCode().ToString()));
            uxAliasFormatDropDownList.Items.Add(new ListItem(getLocalPageTypeName(EkEnumeration.AutoAliasNameType.ContentIdAndLanguage), EkEnumeration.AutoAliasNameType.ContentIdAndLanguage.GetHashCode().ToString()));
            uxAliasFormatDropDownList.DataBind();

            uxAliasTaxFormatDropDownList.Items.Add(new ListItem(getLocalPageTypeName(EkEnumeration.AutoAliasNameType.ContentTitle), EkEnumeration.AutoAliasNameType.ContentTitle.GetHashCode().ToString()));
            uxAliasTaxFormatDropDownList.Items.Add(new ListItem(getLocalPageTypeName(EkEnumeration.AutoAliasNameType.ContentId), EkEnumeration.AutoAliasNameType.ContentId.GetHashCode().ToString()));
            uxAliasTaxFormatDropDownList.Items.Add(new ListItem(getLocalPageTypeName(EkEnumeration.AutoAliasNameType.ContentIdAndLanguage), EkEnumeration.AutoAliasNameType.ContentIdAndLanguage.GetHashCode().ToString()));
            uxAliasTaxFormatDropDownList.DataBind();


        }

        private void bindTypeList()
        {
            //See EkEnumeration.AliasType
            if (urlAliasingSettingsData.IsTaxonomyAliasingEnabled)
            {
                uxSourceTypeDropDownList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Taxonomy), EkEnumeration.AliasRuleType.Taxonomy.GetHashCode().ToString()));
            }
            if (urlAliasingSettingsData.IsFolderAliasingEnabled)
            {
                uxSourceTypeDropDownList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Folder), EkEnumeration.AliasRuleType.Folder.GetHashCode().ToString()));
            }
            if (urlAliasingSettingsData.IsUserAliasingEnabled)
            {
                uxSourceTypeDropDownList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.User), EkEnumeration.AliasRuleType.User.GetHashCode().ToString()));
            }
            if (urlAliasingSettingsData.IsGroupAliasingEnabled)
            {
                uxSourceTypeDropDownList.Items.Add(new ListItem(getLocalTypeName(EkEnumeration.AliasRuleType.Group), EkEnumeration.AliasRuleType.Group.GetHashCode().ToString()));
            }
        }

        private void bindLanguageList(long defaultlanguage)
        {
            LanguageData[] language_data = this.siteAPI.GetAllActiveLanguages();

            var languages = from LanguageData in language_data
                            select new
                            {
                                Name = LanguageData.Name,
                                value = LanguageData.Id
                            };

            uxLanguageList.Items.Clear();
            uxLanguageList.DataSource = languages;
            uxLanguageList.DataTextField = "Name";
            uxLanguageList.DataValueField = "value";
            uxLanguageList.Items.Add(new ListItem("All", Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES.ToString()));
            uxLanguageList.SelectedValue = defaultlanguage.ToString();
        }

        private void bindSiteList()
        {
            Dictionary<long, string> siteList = site.GetSiteList();
            var displaySites = from sites in siteList
                               select new { Label = sites.Value, ID = sites.Key };
            uxSiteList.DataSource = displaySites;
            uxSiteList.DataTextField = "Label";
            uxSiteList.DataValueField = "ID";
            uxSiteList.DataBind();
        }

        private string getLocalPageTypeName(EkEnumeration.AutoAliasNameType nameType)
        {
            string returnVal;
            try
            {
                returnVal = GetLocalResourceObject("nameType" + nameType.GetHashCode().ToString()) as string;
            }
            catch
            {
                returnVal = nameType.ToDescriptionString();
            }
            return returnVal;
        }

        private string getLocalTypeName(EkEnumeration.AliasRuleType configType)
        {
            string returnVal;
            try
            {
                returnVal = GetLocalResourceObject("configType" + configType.GetHashCode().ToString()) as string;
            }
            catch
            {
                returnVal = configType.ToDescriptionString();
            }
            return returnVal;
        }

        private void showTypePanel(EkEnumeration.AliasRuleType configType)
        {
            switch (configType)
            {
                case EkEnumeration.AliasRuleType.Folder:
                    uxDefineFolderRulePanel.Visible = true;
                    uxReplacementCharactersPanel.Visible = true;
                    uxPreviewPanel.Visible = true;
                    break;
                case EkEnumeration.AliasRuleType.Taxonomy:
                    uxDefineTaxonomyRulePanel.Visible = true;
                    uxReplacementCharactersPanel.Visible = true;
                    uxPreviewPanel.Visible = true;
                    break;
                case EkEnumeration.AliasRuleType.User:
                    uxDefineUserRulePanel.Visible = true;
                    uxReplacementCharactersPanel.Visible = true;
                    uxPreviewPanel.Visible = true;
                    break;
                case EkEnumeration.AliasRuleType.Group:
                    uxDefineGroupRulePanel.Visible = true;
                    uxReplacementCharactersPanel.Visible = true;
                    uxPreviewPanel.Visible = true;
                    break;
                case EkEnumeration.AliasRuleType.None:

                    break;
            }
        }

        private void bindData(AliasRuleData rule)
        {
            uxNameTextBox.Text = rule.Name;
            uxActiveCheckBox.Checked = rule.IsEnabled;
            uxSourceTypeDropDownList.SelectedValue = rule.Type.GetHashCode().ToString();
            uxSiteList.SelectedValue = rule.SiteId.ToString();

            if (rule.LanguageId == 0 || rule.LanguageId == -1)
            {
                uxLanguageList.SelectedValue = "-1";
            }
            else
            {
                uxLanguageList.SelectedValue = rule.LanguageId.ToString();
            }


            uxReplacementCharListView.DataSource = rule.ReplacementCharacterMap;
            uxReplacementCharListEdit.DataSource = rule.ReplacementCharacterMap;
            uxCharacterMapHidden.Value = aJsSerializer.Serialize(rule.ReplacementCharacterMap);
            uxPriorityField.Value = rule.AliasPriority;

            switch (rule.Type)
            {
                case EkEnumeration.AliasRuleType.Folder:
                    bindFolderData(rule);
                    break;
                case EkEnumeration.AliasRuleType.Taxonomy:
                    bindTaxonomyData(rule);
                    break;
                case EkEnumeration.AliasRuleType.User:
                    bindUserData(rule);
                    break;
                case EkEnumeration.AliasRuleType.Group:
                    bindGroupData(rule);
                    break;
            }
        }

        private void bindGroupData(AliasRuleData rule)
        {
            uxGroupAliasPath.Text = rule.Prefix;
            uxExtensionsForGroupDropDownList.SelectedValue = rule.FileExtensionId.ToString();
            ////uxQueryStringParamForGroup.Text = rule.SourceParamName;
        }

        private void bindUserData(AliasRuleData rule)
        {
            uxUserAliasPath.Text = rule.Prefix;
            uxExtensionsForUserDropDownList.SelectedValue = rule.FileExtensionId.ToString();
            ////uxQueryStringParamForUser.Text = rule.SourceParamName;
        }

        private void bindTaxonomyData(AliasRuleData rule)
        {
            TaxonomyManager tManager = new TaxonomyManager();
            if (rule.LanguageId > 0) tManager.ContentLanguage = rule.LanguageId;
            TaxonomyData sourceNode = tManager.GetItem(rule.SourceId);
            string cleanPath = sourceNode.Path;
            try
            {
                cleanPath += "/";
                cleanPath = cleanPath.Replace('\\', '/');
                cleanPath = cleanPath.TrimStart('/');
            }
            catch { }
            finally { uxTaxonomyPath.Text = cleanPath; }
            uxAliasTaxFormatDropDownList.SelectedValue = rule.PageTypeId.GetHashCode().ToString();
            uxTaxExtensionDropDownList.SelectedValue = rule.FileExtensionId.ToString();
            uxAliasTemplateSource.SelectedValue = rule.TargetTemplateSource.GetHashCode().ToString();
            frm_hdnSourceId.Value = rule.SourceId.ToString();
            //if (rule.ExcludedPathId > 0)
            //{
            //    TaxonomyData excludePath = tManager.GetItem(rule.ExcludedPathId);
            //}
            uxTaxQueryStringParam.Text = rule.SourceParamName;
            uxTaxonomyPathSelectIframe.Src = "../urlAutoAliasSourceSelector.aspx?FolderID=0&browser=1&WantXmlInfo=1&noblogfolders=1&mode=Taxonomy";
            if (rule.LanguageId > 0) uxTaxonomyPathSelectIframe.Src += "&LangType=" + rule.LanguageId.ToString();
        }

        private void bindFolderData(AliasRuleData rule)
        {
            FolderManager fmanager = new FolderManager(Cms.Framework.ApiAccessMode.Admin);
            FolderData sourceFolder = fmanager.GetItem(rule.SourceId);
            if (sourceFolder != null)
            {
                if (rule.SiteId == 0 || rule.Type != EkEnumeration.AliasRuleType.Folder)
                {
                    uxFolderPath.Text = sourceFolder.NameWithPath;
                }
                else
                {
                    uxFolderPath.Text = sourceFolder.NameWithPath;
                    if (uxFolderPath.Text.StartsWith("/")) uxFolderPath.Text = uxFolderPath.Text.TrimStart('/');
                    if (uxFolderPath.Text.Contains("/")) uxFolderPath.Text = uxFolderPath.Text.Substring(uxFolderPath.Text.IndexOf("/"));
                    uxFolderPath.Text = uxFolderPath.Text.TrimStart('/');
                }
                uxAliasFormatDropDownList.SelectedValue = rule.PageTypeId.GetHashCode().ToString();
                uxExtensionDropDownList.SelectedValue = rule.FileExtensionId.ToString();
                uxQueryStringParam.Text = rule.SourceParamName;
                frm_hdnSourceId.Value = rule.SourceId.ToString();
                //if (rule.ExcludedPathId > 0)
                //{
                //    FolderData excludeFolder = fmanager.GetItem(rule.ExcludedPathId);
                //}
                uxFolderPathSelectIframe.Src = "../urlAutoAliasSourceSelector.aspx?FolderID=" + rule.SiteId.ToString() + "&browser=1&WantXmlInfo=1&noblogfolders=0&mode=Folder";
            }
        }

        private bool HasViewPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0
                );
        }

        private bool HasEditPermission()
        {
            return
                (
                    !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                    ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                    ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.UrlAliasingAdmin), ContentAPI.Current.UserId, false))
                );
        }

        private DisplayMode getDisplayMode()
        {
            DisplayMode mode = DisplayMode.None;

            if (rule != null && !Page.IsPostBack)
            {
                mode = DisplayMode.View;
            }

            if (rule != null && Page.IsPostBack)
            {
                mode = DisplayMode.Edit;
            }

            if (rule == null)
            {
                mode = DisplayMode.AddStep1;
            }

            return mode;
        }

        #endregion

        #region JavaScript/CSS

        /// <summary>
        /// Register Javascripts and CSS packages
        /// </summary>
        private void RegisterResources()
        {
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
            Package resources = new Package()
            {
                Components = new List<Component>()
                {
                    Packages.EktronCoreJS,
                    Packages.Ektron.Namespace,
                    Packages.Ektron.Workarea.Core, 
                    JavaScript.Create(cmsContextService.WorkareaPath + "/UrlAliasing/js/ektron.workarea.urlaliasing.js")
                }
            };
            resources.Register(this);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
            Packages.Ektron.JSON.Register(this);
        }

        #endregion
    }

    public enum DisplayMode
    {
        None = 0,
        View = 1,
        Edit = 2,
        AddStep1 = 3,
        AddStep2 = 4
    }
}
