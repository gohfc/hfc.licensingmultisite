﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WorkareaError.aspx.cs" Inherits="WorkareaError" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        div.PageContent {
            margin: 20px;
            color: #444444;
            font-family: Arial,Verdana,sans-serif;
        }
        img.ErrorLogo {
            float: left;
            margin: 0 10px 0 0;
        }
        div.PageContent h1 {
            font-size: 22px;
            padding: 50px 0 0;
        }
        div.PageContent p {
            font-size: 12px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="main" class="main">
        <div id="PageContent" class="PageContent">
            
            <img src="images/WorkareaErrorLogo.jpg" class="ErrorLogo" alt="Workarea Error" />
            <h1>Uh Ohh...</h1>
            <p>It looks like an error may have occurred.<br />Rest assured, we've logged this error and we're working quickly to fix the issue.</p>

        </div>
    </div>
    </form>
</body>
</html>
