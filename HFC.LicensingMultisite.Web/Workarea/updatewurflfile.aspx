﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="updatewurflfile.aspx.cs"
    Inherits="Workarea_updatewurflfile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .MainCentreBottom
        {
            font-size: 11pt;
            text-align: left;
            color: #444444;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="ektronTopSpace">
    </div>
    <div class="ektronPageContainer ektronPageGrid" style="overflow: hidden;">
        <asp:MultiView ID="mvUpdateFile" runat="server">
            <asp:View ID="vView" runat="server">
                <div class="ektronPageContainer">
                    <div id="dvDeviceAdd" class="ektronPageInfo">
                        <table>
                            <tr>
                                <td>
                                    <asp:Literal ID="ltrDateUpdated" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HyperLink CssClass="browseButton button buttonAdd buttonLeft greenHover" ToolTip="Click here to updateFile"
                                        ID="hlUpdate" runat="server" Text="Update File"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="vUpdate" runat="server">
                <div>
                    <asp:Literal ID="ltrUpdateMessage" runat="server"></asp:Literal>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
    </form>
</body>
</html>
