<%@ Page Language="C#" AutoEventWireup="true" CodeFile="mobilephone.aspx.cs" Inherits="StarterApps_SEO_mobilephone" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mobile Phone</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" width="667">
            <!-- fwtable fwsrc="Untitled" fwpage="Page 1" fwbase="mobilephone.jpg" fwstyle="Dreamweaver" fwdocid = "727665225" fwnested="0" -->
            <tr>
                <td>
                    <img src="images/spacer.gif" width="200" height="1" style="border: none" alt="" />
                </td>
                <td>
                    <img src="images/spacer.gif" width="272" height="1" style="border: none" alt="" />
                </td>
                <td>
                    <img src="images/spacer.gif" width="195" height="1" style="border: none" alt="" />
                </td>
                <td>
                    <img src="images/spacer.gif" width="1" height="1" style="border: none" alt="" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <img src="images/mobilephone_r1_c1.jpg" width="667" height="172" style="border: none"
                        id="mobilephone_r1_c1" alt="" />
                </td>
                <td>
                    <img src="images/spacer.gif" width="1" height="172" style="border: none" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    <img src="images/mobilephone_r2_c1.jpg" width="200" height="364" style="border: none"
                        id="mobilephone_r2_c1" alt="" />
                </td>
                <td valign="top">
                    <asp:Literal ID="litIFrame" runat="server" />
                </td>
                <td>
                    <img src="images/mobilephone_r2_c3.jpg" width="195" height="364" style="border: none"
                        id="mobilephone_r2_c3" alt="" />
                </td>
                <td>
                    <img src="images/spacer.gif" width="1" height="364" style="border: none" alt="" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <img src="images/mobilephone_r3_c1.jpg" width="667" height="464" style="border: none"
                        id="mobilephone_r3_c1" alt="" />
                </td>
                <td>
                    <img src="images/spacer.gif" width="1" height="464" style="border: none" alt="" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
