﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="collectionreport.ascx.cs"
    Inherits="Workarea_controls_collection_collectionreport" %>
<%@ Register TagPrefix="uxEktron" TagName="Paging" Src="../paging/paging.ascx" %>

<form id="frmCollectionList" runat="server">
<div id="dhtmltooltip">
</div>
<div class="ektronPageHeader">
    <div class="ektronTitlebar" id="txtTitleBar" runat="server">
    </div>
    <div class="ektronToolbar" id="htmToolBar" runat="server">
    </div>
</div>
<div class="ektronPageContainer ektronPageGrid">
    <div class="heightFix">
        <asp:GridView ID="CollectionListGrid" runat="server" AutoGenerateColumns="False"
            EnableViewState="False" Width="100%" CssClass="ektronGrid" GridLines="None">
            <HeaderStyle CssClass="title-header" />
        </asp:GridView>
        <uxEktron:Paging ID="uxPaging" runat="server"  visible="false" />
        
        <input type="hidden" runat="server" id="isCPostData" name="isCPostData" class="isCPostData" value="true" />
        <input type="hidden" runat="server" id="isSearchPostData" name="isSearchPostData" class="isSearchPostData" value="" />
        <asp:Literal ID="litRefreshAccordion" runat="server" />
    </div>
</div>
</form>
