<%@ Control Language="C#" AutoEventWireup="true" Inherits="removefolderitem" CodeFile="removefolderitem.ascx.cs" %>
<%@ Register TagPrefix="uxEktron" TagName="Paging" Src="../paging/paging.ascx" %>
<script type="text/javascript" language="javascript">
function resetPostback()
{
    document.forms[0].deletecontentbycategory_isPostData.value = "";
}

var browseURL = '<asp:literal id="browseurljs" runat="Server"/>';
var pagebetween = '<asp:literal id="pagebetweenjs" runat="Server"/>';

function GoToDeletePage(pageid, pagetotal) 
{
    if (pageid <= pagetotal && pageid >= 1) {
        window.location.href = browseURL + pageid;
    } else { 
        alert(pagebetween); 
    }
}
</script>

<div id="dhtmltooltip"></div>
<div class="ektronPageHeader">
    <div class="ektronTitlebar" id="txtTitleBar" runat="server"></div>
    <div class="ektronToolbar" id="htmToolBar" runat="server"></div>
</div>
<div class="ektronPageContainer ektronPageGrid">
    <asp:DataGrid ID="DeleteContentByGategoryGrid" 
        runat="server"
        CssClass="ektronGrid"
        AutoGenerateColumns="False"
        EnableViewState="False">
        <HeaderStyle CssClass="title-header" />
    </asp:DataGrid>
    <uxEktron:Paging ID="uxPaging" runat="server"  visible="false"/>  

    <input type="hidden" id="isPostData" value="true" name="isPostData" runat="server" />
    <input type="hidden" id="folder_id" name="folder_id" runat="server" />
    <input type="hidden" id="contentids" name="contentids" runat="server" />
    <input type="hidden" id="contentlanguages" name="contentlanguages" runat="server" />
    <input type="hidden" id="content_id" name="content_id" value="0" runat="server" />
    <input type="hidden" id="page_id" name="page_id" value="0" runat="server" />
</div>
