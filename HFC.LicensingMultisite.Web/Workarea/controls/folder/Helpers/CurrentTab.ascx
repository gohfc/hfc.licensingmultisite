﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CurrentTab.ascx.cs" Inherits="Workarea_controls_folder_Helpers_CurrentTab" %>

<ektronUI:JavaScriptBlock runat="server" ID="currentTabRegistration" ExecutionMode="OnEktronReady">
    <ScriptTemplate>
        Ektron.Workarea.Controls.Folder.theCurrentTab = Ektron.Workarea.Controls.Folder.CurrentTab($ektron("#<%# ButtonIdSelector %>"));
        Ektron.Workarea.Controls.Folder.theCurrentTab.parseCurrentTab();
    </ScriptTemplate>
</ektronUI:JavaScriptBlock>
<asp:HiddenField runat="server" ID="hdnTab" />
