﻿Ektron.Namespace.Register("Ektron.Workarea.Controls.Folder.CurrentTab");
Ektron.Workarea.Controls.Folder.CurrentTab = function ($buttonElem) {
    var $button = $buttonElem, originalHref = $button.attr("href"), $hiddenField = $ektron('input[id$=hdnTab]');

    return {
        setSelectedTab: function (mytab) {
            this.setButtonHash("#" + mytab);
        },

        setButtonHash: function (hashId) {
            var href = originalHref + hashId;
            $button.attr("href", href);
            $hiddenField.val(hashId);
        },

        parseCurrentTab: function () {
            var hashArray = window.location.href.split("").filter(function (elem) { return elem === "#"; }),
                hashId;
            if (hashArray.length === 1) {
                hashId = window.location.href.substr(window.location.href.indexOf("#"));
                this.setButtonHash(hashId);
            }
            else {
                this.setSelectedTab("dvProperties");
            };
        }

    };

};