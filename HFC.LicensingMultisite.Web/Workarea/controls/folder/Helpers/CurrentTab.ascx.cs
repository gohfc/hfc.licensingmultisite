﻿using Ektron.Cms.Framework.UI;
using Ektron.Cms.Interfaces.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Workarea_controls_folder_Helpers_CurrentTab : System.Web.UI.UserControl
{
    public string ButtonIdSelector { get; set; }
    public string HiddenFieldUniqueId { get { return this.hdnTab.UniqueID; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        ICmsContextService context = ServiceFactory.CreateCmsContextService();
        var package = new Ektron.Cms.Framework.UI.Package
        {
            Components = new List<Ektron.Cms.Framework.UI.Component>
            {
                Packages.Ektron.Namespace,
                JavaScript.Create(context.WorkareaPath.TrimEnd('/') + "/controls/folder/Helpers/currentTab.js")
            }
        };
        package.Register(this);

    }
}