﻿namespace Ektron.Workarea.Controls.Content
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    public partial class ViewSharePointContent : System.Web.UI.UserControl
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.LoadData();
        }

        private void LoadData()
        {
            List<SPCDataItem> toBind = new List<SPCDataItem>();
            toBind.Add(new SPCDataItem("Title", "George Sanders"));
            toBind.Add(new SPCDataItem("Email", "email@address.com"));
            toBind.Add(new SPCDataItem("Address", "1875 Columnbia Ave, NW, Washington, DC, 20006"));
            toBind.Add(new SPCDataItem("Phone", "+1 123 456 7890"));
            toBind.Add(new SPCDataItem("Education", "JD. UNH"));
            toBind.Add(new SPCDataItem("Bar Admissions", "DC,VA"));

            rptDataList.DataSource = toBind;
            rptDataList.DataBind();
        }

        public class SPCDataItem
        {
            public SPCDataItem()
            { 
            }
            public SPCDataItem(string name, string value)
            {
                this.Name = name;
                this.Value = value;
            }
            public string Name { get; set; }
            public string Value { get; set; }
        }
    }
}