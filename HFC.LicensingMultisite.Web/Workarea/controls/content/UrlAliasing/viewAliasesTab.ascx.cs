﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.Settings.UrlAliasing;
using Ektron.Cms.Settings.UrlAliasing.DataObjects;
using Ektron.Cms.Common;
using Ektron.Cms;

public partial class Workarea_controls_content_viewaliasesTab : System.Web.UI.UserControl
{
    long contentId = 0;
    
    AliasSettings settings;
    AliasSettingsManager settingsManager = new AliasSettingsManager();
    AliasManager aliasManager = new AliasManager();
    List<AliasData> manualAliases;
    List<AliasData> autoAliases;
    public bool IsEditable { get; set; }

    #region events 
    
    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            contentId = long.Parse(Request.QueryString["id"].ToString());
        }
        catch { }
        settings = settingsManager.Get();
        if (!settings.IsManualAliasingEnabled)
        {
            uxManualMessage.Visible = true;
            uxManualMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
            uxManualMessage.Text = GetLocalResourceObject("manualDisabledMessage").ToString();
        }
        if (!settings.IsFolderAliasingEnabled && !settings.IsTaxonomyAliasingEnabled)
        {
            uxAutoMessage.Visible = true;
            uxAutoMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
            uxAutoMessage.Text = GetLocalResourceObject("autoDisabledMessage").ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (settings.IsManualAliasingEnabled)
        {
            AliasCriteria manualCriteria = new AliasCriteria();
            manualCriteria.AddFilter(AliasProperty.TargetId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, this.contentId);
            manualCriteria.AddFilter(AliasProperty.LanguageId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, aliasManager.RequestInformation.ContentLanguage);
            manualCriteria.AddFilter(AliasProperty.Type, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, Ektron.Cms.Common.EkEnumeration.AliasRuleType.Manual);
            manualAliases = aliasManager.GetList(manualCriteria);

            if (manualAliases == null || manualAliases.Count == 0)
            {
                uxManualMessage.Visible = true;
                uxManualMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxManualMessage.Text = GetLocalResourceObject("noManualAliasesMessage").ToString();
            }
            else
            {
                uxManualMessage.Visible = false;
                uxManualAliasListGridView.DataSource = manualAliases;
                uxManualAliasListGridView.EktronUIPagingInfo = manualCriteria.PagingInfo;
                uxManualAliasListGridView.EktronUIOrderByFieldText = manualCriteria.OrderByField.ToString();
            }
        }
        if (settings.IsFolderAliasingEnabled || settings.IsTaxonomyAliasingEnabled)
        {
            AliasCriteria autoCriteria = new AliasCriteria();
            autoCriteria.Condition = LogicalOperation.And;
            autoCriteria.AddFilter(AliasProperty.TargetId, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, this.contentId);
            autoCriteria.AddFilter(AliasProperty.LanguageId, CriteriaFilterOperator.EqualTo, aliasManager.RequestInformation.ContentLanguage);
            CriteriaFilterGroup<AliasProperty> typeGroup = new CriteriaFilterGroup<AliasProperty>();
            typeGroup.Condition = LogicalOperation.Or;
            typeGroup.AddFilter(AliasProperty.Type, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, Ektron.Cms.Common.EkEnumeration.AliasRuleType.Taxonomy);
            typeGroup.AddFilter(AliasProperty.Type, Ektron.Cms.Common.CriteriaFilterOperator.EqualTo, Ektron.Cms.Common.EkEnumeration.AliasRuleType.Folder);
            autoCriteria.FilterGroups.Add(typeGroup);
            autoAliases = aliasManager.GetList(autoCriteria);

            if (autoAliases == null || autoAliases.Count == 0)
            {
                uxAutoMessage.Visible = true;
                uxAutoMessage.DisplayMode = Ektron.Cms.Framework.UI.Controls.EktronUI.Message.DisplayModes.Information;
                uxAutoMessage.Text = GetLocalResourceObject("noAutoAliasesMessage").ToString();
            }
            else
            {
                uxAutoAliasListGridView.DataSource = autoAliases;
                uxAutoAliasListGridView.EktronUIPagingInfo = autoCriteria.PagingInfo;
                uxAutoAliasListGridView.EktronUIOrderByFieldText = autoCriteria.OrderByField.ToString();
            }
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        this.DataBind();
    }

    #endregion

    #region private

    protected string getTypeImage(EkEnumeration.AliasRuleType autoAliasType)
    {
        SiteAPI siteAPI = new SiteAPI();
        string image = "";
        switch (autoAliasType)
        {
            case EkEnumeration.AliasRuleType.Taxonomy:
                image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/taxonomy.png", getLocalTypeName(autoAliasType));
                break;
            case EkEnumeration.AliasRuleType.Folder:
                image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/folder.png", getLocalTypeName(autoAliasType));
                break;
            case EkEnumeration.AliasRuleType.User:
                image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/userMembership.png", getLocalTypeName(autoAliasType));
                break;
            case EkEnumeration.AliasRuleType.Group:
                image = String.Format("<img src =\"{0}\" alt=\"{1}\" title=\"{1}\"/ >", siteAPI.AppPath + "images/ui/icons/usersMemberGroups.png", getLocalTypeName(autoAliasType));
                break;
        }
        return image;
    }

    private string getLocalTypeName(EkEnumeration.AliasRuleType configType)
    {
        string returnVal;

        try
        {
            returnVal = GetLocalResourceObject("configType" + configType.GetHashCode().ToString()) as string;
        }
        catch
        {
            returnVal = configType.ToDescriptionString();
        }

        return returnVal;
    }

    protected string BuildAliasDest(AliasData alias)
    {
        string target = "";
        FolderData fdat = ContentAPI.Current.GetFolderById(alias.SiteId, false, false);
        if (ContentAPI.Current.RequestInformationRef.IsStaging && fdat.DomainStaging != string.Empty)
        {
            target = "http://" + fdat.DomainStaging + "/" + alias.Alias;
        }
        else if (fdat.IsDomainFolder)
        {
            target += "http://" + fdat.DomainProduction + "/" + alias.Alias;
        }
        else
        {
            target += CommonApi.Current.SitePath + alias.Alias;
        }
        return target;
    }
    #endregion
}