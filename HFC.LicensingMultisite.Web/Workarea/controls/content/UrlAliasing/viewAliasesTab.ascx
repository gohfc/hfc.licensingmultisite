﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="viewAliasesTab.ascx.cs"
    Inherits="Workarea_controls_content_viewaliasesTab" %>
<div id="dvAliases">
    <ektronUI:Message ID="uxMessage" runat="server" Visible="false" />
    <fieldset id="uxManualList" runat="server">
        <legend>
            <asp:Literal ID="uxManualListTitle" runat="server" meta:resourcekey="uxManualListTitle"></asp:Literal>
        </legend>
        <ektronUI:Message ID="uxManualMessage" runat="server" Visible="false" />
        <div class="ektronPageGrid manualAliasListContainer">
            <ektronUI:GridView ID="uxManualAliasListGridView" runat="server" AutoGenerateColumns="false"
                EktronUIAllowResizableColumns="false" EnableEktronUITheme="true">
                <Columns>
                    <ektronUI:RadioButtonField ID="uxDefaultField">
                        <RadioButtonFieldHeaderStyle HeaderText="<%$ Resources:columnHeaderDefault %>" />
                        <RadioButtonFieldColumnStyle Width="200px" />
                        <RadioButtonFieldItemStyle CheckedBoundField="IsDefault" Enabled="<%# this.IsEditable %>" />
                        <KeyFields>
                            <ektronUI:KeyField DataField="Id" />
                        </KeyFields>
                    </ektronUI:RadioButtonField>
                    <ektronUI:CheckBoxField meta:resourceKey="columnHeaderActive" ID="uxActiveField">
                        <CheckBoxFieldHeaderStyle HeaderText="<%$ Resources:columnHeaderActive %>" HeaderCheckBoxEnabled="<%# this.IsEditable %>" />
                        <CheckBoxFieldColumnStyle Width="200px" />
                        <CheckBoxFieldItemStyle CheckedBoundField="IsEnabled" Enabled="<%# this.IsEditable %>" />
                        <KeyFields>
                            <ektronUI:KeyField DataField="Id" />
                        </KeyFields>
                    </ektronUI:CheckBoxField>
                    <asp:TemplateField meta:resourceKey="columnHeaderAlias">
                        <ItemTemplate>
                            <asp:HyperLink Target="_blank" ID="uxaliaslink" Text='<%# Eval("Alias") %>' runat="server"
                                NavigateUrl='<%# BuildAliasDest(Container.DataItem as Ektron.Cms.Settings.UrlAliasing.DataObjects.AliasData) %>' meta:resourceKey="columnHeaderAlias" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </ektronUI:GridView>
        </div>
    </fieldset>
    <fieldset id="uxAutoList" runat="server">
        <legend>
            <asp:Literal ID="uxAutomaticListTitle" runat="server" meta:resourcekey="uxAutomaticListTitle"></asp:Literal>
        </legend>
        <ektronUI:Message ID="uxAutoMessage" runat="server" Visible="false" />
        <div class="ektronPageGrid manualAliasListContainer">
            <ektronUI:GridView ID="uxAutoAliasListGridView" runat="server" AutoGenerateColumns="false"
                EktronUIAllowResizableColumns="false" EnableEktronUITheme="true">
                <Columns>
                    <ektronUI:CheckBoxField ID="uxActiveAutoField">
                        <CheckBoxFieldHeaderStyle HeaderText="<%$ Resources:columnHeaderActive %>" HeaderCheckBoxEnabled="<%# this.IsEditable %>" />
                        <CheckBoxFieldColumnStyle Width="200px" />
                        <CheckBoxFieldItemStyle CheckedBoundField="IsEnabled" Enabled="<%# this.IsEditable %>" />
                        <KeyFields>
                            <ektronUI:KeyField DataField="Id" />
                        </KeyFields>
                    </ektronUI:CheckBoxField>
                    <asp:TemplateField meta:resourceKey="columnHeaderType">
                        <ItemTemplate>
                            <asp:Literal runat="server" ID="Type" Text='<%#this.getTypeImage((Ektron.Cms.Common.EkEnumeration.AliasRuleType)Eval("Type"))%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField meta:resourceKey="columnHeaderAlias">
                        <ItemTemplate>
                            <asp:HyperLink Target="_blank" ID="uxautoaliaslink" Text='<%# Eval("Alias") %>' runat="server"
                                NavigateUrl='<%# BuildAliasDest(Container.DataItem as Ektron.Cms.Settings.UrlAliasing.DataObjects.AliasData) %>' meta:resourceKey="columnHeaderAlias" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </ektronUI:GridView>
        </div>
    </fieldset>
</div>
