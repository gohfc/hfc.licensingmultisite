<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Aloha.ascx.cs" Inherits="Ektron.AlohaEditor" %>
<div id="messagebox" style="display:none;"></div>
<asp:HiddenField runat="server" ID="textValue" OnValueChanged="textValue_TextChanged" Value="<%#Content%>" />
<div class="ektron-aloha">
    <ektronUI:Editor ID="EIC_Body" ToolbarConfig="Minimal" runat="server"></ektronUI:Editor>
</div>