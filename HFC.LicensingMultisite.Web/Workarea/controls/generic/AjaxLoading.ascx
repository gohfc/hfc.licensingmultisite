﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AjaxLoading.ascx.cs" Inherits="Workarea_controls_generic_AjaxLoading" %>
<style type="text/css">
   
    div#processingMsg
                {
                    width: 128px;
                    height: 128px;
                    margin: -64px 0 0 -64px;
                    background-color: #fff;
                    background-image: url("images/ui/loading_big.gif");
                    background-repeat: no-repeat;
                    text-indent: -10000px;
                    border: none;
                    padding: 0;
                    top: 50%;
                }
</style>
<script type="text/javascript">
 $ektron(document).ready(function()
        {
  $ektron("#processingMsg").modal(
                           {
                               trigger: '',
                               modal: true,
                               toTop: true,
                               onShow: function(hash)
                               {
                                   hash.o.fadeIn();
                                   hash.w.fadeIn();
                               },
                               onHide: function(hash)
                               {
                                   hash.w.fadeOut("fast");
                                   hash.o.fadeOut("fast", function()
                                   {
                                       if (hash.o)
                                       {
                                           hash.o.remove();
                                       }
                                   }
                                   );
                               }
                           }
                       );
 });
    </script>
<div class="ektronWindow" id="processingMsg">
    <h3>
        <asp:Literal ID="LoadingImg" runat="server" /></h3>
</div>