﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.Common;

public partial class Workarea_controls_generic_AjaxLoading : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ContentAPI ContentApi = new ContentAPI();
        EkMessageHelper MessageHelper = ContentApi.EkMsgRef;
        LoadingImg.Text = MessageHelper.GetMessage("one moment msg");
    }
}