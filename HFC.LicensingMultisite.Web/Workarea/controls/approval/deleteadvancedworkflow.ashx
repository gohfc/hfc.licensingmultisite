﻿<%@ WebHandler Language="C#" Class="deleteadvancedworkflow" %>

using System;
using System.Web;

using Ektron.Cms.BusinessObjects.ContentWorkflow;
using Ektron.Cms.Contracts.Common;
using Ektron.Cms.Contracts.ContentWorkflow;
using Ektron.Cms.Framework;
using Ektron.Cms.Framework.ContentWorkflow;
using Ektron.Cms.Framework.Settings;
using Ektron.Cms.Framework.User;
using Ektron.Cms.Settings;
using Ektron.Cms.Common;

public class deleteadvancedworkflow : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        
        long aDefId;
        long aObjId;

        long.TryParse(context.Request.Form["definitionId"], out aDefId);
        long.TryParse(context.Request.Form["objectId"], out aObjId);
        var aObjType = context.Request.Form["objectType"];
        var aAdvancedWorkflowUtility = new ContentWorkflowUtilities(Ektron.Cms.ObjectFactory.GetRequestInfoProvider().GetRequestInformation());
        
        if (aAdvancedWorkflowUtility.IsInprocess(aObjId, aObjType))
        {
            context.Response.StatusCode = 500;
            context.Response.Write("Unable to delete the definition. The definition currently has items in process.");
            context.Response.ContentType = "text/plain";
        }
        else
        {
        DoAdvancedWorkflowDelete(aDefId);
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    private void DoAdvancedWorkflowDelete(long theDefinitionId)
    {
        var aAdvancedWorkflowManager = new ContentWorkflowDefinitionManager();
        var aAdvancedWorkflowActivityManager = new ContentWorkflowActivityManager();
        
        //Delete activities for a definition
        var aActivityCriteria = new ContentWorkflowActivityCriteria();
        aActivityCriteria.AddFilter(ContentWorkflowActivityProperty.DefinitionId, CriteriaFilterOperator.EqualTo, theDefinitionId);
        var aActivityList = aAdvancedWorkflowActivityManager.GetList(aActivityCriteria);

        foreach (var aActivity in aActivityList)
        {
            aAdvancedWorkflowActivityManager.Delete(aActivity.Id);
        }
        
        //Delete the definition
        aAdvancedWorkflowManager.Delete(theDefinitionId);
    }
    
}