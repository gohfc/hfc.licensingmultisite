using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Ektron.Cms;
using Ektron.Cms.BusinessObjects.ContentWorkflow;
using Ektron.Cms.Common;
using Ektron.Cms.Content;
using Ektron.Cms.Contracts.Common;
using Ektron.Cms.Contracts.ContentWorkflow;
using Ektron.Cms.Framework;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Framework.ContentWorkflow;
using Ektron.Cms.Framework.Organization;
using Ektron.Cms.Framework.Settings;
using Ektron.Cms.Framework.User;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Settings;
using Ektron.Cms.Workarea.ContentWorkflow;
using Microsoft.VisualBasic;
using System.Web;


public partial class ManageApprovals : System.Web.UI.UserControl
{
    protected static string FOLDER = "folder";
    protected static string CONTENT = "content";
    protected static bool advWorkflowEnabled;
    protected bool m_isApprovalChainExists = false;
    protected ContentAPI m_refContentApi = new ContentAPI();
    protected StyleHelper m_refStyle = new StyleHelper();
    protected EkMessageHelper m_refMsg;
    protected string m_appImgPath = "";
    protected string m_sitePath = "";
    protected string m_workareaPath = "";
    protected string m_folderPath = "";
    protected long m_objectId = 0;
    protected string m_objectType = "";
    protected int m_contentLanguage = -1;
    protected long m_currentUserId = 0;
    protected bool m_isHerited = false;
    protected FolderData m_folderData;
    protected PermissionData m_securityData;
    protected Collection m_pageData;
    protected ContentData m_contentData;
    protected string m_strPageAction = "";
    protected string m_strOrderBy = "";
    protected int m_enableMultilingual = 0;
    protected bool m_isAdvancedWorkflow = false;
    protected string m_versionMap = "";
    protected string m_title = "";
    protected bool m_isInProcess = false;
    protected int m_forceAllApprovers;	//only applies to basic wf
    protected static string AdvancedInProcessQuery = "";
    protected long m_workflowDefinitionId;

    protected string advancedWorkflowRemovalDescription = string.Empty;
    protected string uxCancelWorkflowQuestion = string.Empty;

    public ManageApprovals()
    {
        bool.TryParse(ConfigurationManager.AppSettings["ek_AdvancedWorkflowEnabled"], out advWorkflowEnabled);
    }

    private class InProcess
    {
        public string Title { get; set; }
        public string Username { get; set; }
        public string Updated { get; set; }
    }
    private List<InProcess> docsInprocess = new List<InProcess>();

    private class UserInfo
    {
        public string DisplayName { get; set; }
        public string UserIdType { get; set; }
    }

    private List<UserInfo> _currentAllowedUsers { get; set; }

    private Dictionary<string, string> _configuredWorkflowDisplayNames
    {
        get
        {
            Dictionary<string, string> names = null;
            names = (Dictionary<string, string>)Session["WorkflowDisplayNames"];
            if (names == null)
            {
                ContentWorkflowConfigurationManager _configMgr = new ContentWorkflowConfigurationManager();
                var workflows = _configMgr.GetWorkflowList();
                names = workflows.ToDictionary(x => x.DisplayName, x => x.ClassName + "," + x.AssemblyName);
                Session["ConfiguredWorkflowDisplayNames"] = names;
            }
            return names;
        }
    }

    private List<ContentWorkflowActivityData> _currentActivities
    {
        get { return (List<ContentWorkflowActivityData>)Session["CurrentContentWorkflowActivity"]; }
        set { Session["CurrentContentWorkflowActivity"] = value; }
    }

    #region Page Events
    private void Page_Load(System.Object sender, System.EventArgs e)
    {
        //Put user code to initialize the page here
        m_refMsg = m_refContentApi.EkMsgRef;
        m_workareaPath = m_refContentApi.AppPath;
        if (!IsPostBack)
        {
            SetupCancelConfirmationDialog();
        }
    }

    protected void Page_PreRender(Object sender, EventArgs e)
    {
        RegisterResources();
    }

    #endregion

    //called by content.aspx
    public bool ViewApproval()
    {
        SetQueryStringParm();
        SetObjectData();
        if (!advWorkflowEnabled)
        {
            DisplayBasicApproval();
        }
        else if (m_versionMap == m_refMsg.GetMessage("lblContentWorkflowSelectOne"))
        {
            pnlAdvWorkflowDetails.Visible = false;
        }
        // events fired from this control will not be passed to it. Instead they go to content.aspx which then calls here.
        // So check the event target to see if one of our buttons has been clicked.
        else if ((Request.Params["__EVENTTARGET"] != null) &&
            ((Request.Params["__EVENTTARGET"].IndexOf("ltrAdvancedHeading") > -1) ||
                (Request.Params["__EVENTTARGET"].IndexOf("btnSelectAdvanced") > -1)))
        {
            DisplaySelectAdvancedWorkflow();
        }
        else if ((Request.Params["__EVENTTARGET"] != null) &&
            ((Request.Params["__EVENTTARGET"].IndexOf("ltrBasicHeading") > -1) ||
                (Request.Params["__EVENTTARGET"].IndexOf("btnSelectBasic") > -1)))
        {
            DisplayBasicApproval();
        }
        else if (m_isInProcess)
        {
            DisplayInProcess();
        }     
        else if (m_isHerited)
        {
            DisplayInherited();
        }        
        else if (m_isAdvancedWorkflow && (Request.Params["approval$hdnWorkflowChanged"] == null || (string)Request.Params["approval$hdnWorkflowChanged"] != "true"))
        {
            ViewApprovalToolBar();
            pnlBasicWorkFlow.Visible = false;
            pnlAdvancedWorkFlow.Visible = true;
            SetWorkflowInstructions();
            PopulateExistingWorkflowGrid();
            PopulateWorkFlowOptions();
            pnlAdvWorkflowDetails.Visible = true;
        }
        else if ((string)Request.Params["approval$hdnWorkflowChanged"] == "true")
        {
            m_isAdvancedWorkflow = true;  //need to over ride value set in SetObjectData because we're now assigning an adv wf
            ViewApprovalToolBar();
            pnlAdvancedWorkFlow.Visible = true;
            pnlAdvWorkflowDetails.Visible = true;
            PopulateWorkFlowOptions();
            PopulateNewWorkflowGrid();
            SetWorkflowInstructions();
            pnlBasicWorkFlow.Visible = false;
            pnlChooseMethod.Visible = false;
        }
        else if (IsBasicWorkflow())
        {
            DisplayBasicApproval();
        }
        else if (Request.UrlReferrer != null
            && !string.IsNullOrEmpty(Request.UrlReferrer.Query)
            && !string.IsNullOrEmpty(HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["action"])
            && HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["action"].ToLower() == "editapprovalmethod")
        {
            DisplayBasicApproval();
        }
        else
        {
            DisplaySelectionPanel();
        }

        return false;
    }

    #region Basic Approval

    private bool IsBasicWorkflow()
    {
        if (m_objectType == FOLDER && m_folderData != null)
        {
            if (m_folderData.Type == EkEnumeration.FolderType.Calendar)
                return true;
        }

        else if (m_objectType == CONTENT && m_contentData != null)
        {
            if (m_contentData.SubType == EkEnumeration.CMSContentSubtype.WebEvent)
                return true;
        }

        EkContent _content = new EkContent(CommonApi.Current.RequestInformationRef);
        Microsoft.VisualBasic.Collection items = _content.GetApprovalInfov2_0(m_objectId, m_objectType);
        if (items.Count > 0)
        {
            //The Visual Basic collection item with a value of 0 is recognized by C# as int where any other value is a long
            // to get around this treat the count as a string
            string count = items[1].ToString();
            return (count != "0") ? true : false;
        }
        else
            return false;
    }

    private void DisplayBasicApproval()
    {
        m_isAdvancedWorkflow = false;
        SetupBasicApproval();
        ViewApprovalToolBar();
        pnlChooseMethod.Visible = false;
        pnlAdvancedWorkFlow.Visible = false;
        pnlAdvWorkflowDetails.Visible = false;
        pnlBasicWorkFlow.Visible = true;
    }

    private void SetupBasicApproval()
    {
        ApprovalItemData[] approval_data;
        approval_data = m_refContentApi.GetItemApprovals(m_objectId, m_objectType);
        if (approval_data != null)
        {
            if (approval_data.Length > 0)
            {
                m_isApprovalChainExists = true;
            }
        }

        if (m_forceAllApprovers == 1)
        {
            lblMethod.Text = m_refMsg.GetMessage("display for force all approvers");
        }
        else
        {
            lblMethod.Text = m_refMsg.GetMessage("display for do not force all approvers");
        }
        Populate_ViewApprovalsGrid(approval_data);
    }

    private void Populate_ViewApprovalsGrid(ApprovalItemData[] approval_data)
    {
        System.Web.UI.WebControls.BoundColumn colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "TITLE";
        colBound.HeaderText = m_refMsg.GetMessage("user or group name title");
        colBound.ItemStyle.Wrap = false;
        colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
        ViewApprovalsGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "ID";
        colBound.HeaderText = m_refMsg.GetMessage("generic ID");
        colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
        colBound.ItemStyle.Wrap = false;
        ViewApprovalsGrid.Columns.Add(colBound);

        colBound = new System.Web.UI.WebControls.BoundColumn();
        colBound.DataField = "ORDER";
        colBound.HeaderText = m_refMsg.GetMessage("approval order title");
        colBound.ItemStyle.VerticalAlign = VerticalAlign.Top;
        colBound.ItemStyle.Wrap = false;
        ViewApprovalsGrid.Columns.Add(colBound);

        DataTable dt = new DataTable();
        DataRow dr;

        dt.Columns.Add(new DataColumn("TITLE", typeof(string)));
        dt.Columns.Add(new DataColumn("ID", typeof(string)));
        dt.Columns.Add(new DataColumn("ORDER", typeof(string)));


        bool bInherited = false;
        if (m_objectType == FOLDER)
        {
            bInherited = m_folderData.IsPermissionsInherited;
            jsFolderId.Text = m_folderData.Id.ToString();
        }
        else
        {
            bInherited = m_contentData.IsPermissionsInherited;
            jsFolderId.Text = m_contentData.FolderId.ToString();
        }
        if (bInherited)
        {
            lblBasicInheritedMsg.Text = m_refMsg.GetMessage("approval chain inherited msg");
            lblBasicInheritedMsg.Visible = true;
        }
        int i;
        if (!(approval_data == null))
        {
            for (i = 0; i <= approval_data.Length - 1; i++)
            {
                dr = dt.NewRow();
                if (approval_data[i].UserId != 0)
                {
                    dr[0] = "<img class=\"imgUsers\" src=\"" + m_workareaPath + "images/UI/Icons/user.png\" />" + approval_data[i].DisplayUserName;
                    dr[1] = approval_data[i].UserId;
                }
                else
                {
                    dr[0] = "<img class=\"imgUsers\" src=\"" + m_workareaPath + "images/UI/Icons/users.png\" />" + approval_data[i].DisplayUserGroupName;
                    dr[1] = approval_data[i].GroupId;
                }
                dr[2] = approval_data[i].ApprovalOrder;

                dt.Rows.Add(dr);
            }
        }

        DataView dv = new DataView(dt);
        ViewApprovalsGrid.DataSource = dv;
        ViewApprovalsGrid.DataBind();
    }

    #endregion

    #region Advanced Approval

    private void SetWorkFlowDisplay()
    {
        PopulateWorkFlowOptions();
        PopulateNewWorkflowGrid();
    }

    private void SetWorkflowInstructions()
    {
        StringBuilder sb = new StringBuilder(m_refMsg.GetMessage("lblContentWorkflowInstructions"));
        sb.Append(" ");
        if (Request.Params["type"].ToLower() == FOLDER)
        {
            sb.Append(m_refMsg.GetMessage("lblContentWorkflowFolder"));
        }
        else
        {
            sb.Append(m_refMsg.GetMessage("lblContentWorkflowContent"));
        }
        lblInstructions.Text = sb.ToString();
    }

    private void PopulateExistingWorkflowGrid()
    {
        GetAllowedUsers();
        lblContentWorkflowTypeDropDownInstructions.Text = m_refMsg.GetMessage("lblContentWorkflowTypeDropDownInstructions");
        ltrSelectInstructions.Text = m_refMsg.GetMessage("lblContentWorkflowSelectionInstructions") + " " + m_objectType;
        pnlBasicWorkFlow.Visible = false;
        pnlAdvancedWorkFlow.Visible = true;
        pnlAdvWorkflowDetails.Visible = true;
        ContentWorkflowActivityCriteria criteria = new ContentWorkflowActivityCriteria(ContentWorkflowActivityProperty.ApprovalOrder, EkEnumeration.OrderByDirection.Ascending);
        criteria.AddFilter(ContentWorkflowActivityProperty.DefinitionId, CriteriaFilterOperator.EqualTo, m_workflowDefinitionId);
        ContentWorkflowActivityManager activityMgr = new ContentWorkflowActivityManager(ApiAccessMode.Admin);
        _currentActivities = activityMgr.GetList(criteria);
        gridAdvWorkflowApprovers.EktronUIPagingInfo = new Ektron.Cms.PagingInfo(50);
        gridAdvWorkflowApprovers.EktronUIPagingInfo.TotalRecords = _currentActivities.Count;
        gridAdvWorkflowApprovers.DataSource = _currentActivities;
        gridAdvWorkflowApprovers.DataBind();
    }

    private void PopulateNewWorkflowGrid()
    {
        GetAllowedUsers();
        var m_defData = new ContentWorkflowDefinitionData()
        {
            Id = 0,
            FolderId = m_objectId,
            LanguageId = m_contentLanguage,
            VersionMap = m_versionMap
        };
        List<ContentWorkflowActivityData> lstActivities = GetActivityDefinition(m_versionMap);
        _currentActivities = lstActivities;
        gridAdvWorkflowApprovers.EktronUIPagingInfo = new Ektron.Cms.PagingInfo(50);
        gridAdvWorkflowApprovers.EktronUIPagingInfo.TotalRecords = lstActivities.Count;
        gridAdvWorkflowApprovers.DataSource = lstActivities;
        gridAdvWorkflowApprovers.DataBind();
    }

    protected void grvGrid_RowBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            Label lblApprovalOrder = (Label)e.Row.FindControl("lblApprovalOrder");
            if (lblApprovalOrder != null) lblApprovalOrder.Text = m_refMsg.GetMessage("lblContentWorkflowApprovalOrder");

            Label lblActivityName = (Label)e.Row.FindControl("lblActivityName");
            if (lblActivityName != null) lblActivityName.Text = m_refMsg.GetMessage("lblContentWorkflowActivityName");


            Label lblApprovers = (Label)e.Row.FindControl("lblApprovers");
            if (lblApprovers != null) lblApprovers.Text = m_refMsg.GetMessage("lblContentWorkflowApprover");

            Label lblWait = (Label)e.Row.FindControl("lblEscalation");
            if (lblWait != null) lblWait.Text = m_refMsg.GetMessage("lblContentWorkflowEscalation");

            Label lblEscalateTo = (Label)e.Row.FindControl("lblEscalateTo");
            if (lblEscalateTo != null) lblEscalateTo.Text = m_refMsg.GetMessage("lblContentWorkflowEscalation"); ;
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ContentWorkflowActivityData activityData = (ContentWorkflowActivityData)e.Row.DataItem;
            DropDownList ddlApprovers = (DropDownList)e.Row.FindControl("ddlApprovers");
            if (ddlApprovers != null)
            {
                ddlApprovers.DataSource = _currentAllowedUsers;
                ddlApprovers.DataTextField = "DisplayName";
                ddlApprovers.DataValueField = "UserIdType";
                ddlApprovers.DataBind();

                //if definitionId is 0 then this is a new Definition so don't set the selected index
                if (activityData.DefinitionId != 0)
                {
                    try
                    {
                        ddlApprovers.SelectedIndex = _currentAllowedUsers.IndexOf((_currentAllowedUsers.Where(x => x.UserIdType == activityData.UserId + "|" + activityData.UserType).First()));
                    }
                    catch (Exception e1)
                    {
                        ddlApprovers.SelectedIndex = 0;
                    }
                }
            }

            DropDownList ddlEscalateTo = (DropDownList)e.Row.FindControl("ddlEscalateTo");
            if (ddlEscalateTo != null)
            {
                ddlEscalateTo.DataSource = _currentAllowedUsers;
                ddlEscalateTo.DataTextField = "DisplayName";
                ddlEscalateTo.DataValueField = "UserIdType";
                ddlEscalateTo.DataBind();
                if (activityData.EscalationObjectId != 0)
                {
                    try
                    {
                        ddlEscalateTo.SelectedIndex = _currentAllowedUsers.IndexOf((_currentAllowedUsers.Where(x => x.UserIdType == activityData.EscalationObjectId + "|" + activityData.EscalationUserType).First()));
                    }
                    catch (Exception e2)
                    {
                        ddlEscalateTo.SelectedIndex = 0;
                    }
                }

                DropDownList ddlEscalationWait = (DropDownList)e.Row.FindControl("ddlEscalationWait");
                if (ddlEscalationWait != null)
                {
                    for (int ii = 1; ii <= 31; ii++)
                    {
                        ddlEscalationWait.Items.Add(new ListItem() { Text = ii.ToString(), Value = ii.ToString() });
                    }
                }
            }

            RadioButton radioEscalate = (RadioButton)e.Row.FindControl("radioEscalate");
            RadioButton radioDoNotEscalate = (RadioButton)e.Row.FindControl("radioDoNotEscalate");
            if (radioEscalate != null)
            {
                radioEscalate.Text = m_refMsg.GetMessage("lblContentWorkflowEscalationWait1");
                if (activityData.EscalationObjectId != 0)
                {
                    radioEscalate.Checked = true;
                    radioDoNotEscalate.Checked = false;
                    DropDownList ddlDuration = (DropDownList)e.Row.FindControl("ddlEscalationWait");
                    if (ddlDuration != null)
                    {
                        ListItem item = new ListItem(activityData.EscalationTimeInterval.ToString());
                        ddlDuration.SelectedIndex = ddlDuration.Items.IndexOf(item);
                    }
                }
            }
            radioDoNotEscalate = (RadioButton)e.Row.FindControl("radioDoNotEscalate");
            if (radioDoNotEscalate != null)
            {
                radioDoNotEscalate.Text = m_refMsg.GetMessage("lblContentWorkflowDoNotEscalate"); ;
            }
        }
    }

    private List<ContentWorkflowActivityData> GetActivityDefinition(string versionMap)
    {
        ContentWorkflowConfigurationManager _configMgr = new ContentWorkflowConfigurationManager();
        var wf = versionMap.Split(',');
        var activityNames = _configMgr.GetWorkflowActivities(wf[0], wf[1]);
        List<ContentWorkflowActivityData> lstActivitySetup = new List<ContentWorkflowActivityData>();
        int ii = 1;
        activityNames.ForEach(x => { lstActivitySetup.Add(new ContentWorkflowActivityData() { ActivityName = x.ActivityName, ApprovalOrder = ii++ }); });
        return lstActivitySetup;
    }

    private void GetAllowedUsers()
    {
        //this method resets the session cache with the list of allowed users for a folder. The list of users is
        // used by grvGrid_RowBound
        if (_currentAllowedUsers != null) _currentAllowedUsers.Clear();
        List<UserInfo> allowedUsers = new List<UserInfo>();
        UserManager userMgr = new UserManager();
        UserGroupManager userGroupMgr = new UserGroupManager();
        PermissionManager permMgr = new PermissionManager(ApiAccessMode.Admin);
        PermissionCriteria criteria = new PermissionCriteria();

        if (m_objectType == FOLDER)
            criteria.AddFilter(PermissionProperty.FolderId, CriteriaFilterOperator.EqualTo, m_objectId);
        else
        {
            criteria.AddFilter(PermissionProperty.ContentId, CriteriaFilterOperator.EqualTo, m_objectId);
            criteria.AddFilter(PermissionProperty.LanguageId, CriteriaFilterOperator.EqualTo, m_contentLanguage);
        }
        var userPermissions = permMgr.GetList(criteria);
        foreach (UserPermissionData upd in userPermissions)
        {
            if (upd.UserId > 0)
            {
                UserData uData = userMgr.GetItem(upd.UserId);
                allowedUsers.Add(new UserInfo() { UserIdType = upd.UserId.ToString() + '|' + ContentWorkflowUserType.User.ToString(), DisplayName = uData.Username });
            }
            else if (upd.GroupId > 0)
            {
                UserGroupData ugData = userGroupMgr.GetItem(upd.GroupId);
                allowedUsers.Add(new UserInfo() { UserIdType = upd.GroupId.ToString() + '|' + ContentWorkflowUserType.Group.ToString(), DisplayName = ugData.Name });
            }

        }
        allowedUsers = allowedUsers.OrderBy(x => x.DisplayName).ToList();
        _currentAllowedUsers = allowedUsers;
    }

    private void PopulateWorkFlowOptions()
    {
        int selectedIndex = 0;
        pnlBasicWorkFlow.Visible = false;
        pnlAdvancedWorkFlow.Visible = true;
        lblSelType.Text = m_refMsg.GetMessage("select workflow type");
        ddlSelType.Items.Clear();
        ddlSelType.Items.Add(new ListItem(m_refMsg.GetMessage("lblContentWorkflowSelectOne"), m_refMsg.GetMessage("lblContentWorkflowSelectOne")));

        foreach (KeyValuePair<string, string> name in _configuredWorkflowDisplayNames)
        {
            string vMap = name.Value.ToString();
            ddlSelType.Items.Add(new ListItem(name.Key.ToString(), vMap));
            if (vMap == m_versionMap) selectedIndex = ddlSelType.Items.Count - 1;
        }
        //ddlSelType.DataBind();
        ddlSelType.SelectedIndex = selectedIndex;
    }

    private InProcess GetApprovalInfo(ContentData content)
    {
        InProcess item = new InProcess();
        item.Title = content.Title;
        if (m_isAdvancedWorkflow)
        {
            ContentWorkflowInstanceCriteria criteria = new ContentWorkflowInstanceCriteria();
            criteria.AddFilter(ContentWorkflowInstanceProperty.ContentId, CriteriaFilterOperator.EqualTo, content.Id);
            ContentWorkflowInstanceManager instMgr = new ContentWorkflowInstanceManager(ApiAccessMode.Admin);
            ContentWorkflowInstanceData instData = instMgr.GetList(criteria).FirstOrDefault();
            if (instData != null)
            {
                item.Updated = instData.DateModified.ToString();
                ContentWorkflowActivityManager activityMgr = new ContentWorkflowActivityManager(ApiAccessMode.Admin);
                ContentWorkflowActivityCriteria criteria1 = new ContentWorkflowActivityCriteria();
                criteria1.AddFilter(ContentWorkflowActivityProperty.DefinitionId, CriteriaFilterOperator.EqualTo, instData.DefinitionId);
                List<ContentWorkflowActivityData> lstActivities = activityMgr.GetList(criteria1);
                int step = (from x in lstActivities where x.ActivityName == instData.CurrentState select x.ApprovalOrder).First();
                var activity = (from x in lstActivities where x.ApprovalOrder == step select x).First();
                if (activity.UserType == ContentWorkflowUserType.User)
                {
                    UserManager userMgr = new UserManager();
                    UserData user = userMgr.GetItem(activity.UserId);
                    item.Username = user.DisplayName;
                }
                else if (activity.UserType == ContentWorkflowUserType.Group)
                {
                    UserGroupManager userGroupMgr = new UserGroupManager();
                    UserGroupData group = userGroupMgr.GetItem(activity.UserId);
                    item.Username = group.Name;
                }
            }
        }
        return item;
    }

    protected void grdDocsInProcessBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            Label lblTitle = (Label)e.Row.FindControl("lblContentTitle");
            if (lblTitle != null) lblTitle.Text = m_refMsg.GetMessage("generic title");
            Label lblUserName = (Label)e.Row.FindControl("lblUserName");
            if (lblUserName != null) lblUserName.Text = m_refMsg.GetMessage("lblContentWorkflowAwaitingApproval");
            Label lblUpdated = (Label)e.Row.FindControl("lblUpdated");
            if (lblUpdated != null) lblUpdated.Text = m_refMsg.GetMessage("lblContentWorkflowLastUpdated");
        }
    }

    private void DisplayInProcess()
    {
        pnlInProcess.Visible = true;
        ltrInProcessMsg.Text = m_refMsg.GetMessage("lblContentWorkflowInProcess");
        if (m_objectType == FOLDER)
        {
            BackOnlyToolBar("lblContentWorkflowManageAdvancedApprovals", m_title);
        }
        else
        {
            ClientWorkflowUtilities cwfUtils = new ClientWorkflowUtilities();
            if (cwfUtils.IsWorkflowAdmin(m_refContentApi.RequestInformationRef.UserId) && m_isAdvancedWorkflow)
            {
                SetupCancelConfirmationDialog();
                CancelWorkflowToolBar();
            }
            else
            {
                BackOnlyToolBar("lblContentWorkflowManageAdvancedApprovals", m_title);
            }
        }
    }

    private void SetupCancelConfirmationDialog()
    {
        uxAdvancedWorkflowCancelDialog.Title = m_refMsg.GetMessage("lblContentWorkflowCancelDialogTitle");
        uxAdvancedWorkflowCancelSuccessDialog.Title = m_refMsg.GetMessage("lblContentWorkflowCancelDialogTitle");
        uxButtonDoCancelWorkflow.Text = m_refMsg.GetMessage("generic yes");
        uxButtonExitCancelWorkflow.Text = m_refMsg.GetMessage("generic no");
        uxCancelWorkflowMessage.Text = m_refMsg.GetMessage("lblContentWorkflowCancelMessage");
        uxCancelWorkflowQuestion = m_refMsg.GetMessage("lblContentWorkflowCancelQuestion");
        uxAdvancedWorkflowCancelSuccessMessage.Text = m_refMsg.GetMessage("lblContentWorkflowCancelSuccessDialog");
    }

    private void DisplayInherited()
    {
        string msg = m_refMsg.GetMessage("lblContentWorkflowInheritanceMsg");
        string type = (m_objectType == FOLDER) ? "folder" : "content";
        msg = string.Format(msg, type, m_title);
        lblContentWorkflowInheritanceMsg.Text = msg;
        pnlInherited.Visible = true;
        string msg1 = (m_objectType == "folder") ? "view folder approvals msg" : "view content approvals msg";
        BackOnlyToolBar(msg1, "");
    }

    private void SetAdvancedWorkflowRemovalMessages(string workflowItemType)
    {
        var aLocaleMgr = new Ektron.Cms.BusinessObjects.Localization.LocaleManager(m_refContentApi.RequestInformationRef);
        var warningMessage = string.Empty;
        var description = string.Empty;
        var success = string.Empty;
        var aCurrentLocale = aLocaleMgr.GetItem(m_contentLanguage);

        if (workflowItemType == "folder")
        {
            //Set removal messaging for folders
            warningMessage = string.Format(m_refMsg.GetMessage("advancedWorkflowRemovalWarningFolder"), aCurrentLocale.CombinedName);
            description = m_refMsg.GetMessage("advancedWorkflowRemovalDescriptionFolder");
            success = string.Format(m_refMsg.GetMessage("advancedWorkflowRemovalSuccessFolder"), aCurrentLocale.CombinedName);
        }
        else
        {
            //Set removal messaging for content
            warningMessage = string.Format(m_refMsg.GetMessage("advancedWorkflowRemovalWarningContent"), aCurrentLocale.CombinedName);
            description = m_refMsg.GetMessage("advancedWorkflowRemovalDescriptionContent");
            success = string.Format(m_refMsg.GetMessage("advancedWorkflowRemovalSuccessContent"), aCurrentLocale.CombinedName);
        }

        var aDialogTitle = m_refMsg.GetMessage("advancedWorkflowRemovalDialogTitle");

        advancedWorkflowDeleteDialog.Title = aDialogTitle;
        advancedWorkflowDeleteDialogSuccess.Title = aDialogTitle;
        uxAdvancedWorkflowRemoveWarning.Text = warningMessage;
        advancedWorkflowRemovalDescription = description;
        uxAdvancedWorkflowRemovalSuccess.Text = success;

        //translated Button text
        uxRemoveAdvancedWorkflowButton.Text = m_refMsg.GetMessage("advancedWorkflowRemovalRemoveButton");
        uxCancelAdvancedWorkflowDeleteButton.Text = m_refMsg.GetMessage("resolve cancel button");
        uxRemoveAdvancedWorkflowOkayButton.Text = m_refMsg.GetMessage("advancedWorkflowRemovalOkayButton");
    }

    private void DisplaySelectionPanel()
    {
        if (m_objectType == FOLDER)
        {
            BackOnlyToolBar("view folder approvals msg", m_title);
        }
        else
        {
            BackOnlyToolBar("view content approvals msg", m_title);
        }
        lblSelType.Text = m_refMsg.GetMessage("select workflow type");
        lblContentWorkflowTypeDropDownInstructions.Text = m_refMsg.GetMessage("lblContentWorkflowTypeDropDownInstructions");
        ltrSelectInstructions.Text = m_refMsg.GetMessage("lblContentWorkflowSelectionInstructions") + " " + m_objectType;
        ltrBasicHeading.Text = m_refMsg.GetMessage("lblContentWorkflowBasic");
        ltrBasicInstructions.Text = m_refMsg.GetMessage("lblContentWorkflowBasicDescription");
        btnSelectBasic.Text = m_refMsg.GetMessage("lblContentWorkflowBasicButton");
        ltrAdvancedHeading.Text = m_refMsg.GetMessage("lblContentWorkflowAdvanced");
        ltrAdvancedInstructions.Text = m_refMsg.GetMessage("lblContentWorkflowAdvancedDescription");
        btnSelectAdvanced.Text = m_refMsg.GetMessage("lblContentWorkflowAdvancedButton");
        pnlChooseMethod.Visible = true;
        pnlInherited.Visible = false;
        pnlInProcess.Visible = false;
        pnlAdvancedWorkFlow.Visible = false;
    }

    private void DisplaySelectAdvancedWorkflow()
    {
        if (m_objectType == FOLDER)
        {
            BackOnlyToolBar("view folder approvals msg", m_title);
        }
        else
        {
            BackOnlyToolBar("view content approvals msg", m_title);
        }
        hdnDefinitionId.Value = "0";
        pnlChooseMethod.Visible = false;
        pnlBasicWorkFlow.Visible = false;
        pnlAdvancedWorkFlow.Visible = true;
        PopulateWorkFlowOptions();
    }

    #endregion

    #region Common

    private void GetWorkflowTypeAndStatus()
    {
        ContentWorkflowUtilities cfUtilities = new ContentWorkflowUtilities(m_refContentApi.EkContentRef.RequestInformation);
        long contentId = m_objectId;
        long folderId = 0;
        if (m_objectType.ToLower() == "content")
        {
            if (m_contentData == null)
                m_contentData = m_refContentApi.GetContentById(m_objectId);
            if (m_contentData != null)
                folderId = m_contentData.FolderId;

        }
        else
        {
            contentId = 0;
            folderId = m_objectId;
        }
        if (advWorkflowEnabled)
        {
            m_workflowDefinitionId = cfUtilities.GetInheritedWorkflowDefinitionId(contentId, folderId, m_refContentApi.RequestInformationRef.ContentLanguage);
            // hdnDefinitionId is used in the ajax call to save the definition
            hdnDefinitionId.Value = m_workflowDefinitionId.ToString();
            if (m_workflowDefinitionId > 0)
            {
                m_isAdvancedWorkflow = true;
                var cwfDefManager = new ContentWorkflowDefinitionManager();
                var defData = cwfDefManager.GetItem(m_workflowDefinitionId);
                if ((string)Request.Params["approval$hdnWorkflowChanged"] != "true")
                    m_versionMap = defData.VersionMap;
            }
        }
        m_isInProcess = cfUtilities.IsInstanceInProcess(contentId, folderId, m_refContentApi.RequestInformationRef.ContentLanguage);
    }


    /// <summary>
    /// Set initial parameters, called from view approval and add advanced approval methods.
    /// </summary>
    private void SetQueryStringParm()
    {
        if (Request.QueryString["type"] != null)
        {
            m_objectType = Convert.ToString(Request.QueryString["type"]).Trim().ToLower();
        }
        if (Request.QueryString["id"] != null)
        {
            long.TryParse(Request.QueryString["id"], out m_objectId);
        }
        if (Request.QueryString["action"] != null)
        {
            m_strPageAction = Convert.ToString(Request.QueryString["action"]).ToLower().Trim();
        }
        if (Request.QueryString["orderby"] != null)
        {
            m_strOrderBy = Convert.ToString(Request.QueryString["orderby"]);
        }
        if (Request.QueryString["LangType"] != null)
        {
            if (Request.QueryString["LangType"] != "")
            {
                int.TryParse(Request.QueryString["LangType"], out m_contentLanguage);
                m_refContentApi.SetCookieValue("LastValidLanguageID", m_contentLanguage.ToString());
            }
            else
            {
                if (m_refContentApi.GetCookieValue("LastValidLanguageID") != "")
                {
                    int.TryParse(m_refContentApi.GetCookieValue("LastValidLanguageID"), out m_contentLanguage);
                }
            }
        }
        else
        {
            if (m_refContentApi.GetCookieValue("LastValidLanguageID") != "")
            {
                int.TryParse(m_refContentApi.GetCookieValue("LastValidLanguageID"), out m_contentLanguage);
            }
        }

        if (m_contentLanguage == Ektron.Cms.Common.EkConstants.CONTENT_LANGUAGES_UNDEFINED)
        {
            m_refContentApi.ContentLanguage = Ektron.Cms.Common.EkConstants.ALL_CONTENT_LANGUAGES;
        }
        else
        {
            m_refContentApi.ContentLanguage = m_contentLanguage;
        }

        if (Request.QueryString["vmap"] != null)
        {
            m_versionMap = Request.Params["vmap"];
        }

        if (Request.Params["approval$hdnVersionMap"] != null && Request.Params["approval$hdnVersionMap"] != "")
        {
            m_versionMap = Request.Params["approval$hdnVersionMap"];
        }

        jsType.Text = m_objectType;
        jsFolderId.Text = m_objectId.ToString();
        jsId.Text = m_objectId.ToString();
        jsAction.Text = m_strPageAction;
        jsLang.Text = this.m_contentLanguage.ToString();
        m_currentUserId = m_refContentApi.UserId;
        m_appImgPath = m_refContentApi.AppImgPath;
        m_sitePath = m_refContentApi.SitePath;
        m_enableMultilingual = m_refContentApi.EnableMultilingual;
    }

    /// <summary>
    /// Register JavaScript and CSS files.
    /// </summary>
    private void RegisterResources()
    {
        ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
        Ektron.Cms.Framework.UI.Package resources = new Ektron.Cms.Framework.UI.Package()
        {
            Components = new List<Component>()
                {
					// js resources
					Ektron.Cms.Framework.UI.Packages.EktronCoreJS,
					Ektron.Cms.Framework.UI.Packages.Ektron.Namespace,
					Ektron.Cms.Framework.UI.Packages.Ektron.Workarea.Core,
					Ektron.Cms.Framework.UI.Packages.Ektron.JSON,

					// css resources
					Css.Create(cmsContextService.WorkareaPath + "/controls/approval/manageApprovals.css")
				}
        };
        resources.Register(this);
    }

    private void SetObjectData()
    {
        m_securityData = m_refContentApi.LoadPermissions(m_objectId, m_objectType, 0);
        ContentWorkflowDefinitionManager defManager = new ContentWorkflowDefinitionManager(ApiAccessMode.Admin);
        m_isHerited = defManager.IsInherited(m_objectId, m_objectType);
        if (m_objectType == FOLDER)
        {
            m_folderData = m_refContentApi.GetFolderById(m_objectId);
            m_forceAllApprovers = m_folderData.ApprovalMethod;
            if (m_isHerited)
            {
                FolderData fd = m_refContentApi.GetFolderById(m_folderData.InheritedFrom);
                m_folderPath = fd.NameWithPath;
                if (m_folderPath.Length == 1) m_folderPath = m_refMsg.GetMessage("lbl root folder");
                m_title = m_folderData.NameWithPath.Substring(0, m_folderData.NameWithPath.Length - 1);
            }
            else
            {
                m_title = m_folderData.NameWithPath.Substring(0, m_folderData.NameWithPath.Length - 1);
                m_folderPath = m_folderData.NameWithPath;
            }
        }
        else
        {
            m_contentData = m_refContentApi.GetContentById(m_objectId, 0);
            m_title = m_contentData.Title;
            jsFolderId.Text = m_contentData.FolderId.ToString();
        }

        GetWorkflowTypeAndStatus();
    }

    #endregion

    #region ToolBars

    private void SetTitleBar(string title, string optional)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(m_refMsg.GetMessage(title));
        if (!string.IsNullOrEmpty(optional))
        {
            sb.Append(" ");
            sb.Append(optional);
        }

        var aLocalizationApi = new LocalizationAPI();
        var aLocalizedFlagString = "&nbsp;&nbsp;<img style='vertical-align:middle;' src='" + aLocalizationApi.GetFlagUrlByLanguageID(m_contentLanguage) + "' />";

        txtTitleBar.InnerHtml = m_refStyle.GetTitleBar(sb.ToString()) + aLocalizedFlagString;
    }

    private void FinishToolBar(StringBuilder result)
    {
        result.Append(StyleHelper.ActionBarDivider);
        result.Append("<td>");
        result.Append(m_refStyle.GetHelpButton(m_strPageAction, ""));
        result.Append("</td>");
        result.Append("</tr></table>");
        htmToolBar.InnerHtml = result.ToString();
    }

    private void BackOnlyToolBar(string title, string optional)
    {
        SetTitleBar(title, optional);
        StringBuilder sb = new StringBuilder();
        sb.Append("<table><tr><td>");

        if (m_objectType == FOLDER)
        {
            sb.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/back.png", (string)("content.aspx?action=ViewFolder&id=" + m_objectId + "&treeViewId=0&LangType=" + m_contentLanguage), m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));
        }
        else
        {
            sb.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/back.png", (string)("content.aspx?action=view&id=" + m_objectId + "&LangType=" + m_contentLanguage), m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));
        }

        sb.Append("</td>");
        FinishToolBar(sb);
    }

    private void CancelWorkflowToolBar()
    {
        SetTitleBar("lblContentWorkflowManageApprovals", "");
        StringBuilder sb = new StringBuilder();
        sb.Append("<table><tr><td>");
        sb.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/back.png", (string)("content.aspx?action=view&id=" + m_objectId + "&LangType=" + m_contentLanguage), m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));
        sb.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/cancel.png", (string)("content.aspx?action=view&id=" + m_objectId + "&LangType=" + m_contentLanguage), m_refMsg.GetMessage("lblContentWorkflowCancel"), m_refMsg.GetMessage("lblContentWorkflowCancel"), "onclick=\"openCancelWorkflowDialog(); return false;\"", "", true));

        sb.Append("</td>");
        FinishToolBar(sb);
    }

    private void ViewApprovalToolBar()
    {
        System.Text.StringBuilder result = new System.Text.StringBuilder();
        bool bInherited = false;
        bool bFolderUserAdmin;

        if (!(m_folderData == null))
        {
            bFolderUserAdmin = m_securityData.IsAdmin || m_refContentApi.IsARoleMemberForFolder_FolderUserAdmin(m_folderData.Id, 0, false);
        }
        else
        {
            if (!(m_contentData == null))
            {
                bFolderUserAdmin = m_securityData.IsAdmin || m_refContentApi.IsARoleMemberForFolder_FolderUserAdmin(m_contentData.FolderId, 0, false);
            }
            else
            {
                bFolderUserAdmin = m_securityData.IsAdmin;
            }
        }
        if (m_objectType == FOLDER)
        {
            bInherited = m_folderData.IsPermissionsInherited;
        }
        else
        {
            bInherited = m_contentData.IsPermissionsInherited;
        }

        if (m_objectType == FOLDER)
        {
            SetTitleBar("view folder approvals msg", m_folderData.Name);
        }
        else
        {
            SetTitleBar("view content approvals msg", m_contentData.Title);
        }
        result.Append("<table><tr>");

        if (m_objectType == FOLDER)
        {
            result.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/back.png", (string)("content.aspx?action=ViewFolder&id=" + m_objectId + "&LangType=" + m_contentLanguage), m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));
        }
        else
        {
            result.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/back.png", (string)("content.aspx?LangType=" + m_contentLanguage + "&action=View&id=" + m_objectId), m_refMsg.GetMessage("alt back button text"), m_refMsg.GetMessage("btn back"), "", StyleHelper.BackButtonCssClass, true));
        }

        if (bFolderUserAdmin && (bInherited == false) && m_isAdvancedWorkflow)
        {
            // Apply the logic to save the workflow.
            result.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "../UI/Icons/save.png", "", m_refMsg.GetMessage("btn save"), m_refMsg.GetMessage("btn save"), "onclick=\"return VerifyInputs();\"", StyleHelper.SaveButtonCssClass, true)).Append(Environment.NewLine);

            if (m_workflowDefinitionId > 0)
            {
                result.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/remove.png", "", "Remove Workflow", "Remove Workflow", "onclick=\"openDialog(); return false;\""));
                SetAdvancedWorkflowRemovalMessages(m_objectType);
            }
        }
        else if (bFolderUserAdmin && (bInherited == false) && !m_isAdvancedWorkflow)
        {
            result.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/add.png", (string)("content.aspx?LangType=" + m_contentLanguage + "&action=AddApproval&id=" + m_objectId + "&type=" + m_objectType), m_refMsg.GetMessage("alt add button text (approvals)"), m_refMsg.GetMessage("btn add"), "", StyleHelper.AddButtonCssClass, true));
            if (m_isApprovalChainExists)
            {
                result.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/delete.png", (string)("content.aspx?LangType=" + m_contentLanguage + "&action=DeleteApproval&id=" + m_objectId + "&type=" + m_objectType), m_refMsg.GetMessage("alt delete button text (approvals)"), m_refMsg.GetMessage("alt delete button text (approvals)"), "", StyleHelper.DeleteButtonCssClass));
                result.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/arrowUpDown.png", (string)("content.aspx?LangType=" + m_contentLanguage + "&action=EditApprovalOrder&id=" + m_objectId + "&type=" + m_objectType), m_refMsg.GetMessage("alt edit button text (approvals)"), m_refMsg.GetMessage("alt edit button text (approvals)"), "", StyleHelper.ReOrderButtonCssClass));
            }
            result.Append(m_refStyle.GetButtonEventsWCaption(m_workareaPath + "images/UI/Icons/contentEdit.png", (string)("content.aspx?LangType=" + m_contentLanguage + "&action=EditApprovalMethod&id=" + m_objectId + "&type=" + m_objectType), m_refMsg.GetMessage("edit approvals method"), m_refMsg.GetMessage("edit approvals method"), "", StyleHelper.EditButtonCssClass));
        }
        result.Append(StyleHelper.ActionBarDivider);
        result.Append("<td>");
        result.Append(m_refStyle.GetHelpButton(m_strPageAction, ""));
        result.Append("</td>");
        result.Append("</tr></table>");
        htmToolBar.InnerHtml = result.ToString();

    }

    #endregion


    // this routine is never called because ASP sends the events to the containing page first (content.aspx). 
    // The actual event is handled by the ViewApproval method
    protected void btnPlaceHolder_click(object sender, EventArgs e) { }

}

