﻿<%@ WebHandler Language="C#" Class="addadvancedworkflow" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Collections.Generic;

using Ektron.Cms.Contracts.Common;
using Ektron.Cms.Contracts.ContentWorkflow;
using Ektron.Cms.Framework;
using Ektron.Cms.Framework.ContentWorkflow;
using Ektron.Cms.Framework.Settings;
using Ektron.Cms.Framework.User;
using Ektron.Cms.Settings;
using Ektron.Cms.Common;

public class addadvancedworkflow : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{
    private ContentWorkflowDefinitionData _currentDefinition
    {
        get { return (ContentWorkflowDefinitionData)System.Web.HttpContext.Current.Session["CurrentContentWorkflowDefinition"]; }
        set { System.Web.HttpContext.Current.Session["CurrentContentWorkflowDefinition"] = value; }
    }

    private List<ContentWorkflowActivityData> _currentActivities
    {
        get { return (List<ContentWorkflowActivityData>)System.Web.HttpContext.Current.Session["CurrentContentWorkflowActivity"]; }
        set { System.Web.HttpContext.Current.Session["CurrentContentWorkflowActivity"] = value; }
    }
    private long definitionId = 0;
    private string versionMap = string.Empty;
    private long id = 0;    
    private int contentLanguage = 1033;
    private string type = string.Empty;
    private long folderId = 0;
    
    public void ProcessRequest (HttpContext context) {
        try
        {
            
            context.Response.ContentType = "text/plain";
            context.Server.ScriptTimeout = 6000;

            ContentWorkflowDefinitionData _data = new ContentWorkflowDefinitionData();

            ContentWorkflowDefinitionManager defMgr = new ContentWorkflowDefinitionManager(ApiAccessMode.Admin);
            ContentWorkflowActivityManager activityMgr = new ContentWorkflowActivityManager(ApiAccessMode.Admin);

            _currentDefinition = new ContentWorkflowDefinitionData();
            
            string [] requestParm = context.Request.Form.ToString().Split('&');
			long.TryParse(requestParm[0].Split('=')[1], out definitionId);
            this.versionMap = System.Web.HttpUtility.UrlDecode(requestParm[1].Split('=')[1]);
            
            this.type= requestParm[2].Split('=')[1];
			int.TryParse(requestParm[3].Split('=')[1], out this.contentLanguage);
			long.TryParse(requestParm[5].Split('=')[1], out this.id);
			long.TryParse(requestParm[7].Split('=')[1], out this.folderId);

			var activities = requestParm[8].Split('=');
			var decoded = HttpUtility.UrlDecode(activities[1]);
			List<ContentWorkflowActivityData> lstActivities = Ektron.Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContentWorkflowActivityData>>(decoded);

			_currentDefinition.Id = definitionId;
			_currentDefinition.FolderId = this.folderId;
			_currentDefinition.IsActive = true;
			_currentDefinition.LanguageId = this.contentLanguage;
			_currentDefinition.VersionMap = this.versionMap;
			_currentDefinition.ContentId = (this.type == "content") ? this.id : 0;               
             
			//don't add/update activities if the operation on the definition failed.
			try
			{
				if (_currentDefinition.Id == 0)
				{
					_currentDefinition = defMgr.Add(_currentDefinition);
                    try
                    {
                        lstActivities.ForEach(x => x.DefinitionId = _currentDefinition.Id);
                        lstActivities = activityMgr.Add(lstActivities);
                    }
                    catch (Exception ex3)
                    {
                        // delete the wf definition if adding the activities failed
                        defMgr.Delete(_currentDefinition.Id);
						ReturnAjaxError(ex3.Message);
                        return;
                    }

				}
				else
				{
					_currentDefinition = defMgr.Update(_currentDefinition);
                    try
                    {
                        lstActivities.ForEach(x => x.DefinitionId = _currentDefinition.Id);
                        lstActivities = activityMgr.Update(lstActivities);
                    }
                    catch (Exception ex2)
                    {	
						ReturnAjaxError(ex2.Message);
                        return;
                    }
				}
			}
			catch (Exception ex1)
			{
				ReturnAjaxError(ex1.Message);
				return;
			}

			
        }
        catch(Exception ex)  {
			ReturnAjaxError(ex.Message);
        }
    }

	private void ReturnAjaxError(string msg)
	{
		HttpContext.Current.Response.StatusCode = 500;
		HttpContext.Current.Response.StatusDescription = msg;
		HttpContext.Current.Response.End();
	}
	
    private void UpdateAssignedWfDefCache(ContentWorkflowDefinitionData defData)
    {
        List<ContentWorkflowDefinitionData> assignedWf = GetAssignedWorkflowDefinitions(defData.FolderId);
        int ii = assignedWf.IndexOf(defData);
        if (ii > -1)
        {
            assignedWf[ii] = defData;
        }
        else
        {
            assignedWf.Add(defData);
        }
        System.Web.HttpContext.Current.Session["ContentWorkflowDefinitionId" + defData.FolderId.ToString()] = assignedWf;
    }

    private List<ContentWorkflowDefinitionData> GetAssignedWorkflowDefinitions(long folderId)
    {
        List<ContentWorkflowDefinitionData> assignedWf = null;
        assignedWf = (List<ContentWorkflowDefinitionData>)HttpContext.Current.Session["ContentWorkflowDefinitionId" + folderId.ToString()];
        if (assignedWf == null)
        {
            ContentWorkflowDefinitionManager _defMgr = new ContentWorkflowDefinitionManager();
            assignedWf = new List<ContentWorkflowDefinitionData>();
            ContentWorkflowDefinitionCriteria criteria = new ContentWorkflowDefinitionCriteria();
            criteria.AddFilter(ContentWorkflowDefinitionProperty.FolderId, CriteriaFilterOperator.EqualTo, folderId);
            assignedWf = _defMgr.GetList(criteria);
            System.Web.HttpContext.Current.Session["ContentWorkflowDefinitionId" + folderId.ToString()] = assignedWf;
        }
        return assignedWf;
    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}