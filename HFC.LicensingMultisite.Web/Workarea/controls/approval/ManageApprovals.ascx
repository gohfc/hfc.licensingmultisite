<%@ Control Language="C#" AutoEventWireup="true" Inherits="ManageApprovals" CodeFile="ManageApprovals.ascx.cs" %>
<%@ Register Assembly="Ektron.Cms.Framework.UI.Controls.EktronUI" Namespace="Ektron.Cms.Framework.UI.Controls.EktronUI" TagPrefix="ektronUI" %>

<script type="text/javascript">
    var jsAction="<asp:literal id="jsAction" runat="server"/>",
	 jsType="<asp:literal id="jsType" runat="server"/>",
	 jsId="<asp:literal id="jsId" runat="server"/>",
	 jsLang="<asp:literal id="jsLang" runat="server"/>",
	 jsFolderId="<asp:literal id="jsFolderId" runat="server"/>",
	 jsMode = "<asp:literal id="jsMode" runat="server"/>"; 

	 Ektron.ready(function(){
    	$ektron(".ddlSelType").change(function() {
    		if ($ektron(".hdnVersionMap").val() !== this.value) {
    			$ektron(".hdnVersionMap").val(this.value);
    			$ektron(".hdnWorkflowChanged").val("true");
    		}
    		else 
    		{
    			$ektron(".hdnWorkflowChanged").val("false");
    		}

            document.forms[0].submit();
        });
        $ektron(".wfRadioButton").change(function () {		
            //buttons is a collection of span elements that contain the radio button
            var buttons = $ektron(this.parentElement).find(".wfRadioButton"),
				ClickedId = $ektron(this).find("input").attr("value");
            buttons.each(function() {
                var inputButton = $ektron(this).find("input");
                if (ClickedId !== inputButton.attr("value")) {
                    $ektron(inputButton).attr('checked',false);
                }
            });
        });
        $ektron('.hideDialog').removeClass('hideDialog');
    });

    function LoadApproval(FormName){

        var num=document.forms[0].selLang.selectedIndex;
        document.forms[0].action="content.aspx?action="+jsAction+"&type="+jsType+"&id="+jsId+"&LangType="+document.forms[0].selLang.options[num].value+"&approvalmode="+jsMode;
        document.forms[0].submit();
        return false;
    }
    VerifyInputs = function(){	
        var activities = [],approvalOrder = 1;
        var buildActivities = function(ii,tr) 
        {
            var activityName = $ektron(this).find('.ActivityName').html();
            //skip the header row
            if ((activityName !== undefined) && (activityName.length > 0))
            {
                // 0 = user id, 1 = user type
                var approver = $ektron(this).find('.wfApprover').val().split('|');				
                var activity = {
                    ActivityName: activityName,
                    UserId: approver[0],
                    UserType: approver[1],
                    DefinitionId: 0,
                    ApprovalOrder: approvalOrder };
                approvalOrder = approvalOrder + 1;

                var escalate = $ektron(this).find('#radioEscalate');
                if (escalate[0].checked == true) {
                    activity.EscalationTimeInterval = $ektron(this).find('.ddlEscalationWait ').val();
                    var user = $ektron(this).find('.ddlEscalateTo').val().split('|');
                    activity.EscalationObjectId = user[0];
                    activity.EscalationUserType = user[1];
                }
                activities.push(activity);
            }
        };
        var table = $ektron("#gridAdvWorkflowApprovers");
        table.find('tr').each(buildActivities);
        var jsonActivities = JSON.stringify(activities);	

        selWorkFlowItem = $ektron('.ddlSelType').prop("selectedIndex");   
	    
        if(selWorkFlowItem == 0)
        {
            alert('Please select a workflow.'); 
        }
        else
        { 
            var workflowDefinitionData = {
                definitionId:  $ektron('.hdnDefinitionId').val(),
                versionMap: $ektron('.ddlSelType').val(),
                type: jsType,
                langType: jsLang, 
                isActive: true,
                id: jsId,
                queryStringParm: "action=addadvancedapproval",
                folderId: jsFolderId,
                activities: jsonActivities
            };

            $ektron.ajax({
                type: "POST",
                url: "controls/approval/addadvancedworkflow.ashx",
                data: workflowDefinitionData,
                success: function(data) {
                    DoBackRedirect();
                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert(errorThrown);
                }
            }); 
        }
        return false;	    
    };
    
    DeleteDefinition = function() {
        var defId = $ektron('.hdnDefinitionId').val();

        if(defId)
            var workflowDefinitionData = {
                definitionId:  defId,
                objectId: jsId,
                objectType: jsType
            };

        $ektron.ajax({
            type: "POST",
            url: "controls/approval/deleteadvancedworkflow.ashx",
            data: workflowDefinitionData,
            success: function(data) {
                openSuccessDialog();
            },
            error: function(jqXHR, textStatus, errorThrown){
                alert(jqXHR.responseText);
            }
        }); 
        return false;
    };

    CancelWorkflow = function() {
        var cancelData = {
            contentId: jsId,
            languageId: jsLang,
            folderId: jsFolderId
        }
        $ektron.ajax({
            type: "POST",
            url: "controls/approval/canceladvancedworkflow.ashx",
            data: cancelData,
            success: function(data) {
                $ektron("<%= uxAdvancedWorkflowCancelDialog.Selector %>").dialog("close");
			    $ektron("<%= uxAdvancedWorkflowCancelSuccessDialog.Selector %>").dialog("open");
			},
		    error: function(jqXHR, textStatus, errorThrown){
		        alert(jqXHR.responseText);
		    }
		}); 
	    return false;
	}
    DoBackRedirect = function() {
        var pageAction = jsType == "folder" ? "ViewFolder" : "view";
        var redirectUrl = "content.aspx?LangType=" + jsLang + "&action=" + pageAction + "&id=" + jsId;
        $ektron(top.document).find("#BottomRightFrame").find("#ek_main")[0].src = redirectUrl;
    };
</script>



<div id="dhtmltooltip"></div>
<div class="ektronPageHeader">
	<div class="ektronTitlebar" id="txtTitleBar" runat="server"></div>
	<div class="ektronToolbar" id="htmToolBar" runat="server"></div>
</div>

<div>
	
	<div style="margin-top: 100px; margin-left: 15px" >
		<asp:Panel ID="pnlInherited" runat="server" Visible="false" CssClass="hideDialog">
			<ektronUI:Message ID="uxInheritedMessage" DisplayMode="Information" runat="server" CssClass="hideDialog">
				<ContentTemplate>
					<asp:Literal ID="lblContentWorkflowInheritanceMsg" runat="server" />
				</ContentTemplate>
			</ektronUI:Message>
		</asp:Panel>
	</div>

	<div >
		<asp:Panel ID="pnlChooseMethod" runat="server" Visible="false">
			<div>
				<asp:Literal ID="ltrSelectInstructions" runat="server" />
			</div>
			<div style="margin-top: 20px; font-weight: 800; text-decoration: underline;">
				<asp:LinkButton ID="ltrBasicHeading" runat="server" OnClick="btnPlaceHolder_click" />
			</div>
			<div style="margin-top: 5px; margin-left: 25px;">
				<asp:Literal ID="ltrBasicInstructions" runat="server" />
			</div>
			<div style="margin-top: 10px; margin-left: 25px;">
				<ektronUI:Button ID="btnSelectBasic" OnClick="btnPlaceHolder_click" runat="server"></ektronUI:Button>
			</div>

			<div style="margin-top: 20px; font-weight: 800; text-decoration: underline;">
				<asp:LinkButton ID="ltrAdvancedHeading" runat="server" OnClick="btnPlaceHolder_click" />
			</div>
			<div style="margin-top: 5px; margin-left: 25px;">
				<asp:Literal ID="ltrAdvancedInstructions" runat="server" />
			</div>
			<div style="margin-top: 10px; margin-left: 25px;">
				<ektronUI:Button ID="btnSelectAdvanced" OnClick="btnPlaceHolder_click" runat="server"></ektronUI:Button>
			</div>
		</asp:Panel>
	</div>

	<asp:Panel runat="server" ID="pnlBasicWorkFlow" Visible="false">
		<asp:Label ID="lblBasicInheritedMsg" runat="server" Visible="false" />
		<table class="ektronGrid">
			<tr>
				<td class="label"><%=this.m_refMsg.GetMessage("lbl approval method")%></td>
				<td class="readOnlyValue">
					<asp:Label ID="lblMethod" runat="server" /></td>
			</tr>
		</table>
		<div style="margin-top: .5em;">
			<asp:DataGrid ID="ViewApprovalsGrid"
				runat="server"
				Width="100%"
				AutoGenerateColumns="False"
				EnableViewState="False"
				CssClass="ektronGrid"
				GridLines="None">
				<HeaderStyle CssClass="title-header" />
			</asp:DataGrid>
		</div>
	</asp:Panel>

	<asp:Panel runat="server" ID="pnlAdvancedWorkFlow" Visible="false" Style="margin-top: 20px; margin-left: 15px;">
		<asp:Literal runat="server" ID="ltrAdvancedWorkflowDesc" />
		<div style="margin-top: 105px;">
			<div>
				<asp:Label ID="lblContentWorkflowTypeDropDownInstructions" runat="server" />
			</div>
			<div style="margin-top: 20px; margin-bottom: 20px;">
				<asp:Label runat="server" ID="lblSelType" AssociatedControlID="ddlSelType" CssClass="ektron-approval"></asp:Label><span class="ektron-ui-required" style="margin-left: 5px;">*</span>
				<asp:DropDownList runat="server" ID="ddlSelType" CssClass="ddlSelType" EnableViewState="true" Style="margin-left: 10px;">
				</asp:DropDownList>
			</div>
		</div>
		<div style="margin-top: 10px;">
			<asp:Panel runat="server" ID="pnlAdvWorkflowDetails" Visible="false">
				<div style="border: solid black 1px; border-radius: 5px; padding: 10px;">
					<div style="font-weight: normal;">
						<asp:Label runat="server" ID="lblInstructions" />
					</div>
					<div style="margin-top: 10px; padding: 5px; max-height:400px; overflow:auto;">
						<ektronUI:GridView ID="gridAdvWorkflowApprovers" ClientIDMode="Static" runat="server"
							AutoGenerateColumns="false" OnRowDataBound="grvGrid_RowBound">
							<Columns>
								<asp:TemplateField>
									<HeaderTemplate>
										<asp:Label runat="server" ID="lblActivityName" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Label ID="Label1" Text='<%# Eval("ActivityName") %>' runat="server" class="ActivityName" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField>
									<HeaderTemplate>
										<asp:Label runat="server" ID="lblApprovers" />
									</HeaderTemplate>
									<ItemTemplate>
										<span style="margin-bottom: 5px; display: block;"><%=this.m_refMsg.GetMessage("lblContentWorkflowAssignTo")%></span>
										<asp:DropDownList Style="min-width: 50%;" runat="server" ID="ddlApprovers" Class="wfApprover" DataTextField="DisplayName" DataValueField="UserIdType">
										</asp:DropDownList>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField>
									<HeaderTemplate>
										<asp:Label runat="server" ID="lblEscalation" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:RadioButton runat="server" ID="radioDoNotEscalate" Style="display: block; margin-top: 10px; margin-bottom: 5px;" CssClass="wfRadioButton" Checked="true" />
										<asp:RadioButton runat="server" ID="radioEscalate" Style="margin-right: 0px" CssClass="wfRadioButton" Checked="false" />
										<asp:DropDownList runat="server" ID="ddlEscalationWait" Style="width: 15%;" CssClass="ddlEscalationWait">
										</asp:DropDownList>
										<span><%=this.m_refMsg.GetMessage("lblContentWorkflowEscalationWait2")%></span>
										<asp:DropDownList runat="server" ID="ddlEscalateTo" class="ddlEscalateTo"
											Style="min-width: 50%; margin-left: 20px; margin-top: 10px; margin-bottom: 10px; display: block" DataTextField="DisplayName" DataValueField="UserIdType">
										</asp:DropDownList>
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</ektronUI:GridView>
					</div>
				</div>
			</asp:Panel>
		</div>
	</asp:Panel>

	<asp:Panel runat="server" ID="pnlInProcess" Visible="false" CssClass="ektron-workflow-ui-wrapper hideDialog">
		<div style="margin-top: 100px; margin-left: 15px" class="hideDialog">
			<ektronUI:Message DisplayMode="Error" runat="server" ID="uxInprocessMsg" CssClass="hideDialog">
				<ContentTemplate>
					<asp:Literal ID="ltrInProcessMsg" runat="server" />
				</ContentTemplate>
			</ektronUI:Message>
			<p>
				<asp:Literal ID="ltrWaitingMsg" runat="server"></asp:Literal>
			</p>
			<div style="width: 60%; margin-top: 10px">
				<ektronUI:GridView ID="gridDocsInProcess" ClientIDMode="Static" runat="server"
					AutoGenerateColumns="false" OnRowDataBound="grdDocsInProcessBound" Visible="true">
					<Columns>
						<asp:TemplateField>
							<HeaderTemplate>
								<asp:Label runat="server" ID="lblContentTitle" />
							</HeaderTemplate>
							<ItemTemplate>
								<asp:Label ID="Label2" Text='<%# Eval("Title") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField>
							<HeaderTemplate>
								<asp:Label runat="server" ID="lblUserName" />
							</HeaderTemplate>
							<ItemTemplate>
								<asp:Label ID="Label3" Text='<%# Eval("Username") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField>
							<HeaderTemplate>
								<asp:Label runat="server" ID="lblUpdated" />
							</HeaderTemplate>
							<ItemTemplate>
								<asp:Label ID="Label4" Text='<%# Eval("Updated") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</ektronUI:GridView>
			</div>
		</div>
	</asp:Panel>

	<input type="hidden" name="hdnWorkflowChanged" id="hdnWorkflowChanged" class="hdnWorkflowChanged" runat="server" />
	<input type="hidden" id="hdnVersionMap" class="hdnVersionMap" runat="server"  />
	<input type="hidden" id="hdnDefinitionId" runat="server" class="hdnDefinitionId" />
	<input id="hdnIsSave" runat="server" name="hdnIsSave" class="hdnIsSave" type="hidden" />
	<input id="hdnWFItem" runat="server" name="hdnWFItem" type="hidden" class="hdnWFItem" />
	<input id="hdnWFValue" runat="server" name="hdnWFValue" type="hidden" class="hdnWFValue" />
	<%--	<asp:label id="lblApproverErrorMsg" runat="server"  type="hidden" ClientIDMode="Static" style="display:none" />--%>

	<ektronUI:JavaScriptBlock ID="uxOpenDialogScript" runat="server" ExecutionMode="OnParse">
		<ScriptTemplate>
			function openDialog(){
            $ektron("<%= advancedWorkflowDeleteDialog.Selector %>").dialog("open");
        }
        function openSuccessDialog() {
            $ektron("<%= advancedWorkflowDeleteDialog.Selector %>").dialog("close");
            $ektron("<%= advancedWorkflowDeleteDialogSuccess.Selector  %>").dialog("open");
        }
		function openCancelWorkflowDialog() {
			$ektron("<%= uxAdvancedWorkflowCancelDialog.Selector %>").dialog("open");
			}
		</ScriptTemplate>
	</ektronUI:JavaScriptBlock>

	<ektronUI:Dialog ID="advancedWorkflowDeleteDialog" runat="server" AutoOpen="false" Modal="true" Width="600">
		<Buttons>
			<ektronUI:DialogButton ID="uxRemoveAdvancedWorkflowButton" runat="server" Text="Remove" OnClientClick="DeleteDefinition()" />
			<ektronUI:DialogButton ID="uxCancelAdvancedWorkflowDeleteButton" runat="server" CloseDialog="true" Text="Cancel" />

		</Buttons>
		<ContentTemplate>
			<div class="uxAdvancedWorkflowRemovalConfirmation hideDialog">
				<div class="confirmation-message hideDialog">
					<ektronUI:Message ID="uxAdvancedWorkflowRemoveWarning" runat="server" DisplayMode="Warning" CssClass="hideDialog" />
				</div>
				<div>
					<%= advancedWorkflowRemovalDescription %>
				</div>
			</div>
		</ContentTemplate>
	</ektronUI:Dialog>

	<ektronUI:Dialog ID="advancedWorkflowDeleteDialogSuccess" runat="server" AutoOpen="false" Modal="true" Width="600">
		<Buttons>
			<ektronUI:DialogButton ID="uxRemoveAdvancedWorkflowOkayButton" runat="server" Text="Okay" OnClientClick="DoBackRedirect()" />
		</Buttons>
		<ContentTemplate>
			<div class="uxAdvancedWorkflowRemovalAjaxSuccess hideDialog">
				<ektronUI:Message ID="uxAdvancedWorkflowRemovalSuccess" runat="server" DisplayMode="Success" CssClass="hideDialog" />
			</div>
		</ContentTemplate>
	</ektronUI:Dialog>

	<ektronUI:Dialog ID="uxAdvancedWorkflowCancelDialog" runat="server" AutoOpen="false" Modal="true" Width="600">
		<Buttons>
			<ektronUI:DialogButton ID="uxButtonDoCancelWorkflow" runat="server" OnClientClick="CancelWorkflow()" Text="Yes" />
			<ektronUI:DialogButton ID="uxButtonExitCancelWorkflow" runat="server" CloseDialog="true" Text="No" />
		</Buttons>
		<ContentTemplate>
			<div class="hideDialog">
				<div>
					<ektronUI:Message ID="uxCancelWorkflowMessage" runat="server" DisplayMode="Warning" CssClass="hideDialog" />
				</div>
				<div style="font-weight: bold">
					<%= uxCancelWorkflowQuestion %>
				</div>
			</div>
		</ContentTemplate>
	</ektronUI:Dialog>

	<ektronUI:Dialog ID="uxAdvancedWorkflowCancelSuccessDialog" runat="server" AutoOpen="false" Modal="true" Width="600">
		<Buttons>
			<ektronUI:DialogButton ID="uxBtnCancelOk" runat="server" Text="Okay" OnClientClick="document.location.reload(true);" />
		</Buttons>
		<ContentTemplate>
			<div class="hideDialog">
				<ektronUI:Message ID="uxAdvancedWorkflowCancelSuccessMessage" runat="server" DisplayMode="Success" CssClass="hideDialog" />
			</div>
		</ContentTemplate>
	</ektronUI:Dialog>

</div>

