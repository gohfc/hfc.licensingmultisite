﻿<%@ WebHandler Language="C#" Class="CanDeleteAdvWorfklowContent" %>

using System;
using System.Web;
using Ektron.Cms;
using Ektron.Cms.BusinessObjects.ContentWorkflow;
using Ektron.Cms.User;
using Ektron.Cms.Settings;
using Ektron.Cms.Common;

public class CanDeleteAdvWorfklowContent : IHttpHandler
{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
		context.Server.ScriptTimeout = 6000;
		
		string response = "";
		long folderId = -1;
		long contentId = -1;
		if (!long.TryParse(context.Request.Params["contentId"], out contentId)) {
			response = "contentId parameter is missing from the request";
		} else if (!long.TryParse(context.Request.Params["folderId"], out folderId)) {
			response = "folderId parameter is missing from the request";
		} else {
			//if iterator's managed then it can'testc be deleted
			response = (CheckWorkflow(contentId, folderId, context.Request.Params["objectType"])) ? "true" : "false";
		}
        context.Response.Write(response);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

	public static bool CheckWorkflow(long contentId, long folderId, string objectType)
	{
		bool canDelete = false;
		ContentAPI contentApi = new ContentAPI();
		try
		{
			ContentData contentData = contentApi.GetContentById(contentId);
			folderId = (contentData.IsPermissionsInherited) ? contentData.PermissionInheritedFrom : contentData.FolderId;

			ContentWorkflowUtilities cwfutilties = new ContentWorkflowUtilities(contentApi.RequestInformationRef);
			long workflowdefinitionid = cwfutilties.GetInheritedWorkflowDefinitionId(contentId, folderId, contentApi.RequestInformationRef.ContentLanguage);
			if (workflowdefinitionid > 0)
			{
				IUserGroup _usergroupmanager = ObjectFactory.GetUserGroup(contentApi.RequestInformationRef);
				IPermissionManager _ipermissionmanager = ObjectFactory.GetPermissionManager(contentApi.RequestInformationRef);
				if (contentData.IsPermissionsInherited)
				{
					var permissionData = _ipermissionmanager.GetUserPermissionForFolder(contentApi.RequestInformationRef.UserId, contentData.PermissionInheritedFrom, contentApi.RequestInformationRef.ContentLanguage);
					canDelete = permissionData.CanDelete;
				}
				else
				{
					var permissionData = _ipermissionmanager.GetUserPermissionForContent(contentApi.RequestInformationRef.UserId, contentId, contentApi.RequestInformationRef.ContentLanguage);
					canDelete = permissionData.CanDelete;
				}


				if ((contentApi.IsARoleMember((long)EkEnumeration.CmsRoleIds.ContentWorkflowAdmin, contentApi.RequestInformationRef.UserId, true) && canDelete)
						|| _usergroupmanager.IsUserInGroup(contentApi.RequestInformationRef.UserId, 1) || contentApi.RequestInformationRef.CallerId == EkConstants.InternalAdmin)
				{
					canDelete = true;
				}
				else
				{
					canDelete = false;
				}
			}
			else
			{
				canDelete = true;
			}
		}
		catch (Exception ex)
		{
			EkException.LogException(ex, System.Diagnostics.EventLogEntryType.Error);
		}
		return canDelete;
	}
}