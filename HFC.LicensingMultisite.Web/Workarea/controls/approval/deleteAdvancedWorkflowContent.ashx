﻿<%@ WebHandler Language="C#" Class="deleteAdvancedWorkflowContent" %>

using System;
using System.Web;
using Ektron.Cms;
using Ektron.Cms.ContentWorkflow;
using Ektron.Cms.Workarea.ContentWorkflow;

public class deleteAdvancedWorkflowContent : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        long aContentId;
        long aFolderId;
        int aLanguageId;

        long.TryParse(context.Request.Form["contentId"], out aContentId);
        long.TryParse(context.Request.Form["folderId"], out aFolderId);
        int.TryParse(context.Request.Form["languageId"], out aLanguageId);

        DoContentForceDelete(aContentId, aFolderId, aLanguageId);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private void DoContentForceDelete(long theContentId, long theFolderId, int theLanguageId)
    {
        var contentApi = new Ektron.Cms.ContentAPI();
        var aRequestInfo = contentApi.RequestInformationRef;
        var _usergroupmanager = ObjectFactory.GetUserGroup(aRequestInfo);
        var _ipermissionmanager = ObjectFactory.GetPermissionManager(aRequestInfo);
        var canDelete = false;
        
        var aContentData = contentApi.GetContentById(theContentId, ContentAPI.ContentResultType.Published);
        if (aContentData != null)
        {
            UserPermissionData permissionData;

            if (aContentData.IsPermissionsInherited)
                permissionData = _ipermissionmanager.GetUserPermissionForFolder(aRequestInfo.UserId, theFolderId, theLanguageId);
            else
                permissionData = _ipermissionmanager.GetUserPermissionForContent(aRequestInfo.UserId, theContentId, theLanguageId);

            canDelete = permissionData.CanDelete;

            var aWorkareaUtil = new ClientWorkflowUtilities();
            if (aWorkareaUtil.IsWorkflowAdmin(aRequestInfo.UserId) && canDelete)
            {
                theFolderId = aContentData.IsPermissionsInherited ? aContentData.PermissionInheritedFrom : theFolderId;
                var isInheritedWorkflow = aContentData.IsPermissionsInherited;
                ContentWorkflowManager.ForceDelete(theContentId, theFolderId, theLanguageId, contentApi.RequestInformationRef, isInheritedWorkflow);

            }
        }
    }

}