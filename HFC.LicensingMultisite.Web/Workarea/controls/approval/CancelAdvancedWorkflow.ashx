﻿<%@ WebHandler Language="C#" Class="CancelAdvancedWorkflow" %>

using System;
using System.Web;
using Ektron.Cms;
using Ektron.Cms.ContentWorkflow;

public class CancelAdvancedWorkflow : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
		try
		{
			long contentId = 0;
			long folderId;
			int languageId;
			long.TryParse(context.Request.Form["contentId"], out contentId);
			long.TryParse(context.Request.Form["folderId"], out folderId);
			int.TryParse(context.Request.Form["languageId"], out languageId);
			ContentAPI contentAPI = new ContentAPI();
			ContentWorkflowManager.CancelWorkflow(contentId, folderId, languageId, contentAPI.RequestInformationRef);
		}
		catch (Exception ex)
		{
		}
	}
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}