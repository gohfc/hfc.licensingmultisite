﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="addLocaleTaxonomy.ascx.cs" Inherits="addLocaleTaxonomy" %>
<%@ Register Src="~/Workarea/controls/SharedTaxonomyScripts/ValidateTaxonomyName.ascx" TagPrefix="uc1" TagName="ValidateTaxonomyName" %>


<uc1:ValidateTaxonomyName runat="server" ID="ValidateTaxonomyName" />
<script type="text/javascript">

    function confirmSubmit(chkBox) {
        var outcome;
        if (chkBox.checked) {
            outcome = confirm('<%=m_refMsg.GetMessage("js:Confirm enable taxonomy all languages")%>');
	       if (outcome)
	           document.getElementById('<%=alllanguages.ClientID%>').value = "";
       }
       else {
           outcome = confirm('<%=m_refMsg.GetMessage("js:Confirm disable taxonomy all languages")%>');
	       if (outcome)
	           document.getElementById('<%=alllanguages.ClientID%>').value = "false";

     }
 }

 function updateText(obj) {
     $ektron("#taxonomy_txtValue")[0].value = obj.value;
 }

 function ToggleSelection(obj) {
     $ektron("#taxonomy_hdnSelectedIDS")[0].value = obj.checked;
     if (!obj.checked) {
         $ektron($ektron(obj)[0].parentNode.parentNode).find("td")[3].childNodes.item().disabled = true;
     }
     else {
         $ektron($ektron(obj)[0].parentNode.parentNode).find("td")[3].childNodes.item().disabled = false;
     }
     return false;
 }
</script>

<div id="dhtmltooltip"></div>
<div class="ektronPageHeader">
    <div class="ektronTitlebar" id="txtTitleBar" runat="server"></div>
    <div class="ektronToolbar" id="htmToolBar" runat="server"></div>
</div>
<div id="searchpanel" class="ektronPageContainer ektronPageInfo">
        <div class="tabContainerWrapper">
            <div class="tabContainer">
                <ul>
                    <li>
                        <a title="Properties" href="#dvProperties">
                            <%=m_refMsg.GetMessage("properties text")%>
                        </a>
                    </li>
                    
                </ul>
                <div id="dvProperties">
                    <table class="ektronForm">
                        <tr>
                            <td class="label" title="Category Title"><%=m_refMsg.GetMessage("localecategorytitle")%>:</td>
                            <td><asp:TextBox ToolTip="Category Title" ID="taxonomytitle" runat="server" />&nbsp;<asp:Label ID="lblLanguage" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label" title="Category Description"><%=m_refMsg.GetMessage("localecategorydescription")%>:</td>
                            <td><asp:TextBox ToolTip="Category Description" ID="taxonomydescription" Rows="5" TextMode="MultiLine" runat="server" /></td>
                        </tr>

                    </table>
                </div>    
             
            </div>
        </div>    
    <input type="hidden" id="alllanguages" runat="server"  />
    <input type="hidden" runat="server" id="txtValue" name="txtValue" />
    <input type='hidden' id='hdnSelectedIDS' name='hdnSelectedIDS' runat="server" />
    <input type='hidden' id='hdnSelectValue' name='hdnSelect' runat="server" />
    
</div>