<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Referring_url.ascx.cs" Inherits="controls_analytics_Referring_url" %>
<%@ Register TagPrefix="ektron" TagName="PercentPieChart" Src="../reports/PercentPieChart.ascx" %>

<asp:Panel ID="pnlnavBarcont" runat="server" Visible="false">
<div class="ektronPageContainer ektronPageTabbed" style="margin: 5px 0 0 0; position: inherit;top: 0;">
    <div class="tabContainerWrapper">
        <ektronUI:Tabs ID="Tabs" runat="server">
            <ektronUI:Tab ID="TabReferrerStatistics" runat="server" Text="Referrer Statistics">
                <ContentTemplate>
                    <div id="dvreferStatistics" >
                        <blockquote>
                            <div id="stats_aggr" runat="server" style="width: 50%">
                                <table border="0" width="95%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_total_hits" runat="server" Font-Bold="True" Text="Total Views of Page" />
                                        </td>
                                        <td>
                                            <asp:Label ID="num_total_hits" runat="server" Text="100" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_total_visitors" runat="server" Font-Bold="True" Text="Total Visitors to Page" />
                                        </td>
                                        <td>
                                            <asp:Label ID="num_total_visitors" runat="server" Text="20" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_hits_per_visitor" runat="server" Font-Bold="True" Text="Page Views/Visitor" />
                                        </td>
                                        <td>
                                            <asp:Label ID="num_hits_per_visitor" runat="server" Text="5" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_new_visitors" runat="server" Font-Bold="True" Text="New Visitors to Page" />
                                        </td>
                                        <td>
                                            <asp:Label ID="num_new_visitors" runat="server" Text="10" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_returning_visitors" runat="server" Font-Bold="True" Text="Returning Visitors to Page" />
                                        </td>
                                        <td>
                                            <asp:Label ID="num_returning_visitors" runat="server" Text="10" />
                                        </td>
                                    </tr>
                                </table>
                                <table id="TABLE1" width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_hits_vs_visitors" runat="server" Text="Content Views vs. Visitors"
                                                Font-Bold="True" />
                                            <br />
                                            <ektron:PercentPieChart ID="graph_hits_per_visitor" Width="175px" Height="150px"
                                                Legend="BottomHorizontal" runat="server" Visible="true" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_new_vs_returning_visitors" runat="server" Text="New Vs. Returning Visitors"
                                                Font-Bold="True" />
                                            <br />
                                            <ektron:PercentPieChart ID="graph_new_vs_returning_visitors" Width="175px" Height="150px"
                                                Legend="BottomHorizontal" runat="server" Visible="true" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </blockquote>
                    </div>
                </ContentTemplate>
            </ektronUI:Tab>
            <ektronUI:Tab ID="TabReferrerActivity" runat="server" Text="Referrer Activity">
                <ContentTemplate>
                    <div id="dvreferActivity" style="margin-top:10px;">
                        <asp:Image ID="Image1" runat="server" />
                    </div>
                </ContentTemplate>
            </ektronUI:Tab>
            <ektronUI:Tab ID="TabPaths" runat="server" Text="Paths" OnClick="TabPaths_Click">
                <ContentTemplate>
                    <div id="dvPaths">
                        <asp:GridView ID="GridView2" CssClass="ektronGrid ektronTopSpace"
                            runat="server" 
                            AutoGenerateColumns="False" 
                            Width="100%" 
                            BorderColor="White"
                            AllowPaging="True" 
                            AllowSorting="True" 
                            PageSize="3"  OnSorting="GridView2_Sorting" OnPageIndexChanging="GridView2_PageIndexChanging">
                            <HeaderStyle CssClass="title-header" />
                            <Columns>
                                <asp:BoundField DataField="url" HeaderText="Page" SortExpression="url" />
                                <asp:BoundField DataField="Landings" HeaderText="Landings" SortExpression="Landings" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </ektronUI:Tab>
            <ektronUI:Tab ID="TabTopLandingPages" runat="server" Text="Top Landing Pages" OnClick="TabTopLandingPages_Click">
                <ContentTemplate>
                    <div id="dvLandPage">
                        <asp:GridView ID="GridView3" CssClass="ektronGrid ektronTopSpace"
                            runat="server" 
                            AutoGenerateColumns="False" 
                            Width="100%" 
                            AllowPaging="True" 
                            BorderColor="White"     
                            AllowSorting="True" 
                            PageSize="3"  OnSorting="GridView3_Sorting" OnPageIndexChanging="GridView3_PageIndexChanging">
                            <HeaderStyle CssClass="title-header" />
                            <Columns>
                                <asp:BoundField DataField="referring_url_path" HeaderText="Referring URL" SortExpression="referring_url_path" />
                                <asp:BoundField DataField="Landings" HeaderText="Landings" SortExpression="Landings" />
                            </Columns>
                        </asp:GridView>                
                    </div>
                </ContentTemplate>
            </ektronUI:Tab>
        </ektronUI:Tabs>
    </div>
</div>
</asp:Panel>
<asp:HiddenField ID="selTab" runat="server" Value="null" /> 

<asp:GridView ID="GridView1" CssClass="ektronGrid ektronTopSpace"
    runat="server" 
    AutoGenerateColumns="False" 
    Width="100%"     
    AllowPaging="True" 
    AllowSorting="True" 
    PageSize="3" GridLines="None" OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging">
    <HeaderStyle CssClass="title-header" />
    <Columns>
        <asp:HyperLinkField HeaderText="Page" DataNavigateUrlFields="referring_url" DataTextField="referring_url" ShowHeader="False" SortExpression="referring_url" />
        <asp:BoundField DataField="referrals" HeaderText="Referrals" SortExpression="referrals" />
    </Columns>
</asp:GridView>
<asp:Label ID="ErrMsg" runat="server" EnableViewState="False" Visible="False"/>

