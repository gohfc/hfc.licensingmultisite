﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ValidateTaxonomyName.ascx.cs" Inherits="Workarea_controls_SharedTaxonomyScripts_ValidateTaxonomyName" %>
<ektronUI:JavaScriptBlock runat="server" ID="validateTaxonomyNameScriptBlock" ExecutionMode="OnEktronReady">
    <ScriptTemplate>
        var Ektron = window.Ektron || {};
        Ektron.Workarea = Ektron.Workarea || {};
        
        Ektron.Workarea.Taxonomy = {

                   Validate:function() {
                            var controlid = "taxonomy_";
                   var idInfo = document.getElementById(controlid + "taxonomytitle");
        
                   if(idInfo != null){
                            var taxonomyName = idInfo.value;
                            if (document.getElementById(controlid + "taxonomytitle").value == '') {
                                alert('<%=m_refMsg.GetMessage("js:alert taxonomy required field")%>');
                                return false;
                            }
                            if (document.getElementById(controlid + 'chkConfigContent') != null && document.getElementById(controlid + 'chkConfigUser') != null && document.getElementById(controlid + 'chkConfigGroup') != null) {
                                if (document.getElementById(controlid + 'chkConfigContent').checked == false && document.getElementById(controlid + 'chkConfigUser').checked == false && document.getElementById(controlid + 'chkConfigGroup').checked == false) {
                                    alert('<%=m_refMsg.GetMessage("js:alert configuration selection required")%>');
                                    return false;
                                }
                            }
                            if ((taxonomyName.indexOf('>') > -1) || (taxonomyName.indexOf('<') > -1) || (taxonomyName.indexOf('"') > -1) || (taxonomyName.indexOf(';') > -1)) {
                                alert("The taxonomy name can not contain <, >, \" or ; character");
                            }
                            else if (taxonomyName.toLowerCase() == 'assets' || taxonomyName.toLowerCase() == 'privateassets') {
                                alert('<%=m_refMsg.GetMessage("js:alert tax name cannot be assets")%>');
                            }
                            else {
                                document.forms[0].submit();
                            }
                        }
                    else
                    {
                        document.forms[0].submit();
                    }
        }

        };

    </ScriptTemplate>

</ektronUI:JavaScriptBlock>
