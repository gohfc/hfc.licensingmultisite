﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Ektron.Cms.Common;
using Ektron.Cms;
using System.Net;
using System.IO.Compression;
using SharpZipLib2.SharpZipLib.GZip;

public partial class Workarea_updatewurflfile : Ektron.Cms.Workarea.Page
{
    protected EkMessageHelper _MessageHelper;
    protected ContentAPI _ContentApi = new ContentAPI();
    protected StyleHelper _StyleHelper = new StyleHelper();
    protected CommonApi _CommonApi = new CommonApi();
    protected void Page_Init(object sender, System.EventArgs e)
    {
        this.RegisterCSS();
        this.RegisterJS();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["action"]))
        {
            switch (Request.QueryString["action"].ToLower())
            {
                case "view":
                default:
                    View();
                    break;
                case "update":
                    ltrUpdateMessage.Text = _CommonApi.UpdateWurflFile();
                    mvUpdateFile.SetActiveView(vUpdate);
                    break;
            }
        }
    }
    private void View()
    {
        _MessageHelper = _ContentApi.EkMsgRef;
        FileInfo fInfo = new FileInfo(Server.MapPath("~/App_Data/wurfl.zip"));
        if (fInfo.Exists)
        {
            ltrDateUpdated.Text = _MessageHelper.GetMessage("lbl device list update") + fInfo.LastWriteTime + ". ";
        }
        else
        {
            ltrDateUpdated.Text = "File doesnot exists";
        }
        hlUpdate.NavigateUrl = _ContentApi.ApplicationPath + "updatewurflfile.aspx?action=update";
        mvUpdateFile.SetActiveView(vView);
    }
    private void RegisterCSS()
    {
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaCss);
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronWorkareaIeCss, Ektron.Cms.API.Css.BrowserTarget.LessThanEqualToIE7);
    }

    private void RegisterJS()
    {
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronWorkareaJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronWorkareaHelperJS);
        Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJFunctJS);
    }
}