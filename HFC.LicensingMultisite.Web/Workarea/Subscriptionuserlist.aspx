﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Subscriptionuserlist.aspx.cs"
    Inherits="Workarea_Subscriptionuserlist" ValidateRequest="false" %>
<%@ Register TagPrefix="uxEktron" TagName="Paging" Src="Controls/paging/paging.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Subscription Users List</title>
    <asp:Literal ID="jsStyleSheet" runat="server" />
    <script type="text/javascript">
        function resetPostback() {
            document.forms[0].isPostData.value = "";
        }
        function searchuser() {            
            if (document.forms[0].txtSearch.value.indexOf('\"') != -1) {
                alert('remove all quote(s) then click search');
                return false;
            }
            document.forms[0].isSearchPostData.value = "1";
            document.forms[0].isPostData.value = "true";
            $ektron("#txtSearch").clearInputLabel();
            document.forms[0].submit();
            return true;
        }
        function CheckForReturn(e) {
            var keynum;
            var keychar;

            if (window.event) // IE
            {
                keynum = e.keyCode
            }
            else if (e.which) // Netscape/Firefox/Opera
            {
                keynum = e.which
            }

            if (keynum == 13) {
                document.getElementById('btnSearch').focus();
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" method="post">
    <div class="ektronPageHeader">
        <div class="ektronTitlebar" id="txtTitleBar" runat="server">
        </div>
        <div class="ektronToolbar" id="htmToolBar" runat="server">
    </div>
    </div>
    <div class="ektronPageContainer ektronPageInfo">
        <asp:DataGrid ID="MapCMSSubUserToADGrid" runat="server" AutoGenerateColumns="false"
            GridLines="none" Width="100%" AllowCustomPaging="True" PageSize="10" PagerStyle-Visible="False"
            CssClass="ektronGrid" EnableViewState="False">
            <HeaderStyle CssClass="title-header" />
        </asp:DataGrid>
        <asp:Literal ID="ltr_message" runat="server" />
    <uxEktron:Paging ID="uxPaging" runat="server" />
    </div>
    <input type="hidden" runat="server" id="isPostData" value="true" name="isPostData" />
    <input type="hidden" runat="server" id="isSearchPostData" value="" name="isSearchPostData" />

    </form>
</body>
</html>
