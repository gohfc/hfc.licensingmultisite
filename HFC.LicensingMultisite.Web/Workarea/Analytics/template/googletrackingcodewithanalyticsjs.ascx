﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="googletrackingcodewithanalyticsjs.ascx.cs" Inherits="Analytics_Template_GoogleTrackingCodeWithAnalyticsJS" EnableTheming="false" EnableViewState="false" %>
<!-- Start Google Code -->
<script type="text/javascript">
 (function (i, s, o, g, r, a, m) {
     i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
         (i[r].q = i[r].q || []).push(arguments)
     }, i[r].l = 1 * new Date(); a = s.createElement(o),
     m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
 })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', '<asp:literal id="GoogleUserAccount" runat="server"/>', document.location.hostname);
ga('send', 'pageview');
ga('set', 'Member', '<asp:literal runat="server" id="variables"/>');

</script>
<!-- End Google Code -->