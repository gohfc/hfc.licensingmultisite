﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Interfaces.Context;

public partial class Workarea_Analytics_Tracking_Beacon_JS : System.Web.UI.Page
{
    private ICmsContextService cmsContextService;
    public ICmsContextService CmsContextService
    {
        get
        {
            if (this.cmsContextService == null)
            {
                this.cmsContextService = ServiceFactory.CreateCmsContextService();
            }
            return this.cmsContextService;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
        HttpContext.Current.Response.Expires = 0;
        HttpContext.Current.Response.AddHeader("Pragma", "no-cache");
        HttpContext.Current.Response.AddHeader("cache-control", "private, no-cache, must-revalidate no-store pre-check=0 post-check=0 max-stale=0");
        HttpContext.Current.Response.Cache.SetNoServerCaching();
        HttpContext.Current.Response.ContentType = "application/javascript";
        this.DataBind();
        base.OnLoad(e);
    }
}