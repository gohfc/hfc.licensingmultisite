﻿<%@ Page Language="C#" Debug="true" AutoEventWireup="true" CodeFile="embedinsert.aspx.cs"
    Inherits="Workarea_embedinsert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style>
        html,body{
    background-color:rgb(94,94,94) !important;
    color:white;
    padding:20px;
    overflow:hidden;
}
        textarea, select{width:100%;margin-bottom:20px;}
        .option input {margin-bottom: 15px;}
        .embedbuttontext{
            color:white;
            background: #D99F42;
            background: linear-gradient(to bottom, #E9C892 0%, #E0B267 40%, #DCA752 63%, #D9A043 87%, #D99F42 100%) repeat scroll 0 0 #D99F42;
            border: 0 none;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 1px 1px 2px #000000;
            color: #FFFFFF;
            padding: 10px;
            margin-right:20px;
            cursor: pointer;
        }
        .cancelbuttontext{
            color:white;
            background: #8D8D8D;
            background: linear-gradient(to bottom, #BDBDBD 0%, #9A9A9A 57%, #919191 77%, #8D8D8D 90%, #8D8D8D 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
            border: 0 none;
            border-radius: 3px 3px 3px 3px;
            box-shadow: 1px 1px 2px #000000;
            color: #FFFFFF;
            padding: 10px;
            cursor: pointer;
        }
    </style>
</head>
<script language="javascript" type="text/javascript">
    function insertHTML() {
        var resultTag = '';
        var text = '';
        var optionSelected = $ektron("option:selected")[0].value;
        switch (optionSelected) {
            case 'other': createOtherEmbed(); break;
            case 'youtube': createYouTubeEmbed(); break;
            case 'vimeo': createVimeoEmbed(); break;
            case 'dailymotion': createDailyMotionEmbed(); break;
            case 'googlemaps': createGoogleMapsEmbed(); break;
            case 'instagram': createInstagramEmbed(); break;
            case 'vine': createVineEmbed(); break;
            case 'iframe': createiFrameEmbed(); break;
            default: createOtherEmbed(); break;
        }
        text = $ektron('#EmbedSource').val()
        text = $ektron.trim(text);
        //if (text.indexOf('<') == -1 && text.length > 0) {
        //    text = '<span>' + text + '</span>';
        //}

        if (Ektron.Namespace.Exists("parent.Ektron.Embed.AcceptHTML")) {
            //Get allowed elements from AlohaEktron configurations settings to lower case
            //var aeArray = normalize();
            //var jqueryText = $ektron('<div>' + text + '</div>');
            //var elements = jqueryText.children();
            //if (elements.length > 0) {
            //    $(elements).each(function () {
            //        resultTag = verifyHTML($(this), aeArray, "");
            //        if (resultTag.length > 0) {
            //            return false;
            //        }
            //    });
            //}
            //if (resultTag.length > 0) {
            //    var msg1 = parent.Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.errorMessage1;
            //    var msg2 = parent.Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.errorMessage2;
            //    msg2 = msg2.replace(/{TagToken}/gi, resultTag);
            //    var msgall = "<p>" + msg1 + "</p><p>" + msg2 + "</p>";
            //    $ektron(".ektron-ui-messageBody p").remove();
            //    $ektron(msgall).appendTo(".ektron-ui-messageBody");
            //    $ektron("#uxErrorMessage").show();
            //}
            //else {
            if (text != '') {
                parent.Ektron.Embed.AcceptHTML({ "html": text });
            }
            //}
        }
    }

    //Check for elements and attributes not allowed before insert
    function verifyHTML(obj, allowedObj, failTag) {
        if (failTag.length > 0) { return failTag; }
        var thisTag = obj.get(0).tagName.toLowerCase();

        if ($.inArray(thisTag, allowedObj.elements) === -1) {
            failTag = thisTag;
            return failTag;
        }
        else {
            //Get attributes for element
            var myval = getAttributes(obj);
            //Check attributes present
            if (myval.length > 0) {
                //Get allowed attributes for this element
                var myList = allowedObj.attributes[thisTag];
                if (myList) {
                    for (var a = 0; a < myval.length; a++) {
                        if ($.inArray(myval[a], myList) === -1) {
                            failTag = 'attribute ' + myval[a] + ' for element ' + thisTag;
                            break;
                        }
                    }
                }
                else {
                    //no attributes configured
                    failTag = 'attributes for ' + thisTag;
                }
            }
        }

        if (failTag.length === 0) {
            obj.children().each(function () {
                var thisTag = $(this).get(0).tagName.toLowerCase();
                if ($.inArray(thisTag, allowedObj.elements) === -1) {
                    failTag = thisTag;
                    return false;
                }
                else {
                    failTag = verifyHTML($(this), allowedObj, failTag);
                }
            });
        }
        return failTag;
    }

    //Get attibutes for current element
    function getAttributes(obj) {
        var queryArr = [];
        var objAttributes = obj[0].attributes;
        var lenAttr = objAttributes.length;
        var objTag = obj.get(0).tagName.toLowerCase();
        for (a = 0; a < lenAttr; a++) {
            queryArr.push(objAttributes[a].name);
        }
        return queryArr;
    }

    //Get allowed elements from AlohaEktron configurations settings to lower case
    function normalize() {
        //Get allowed list from AlohaEktron configurations settings to lower case
        var allowed = parent.Aloha.settings.contentHandler.allows;
        $.each(allowed.elements, function (index, item) {
            allowed.elements[index] = item.toLowerCase();
        });
        var allowedAttributes = allowed.attributes;

        for (var key in allowedAttributes) {
            var temp;
            temp = allowedAttributes[key];
            $.each(temp, function (index, item) {
                temp[index] = item.toLowerCase();
            });
            delete allowedAttributes[key];
            allowedAttributes[key.toLowerCase()] = temp;
        }
        return allowed;
    }



    $(document).ready(function () {
        var inst = parent.Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.instructionMessage;
        //$ektron("span.ektron-aloha-embed-header").text(inst);

        var canceltext = parent.Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.canceltext;
        $ektron(".cancelbuttontext:button").val(canceltext);

        var embedtext = parent.Ektron.Controls.Editor.Aloha.Plugins.Embed.ResourceText.embedtext;
        $ektron(".embedbuttontext:button").val(embedtext);

        $ektron('#embedselect').on('change', function (e) {
            $ektron('.option').hide();
            $ektron('.' + this.value).show();
            $ektron('#EmbedSource').val('');
            $ektron('#uxErrorMessage').hide();
            //debugger;
        });

    });      //end ready

    ////////////////////////////////////////////EMBED CODE
    var embedContainerCSS = "<style>.embed-container { position: relative; padding-bottom: 56.25%; padding-top: 30px; height: 0; overflow: hidden; max-width: 100%; height: auto; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>";
    var embedSquareContainerCSS = "<style>.embed-container {position: relative; padding-bottom: 100%; padding-top: 30px; height: 0; overflow: hidden;} .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>";
    var embedSquarishContainerCSS = "<style>.embed-container {position: relative; padding-bottom: 120%; padding-top: 30px; height: 0; overflow: hidden;} .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>";
    var embedContainerDivOpen = "<div class='embed-container' contenteditable='false'>";
    var embedContainerDivClose = "</div>";

    function createYouTubeEmbed() {
        var youtubeURL = $("#youtube-url").val();
        if (youtubeURL.length > 28) {
            var uri = youtubeURL;
            var queryString = {};
            uri.replace(
			    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
			    function ($0, $1, $2, $3) { queryString[$1] = $3; }
			);
            var youtubeID = (queryString['v']);
        } else {
            var youtubeID = youtubeURL.substring(16);
        }

        var youtubeEmbed = "<iframe src='http://www.youtube.com/embed/" + youtubeID + "?wmode=transparent' frameborder='0' allowfullscreen></iframe>";
        var youtubepreview = "<iframe src='http://www.youtube.com/embed/" + youtubeID + "?wmode=transparent' frameborder='0' allowfullscreen></iframe>";

        //$("#youtubeembedCode").html(embedLabel + "<textarea rows='12' class='codebox'>" + embedContainerCSS + embedContainerDivOpen + youtubeEmbed + embedContainerDivClose + "</textarea>");

        if (youtubeURL.indexOf('youtube.com') == -1) {
            $ektron('#uxErrorMessage .ektron-ui-messageBody').text("The URL is incorrect. The URL must be from youtube.com");
            $ektron('#uxErrorMessage').show();
            $ektron('#EmbedSource').val('');
        } else {
            $ektron('#EmbedSource').val(embedContainerCSS + embedContainerDivOpen + youtubeEmbed + embedContainerDivClose);
        }
    }

    function createiFrameEmbed() {
        var iframeSrc = $("#iframe-src").val();

        var iframeEmbed = "<iframe src='" + iframeSrc + "' frameborder='0'></iframe>";
        //$("#youtubeembedCode").html(embedLabel + "<textarea rows='12' class='codebox'>" + embedContainerCSS + embedContainerDivOpen + youtubeEmbed + embedContainerDivClose + "</textarea>");

        $ektron('#EmbedSource').val(embedContainerCSS + embedContainerDivOpen + iframeEmbed + embedContainerDivClose);
    }

    function createVimeoEmbed() {
        var vimeoURL = $("#vimeo-url").val();
        var protocol = vimeoURL.slice(0, 5);
        if (protocol == "https") {
            //then the video is served via https, doy
            var vimeoID = vimeoURL.substring(18);
        } else {
            protocol = "http";
            var vimeoID = vimeoURL.substring(17);
        }

        var vimeoEmbed = "<iframe src='" + protocol + "://player.vimeo.com/video/" + vimeoID + "' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
        var vimeopreview = "<iframe src='" + protocol + "://player.vimeo.com/video/" + vimeoID + "'  frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";

        //$("#vimeoembedCode").html(embedLabel + "<textarea rows='12' class='codebox'>" + embedContainerCSS + embedContainerDivOpen + vimeoEmbed + embedContainerDivClose + "</textarea>");
        if (vimeoURL.indexOf('vimeo.com') == -1) {
            $ektron('#uxErrorMessage .ektron-ui-messageBody').text("The URL is incorrect. The URL must be from vimeo.com");
            $ektron('#uxErrorMessage').show();
            $ektron('#EmbedSource').val('');
        } else {
            $("#EmbedSource").val(embedContainerCSS + embedContainerDivOpen + vimeoEmbed + embedContainerDivClose);
        }

    }

    function createDailyMotionEmbed() {
        var dailymotionURL = $("#dailymotion-url").val();
        var dailymotionID = (m = dailymotionURL.match(new RegExp("\/video\/([^_?#]+).*?"))) ? m[1] : void 0;
        var dailymotionEmbed = "<iframe src='http://www.dailymotion.com/embed/video/" + dailymotionID + "' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
        var dailymotionpreview = "<iframe src='http://www.dailymotion.com/embed/video/" + dailymotionID + "'  frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";

        //$("#dailymotionembedCode").html(embedLabel + "<textarea rows='12' class='codebox'>" + embedContainerCSS + embedContainerDivOpen + dailymotionEmbed + embedContainerDivClose + "</textarea>");
        if (dailymotionURL.indexOf('dailymotion.com') == -1) {
            $ektron('#uxErrorMessage .ektron-ui-messageBody').text("The URL is incorrect. The URL must be from dailymotion.com");
            $ektron('#uxErrorMessage').show();
            $ektron('#EmbedSource').val('');
        } else {
            $ektron('#EmbedSource').val(embedContainerCSS + embedContainerDivOpen + dailymotionEmbed + embedContainerDivClose);
        }

    }

    function createGoogleMapsEmbed() {
        var googlemapsURL = $("#googlemaps-url").val();
        var escapediFrameURL = googlemapsURL.replace(/\"/g, '\'');
        var escapediFrameURLCode = escapediFrameURL.replace(/</g, '&lt;');
        var escapediFrameURLCodeFinal = escapediFrameURLCode.replace(/>/g, '&gt;');

        //alert(escapediFrameURL);
        //$("#googlemapsembedCode").html(embedLabel + "<textarea rows='12' class='codebox'>" + embedContainerCSS + embedContainerDivOpen + escapediFrameURLCodeFinal + embedContainerDivClose + "</textarea>");
        if ((googlemapsURL.indexOf('google.com/maps') == -1) || (googlemapsURL.indexOf('iframe') == -1)) {
            $ektron('#uxErrorMessage .ektron-ui-messageBody').text("The URL is incorrect, or no IFRAME detected. The URL must be from google.com/maps and contain an IFRAME.");
            $ektron('#uxErrorMessage').show();
            $ektron('#EmbedSource').val('');
        } else {
            $ektron('#EmbedSource').val(embedContainerCSS + embedContainerDivOpen + escapediFrameURL + embedContainerDivClose);
        }

    }

    function createInstagramEmbed() {
        var instagramURL = $("#instagram-url").val();
        var shortinstagramURL = instagramURL.slice(5);
        var last_character = shortinstagramURL[shortinstagramURL.length - 1];

        if (last_character != "/") {
            shortinstagramURL = shortinstagramURL + "/";
        }

        var instagramEmbedURL = "<iframe src='" + shortinstagramURL + "embed/' frameborder='0' scrolling='no' allowtransparency='true'></iframe>";
        //$("#instagramembedCode").html(embedLabel + "<textarea rows='12' class='codebox'>" + embedSquarishContainerCSS + embedContainerDivOpen + instagramEmbedURL + embedContainerDivClose + "</textarea>");

        if (instagramURL.indexOf('instagram.com') == -1) {
            $ektron('#uxErrorMessage .ektron-ui-messageBody').text("The URL is incorrect. The URL must be from [instagram.com]");
            $ektron('#uxErrorMessage').show();
            $ektron('#EmbedSource').val('');
        } else {
            $ektron('#EmbedSource').val(embedSquarishContainerCSS + embedContainerDivOpen + instagramEmbedURL + embedContainerDivClose);
        }
    }

    function createVineEmbed() {

        var vineURL = $("#vine-url").val();
        var shortvineURL = vineURL;
        var last_character = shortvineURL[shortvineURL.length - 1];
        var vineScript = "&lt;script async src='//platform.vine.co/static/scripts/embed.js' charset='utf-8'&gt;&lt;/script&gt;";

        if (last_character != "/") {
            shortvineURL = shortvineURL + "/";
        }

        var vineEmbedURL = "<iframe width='100%' src='" + shortvineURL + "embed/simple' frameborder='0' scrolling='no' allowtransparency='true'></iframe>" + vineScript;
        //$("#vineembedCode").html(embedLabel + "<textarea rows='12' class='codebox'>" + embedSquareContainerCSS + embedContainerDivOpen + vineEmbedURL + embedContainerDivClose + "</textarea>");
        if (instagramURL.indexOf('vine.com') == -1) {
            $ektron('#uxErrorMessage .ektron-ui-messageBody').text("The URL is incorrect. The URL must be from [vine.com]");
            $ektron('#uxErrorMessage').show();
            $ektron('#EmbedSource').val('');
        } else {
            $ektron('#EmbedSource').val(embedSquareContainerCSS + embedContainerDivOpen + vineEmbedURL + embedContainerDivClose);
        }

    }

    function createOtherEmbed() {
        var txt = $ektron('#other-code').val();

        if (txt == '') {
            $ektron('#uxErrorMessage .ektron-ui-messageBody').text("There has been no code added to embed.");
            $ektron('#uxErrorMessage').show();
            $ektron('#EmbedSource').val('');
        } else {
            $ektron('#EmbedSource').val(txt);
        }
    }
    //function createGenericEmbed() {
    //    var genericURL = $("#generic-url").val();
    //    var escapediFrameURL = genericURL.replace(/\"/g, '\'');
    //    var escapediFrameURLCode = escapediFrameURL.replace(/</g, '&lt;');
    //    var escapediFrameURLCodeFinal = escapediFrameURLCode.replace(/>/g, '&gt;');

    //    //alert(escapediFrameURL);
    //    $("#genericembedNote").fadeIn();
    //    $("#genericembedCode").html(embedLabel + "<textarea rows='12' class='codebox'>" + embedContainerCSS + embedContainerDivOpen + escapediFrameURLCodeFinal + embedContainerDivClose + "</textarea>");
    //    $("#genericpreview").html(previewLabel + previewPrefix + escapediFrameURL + previewSuffix);

    //}

</script>
<body>
    <form id="form1" runat="server">
        <div class="ektron-aloha-embed-header">
            <span class="ektron-aloha-embed-header">Select the Media Source:</span>
        </div>
        <div class="ektron-aloha-embed-selector">
            <select id="embedselect">
                <option value="other">Other</option>
                <option value="youtube">YouTube</option>
                <option value="vimeo">Vimeo</option>
                <option value="dailymotion">DailyMotion</option>
                <option value="googlemaps" >Google Maps</option>
                <option value="iframe" >iFrame</option>
            </select>
        </div>
        <div>
            <div class="other option">
                <span>Embed Source:</span>
                <textarea id="other-code" class="embed-textval" cols="20" rows="2" style="height: 120px; resize: none; margin-bottom: 15px;"></textarea>
            </div>
            <div class="youtube option" style="display:none;">
                <span>YouTube URL:</span>
                <input id="youtube-url" style="width:100%;" />
            </div>
            <div class="vimeo option" style="display:none;">
                <span>Vimeo URL:</span>
                <input id="vimeo-url" style="width:100%;" />
            </div>
            <div class="dailymotion option" style="display:none;">
                <span>DailyMotion URL:</span>
                <input id="dailymotion-url" style="width:100%;" />
            </div>
            <div class="googlemaps option" style="display:none;">
                <span>Google Maps (IFRAME):</span>
                <input id="googlemaps-url" style="width:100%;" />
            </div>
            <div class="instagram option" style="display:none;">
                <span>Instagram URL:</span>
                <input id="instagram-url" style="width:100%;" />
            </div>
            <div class="vine option" style="display:none;">
                <span>Vine URL:</span>
                <input id="vine-url" style="width:100%;" />
            </div>
            <div class="iframe option" style="display:none;">
                <span>iFrame Src:</span>
                <input id="iframe-src" style="width:100%;" />
            </div>

            <div class="ektron-ui-control ui-corner-all ektron-ui-message ektron-ui-error ektron-ui-clearfix" id="uxErrorMessage" style="width: 100%;display:none;">
                <span class="ui-icon ektron-ui-sprite ui-icon-notice ektron-ui-sprite-exclamation" id="uxErrorMessage_Message_aspIcon"></span>
                <div class="ektron-ui-clearfix ektron-ui-messageBody">
                </div>
            </div>
            <hr />
            <div class="ektron-aloha-dialog">
                <div style="float: right; margin-right: 16px;">
                    <span>
                        <input type="button" class="embedbuttontext" value="Embed" onclick="insertHTML()" />
                    </span>
                    <span>
                        <input type="button" class="cancelbuttontext" value="Cancel" onclick="parent.$ektron('.ektron-aloha-embed-modal').dialog('close');" />
                    </span>
                </div>
            </div>
        </div>
        <input id="EmbedSource" style="display:none;" />
    </form>
</body>
</html>
