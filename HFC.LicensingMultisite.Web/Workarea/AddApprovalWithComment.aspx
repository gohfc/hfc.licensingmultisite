﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddApprovalWithComment.aspx.cs" Inherits="Workarea_AddApprovalWithComment" %>

<%@ Register Assembly="Ektron.Cms.Framework.UI.Controls.EktronUI" Namespace="Ektron.Cms.Framework.UI.Controls.EktronUI" TagPrefix="ektronUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title></title>
</head>
<body>
	<div style="height: auto; width: auto;">
		<form id="form1" runat="server">
			<asp:Panel ID="uxCommentPanel" runat="server" Visible="true">
				<div style="margin-top: 10px;">
					<ektronUI:Label ID="lblApprovalQuestion" runat="server"></ektronUI:Label>
				</div>
				<div style="margin-left: 10px; margin-top: 10px;">
					<ektronUI:TextField ID="txtApprovalComment" TextMode="MultiLine" runat="server" placeholder="enter comment"></ektronUI:TextField>
				</div>
				<div style="float: right; padding-right: 6px; margin-top: 10px">
					<ektronUI:Button ID="btnApprovalCommentAccept" runat="server" Text="Accept" OnClick="btnApprovalCommentAccept_click"></ektronUI:Button>
					<ektronUI:Button ID="btnApprovalCommentClose" runat="server" Text="Close Dialog" OnClientClick="closeDialog(false);"></ektronUI:Button>
				</div>
			</asp:Panel>
			<asp:Panel ID="uxErrorMessage" runat="server" Visible="false">
				<ektronUI:Message ID="uxAddCommentErrorMessage" runat="server" DisplayMode="Error" />
				<asp:Button ID="btnCloseErrorDialog" runat="server"  OnClick="btnCloseErrorDialog_Click"/>
			</asp:Panel>
			<ektronUI:JavaScriptBlock runat="server" ID="JavaScriptBlock1" ExecutionMode="OnParse">
				<ScriptTemplate>
				Ektron.ready(function(){
						var doApproval=true;

						$ektron('#txtApprovalComment_TextField_aspTextarea').click(function() {

							$ektron('#txtApprovalComment_TextField_aspTextarea').select();
						});

						$('#txtApprovalComment_TextField_aspTextarea').select(function() {
							doApproval = true;
						});
					});
					function closeDialog(comment, doApproval){
						parent.closeApprovalCommentDialog(comment,doApproval);
					}
				</ScriptTemplate>
			</ektronUI:JavaScriptBlock>
		</form>
	</div>
</body>
</html>
