﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page language="c#" CodeFile="resourceselectorpopup.aspx.cs" Inherits="Ektron.ContentDesigner.Dialogs.ResourceSelectorPopup" AutoEventWireup="false" %>
<%@ Register TagPrefix="ek" TagName="FieldDialogButtons" Src="ucFieldDialogButtons.ascx" %>
<%@ Register TagPrefix="radTS" Namespace="Telerik.WebControls" Assembly="RadTabStrip.NET2" %>
<%@ Register TagPrefix="ek" TagName="FolderTree" Src="ucContentFolderTree.ascx" %>
<%@ Register TagPrefix="ek" TagName="TaxonomyTree" Src="ucTaxonomyTree.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title id="Title" runat="server">Select Content</title>
    <style type="text/css">
        div#CBResults 
        {
        	height: 10em !important;
        }
        div#Ektron_Dialog_Tabs_BodyContainer, .Ektron_Dialog_Tabs_BodyContainer, .Ektron_DialogTabBodyContainer
        {
        	margin: 0 !important;
        }
        .Invisible
        {
			display: none;
        }
        span.folder
        {
        	cursor: hand; 
        	cursor: pointer;
        }
        .CBfoldercontainer span.folderselected, div.treecontainer .selected
        {
	        background-color: #9cf !important;
        }
    </style>
</head>
<body class="dialog">
<form id="Form1" runat="server">
   <div class="Ektron_DialogTabstrip_Container">
    <radTS:RadTabStrip id="RadTabStrip1" runat="server" MultiPageID="RadMultiPage1" SelectedIndex="0" ReorderTabRows="true" 
			OnClientTabSelected="ClientTabSelectedHandler" SkinID="TabstripDialog">
        <Tabs>
            <radTS:Tab ID="Folder" Text="Folder" Value="Folder" DisabledCssClass="Invisible" />
            <radTS:Tab ID="Taxonomy" Text="Taxonomy" Value="Taxonomy" DisabledCssClass="Invisible" />
            <radTS:Tab ID="Search" Text="Search" Value="Search" DisabledCssClass="Invisible" />
        </Tabs>
    </radTS:RadTabStrip>  
    </div>
    <div id="TreeViewDiv" class="Ektron_Dialog_Tabs_BodyContainer CBWidget">
        <radTS:RadMultiPage Height="350" id="RadMultiPage1" runat="server" SelectedIndex="0" AutoScrollBars="true">
            <radTS:PageView id="PageViewFolder" runat="server" > 
	            <ek:FolderTree ID="foldertree" runat="server" ShowTree="true" />
	        </radTS:PageView>
            <radTS:PageView id="PageViewTaxonomy" runat="server">
                <ek:TaxonomyTree ID="taxtree" runat="server" />
	        </radTS:PageView>
	        <radTS:PageView id="PageViewSearch" runat="server">
                <ek:FolderTree ID="SearchGrid" runat="server" ShowTree="false" />
	        </radTS:PageView>
        </radTS:RadMultiPage>
        <div id="GroupOptionsDiv" runat="server">
            <input id="chkAllItems" title="<%=GetMessage("lbl all folders") %>" name="chkItem" type="radio" /><label id="lblAllItems" title="All folders" for="chkAllItems" runat="server">All folders</label><br />
            <input id="chkChildItem" title="<%=GetMessage("lbl include child folder") %>" name="chkItem" type="radio" checked="checked" /><label id="lblChildItem" title="From this folder and its subfolder" for="chkChildItem" runat="server">From this folder and its subfolder</label><br />
            <input id="chkCurrentItem" title="<%=GetMessage("lbl this folder only") %>" name="chkItem" type="radio" /><label id="lblCurrentItem" title="From this folder only" for="chkCurrentItem" runat="server">From this folder only</label><br />
        </div>
        <asp:TextBox ID="tbData" ToolTip="Data" CssClass="HiddenTBData" runat="server" style="display:none;"></asp:TextBox>
        <asp:TextBox ID="tbFolderPath" ToolTip="Folder Path" CssClass="HiddenTBFolderPath" runat="server" style="display:none;"></asp:TextBox>
    	<input type="hidden" id="hdnAppPath" name="hdnAppPath" value="" />
        <input type="hidden" id="hdnLangType" name="hdnLangType" value="" />
        <input type="hidden" id="hdnFolderId" name="hdnFolderId" value="0"/>
    </div>
    <div class="Ektron_Dialogs_LineContainer">
        <div class="Ektron_TopSpaceSmall"></div>
        <div class="Ektron_StandardLine"></div>
    </div>		 
    <ek:FieldDialogButtons ID="FieldDialogButtons" OnOK="return insertField();" runat="server" />    
</form> 
<script language="javascript" type="text/javascript">
<!--
    Ektron.ready(function(){
        initField();
        window.focus();
        if($ektron("div#RadTabStrip1 a").is(":visible")) {
            $ektron("div#RadTabStrip1 a").eq(0).focus();
        }
        BindOnRadWindowKeyDown();
    });
    
	var ResourceText = 
	{
		sWarnNoSelection: "<asp:literal id="sWarnNoSelection" runat="server"/>"
	,	sWarnMultiSelection: "<asp:literal id="sWarnMultiSelection" runat="server"/>"
	,	sWarnNoResult: "<asp:literal id="sWarnNoResult" runat="server"/>"
	,   TreeRoot: "<asp:literal id="sTreeRoot" runat="server"/>"
	,   CollectionItems: "<asp:literal id="collectionItems" runat="server"/>"
	,   TaxonomyItems: "<asp:literal id="taxonomyItems" runat="server"/>"
	,   sChildren: "<asp:literal id="sChildren" runat="server"/>"
	,   sItems: "<asp:literal id="sItems" runat="server"/>"
	,   idType: {
            "folder": "<asp:literal id="sFolder" runat="server"/>" 
        ,   "content": "<asp:literal id="sContent" runat="server"/>"
        ,   "content:htmlcontent": "<asp:literal id="sHTMLContent" runat="server"/>" 
        ,   "content:htmlform": "<asp:literal id="sHtmlForm" runat="server"/>" 
	    ,   "content:smartform": "<asp:literal id="sSmartForm" runat="server"/>" 
        ,   "content:asset": "<asp:literal id="sAsset" runat="server"/>" 
        ,   "content:image": "<asp:literal id="sImage" runat="server"/>"
        ,   "content:multimedia": "<asp:literal id="sVideo" runat="server"/>" 
        ,   "content:product": "<asp:literal id="sProduct" runat="server"/>" 
        ,   "content:mso": "<asp:literal id="sMSOffice" runat="server"/>" 
        ,   "taxonomy": "<asp:literal id="sTaxonomy" runat="server"/>" 
        ,   "collection": "<asp:literal id="sCollection" runat="server"/>"
        }  
	};
    var webserviceURL = "";
    var m_filterBy = "content";
    var m_selectorType = "content";
    var m_idType = "content:htmlcontent";
    var m_defaultFolder = 0;
    var m_folderNavigation = "descendant";
    var m_objFormField = null;
    var m_waPath = "<%= new Ektron.Cms.SiteAPI().ApplicationPath %>";
    var m_langType;
    var m_resourceTitle = "";
    
    function initField()
    {
        m_objFormField = new EkFormFields();
        var appPath;
        var idValue;
		var bSearchByFolder = true;
		var bSearchByTaxonomy = true;
		var bSearchByWords = true;
		var sRoot = ResourceText.TreeRoot;
		var searchTerm;

        var args = GetDialogArguments();
        if (args)
        {
            appPath = args.appPath;
            m_langType = args.langType;
			m_idType = args.idType;
			idValue = args.idValue + "";
            bSearchByFolder = args.searchByFolder;
            bSearchByTaxonomy = args.searchByTaxonomy;
            bSearchByWords = args.searchByWords;
            m_filterBy = (args.filterBy || "content:htmlcontent ");
            m_selectorType = args.selectorType;
            m_defaultFolder = args.defaultFolder;
            m_folderNavigation = (args.folderNavigation || m_folderNavigation);
            var startFolderTitle = args.startFolderTitle;
            var rootFolder = 0;
            if (startFolderTitle && startFolderTitle.length > 0 && m_folderNavigation != "ancestor" && m_selectorType != "startingfolder")
            {
                sRoot = args.startFolderTitle;
                rootFolder = m_defaultFolder;
            }
            searchTerm = args.searchTerm;
            m_resourceTitle = getResourceFieldResourceTitle(args.display);
        }
	    document.getElementById("hdnAppPath").value = appPath;
	    document.getElementById("hdnLangType").value = m_langType;
	    
		webserviceURL = (appPath ? appPath : m_waPath + "ContentDesigner/");
		webserviceURL += "dialogs/contentfoldertree.ashx";
		if (m_langType)
		{
			webserviceURL += "?LangType=" + m_langType;
		}
		
        var oTabCtl = <%= RadTabStrip1.ClientID %>;
        var tabFolder = oTabCtl.FindTabById("<%= RadTabStrip1.ClientID %>_Folder");
        var tabTaxonomy = oTabCtl.FindTabById("<%= RadTabStrip1.ClientID %>_Taxonomy");
        var tabWords = oTabCtl.FindTabById("<%= RadTabStrip1.ClientID %>_Search");
		if (bSearchByFolder)
		{
			switch (m_selectorType)
			{
			    case "startingfolder":
                case "folder":
			        //folder selector
			        if ("folder" == m_idType)
			        {
			            if (idValue != "undefined" && idValue.length > 0) 
			            {
			                m_defaultFolder = idValue;
			            }
			            var oCurrentItem = document.getElementById("chkCurrentItem");
			            if (oCurrentItem)
			            {
			                oCurrentItem.disabled = true;
			            }
			        }
		            if (document.getElementById("chkAllItems"))
		            {
		                if ("ancestor" == m_folderNavigation)
		                {
		                    document.getElementById("chkAllItems").checked = true;
		                }
		                else if ("descendant" == m_folderNavigation)
		                {
		                    document.getElementById("chkChildItem").checked = true;
		                }
		                else if (document.getElementById("chkCurrentItem"))
		                {
		                    document.getElementById("chkCurrentItem").checked = true;
		                }
		            }
                    break;
			    case "content":
			    default:
			        //content selector
				    if (searchTerm)
			        {
			            $ektron("span.searchBox > input").attr("value", searchTerm);
			        }
			        break;
			}
		}
		else
		{
            tabFolder.Disable();
		}
		if (bSearchByTaxonomy)
		{
            if (!bSearchByFolder) 
            {
                tabTaxonomy.SelectParents();
            }
        }
		else
		{
            tabTaxonomy.Disable();
		}
		if (bSearchByWords)
		{
            if (!bSearchByFolder && !bSearchByTaxonomy) tabWords.SelectParents();
		}
		else
		{
			tabWords.Disable();
		}
    }	
    
    function insertField()
    {
        var retObj = null;
        var el = null;
        var sIdType = m_idType;
        var idValue;
        switch(m_selectorType)
        {
            case "taxonomy":
                el = $ektron("div.ektron-ui-taxonomyTree").find("li.selected");
                if (el.length === 0)
                {
                    alert(ResourceText.sWarnNoSelection);
                }
                else
                {
                    idValue = parseInt(el.attr("data-ektron-id"), 10);
                    var sResTitle = $ektron.trim($ektron("span.textWrapper", el).eq(0).text());
                    retObj = 
                    {
                        resourceId: idValue
                    ,   resourceTitle: sResTitle
                    ,   sDisplay: m_objFormField.getResourceFieldDisplayValue(ResourceText, m_waPath, sIdType, idValue, sResTitle, m_langType)
                    ,   idType: sIdType
                    };
                }
                break;
            case "folder":
            case "startingfolder":
                el = $ektron("div.ektron-ui-folderTree").find("li.selected");
                if (el.length === 0)
                {
                    alert(ResourceText.sWarnNoSelection);
                }
                else
                {
                    var nav = getBrowseByString();
                    idValue = parseInt(el.attr("data-ektron-id"), 10);
                    var sFolderTitle = $ektron.trim((el.find("span.textWrapper").eq(0).text() || ResourceText.TreeRoot));
                    retObj = 
                    {
                        folderId: idValue
                    ,	folderNavigation: nav
                    ,   folderTitle: sFolderTitle
                    ,   sDisplay: m_objFormField.getResourceFieldDisplayValue(ResourceText, m_waPath, "folder", idValue, sFolderTitle, m_langType, nav)
                    ,   idType: sIdType
                    };  
                }
                break;
            case "content":
            default: 
                el = $ektron("tr.selected");
                if (el.length === 0)
                {
                    alert(ResourceText.sWarnNoSelection);
                }
                else if (el.length > 1)
                {
                    alert(ResourceText.sWarnMultiSelection);
                }
                else
                {
                    var searchTerm = $ektron("span.searchBox > input").attr("value");
                    sIdType = el.find(".ContentType").text().toLowerCase();
                    idValue = parseInt(el.find(".ContentId").text(), 10);
                    var sContentTitle = $ektron.trim(el.find(".ContentTitle").text());
                    retObj = 
                    {
                        idValue: idValue
                    ,	idType: sIdType
                    ,   contentTitle: sContentTitle
                    ,   sDisplay: m_objFormField.getResourceFieldDisplayValue(ResourceText, m_waPath, sIdType, idValue, sContentTitle, m_langType)
                    ,   searchTerm: searchTerm
                    };  
                }
                break;
        }
        if (retObj)
        {
            retObj.selectorType = m_selectorType;
            CloseDlg(retObj);	
        }
        return false;
    }
    
    function getResourceFieldResourceTitle(fullDisplay)
    {
        //For example, resource «Smart Form:564»
        if (!fullDisplay) return "";
        var lend = fullDisplay.indexOf("«");
        if (lend > -1)
        {
            return $ektron.trim(fullDisplay.substr(0, lend));
        }
        else
        {
            return fullDisplay;
        }
    }
    
    function getBrowseByString()
    {
        var sBrowseBy = "";
        if (m_selectorType != "folder")
        {
            if (document.getElementById("chkAllItems").checked) sBrowseBy = "ancestor";
            else if (document.getElementById("chkChildItem").checked) sBrowseBy = "descendant";
            else if (document.getElementById("chkCurrentItem").checked) sBrowseBy = "children";
        }
        return sBrowseBy;
    }
    
    function ClientTabSelectedHandler(sender, eventArgs)
	{
	    $ektron("#CBResults").html("<div class=\"CBNoresults\">&#160;</div>");
	    $ektron("#CBPaging").html("&#160;");
	}
// -->
</script>
</body> 
</html> 
