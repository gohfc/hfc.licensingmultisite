using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Framework.UI.Tree;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using Ektron.Cms.Framework.Organization;
using Ektron.Cms.Organization;
using Ektron.Cms.Common;
using Ektron.Cms.Content;
using System.Text.RegularExpressions;

namespace Ektron.ContentDesigner.Dialogs
{
	public partial class CollectionTree : System.Web.UI.UserControl
	{
        protected SiteAPI m_refSiteApi = new SiteAPI();
        protected Ektron.Cms.Common.EkMessageHelper m_refMsg;
        private string _defaultCollectionId = string.Empty;
        private Ektron.Cms.PagingInfo GridPagingInfo
        {
            get 
            {
                if (ViewState["GridPagingInfo"] == null)
                {
                    Ektron.Cms.PagingInfo info = new Ektron.Cms.PagingInfo();
                    info.RecordsPerPage = 20;
                    ViewState["GridPagingInfo"] = info;
                }
                return ViewState["GridPagingInfo"] as Ektron.Cms.PagingInfo; 
            }
            set { ViewState["GridPagingInfo"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
		{
			ContentAPI capi = new ContentAPI();
            m_refMsg = m_refSiteApi.EkMsgRef;
            CollectionContentList.Caption = m_refMsg.GetMessage("lbl collection items");
            if (!string.IsNullOrEmpty(Request.QueryString["idValue"]))
            {
                _defaultCollectionId = Request.QueryString["idValue"];
            }

            if (!IsPostBack)
            {
                PageRequestData colrequest = new PageRequestData();
                colrequest.PageSize = 1000;
                colrequest.CurrentPage = 1;
                CollectionListData[] collection_list = capi.EkContentRef.GetCollectionList("", ref colrequest);
                
                var data = new List<Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode>();
                foreach (CollectionListData d in collection_list)
                {
                    data.Add(new Ektron.Cms.Framework.UI.Controls.EktronUI.TreeNode() { Id = d.Id.ToString(), Text = d.Title, CommandName = "GetCollectionContent" });
                }

                CollectionsTree.DataSource = data;
                CollectionsTree.DataBind();
            }

			// Register JS
			JS.RegisterJS(this, JS.ManagedScript.EktronJS);
		}
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!Page.IsPostBack && !string.IsNullOrEmpty(_defaultCollectionId))
            {
                GetCollectionContent(Int64.Parse(_defaultCollectionId));
            }
            else if (!string.IsNullOrEmpty(CollectionsTree.CurrentNodeId))
            {
                GetCollectionContent(Int64.Parse(CollectionsTree.CurrentNodeId));
            }
        }

        protected void OnEktronUIPageChanged(Object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            GridPagingInfo = e.PagingInfo;
        }
        protected void CollectionsTree_OnPreSerializeData(Object sender, EventArgs e)
        {
            if (!Page.IsPostBack && CollectionsTree.ItemsFlatList != null)
            {
                foreach (var node in CollectionsTree.ItemsFlatList)
                {
                    if (_defaultCollectionId == node.Id)
                    {
                        node.Selected = true;
                    }
                }
            }
        }

        private void GetCollectionContent(long objectID)
        {
            List<Ektron.Cms.ContentData> items = new List<Ektron.Cms.ContentData>();
            if (objectID > -1)
            {
                Ektron.Cms.Framework.Content.ContentManager contentManager = new Ektron.Cms.Framework.Content.ContentManager();
                ContentCollectionCriteria criteria = new ContentCollectionCriteria(ContentProperty.Id, EkEnumeration.OrderByDirection.Ascending);
                criteria.PagingInfo = GridPagingInfo;
                criteria.AddFilter(objectID);
                List<ContentData> contentList = contentManager.GetList(criteria);

                if (contentList != null && contentList.Count > 0)
                {
                    CollectionContentList.EktronUIPagingInfo = GridPagingInfo;
                    CollectionContentList.DataSource = contentList;
                    CollectionContentList.DataBind();
                }
            }
        }

        protected string GetContentIcon(object ContentTypeID, object ContentSubType, object ImageUrl)
        {
            int contentTypeID = int.Parse(ContentTypeID.ToString());
            int contentSubType = System.Convert.ToInt16(ContentSubType);
            ContentAPI capi = new ContentAPI();
            Microsoft.VisualBasic.Collection item = new Microsoft.VisualBasic.Collection();
            item.Add(contentSubType, "ContentSubType", null, null);
            item.Add(ImageUrl.ToString(), "ImageUrl", null, null);
            return Ektron.Cms.Common.EkFunctions.getContentTypeIconAspx(contentTypeID, item, capi.ApplicationPath);
        }

    }
}
