using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Ektron.Cms;
using Ektron.Cms.API;
using Ektron.Cms.Framework.UI.Tree;
using Ektron.Cms.Content;
using Ektron.Cms.Common;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using Ektron.Cms.Framework.Organization;
using Ektron.Cms.Organization;

namespace Ektron.ContentDesigner.Dialogs
{
	public partial class TaxonomyTree : System.Web.UI.UserControl
	{
		public string Filter = "";
		protected SiteAPI m_refSiteApi = new SiteAPI();
		protected Ektron.Cms.Common.EkMessageHelper m_refMsg;
        private string _defaultTax;
        private string _selectorType = "content";
        private int _recordsPerPage = 10;
        private Ektron.Cms.PagingInfo GridPagingInfo
        {
            get
            {
                if (ViewState["GridPagingInfo"] == null)
                {
                    Ektron.Cms.PagingInfo info = new Ektron.Cms.PagingInfo();
                    info.RecordsPerPage = _recordsPerPage;
                    ViewState["GridPagingInfo"] = info;
                }
                return ViewState["GridPagingInfo"] as Ektron.Cms.PagingInfo;
            }
            set { ViewState["GridPagingInfo"] = value; }
        }
        private string GridFolderId
        {
            get { return ViewState["GridFolderId"] as string; }
            set { ViewState["GridFolderId"] = value; }
        }

		protected void Page_Load(object sender, EventArgs e)
		{
			m_refMsg = m_refSiteApi.EkMsgRef;
			ContentAPI capi = new ContentAPI();

            if (!string.IsNullOrEmpty(Request.QueryString["SelectorType"]))
            {
                _selectorType = Request.QueryString["SelectorType"];
            }
            if ("content" == _selectorType)
            {
                //Taxonomy Tab on Content Selector
                right.Visible = true;
                ItemPathDiv.Visible = false;
                TaxonomyContentList.Visible = true;
            }
            else
            {
                // Taxonomy Selector
                right.Visible = false;
                ItemPathDiv.Visible = true;
                left.Attributes.Add("style", "width:98%; height:90%");
                TaxonomyContentList.Visible = false;

                if (!string.IsNullOrEmpty(Request.QueryString["idValue"]))
                {
                    _defaultTax = Request.QueryString["idValue"];
                }
                this.lblCurrentPath.Text = m_refMsg.GetMessage("lbl current path");
            }

			// Register JS
            JS.RegisterJS(this, JS.ManagedScript.EktronJS);

		}
        protected void OnEktronUIPageChanged(Object sender, GridViewEktronUIThemePageChangedEventArgs e)
        {
            GridPagingInfo = e.PagingInfo;
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if ("content" == _selectorType)
            {
                if (Page.IsPostBack && !string.IsNullOrEmpty(TaxTree.CurrentNodeId))
                {
                    // need to reset paging info
                    GridPagingInfo = new PagingInfo();
                    GridPagingInfo.RecordsPerPage = _recordsPerPage;
                    GetTaxonomyContent(Int64.Parse(TaxTree.CurrentNodeId));
                }
                else if (!string.IsNullOrEmpty(GridFolderId))
                {
                    // paging to a different page 
                    GetTaxonomyContent(Int64.Parse(GridFolderId));
                }
            }
        }
        protected void TaxTree_OnPreSerializeData(Object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !string.IsNullOrEmpty(_defaultTax))
            {
                foreach (var node in TaxTree.ItemsFlatList)
                {
                    if (_defaultTax == node.Id)
                    {
                        node.Selected = true;
                        ExpandAncestors(node, TaxTree.ItemsFlatList);
                        tbTaxonomyPath.Text = node.IdPath.Substring(1, node.IdPath.Length -2).Replace("/", "^");
                        if ("taxonomy" == _selectorType)
                        {
                            taxPath.InnerHtml = "<i>" + node.TaxonomyPath.Substring(1).Replace("\\", " > ") + "</i>";
                        }
                    }
                }
            }
        }
        private void ExpandAncestors<T>(T node, IList<T> flatList, bool select = false) where T : class, ITreeNode, new()
        {
            var parentNode = GetParent<T>(node, flatList);
            if (parentNode != null)
            {
                parentNode.Expanded = true;
                parentNode.Selected = parentNode.Selected || select;
                ExpandAncestors<T>(parentNode, flatList);
            }
        }

        private T GetParent<T>(T node, IList<T> flatList) where T : class, ITreeNode, new()
        {
            if (node != null && !string.IsNullOrEmpty(node.ParentId) && node.ParentId != node.Id && flatList != null && node.ParentId != "0")
            {
                var parentId = node.ParentId;
                var parentNode = (
                    from f in flatList
                    where f.Id == parentId
                    select f
                    ).First();
                return parentNode;
            }
            return null;
        }

        protected string GetContentIcon(object ContentTypeID, object ContentSubType, object ImageUrl)
        {
            int contentTypeID = int.Parse(ContentTypeID.ToString());
            int contentSubType = System.Convert.ToInt16(ContentSubType);
            ContentAPI capi = new ContentAPI();
            Microsoft.VisualBasic.Collection item = new Microsoft.VisualBasic.Collection();
            item.Add(contentSubType, "ContentSubType", null, null);
            if (ImageUrl != null)
            {
                item.Add(ImageUrl.ToString(), "ImageUrl", null, null);
            }
            return Ektron.Cms.Common.EkFunctions.getContentTypeIconAspx(contentTypeID, item, capi.ApplicationPath);
        }

        private void GetTaxonomyContent(long objectID)
        {
            GridFolderId = objectID.ToString();
            List<Ektron.Cms.ContentData> items = new List<Ektron.Cms.ContentData>();
            if (objectID > -1)
            {
                TaxonomyItemManager manager = new TaxonomyItemManager();
                TaxonomyItemCriteria criteria = new TaxonomyItemCriteria();

                criteria.AddFilter(TaxonomyItemProperty.TaxonomyId, CriteriaFilterOperator.EqualTo, objectID);
                criteria.AddFilter(TaxonomyItemProperty.LanguageId, CriteriaFilterOperator.EqualTo, CommonApi.Current.ContentLanguage);
                criteria.PagingInfo = GridPagingInfo;
                var contentList = manager.GetList(criteria);

                if (contentList != null && contentList.Count > 0)
                {
                    List<ContentData> subList = new List<ContentData>();
                    foreach (TaxonomyItemData t in contentList)
                    {
                        ContentData item = new ContentData();
                        if (t.ContentSubType != EkEnumeration.CMSContentSubtype.PageBuilderData && t.ContentSubType != EkEnumeration.CMSContentSubtype.PageBuilderMasterData)
                        {
                            item.Id = t.ItemId;
                            item.ContType = int.Parse(t.ContentType); ;
                            item.SubType = t.ContentSubType;
                            item.Title = t.Title;
                            if (t.AssetData != null)
                            {
                                item.AssetData = t.AssetData;
                            }
                            subList.Add(item);
                        }
                    }

                    if (subList.Count > 0)
                    {
                        TaxonomyContentList.EktronUIPagingInfo = GridPagingInfo;
                        TaxonomyContentList.DataSource = subList;
                        TaxonomyContentList.DataBind();
                    }
                }
            }
        }

        protected string GetContentTypeString(object contentType)
        {
            return getContentTypeString(int.Parse(contentType.ToString()));
        }
        private string getContentTypeString(int contentType)
        {
            string sContentType;
            switch (contentType)
            {
                case 2:
                    sContentType = "content:htmlform";
                    break;
                case 101:
                    sContentType = "content:mso";
                    break;
                case 102:
                case 106:
                case 103:
                case 8:
                    sContentType = "content:asset";
                    break;
                case 104:
                    sContentType = "content:multimedia";
                    break;
                case 3333: //CatalogEntry 
                    sContentType = "content:product";
                    break;
                case 1:
                    sContentType = "content:htmlcontent";
                    break;
                case 7: //Ektron.Cms.Common.EkEnumeration.CMSContentType.LibraryItem
                    sContentType = "LibraryItem"; // not supported
                    break;
                case 3:  //Archive_Content
                case 4:  //Archive_Forms
                case 9:  //Archive_Assets    
                case 12: //Archive_Media
                case 99: //NonLibraryContent    
                case 111: //DiscussionTopic  
                default:
                    sContentType = "not supported types";
                    break;
            }
            return sContentType;
        }
	}
}
