<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCollectionTree.ascx.cs" Inherits="Ektron.ContentDesigner.Dialogs.CollectionTree" %>
<ektronUI:CssBlock ID="folderTreeTab" runat="server">
    <CssTemplate>
    .left, .right {   
        overflow:auto;
        height:95%;
    }  
    .left  { float:left; width: 25%; border:1px solid black; } 
    .right { float:right; width:73%; /*border:1px solid black;*/}  
    div.collectionTree li.ektron-ui-tree-leaf span.ui-icon
    {
        background-image: url(../../images/UI/icons/collection.png);
        display:inline-block;
    }
    .ContentType {display:none;}
    div.collectionTree li.ektron-ui-tree-branch div.hitLocation div.selectionLocation { white-space:nowrap; }
    </CssTemplate>
</ektronUI:CssBlock>
<div id="left" runat="server" class="left">
    <ektronUI:Tree ID="CollectionsTree" runat="server" SelectionMode="SingleForAll" OnPreSerializeData="CollectionsTree_OnPreSerializeData" ShowRoot="false"  
        AutoPostBackOnSelectionChanged="true" CssClass="collectionTree" >
    </ektronUI:Tree>
</div>
<div id="right" runat="server" class="right">
    <ektronUI:GridView ID="CollectionContentList" runat="server" AutoGenerateColumns="false" EnableEktronUITheme="true" AllowPaging="true" Caption="Collection Items" 
         OnEktronUIPageChanged="OnEktronUIPageChanged"  >
        <Columns>
            <asp:TemplateField ItemStyle-Width="10%">
                <ItemTemplate>
                    <asp:literal ID="ItemIcon" runat="server" Text='<%#GetContentIcon(Eval("ContType"), Eval("SubType"), DataBinder.Eval(Container, "DataItem.AssetData.Icon")) %>'></asp:literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="ContentTitle" runat="server" Text='<%# Eval("Title") %>' CssClass="ContentTitle item"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="18%">
                <ItemTemplate>
                    <asp:Label ID="ContentId" runat="server" Text='<%# Eval("Id") %>' CssClass="ContentId item"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </ektronUI:GridView>
</div>
