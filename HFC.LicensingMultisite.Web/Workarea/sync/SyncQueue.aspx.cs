﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.Context;
using Ektron.Cms;
using Ektron.Sync.Entities;
using Ektron.Cms.Sync.Client;
using Ektron.Cms.Framework.UI;
using System.Configuration;
using Ektron.Cms.Sync.Presenters;

public partial class Workarea_sync_SyncQueue : System.Web.UI.Page, ISyncQueueListView
{
    private EktronServiceProxy EkSvcProxy = new EktronServiceProxy();
    private CommonApi ComApi = new CommonApi();
    private SyncQueuePresenter _presenter;

    /// <summary>
    /// Gets or sets the collection of queue jobs to be displayed
    /// on this page.
    /// </summary>
    public List<QueueJobData> QueueJobs { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (this.HasViewEditPermission())
        {
            try
            {
                this.initWorkareaUI();
                _presenter = new SyncQueuePresenter(this);
            }
            catch (Exception exc)
            {
                Utilities.ShowError(exc.Message);
            }
        }
        else
        {
            Response.Redirect(ContentAPI.Current.ApplicationPath + "Login.aspx", true);
        }

        uxSyncQueueGridView.RowCommand += new GridViewCommandEventHandler(uxSyncQueueGridView_RowCommand);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (this.IsQueueEnabled())
            {
                this.uxSyncQueueGridView.Visible = true;
                this.uxSyncQueueDisabled.Visible = false;
                _presenter.InitializeView();
            }
            else
            {
                this.uxSyncQueueDisabled.Visible = true;
                this.uxSyncQueueGridView.Visible = false;
            }
        }
    }

    public void Bind()
    {
        if (QueueJobs != null && QueueJobs.Count > 0)
        {
            uxSyncQueueGridView.DataSource = QueueJobs;
            uxSyncQueueGridView.EktronUIPagingInfo = new PagingInfo();
            uxSyncQueueGridView.EktronUIPagingInfo.TotalPages = 1;
            uxSyncQueueGridView.EktronUIPagingInfo.CurrentPage = 1;
            uxSyncQueueGridView.EktronUIPagingInfo.TotalRecords = QueueJobs.Count;
        }
        else
        {
            uxSyncQueueGridView.DataSource = null;
            ////showMessage = true;
            uxNoJobsMessage.Visible = true;
            uxNoJobsMessage.Text = GetLocalResourceObject("NoData").ToString();
        }

        uxSyncQueueGridView.DataBind();
    }

    protected void uxSyncQueueGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.StartsWith("reorder"))
        {
            _presenter.InitializeView();
            Guid jobId = new Guid(e.CommandArgument.ToString());
            QueueJobData itemMoved = QueueJobs.Find(x => x.JobId == jobId);
            int itemIndex = QueueJobs.IndexOf(itemMoved);

            // Fetching the jobs again so that the the current job state is more accurate
            QueueJobData currentJob = _presenter.GetQueueJobs().Where(x => x.JobId == jobId).SingleOrDefault();
            if (e.CommandName == "reorderUP")
            {
                if (currentJob != null && (currentJob.State != JobState.InProcess && currentJob.State != JobState.Completed))
                {
                    this.uxCannotReorderMessage.Visible = false;
                    _presenter.ChangePositionOfJob(jobId, itemMoved.Position - 1);
                }
                else
                {
                    this.uxCannotReorderMessage.Visible = true;
                }
            }
            if (e.CommandName == "reorderDOWN")
            {
                if (currentJob != null && (currentJob.State != JobState.InProcess && currentJob.State != JobState.Completed))
                {
                    this.uxCannotReorderMessage.Visible = false;
                    _presenter.ChangePositionOfJob(jobId, itemMoved.Position + 1);
                }
                else
                {
                    this.uxCannotReorderMessage.Visible = true;
                }
            }

            _presenter.InitializeView();
        }
    }

    public void DeleteJob_Click(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "delJob")
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            Guid jobId = new Guid(commandArgs[0]);
            ////JobState state = (JobState)Enum.Parse(typeof(JobState), commandArgs[1]);

            // Fetching the jobs again so that the the current job state is more accurate
            QueueJobData currentJob = _presenter.GetQueueJobs().Where(x => x.JobId == jobId).SingleOrDefault();

            if (currentJob != null && (currentJob.State != JobState.InProcess && currentJob.State != JobState.Completed))
            {
                this.uxCannotDeleteMessage.Visible = false;
                _presenter.RemoveJobFromQueue(jobId, currentJob.State);
            }
            else
            {
                this.uxCannotDeleteMessage.Visible = true;
            }

            _presenter.InitializeView();
        }
    }

    private void DeleteAllJobs()
    {

    }

    private string GetConnectionString()
    {
        return ConfigurationManager.ConnectionStrings["Ektron.DbConnection"].ConnectionString;
    }

    private bool IsQueueEnabled()
    {
        bool isQueueEnabled = false;
        if (SyncQueuePresenter.IsQueueEnabledAtEWSLevel() && SyncQueuePresenter.IsQueueEnabledAtSiteLevel())
        {
            isQueueEnabled = true;
        }

        return isQueueEnabled;
    }

    private bool HasViewEditPermission()
    {
        // IF       CMS USER
        // AND      LOGGED IN
        // AND      IS ADMIN    OR  IS SYNC ADMIN
        return
            (
                !Convert.ToBoolean(ContentAPI.Current.RequestInformationRef.IsMembershipUser) &&
                ContentAPI.Current.RequestInformationRef.UserId != 0 && (ContentAPI.Current.IsAdmin() ||
                ContentAPI.Current.IsARoleMember(Convert.ToInt64(Ektron.Cms.Common.EkEnumeration.CmsRoleIds.SyncAdmin), ContentAPI.Current.UserId, false))
            );
    }

    private void initWorkareaUI()
    {
        this.RegisterResources();
        StyleHelper styleHelper = new StyleHelper();
        aspHelpButton.Text = styleHelper.GetHelpButton("sync_settings", string.Empty);
        ////uxMessage.Visible = false;

        if (!Page.IsPostBack)
        {

        }
    }

    protected void Delete_Click(object sender, EventArgs e)
    {
        try
        {
            // Delete code here
        }
        catch (Exception exc)
        {

            Utilities.ShowError(exc.Message);
        }


    }

    #region JavaScript/CSS

    /// <summary>
    /// Register Javascripts and CSS packages
    /// </summary>
    private void RegisterResources()
    {
        ////Ektron.Cms.Framework.UI.Packages.EktronCoreJS.Register(this);
        Ektron.Cms.Framework.UI.Packages.Ektron.Namespace.Register(this);
        Packages.Ektron.CssFrameworkBase.Register(this);
        Ektron.Cms.Framework.UI.Packages.Ektron.Workarea.Core.Register(this);
        Ektron.Cms.Framework.UI.JavaScript.Register(this, CmsContextService.Current.WorkareaPath + "/java/jfunct.js");
        Ektron.Cms.API.Css.RegisterCss(this, Ektron.Cms.API.Css.ManagedStyleSheet.EktronFixedPositionToolbarCss);
        Package MenuCss = new Package()
        {
            Components = new List<Component>()
                {
                    Css.Create(ResolveUrl("~/workarea/wamenu/css/com.ektron.ui.menu.css")),
                }
        };

        MenuCss.Register(this);
    }

    #endregion

    /// <summary>
    /// Displays the specified error message to the user.
    /// </summary>
    /// <param name="message"></param>
    public void DisplayError(string message)
    {
        Response.Write("**** " + message + " ****");
    }
}