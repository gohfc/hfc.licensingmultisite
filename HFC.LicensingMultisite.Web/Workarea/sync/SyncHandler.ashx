﻿<%@ WebHandler Language="C#" Class="SyncHandler" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
using System.Linq;

using Ektron.Cms;
using Ektron.Cms.Common;
using Ektron.Cms.Sync;
using Ektron.Cms.Sync.Client;
using Ektron.Cms.Sync.Presenters;
using Ektron.Cms.Sync.Web;
using Ektron.Cms.Sync.Web.Parameters;
using Ektron.Cms.Sync.Web.Responses;
using Ektron.FileSync.Common;
using Ektron.Cms.Sync.Presenters;
using Ektron.Cms.Framework.Organization;
using Ektron.Sync.Entities;

public class SyncHandler : IHttpHandler
{

    private const string ResponseContentType = "text/plain";

    private readonly SyncHandlerController _controller;
    private readonly JavaScriptSerializer _serializer;
    private readonly SiteAPI _siteApi;
    private readonly CommonApi _commonApi;

    /// <summary>
    /// Constructor
    /// </summary>
    public SyncHandler()
    {
        _controller = new SyncHandlerController();
        _serializer = new JavaScriptSerializer();
        _siteApi = new SiteAPI();
        _commonApi = new CommonApi();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest(HttpContext context)
    {
        ResponseBase response = null;
        // Prevent the request from timing out during
        // extended synchronization activities.
        context.Server.ScriptTimeout = 6000;
        try
        {
            SyncHandlerParameters parameters = new SyncHandlerParameters(context.Request);
            switch (parameters.Action)
            {
                case SyncHandlerAction.DeleteProfile:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                    {
                        response = DeleteProfile(parameters);
                    }
                    break;
                case SyncHandlerAction.DeleteRelationship:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                    {
                        response = DeleteRelationship(parameters);
                    }
                    break;
                case SyncHandlerAction.Pause:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                    {
                        response = PauseProfile(parameters);
                    }
                    break;
                case SyncHandlerAction.Resume:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                    {
                        response = ResumeProfile(parameters);
                    }
                    break;
                case SyncHandlerAction.Synchronize:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = Synchronize(parameters, false);
                    }
                    break;
                case SyncHandlerAction.ContentFolderSync:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = Synchronize(new ContentFolderSyncParameters(parameters));
                    }
                    break;
                case SyncHandlerAction.Certificates:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = GetCertificates();
                    }
                    break;
                case SyncHandlerAction.Sites:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = GetSites(new GetSitesParameters(parameters));
                    }
                    break;
                case SyncHandlerAction.CreateRelationship:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                    {
                        response = CreateRelationship(new CreateRelationshipParameters(parameters));
                    }
                    break;
                case SyncHandlerAction.ResolveConflicts:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin))
                    {
                        response = ResolveConflicts();
                    }
                    break;
                case SyncHandlerAction.GetProfiles:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = GetProfiles(new GetProfilesParameters(parameters));
                    }
                    break;
                case SyncHandlerAction.IsSyncInProgress:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = IsSyncInProgress();
                    }
                    break;
                case SyncHandlerAction.CanSyncBeTriggered:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = CanSyncBeTriggered();
                    }
                    break;
                case SyncHandlerAction.GetFiles:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = GetFiles(new GetFilesParameters(parameters));
                    }
                    break;
                case SyncHandlerAction.SyncFiles:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = Synchronize(new SyncFilesParameters(parameters));
                    }
                    break;
                case SyncHandlerAction.SynchronizePackage:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = SynchronizePackage(parameters);
                    }
                    break;
                case SyncHandlerAction.ForceSynchronize:
                    if (_siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncAdmin) ||
                        _siteApi.IsARoleMember(EkEnumeration.CmsRoleIds.SyncUser))
                    {
                        response = Synchronize(parameters, true);
                    }
                    break;
            }


        }
        catch (Exception e)
        {
            EkException.LogException(e);
        }
        finally
        {
            context.Response.ContentType = ResponseContentType;
            context.Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.Cache.SetNoStore();
            if (response != null)
            {
                context.Response.Write(_serializer.Serialize(response));
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="parameters"></param>
    /// <returns></returns>
    private DeleteProfileResponse DeleteProfile(SyncHandlerParameters parameters)
    {
        DeleteProfileResponse response = new DeleteProfileResponse();

        if (parameters.IsValid)
        {
            ContentAPI conApi = new ContentAPI();
            EktronServiceProxy EkSvcProxy = new EktronServiceProxy();
            string sitePath = conApi.RequestInformationRef.PhysicalSitePath.TrimEnd(new char[] { '\\' });
            string conString = conApi.RequestInformationRef.ConnectionString;
            List<Job> jobsList = EkSvcProxy.GetList(conString, sitePath);
            List<Job> jobsWithCurrentProfile = null;
            bool safeToDeleteProfile = true;
            if (jobsList != null && jobsList.Count > 0)
            {
                jobsWithCurrentProfile = jobsList.Where(x => x.ProfileId == parameters.Id).ToList();
                foreach (Job job in jobsWithCurrentProfile)
                {
                    if (job.State != JobState.InProcess)
                    {
                        EkSvcProxy.RemoveJobFromQueue(job.Id, job.State, conString, sitePath);
                    }
                    else
                    {
                        safeToDeleteProfile = false;
                    }
                }
            }

            if (safeToDeleteProfile)
            {
                SyncHandlerController.ResultCode result;
                _controller.DeleteProfile(parameters.Id, out result);

                switch (result)
                {
                    case SyncHandlerController.ResultCode.Success:
                        response.Success = true;
                        break;
                    case SyncHandlerController.ResultCode.DatabaseError:
                        response.Success = false;
                        response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync database error"));
                        break;
                    case SyncHandlerController.ResultCode.UnknownError:
                        response.Success = false;
                        response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync unexpected error"));
                        break;
                }
            }
            else
            {
                response.Success = false;
                response.Messages.Add("Job is in process");
            }
        }
        else
        {
            response.Success = false;
            response.Messages.Add("Missing parameter: id");
        }

        return response;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="parameters"></param>
    /// <returns></returns>
    private DeleteRelationshipResponse DeleteRelationship(SyncHandlerParameters parameters)
    {
        DeleteRelationshipResponse response = new DeleteRelationshipResponse();

        if (parameters.IsValid)
        {
            SyncHandlerController.ResultCode result;
            _controller.DeleteRelationship(parameters.Id, out result);

            switch (result)
            {
                case SyncHandlerController.ResultCode.Success:
                    response.Success = true;
                    break;
                case SyncHandlerController.ResultCode.DatabaseError:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync database error"));
                    break;
                case SyncHandlerController.ResultCode.UnknownError:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync unexpected error"));
                    break;
            }
        }
        else
        {
            response.Success = false;
            response.Messages.Add("Missing parameter: id");
        }

        return response;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="parameters"></param>
    /// <returns></returns>
    private ProfileStatusResponse PauseProfile(SyncHandlerParameters parameters)
    {
        ProfileStatusResponse response = new ProfileStatusResponse();

        if (parameters.IsValid)
        {
            SyncHandlerController.ResultCode result;
            DateTime nextRunTime = _controller.PauseSchedule(parameters.Id, out result);

            response.Id = parameters.Id;
            response.NextRunTime = nextRunTime.ToString(System.Globalization.CultureInfo.CurrentCulture);

            switch (result)
            {
                case SyncHandlerController.ResultCode.Success:
                    response.Success = true;
                    break;
                case SyncHandlerController.ResultCode.DatabaseError:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync database error"));
                    break;
                case SyncHandlerController.ResultCode.UnknownError:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync unexpected error"));
                    break;
            }
        }
        else
        {
            response.Success = false;
            response.Messages.Add("Missing parameter: id");
        }

        return response;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="parameters"></param>
    /// <returns></returns>
    private ProfileStatusResponse ResumeProfile(SyncHandlerParameters parameters)
    {
        ProfileStatusResponse response = new ProfileStatusResponse();

        if (parameters.IsValid)
        {
            SyncHandlerController.ResultCode result;
            DateTime nextRunTime = _controller.ResumeSchedule(parameters.Id, out result);

            response.Id = parameters.Id;
            response.NextRunTime = nextRunTime.ToString(System.Globalization.CultureInfo.CurrentCulture);

            switch (result)
            {
                case SyncHandlerController.ResultCode.Success:
                    response.Success = true;
                    break;
                case SyncHandlerController.ResultCode.DatabaseError:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync database error"));
                    break;
                case SyncHandlerController.ResultCode.UnknownError:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync unexpected error"));
                    break;
            }
        }
        else
        {
            response.Success = false;
            response.Messages.Add("Missing parameter: id");
        }

        return response;
    }

    private SynchronizeResponse SynchronizePackage(SyncHandlerParameters parameters)
    {
        SynchronizeResponse response = new SynchronizeResponse();
        SyncHandlerController.SynchronizeResult result;
        Relationship relationship = Relationship.GetRelationship(parameters.Id);
        if (relationship.Type != ProfileType.Azure || relationship.Type != ProfileType.Amazon)
        {
            Profile profile = _controller.SynchronizePackageProfile(parameters.Id, out result);

            switch (result)
            {
                case SyncHandlerController.SynchronizeResult.Success:
                    response.Success = true;
                    break;
                case SyncHandlerController.SynchronizeResult.DatabaseError:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync database error"));
                    break;
                case SyncHandlerController.SynchronizeResult.ProfileNotFound:
                    response.Success = false;
                    response.Messages.Add("Profile not found.");
                    break;
                case SyncHandlerController.SynchronizeResult.RelationshipNotInitialized:
                    response.Success = false;
                    response.Messages.Add("Relationship is not initialized.");
                    break;
                case SyncHandlerController.SynchronizeResult.SynchronizationInProgress:
                    response.Success = false;
                    response.ProfileId = profile.Id;
                    response.ProfileName = profile.Name;
                    response.Messages.Add("Synchronization is already in progress: " + profile.Id.ToString());
                    break;
                case SyncHandlerController.SynchronizeResult.UnknownError:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync unexpected error"));
                    break;
            }
        }

        return response;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="parameters"></param>
    /// <returns></returns>
    private SynchronizeResponse Synchronize(SyncHandlerParameters parameters, bool forceInit)
    {
        SynchronizeResponse response = new SynchronizeResponse();
        EktronServiceProxy proxy = new EktronServiceProxy();
        SyncHandlerController.SynchronizeResult result;

        Relationship relationship = Relationship.GetRelationship(parameters.Id);
        if (relationship.DefaultProfile.SynchronizePackage)
        {
            return this.SynchronizePackage(parameters);
        }

        if ((relationship.Type == ProfileType.Azure || relationship.Type == ProfileType.Amazon) && relationship.ParentId == 0)
        {
            //CloudParamWrapper defined in app_code
            SyncProfileActions cloudSyncParams = (new CloudParamWrapper()).GetParams(relationship, parameters.Id);
            cloudSyncParams.ExternalArgs = relationship.ExternalArgs;
            proxy.CloudDeployment(cloudSyncParams, Ektron.Cloud.Common.CloudCommandType.CreateDeployment, relationship.Type);
        }
        else if (relationship.Type == ProfileType.Azure && relationship.ParentId > 0)
        {
            SyncProfileActions cloudSyncParams = (new CloudParamWrapper()).GetParams(relationship, parameters.Id);
            cloudSyncParams.ExternalArgs = relationship.ExternalArgs;
            proxy.CloudSyncronize(cloudSyncParams);
        }
        else
        {
            Profile profile = _controller.Synchronize(parameters.Id, out result, forceInit);
            switch (result)
            {
                case SyncHandlerController.SynchronizeResult.Success:
                    response.Success = true;
                    break;
                case SyncHandlerController.SynchronizeResult.DatabaseError:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync database error"));
                    break;
                case SyncHandlerController.SynchronizeResult.ProfileNotFound:
                    response.Success = false;
                    response.Messages.Add("Profile not found.");
                    break;
                case SyncHandlerController.SynchronizeResult.RelationshipNotInitialized:
                    response.Success = false;
                    response.Messages.Add("Relationship is not initialized.");
                    break;
                case SyncHandlerController.SynchronizeResult.SynchronizationInProgress:
                    response.Success = false;
                    response.ProfileId = profile.Id;
                    response.ProfileName = profile.Name;
                    response.Messages.Add("Synchronization is already in progress: " + profile.Id.ToString());
                    break;
                case SyncHandlerController.SynchronizeResult.CertificateNotFound:
                    response.Success = false;
                    response.Messages.Add("The remote server certificates could not be found");
                    break;
                case SyncHandlerController.SynchronizeResult.UnknownError:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync unexpected error"));
                    break;
            }
        }

        return response;
    }
    private SynchronizeResponse Synchronize(SyncFilesParameters parameters)
    {
        SynchronizeResponse response = new SynchronizeResponse();

        SyncHandlerController.SynchronizeResult result;
        Profile profile = _controller.Synchronize(parameters.Id, parameters.Files, out result);

        switch (result)
        {
            case SyncHandlerController.SynchronizeResult.Success:
                response.Success = true;
                break;
            case SyncHandlerController.SynchronizeResult.DatabaseError:
                response.Success = false;
                response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync database error"));
                break;
            case SyncHandlerController.SynchronizeResult.ProfileNotFound:
                response.Success = false;
                response.Messages.Add("Profile not found.");
                break;
            case SyncHandlerController.SynchronizeResult.RelationshipNotInitialized:
                response.Success = false;
                response.Messages.Add("Relationship is not initialized.");
                break;
            case SyncHandlerController.SynchronizeResult.SynchronizationInProgress:
                response.Success = false;
                response.ProfileId = profile.Id;
                response.ProfileName = profile.Name;
                response.Messages.Add("Synchronization is already in progress: " + profile.Id.ToString());
                break;
            case SyncHandlerController.SynchronizeResult.UnknownError:
                response.Success = false;
                response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync unexpected error"));
                break;
        }

        return response;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="parameters"></param>
    /// <returns></returns>
    private SyncResponse Synchronize(ContentFolderSyncParameters parameters)
    {
        SynchronizeResponse response = new SynchronizeResponse();

        SyncHandlerController.SynchronizeResult result;

        Profile profile = null;
        if (parameters.IsContentSync)
        {
            if (!String.IsNullOrEmpty(parameters.AssetId) && !String.IsNullOrEmpty(parameters.AssetVersion))
            {
                profile = _controller.SynchronizeAssetContent(parameters.Id, parameters.ContentId, parameters.LanguageId,
                            parameters.FolderId, parameters.AssetId, parameters.AssetVersion, out result);
            }
            else
            {
                profile = _controller.SynchronizeContent(parameters.Id, parameters.ContentId, parameters.LanguageId, parameters.FolderId, out result);
            }
        }
        else
        {
            profile = _controller.Synchronize(
                parameters.Id,
                parameters.FolderId,
                out result);
        }

        switch (result)
        {
            case SyncHandlerController.SynchronizeResult.Success:
                response.Success = true;
                break;
            case SyncHandlerController.SynchronizeResult.DatabaseError:
                response.Success = false;
                response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync database error"));
                break;
            case SyncHandlerController.SynchronizeResult.ProfileNotFound:
                response.Success = false;
                response.Messages.Add("Profile not found.");
                break;
            case SyncHandlerController.SynchronizeResult.RelationshipNotInitialized:
                response.Success = false;
                response.Messages.Add("Relationship is not initialized.");
                break;
            case SyncHandlerController.SynchronizeResult.SynchronizationInProgress:
                response.Success = false;
                response.ProfileId = profile.Id;

                if (profile.IsDefault)
                {
                    ConnectionInfo connectionInfo = new ConnectionInfo(profile.Name);
                    response.ProfileName = string.Format(
                        "{0}/{1}",
                        connectionInfo.ServerName,
                        connectionInfo.DatabaseName);
                }
                else
                {
                    response.ProfileName = profile.Name;
                }
                break;
            case SyncHandlerController.SynchronizeResult.UnknownError:
                response.Success = false;
                response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync unexpected error"));
                break;
        }

        return response;
    }

    private GetCertificatesResponse GetCertificates()
    {
        GetCertificatesResponse response = new GetCertificatesResponse();

        SyncHandlerController.ResultCode result;
        response.Certificates = _controller.GetCertificates(out result);

        switch (result)
        {
            case SyncHandlerController.ResultCode.Success:
                response.Success = true;
                break;
            case SyncHandlerController.ResultCode.UnknownError:
                response.Success = false;
                response.Messages.Add("Service error");
                break;
        }

        return response;
    }

    private GetSitesResponse GetSites(GetSitesParameters parameters)
    {
        GetSitesResponse response = new GetSitesResponse();
        response.Sites = new List<GetSitesResponse.Site>();

        if (parameters.IsValid)
        {
            SyncHandlerController.ResultCode result;
            List<SiteConfiguration> sites = _controller.GetSites(
                parameters.Server,
                parameters.Certificate,
                parameters.IncludeLocal,
                out result);

            if (sites != null)
            {
                Dictionary<string, GetSitesResponse.Site> sitesTable =
                    new Dictionary<string, GetSitesResponse.Site>();

                foreach (SiteConfiguration siteConfiguration in sites)
                {
                    //Handling scenario for Azure, where the "Address" will be the windows service host name.
                    if (siteConfiguration.Address.ToLower() == parameters.Certificate.ToLower() ||
                        (string.IsNullOrEmpty(siteConfiguration.Address) == false &&
                         siteConfiguration.Address.ToLower() == System.Net.Dns.GetHostName().ToLower()))
                    {
                        string connectionString = siteConfiguration.ConnectionString;
                        if (!sitesTable.ContainsKey(connectionString))
                        {
                            GetSitesResponse.Site site = new GetSitesResponse.Site();
                            site.ServerName = siteConfiguration.Address;
                            site.DatabaseName = siteConfiguration.Connection.DatabaseName;
                            site.DatabaseServerName = siteConfiguration.Connection.ServerName;
                            site.IntegratedSecurity = siteConfiguration.Connection.IntegratedSecurity;
                            site.SitePaths = new List<string>();
                            site.SitePaths.Add(siteConfiguration.SitePath);

                            // Add the site to the reference table for
                            // future lookups.

                            sitesTable.Add(connectionString, site);

                            // Add the site to the response.

                            response.Sites.Add(site);
                        }
                        else
                        {
                            sitesTable[connectionString].SitePaths.Add(siteConfiguration.SitePath);
                        }
                    }
                }
            }

            switch (result)
            {
                case SyncHandlerController.ResultCode.Success:
                    response.Success = true;
                    break;
                case SyncHandlerController.ResultCode.UnknownError:
                    response.Success = false;
                    response.Messages.Add("Service error");
                    break;
            }
        }
        else
        {
            response.Success = false;
            response.Messages.Add("Invalid parameters");
        }

        return response;
    }

    private CreateRelationshipResponse CreateRelationship(CreateRelationshipParameters parameters)
    {
        CreateRelationshipResponse response = new CreateRelationshipResponse();

        if (parameters.IsValid)
        {
            SyncHandlerController.CreateRelationshipResult result;

            Relationship relationship = _controller.CreateRelationship(
                parameters.LocalDatabaseName,
                parameters.LocalServerName,
                parameters.LocalSitePath,
                parameters.MultiSiteFolderId,
                parameters.RemoteDatabaseName,
                parameters.RemoteServerName,
                parameters.RemoteDatabaseServer,
                parameters.RemoteSitePath,
                parameters.Certificate,
                parameters.Direction,
                out result);

            if (relationship != null)
            {
                response.RelationshipId = relationship.Id;
            }

            switch (result)
            {
                case SyncHandlerController.CreateRelationshipResult.Success:
                    response.Success = true;
                    break;
                case SyncHandlerController.CreateRelationshipResult.RelationshipAlreadyExists:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("js sync relationship exists"));
                    break;
                case SyncHandlerController.CreateRelationshipResult.RelationshipResurrected:
                    response.Success = true;
                    response.Resurrected = true;
                    break;
                case SyncHandlerController.CreateRelationshipResult.DatabaseError:
                    response.Success = false;
                    response.Messages.Add("Database error.");
                    break;
                case SyncHandlerController.CreateRelationshipResult.UnknownError:
                    response.Success = false;
                    response.Messages.Add("Unknown error.");
                    break;
                case SyncHandlerController.CreateRelationshipResult.CommunicationFailure:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync service error"));
                    break;
                case SyncHandlerController.CreateRelationshipResult.RelationshipNotInitialized:
                    response.Success = false;
                    response.Messages.Add(_siteApi.EkMsgRef.GetMessage("sync unintialized all folders"));
                    break;
            }
        }
        else
        {
            response.Success = false;
            response.Messages.Add("Invalid parameters.");
        }

        return response;
    }

    private ResolveConflictsResponse ResolveConflicts()
    {
        ResolveConflictsResponse response = new ResolveConflictsResponse();
        SyncHandlerController.ResultCode result;

        long syncConflictResolutionMode = -1;
        if (System.Configuration.ConfigurationManager.AppSettings["ek_syncConflictResolutionMode"] != null)
        {
            long.TryParse(System.Configuration.ConfigurationManager.AppSettings["ek_syncConflictResolutionMode"], out syncConflictResolutionMode);
        }

        _controller.ResolveSynchronizationConflicts(out result, syncConflictResolutionMode);

        switch (result)
        {
            case SyncHandlerController.ResultCode.Success:
                response.Success = true;
                break;
            case SyncHandlerController.ResultCode.DatabaseError:
                response.Messages.Add(_siteApi.EkMsgRef.GetMessage("resolve database error"));
                response.Success = false;
                break;
            case SyncHandlerController.ResultCode.UnknownError:
                response.Messages.Add(_siteApi.EkMsgRef.GetMessage("resolve unexpected error"));
                response.Success = false;
                break;
        }

        return response;
    }

    private GetProfilesUIResponse GetProfiles(GetProfilesParameters parameters)
    {
        GetProfilesUIResponse response = new GetProfilesUIResponse();

        SyncHandlerController.ResultCode result = SyncHandlerController.ResultCode.None;

        // Retrieve profiles meeting the specified input criteria.

        Ektron.Cms.Sync.Client.GetProfilesResponse getProfilesResponse = _controller.GetProfiles(
            parameters.IsContentSync,
            parameters.IncludeMultisiteProfiles,
            parameters.ContentId,
            parameters.Language,
            parameters.FolderId, parameters.isFilteredSync,
            out result);

        switch (result)
        {
            case SyncHandlerController.ResultCode.DatabaseError:
                response.Success = false;
                response.Messages.Add("Database error.");
                break;
            case SyncHandlerController.ResultCode.UnknownError:
                response.Success = false;
                response.Messages.Add("Unknown error.");
                break;
            default:
                response.Success = true;
                break;
        }



        if (getProfilesResponse.UnVerifiedProfiles != null)
        {
            Dictionary<long, GetProfilesUIResponse.Relationship> relationships =
                new Dictionary<long, GetProfilesUIResponse.Relationship>();

            List<long> missingFolders = new List<long>();

            foreach (Profile profile in getProfilesResponse.UnVerifiedProfiles)
            {
                GetProfilesUIResponse.Relationship relationshipResponse = null;

                // Determine if this profile belongs to a relationship
                // for which a response has already been created.

                if (!relationships.ContainsKey(profile.Parent.Id))
                {
                    // If the relationship response does not yet exist for this
                    // profile's parent, create one.

                    relationshipResponse = new GetProfilesUIResponse.Relationship();
                    relationshipResponse.RelationshipId = profile.Parent.Id;
                    relationshipResponse.DatabaseName = profile.Parent.RemoteSite.Connection.DatabaseName;
                    relationshipResponse.LocalSitePath = profile.Parent.LocalSite.SitePath;
                    relationshipResponse.RemoteSitePath = profile.Parent.RemoteSite.SitePath;
                    relationshipResponse.ServerName = profile.Parent.RemoteSite.Connection.ServerName;
                    relationshipResponse.Direction = profile.Parent.DefaultProfile.Direction.ToString();

                    if (profile.Parent.MultiSite.IsMultiSite)
                    {
                        relationshipResponse.LocalMultiSite = profile.Parent.MultiSite.SiteName;
                    }

                    // Add the relationship response to a table for more
                    // convenient lookups.

                    relationships.Add(relationshipResponse.RelationshipId, relationshipResponse);

                    // Add the relationship to the response data structure.

                    response.Relationships.Add(relationshipResponse);
                }
                else
                {
                    // This profile's parent relationship already has a response
                    // so retrieve it.

                    relationshipResponse = relationships[profile.Parent.Id];
                }

                // Create response data for the profile.

                GetProfilesUIResponse.Profile profileResponse = new GetProfilesUIResponse.Profile();
                profileResponse.ProfileId = profile.Id;
                profileResponse.Direction = profile.Direction.ToString();
                if (profile.SynchronizeDatabase)
                {
                    profileResponse.Items.Add("Database");
                }
                if (profile.SynchronizeTemplates)
                {
                    profileResponse.Items.Add("Templates");
                }
                if (profile.SynchronizeWorkarea)
                {
                    profileResponse.Items.Add("Workarea");
                }
                if (profile.SynchronizeBinaries)
                {
                    profileResponse.Items.Add("Bin");
                }

                if (profile.IsDefault)
                {
                    // Default profile's use the connection string
                    // as a name. Replace this with a safer string.

                    profileResponse.ProfileName = "Default Relationship Profile";
                }
                else
                {
                    profileResponse.ProfileName = profile.Name;
                }


                profileResponse.Impediments = profile.SyncImpediments;

                SyncHandlerController.ResultCode profileRCode = Ektron.Cms.Sync.Presenters.SyncHandlerController.ResultCode.Success;

                // Find out if there is a global impediment, mainly no initial sync.
                if (response.Success == true)
                {
                    if (profile.SyncImpediments.Any(impediment => (impediment.Suggestion == ContentOrFolderSync.ImpedimentSuggestion.RunSuccessfulSyncFirst
                            || impediment.Suggestion == ContentOrFolderSync.ImpedimentSuggestion.SyncParentFolderFirst)))
                    {
                        profileRCode = SyncHandlerController.ResultCode.BlockingImpediment;
                    }
                }

                // Find out if there is a retry.
                if (profileRCode == SyncHandlerController.ResultCode.Success)
                {
                    if (profile.SyncImpediments.Any(impediment => impediment.Suggestion == ContentOrFolderSync.ImpedimentSuggestion.RetryOperation))
                    {
                        profileRCode = SyncHandlerController.ResultCode.RetryOperation;
                    }
                }

                // Find out if there is global flag from which we can recover by expanding scope.
                if (profileRCode == SyncHandlerController.ResultCode.Success)
                {
                    if (profile.SyncImpediments.Any(impediment => impediment.Suggestion == ContentOrFolderSync.ImpedimentSuggestion.ContinueOnUserApprovalSyncingMoreRecordsInExpandedScope))
                    {
                        profileRCode = SyncHandlerController.ResultCode.NonBlockingImpediment;
                    }
                }

                // Collect missing folder ids for later, will perform a folder path look up
                foreach (ContentOrFolderSync.ImpedimentDetail impediment in profile.SyncImpediments)
                {
                    var folders = from f in impediment.ParentFolderStatus
                                  where (bool)!f.Exists
                                  select f;
                    foreach (PeerServerParentFolderStatus folder in folders)
                    {
                        missingFolders.Add((long)folder.FolderId);
                    }
                }


                profileResponse.resultCode = profileRCode;
                // Add the profile response to the appropriate parent
                // response.

                relationshipResponse.Profiles.Add(profileResponse);
            }


            if (missingFolders.Count > 0)
            {
                List<GetProfilesUIResponse.FolderPath> folderPaths = new List<GetProfilesUIResponse.FolderPath>();
                FolderManager folderManager = new FolderManager();
                FolderCriteria criteria = new FolderCriteria();
                criteria.Condition = LogicalOperation.Or;
                foreach (long folderID in missingFolders)
                {
                    criteria.AddFilter(FolderProperty.Id, CriteriaFilterOperator.EqualTo, folderID);
                }
                List<FolderData> folderDataList = folderManager.GetList(criteria);
                if (folderDataList != null && folderDataList.Any())
                {
                    foreach (FolderData folder in folderDataList)
                    {
                        folderPaths.Add(new GetProfilesUIResponse.FolderPath() { ID = folder.Id, Path = folder.NameWithPath });
                    }

                    // Sort List By FolderPath 
                    var sortedfolderList = (from folder in folderPaths
                                            orderby folder.Path.Length ascending
                                            select folder).ToList();
                    response.FolderPaths = sortedfolderList;
                }
            }
        }



        return response;
    }

    public IsSyncInProgressResponse IsSyncInProgress()
    {
        IsSyncInProgressResponse response = new IsSyncInProgressResponse();

        SyncHandlerController.ResultCode result;
        Profile profile = _controller.IsSyncInProgress(out result);

        switch (result)
        {
            case SyncHandlerController.ResultCode.Success:
                response.Success = true;
                if (profile != null)
                {
                    response.IsSyncInProgress = true;
                    response.ProfileId = profile.Id;

                    if (profile.IsDefault)
                    {
                        ConnectionInfo connectionInfo = new ConnectionInfo(profile.Name);
                        response.ProfileName = string.Format(
                            "{0}/{1}",
                            connectionInfo.ServerName,
                            connectionInfo.DatabaseName);
                    }
                    else
                    {
                        response.ProfileName = profile.Name;
                    }

                    string message = string.Format(
                        _siteApi.EkMsgRef.GetMessage("lbl sync running confirm"),
                        response.ProfileName);

                    response.Messages.Add(message);
                }
                else
                {
                    response.IsSyncInProgress = false;
                }
                break;
            case SyncHandlerController.ResultCode.DatabaseError:
                response.Success = false;
                response.Messages.Add("Database error.");
                break;
            case SyncHandlerController.ResultCode.UnknownError:
                response.Success = false;
                response.Messages.Add("Unknown error.");
                break;
        }

        return response;
    }

    public CanSyncBeTriggeredResponse CanSyncBeTriggered()
    {
        CanSyncBeTriggeredResponse response = new CanSyncBeTriggeredResponse();

        SyncHandlerController.ResultCode result;
        long profileID = 0;
        bool canSyncBeTriggered = _controller.CanSyncBeTriggered(out result, out profileID);
        response.CurrentlyRunningProfileID = profileID;

        switch (result)
        {
            case SyncHandlerController.ResultCode.Success:
                response.CanSyncBeTriggered = canSyncBeTriggered;
                response.Success = true;
                break;
            case SyncHandlerController.ResultCode.None:
            case SyncHandlerController.ResultCode.UnknownError:
                response.Success = false;
                response.Messages.Add("Unknown error.");
                break;
        }

        return response;
    }

    public GetFilesResponse GetFiles(GetFilesParameters parameters)
    {
        GetFilesResponse response = new GetFilesResponse();

        SyncHandlerController.ResultCode result;
        FileSyncNode node = _controller.GetFiles(
            parameters.Id,
            parameters.Path,
            false,
            out result);

        switch (result)
        {
            case SyncHandlerController.ResultCode.Success:
                response.Success = true;
                response.Node = node;
                break;
            case SyncHandlerController.ResultCode.DatabaseError:
                response.Success = false;
                response.Messages.Add("Database error.");
                break;
            case SyncHandlerController.ResultCode.UnknownError:
                response.Success = false;
                response.Messages.Add("Unknown error.");
                break;
        }

        return response;
    }
}
