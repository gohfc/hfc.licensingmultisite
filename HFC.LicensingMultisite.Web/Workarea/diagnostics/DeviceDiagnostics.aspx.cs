﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text;

using Ektron.Cms.Mobile;

public partial class Workarea_diagnostics_images_DeviceDiagnostics : Ektron.Cms.Workarea.Page
{
    private EkDeviceInfo device;
    private string userAgent;
    private StringBuilder sb = new StringBuilder();

    protected void Page_Init(object sender, EventArgs e)
    {

        IDeviceInfoProvider wurflProvider = Ektron.Cms.ObjectFactory.Get<IDeviceInfoProvider>();
        userAgent = Request.ServerVariables["HTTP_USER_AGENT"];
        device = wurflProvider.GetDeviceInfo(userAgent);
       
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        Utilities.ValidateUserLogin();


        sb.Append("<b>User Agent :</b><br/>").Append(userAgent).Append(Environment.NewLine);
        sb.Append(String.Format("<b>{0}:{1}</b><br/>","Device UserAgent", device.DeviceUserAgent)).Append(Environment.NewLine);
        sb.Append(String.Format("<b>{0}:{1}</b><br/>", "Device ID", device.DeviceID)).Append(Environment.NewLine);

        sb.Append(String.Format("<b>{0}</b> - {1}<br/>", "model_name", device.DeviceModel)).Append(Environment.NewLine);
        sb.Append(String.Format("<b>{0}</b> - {1}<br/>", "brand_name", device.DeviceBrandName)).Append(Environment.NewLine);
        sb.Append(String.Format("<b>{0}</b> - {1}<br/>", "max_image_height", device.DeviceImageHeight)).Append(Environment.NewLine);
        sb.Append(String.Format("<b>{0}</b> - {1}<br/>", "max_image_width", device.DeviceImageWidth)).Append(Environment.NewLine);
        sb.Append(String.Format("<b>{0}</b> - {1}<br/>", "resolution_height", device.DeviceResHeight)).Append(Environment.NewLine);
        sb.Append(String.Format("<b>{0}</b> - {1}<br/>", "resolution_width", device.DeviceResWidth)).Append(Environment.NewLine);
        
        
        ltrUI.Text = sb.ToString();
    }
    
}
