﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CacheViewer.aspx.cs" Inherits="developer_CacheViewer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style>
        table.cacheviewer{border: 1px solid gray;margin-top: 10px;}
        th, td {Padding:0 10px;vertical-align: middle;float: none !important; font-weight: normal;text-align: left;}
        thead th{ background: none repeat scroll 0 0 #C3D9FF;}
        tbody tr:nth-child(2n) td, tbody tr.even td {background: none repeat scroll 0 0 #E5ECF9;}
        a.cache{margin: 10px 5px;display:inline-block;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
     <asp:Literal ID="uxLitNotLoggedIn" runat="server" Visible="false"></asp:Literal>
        <asp:MultiView ID="uxPageMultiView" runat="server" ActiveViewIndex="0">
            <asp:View ID="uxViewList" runat="server">
                <h2>Cache Details</h2>
                <asp:ListView ID="uxCacheListView" runat="server"  ItemPlaceholderID="aspItemPlaceholder">
                    <EmptyDataTemplate >Criteria did not match any records.</EmptyDataTemplate>
                    <LayoutTemplate>
                        <table class="cacheviewer">
                            <thead>
                                <tr>
                                    <th>Key</th>
                                    <th>Object Type</th>
                                    <th>Data</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:PlaceHolder ID="aspItemPlaceholder" runat="server"></asp:PlaceHolder>
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="devsite-method">
                                <%# Eval("Key")%>
                            </td>
                            <td class="devsite-method">
                                <%# Eval("Value").GetType().ToString()%>
                            </td>
                            <td class="devsite-method">
                                <a href='CacheViewer.aspx?id=<%# Eval("Key")%>'>Click here for data</a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <a href="cacheviewer.aspx?contains=ektron" class="cache">Ektron Cache Keys</a> | 
                <a href="cacheviewer.aspx" class="cache">All Cache Keys</a> | 
                <asp:LinkButton id="uxClearCache" OnClientClick="return confirm('Are you sure you want to clear the cache?');" OnClick="uxClearCache_Click" runat="server" CssClass="cache">Clear Cache</asp:LinkButton>
            </asp:View>
            <asp:View ID="uxViewItem" runat="server">
            <h2>Cache Item Detail</h2>
            <h4><asp:Literal ID="uxItemKey" runat="server" /></h4>
            <asp:Literal ID="uxCacheValue" runat="server"></asp:Literal>
              <asp:LinkButton id="LinkButton1" OnClientClick="return confirm('Are you sure you want to remove this item?');" OnClick="uxClearCache_Click" runat="server" CssClass="cache" style="display:block;">Clear Item</asp:LinkButton>
            </asp:View>
        </asp:MultiView>
    </form>
</body>
</html>
