<%@ Page Language="C#" AutoEventWireup="true" CodeFile="bydates.aspx.cs" Inherits="Commerce_bydates" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reporting By Dates</title>

    <script language="JavaScript" type="text/javascript">
    Ektron.ready( function() {
        $ektron('#go_live_span').addClass('minWidthSpan');
        $ektron('#end_date_span').addClass('minWidthSpan');    
    });
    function captureValue()
    {
        var startDate = new Date();
        var endDate = new Date();
        var hdnStartDate = document.getElementById('hdnStartDate');
        var hdnEndDate = document.getElementById('hdnEndDate');

        startDate = document.getElementById('go_live').value;
        endDate = document.getElementById('end_date').value;

        if ((startDate == '') || (endDate == '') || (Date.parse(endDate) < Date.parse(startDate)))
         {   
            alert('<asp:Literal runat="server" id="ltr_errStartEndDate" />');
            return false;
        }
        else
        {
            hdnStartDate.value = startDate;
            hdnEndDate.value = endDate;
                     
            window.parent.location.href = "fulfillment.aspx?action=bydates&startdate="+hdnStartDate.value+"&enddate="+hdnEndDate.value;
            return false;
        } 
    }    

    function Close() 
    {
        parent.ektb_remove();
    }
    </script>
    <style type="text/css">
        .minWidthSpan { width: 260px !important;}
        .btnFilter { display:inline-block !important; margin: 1em 1em 0 0 !important; }
    </style>
</head>
<body>
    <form id="frmMain" runat="server">
        <div>            
            <table class="ektronGrid" runat="server">                
                <tr>
                    <td>
                        <span title="<% =m_refContentApi.EkMsgRef.GetMessage("generic start date label") %>">
                            <asp:Literal Visible="true" ID="ltr_startdate" runat="server" />:</span></td>
                        
                    <td>
                        <span title="<% =m_refContentApi.EkMsgRef.GetMessage("tooptip select a start date and time from the calendar") %>">
                        <asp:Literal Visible="true" ID="ltr_startdatesel" runat="Server"/>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td title="End Date">
                        <span title="<% =m_refContentApi.EkMsgRef.GetMessage("generic end date label") %>">
                            <asp:Literal Visible="true" ID="ltr_enddate" runat="server" />:
                        </span>
                    </td>
                    <td>
                        <span title="<% =m_refContentApi.EkMsgRef.GetMessage("tooptip select an end date and time from the calendar") %>">
                        <asp:Literal Visible="true" ID="ltr_enddatesel" runat="Server"/>
                        </span>
                    </td>
                </tr>                
            </table>
            <div class="right">
                 <a class="button buttonFilter blueHover btnFilter" onclick="captureValue();" value="Filter" id="Button1" name="btnFilter" title="Filter">Filter</a>
            </div>
            <input type="hidden" id="hdnStartDate" runat="server" name="hdnStartDate" />
            <input type="hidden" id="hdnEndDate" runat="server" name="hdnEndDate" />           
        </div>
    </form>
</body>
</html>

