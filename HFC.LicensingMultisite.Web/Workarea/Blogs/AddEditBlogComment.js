﻿//Dependencies
//Ektron.Namespace.js

if ("undefined" !== typeof ($ektron)) {

    Ektron.Namespace.Register('Ektron.Workarea.Blog.AddEditComment');
    
    Ektron.Workarea.Blog.AddEditComment.SubmitForm = function () {
        var failCount = (0 !== $ektron('.ektron-ui-invalid').length), 
            emailFail = (0 === $ektron('.email input').val().length ),
            commentFail = (0 === $ektron.trim($ektron('.comment textarea').val()).length ); 
        if ( emailFail ) { alert('Please enter a valid email address.'); }
        else if ( commentFail ) { alert('Please enter a valid comment.'); }
        else if ( failCount ) { alert('Required fields missing.'); }
        else { document.forms[0].submit(); }

    }

}
