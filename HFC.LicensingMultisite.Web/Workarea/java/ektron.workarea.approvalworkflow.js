﻿/*
* Depends:
*   jQuery	
*   Ektron.Namespace.js
*   Ektron.String.js
*/

Ektron.Namespace.Register("Ektron.Workarea.ApprovalWorkFlow");
Ektron.Workarea.ApprovalWorkFlow = {
    closeWorkFlowDialog: function () {
        var pageAction = $ektron('#hdnRedirectAction').val();
        var id = $ektron('#hdnId').val();
        var lang = $ektron('#hdnLang').val();

        var redirectUrl = "content.aspx?LangType=" + lang + "&action=" + pageAction + "&id=" + id;
        $ektron(top.document).find("#BottomRightFrame").find("#ek_main")[0].src = redirectUrl;
        return false;
    },

    redirectToApprovalPage: function () {
        var contentFrame = $ektron(top.document).find("#BottomRightFrame").find("#ek_main")[0];
        var approvalMode = $ektron('#rblWorkflow input:checked').val();
        contentFrame.src = $ektron("#hdnRedirectURL").val() + "&approvalmode=" + approvalMode;
        return false;
    }
}