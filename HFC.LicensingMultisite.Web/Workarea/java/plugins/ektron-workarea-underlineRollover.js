﻿//underline rollover plugin
(function ($)
{
    $.fn.underlineRollover = function (options)
    {
        var defaults = {
            "maxWidth": 24,
            "className": "action-bar-rollover-indicator",
            "containerSelector": ".ektronToolbar, .couponList ul"
        };

        var options = $.extend(defaults, options);

        return this.each(function ()
        {
            var button = $(this);
            button.hover(function ()
            {
                var container = button.closest(options.containerSelector);
                container = container.first();
                if (container.size() == 0) { return; }
                button.rollover = document.createElement("div");
                container.append(button.rollover);
                button.rollover = $(button.rollover);
                button.rollover.addClass(options.className);
                var lineWidth = button.outerWidth();
                if (lineWidth > options.maxWidth)
                {
                    lineWidth = options.maxWidth;
                }
                var xpos = button.position().left;
                var leftmargin = Number(button.css("margin-left").replace("px", ""));
                if (leftmargin > 0)
                {
                    xpos += leftmargin;
                }
                button.rollover.css({
                    "width": +lineWidth + "px",
                    "left": +xpos + "px"
                });
            }, function ()
            {
                if (button.rollover)
                {
                    button.rollover.remove();
                }
            });
        });
    };
})($ektron);