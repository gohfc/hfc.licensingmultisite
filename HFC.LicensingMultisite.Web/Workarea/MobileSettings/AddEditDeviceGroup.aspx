﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEditDeviceGroup.aspx.cs"
    Inherits="Workarea.MobileSettings.AddEditDeviceGroup" %>

<%@ Register Src="WURFLUpdate.ascx" TagName="WURFLUpdate"
    TagPrefix="EkMobile" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .deviceTree .ui-icon
        {
            background-image: url("../FrameworkUI/images/silk/icons/phone.png");
        }
        .deviceTree .ektron-ui-tree-type-device .ui-icon
        {
            display: none;
        }
        .treeHeader, .gridHeader
        {
            background-color: rgb(230, 230, 230);
            border-radius: 6px 6px 0 0;
            font-size: 1em;
            color: #000;
            font-weight: bold;
            padding: 3px 10px;
            overflow: hidden;
            margin-bottom: 0px;
            margin-top: 5px;
        }
        .deviceTree .ektron-ui-tree-type-device .textWrapper
        {
            margin-left: 0;
        }
        .deviceTree li
        {
            margin-top: .25em;
        }
        .tdSpacer
        {
            width: 20px;
        }
        .intField
        {
            width: 40px !important;
        }
        .deleteBTN
        {
            width: 16px;
            height: 16px;
            background-image: url("../FrameworkUI/images/silk/icons/delete.png");
        }
        .tfName input
        {
            width:99%!important;
        }
        .tfSearchBox input {
            width: 50%;
        }
        .searchMessageWrapper {
            margin-top: 5px;
        }
    </style>
    <script type="text/javascript">
        function NoTagValidation(Fieldval, FieldName) {
            var re = /<(.|\n)*?>/g;
            if (re.test(Fieldval)) {
                return false;
            }
            else
                return true;

        }
        
        Ektron.ready(function () {
            $ektron(".tfSearchBoxWrapper input").focus(function () {
                var input = $ektron(this);
                if ("undefined" == typeof input.attr("data-ektron-initial-value")) {
                    input.attr("data-ektron-initial-value", input.val());
                    input.val("");
                }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div>
        <div class="ektronPageHeader">
         <div class="ektronTitlebar" id="divTitleBar">
                    <span id="WorkareaTitleBar">
                        <asp:Literal runat="server" ID="ltrTitle" />
                    </span>
                </div>
                <div class="ektronToolbar" id="htmToolBar">
                    <table>
                        <tr>
                            <td>
                                <a style="cursor: default" class="primary backButton" href="DeviceGroupingView.aspx">
                                </a>
                            </td>
                            <td style="padding-left: 5px; width: 32px;">
                                <asp:LinkButton runat="server" ID="lbtnSave" Text="<%$Resources: btnSave%>" ValidationGroup="Save"
                                    CssClass="primary editButton" />
                            </td>
                            <td>
                                <a id="HelpLink" runat="server" href="#Help" class="help" rel="nofollow" >
							        <img title="<%$Resources:Help %>" runat="server" alt="<%$Resources:Help %>" id="imgHelp" src="../images/UI/Icons/help.png" />
						        </a>
                            </td>
                        </tr>
                    </table>
                </div>
    <asp:UpdatePanel runat="server" ID="updPanel" ChildrenAsTriggers="true" UpdateMode="Always">
        <ContentTemplate>  
                <div class="ektronPageContainer">
                    <div style="border: #d4d4d4 0px solid; margin-left: 5px; margin-right: 5px; margin-top: 5px;">
                        <EkMobile:WURFLUpdate runat="server" ID="updateWURFL" />
                        <table width="100%">
                            <tr>
                                <td style="width: 50%; text-align:right;">
                                        <table style="width:100%">
                                            <tr>
                                                <td style="width:50px;"><b><asp:Literal ID="Literal1" runat="server" Text="<%$Resources: page_Name%>" /></b><span class="ektron-ui-required">*</span>:</td>
                                                <td>
                                                    <ektronUI:TextField runat="server" ID="tfName" ValidationGroup="Save" CssClass="tfName">
                                                <ValidationRules>
                                                    <ektronUI:RequiredRule  />
                                                    <ektronUI:CustomRule ClientValidationEnabled="true" JavascriptFunctionName="NoTagValidation" />
                                                </ValidationRules>
                                            </ektronUI:TextField>

                                                </td>
                                            </tr>
                                        </table>
                                </td>
                                <td class="tdSpeacer">
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="height:15px;"></td>

                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel runat="server" ID="pnlSearchArea" DefaultButton="btnSearch" style="vertical-align:middle; text-align:right;">
                                        <table style="width:100%">
                                            <tr>
                                                <td style="text-align:right;" class="tfSearchBoxWrapper"><ektronUI:TextField runat="server" ID="tfSearchBox" Text="<%$Resources: str_SearchHint%>" CssClass="tfSearchBox"></ektronUI:TextField></td>
                                                <td style="width:16px; text-align:right;">
                                                    <asp:ImageButton runat="server" ImageUrl="~/Workarea/FrameworkUI/images/silk/icons/magnifier.png" ID="btnSearch" OnClick="btnSearch_Click" />
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </asp:Panel>
                                </td>
                                <td class="tdSpeacer">
                                </td>
                                <td>
                                    <b><asp:Literal runat="server" Text="<%$Resources: page_DimHeader%>" /></b>
                                    <ektronUI:Infotip ID="infoPreviewDimensions" runat="server" PositionCollision="flip" Hide="fadeOut" PositionMy="left top" Text="<%$Resources: str_PreviewDimensionsInfo %>" />
                                    <br />
                                    <b></b>
                                    <asp:Literal runat="server" Text="<%$Resources: dim_Width%>" />: </b>
                                    <ektronUI:IntegerField runat="server" ID="tfWidth" MinValue="0" Incremental="false"
                                        CssClass="intField"></ektronUI:IntegerField>
                                    <asp:Literal runat="server" Text="<%$Resources: dim_Pixels%>" />
                                    &nbsp;&nbsp;&nbsp; <b>
                                        <asp:Literal runat="server" Text="<%$Resources: dim_Height%>" />: </b>
                                    <ektronUI:IntegerField runat="server" ID="tfHeight" MinValue="0" Incremental="false"
                                        CssClass="intField"></ektronUI:IntegerField>
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$Resources: dim_Pixels%>" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%; vertical-align: top;">
                                    <h2 id="uxTreeHeader" class="treeHeader" runat="server" />
                                    <div style="border: #d4d4d4 1px solid;">
                                        <ektronUI:Tree runat="server" ID="treeCtrl" RegisterCss="false" SelectionMode="MultiForAll"
                                            CssClass="showControls deviceTree" AutoPostBackOnSelectionChanged="true">
                                        </ektronUI:Tree>
                                    </div>
                                    <div class="searchMessageWrapper">
                                        <ektronUI:Message runat="server" ID="msgResultShowHere" Text="<%$Resources: str_SearchResultsDisplayHere%>" DisplayMode="Information"  Visible="True" EnableViewState="false"/>
                                        <ektronUI:Message runat="server" ID="msgNoResult" Text="<%$Resources: str_SearchNoRes%>" DisplayMode="Warning"  Visible="false" EnableViewState="false"/>
                                    </div>
                                </td>
                                <td class="tdSpacer">
                                </td>
                                <td style="vertical-align: top">
                                    <asp:Panel runat="server" ID="gridArea">
                                        <h2 id="uxGridHeader" class="gridHeader" runat="server" />
                                        <ektronUI:GridView runat="server" ID="mainGrid" AutoGenerateColumns="false" EnableEktronUITheme="true">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="20">
                                                    <ItemTemplate>
                                                        <asp:ImageButton runat="server" ImageUrl="~/Workarea/FrameworkUI/images/silk/icons/delete.png"
                                                            ID="lbDeleteItem" CommandArgument='<%#Eval("Model")%>' CommandName="delItem" />
                                                        <%--<asp:LinkButton runat="server" ID="lbDeleteItem"  CssClass="deleteBTN" CommandArgument='<%#Eval("Model")%>'
                                                        CommandName="delItem"></asp:LinkButton>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </ektronUI:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            
        </ContentTemplate>
    </asp:UpdatePanel>
    </div></div>
    </form>
</body>
</html>
