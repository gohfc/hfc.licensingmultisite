﻿Ektron.ready(function () {
    // jCarousel
    $ektron('.phonepreview-carousel').jcarousel({
        visible: 3,
        itemFallbackDimension: 100
    });
    $ektron("div.jcarousel-next").hide();
    $ektron("div.jcarousel-prev").hide();

    // highlighted current row
    $ektron("table.breakpointTbl tr").on("click", function () {
        $ektron("tr.selected").removeClass("selected");
        $ektron("div.jcarousel-next").hide();
        $ektron("div.jcarousel-prev").hide();
        $ektron(this).addClass("selected");
        $ektron("div.jcarousel-next", this).show();
        $ektron("div.jcarousel-prev", this).show();
    });

    // re-set the width information box when page load and when window resize
    var resizeTimer;
    ResetBreakpointInfoBox();
    $ektron(window).on("resize", function () {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(ResetBreakpointInfoBox, 500);
    });

    // hide and show the breakpoint name text box
    $ektron("span.txtGroupName").hide();
    $ektron("a.DeviceGroupName").on("click", function () {
        var eThis = $ektron(this);
        var hiddenTextBox = eThis.siblings("span.txtGroupName").show();
        eThis.hide();
    });
    $ektron("span.txtGroupName input").on("blur", function (e) {
        UpdateFileLabel(e);
    }).on("keydown", function (e) {
        if (13 == e.keyCode && !e.altKey && !e.ctrlKey && !e.shiftKey) {
            e.stopPropagation();
            UpdateFileLabel(e);
            return false;
        }
    });
});

function ResetBreakpointInfoBox() {
    $ektron("span#BreakpointRange").each(function () {
        var eThis = $ektron(this);
        var eParentCell = eThis.closest("td#left");
        var endbarLen = $ektron("img.endbar", eParentCell).offset();
        if (typeof endbarLen != "undefined" && endbarLen.left > 130) {
            eThis.css("left", endbarLen.left - 40);
        }
    });
}

function UpdateFileLabel(evt) {
    var targetInputElement = (evt.target ? evt.target : evt.srcElement);
    var eThis = $ektron(targetInputElement);
    eThis.removeClass("ektron-ui-invalid");
    if (eThis.val().length > 0) {
        eThis.closest("span.txtGroupName").siblings("a.DeviceGroupName").text(eThis.val()).show();
        eThis.hide();
    }
    else {
        eThis.addClass("ektron-ui-invalid");
    }
}

/*
 * This checks if any breakpoint has no device enabled, 
 * highlighting the particular breakpoint for attention,
 * displays a page-wide error message.
 */
function EnableDeviceValidation(src, args) {
    var carousels = $ektron(".phonepreview-carousel");
    carousels.removeClass("error");

    var invalidCarousels = carousels.not(":has(:checked)");
    if (invalidCarousels.length == 0) {
        args.IsValid = true;
        $ektron(".enableDeviceErrorMessage").hide();
    }
    else {
        args.IsValid = false;
        $ektron(".enableDeviceErrorMessage").show();

        $ektron(invalidCarousels).addClass("error");
    }
}

// delete dialog helper
Ektron.ready(function () {
    if (typeof jsDeleteDlgId === 'undefined') {
        alert("Opening delete dialog is disabled: the dialog selector variable 'jsDeleteDlgId' is undefined.");
    }
});
function openDeleteDialog() {
    $ektron(jsDeleteDlgId).dialog("open");
}
function closeDeleteDialog() {
    $ektron(jsDeleteDlgId).dialog("close");
}
