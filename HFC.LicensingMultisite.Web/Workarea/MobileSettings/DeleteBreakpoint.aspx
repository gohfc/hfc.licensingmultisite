﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeleteBreakpoint.aspx.cs" Inherits="Workarea.MobileSettings.DeleteBreakpoint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar"></span>
                    
            </div>
            <div class="ektronToolbar" id="htmToolBar" runat="server">

             <table>
                    <tr>
                        <td>
                            <a style="cursor: default" class="primary backButton" runat="server"  id="ancBack" >
                            </a>
                        </td>
                        <td style="padding-left: 5px; width:45px;">
                            <asp:LinkButton runat="server" ID="btnDelete" Text="<%$Resources: btn_Delete%>" CssClass="primary editButton"  />
                        </td>
                    </tr>
                  </table>
            </div>
        </div>
        <div class="ektronPageContainer" >
            <div style="border:#d4d4d4 1px solid; margin-left:5px; margin-right:5px; margin-top:5px; "  >
            <ektronUI:Message runat="server" ID="msgBox" />
                <asp:Literal  ID="ltrBrkName" runat="server" />
                <asp:Repeater runat="server" ID="rptTemplateMapping">
                    <HeaderTemplate>
                    <table  class="ektronGrid"  style="  margin:5px; border:1px solid #d4d4d4;">
                            <tr>
                                <td>Template Id</td>
                                <td>Template File Name</td>
                                <td>Breakpoint File Name</td>
                                <td>Enabled</td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr>
                                <td><%#Eval("TemplateId")%></td>
                                <td><%#Eval("TemplateSourceFile")%></td>
                                <td><%#Eval("TemplateFileName")%></td>
                                <td><asp:CheckBox runat="server" ID="cbEnabled"  Checked='<%#Eval("Enabled")%>' Enabled="false"/> </td>
                            </tr>

                    </ItemTemplate>
                    <FooterTemplate>
                    </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
