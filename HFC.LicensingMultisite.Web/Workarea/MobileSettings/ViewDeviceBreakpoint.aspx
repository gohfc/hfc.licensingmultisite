﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewDeviceBreakpoint.aspx.cs" Inherits="Workarea.MobileSettings.ViewDeviceBreakpoint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <div class="ektronPageHeader">
            <div class="ektronTitlebar" id="divTitleBar" runat="server">
                <span id="WorkareaTitleBar"></span>
                    
            </div>
            <div class="ektronToolbar" id="htmToolBar" runat="server">
                <a href="AddEditBreakPoint.aspx" class="primary editButton" style=" width:100px;"><asp:Literal runat="server" ID="ltrBtnAddNew"  Text="<%$Resources: btn_Addnew%>"/> </a>
            </div>
        </div>
        <div class="ektronPageContainer" >
            <div style="border:#d4d4d4 1px solid; margin-left:5px; margin-right:5px; margin-top:5px; "  >
            <ektronUI:GridView runat="server" ID="mainGrid" AutoGenerateColumns="false" EnableEktronUITheme="true"   OnEktronUISortChanged="EkGrid_EktronUIThemeSortChanged"  OnEktronUIPageChanged="EkGrid_EktronUIThemePageChanged"  >
                <Columns>
                <asp:BoundField HeaderText="Id" DataField="<%$Resources: gridHeader_Id%>" SortExpression="Id" />
                <asp:TemplateField HeaderText="<%$Resources: gridHeader_Name%>" SortExpression="Name">
                    <ItemTemplate>
                        <a href= "<%#"AddEditBreakPoint.aspx?Id="+Eval("Id") %>"  ><%#Eval("Name") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                    
                    <asp:BoundField HeaderText="<%$Resources: gridHeader_FileLabel%>" DataField="FileLabel" SortExpression="FileLabel"/>
                    <asp:BoundField HeaderText="<%$Resources: gridHeader_Width%>" DataField="Width" SortExpression="Width" />
                    <asp:BoundField HeaderText="<%$Resources: gridHeader_Height%>" DataField="Height" SortExpression="Height" />
                </Columns>
            </ektronUI:GridView>
            </div>

            

        </div>
    </div>
    </form>
</body>
</html>
