﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeviceGroupingView.aspx.cs" Inherits="Workarea.MobileSettings.DeviceGroupingView" %>

<%@ Reference Control="DeviceGroupTemplateListingCtrl.ascx" %>
<%@ Register Src="WURFLUpdate.ascx" TagName="wurflupdate" TagPrefix="ektControl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <ektronUI:Css Path="css/deviceGroupingView.css" runat="server" />
        <asp:ScriptManager runat="server" ID="scriptManager1" />

        <div>
            <div class="ektronPageHeader">
                <div class="ektronTitlebar" id="divTitleBar" runat="server">
                    <span id="WorkareaTitleBar">
                        <asp:Literal runat="server" Text="<%$Resources:page_title %>" />
                    </span>

                </div>
                <div class="ektronToolbar" id="htmToolBar" runat="server" style="text-align: center;">
                    <table style="" cellpadding="1px" cellspacing="1px">
                        <tr>
                            <td>
                                <a onclick="openAddDialog();" href="#" class="primary editButton">
                                    <asp:Literal runat="server" ID="ltrBtnAddNew" Text="<%$Resources: btn_newgroup%>" />
                                </a>
                            </td>
                            <td>
                                <a id="HelpLink" runat="server" href="#Help" class="help" rel="nofollow">
                                    <img title="<%$Resources:Help %>" runat="server" alt="<%$Resources:Help %>" id="imgHelp" src="../images/UI/Icons/help.png" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="ektronPageContainer">
                <div class="deviceGroupDescription">
                    <asp:Literal runat="server" ID="ltr_headerText" Text="<%$Resources: str_header%>" />
                </div>

                <ektControl:wurflupdate runat="server" ID="wurflUpdateCtrl" />

                <asp:Panel ID="uxDeviceGroups" runat="server">
                    <h1>
                        <asp:Literal runat="server" Text="<%$Resources:gridLbl_model %>" /></h1>
                    <ektronUI:GridView runat="server" ID="gvDeviceGroup" AutoGenerateColumns="false" HeaderStyle-CssClass="GridHeader" CssClass="flexGrid">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ImageUrl="~/Workarea/FrameworkUI/images/silk/icons/delete.png"
                                        ID="lbDeleteItem" CommandArgument='<%#Eval("Id")%>' CommandName="delItem" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lbreOrderUp" Text="" CommandName="reorderUP" CommandArgument='<%#Eval("Id") %>' Visible='<%# Container.DataItemIndex!=0%>' CssClass="ui-state-default ui-corner-all ui-icon ui-icon-triangle-1-n reorderUp" />
                                    <asp:LinkButton runat="server" ID="lbreOrderDown" Text="" CommandName="reorderDOWN" CommandArgument='<%#Eval("Id") %>' CssClass="ui-state-default ui-corner-all ui-icon ui-icon-triangle-1-s reorderDown"
                                        Visible='<%# Container.DataItemIndex != ((List<DeviceGroupingViewData>)gvDeviceGroup.DataSource).Count-1%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Id" HeaderText="<%$Resources: gridHeader_Id%>" />
                            <asp:BoundField DataField="GroupTypeText" HeaderText="<%$Resources: gridHeader_GroupBy%>" />
                            <asp:TemplateField HeaderText="<%$Resources: gridHeader_Name%>">
                                <ItemTemplate>
                                    <a href="<%#"AddEditDeviceGroup.aspx?DeviceGroupID="+Eval("Id") %>&AddGroupType=<%#Eval("GroupTypeText")%>"><%#Eval("GroupName")%></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="<%$Resources: gridHeader_Info%>" DataField="GroupInfo" ItemStyle-CssClass="primary" />
                            <asp:TemplateField HeaderText="<%$Resources: gridHeader_NumofTemplate%>">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lbViewTemplates" CommandName="viewTemplates" CommandArgument='<%#Eval("Id")%>' Text='<%#string.Format(GetLocalResourceObject("str_infoTemp").ToString(), Eval("NumOfTemplates"))%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </ektronUI:GridView>
                </asp:Panel>

                <asp:Panel ID="uxBreakpointGroups" runat="server">
                    <h1>
                        <asp:Literal runat="server" Text="<%$Resources:gridLbl_breakpoint %>" /></h1>
                    <ektronUI:GridView runat="server" ID="gvBreakpoint" AutoGenerateColumns="false" EnableEktronUITheme="true" HeaderStyle-CssClass="GridHeader" CssClass="flexGrid">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" ImageUrl="~/Workarea/FrameworkUI/images/silk/icons/delete.png"
                                        ID="lbDeleteItem" CommandArgument='<%#Eval("Id")%>' CommandName="delItem" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Id" HeaderText="<%$Resources: gridHeader_Id%>" />
                            <asp:BoundField HeaderText="<%$Resources: gridHeader_BP_Width%>" DataField="GroupInfo" />
                            <asp:TemplateField HeaderText="<%$Resources: gridHeader_Name%>">
                                <ItemTemplate>
                                    <a href="<%#"AddEditBreakPoint.aspx?Id="+Eval("Id") %>"><%#Eval("GroupName")%></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ControlStyle-CssClass="primary" HeaderText="<%$Resources: gridHeader_BP_Example%>" DataField="ExampleDevice" />
                            <asp:TemplateField HeaderText="<%$Resources: gridHeader_NumofTemplate%>">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lbViewTemplates" CommandName="viewTemplates" CommandArgument='<%#Eval("Id")%>' Text='<%#string.Format(GetLocalResourceObject("str_infoTemp").ToString(), Eval("NumOfTemplates"))%>' />
                                    <%# ((DeviceGroupingViewData)Container.DataItem).GroupType == DeviceGroupingViewData.GroupTypes.Size ? "<br/><a class='oneLine' href='AdaptiveImageSettings.aspx'>" + GetLocalResourceObject("str_adpImgSetting").ToString() + "</a>" : ""%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </ektronUI:GridView>
                </asp:Panel>
            </div>
            <!--Grid Ends-->
            <!--Dialogs below-->
            <ektronUI:Dialog runat="server" ID="diagAddNewGroup" Modal="true" AutoOpen="false" Title="<%$Resources:diag_Add_title_outer %>" Resizable="false" Width="640" Height="480">
                <ContentTemplate>
                    <div style="margin-top: 10px;">
                        <h1>
                            <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:diag_Add_title %>" /></h1>
                        <asp:Literal runat="server" Text="<%$Resources:diag_Add_topText %>" />
                        <asp:RadioButtonList runat="server" ID="rblAddNew">
                            <asp:ListItem Value="breakpoint" Text="<%$Resources:diag_Add_item_breakpoint %>" />
                            <asp:ListItem Value="os" Text="<%$Resources:diag_Add_item_os %>" />
                            <asp:ListItem Value="model" Text="<%$Resources:diag_Add_item_model %>" />
                        </asp:RadioButtonList>
                        <br />
                        <div style="text-align: right">
                            <ektronUI:Button runat="server" ID="btnAddNew" Text="<%$Resources:diag_Add_btn_Create %>" OnClick="btnAddNew_Click"></ektronUI:Button>
                            <ektronUI:Button runat="server" ID="btnAddCancel" Text="<%$Resources:diag_Cancel %>" OnClientClick="return closeAddDialog();"></ektronUI:Button>
                        </div>
                    </div>
                </ContentTemplate>
            </ektronUI:Dialog>

            <ektronUI:Dialog runat="server" ID="diagDelete" Modal="true" AutoOpen="false" Title="<%$Resources:diagDel_title %>" Width="600" Height="350">
                <ContentTemplate>
                    <h1>
                        <asp:Literal ID="ltrdiagDeleteHeader" runat="server" /></h1>
                    <asp:Literal runat="server" Text="<%$Resources:diagDel_body %>" />
                    <asp:Panel runat="server" ID="pnlDeleteListHolder" />
                    <b>
                        <asp:Literal runat="server" Text="<%$Resources:diagDel_confirm %>" /></b>
                    <div style="text-align: right">
                        <ektronUI:Button runat="server" ID="btnDelete" DisplayMode="Anchor" Text="<%$Resources:diagDel_btnDelete %>" OnClick="btnDelete_Click"></ektronUI:Button>
                        <ektronUI:Button runat="server" ID="btnDelteCancel" Text="<%$Resources:diag_Cancel %>" OnClientClick="return closeDeleteDialog();"></ektronUI:Button>
                    </div>
                </ContentTemplate>
            </ektronUI:Dialog>

            <ektronUI:Dialog runat="server" ID="diagViewTemplates" Modal="true" AutoOpen="false" Title="<%$Resources:diag_View_title %>" Width="600" Height="350">
                <ContentTemplate>
                    <asp:Panel runat="server" CssClass="templateList" ID="pnlViewListHolder" />
                    <div style="text-align: right">
                        <ektronUI:Button runat="server" ID="btnCloseView" Text="<%$Resources:diag_Cancel %>" OnClientClick="return closeViewDialog();"></ektronUI:Button>
                    </div>
                </ContentTemplate>
            </ektronUI:Dialog>

            <script type="text/javascript">
                function openDeleteDialog() {
                    $ektron("<%=diagDelete.Selector %>").dialog("open");
            }
            function closeDeleteDialog() {
                $ektron("<%=diagDelete.Selector %>").dialog("close");
                return false;
            }

            function openViewDialog() {
                $ektron("<%=diagViewTemplates.Selector %>").dialog("open");
            }
            function closeViewDialog() {
                $ektron("<%=diagViewTemplates.Selector %>").dialog("close");
                return false;
            }
            function openAddDialog() {
                $ektron("<%=diagAddNewGroup.Selector %>").dialog("open");
            }
            function closeAddDialog() {
                $ektron("<%=diagAddNewGroup.Selector %>").dialog("close");
                return false;
            }
            </script>
        </div>
    </form>
</body>
</html>
