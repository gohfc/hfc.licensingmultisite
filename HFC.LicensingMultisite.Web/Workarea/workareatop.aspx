<%@ Page Language="C#" AutoEventWireup="true" Inherits="workareatop" CodeFile="workareatop.aspx.cs" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        var selectSettingsTab = <asp:Literal id="SelectSettingsTab" runat="server" text="false"/>;
        var iFrameSize = 1;
        var m_MinFrameSize = 0;
        var bBusy = false;
        var iCurrentSize = 0;
        var iIe5FrameSizeAdjustment = 4;
        var iFadeInterval = 20;
        var iChangeSize = 85;
        var bEnlargePending = false;
        var szOldRowsSize = "58,*";
        var aszFramerows = "";
        var m_OrigMouseOver = "";
        var m_OrigMouseOut = "";
        var topMenuWhiteListdata="<asp:literal id="topMenuWhiteList" runat="server" />";
        
        function Initialize()
        {
            self.focus();
            if (selectSettingsTab) {
                top.switchSettingsTab();
            }
        }

		function ChangePage(tab, pageName)
		{
			if (typeof(top.CanNavigate) == "function" && !top.CanNavigate())
			{
				alert('You are currently in edit mode, please either save/update or cancel. ');
                return;
			}

			//ensure ie9 resizes the accordion properly
			if ("undefined" !== typeof (top.Ektron.Workarea.Height)) {
                		top.Ektron.Workarea.Height.execute();
            		}

			// Load the appropriate page on the right side
		    top.SelectMainWindow(pageName);

            // Update the tree on the left side
            top.MakeNavTreeVisible(pageName);
            
            //highlight root folder.
            try{
                if ("undefined" != typeof(top.ek_nav_bottom.NavIframeContainer.nav_folder_area.ContentTree.g_selectedFolderList)) {
                    top.ek_nav_bottom.NavIframeContainer.nav_folder_area.ContentTree.g_selectedFolderList = '0';
                    top.ek_nav_bottom.NavIframeContainer.nav_folder_area.ContentTree.showSelectedFolderTree();
                }
            }catch(e){}

			$ektron("#tabs .selected").removeClass("selected");
			$ektron(tab).addClass("selected");
		}

          function PopUpWindow(url, hWind, nWidth, nHeight, nScroll, nResize) {
              var cToolBar = "toolbar=0,location=0,directories=0,status=" + nResize + ",menubar=0,scrollbars=" + nScroll + ",resizable=" + nResize + ",fullscreen=no";
            var popupwin = window.open(url, hWind, cToolBar);
            return popupwin;
        }
        function ResizeTopMenu(Direction,location) {
            var isWhiteList=false;
            if((topMenuWhiteListdata.length>0) && (typeof(location)!="undefined") && (location.length>0))
            {
                var url=location.split('?');
                var whiteList=topMenuWhiteListdata.split(',');
                for(var j=0;j<whiteList.length;j++)
                {
                    if(url[0].toLowerCase().indexOf(whiteList[j].toLowerCase())>0)
                    {
                        isWhiteList=true;
                        break;
                    }
                }
            }
            if(((isWhiteList) && (Direction == 0)) || (Direction == 1)){
                var NavFrame = top.document.getElementById('TopFrameset');
                if(NavFrame){
                    
                    if (Direction != null) {
                        if ((iFrameSize == 1) && (Direction == 1))
                            return;
                        if ((iFrameSize == 0) && (Direction == 0))
                            return;
                    }
                    bBusy = true;
                    iCurrentSize = 0;
                    if (((Direction == null) && (iFrameSize == 1)) || (Direction == 0)) {
                        szOldRowsSize = NavFrame.rows;
                        if (top.IsBrowserIE() && (!((navigator.userAgent.toLowerCase()).indexOf("msie 5.") == -1))) {
                            var tmpFrameSizes = NavFrame.rows.split(",");
                            tmpFrameSizes[0] = (parseInt(document.body.clientWidth) + (iIe5FrameSizeAdjustment * 1));
                            szOldRowsSize = "";
                            for (var iLoop = 0; iLoop < tmpFrameSizes.length; iLoop++) {
                                szOldRowsSize += tmpFrameSizes[iLoop].toString() + ",";
                            }
                            szOldRowsSize = szOldRowsSize.substring(0, (szOldRowsSize.length - 1));
                        }
                        aszFramerows = szOldRowsSize.split(",");
                        iCurrentSize = aszFramerows[0];
                        setTimeout("FadeFrame(0)", 50);
                    }
                    else {
                        document.getElementById("ektronWorkAreaHeader").style.display = "";
                        aszFramerows = szOldRowsSize.split(",");
                        iCurrentSize = 0;
                        setTimeout("FadeFrame(1)", 50);
                    }
                }
            }
        }
        function FadeFrame(Direction) {
            var NavMenuFrame = top.document.getElementById('TopFrameset');
            if (Direction == 0) {
                bEnlargePending = false;
                iCurrentSize = iCurrentSize - iChangeSize;
                if ((iCurrentSize <= m_MinFrameSize) || SnapTopMenuFrame()) {
                    iCurrentSize = m_MinFrameSize;
                    iFrameSize = 0;
                    bBusy = false;
                    document.body.onmouseover = m_OrigMouseOver;
                    document.body.onmouseout = m_OrigMouseOut;
                }
                else {
                    setTimeout("FadeFrame(" + Direction + ")", iFadeInterval);
                }
            }
            else {
                iCurrentSize = iCurrentSize + iChangeSize;
                if ((iCurrentSize >= aszFramerows[0]) || SnapTopMenuFrame()) {
                    iCurrentSize = aszFramerows[0];
                    iFrameSize = 1;
                    bBusy = false;
                    document.body.onmouseover = null;
                    document.body.onmouseout = null;
                }
                else {
                    setTimeout("FadeFrame(" + Direction + ")", iFadeInterval);
                }
            }
            NavMenuFrame.rows = iCurrentSize + ",*";
            if (iCurrentSize <= m_MinFrameSize) {
                iCurrentSize = 0;
            }
            document.getElementById("ektronWorkAreaHeader").style.top = (iCurrentSize - aszFramerows[0]) + "px";
        }
        function SnapTopMenuFrame() {
            if ((typeof (top.SnapTopMenuFrame) == "function") && top != self) {
                return (top.SnapTopMenuFrame());
            } else {
                return false;
            }
        }
    </script>

</head>
<body onload="Initialize();">
    <div class="ektronWorkAreaHeader" id="ektronWorkAreaHeader">
        <h1 class="logo" title="Ektron CMS400.NET">
            Ektron CMS400.NET</h1>
        <div class="dvVersion">
            <label class="version">
                <asp:Literal runat="Server" ID="ltrVersion" />
            </label>
         </div>
         <div class="licInfo">
             <asp:Literal runat="Server" ID="ltrLic" />
         </div>
        <div class="userInfo">
            <asp:Label ToolTip="User Name" ID="lblUser" CssClass="userName" runat="server" />
            |
            <asp:Label ToolTip="New Messages" ID="userUnreadMessages" CssClass="userMessages"
                runat="server" />
        </div>
        <div class="tabs">
            <div id="tabs" class="ui-tabs">
                <ul class="ui-tabs-nav ui-helper-reset">
                    <li class="left">&nbsp;</li>
                    <li id="Desktop"><a title="Desktop" href="#" id="DesktopLink" onclick="ChangePage(this, 'SmartDesktopTree')" 
                        runat="server">Desktop</a> </li>
                    <li class="divider"></li>
                    <li id="Content"><a title="Content" href="#" id="ContentLink" onclick="ChangePage(this, 'ContentTree')"
                        runat="server">Content</a> </li>
                    <li class="divider"></li>
                    <li id="Library"><a title="Library" href="#" id="LibraryLink" onclick="ChangePage(this, 'LibraryTree')"
                        runat="server">Library</a> </li>
                    <li class="divider"></li>
                    <li id="Settings"><a title="Settings" href="#" id="SettingsLink" onclick="ChangePage(this, 'AdminTree')"
                        runat="server">Settings</a> </li>
                    <li class="divider"></li>
                    <li id="Reports"><a title="Reports" href="#" id="ReportsLink" onclick="ChangePage(this, 'ReportTree')"
                        runat="server">Reports</a> </li>
                    <li class="divider"></li>
                    <li id="Help"><a title="Help" href="#" id="HelpLink" onclick="ChangePage(this, 'Help')"
                        runat="server" rel="nofollow">Help</a> </li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>
