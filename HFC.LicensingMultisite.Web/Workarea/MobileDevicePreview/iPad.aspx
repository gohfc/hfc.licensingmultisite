﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="iPad.aspx.cs" Inherits="iPad" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>iPad Peek</title>
    <%--<script type="text/javascript" src="iPad_files/jquery_002.js"></script>
    <script type="text/javascript" src="iPad_files/jquery.js"></script>
    <script type="text/javascript" src="iPad_files/script.js"></script>--%>
    
    <script language="javascript" type="text/javascript">
     Ektron.ready(function () {

        });
        function rotatediv() {
            if ($ektron("#ipad")[0].className == "landscape")
                $ektron("#ipad")[0].className = "portrait";
            else
                $ektron("#ipad")[0].className = "landscape";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="ipad" class="landscape">
          <div id="rotate" runat="server" onclick="rotatediv();"></div>
          <div id="reload"></div>
          <div style="display: none;" id="kbd"></div>
          <input value="http://localhost/cms400min851b372" id="url" runat="server">
          <input id="google">
          <iframe id="frame" runat="server"></iframe>
        </div>
    </form>
</body>
</html>
