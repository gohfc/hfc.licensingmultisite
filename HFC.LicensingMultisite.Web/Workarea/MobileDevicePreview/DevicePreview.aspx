﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DevicePreview.aspx.cs" Inherits="Workarea.MobileDevicePreview.DevicePreview" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        $(function () {
            var f = $('#ektIframe')
            f.load(function () {
                var ifr = document.getElementById("ektIframe"),
                    images = ifr.contentDocument.getElementsByTagName("img"),
                    rndNum = Math.floor((Math.random() * 1000) + 1);

                for (var i = 0; i < images.length; i++) {
                    images[i].setAttribute("src", images[i].getAttribute('src') + "?reload=" + rndNum);
                }
            });
        });

        function getCookie(c_name) {
            var i, x, y, ARRcookies = document.cookie.split(";");

            for (i = 0; i < ARRcookies.length; i++) {
                x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                x = x.replace(/^\s+|\s+$/g, "");
                if (x == c_name) {
                    return unescape(y);
                }
            }
        }

        function setCookie(c_name, value, exdays) {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + exdays);

            var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
            document.cookie = c_name + "=" + c_value;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="uxBreakpointLabel" runat="server" />
            <asp:DropDownList runat="server" ID="ddlBreakpoints" AutoPostBack="true" />
            <br />
            <br />
            <asp:Literal runat="server" ID="ltrDem"></asp:Literal><br />
            <asp:Panel runat="server" ID="frameContainer">
                <iframe runat="server" id="ektIframe" style="border: 5px solid #000; padding: 3px; width: 100%; height: 100%" />
            </asp:Panel>
        </div>
    </form>
</body>
</html>
