﻿namespace Workarea.MobileDevicePreview
{
    using System;
    using System.Web.UI;
    using Ektron.Cms;
    using Ektron.Cms.Common;
    using Ektron.Cms.Framework.Settings.Mobile;

    public partial class DevicePreview : Ektron.Cms.Workarea.Page
    {
        DeviceBreakpointManager breakpointManager = new DeviceBreakpointManager();
        EkMessageHelper messages;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            messages = CommonApi.Current.EkMsgRef;

            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronJS);
            Ektron.Cms.API.JS.RegisterJS(this, Ektron.Cms.API.JS.ManagedScript.EktronUICoreJS);

            ddlBreakpoints.SelectedIndexChanged += new EventHandler(ddlBreakpoints_SelectedIndexChanged);
            ddlBreakpoints.DataTextField = "Name";
            ddlBreakpoints.DataValueField = "Id";

            uxBreakpointLabel.Text = messages.GetMessage("lbl device preview selection");
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!Page.IsPostBack)
            {
                ddlBreakpoints.DataSource = breakpointManager.GetList(new Ektron.Cms.Settings.Mobile.DeviceBreakpointCriteria());
                ddlBreakpoints.DataBind();
                LoadPreviewFrame();
            }
        }

        private void ddlBreakpoints_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPreviewFrame();
        }

        private void LoadPreviewFrame()
        {
            var selectedBreakpoint = breakpointManager.GetItem(long.Parse(ddlBreakpoints.SelectedValue));
            frameContainer.Width = selectedBreakpoint.Width;
            frameContainer.Height = selectedBreakpoint.Height;

            string targetURL=System.Web.HttpUtility.UrlEncode( Request.QueryString["postbackurl"].ToString());
            string ProxyURL = string.Format("DeviceViewProxy.aspx?targetURL={0}&width={1}&height={2}&model={3}", targetURL, selectedBreakpoint.Width.ToString(), selectedBreakpoint.Height.ToString(), "mobile");

            ltrDem.Text = string.Format("W:{0}px H:{1}px", selectedBreakpoint.Width.ToString(), selectedBreakpoint.Height.ToString());

            ektIframe.Attributes["src"] = ProxyURL;           
        }
    }
}