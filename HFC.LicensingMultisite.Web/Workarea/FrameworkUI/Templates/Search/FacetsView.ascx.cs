﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.UI.Views;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Framework.UI.Controls;

interface ISearchResult
{
    object this[string key] { get; set; }
    
    IEnumerable<string> GetKeys();

    IDictionary<string, object> GetData();
}

public partial class FrameworkUI_Templates_Search_FacetsView : Ektron.Cms.Framework.UI.Views.BaseTemplate<IFacetsView, IFacetsController>
{
    protected void facets_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        Facet facet = e.Item.DataItem as Facet;
        ListViewDataItem dataItem = sender as ListViewDataItem;

        CheckBox facetIsSelected = e.Item.FindControl("facetIsSelected") as CheckBox;

        if (facetIsSelected != null)
        {
            facetIsSelected.CheckedChanged += (s, args) =>
                {
                    if (facetIsSelected.Checked)
                    {
                        this.Controller.Select(facet);
                    }
                    else
                    {
                        this.Controller.Deselect(facet);
                    }
                };
        }
    }
}