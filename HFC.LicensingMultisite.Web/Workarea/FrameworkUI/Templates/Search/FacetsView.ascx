﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FacetsView.ascx.cs" Inherits="FrameworkUI_Templates_Search_FacetsView" %>

<ul class="facetGroups">
    <asp:ListView ID="facetGroups" runat="server" DataSource='<%# Eval("FacetGroups") %>'>
        <ItemTemplate>
            <li class="facetGroup">
                <h3 class="facetGroup-DisplayName"><%# Eval("DisplayName") %></h3>
                <ul class="facets">
                    <asp:ListView ID="facets" runat="server" OnItemDataBound="facets_OnItemDataBound" DataSource='<%# Eval("Facets") %>'>
                        <ItemTemplate>
                            <li class="facet">
                                <asp:CheckBox ID="facetIsSelected" runat="server" Checked='<%# Eval("IsSelected") %>' AutoPostBack="True" Text='<%# Eval("DisplayName") %>'></asp:CheckBox>
                                <span class="facet-Count">(<%# Eval("Count") %>)</span>
                            </li>
                        </ItemTemplate>
                    </asp:ListView>
                </ul>
            </li>
        </ItemTemplate>
    </asp:ListView>
</ul>