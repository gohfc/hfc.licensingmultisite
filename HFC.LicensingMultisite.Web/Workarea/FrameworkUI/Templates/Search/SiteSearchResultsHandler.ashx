<%@ WebHandler Language="C#" Class="Ektron.Cms.Framework.UI.Controls.Templates.SiteSearchResultsHandler" %>

namespace Ektron.Cms.Framework.UI.Controls.Templates
{
    using System;
    using System.Web;
    using Ektron.Newtonsoft.Json;
    using Ektron.Cms.Common;
    using Ektron.Cms.Search;
    using Ektron.Cms.Search.QueryStatistics;
    
    public class SiteSearchResultsHandler : IHttpHandler
    {

        private SearchStatistics result;
        HttpContext _context;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Buffer = false;

            _context = context;
            try
            {
                if (context.Request["searchresult"] != null)
                {
                    result = (SearchStatistics)JsonConvert.DeserializeObject(context.Request["searchresult"], typeof(SearchStatistics));

                    // _context.Response.Write("action=" + request.action + "\n");
                    switch (result.Action)
                    {
                        case "addquerysuggestion":
                            AddQuerySuggestion();
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Ektron.Cms.Search.QueryStatistics.JsonException ex = new Ektron.Cms.Search.QueryStatistics.JsonException();
                ex.Message = e.Message;
                if (e.InnerException != null) ex.InnerMessage = e.InnerException.Message;

                context.Response.Write(JsonConvert.SerializeObject(ex));
            }
            context.Response.End();

        }

        private void AddQuerySuggestion()
        {
            string quicklink = result.ResultItem.Url;

            //In our case requested page size and offset is same as that of Result.
            result.QueryInfo.RequestedResultOffset = result.ResultInfo.ResultsPageInfo.PageStartOffset;
            result.QueryInfo.RequestedResultSetSize = result.ResultInfo.ResultsPageInfo.PageSize;

            IRecordQueryStatisticsHelperService service = ObjectFactory.Get<IRecordQueryStatisticsHelperService>();
            result.UserWhoSelected = service.GetSearchUserInfo();
            
            result.ContentSourceData = service.GetContentSourceData();
            result.ResultItem.RelativeUrl = service.GetRelativeQuickLink(result.ResultItem.Url, result.ResultItem.SiteId);

            foreach (SearchResultItem item in result.UnselectedResultItems)
            {
                item.RelativeUrl = service.GetRelativeQuickLink(item.Url, item.SiteId);
            } 
            result.TimeOfSelection = DateTime.Now;

            result.CultureInfo = service.GetSearchCultureInfo();

            try
            {
                service.PublishStatistics(result);
            }
            catch {}
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}