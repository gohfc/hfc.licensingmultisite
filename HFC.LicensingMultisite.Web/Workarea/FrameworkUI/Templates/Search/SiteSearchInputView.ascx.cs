﻿namespace Ektron.Cms.Framework.UI.Controls.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using Ektron.Cms.Framework.UI.Controls.EktronUI;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Views;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Framework.Search;
    using Ektron.Cms.Search;

    public partial class SiteSearchInputView : BaseTemplate<ISearchView, SiteSearchController>
    {
        protected Controls.SiteSearchInputView View { get; private set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Controller.EnableAutoSuggest && this.Controller.EnableAutoComplete)
            {
                throw new Exception((string)this.GetLocalResourceObject("QueryPropositionErrorMsg"));
            }
            if (this.Controller.EnableAutoSuggest && this.Controller.AutoSuggestMaxCount > 0 ||
                this.Controller.EnableAutoComplete && this.Controller.AutoCompleteMaxCount > 0) 
                uxSearchText.SourceRequest += uxSearchText_OnSourceRequest;

            this.View = (Controls.SiteSearchInputView)this.NamingContainer.Parent;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (this.Visible)
            {
                ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();

                aspAdvancedSearchIcon.HRef = aspAdvancedSearchLink.HRef = cmsContextService.WorkareaPath + "/JavascriptRequired.aspx";
                aspAdvancedSearchLink.InnerText = GetLocalResourceObject("ToggleButtonText") as string; // meta:resourcekey="uxAdvancedSearchLinkResource1" 

                // Render sort properties drop down if more than one
                // sort property has been specified.

                if (this.View.SortProperties == null || this.View.SortProperties.Count < 2)
                {
                    aspSortProperties.Visible = false;
                    aspSortLabel.Visible = false;
                }
                else
                {
                    if (!IsPostBack || (this.aspSortProperties.SelectedIndex == -1))
                    {
                        // Render sort properties drop down if more than one
                        // sort property has been specified.
                        aspSortProperties.Visible = true;
                        aspSortLabel.Visible = true;
                        aspSortProperties.DataSource = this.View.SortProperties;
                        aspSortProperties.DataBind();
                    }
                }

                // create a package that will register the UI JS and CSS we need
                Package searchControlPackage = new Package()
                {
                    Components = new List<Component>()
                {
                    // Register JS Files
                    Packages.EktronCoreJS,
                    Packages.jQuery.jQueryUI.Position,
                    Packages.jQuery.Plugins.BGIframe,
                    Packages.jQuery.Plugins.ToTop,
                    UI.JavaScript.Create(cmsContextService.UIPath + "/js/Ektron/Controls/Ektron.Controls.Search.SiteSearch.js"),
                    Packages.jQuery.Plugins.BindReturnKey,
                    // Register CSS Files
                    Packages.Ektron.CssFrameworkBase,
                    UI.Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-search.css")
                }
                };

                searchControlPackage.Register(this);
            }
        }

        protected void uxBasicSearch_Click(object sender, EventArgs e)
        {
            if (this.View.SortProperties != null && this.View.SortProperties.Count > 0)
            {
                int propertyIndex = aspSortProperties.SelectedIndex >= 0
                    ? aspSortProperties.SelectedIndex
                    : 0;

                this.Controller.OrderBy = new SortableSearchPropertyCollection() { this.View.SortProperties[propertyIndex] };
            }

            this.Controller.Search(uxSearchText.Text);
        }

        protected void uxAdvancedSearch_Click(object sender, EventArgs e)
        {
            if (this.View.SortProperties != null && this.View.SortProperties.Count > 0)
            {
                int propertyIndex = aspSortProperties.SelectedIndex >= 0
                    ? aspSortProperties.SelectedIndex
                    : 0;

                this.Controller.OrderBy = new SortableSearchPropertyCollection() { this.View.SortProperties[propertyIndex] };
            }

            this.Controller.Search(uxWithAllWords.Text, uxWithoutWords.Text, uxAnyWords.Text, uxExactPhrase.Text);
        }

        protected void uxSearchText_OnSourceRequest(object sender, AutocompleteSourceRequestEventArgs e)
        {
            string partialSearchText = e.Text;
            string spellingSuggestion = String.Empty;
            if (partialSearchText.Trim().Length > 0)
            {
                if (this.Controller.EnableAutoComplete && this.Controller.AutoCompleteMaxCount > 0)
                {
                    IQueryProposition queryPropositionProvider = ObjectFactory.Get<IQueryProposition>();
                    QueryCompletionResponse response =
                                queryPropositionProvider.GetQueryCompletions(partialSearchText.Trim(),
                                                                             this.Controller.AutoCompleteMaxCount);
                    uxSearchText.SourceData.Clear();

                    if (response.QueryCompletions != null && response.QueryCompletions.Count > 0)
                    {
                        foreach (QueryCompletionTerm item in response.QueryCompletions)
                        {
                            AutocompleteData data = new AutocompleteData();
                            data.Label = data.Value = item.Completion;
                            uxSearchText.SourceData.Add(data);
                        }
                    }
                }
                else if (this.Controller.EnableAutoSuggest && this.Controller.AutoSuggestMaxCount > 0)
                {
                    IQueryProposition queryPropositionProvider = ObjectFactory.Get<IQueryProposition>();
                    QuerySuggestionResponse response =
                                queryPropositionProvider.GetQuerySuggestions(partialSearchText.Trim(),
                                                                             this.Controller.AutoSuggestMaxCount);
                    uxSearchText.SourceData.Clear();

                    if (response.QuerySuggestions != null && response.QuerySuggestions.Count > 0)
                    {
                        foreach (QuerySuggestionTerm item in response.QuerySuggestions)
                        {
                            AutocompleteData data = new AutocompleteData();
                            data.Label = data.Value = item.Suggestion;
                            uxSearchText.SourceData.Add(data);
                        }
                    }
                }
            }
        }
    }
}