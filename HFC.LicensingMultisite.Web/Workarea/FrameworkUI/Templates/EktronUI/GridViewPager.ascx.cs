﻿namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using System.Web.Script.Serialization;

    public partial class GridViewPager : System.Web.UI.UserControl, IPostBackEventHandler
    {
        public GridViewPager()
        {
            this.ID = "GridViewPager";
        }

        public string PostBackTrigger
        {
            get
            {
                ClientScriptManager clientScriptManager = this.Page.ClientScript;
                PostBackOptions options = new PostBackOptions(this);
                return clientScriptManager.GetPostBackEventReference(options).TrimEnd(new char[] { ';' }) + ";";
            }
        }

        private PagingInfo pagingInfo;
        public PagingInfo PagingInfo
        {
            get
            {
                if (this.pagingInfo == null)
                {
                    this.pagingInfo = this.GetGridView().EktronUIPagingInfo;
                }
                return this.pagingInfo;
            }
            set
            {
                this.pagingInfo = this.GetGridView().EktronUIPagingInfo;
            }
        }
        public string PagingInfoText
        {
            get
            {
                JavaScriptSerializer json = new JavaScriptSerializer();
                return json.Serialize(this.PagingInfo);
            }
        }

        private EktronUI.GridView GetGridView()
        {
            Control parent = this.Parent;
            do
            {
                if (parent is EktronUI.GridView)
                {
                    EktronUI.GridView gridView = parent as EktronUI.GridView;
                    break;
                }
                parent = parent.Parent;
            } while (!(parent is System.Web.UI.HtmlControls.HtmlForm));
            return parent as EktronUI.GridView;
        }

        public override void DataBind()
        {
            this.PagingInfo = this.GetGridView().EktronUIPagingInfo;

            uxInteraction.Visible = this.PagingInfo.TotalPages > 1;
            int endRow = (this.PagingInfo.StartRow + this.PagingInfo.RecordsPerPage) - 1;
            if (endRow > this.PagingInfo.TotalRecords)
            {
                endRow = this.PagingInfo.TotalRecords;
            }
            uxAdHocPageNumber.Text = (this.PagingInfo.CurrentPage).ToString();
            string startRow = endRow == 0 ? "0" : this.PagingInfo.StartRow.ToString();
            uxTotalEntriesText.Text = String.Format(this.GetLocalResourceObject("EntryTotalText") as String, startRow, endRow.ToString(), this.PagingInfo.TotalRecords.ToString());
            uxTotalPagesText.Text = String.Format(this.GetLocalResourceObject("TotalPagesText") as String, this.PagingInfo.TotalPages.ToString());
            uxTotalPagesText.Visible = true;

            this.uxFirst.Enabled = this.PagingInfo.CurrentPage > 1;
            this.uxPrevious.Enabled = this.PagingInfo.TotalPages > 1 && this.PagingInfo.CurrentPage > 1;
            this.uxAdHocPageNumber.Enabled = this.PagingInfo.TotalPages > 1;
            this.uxNext.Enabled = this.PagingInfo.TotalPages > 1 && this.PagingInfo.CurrentPage < this.PagingInfo.TotalPages;
            this.uxLast.Enabled = this.PagingInfo.CurrentPage < this.PagingInfo.TotalPages;

            base.DataBind();
        }

        protected void OnPagingCommand(object sender, CommandEventArgs e)
        {
            GridViewEktronUIThemePageChangedEventArgs eventArgs = new GridViewEktronUIThemePageChangedEventArgs();
            switch (e.CommandName)
            {
                case "First":
                    this.PagingInfo.CurrentPage = 1;
                    break;
                case "Previous":
                    this.PagingInfo.CurrentPage = this.PagingInfo.CurrentPage - 1;
                    break;
                case "Next":
                    this.PagingInfo.CurrentPage = this.PagingInfo.CurrentPage + 1;
                    break;
                case "Last":
                    this.PagingInfo.CurrentPage = this.PagingInfo.TotalPages;
                    break;
            }
            eventArgs.PagingInfo = this.PagingInfo;
            RaiseBubbleEvent(this, eventArgs as EventArgs);
        }

        public void RaisePostBackEvent(string eventArgument)
        {
            this.PagingInfo.CurrentPage = int.Parse(uxAdHocPageNumber.Text);
            GridViewEktronUIThemePageChangedEventArgs eventArgs = new GridViewEktronUIThemePageChangedEventArgs();
            eventArgs.PagingInfo = this.PagingInfo;
            RaiseBubbleEvent(this, eventArgs as EventArgs);
        }
    }
}