﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GridViewSelectListField.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.GridViewSelectListField" %>
<asp:MultiView ID="uxTemplate" runat="server" ActiveViewIndex="<%# this.View %>">
    <asp:View ID="uxHeaderTemplate" runat="server">
        <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible="<%# !String.IsNullOrEmpty(this.HeaderText) %>">
            <span class="ektron-ui-item-selector-header"><%= this.HeaderText %></span>
        </asp:PlaceHolder>
        <input type="hidden" name="<%= this.DirtyFieldsName %>" class="ektron-ui-gridview-column-data" value='<%= this.DirtyFieldsJson %>' />
        <input type="hidden" class="ektron-ui-item-selector-autoPostBack" value="<%= this.AutoPostBack.ToString().ToLower() %>" />
        <input type="hidden" class="ektron-ui-item-selector-uniqueId" value="<%= this.UniqueID %>" />
    </asp:View>
    <asp:View ID="uxItemTemplate" runat="server">
        <asp:ListView ID="uxSelectListView" runat="server" ItemPlaceholderID="aspItemPlaceholder" Visible="true">
            <LayoutTemplate>
                <asp:PlaceHolder runat="server">
                    <select class="ektron-ui-item-selector ektron-ui-gridview-selectlist" id="<%= this.SelectListClientID %>">
                </asp:PlaceHolder>
                    <asp:PlaceHolder ID="aspItemPlaceholder" runat="server" />
                </select>
            </LayoutTemplate>
            <ItemTemplate>
                <asp:PlaceHolder ID="itemPlaceholder" runat="server">
                    <option <%# (string)Eval(this.OptionValueDataField) == this.SelectedOptionValue ? "selected=\"selected\"" : String.Empty %> value='<%# Eval(this.OptionValueDataField) %>'><%# Eval(this.OptionTextDataField) %></option>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:ListView>
        <asp:PlaceHolder ID="aspContents" runat="server"></asp:PlaceHolder>
        <input type="hidden" name="<%= this.SelectListFieldName %>" class="ektron-ui-gridview-selectlistfield-data" value='<%= this.SelectListFieldValue %>' />
    </asp:View>
</asp:MultiView>

