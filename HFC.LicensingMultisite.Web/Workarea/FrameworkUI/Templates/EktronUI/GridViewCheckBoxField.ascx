﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GridViewCheckboxField.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.GridViewCheckBoxField" %>
<asp:MultiView ID="uxTemplate" runat="server" ActiveViewIndex="<%# this.View %>">
    <asp:View ID="uxHeaderTemplate" runat="server">
        <asp:PlaceHolder ID="uxHeaderCheckBox" runat="server" Visible="<%# this.HeaderFieldVisible %>">
            <input id="<%= this.HeaderFieldId %>" type="checkbox" name="ektron-ui-item-selector-header" <%= this.HeaderFieldToolTip %> <%= this.HeaderFieldEnabledText %> class="ektron-ui-item-selector-header ektron-ui-gridview-checkbox-header" />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="uxHeaderLabel" runat="server" Visible="<%# this.HeaderFieldVisible && !String.IsNullOrEmpty(this.HeaderText) %>">
            <label for="<%= this.HeaderFieldId %>"><%= this.HeaderText %></label>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible="<%# !this.HeaderFieldVisible %>">
            <%= this.HeaderText %>
        </asp:PlaceHolder>
        <input type="hidden" name="<%= this.DirtyFieldsName %>" class="ektron-ui-gridview-column-data" value='<%= this.DirtyFieldsJson %>' />
        <input type="hidden" class="ektron-ui-item-selector-autoPostBack" value="<%= this.AutoPostBack.ToString().ToLower() %>" />
        <input type="hidden" class="ektron-ui-item-selector-uniqueId" value="<%= this.UniqueID %>" />
    </asp:View>
    <asp:View ID="uxItemTemplate" runat="server">
        <input type="checkbox" id="<%= this.CheckboxFieldClientID %>" <%= this.ToolTip %> <%= this.EnabledText %> class="ektron-ui-item-selector ektron-ui-gridview-checkbox" <%= this.Checked ? " checked=\"checked\"" : String.Empty %> />
        <input type="hidden" name="<%= this.CheckboxFieldName %>" class="ektron-ui-gridview-checkbox-data" value='<%= this.CheckboxFieldValue %>' />
    </asp:View>
</asp:MultiView>