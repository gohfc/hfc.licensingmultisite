﻿namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;
    using Ektron.Cms.Interfaces.Context;

    /// <summary>
    /// The display markup and style for the Infotip server control.
    /// </summary>
    public partial class Infotip : TemplateBase<EktronUI.Infotip>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Infotip"/> class.
        /// </summary>
        public Infotip()
        {
            this.ID = "Infotip";
            this.Classes = new List<string>();
            this.IconClasses = new List<string>();
        }

        /// <summary>
        /// Gets the full class attribute for the outer tag of the control.
        /// </summary>
        public string ClassAttribute
        {
            get { return " class=\"" + string.Join(" ", this.Classes.ToArray()) + "\""; }
        }

        /// <summary>
        /// Gets or sets the classes to apply to the outer tag of the control.
        /// </summary>
        /// <value>
        /// The icon classes.
        /// </value>
        private List<string> Classes { get; set; }

        /// <summary>
        /// Gets or sets the classes to apply to the icon.
        /// </summary>
        /// <value>
        /// The icon classes.
        /// </value>
        private List<string> IconClasses { get; set; }

        /// <summary>
        /// Called when initializing.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnInitialize(object sender, EventArgs e)
        {
            // Do nothing
        }

        /// <summary>
        /// Called when when the control is loaded and ready.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnReady(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                // Set visual control-level classes
                this.Classes.Add("ektron-ui-control");
                this.Classes.Add(this.ControlContainer.ControlClass);
                if (!string.IsNullOrEmpty(this.ControlContainer.CssClass))
                {
                    this.Classes.Add(this.ControlContainer.CssClass);
                }

                // Set visual icon classes
                this.IconClasses.Add("ui-icon-info");
                this.IconClasses.Add("ektron-ui-sprite");
                this.IconClasses.Add("ektron-ui-sprite-information");
                this.IconClasses.Add(this.ControlContainer.IconClass);
                uxAnchor.CssClass = string.Join(" ", this.IconClasses.ToArray());
            }
        }

        /// <summary>
        /// Called to register styling resources.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnRegisterResources(object sender, EventArgs e)
        {
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();

            // Register any resources required for styling (vs. behavior/JS) here
            Package stylePackage = new Package()
            {
                Components = new List<Ektron.Cms.Framework.UI.Component>()
                {
                    Packages.jQuery.jQueryUI.ThemeRoller,
                    Packages.Ektron.CssFrameworkBase,
                    UI.Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-tooltip.css")
                }
            };

            stylePackage.Register(this);
        }
    }
}