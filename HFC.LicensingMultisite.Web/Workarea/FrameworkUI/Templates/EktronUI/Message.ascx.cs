﻿namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;
    using Ektron.Cms.Interfaces.Context;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;

    public partial class Message : TemplateBase<EktronUI.Message>
    {
        #region properties
        [Browsable(false), Bindable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public string classes {
            get {return " class=\"" + String.Join(" ", this.Classes.ToArray()) + "\"";}
        }

        [Browsable(false), Bindable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public string tooltip {
            get {
                if (!String.IsNullOrEmpty(this.ControlContainer.ToolTip)) {
                    return " title=\"" + this.ControlContainer.ToolTip + "\"";
                }
                return string.Empty;                
            }
        }
        #endregion

        #region members, enums and constants
        private List<string> Classes { get; set; }
        private List<string> IconClasses { get; set; }
        #endregion

        #region constructor
        public Message()
        {
            this.ID = "Message";
            this.Classes = new List<string>();
            this.IconClasses = new List<string>();
        }
        #endregion

        #region events

        protected override void OnInitialize(object sender, EventArgs e)
        {
            //do nothing
        }

        protected override void OnRegisterResources(object sender, EventArgs e)
        {
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();    
            // register any required Javascript or CSS resources here:
            Package interactionPackage = new Package()
            {
                Components = new List<Ektron.Cms.Framework.UI.Component>()
                {
                    Packages.jQuery.jQueryUI.ThemeRoller,
                    Packages.Ektron.CssFrameworkBase,
                    Css.Create(cmsContextService.UIPath + "/css/Ektron/Controls/ektron-ui-message.css")
                }
            };
            interactionPackage.Register(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            // if there are controls in the content template, this is the latest 
            // we can instantiate in the placeholder to ensure proper wiring of events
            if (string.IsNullOrEmpty(this.ControlContainer.Text) && this.ControlContainer.ContentTemplate != null)
            {
                this.ControlContainer.ContentTemplate.InstantiateIn(aspContentTemplateControl);
            }
        }

        protected override void OnReady (object sender, EventArgs e)
        {
            if (this.Visible)
            {
                // control classes
                this.Classes.Add("ektron-ui-control");
                this.Classes.Add("ui-corner-all");
                this.Classes.Add("ektron-ui-message");
                this.Classes.Add(this.ControlContainer.ControlClass);
                if (!String.IsNullOrEmpty(this.ControlContainer.CssClass))
                {
                    this.Classes.Add(this.ControlContainer.CssClass);
                }
                this.Classes.Add("ektron-ui-clearfix");

                // icon classes
                this.IconClasses.Add("ui-icon");
                this.IconClasses.Add("ektron-ui-sprite");
                this.IconClasses.Add(this.ControlContainer.IconClass);
                aspIcon.CssClass = String.Join(" ", this.IconClasses.ToArray());

                aspContentTemplateControl.DataBind();
                // for messages that simply set a Text value change, we need to create a MessageTextTemplate and assign it as the Content Template
                if (!string.IsNullOrEmpty(this.ControlContainer.Text) && this.ControlContainer.ContentTemplate != null)
                {
                    this.ControlContainer.ContentTemplate = new MessageTextTemplate(this.ControlContainer.Text); 
                    this.ControlContainer.ContentTemplate.InstantiateIn(aspContentTemplateControl);
                }
            }
        }
        #endregion
    }
}