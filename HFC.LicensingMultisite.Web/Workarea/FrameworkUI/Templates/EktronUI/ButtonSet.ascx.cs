namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    using System;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Core;
    using Ektron.Cms.Interfaces.Context;

    public partial class ButtonSet : TemplateBase<EktronUI.ButtonSet>
    {
        public ButtonSet()
        {
            this.ID = "ButtonSet";
        }
        
        protected override void OnInitialize(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.ControlContainer.CssClass))
            {
                this.ControlContainer.CssClass = "ektron-ui-buttonSet";
            }
            else
            {
                this.ControlContainer.CssClass = "ektron-ui-buttonSet " + this.ControlContainer.CssClass;
            }
        }

        protected override void OnRegisterResources(object sender, EventArgs e)
        {
            // register any required Javascript or CSS resources here:
            ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
            Ektron.Cms.Framework.UI.JavaScript.Register(this, cmsContextService.UIPath + "/js/Ektron/Controls/EktronUI/Ektron.Controls.EktronUI.ButtonSet.js", false);           
        }

        protected override void OnReady(object sender, EventArgs e)
        {
            
        }
    }
}