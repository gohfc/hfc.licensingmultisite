<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JavaScriptControlGroup.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.JavaScriptGroups.JavaScriptControlGroup" %>

        <script type="text/javascript"><!--//--><![CDATA[//><!--
        Ektron.ready(function(event, eventName){
            $ektron("<%= AssociatedTriggers %>").on("<%= AssociatedEvents %>", function(){
                //begin modules
                <asp:PlaceHolder ID="aspModulePlaceholder" runat="server" />
                // end modules
            });
        })
        //--><!]]>
        </script>