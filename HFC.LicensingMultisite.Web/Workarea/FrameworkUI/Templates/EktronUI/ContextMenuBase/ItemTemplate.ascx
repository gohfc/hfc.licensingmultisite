﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItemTemplate.ascx.cs"
	Inherits="Ektron.Cms.Framework.UI.Web.FrameworkUI.Templates.ContextMenuBase.ItemTemplate" %>

<li class="contextMenuBaseItem">
	<asp:HyperLink ID="link" runat="server" NavigateUrl='<%# Eval("NavigateUrl") != null ? Eval("NavigateUrl") : "javascript:" + Eval("OnClientClick") %>' Text='<%# Eval("Text")%>' Visible='<%# Eval("NavigateUrl") != null || Eval("OnClientClick") != null %>' />
	<asp:Label ID="sp" runat="server" Text='<%# Eval("Text")%>' Visible='<%# Eval("NavigateUrl") == null && Eval("OnClientClick") == null %>' />
    <ektronUI:PlaceHolderControl ID="itemPlaceholder" runat="server" />
</li>
