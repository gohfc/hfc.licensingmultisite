﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectionMenuTreeTemplate.ascx.cs" Inherits="Tree.TreeResources.Template.ServerRender.SelectionMenuTreeTemplate" %>
<div id="<%= ClientID %>" <%= CssManager %> >
    <asp:PlaceHolder ID="placeHolder" runat="server" />
    <ul id="<%= ClientID %>_TreeRootElement" class="ektron-ui-tree-root ektron-ui-clearfix" >
        <asp:PlaceHolder ID="childContainerPlaceHolder" runat="server" />
    </ul>
</div>