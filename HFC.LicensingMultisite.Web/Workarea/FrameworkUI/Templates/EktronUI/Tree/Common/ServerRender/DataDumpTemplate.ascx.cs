﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Ektron.Cms.Framework.UI;
using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Framework.UI.Tree;
using System.Web.UI.HtmlControls;
using System.Reflection;

namespace Tree.TreeResources.Template.ServerRender {
    public partial class DataDumpTemplate : TreeTemplateBase {

        public DataDumpTemplate() { }

        protected override PlaceHolder ControlPlaceHolder { get { return new PlaceHolder(); } }

        public override string SerializeData(Ektron.Cms.Framework.UI.Tree.TreeNodeCollection data, bool ajaxCallback) {
            DumpData(data, outerList);
            return null;
        }

        protected void DumpData(Ektron.Cms.Framework.UI.Tree.TreeNodeCollection data, HtmlGenericControl ctl) {
            foreach (var node in data) {
                var li = new HtmlGenericControl("li");
                ctl.Controls.Add(li);
                var span = new HtmlGenericControl("span");
                li.Controls.Add(span);
                span.Attributes.Add("class", "title");
                span.InnerText = node.Text + " [ID:" + node.Id + "]";
                DumpNodeData(node, ctl);
                if (node.Items != null && node.Items.Count > 0) {
                    var ul = new HtmlGenericControl("ul");
                    ctl.Controls.Add(ul);
                    DumpData(node.Items, ul);
                }
            }
        }

        protected void DumpNodeData(Ektron.Cms.Framework.UI.Tree.ITreeNode node, HtmlGenericControl ctl) {
            if (node == null) { return; }
            HtmlGenericControl li = null;
            Type type = node.GetType();
            foreach (PropertyInfo propInfo in type.GetProperties()) {
                var name = propInfo.Name;
                string strValue = string.Empty;
                var getter = propInfo.GetGetMethod(false);
                if (getter != null) {
                    var value = getter.Invoke(node, null);
                    if (value != null) {
                        strValue = value.ToString();
                    }
                }
                li = new HtmlGenericControl("li");
                ctl.Controls.Add(li);
                var span = new HtmlGenericControl("span");
                li.Controls.Add(span);
                span.Attributes.Add("class", "property");
                span.InnerText = " " + name + " = " + strValue;
            }
            if (li != null) {
                li.Controls.Add(new HtmlGenericControl("br"));
            }
        }

        protected override void OnPreRender(EventArgs e) {
            base.OnPreRender(e);
            if (this.Visible) {
                ICmsContextService cmsContext = ServiceFactory.CreateCmsContextService();
                if (base.RegisterCss) {
                    Ektron.Cms.Framework.UI.Css.Register(this, cmsContext.UIPath + "/css/Ektron/Controls/Tree/Common/commonTree.css");
                    Ektron.Cms.Framework.UI.Css.Register(this, cmsContext.UIPath + "/css/Ektron/Controls/Tree/Common/ektron-ui-dataDumpTree.css");
                }
            }
        }
    }
}