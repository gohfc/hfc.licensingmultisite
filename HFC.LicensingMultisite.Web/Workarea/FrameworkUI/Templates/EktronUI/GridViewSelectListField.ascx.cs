﻿namespace Ektron.Cms.Framework.UI.Controls.EktronUI.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Ektron.Cms.Framework.UI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI;
    using Ektron.Cms.Framework.UI.Controls.EktronUI.Widgets;
    using System.Web.Script.Serialization;
    using Ektron.Newtonsoft.Json;

    public partial class GridViewSelectListField : System.Web.UI.UserControl, ITemplate, IGridViewDataControlField, IPostBackEventHandler
    {
        //enum
        public enum RenderState
        {
            InitialPageLoad,
            PostBackWithDataBind,
            PostBackNoDataBind
        }

        //members
        private bool isDataBound;
        private List<GridViewDirtyField> dirtyFields;
        private List<GridViewDirtyField> currentFields;
        private List<GridViewSelectListOptionData> optionsData;
        private bool isCurrentFieldsDeserialized;

        //constructor
        public GridViewSelectListField()
        {
            this.ID = "SelectListField";
            this.isDataBound = false;
            this.optionsData = new List<GridViewSelectListOptionData>();
        }

        public RenderState State
        {
            get
            {
                RenderState state = RenderState.InitialPageLoad;
                if (this.Page.IsPostBack)
                {
                    state = this.isDataBound ? RenderState.PostBackWithDataBind : RenderState.PostBackNoDataBind;
                }
                return state;
            }
        }

        private List<GridViewDirtyField> CurrentFields
        {
            get
            {
                if (!this.isCurrentFieldsDeserialized)
                {
                    string currentFieldsJson = HttpContext.Current.Request.Form[this.SelectListFieldName];
                    if (!String.IsNullOrEmpty(currentFieldsJson))
                    {
                        this.currentFields = Ektron.Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridViewDirtyField>>("[" + currentFieldsJson + "]");
                    }
                    else
                    {
                        this.currentFields = new List<GridViewDirtyField>();
                    }
                    this.isCurrentFieldsDeserialized = true;
                }
                return this.currentFields;
            }
        }

        //properties
        public bool IsDataBound { get; set; }

        //header
        public string HeaderFieldId
        {
            get
            {
                string headerFieldId = String.Empty;
                if (ViewState["HeaderFieldId"] != null)
                {
                    headerFieldId = (string)ViewState["HeaderFieldId"];
                }
                return headerFieldId;
            }
            set
            {
                ViewState["HeaderFieldId"] = value;
            }
        }
        public string HeaderFieldToolTip
        {
            get
            {
                string headerFieldToolTip = String.Empty;
                if (ViewState["HeaderFieldToolTip"] != null)
                {
                    headerFieldToolTip = "title=\"" + (string)ViewState["HeaderFieldToolTip"] + "\"";
                }
                return headerFieldToolTip;
            }
            set
            {
                ViewState["HeaderFieldToolTip"] = value;
            }
        }
        public bool HeaderTextBeforeFieldVisible
        {
            get
            {
                bool headerTextBeforeFieldVisible = false;
                if (ViewState["HeaderTextBeforeFieldVisible"] != null)
                {
                    headerTextBeforeFieldVisible = (bool)ViewState["HeaderTextBeforeFieldVisible"];
                }
                return headerTextBeforeFieldVisible;
            }
            set
            {
                ViewState["HeaderTextBeforeFieldVisible"] = value;
            }
        }
        public bool HeaderTextAfterFieldVisible
        {
            get
            {
                bool headerTextAfterFieldVisible = false;
                if (ViewState["HeaderTextAfterFieldVisible"] != null)
                {
                    headerTextAfterFieldVisible = (bool)ViewState["HeaderTextAfterFieldVisible"];
                }
                return headerTextAfterFieldVisible;
            }
            set
            {
                ViewState["HeaderTextAfterFieldVisible"] = value;
            }
        }
        public bool HeaderFieldVisible
        {
            get
            {
                bool headerFieldVisible = false;
                if (ViewState["HeaderFieldVisible"] != null)
                {
                    headerFieldVisible = (bool)ViewState["HeaderFieldVisible"];
                }
                return headerFieldVisible;
            }
            set
            {
                ViewState["HeaderFieldVisible"] = value;
            }
        }
        public bool HeaderFieldEnabled
        {
            get
            {
                bool headerFieldEnabled = false;
                if (ViewState["HeaderFieldEnabled"] != null)
                {
                    headerFieldEnabled = (bool)ViewState["HeaderFieldEnabled"];
                }
                return headerFieldEnabled;
            }
            set
            {
                ViewState["HeaderFieldEnabled"] = value;
            }
        }
        public string HeaderFieldEnabledText
        {
            get
            {
                return this.HeaderFieldEnabled ? String.Empty : "disabled=\"disabled\"";
            }
        }
        public string HeaderText
        {
            get
            {
                string headerText = String.Empty;
                if (ViewState["HeaderText"] != null)
                {
                    headerText = (string)ViewState["HeaderText"];
                }
                return headerText;
            }
            set
            {
                ViewState["HeaderText"] = value;
            }
        }
        public bool AutoPostBack
        {
            get
            {
                bool autoPostBack = false;
                if (ViewState["AutoPostBack"] != null)
                {
                    autoPostBack = (bool)ViewState["AutoPostBack"];
                }
                return autoPostBack;
            }
            set
            {
                ViewState["AutoPostBack"] = value;
            }
        }
        public string ColumnId
        {
            get
            {
                string columnId = String.Empty;
                if (ViewState["ColumnId"] != null)
                {
                    columnId = (string)ViewState["ColumnId"];
                }
                return columnId;
            }
            set
            {
                ViewState["ColumnId"] = value;
            }
        }
        public string DirtyFieldsJson
        {
            get
            {
                string dirtyFieldsJson = String.Empty;
                switch (this.State)
                {
                    case RenderState.InitialPageLoad:
                        if (ViewState["DirtyFieldsJson"] != null)
                        {
                            dirtyFieldsJson = (string)ViewState["DirtyFieldsJson"];
                        }
                        break;
                    case RenderState.PostBackNoDataBind:
                        dirtyFieldsJson = HttpContext.Current.Request.Form[this.SelectListClientID];
                        if (String.IsNullOrEmpty(dirtyFieldsJson))
                        {
                            dirtyFieldsJson = (string)ViewState["DirtyFieldsJson"];
                        }
                        else
                        {
                            ViewState["DirtyFieldsJson"] = dirtyFieldsJson;
                        }
                        break;
                    case RenderState.PostBackWithDataBind:
                        if (ViewState["DirtyFieldsJson"] != null)
                        {
                            dirtyFieldsJson = (string)ViewState["DirtyFieldsJson"];
                        }
                        break;
                }
                return dirtyFieldsJson;
            }
            set
            {
                ViewState["DirtyFieldsJson"] = value;
            }
        }
        public string DirtyFieldsName
        {
            get
            {
                string dirtyFieldsName = String.Empty;
                if (ViewState["DirtyFieldsName"] != null)
                {
                    dirtyFieldsName = (string)ViewState["DirtyFieldsName"];
                }
                return dirtyFieldsName;
            }
            set
            {
                ViewState["DirtyFieldsName"] = value;
            }
        }

        //helpers
        public DataControlRowType RowType
        {
            get
            {
                DataControlRowType rowType = DataControlRowType.DataRow;
                if (ViewState["RowType"] != null)
                {
                    rowType = (DataControlRowType)ViewState["RowType"];
                }
                return rowType;
            }
            set
            {
                ViewState["RowType"] = value;
            }
        }
        public int View
        {
            get
            {
                return this.RowType == DataControlRowType.Header ? 0 : 1;
            }
        }

        //datarow - SelectList
        public string ToolTip
        {
            get
            {
                string toolTip = String.Empty;
                if (ViewState["ToolTip"] != null)
                {
                    toolTip = "title=\"" + (string)ViewState["ToolTip"] + "\"";
                }
                return toolTip;
            }
            set
            {
                ViewState["HeaderToolTip"] = value;
            }
        }
        public bool Enabled
        {
            get
            {
                bool enabled = false;
                if (ViewState["Enabled"] != null)
                {
                    enabled = (bool)ViewState["Enabled"];
                }
                return enabled;
            }
            set
            {
                ViewState["Enabled"] = value;
            }
        }

        public string OptionText
        {
            get
            {
                return (ViewState["OptionText"] == null) ? string.Empty : (string)ViewState["OptionText"];
            }
            set
            {
                ViewState["OptionText"] = value;
            }
        }

        public string OptionValue
        {
            get
            {
                return (ViewState["OptionValue"] == null) ? string.Empty : (string)ViewState["OptionValue"];
            }
            set
            {
                ViewState["OptionValue"] = value;
            }
        }

        public bool Selected
        {
            get
            {
                bool isSelected = false;
                switch (this.State)
                {
                    case RenderState.InitialPageLoad:
                        if (ViewState["Selected"] != null)
                        {
                            isSelected = (bool)ViewState["Selected"];
                        }
                        break;
                    case RenderState.PostBackNoDataBind:
                        bool isDirty = false;
                        this.CurrentFields.FindAll(x => x.FieldClientID == this.SelectListClientID).ForEach(delegate(GridViewDirtyField currentField)
                        {
                            isDirty = true;
                            if (currentField.DirtyState == currentField.OriginalState || String.IsNullOrEmpty(currentField.DirtyState))
                            {
                                //set to original state
                                ViewState["Selected"] = isSelected = (currentField.OriginalState == "selected") ? true : false;
                            }
                            else
                            {
                                //set to dirty state
                                ViewState["Selected"] = isSelected = (currentField.DirtyState == "selected") ? true : false;
                            }
                        });
                        if (!isDirty)
                        {
                            //field is not dirty - set to original value
                            if (ViewState["Selected"] != null)
                            {
                                isSelected = (bool)ViewState["Selected"];
                            }
                        }
                        break;
                    case RenderState.PostBackWithDataBind:
                        if (ViewState["Selected"] != null)
                        {
                            isSelected = (bool)ViewState["Selected"];
                        }
                        break;
                }

                return isSelected;
            }
            set
            {
                ViewState["Selected"] = value;
            }
        }
        public string SelectListClientID
        {
            get
            {
                string SelectLisFieldClientID = String.Empty;
                if (ViewState["SelectListFieldClientID"] != null)
                {
                    SelectLisFieldClientID = (string)ViewState["SelectListFieldClientID"];
                }
                return SelectLisFieldClientID;
            }
            set
            {
                ViewState["SelectListFieldClientID"] = value;
            }
        }
        public string SelectListFieldName
        {
            get
            {
                string SelectLisFieldName = String.Empty;
                if (ViewState["SelectListFieldName"] != null)
                {
                    SelectLisFieldName = (string)ViewState["SelectListFieldName"];
                }
                return SelectLisFieldName;
            }
            set
            {
                ViewState["SelectListFieldName"] = value;
            }
        }
        public string SelectListFieldValue
        {
            get
            {
                string SelectListFieldValue = String.Empty;
                switch (this.State)
                {
                    case RenderState.InitialPageLoad:
                        if (ViewState["SelectListFieldValue"] != null)
                        {
                            SelectListFieldValue = (string)ViewState["SelectListFieldValue"];
                        }
                        break;
                    case RenderState.PostBackNoDataBind:
                        this.CurrentFields.FindAll(x => x.FieldClientID == this.SelectListClientID).ForEach(delegate(GridViewDirtyField currentField)
                        {
                            JavaScriptSerializer json = new JavaScriptSerializer();
                            SelectListFieldValue = json.Serialize(currentField);
                        });
                        if (String.IsNullOrEmpty(SelectListFieldValue))
                        {
                            SelectListFieldValue = (string)ViewState["SelectListFieldValue"];
                        }
                        else
                        {
                            ViewState["SelectListFieldValue"] = SelectListFieldValue;
                        }
                        break;
                    case RenderState.PostBackWithDataBind:
                        if (ViewState["SelectListFieldValue"] != null)
                        {
                            SelectListFieldValue = (string)ViewState["SelectListFieldValue"];
                        }
                        break;
                }
                return SelectListFieldValue;
            }
            set
            {
                ViewState["SelectListFieldValue"] = value;
            }
        }
        public string EnabledText
        {
            get
            {
                return this.Enabled ? String.Empty : "disabled=\"disabled\"";
            }
        }

        public string OptionTextDataField
        {
            get
            {
                string optionTextDataField = String.Empty;
                if (ViewState["OptionTextDataField"] != null)
                {
                    optionTextDataField = (string)ViewState["OptionTextDataField"];
                }
                return optionTextDataField;
            }
            set
            {
                ViewState["OptionTextDataField"] = value;
            }
        }
        public string OptionValueDataField
        {
            get
            {
                string optionValueDataField = String.Empty;
                if (ViewState["OptionValueDataField"] != null)
                {
                    optionValueDataField = (string)ViewState["OptionValueDataField"];
                }
                return optionValueDataField;
            }
            set
            {
                ViewState["OptionValueDataField"] = value;
            }
        }
        public string SelectedOptionValue
        {
            get
            {
                string selectedOptionValue = String.Empty;
                if (ViewState["SelectedOptionValue"] != null)
                {
                    selectedOptionValue = (string)ViewState["SelectedOptionValue"];
                }
                return selectedOptionValue;
            }
            set
            {
                ViewState["SelectedOptionValue"] = value;
            }
        }
        //events
        protected override void OnPreRender(EventArgs e)
        {
            if (this.Visible)
            {
                //register any packages necessary here.

                Packages.Ektron.Controls.EktronUI.GridView.SelectListField.Register(this);
                if (!this.isDataBound)
                {
                    this.DataBind();
                }
            }
            base.OnPreRender(e);
        }
        public void RowDataBound(GridViewDataControlFieldEventArgs e)
        {
            var FieldArgs = (e as GridViewSelectListFieldEventArgs);

            this.SelectListClientID = e.FieldClientID;
            this.SelectListFieldName = e.FieldName;
            this.SelectListFieldValue = e.FieldValue;
            this.ToolTip = e.ToolTip;

            this.Enabled = e.Enabled;

            this.OptionTextDataField = FieldArgs.OptionTextDataField;
            this.OptionValueDataField = FieldArgs.OptionValueDataField;
            this.SelectedOptionValue = FieldArgs.SelectedOptionValue;


            this.uxSelectListView.DataSource = FieldArgs.Options;
            this.DataBind();
            this.isDataBound = true;


        }
        public void SetDirtyFields(List<GridViewDirtyField> dirtyFields, string dirtyFieldsName)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            this.DirtyFieldsJson = json.Serialize(dirtyFields);
            this.DirtyFieldsName = dirtyFieldsName;
        }

        //methods
        public void InstantiateIn(Control container)
        {
            container.Controls.Add(this);
        }

        public void RaisePostBackEvent(string eventArgument)
        {
            GridViewSelectListFieldIndexChangedEventArgs eventArgs = new GridViewSelectListFieldIndexChangedEventArgs();
            eventArgs.ColumnId = this.ColumnId;
            eventArgs.DirtyFields = Ektron.Newtonsoft.Json.JsonConvert.DeserializeObject<List<GridViewDirtyField>>(eventArgument);
            RaiseBubbleEvent(this, eventArgs as EventArgs);
        }
    }
}