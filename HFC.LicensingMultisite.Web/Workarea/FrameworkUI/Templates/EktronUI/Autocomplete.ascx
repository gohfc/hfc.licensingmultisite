﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Autocomplete.ascx.cs" Inherits="Ektron.Cms.Framework.UI.Controls.EktronUI.Templates.Autocomplete" %>
<ektronUI:Css ID="uxAutocompleteStyle" runat="server" Path="{UIPath}/css/Ektron/Controls/ektron-ui-autocomplete.css"></ektronUI:Css>
<span id="<%= this.ControlContainer.ClientID %>" class="ektron-ui-control ektron-ui-autocomplete">
    <ektronUI:TextField ID="uxAutocompleteTextBox" runat="server" CssClass="key" />
</span>