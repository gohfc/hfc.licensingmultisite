﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContentViewTemplate.ascx.cs" Inherits="FrameworkUI_Templates_Content_ContentViewTemplate" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Ektron.Cms.Framework.UI.Models" %>
<%@ Import Namespace="System.Linq" %>

            <% if (Model.HasError())
	            {%>
	            <div class="class="ektron-ui-error">
	            <%#  ViewBag.Message %>
	            </div>
            <% } %>

	        <%if (Model.ContentList.First() != null)
		    { %>	
                <ektron:EditorsMenu ID="uxEditorsMenu" runat="server" ObjectId="<%# Model.ContentList.First().Id %>" DisplayType="Content">
                    <asp:PlaceHolder runat="server">
                        <div id="<%# this.ClientID %>" class="ektron-ui-content ektron-ui-content-item ektron-ui-control ">
                            <% if (!String.IsNullOrEmpty(ViewBag.Message))
	                        {%>
	                            <div id="messagebox">
	                                <%#  ViewBag.Message %>
	                            </div>
                            <% } %>	
                            <h1><%# Model.ContentList.First().Title %></h1>
                            <hr />
                            <h5>By <a href="#?userid=<%# Model.ContentList.First().UserId %>"><%# Model.ContentList.First().EditorFirstName%> <%# Model.ContentList.First().EditorLastName%></a>  on <%# Model.ContentList.First().DateCreated.ToLongDateString()%></h5>
		                    <div class="ektron-ui-content-html"><%# Model.ContentList.First().Html%></div>
                        </div>
                    </asp:PlaceHolder>
                </ektron:EditorsMenu>
	        <% } %>

