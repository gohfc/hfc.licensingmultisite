﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContentListViewTemplate.ascx.cs" Inherits="FrameworkUI_Templates_Content_ContentListViewTemplate" %>
<%@ Import Namespace="Ektron.Cms" %>
<%@ Import Namespace="System.Linq" %>

<% if (Model != null && Model.ContentList.Any()) { %>
<div class="ektron-ui-content-list ektron-ui-content ektron-ui-control">
	<ul>	
		<% foreach (ContentData model in Model.ContentList){ %>
            <li>
                <h3><a href='<%= model.Quicklink %>'><%= model.Title %></a></h3>
                <h5> <%= model.DateCreated.ToLongDateString() %></h5>
		        <div class="ektron-ui-content-summary"><%= model.Teaser %></div>
            </li>
        <% } %>
	</ul>
</div>
<% } %>