﻿using System;
using System.Collections.Generic;
using Ektron.Cms;
using Ektron.Cms.Framework.UI.Controllers;
using Ektron.Cms.Framework.UI.Controls.MVC;
using Ektron.Cms.Framework.UI.Models;

public partial class FrameworkUI_Templates_Content_ContentListViewTemplate : BaseTemplate<IContentController, ContentModel>
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Ektron.Cms.Framework.UI.Css.Register(Page, Ektron.Cms.Framework.UI.Css.UIPath + "/css/Ektron/Controls/ektron-ui-Content-list.css");
        Ektron.Cms.Framework.UI.Packages.EktronCoreJS.Register(this);
    }
}