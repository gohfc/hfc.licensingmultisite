﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuViewItemTemplate.ascx.cs" Inherits="Workarea_FrameworkUI_Templates_Menu_MenuViewItemTemplate" %>
<li class='<%# Convert.ToString(Eval("Type")).ToLower() == "submenu"? "subnav":"menuitem" %>'>
    <asp:HyperLink ID="nodeLink" runat="server" Text='<%# Eval("Text") %>' expanded='<%# Eval("Expanded") %>'
        class='<%# (Boolean.Parse(Eval("Expanded").ToString())) ? @"expand" : @"collapse" %>'
        NavigateUrl='<%# Eval("NavigateUrl") %>' itemselected='<%# Eval("Selected") %>'  Target='<%# Eval("Target") %>'/>
    <ektronUI:PlaceHolderControl ID="itemPlaceholder" runat="server" />
</li>

