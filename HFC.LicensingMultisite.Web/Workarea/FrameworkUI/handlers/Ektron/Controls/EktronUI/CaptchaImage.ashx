﻿<%@ WebHandler Language="C#" Class="captchaImage" %>

using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Web.SessionState;
using Ektron.Cms;
using Ektron.Cms.Interfaces.Context;
using Ektron.Cms.Framework.UI;

using Ektron.Cms.Common;

public class captchaImage : IHttpHandler, IRequiresSessionState
{
    public string captchaValue = String.Empty;
    string result = "false";
    int width = 250;
    int height = 45;
    System.Drawing.Color backgroundColor = Color.White;
    System.Drawing.Color fontColor = Color.Black;
    string fontFamily = "GenericSansSerif";    
    int codeLength = 5;
    string uniqueId="";
    protected ICmsContextService cmsContextService = ServiceFactory.CreateCmsContextService();
    private Random random = new Random();
    private Bitmap bitImage;
    private CaptchaType codeType;

    public enum CaptchaType
    {
        Alphabet,
        Numeric,
        AlphaNumeric
    }
    public void ProcessRequest(HttpContext context)
    {

        string action = "refresh";
        if ((context.Request["action"] != null) && (context.Request["action"] != string.Empty))
        {
            action = context.Request["action"].ToString();
        }
        switch (action)
        {
            case "play":
                CreateAudioFile(context);
                break;
            case "delete":
                DeleteAudioFile(context);
                break;
            case "submit":
                SubmitCaptcha(context);
                break;
            default:
                CreateCaptcha(context);
                break;
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    
   
    /// <summary>
    /// Create captcha image.
    /// </summary>
    /// <param name="context">current http context</param>
    private void CreateCaptcha(HttpContext context)
    {
        
        if ((context.Request.QueryString["bc"] != null) && (context.Request.QueryString["bc"] != string.Empty))
        {
            backgroundColor = Color.FromName(context.Request.QueryString["bc"].ToString());
        }               
        else
        {
            backgroundColor = Color.White;
        }

        if ((context.Request.QueryString["fc"] != null) && (context.Request.QueryString["fc"] != string.Empty))
        {
            fontColor = Color.FromName(context.Request.QueryString["fc"].ToString());
        }
        else
        {
            fontColor = Color.Black;
        }

        if ((context.Request.QueryString["ff"] != null) && (context.Request.QueryString["ff"] != string.Empty))
        {
            fontFamily = context.Request.QueryString["ff"].ToString();
        }
        else
        {
            fontFamily = "GenericSansSerif";
        }

        if ((context.Request.QueryString["cl"] != null) && (context.Request.QueryString["cl"] != string.Empty))
        {
            int.TryParse(context.Request.QueryString["cl"], out codeLength);
            if (codeLength < 5)
            {
                codeLength = 5;
            }
            if (codeLength > 12)
            {
                codeLength = 12;
            }
        }
        else
        {
            codeLength = 5;
        }
        if ((context.Request.QueryString["codeType"] != null) && (context.Request.QueryString["codeType"] != string.Empty))
        {
            this.codeType = (CaptchaType)int.Parse((context.Request.QueryString["codeType"].ToString()));
        }
        if ((context.Request.QueryString["unique"] != null) && (context.Request.QueryString["unique"] != string.Empty))
        {
            uniqueId = context.Request.QueryString["unique"].ToString();
        }
        
        this.captchaValue = GenerateRandomCode(codeType, codeLength);        
        context.Session.Add("EkCaptcha_" + uniqueId, this.captchaValue);
        GenerateImage();
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Clear();
        context.Response.ContentType = "image/jpeg";
        bitImage.Save(context.Response.OutputStream, ImageFormat.Jpeg);
        bitImage.Dispose();
    }

    /// <summary>
    /// Create an audio file for captcha.
    /// </summary>
    /// <param name="context">current http context</param>
    private void CreateAudioFile(HttpContext context)
    {
        if ((context.Request["unique"] != null) && (context.Request["unique"] != string.Empty))
        {
            uniqueId = context.Request["unique"].ToString();
        }
        if (context.Session["EkCaptcha_" + uniqueId] != null && !string.IsNullOrEmpty(context.Session["EkCaptcha_" + uniqueId].ToString()))
        {
            captchaValue = context.Session["EkCaptcha_" + uniqueId].ToString();
        }
        char[] inputChars = captchaValue.ToCharArray();
        cmsContextService = ServiceFactory.CreateCmsContextService();
        int bufSize = 1024 * 64;
        byte[] buf = new byte[bufSize];
        string OutputFileName = context.Server.MapPath(cmsContextService.UIPath + "/Templates/EktronUI/Captcha/soundmanager/en-us/" + captchaValue + ".mp3");
        if (System.IO.File.Exists(OutputFileName))
            System.IO.File.Delete(OutputFileName);
        using (FileStream outFile = new FileStream(OutputFileName, FileMode.CreateNew, FileAccess.Write, FileShare.None, bufSize))
        {
            foreach (char c in inputChars)
            {
                using (FileStream _file = new FileStream(context.Server.MapPath(cmsContextService.UIPath + "/Templates/EktronUI/Captcha/soundmanager/en-us/" + c + ".mp3"), FileMode.Open, FileAccess.Read, FileShare.Read, bufSize))
                {
                    int br = 0;
                    while ((br = _file.Read(buf, 0, buf.Length)) > 0)
                    {
                        outFile.Write(buf, 0, br);
                    }
                }
            }
        }
        context.Response.Write(captchaValue);
    }

    /// <summary>
    /// Deletes an audio file once it's played.
    /// </summary>
    /// <param name="context">current http context.</param>
    private void DeleteAudioFile(HttpContext context)
    {
        if ((context.Request["unique"] != null) && (context.Request["unique"] != string.Empty))
        {
            uniqueId = context.Request["unique"].ToString();
        }
        if (context.Session["EkCaptcha_" + uniqueId] != null && !string.IsNullOrEmpty(context.Session["EkCaptcha_" + uniqueId].ToString()))
        {
            captchaValue = context.Session["EkCaptcha_" + uniqueId].ToString();
        }        
        cmsContextService = ServiceFactory.CreateCmsContextService();
        string OutputFileName = context.Server.MapPath(cmsContextService.UIPath + "/Templates/EktronUI/Captcha/soundmanager/en-us/" + captchaValue + ".mp3");
        if (System.IO.File.Exists(OutputFileName))
            System.IO.File.Delete(OutputFileName);
    }

    /// <summary>
    /// Validates the user input with captcha code
    /// </summary>
    /// <param name="context">current http context.</param>
    private void SubmitCaptcha(HttpContext context)
    {
        if ((context.Request["captchaCode"] != null) && (context.Request["captchaCode"] != string.Empty))
        {
            captchaValue = context.Request["captchaCode"].ToString();

            if ((context.Request["unique"] != null) && (context.Request["unique"] != string.Empty))
            {
                uniqueId = context.Request["unique"].ToString();
            }
            if (context.Session["EkCaptcha_" + uniqueId] != null && captchaValue.Equals(context.Session["EkCaptcha_" + uniqueId].ToString()))
            {
                result = "true";
            }
            context.Response.Write(result);
        }
    }
   
    ///// <summary>
    ///// Creates the bitmap image.
    ///// </summary>
    private void GenerateImage()
    {
        // Create a new 32-bit bitmap image.
        Bitmap bitmap = new Bitmap(this.width, this.height, PixelFormat.Format32bppArgb);


        // Create a graphics object for drawing.
        Graphics g = Graphics.FromImage(bitmap);
        g.SmoothingMode = SmoothingMode.AntiAlias;
        Rectangle rect = new Rectangle(0, 0, this.width, this.height);

        // CHANGE THE BACKGROUND COLORS HERE.
        Color clrb1 = this.backgroundColor;
        //HatchBrush hatchBrush = new HatchBrush(HatchStyle.SmallConfetti, Color.LightGray, Color.White);        
        HatchBrush hatchBrush = new HatchBrush(HatchStyle.SmallConfetti, clrb1, clrb1);
        g.FillRectangle(hatchBrush, rect);

        // Set up the text font.
        SizeF size;
        float fontSize = rect.Height + 1;
        System.Drawing.Font font;
        // Adjust the font size until the text fits within the image.
        do
        {
            fontSize--;
            font = new System.Drawing.Font(this.fontFamily, fontSize, FontStyle.Bold);
            //font = new System.Drawing.Font(this.fontFamily, fontSize, (FontStyle)new Random().Next(0, 8));
            size = g.MeasureString(this.captchaValue, font);
        } while (size.Width > rect.Width);

        // Set up the text format.
        StringFormat format = new StringFormat();
        format.Alignment = StringAlignment.Center;
        format.LineAlignment = StringAlignment.Center;

        // Create a path using the text and warp it randomly.
        GraphicsPath path = new GraphicsPath();
        path.AddString(this.captchaValue, font.FontFamily, (int)font.Style, font.Size, rect, format);
        float v = 4F;
        PointF[] points =
            {
                new PointF(this.random.Next(rect.Width) / v, this.random.Next(rect.Height) / v),
                new PointF(rect.Width - this.random.Next(rect.Width) / v, this.random.Next(rect.Height) / v),
                new PointF(this.random.Next(rect.Width) / v, rect.Height - this.random.Next(rect.Height) / v),
                new PointF(rect.Width - this.random.Next(rect.Width) / v, rect.Height - this.random.Next(rect.Height) / v)
            };
        Matrix matrix = new Matrix();
        matrix.Translate(0F, 0F);
        path.Warp(points, rect, matrix, WarpMode.Perspective, 0F);

        //CHANGE THE TEXT COLORS HERE
        Color clrt1 = this.fontColor;

        //hatchBrush = new HatchBrush(HatchStyle.LargeConfetti, Color.LightGray, Color.DarkGray);
        hatchBrush = new HatchBrush(HatchStyle.LargeConfetti, clrt1, clrt1);
        g.FillPath(hatchBrush, path);

        // Add some random noise.
        int m = Math.Max(rect.Width, rect.Height);
        for (int i = 0; i < (int)(rect.Width * rect.Height / 30F); i++)
        {
            int x = this.random.Next(rect.Width);
            int y = this.random.Next(rect.Height);
            int w = this.random.Next(m / 50);
            int h = this.random.Next(m / 50);
            g.FillEllipse(hatchBrush, x, y, w, h);
        }

        // Clean up.
        font.Dispose();
        hatchBrush.Dispose();
        g.Dispose();

        // Set the image.
        this.bitImage = bitmap;
    }
    /// <summary>
    /// Generate random code.
    /// </summary>
    /// <param name="ctype"></param>
    /// <param name="numberOfChars"></param>
    /// <returns></returns>
    private static string GenerateRandomCode(CaptchaType ctype, int numberOfChars)
    {
        string s = "";
        string alphas = "ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghjklmnpqrstuvwxyz"; //if you want to add other charaters add it here - zero and O are removed to avoid confusion
        string alphanumeric = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghjklmnpqrstuvwxyz";
        Random rnd = new Random();

        for (int i = 0; i < numberOfChars; i++)
        {
            if (ctype == CaptchaType.Numeric)
            {
                s = String.Concat(s, rnd.Next(10).ToString());
            }
            else if (ctype == CaptchaType.Alphabet)
            {
                s = String.Concat(s, alphas.Substring(rnd.Next(48), 1));
            }
            else
            {
                s = String.Concat(s, alphanumeric.Substring(rnd.Next(57), 1));
            }

        }
        return s;
    }
  
}