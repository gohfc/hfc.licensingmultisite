﻿if ("undefined" == typeof (Ektron)) { Ektron = {}; }
if ("undefined" == typeof (Ektron.Controls)) { Ektron.Controls = {}; }
if ("undefined" == typeof (Ektron.Controls.Search)) { Ektron.Controls.Search = {}; }
if ("undefined" == typeof (Ektron.Controls.Search.SiteSearch.QueryStatistics)) {
    Ektron.Controls.Search.SiteSearch.QueryStatistics = {
        //methods
        addQuerySuggestion: function (clickedItem, id) {
            var searchResultsControl = $ektron("#" + id);
            var parentResult = $ektron(clickedItem).closest(".result");
            var itemDataField = parentResult.find("input[type='hidden']");

            var handler = searchResultsControl.find(".SiteSearchResultsHandler");
            var lblHandlerPath = $ektron(handler.find("input:hidden"));

            //debugger;
            var divSearchQueryInfo = searchResultsControl.find(".SearchQueryInfo");
            var hdnSearchQueryInfo = $ektron(divSearchQueryInfo.find("input:hidden"));
            var searchQueryInfo;
            if (hdnSearchQueryInfo.length > 0)
                searchQueryInfo = $ektron.parseJSON(hdnSearchQueryInfo.val());

            var divSearchResultInfo = searchResultsControl.find(".SearchResultInfo");
            var hdnSearchResultInfo = $ektron(divSearchResultInfo.find("input:hidden"));
            var searchResultInfo;
            if (hdnSearchResultInfo.length > 0)
                searchResultInfo = $ektron.parseJSON(hdnSearchResultInfo.val());

            var searchUrl = searchResultsControl.data("searchUrl");
            
            var searchResultItem;
            if (clickedItem.length > 0 && itemDataField.length > 0) {
                // deserialize the itemDataField's value and capture it
                searchResultItem = $ektron.parseJSON(itemDataField.val());

                var unselectedSearchResultItems = new Array();
                var unselectedSearchResultItem;
                var unselectedSearchResultlistItems = searchResultsControl.find("div.section.results li.result");
                if (unselectedSearchResultlistItems.length > 0) {
                    unselectedSearchResultlistItems.each(function (i) {
                        unselectedSearchResultItem = $ektron(this).find("input[type='hidden']")
                        //Ensure clickItem is not getting added again.
                        if (unselectedSearchResultItem.length > 0 && unselectedSearchResultItem[0].id != itemDataField[0].id) {
                            var unselectedSearchResultItem = $ektron.parseJSON(unselectedSearchResultItem.val());
                            unselectedSearchResultItems.push(unselectedSearchResultItem);
                        }
                    });
                }
            }

            if ((lblHandlerPath.length > 0 && lblHandlerPath.val().length > 0) && (searchResultItem.Url.length > 0)) {
                $ektron.ajax(
                {
                    type: "POST",
                    cache: false,
                    async: true,
                    url: lblHandlerPath.val(),
                    data: { "searchresult": Ektron.Controls.Search.SiteSearch.QueryStatistics.createSearchResultObj("addquerysuggestion", searchQueryInfo, searchResultInfo, searchResultItem, unselectedSearchResultItems, searchUrl.ClickSourceUrl) },
                    success: function (msg) {
                        //We just log exceptions in this call, no errors (if any) are displayed in UI.
                        return true;
                    }
                });
            }
        },
        createSearchResultObj: function (action, searchQueryInfo, searchResultInfo, searchResultItem, unselectedSearchResultItems, clickSourceUrl) {
            //debugger;
            result = {
                "Action": action,
                "QueryInfo": searchQueryInfo,
                "ResultInfo": searchResultInfo,
                "ResultItem": searchResultItem,
                "UnselectedResultItems": unselectedSearchResultItems,
                "SelectionSourceUrl": clickSourceUrl
            };
            return Ektron.JSON.stringify(result);
        }
    };
}