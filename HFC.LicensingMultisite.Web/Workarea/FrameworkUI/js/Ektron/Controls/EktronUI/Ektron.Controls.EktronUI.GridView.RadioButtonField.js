﻿$ektron.widget("ektron.gridview", $ektron.extend({}, $ektron.ektron.gridview.prototype, {
    RadioButtonField: {
        _bindEvents: function (instance) {
            instance.find("tbody td > input.ektron-ui-gridview-radioButton").change(function () {
                //get changed radio
                var radio = $(this);

                //loop through all radios in column and if one is current marked as "checked", mark it as "unchecked"
                var cell = radio.closest("td");
                var columnIndex = cell.parent("tr").children().index(cell) + 1;
                var selector = "tbody td:nth-child(" + columnIndex + ") input:radio:not(:disabled)";
                instance.find(selector).each(function () {
                    var radio = $(this);
                    var input = radio.next();
                    var data = Ektron.JSON.parse(input.val());
                    if (data.DirtyState == "checked") {
                        data.DirtyState = "unchecked";
                        input.val(Ektron.JSON.stringify(data));
                    }
                });

                //set data for changed radio
                var input = radio.next();
                var data = Ektron.JSON.parse(input.val());
                data.DirtyState = "checked";
                input.val(Ektron.JSON.stringify(data));

                //if autopostback is true, postback
                var autoPostBackSelector = "thead th:nth-child(" + columnIndex + ") input.ektron-ui-item-selector-autoPostBack";
                var autoPostBack = $(autoPostBackSelector);
                var uniqueIdSelector = "thead th:nth-child(" + columnIndex + ") input.ektron-ui-item-selector-uniqueId";
                var uniqueId = $(uniqueIdSelector);
                if (autoPostBack.val() == "true") {
                    __doPostBack(uniqueId.val(), Ektron.JSON.stringify(data));
                }
            });
        },
        _init: function (instance) {
            this._bindEvents(instance);
            this.Header._init(instance);
        },
        Header: {
            RadioButton: {
                _bindEvents: function (instance, RadioButton) {
                    RadioButton.change(function () {
                    });
                },
                _init: function (instance) {
                    var RadioButton = this;
                    instance.find("thead th > input.ektron-ui-gridview-RadioButton-header").each(function () {
                    });
                }
            },
            _init: function (instance) {
                this.RadioButton._init(instance);
            }
        }
    }
}));