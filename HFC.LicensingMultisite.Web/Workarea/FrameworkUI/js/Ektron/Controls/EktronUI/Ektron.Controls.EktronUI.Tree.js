(function ($) {
    $.widget("ektron.tree", {
        options: {
            Data: "[]",
            ControlUniqueId: "",
            AutoPostBackOnSelectionChanged: false,
            BeforeAjax: null,
            controlUniqueId: "",
            json: "[]"
        },
        _treeData: [],
        _create: function (options) {
            this._setup.self = this;
            this._markupData = this.options.Data;
            this._treeContainer = this._setup.self.element;
            this._treeContainerId = this._treeContainer.attr("id");
            this._treeUniqueId = this.options.ControlUniqueId;
            this.autoPostBackOnSelectionChanged = this.options.AutoPostBackOnSelectionChanged;
            this.selectionChanged = false;
            this._postbackPending = false;
            this._callbackPending = false;
            this._postbackJsonField = $(this._treeContainer).find(".postbackJsonField");
            this._setup.setMarkup();
            this._setup.bindEvents();
            this._setup.getData();
        },
        _setup: {
            self: {},
            bindEvents: function () {
                var tree = this.self;
            },
            getData: function () {
            },
            setMarkup: function () {
                var tree = this.self;
                var containerId = tree._treeContainerId;
                var treeRootElement = tree._treeContainer.find("#" + tree._treeContainerId + "_TreeRootElement");
                tree._applyJsonToTemplate(tree, treeRootElement, tree._markupData, false)
                tree._bindEventRouter(tree, tree._treeContainer);
            }
        },
        _bindEventRouter: function (tree, container) {
            container.on("click.ektronTreeControl", function (event) {
                if (tree._callbackPending || tree._postbackPending) return;
                var trigger = $ektron(event.target);
                tree.selectionChanged = false;
                var continueProcessing = true;
                continueProcessing = continueProcessing && tree._handleRadioButtonSelectionActivity(tree, trigger);
                continueProcessing = continueProcessing && tree._handleCheckBoxSelectionActivity(tree, trigger);
                continueProcessing = continueProcessing && tree._handleToggleableParentNodes(tree, trigger);
                continueProcessing = continueProcessing && tree._handleParentNodesWithDefferredLoad(tree, trigger);
                continueProcessing = continueProcessing && tree._handlePagingNodes(tree, trigger);
                continueProcessing = continueProcessing && tree._handleCommandNodes(tree, trigger);
                continueProcessing = continueProcessing && tree._handleClickPostbackNodes(tree, trigger);
                continueProcessing = continueProcessing && tree._handleSelectionClickPostbackNodes(tree, trigger);
                continueProcessing = continueProcessing && tree._handleAnchorTagClick(tree, trigger);
                tree._handleSelectionChanged(tree, trigger); // always postback on selection change, if enabled...
                return continueProcessing;
            });
        },
        _clickPostBack: function (tree, triggerElement) {
            var clickPostBack = tree._treeContainer.find(".clickPostBackEventReference").val();
            if (clickPostBack != null && clickPostBack.length > 0) {
                tree._postbackPending = true;
                tree.element.trigger("postback", [tree, triggerElement]);
                eval(clickPostBack);
            }
        },
        _handleToggleableParentNodes: function (tree, trigger) {
            var trigElement = tree._getTargetAncestorElementInCurrentLi(tree, trigger, ".hitLocation");
            if (trigElement.length && tree._getParentLi(trigElement).is(".ektron-ui-tree-branch ")) { tree.toggle(trigger); }
            return true;
        },
        _handleParentNodesWithDefferredLoad: function (tree, trigger) {
            var trigElement = tree._getTargetElementInCurrentLi(tree, trigger, '[data-ektron-action="LoadChildren"]');
            if (trigElement.length) {
                trigElement = trigElement.eq(0);
                trigElement.removeAttr("data-ektron-action");
                tree._updateTreeData({
                    targetElement: trigElement,
                    tree: tree,
                    property: {
                        Action: null
                    },
                    mode: "updateProperty"
                });
                var dataId = tree._getId(tree, trigElement);
                var level = tree._getLevel(tree, trigElement);
                var path = tree._getPath(tree, trigElement);
                tree._ServerCallback(tree, trigElement, tree._treeUniqueId, 'loadChildren:' + dataId + "_level_" + level + "_path_" + path, 'loadChildren^' + tree._treeContainerId + "^" + dataId);
                return false;
            }
            return true;
        },
        _handlePagingNodes: function (tree, trigger) {
            var trigElement = tree._getTargetAncestorElementInCurrentLi(tree, trigger, '[data-ektron-action="GetMore"]');
            if (trigElement.length) {
                trigElement = trigElement.eq(0);
                var dataId = tree._getId(tree, trigElement);
                var level = tree._getLevel(tree, trigElement);
                var path = tree._getPath(tree, trigElement);
                tree._ServerCallback(tree, trigElement, tree._treeUniqueId, 'getMore:' + dataId + "_level_" + level + "_path_" + path, 'getMore^' + tree._treeContainerId + "^" + dataId);
                return false;
            }
            return true;
        },
        _handleCommandNodes: function (tree, trigger) {
            var trigElement = tree._getTargetAncestorElementInCurrentLi(tree, trigger, "[data-ektron-CommandName]");
            if (trigElement.length) {
                trigElement = trigElement.eq(0);
                var dataId = tree._getId(tree, trigElement);
                var path = tree._getPath(tree, trigElement);

                tree._treeContainer.find(".currentNodeId").val(dataId);
                tree._treeContainer.find(".currentNodePath").val(path);

                var cmdName = "";
                var cmdArg = "";

                if (trigElement.data() != null && trigElement.data().ektronCommandargument != null) {
                    cmdName = trigElement.data().ektronCommandname;
                    cmdArg = trigElement.data().ektronCommandargument;
                }

                tree._treeContainer.find(".commandName").val(cmdName);
                tree._treeContainer.find(".commandArgument").val(cmdArg);

                var commandPostBack = tree._treeContainer.find(".commandPostBackEventReference").val();
                if (commandPostBack != null && commandPostBack.length > 0) {
                    eval(commandPostBack);
                }
            }
            return true;
        },
        _handleClickPostbackNodes: function (tree, trigger) {
            var trigElement = tree._getTargetAncestorElementInCurrentLi(tree, trigger, '[data-ektron-ClickCausesPostback="true"]');
            if (trigElement.length) {
                trigElement = trigElement.eq(0);
                var dataId = tree._getId(tree, trigElement);
                var path = tree._getPath(tree, trigElement);

                tree._treeContainer.find(".currentNodeId").val(dataId);
                tree._treeContainer.find(".currentNodePath").val(path);

                tree._clickPostBack(tree, trigElement);
            }
            return true;
        },
        _handleSelectionClickPostbackNodes: function (tree, trigger) {
            var trigElement = tree._getTargetAncestorElementInCurrentLi(tree, trigger, ".postforall, .ektron-ui-tree-leaf .postforitems");
            if (trigElement.length) {
                trigElement = trigElement.eq(0);
                var dataId = tree._getId(tree, trigElement);
                var path = tree._getPath(tree, trigElement);

                tree._treeContainer.find(".currentNodeId").val(dataId);
                tree._treeContainer.find(".currentNodePath").val(path);

                var li = tree._getParentLi(trigElement);
                if (li == null || trigger.is("span.ui-icon")) {
                    return true;
                }
                tree._toggleSelectedState(tree, li);
                tree._clickPostBack(tree, trigElement);
            }
            return true;
        },
        _handleAnchorTagClick: function (tree, trigger) {
            if (trigger == null || !trigger.is("a.textWrapper")) { return true; }
            var nullLink = (trigger.attr("href") == null || trigger.attr("href").length == 0);
            var hasOnClick = (trigger.attr("onclick") != null && trigger.attr("onclick").length > 0);
            return !(nullLink || hasOnClick);
        },
        _handleCheckBoxSelectionActivity: function (tree, trigger) {
            // track checkbox selections in hidden field for server
            var trigElement = tree._getTargetAncestorElementInCurrentLi(tree, trigger, ".multiforall, .ektron-ui-tree-leaf .multiforitems");
            if (trigElement.length) {
                tree.element.trigger("select", [tree, trigElement]);
                trigElement = trigElement.eq(0);
                var li = tree._getParentLi(trigElement);
                if (li == null || trigger.is("span.ui-icon")) {
                    return true;
                }
                tree._toggleSelectedState(tree, li);
                return trigger.is("span") || trigger.is("a");
            }
            return true;
        },
        _handleRadioButtonSelectionActivity: function (tree, trigger) {
            // track radio button selections in hidden field for server
            var trigElement = tree._getTargetAncestorElementInCurrentLi(tree, trigger, ".singleforall, .ektron-ui-tree-leaf .singleforitems");
            if (trigElement.length) {
                tree.element.trigger("select", [tree, trigElement]);
                trigElement = trigElement.eq(0);
                var li = tree._getParentLi(trigElement);
                if (li == null || trigger.is("span.ui-icon")) {
                    return true;
                }
                tree._clearAllSelections(tree);
                tree._setSelectedState(tree, li, true);
                return trigger.hasClass("textWrapper");
            }
            return true;
        },
        _getTargetElementInCurrentLi: function (tree, element, targetSelector) {
            if (element.is(targetSelector)) {
                return element;
            }
            var parentLiNode = tree._getParentLi(element);
            var targetElement = parentLiNode.find(targetSelector);
            if (parentLiNode.is(tree._getParentLi(targetElement))) {
                return targetElement;
            }
            return $ektron(); // target element is outside of current LI; return an empty jQuery object
        },
        _getTargetAncestorElementInCurrentLi: function (tree, element, targetSelector) {
            if (element.is(targetSelector)) {
                return element;
            }
            var parentLiNode = tree._getParentLi(element);
            var targetElement = element.closest(targetSelector);
            if (parentLiNode.is(targetElement)) {
                return targetElement;
            }
            if (parentLiNode.find(targetElement).length) {
                return element.closest(targetElement);
            }
            return $ektron(); // target element is outside of current LI; return an empty jQuery object
        },
        _clearAllSelections: function (tree) {
            tree._treeContainer.find("li.ektron-ui-tree-branch, li.ektron-ui-tree-leaf").removeClass("selected");
            tree._applyPropertyToAllTreeDataNodes({
                tree: tree,
                property: { Selected: false }
            });
            //tree._treeSelectionsField.val("");
        },
        _toggleSelectedState: function (tree, obj) {
            tree._setSelectedState(tree, obj, !tree._getSelectedState(tree, obj));
        },
        _getSelectedState: function (tree, obj) {
            if (!obj.is("li")) {
                obj = tree._getParentLi(obj);
            };
            return obj.hasClass("selected");
        },
        _setSelectedState: function (tree, obj, selected) {
            var nodeId = tree._getId(tree, obj),
                currentNode = tree._findTreeNode(tree, nodeId, tree._treeData),
                currentSelectedState = (currentNode) ? currentNode.Selected : selected,
                selectionStateChanged = (currentSelectedState != selected);

            if (!obj.is("li")) {
                obj = tree._getParentLi(obj);
            };
            if (selected) {
                obj.addClass("selected");
            } else {
                obj.removeClass("selected");
            }
            tree._updateTreeData({
                targetElement: obj,
                tree: tree,
                property: {
                    Selected: selected
                },
                mode: "updateProperty"
            });
            if (selectionStateChanged) {
                tree._handleSelectionChangedHelper(tree, obj);
            }
        },
        _handleSelectionChangedHelper: function (tree, obj) {
            if (!tree.autoPostBackOnSelectionChanged) { return; }
            tree.selectionChanged = true;
            if (obj != null) {
                var dataId = tree._getId(tree, obj);
                var path = tree._getPath(tree, obj);

                tree._treeContainer.find(".currentNodeId").val(dataId);
                tree._treeContainer.find(".currentNodePath").val(path);
            }
        },
        _handleSelectionChanged: function (tree, triggerElement) {
            if (!tree.autoPostBackOnSelectionChanged || !tree.selectionChanged || tree._callbackPending) { return; }
            tree._clickPostBack(tree, triggerElement);
        },
        _applyJsonToTemplate: function (tree, targetElement, data, removeMoreNode) {
            if (!targetElement || targetElement.length == 0) { return; }
            var itemTemplate = tree._treeContainer.find("#" + tree._treeContainerId + "_ItemTemplate");
            if (data != null) {
                var jsonData = Ektron.JSON.parse(data);
                tree._updateTreeData({
                    tree: tree,
                    targetElement: targetElement,
                    payload: jsonData,
                    mode: removeMoreNode ? "addSiblings" : "addChildren"
                });
                if (jsonData != null && jsonData.length > 0) {
                    $(itemTemplate).tmpl(jsonData).appendTo(targetElement);
                }
            }
        },
        _setOption: function (key, value) {
            switch (key) {
                case "clear":
                    break;
            }
            $.Widget.prototype._setOption.apply(this, arguments);
            this._super("_setOption", key, value);
        },
        _getParentLi: function (obj) {
            obj = obj || $ektron(this).eq(0);
            if (obj != null) {
                if (!obj.is("li")) { return obj.closest("li"); }
            }
            return obj;
        },
        _getId: function (tree, obj) {
            var li = obj.is("li") ? obj : tree._getParentLi(obj);
            if (li == null || li.attr == null) return "";
            return li.attr("data-ektron-id");
        },
        _getPath: function (tree, obj) {
            var li = tree._getParentLi(obj);
            if (li == null || li.attr == null) return "";
            return li.attr("data-ektron-path");
        },
        _getLevel: function (tree, obj) {
            var li = tree._getParentLi(obj);
            if (li == null || li.attr == null) return "";
            return li.attr("data-ektron-level");
        },
        _getEventTarget: function (event) {
            return (event != null && event.target != null) ? $ektron(event.target) : $ektron(null);
        },
        toggle: function (trigger) {
            if (trigger.is("input")) { return true; }
            if (trigger.is("a")) {
                var href = trigger.attr("href");
                var onClick = trigger.attr("onclick");
                if (("undefined" != typeof href && href != null && href.length > 0)
                    || ("undefined" != typeof onClick && onClick != null && onClick.length > 0)) {
                    return true;
                }
            }

            var eventTarget = trigger;
            if (!eventTarget.is("li")) { eventTarget = eventTarget.closest("li"); }
            if (eventTarget == null || !eventTarget.is("li")) { return true; }

            var dataObject = eventTarget.closest(".ektron-ui-control").data();
            if (dataObject == null) { return false; }
            var tree = dataObject.tree || dataObject.ektronTree;
            if (tree == null) { return false; }
            tree._toggleNodeExpandedState(tree, eventTarget, trigger.is("span.textWrapper"));

            return false;
        },
        _toggleNodeExpandedState: function (tree, obj, forceExpanded) {
            if (obj.hasClass("expandable") || obj.hasClass("collapsible")) {
                if (forceExpanded) {
                    obj.removeClass("expandable");
                    obj.addClass("collapsible");
                    $ektron(tree).trigger("", [tree, obj]);
                } else {
                    obj.toggleClass("expandable collapsible");
                }
            }
            tree._updateTreeData({
                targetElement: obj,
                tree: tree,
                property: {
                    Expanded: obj.hasClass("collapsible")
                },
                mode: "updateProperty"
            });
            if (obj.hasClass("collapsible")) {
                tree.element.trigger("expand", [tree, obj]);
            }
            else {
                tree.element.trigger("collapse", [tree, obj]);
            }
        },
        _getTargetNodeIds: function (targetNodes, dataId, addId, delimiter) {
            var targetNodeArray = targetNodes.split(delimiter);
            var targetNode = "";
            var len = targetNodeArray.length;
            if (addId) {
                var found = false;
                for (var i = 0; i < len; i++) {
                    targetNode = targetNodeArray[i];
                    if (found = (targetNode == dataId)) {
                        break;
                    }
                }
                if (!found) {
                    if (targetNodes.length > 0) { targetNodes += delimiter; }
                    targetNodes += dataId;
                }
            } else {
                var delim = targetNodes = "";
                for (var i = 0; i < len; i++) {
                    targetNode = targetNodeArray[i];
                    if (targetNode != dataId) {
                        targetNodes += delim + targetNode;
                        delim = delimiter;
                    }
                }
            }
            return targetNodes;
        },
        _ServerCallback: function (tree, triggerElement, callbackUniqueId, args, context) {
            var callBackData = {
                "tree": tree,
                "triggerElement": triggerElement,
                "callBackUniqueId": callbackUniqueId,
                "data": args,
                "context": context,
                "abort": false
            };
            if ("string" == typeof (tree.options.BeforeAjax) && tree.options.BeforeAjax.length > 0) {
                var func = eval(tree.options.BeforeAjax);
                if ("function" == typeof (func)) {
                    func(callBackData);
                    if (callBackData.abort) {
                        return false;
                    }
                }
            }
            tree._callbackPending = true;
            var outData = "&__CALLBACKID=" + callbackUniqueId; // Must use ASP.NET UniqueID as that's what's expected on server side when control is used with master pages.
            outData += "&__CALLBACKPARAM=" + escape(callBackData.data);
            outData += "&__VIEWSTATE="; // + encodeURIComponent($ektron("#__VIEWSTATE").attr("value"));
            outData += "&EktronClientManager=" + $ektron("#EktronClientManager").val();
            var ajaxUrl = window.location.href.replace(window.location.hash, ""); // IE/Windows will block callbacks with a pound/hash symbol in the URL.
            tree.element.trigger("beforeAjaxCallback", [tree, callbackUniqueId, args, context]);

            callBackData.triggerElement.closest(".ektron-ui-tree-branch").toggleClass("loading");
            $ektron.ajax({
                type: "POST",
                url: ajaxUrl,
                data: outData,
                dataType: "html",
                error: function (inData) {
                    tree._callbackPending = false; console.log("inData: " + inData + ", context: " + callBackData.context);
                    tree.element.trigger("afterAjaxCallback", [callBackData.tree, callBackData.callbackUniqueId, callBackData.args, callBackData.context]);
                },
                success: function (inData) {
                    tree._callbackPending = false;
                    var PACKET_START = "EKTRON_PACKET_START|";
                    var payload = String(inData);
                    var idx = payload.indexOf(PACKET_START);
                    if (idx >= 0) {
                        payload = payload.substring(idx + PACKET_START.length);
                    }

                    // Load Children
                    if (callBackData.context != null && callBackData.context.indexOf("loadChildren^") == 0) {
                        var dataId = callBackData.context.split("^")[2];
                        var targetElement = callBackData.tree._treeContainer.find("ul[data-ektron-id='" + dataId + "_childContainer']");
                        callBackData.tree._applyJsonToTemplate(callBackData.tree, targetElement, payload, false);
                        callBackData.tree._handleSelectionChanged(callBackData.tree, callBackData.triggerElement);
                    }

                    // get siblings
                    if (callBackData.context != null && callBackData.context.indexOf("getMore^") == 0) {
                        var dataId = callBackData.context.split("^")[2];
                        var temporaryElement = callBackData.tree._treeContainer.find("li[data-ektron-id='" + dataId + "']");
                        var targetElement = temporaryElement.closest("ul");
                        temporaryElement.remove();
                        callBackData.tree._applyJsonToTemplate(callBackData.tree, targetElement, payload, true);
                    }
                    callBackData.tree.element.trigger("afterAjaxCallback", [callBackData.tree, callBackData.callbackUniqueId, callBackData.args, callBackData.context]);
                }
            });
            triggerElement.closest(".ektron-ui-tree-branch").toggleClass("loading");
            return false;
        },
        _findTreeNode: function (tree, id, obj) {
            var result;

            // JSON Obj could be an object or an array
            if ($.isArray(obj)) {
                for (var x = 0; x < obj.length; x++) {
                    result = tree._findTreeNode(tree, id, obj[x]);
                    if (result !== undefined) {
                        return result;
                    }
                }
            }
            else {
                if (obj.Id === id) {
                    result = obj;
                    return result;
                }

                for (var i = 0; i < obj.Items.length; i++) {
                    result = tree._findTreeNode(tree, id, obj.Items[i]);
                    if (result !== undefined) {
                        return result;
                    }
                }
            }
            return result;
        },

        _applyPropertyToAllTreeDataNodes: function (settings) {
            var s = {
                tree: null,
                property: {},
                obj: null
            };
            s = $.extend(s, settings);

            if (!s.tree || !s.property) { return; }

            if (!s.obj) {
                s.obj = s.tree._treeData;
                if (!s.obj) { return; }
            }

            s.tree._applyPropertyToAllTreeDataNodesHelper(s);
            s.tree._updateTreeDataPostback(s.tree, s.tree._treeData);
        },

        _applyPropertyToAllTreeDataNodesHelper: function (settings) {
            var s = {
                tree: null,
                property: {},
                obj: null
            };
            s = $.extend(s, settings);

            if (!s.tree || !s.property || !s.obj) { return; }

            if ($.isArray(s.obj)) {
                for (var x = 0; x < s.obj.length; x++) {
                    var node = s.obj[x];
                    $.extend(node, s.property);
                    var nodeSettings = {
                        tree: s.tree,
                        property: s.property,
                        obj: node
                    };
                    s.tree._applyPropertyToAllTreeDataNodesHelper(nodeSettings);
                }
            }
            else {
                for (var i = 0; i < s.obj.Items.length; i++) {
                    var node = s.obj.Items[i];
                    $.extend(node, s.property);
                    var nodeSettings = {
                        tree: s.tree,
                        property: s.property,
                        obj: node
                    };
                    s.tree._applyPropertyToAllTreeDataNodesHelper(nodeSettings);
                }
            }
        },

        _updateTreeData: function (settings) {
            var s = {
                payload: [],
                targetElement: null,
                tree: null,
                property: {},
                mode: "addChildren" // possible modes are "addChildren", "addSiblings" and "updateProperty"
            };
            s = $.extend(s, settings);

            if (!s.tree || !s.targetElement) {
                // can't update the treeData without these
                return;
            }

            var id = s.tree._getId(s.tree, s.targetElement);
            if (!id) {
                // tree first initialized, set _treeData to be the payload
                if (s.payload) {
                    s.tree._treeData = s.payload;
                    s.tree._updateTreeDataPostback(s.tree, s.payload);
                }
                return;
            }

            var nodeToUpdate = s.tree._findTreeNode(s.tree, id, s.tree._treeData);

            switch (s.mode) {
                case "addChildren":
                    nodeToUpdate.Items = nodeToUpdate.Items.concat(s.payload);
                    break;
                case "addSiblings":
                    // remove the more node from the Item collection before adding new nodes
                    nodeToUpdate.Items.pop();
                    nodeToUpdate.Items = nodeToUpdate.Items.concat(s.payload);
                    break;
                case "updateProperty":
                    $.extend(nodeToUpdate, s.property);
                    break;
                default:
                    // do nothing, just return
                    return;
            }
            // now update hte hdiden field for postbacks
            s.tree._updateTreeDataPostback(s.tree, s.tree._treeData);
        },
        _updateTreeDataPostback: function (tree, data) {
            tree._postbackJsonField.val(Ektron.JSON.stringify(data));
        },
        destroy: function () {
            $.Widget.prototype.destroy.call(this);
        }
    });
} ($ektron));