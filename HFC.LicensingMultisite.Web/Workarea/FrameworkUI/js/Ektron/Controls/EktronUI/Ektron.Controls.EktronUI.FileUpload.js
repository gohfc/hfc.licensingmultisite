﻿
function EktronFileUploadDispatcher(json) {
	Ektron.Controls.EktronUI.FileUpload.router(json);
}

if ("undefined" == typeof (Ektron)) { Ektron = {}; }
if ("undefined" == typeof (Ektron.Controls)) { Ektron.Controls = {}; }
if ("undefined" == typeof (Ektron.Controls.EktronUI)) { Ektron.Controls.EktronUI = {}; }
if ("undefined" == typeof (Ektron.Controls.EktronUI.FileUpload)) {

    Ektron.Controls.EktronUI.FileUpload = {
        Messages: [],
        Strategies: [],
        bindUI: function (containerId, fileGuid) {
            var inputBox = Ektron.Controls.EktronUI.FileUpload.findHiddenInput(containerId);

            var currentValue = inputBox.val();
            var arr = Ektron.JSON.parse(currentValue);

            var result = '';

            for (i = 0; i < arr.length; i++) {
                var item = arr[i];

                if (result.length > 0)
                    result = result + "," + item.FileGuid;
                else
                    result = item.FileGuid;
            }

            var container = document.getElementById(containerId);
            var obj = container.getElementsByTagName('object')[0];

            obj.Content.UploaderControl.ReBindUI(result);
        },
        cancel: function (containerId, guid) {
            var container = document.getElementById(containerId);
            var obj = container.getElementsByTagName('object')[0];

            obj.Content.UploaderControl.Cancel(guid);
        },
        create: function (json) {
            if ($ektron("#" + json.FileGuid).length > 0) return;

            $("#uploadTemplate").tmpl(json).appendTo("#" + json.Selector + ' div.ektron-ui-fileUpload-files');

            $ektron("#" + json.FileGuid + ' div.ektron-ui-progressbar').progressbar();
        },
        findCancelButton: function (containerId) {
            return $ektron('#' + containerId + ' div.ektron-ui-fileUpload-cancel button');
            //container: $ektron('#' + containerId + ' div.ektron-ui-fileUpload-cancel')
        },
        findCancelContainer: function (containerId) {
            //return $ektron('#' + containerId + ' div.ektron-ui-fileUpload-cancel button');
            return $ektron('#' + containerId + ' div.ektron-ui-fileUpload-cancel');
        },
        findChangeButton: function (containerId) {
            return {
                button: $ektron('#' + containerId + ' div.ektron-ui-fileUpload-delete button'),
                container: $ektron('#' + containerId + ' div.ektron-ui-fileUpload-delete')
            };
        },
        findFileUploadCompleteIcon: function (containerId) {
            return $ektron('#' + containerId + ' span.ektron-ui-fileUploadCompleteIcon');
        },
        findHiddenInput: function (containerId) {
            return $ektron('#' + containerId + ' div.ektron-ui-fileUpload-hidden input');
        },
        findFileName: function (containerId) {
            return $ektron('#' + containerId + ' div.ektron-ui-fileUpload-fileName span');
        },
        findMessage: function (containerId) {
            return $ektron('#' + containerId + ' span.ektron-ui-fileUpload-message');
        },
        findProgressBar: function (containerId) {
            return $ektron('#' + containerId + ' div.ektron-ui-progressbar');
        },
        init: function (containerId) {
            var inputBox = this.findHiddenInput(containerId);

            var currentValue = inputBox.val();

            if (currentValue.length <= 2) return;

            if (this.Strategies.length == 0)
                this.onLoad();

            var arr = Ektron.JSON.parse(currentValue);

            $ektron.each(arr, function (idx, json) {
                Ektron.Controls.EktronUI.FileUpload.create(json);
                Ektron.Controls.EktronUI.FileUpload.Strategies[6].execute(json);
            });
        },
        onLoad: function () {
            if (this.Strategies.length > 0) return;
            // UploadEventTypes HashCode = position
            this.Strategies.push(Ektron.Controls.EktronUI.FileUpload.AddStrategy);         // 0
            this.Strategies.push(Ektron.Controls.EktronUI.FileUpload.UploadingStrategy);   // 1
            this.Strategies.push(Ektron.Controls.EktronUI.FileUpload.FinishedStrategy);    // 2
            this.Strategies.push(Ektron.Controls.EktronUI.FileUpload.AllFinishedStrategy); // 3
            this.Strategies.push(Ektron.Controls.EktronUI.FileUpload.CanceledStrategy);    // 4
            this.Strategies.push(Ektron.Controls.EktronUI.FileUpload.ErrorStrategy);       // 5
            this.Strategies.push(Ektron.Controls.EktronUI.FileUpload.PostBackStrategy);    // 6
        },
        remove: function (containerId, fileGuid) {

            var container = document.getElementById(containerId);
            var obj = container.getElementsByTagName('object')[0];

            obj.Content.UploaderControl.Remove(fileGuid);

            // remove json data from hidden field
            var inputBox = Ektron.Controls.EktronUI.FileUpload.findHiddenInput(containerId);

            var currentValue = inputBox.val();
            var arr = Ektron.JSON.parse(currentValue);

            if (arr == '' || arr == undefined || arr.length == 1) {
                inputBox.val('');
            }
            else {

                var result = '';

                for (i = 0; i < arr.length; i++) {
                    var item = arr[i];

                    if (item.FileGuid != fileGuid) {
                        if (result.length > 0)
                            result = result + "," + Ektron.JSON.stringify(item);
                        else
                            result = Ektron.JSON.stringify(item);
                    }
                }

                inputBox.val('[' + result + ']');
            }

            $ektron('#' + fileGuid).remove();
        },
        router: function (json) {
            if (this.Strategies.length == 0)
                this.onLoad();

            json = eval(json);

            this.create(json);

            this.Strategies[json.UploadEventType].execute(json);
        }
    };

    Ektron.Controls.EktronUI.FileUpload.AddStrategy = {
        execute: function (json) {
            var progressBar = Ektron.Controls.EktronUI.FileUpload.findProgressBar(json.FileGuid);
            progressBar.progressbar({ disabled: false });
            progressBar.progressbar({ value: json.PercentageUploaded + 1 });

            var message = Ektron.Controls.EktronUI.FileUpload.findMessage(json.FileGuid);

            $ektron.grep(Ektron.Controls.EktronUI.FileUpload.Messages, function (n, i) {
                if (n.Type == 'AddMessage' && n.Control == json.Selector) {
                    message.text(n.Message);
                    return;
                }
            });

            var span = Ektron.Controls.EktronUI.FileUpload.findFileName(json.FileGuid);
            span.val(json.FileName);

            var cancelButtonObj = Ektron.Controls.EktronUI.FileUpload.findCancelButton(json.FileGuid);
            var cancelContainerObj = Ektron.Controls.EktronUI.FileUpload.findCancelContainer(json.FileGuid);
            cancelContainerObj.show();
            cancelButtonObj.bind('click', function () {
                Ektron.Controls.EktronUI.FileUpload.cancel(json.Selector, json.FileGuid);
                return false;
            });
        }
    };

	Ektron.Controls.EktronUI.FileUpload.UploadingStrategy = {
		execute: function (json) {
			var progressBar = Ektron.Controls.EktronUI.FileUpload.findProgressBar(json.FileGuid);
			progressBar.progressbar({ value: json.PercentageUploaded });
			var message = Ektron.Controls.EktronUI.FileUpload.findMessage(json.FileGuid);


			$ektron.grep(Ektron.Controls.EktronUI.FileUpload.Messages, function (n, i) {
				if (n.Type == 'UploadingMessage' && n.Control == json.Selector) {
					message.text(n.Message);
					return;
				}
			});

			$ektron('#' + json.Selector).trigger('ektronControlsEktronUIFileUpload-Uploading', json);
		}
	};

	Ektron.Controls.EktronUI.FileUpload.FinishedStrategy = {
	    execute: function (json) {
	        var progressBar = Ektron.Controls.EktronUI.FileUpload.findProgressBar(json.FileGuid);
	        progressBar.progressbar({ value: json.PercentageUploaded, disabled: true });

	        var message = Ektron.Controls.EktronUI.FileUpload.findMessage(json.FileGuid);

	        $ektron.grep(Ektron.Controls.EktronUI.FileUpload.Messages, function (n, i) {
	            if (n.Type == 'FinishedMessage' && n.Control == json.Selector) {
	                message.text(n.Message);
	                return;
	            }
	        });

	        var inputBox = Ektron.Controls.EktronUI.FileUpload.findHiddenInput(json.Selector);

	        var result = {};
	        result.FileName = json.FileName;
	        result.FileGuid = json.FileGuid;
	        result.Selector = json.Selector;

	        var currentValue = inputBox.val();
	        var currentValue = currentValue.substring(1, currentValue.length - 1);

	        if (currentValue.length > 0)
	            currentValue = currentValue + "," + Ektron.JSON.stringify(result);
	        else
	            currentValue = Ektron.JSON.stringify(result);

	        inputBox.val('[' + currentValue + ']');

	        $ektron('#' + json.Selector).trigger('ektronControlsEktronUIFileUpload-Finished', json);

	        var cancelButtonObj = Ektron.Controls.EktronUI.FileUpload.findCancelButton(json.FileGuid);
	        var cancelContainerObj = Ektron.Controls.EktronUI.FileUpload.findCancelContainer(json.FileGuid);
	        cancelButtonObj.unbind('click');
	        cancelContainerObj.hide();

	        var changeButtonObj = Ektron.Controls.EktronUI.FileUpload.findChangeButton(json.FileGuid);
	        changeButtonObj.button.bind('click', function () {
	            Ektron.Controls.EktronUI.FileUpload.remove(json.Selector, json.FileGuid);
	            return false;
	        });
	        changeButtonObj.container.show();
	        progressBar.addClass("ektron-ui-fileUpload-complete");
	    }
	};

	Ektron.Controls.EktronUI.FileUpload.AllFinishedStrategy = {
		execute: function (json) {
			$ektron('#' + json.Selector).trigger('ektronControlsEktronUIFileUpload-AllFinished', json);
		}
	};

	Ektron.Controls.EktronUI.FileUpload.CanceledStrategy = {
		execute: function (json) {
			var progressBar = Ektron.Controls.EktronUI.FileUpload.findProgressBar(json.FileGuid);
			progressBar.progressbar({ disabled: true });

			var message = Ektron.Controls.EktronUI.FileUpload.findMessage(json.FileGuid);

			$ektron.grep(Ektron.Controls.EktronUI.FileUpload.Messages, function (n, i) {
				if (n.Type == 'CanceledMessage' && n.Control == json.Selector) {
					message.text(n.Message);
					return;
				}
			});

            var cancelButtonObj = Ektron.Controls.EktronUI.FileUpload.findCancelButton(json.FileGuid);
            var cancelContainerObj = Ektron.Controls.EktronUI.FileUpload.findCancelContainer(json.FileGuid);
            cancelButtonObj.unbind('click');
            cancelContainerObj.hide();

			$ektron('#' + json.Selector).trigger('ektronControlsEktronUIFileUpload-Cancel', json);

			$ektron('#' + json.FileGuid).fadeOut('1000', function () {
				$ektron('#' + json.FileGuid).remove();
			});
		}
	};

	Ektron.Controls.EktronUI.FileUpload.ErrorStrategy = {
		execute: function (json) {
			var progressBar = Ektron.Controls.EktronUI.FileUpload.findProgressBar(json.FileGuid);
			progressBar.progressbar({ disabled: true });

			var message = Ektron.Controls.EktronUI.FileUpload.findMessage(json.FileGuid);
			message.text('error has occurred: ' + json.Error.Message);

			$ektron('#' + json.Selector).trigger('ektronControlsEktronUIFileUpload-Error', json);

			var cancelButtonObj = Ektron.Controls.EktronUI.FileUpload.findCancelButton(json.FileGuid);
			var cancelContainerObj = Ektron.Controls.EktronUI.FileUpload.findCancelContainer(json.FileGuid);
			cancelButtonObj.unbind('click');
			cancelContainerObj.hide();
		}
	};

	Ektron.Controls.EktronUI.FileUpload.PostBackStrategy = {
		execute: function (json) {
			var progressBar = Ektron.Controls.EktronUI.FileUpload.findProgressBar(json.FileGuid);
			progressBar.progressbar({ value: 100 });
			progressBar.progressbar({ disabled: true });

			var message = Ektron.Controls.EktronUI.FileUpload.findMessage(json.FileGuid);

			$ektron.grep(Ektron.Controls.EktronUI.FileUpload.Messages, function (n, i) {
				if (n.Type == 'FinishedMessage' && n.Control == json.Selector) {
					message.text(n.Message);
					return;
				}
			});

            var cancelButtonObj = Ektron.Controls.EktronUI.FileUpload.findCancelButton(json.FileGuid);
            var cancelContainerObj = Ektron.Controls.EktronUI.FileUpload.findCancelContainer(json.FileGuid);
            cancelButtonObj.unbind('click');
            cancelContainerObj.hide();

            var changeButtonObj = Ektron.Controls.EktronUI.FileUpload.findChangeButton(json.FileGuid);
            changeButtonObj.button.bind('click', function () {
				Ektron.Controls.EktronUI.FileUpload.remove(json.Selector, json.FileGuid);
				return false;
			});
            changeButtonObj.container.show();

			Ektron.Controls.EktronUI.FileUpload.bindUI(json.Selector, json.FileGuid);

		}
	};
}
