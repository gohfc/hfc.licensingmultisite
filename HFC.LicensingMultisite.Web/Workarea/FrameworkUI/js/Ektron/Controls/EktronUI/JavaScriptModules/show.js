﻿/*
* EktronUI JavaScript Modules: Show
*
* Copyright 2011
*
* Depends:
*	jQuery
*   Ektron.Namespace.js
*/

Ektron.Namespace.Register("Ektron.Controls.EktronUI.JavaScriptModules");
Ektron.Controls.EktronUI.JavaScriptModules.Show = function (selector) {
    // ensure the selector being passed is a string
    if ("string" !== typeof (selector)) {
        return;
    }
    $ektron(selector).show();
};