﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="../../../common/ui/css/jquery-ui-1.9m6.css" />
/// <reference path="../../../../../../../../../../../csslib/ektronTheme/jquery-ui-custom.css" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/image-properties.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, TblPropBlock, $ektron, i18njs,
    clearContainer = function (containerId) {
        "use strict";
        while ($ektron(containerId).children().length > 0) {
            $ektron(containerId).children().remove();
        }
    },
    i18n = {
        i18njs : { "table-properties.label.caption": "Caption",
        "table-properties.label.summary": "Summary" },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("Table Properties ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        TblPropBlock = window.createModule(i18n);
        block = new TblPropBlock();
    }
});

test('Table Properties fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundCaptionBox, foundSummaryTextBox,  
        thisId = 'my-div',
        container = $ektron('#dialog-container'),
        block = new TblPropBlock(thisId, document.getElementById('divResult'), i18n);
    myblock = block.create();
    container.append(myblock);

    foundBlock = $ektron('.aloha-table-properties', container);
    foundCaptionBox = $ektron('textarea#txtcaption' + thisId, container);
    foundSummaryTextBox = $ektron('textarea#txtsummary' + thisId, container);

    ok(foundBlock.length > 0, 'Table Properties fieldset found.');
    ok(foundCaptionBox.length > 0, 'Caption text box is found.');
    ok(foundSummaryTextBox.length > 0, 'Summary text box is found.');
});

test('Image Properties are reloaded correctly when target image is selected.', function () {
    "use strict";
    var targetElement = $ektron('#divResult2'), 
        container = $ektron('#dialog-container2'),
        thisId = 'my-div2',
        block = new TblPropBlock(thisId, targetElement.get(0), i18n);
 debugger;
    myblock = block.create();
    container.append(myblock);
   
    equal($ektron('textarea#txtcaption' + thisId, container).val(), targetElement.children('caption').html(), 'table caption is set correctly.');
    equal($ektron('textarea#txtsummary' + thisId, container).val(), targetElement.attr('summary'), 'table summary is set correctly.');
});