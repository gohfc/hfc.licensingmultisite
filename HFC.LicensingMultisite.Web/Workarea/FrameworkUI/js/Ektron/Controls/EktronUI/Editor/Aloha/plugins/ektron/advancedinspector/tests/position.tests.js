﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/text-alignment.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, PositionBlock, $ektron, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs : { "position.checkbox.positionwrapping": "Position &amp; Wrapping", "position.label.left": "Left", "position.label.none": "None", 
        "position.label.right": "Right" },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("Position and Wrapping style ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        PositionBlock = window.createModule(i18n);
        block = new PositionBlock();
    }
});

test('Position fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundButtonSet, foundPosLeft, foundPosNone, foundPosRight,
        container = $ektron('#dialog-container'),
        block = new PositionBlock('my-div', document.getElementById('divResult'), i18n);
    myblock = block.create();
    container.append(myblock);
    block.init();

    foundBlock = $ektron('#my-div', container);
    foundButtonSet = $ektron('div#position-radio', container);
    foundPosLeft = $ektron('input#position-left', container);
    foundPosNone = $ektron('input#position-none', container);
    foundPosRight = $ektron('input#position-right', container);

    ok(foundBlock.length > 0, 'Position and Wrapping fieldset found.');
    ok(foundButtonSet.length > 0, 'Position buttonset found.');
    ok(foundPosLeft.length > 0, 'Position Left found.');
    ok(foundPosNone.length > 0, 'Position None found.');
    ok(foundPosRight.length > 0, 'Position Right found.');
    equal(foundButtonSet.hasClass('ui-buttonset'), true, 'jQuery UI buttonset created for the position radio buttons.');
});

test('Position is not set when checkbox is not checked', function () {
    "use strict";
    var position = 'Left',
        container = $ektron('#dialog-container1');
    block = new PositionBlock('my-div1', document.getElementById('divResult1'), i18n);
    myblock = block.create();
    container.append(myblock);
    block.init();

    $ektron('#position-' + position.toLowerCase(), container).attr('checked', true);
    $ektron('#chk-position', container).attr('checked', false);
    block.updateStyle();

    equal($ektron('#divResult1').css('float'), 'none', 'position style is set correctly.');
});

test('Position set correctly when checkbox is checked', function () {
    "use strict";
    var position = 'Right',
        container = $ektron('#dialog-container2'),
        block = new PositionBlock('my-div2', document.getElementById('divResult2'), i18n);
    myblock = block.create();
    container.append(myblock);
    block.init();

    $ektron('#chk-position', container).attr('checked', true);
    $ektron('#position-' + position.toLowerCase(), container).attr('checked', true);
    block.updateStyle();
    block.showHideModule($ektron('#chk-position', container).get(0));

    ok($ektron('#divResult2').attr('style').toLowerCase().indexOf('float: ' + position.toLowerCase() + ';') > -1, 'position is set correctly.');
});

test('fields are reloaded correctly when target element contains float position style.', function () {
    "use strict";
    var targetElement = $ektron('#divResult3'), 
        position = 'Right', valSelected,
        container = $ektron('#dialog-container3'),
        block = new PositionBlock('my-div3', document.getElementById('divResult3'), i18n);
    targetElement.css('float', position.toLowerCase());
    myblock = block.create();
    container.append(myblock);
    block.init();

    valSelected = $ektron('input[type=radio]:checked', $ektron('#my-div3')).attr('id').toLowerCase().replace(/position-/, "");

    equal(valSelected, position.toLowerCase(), 'value for radio button set is loaded correctly.');
    equal($ektron('#chk-position', container).is(':checked'), true, 'Position checkbox is loaded correctly.');
});