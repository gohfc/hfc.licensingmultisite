﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([
    ''
], function () {
    "use strict";
    var pluginNamespace = 'aloha-spacing';
    function SpacingBlock(id, obj, i18n) {
        this.id = id;
        this.targetElem = {
            elem : obj,
            vMargin : 0,
            margin : 0,
            vPadding : 0,
            padding : 0
        }; 

        this.create = function () {
            var spacingUnitOptions = "<option value='em'>EM</option><option value='px'>px</option>",
                spacingBlock, spacingCheckbox, spacingLabel, spacingGroup, spacingList, spacingInput, spacingSelect, 
                jTarget = $ektron(this.targetElem.elem), 
                currStyle = jTarget.attr('style'),  
                currUnit = "EM",
                that = this, i;
            this.deserializeStyle(currStyle);
            spacingBlock = document.createElement('fieldset');
            spacingBlock.setAttribute('id', this.id);
            spacingBlock.className = pluginNamespace;

            //fieldset checkbox and label
            spacingCheckbox = $ektron('<input type="checkbox" id="chk-spacing" />');
            spacingCheckbox.on('change', function () { that.showHideModule(this); that.updateStyle(); });
            if (this.hasSpacing()) {
                spacingCheckbox.attr('checked', true);
            }
            spacingBlock.appendChild(spacingCheckbox.get(0)); 
            spacingLabel = document.createElement('label');
            spacingLabel.setAttribute('for', 'chk-spacing');
            spacingLabel.setAttribute('title', i18n.t('spacing-style.checkbox.spacing'));
            spacingLabel.innerHTML = i18n.t('spacing-style.checkbox.spacing'); 
            spacingBlock.appendChild(spacingLabel); 

            //margin and padding fields group
            spacingGroup = document.createElement('ul');
            //Horizontal margin
            spacingList = document.createElement('li');
            spacingLabel = document.createElement('label');
            spacingLabel.setAttribute('for', 'txtHMargin');
            spacingLabel.setAttribute('title', i18n.t('spacing-style.label.margin-horizonal'));
            spacingLabel.className = this.nsClass('label');
            spacingLabel.innerHTML = i18n.t('spacing-style.label.margin-horizonal');
            spacingList.appendChild(spacingLabel);
            spacingGroup.appendChild(spacingList);
            spacingList = document.createElement('li');
            spacingInput = $ektron('<input id="txtHMargin" type="text" class="input-box margin horizontal textbox-narrow" />');
            spacingInput.on('input', function () { that.updateStyle(); });
            if (this.targetElem.margin !== 0) {
                currUnit = this.targetElem.margin.slice(-2);
                spacingInput.val(this.targetElem.margin.substring(0, this.targetElem.margin.length - 2));
            }
            spacingList.appendChild(spacingInput.get(0));
            spacingSelect = $ektron('<select id="ddHMarginUnit" class="select-box txtHMargin">').html(spacingUnitOptions);
            spacingSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.margin !== 0) {
                spacingSelect.val(currUnit);
            }
            spacingList.appendChild(spacingSelect.get(0));
            spacingGroup.appendChild(spacingList);
            
            //Vertical margin
            spacingList = document.createElement('li');
            spacingLabel = document.createElement('label');
            spacingLabel.setAttribute('for', 'txtVMargin');
            spacingLabel.setAttribute('title', i18n.t('spacing-style.label.margin-vertical'));
            spacingLabel.className = 'spacing-label';
            spacingLabel.innerHTML = i18n.t('spacing-style.label.margin-vertical');
            spacingList.appendChild(spacingLabel);
            spacingGroup.appendChild(spacingList);
            spacingList = document.createElement('li');
            spacingInput = $ektron('<input id="txtVMargin" type="text" class="input-box margin vertical textbox-narrow" />');
            spacingInput.on('input', function () { that.updateStyle(); });
            if (this.targetElem.vMargin !== 0) {
                currUnit = this.targetElem.vMargin.slice(-2);
                spacingInput.val(this.targetElem.vMargin.substring(0, this.targetElem.vMargin.length - 2));
            }
            spacingList.appendChild(spacingInput.get(0));
            spacingSelect = $ektron('<select id="ddVMarginUnit" class="select-box txtVMargin">').html(spacingUnitOptions);
            spacingSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.vMargin !== 0) {
                spacingSelect.val(currUnit);
            }
            spacingList.appendChild(spacingSelect.get(0));
            spacingGroup.appendChild(spacingList);

            // separator
            spacingList = document.createElement('li');
            spacingList.innerHTML = '<hr />';
            spacingGroup.appendChild(spacingList);

            //Horizontal padding
            spacingList = document.createElement('li');
            spacingLabel = document.createElement('label');
            spacingLabel.setAttribute('for', 'txtHPadding');
            spacingLabel.setAttribute('title', i18n.t('spacing-style.label.padding-horizonal'));
            spacingLabel.className = this.nsClass('label');
            spacingLabel.innerHTML = i18n.t('spacing-style.label.padding-horizonal');
            spacingList.appendChild(spacingLabel);
            spacingGroup.appendChild(spacingList);
            spacingList = document.createElement('li');
            spacingInput = $ektron('<input id="txtHPadding" type="text" class="input-box padding horizontal textbox-narrow" />');
            spacingInput.on('input', function () { that.updateStyle(); });
            if (this.targetElem.padding !== 0) {
                currUnit = this.targetElem.padding.slice(-2);
                spacingInput.val(this.targetElem.padding.substring(0, this.targetElem.padding.length - 2));
            }
            spacingList.appendChild(spacingInput.get(0));
            spacingSelect = $ektron('<select id="ddHPaddingUnit" class="select-box txtHPadding">').html(spacingUnitOptions);
            spacingSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.padding !== 0) {
                spacingSelect.val(currUnit);
            }
            spacingList.appendChild(spacingSelect.get(0));
            spacingGroup.appendChild(spacingList);

            //vertical padding
            spacingList = document.createElement('li');
            spacingLabel = document.createElement('label');
            spacingLabel.setAttribute('for', 'txtVPadding');
            spacingLabel.setAttribute('title', i18n.t('spacing-style.label.padding-vertical'));
            spacingLabel.className = this.nsClass('label');
            spacingLabel.innerHTML = i18n.t('spacing-style.label.padding-vertical');
            spacingList.appendChild(spacingLabel);
            spacingGroup.appendChild(spacingList);
            spacingList = document.createElement('li');
            spacingInput = $ektron('<input id="txtVPadding" type="text" class="input-box padding vertical textbox-narrow" />');
            spacingInput.on('input', function () { that.updateStyle(); });
            if (this.targetElem.vPadding !== 0) {
                currUnit = this.targetElem.vPadding.slice(-2);
                spacingInput.val(this.targetElem.vPadding.substring(0, this.targetElem.vPadding.length - 2));
            }
            spacingList.appendChild(spacingInput.get(0));
            spacingSelect = $ektron('<select id="ddVPaddingUnit" class="select-box txtVPadding">').html(spacingUnitOptions);
            spacingSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.vPadding !== 0) {
                spacingSelect.val(currUnit);
            }
            spacingList.appendChild(spacingSelect.get(0));
            spacingGroup.appendChild(spacingList);

            spacingBlock.appendChild(spacingGroup); 
            if (!this.hasSpacing()) {
                spacingGroup.setAttribute('style', 'display:none');
            }
            return spacingBlock;
        };

        this.updateStyle = function () {
            var styleString = "", jResult = $ektron(this.targetElem.elem);
            jResult.css('margin', "").css('padding', "");
            if (true === document.getElementById('chk-spacing').checked) { 
                $ektron('.input-box').each(function (index, value) {
                    var jThis = $ektron(this);
                    if (jThis.hasClass('margin') && !isNaN(jThis.val())) {
                        styleString = jThis.val() + jThis.siblings('.select-box').val();
                        if (jThis.hasClass('horizontal')) {
                            jResult.css('margin-right', styleString);
                            jResult.css('margin-left', styleString);
                        }
                        else {
                            jResult.css('margin-top', styleString);    
                            jResult.css('margin-bottom', styleString); 
                        }
                    }

                    if (jThis.hasClass('padding') && !isNaN(jThis.val())) {
                        styleString = jThis.val() + jThis.siblings('.select-box').val();
                        if (jThis.hasClass('horizontal')) {
                            jResult.css('padding-right', styleString);
                            jResult.css('padding-left', styleString);
                        }
                        else {
                            jResult.css('padding-top', styleString);
                            jResult.css('padding-bottom', styleString);
                        }
                    }
                });
            }
                
        };

        this.deserializeStyle = function (currStyle) {
            var arrStyle, arrProperty, i, strStyle;
            if (typeof currStyle !== 'undefined' && currStyle !== '') {
                arrStyle = currStyle.split(';');
                for (i = 0; i < arrStyle.length; i += 1)
                {
                    strStyle = $ektron.trim(arrStyle[i]);
                    if (0 === strStyle.indexOf('margin:'))
                    {
                        arrProperty = strStyle.split(' ');
                        this.targetElem.vMargin = arrProperty[1]; 
                        this.targetElem.margin = (arrProperty[2] ? arrProperty[2] : arrProperty[1]);
                    }
                    else if  (0 === strStyle.indexOf('padding:')) {
                        arrProperty = strStyle.split(' ');
                        this.targetElem.vPadding = arrProperty[1]; 
                        this.targetElem.padding = (arrProperty[2] ? arrProperty[2] : arrProperty[1]);
                    }
                }
            }
        };

        this.hasSpacing = function () {
            if (this.targetElem.margin !== 0 || this.targetElem.vMargin !== 0 || this.targetElem.padding !== 0 || this.targetElem.vPadding !== 0) {
                return true;
            }
            return false;
        };

        this.showHideModule = function (elem) {
            //$ektron(elem).siblings('ul').toggle();
            var jElem = $ektron(elem);
            if (jElem.is(':checked')) {
                jElem.siblings('ul').show();
            }
            else {
                jElem.siblings('ul').hide();
            }
        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function ()
        {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function ()
            {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return SpacingBlock;
});
