﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/reset-style.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, ResetBlock, $ektron, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs: { "reset-style.label.class": "Css Class:", "reset-style.label.style": "Styles:", "reset-style.label.button": "Reset Styles" },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("reset style ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        ResetBlock = window.createModule(i18n);
        block = new ResetBlock();
    }
});

test('reset fieldset created successfully', function () {
    "use strict";
    var foundObj = false,
        container = $ektron('#dialog-container'),
        block = new ResetBlock('my-div', document.getElementById('divResult'), i18n),
        arrInputBox, arrSelectBox;
    myblock = block.create();
    container.append(myblock);

    foundObj = $ektron('#my-div', container);
    arrInputBox = $ektron('.input-box', container);

    ok(foundObj.length > 0, 'reset fieldset found.');
    equal(arrInputBox.length, 2, 'Two input box found.');
});

