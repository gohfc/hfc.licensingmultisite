﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/list-style.js" />

var blockUL, blockOL, myblockUL, myblockOL, ListStyleBlock, $ektron, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#UL-container').children().length > 0) {
            $ektron('#UL-container').children().remove();
        }
        while ($ektron('#OL-container').children().length > 0) {
            $ektron('#OL-container').children().remove();
        }
    },
    i18n = {
        i18njs: {
            "list-style.checkbox.text-liststyle": "List Style",
            "liststyle.label.text-liststyle-type": "Type",
            "liststyle.label.text-liststyle-position": "Position"
        },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("List style ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        ListStyleBlock = window.createModule(i18n);
        blockUL = new ListStyleBlock();
        blockOL = new ListStyleBlock();
    }
});

test('UL list style fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundCheckbox, foundTypeBox, foundPositionBox,
        container = $ektron('#UL-container'),
        blockUL = new ListStyleBlock('divUL', document.getElementById('ultest'), i18n);
    myblockUL = blockUL.create();
    container.append(myblockUL);

    foundBlock = $ektron('#divUL', container);
    foundCheckbox = $ektron('input#chkliststyledivUL', container);
    foundTypeBox = $ektron('#ddListTypedivUL', container);
    foundPositionBox = $ektron('#ddListPositiondivUL', container);


    ok(foundBlock.length > 0, 'List style  fieldset found.');
    equal(foundCheckbox.length, 1, 'List style checkbox found.');
    ok(foundTypeBox.length > 0, 'Options for list type found.');
    ok(foundPositionBox.length > 0, 'Options for list position found.');

});

test('OL list style fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundCheckbox, foundTypeBox, foundPositionBox,
        container = $ektron('#OL-container'),
        blockOL = new ListStyleBlock('divOL', document.getElementById('oltest'), i18n);
    myblockOL = blockOL.create();
    container.append(myblockOL);

    foundBlock = $ektron('#divOL', container);
    foundCheckbox = $ektron('input#chkliststyledivOL', container);
    foundTypeBox = $ektron('#ddListTypedivOL', container);
    foundPositionBox = $ektron('#ddListPositiondivOL', container);


    ok(foundBlock.length > 0, 'List style  fieldset found.');
    equal(foundCheckbox.length, 1, 'List style checkbox found.');
    ok(foundTypeBox.length > 0, 'Options for list type found.');
    ok(foundPositionBox.length > 0, 'Options for list position found.');

});
