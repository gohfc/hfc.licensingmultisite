﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/UI/js/jquery-ui-complete.min.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([''], function () {
    "use strict";
    var pluginNamespace = 'aloha-table-properties', modal;
    function TablePropertiesBlock(id, obj, i18n) {
        this.id = id;
        this.folderId = 0;
        this.targetElem = {
            tableObj : obj,
            caption : '',
            summary : ''
        };

        this.create = function () {
            var tblPropBlock, tblPropGroup, captionLabel, captionInput, 
                tblPropList, summaryLabel, summaryInput, 
                that = this;
            this.deserializeProperties();
			tblPropBlock = document.createElement('fieldset');
            tblPropBlock.setAttribute('id', this.id);
            tblPropBlock.className = pluginNamespace + ' aloha';
            tblPropGroup = document.createElement('ul');

            //Table Caption
            tblPropList = document.createElement('li');
            captionLabel = document.createElement('label');
            captionLabel.setAttribute('for', 'txtcaption' + this.id);
            captionLabel.setAttribute('title', i18n.t('table-properties.label.caption'));
            captionLabel.innerHTML = i18n.t('table-properties.label.caption'); 
            tblPropList.appendChild(captionLabel);
            tblPropGroup.appendChild(tblPropList); 
            tblPropList = document.createElement('li');
            captionInput = $ektron('<textarea id="txtcaption' + this.id + '" row="2" col="50" class="input-box table-properties" data-attrname="caption" />');
            captionInput.on('blur', function () { that.syncFields(this); that.updateProperties(); });
            if (this.targetElem.caption !== '') {
                captionInput.val(this.targetElem.caption);
            }
            tblPropList.appendChild(captionInput.get(0));
            tblPropGroup.appendChild(tblPropList);

            //Table Summary 
            tblPropList = document.createElement('li');
            summaryLabel = document.createElement('label');
            summaryLabel.setAttribute('for', 'txtalt' + this.id);
            summaryLabel.setAttribute('title', i18n.t('table-properties.label.summary'));
            summaryLabel.innerHTML = i18n.t('table-properties.label.summary'); 
            tblPropList.appendChild(summaryLabel);
            tblPropGroup.appendChild(tblPropList); 
            tblPropList = document.createElement('li');
            summaryInput = $ektron('<textarea id="txtsummary' + this.id + '" row="2" col="50" class="input-box table-properties" data-attrname="summary" />');
            summaryInput.on('blur', function () { that.syncFields(this); that.updateProperties(); });
            if (this.targetElem.summary != '') {
                summaryInput.val(this.targetElem.summary);
            }
            tblPropList.appendChild(summaryInput.get(0));
            tblPropGroup.appendChild(tblPropList);

			tblPropBlock.appendChild(tblPropGroup);
            return tblPropBlock;
        };

        this.syncFields = function (elem) {
            var updatedText = elem.value, prop = elem.getAttribute('data-attrname');
            $ektron("textarea.input-box[data-attrname='" + prop + "']").each( function () {
                $ektron(this).val(updatedText);
            });
        };

        this.updateProperties = function () {
            var updatedTbl = $ektron(this.targetElem.tableObj), eInput, attrName, updatedText, attrValue, jCaption, tblCaption, that = this;
            $ektron(".input-box.table-properties").each(function () {
                eInput = $ektron(this);
                attrName = eInput.attr("data-attrname");
                if (updatedTbl.attr(attrName) != 'undefined') { 
                    attrValue = eInput.val();
                    if ('caption' === attrName) {
                        that.targetElem.caption = attrValue;
                        if (attrValue.length > 0) { 
                            jCaption = updatedTbl.children('caption');
                            if (0 == jCaption.length) {
                                tblCaption = document.createElement('caption');
                                tblCaption.innerText = attrValue;
                                updatedTbl.prepend(tblCaption);
                            }
                            else {
                                jCaption.text(attrValue);
                            }
                        }
                        else {
                            updatedTbl.children('caption').remove();
                        }
                    }
                    else {
                        that.targetElem.summary = eInput.val();
                        updatedTbl.attr('summary', eInput.val());
                    }
                }
            }); 
        };

        this.deserializeProperties = function () {
            var jElem = $ektron(this.targetElem.tableObj);
            if (jElem.length > 0 ) {
               this.targetElem.caption = jElem.children('caption').text(); 
               this.targetElem.summary = jElem.attr('summary'); 
            }
        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function ()
        {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function ()
            {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return TablePropertiesBlock;
});