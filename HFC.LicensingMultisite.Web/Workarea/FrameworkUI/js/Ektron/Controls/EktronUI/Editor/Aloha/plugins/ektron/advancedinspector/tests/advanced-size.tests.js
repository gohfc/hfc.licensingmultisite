﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/advanced-size.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, AdvancedSizeBlock, $ektron, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs : { "advanced-size.checkbox.size": "Size", "advanced-size.label.constrain-proportions": "Constrain Proportions",
		"advanced-size.label.width": "Width", "advanced-size.label.height": "Height" },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("Advanced Size ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        AdvancedSizeBlock = window.createModule(i18n);
        block = new AdvancedSizeBlock();
    }
});

test('Advanced size fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundCheckbox, foundConstrainCheckbox, foundSelectBoxs, foundWidthBox, foundHeightBox,
        container = $ektron('#dialog-container'),
        block = new AdvancedSizeBlock('my-div', document.getElementById('divResult'), i18n);
    myblock = block.create();
    container.append(myblock);

    foundBlock = $ektron('#my-div', container);
    foundCheckbox = $ektron('input#chk-advancedSize', container);
    foundConstrainCheckbox = $ektron('input#chk-constrainProportions', container);
    foundSelectBoxs = $ektron('.select-box', container);
    foundWidthBox = $ektron('input#txtWidth', container);
    foundHeightBox = $ektron('input#txtHeight', container);

    ok(foundBlock.length > 0, 'Advanced size fieldset found.');
    equal(foundCheckbox.length, 1, 'Advanced size checkbox found.');
    equal(foundConstrainCheckbox.length, 1, 'constrain proportions checkbox found.');
    equal(foundSelectBoxs.length, 1, 'unit option for width and height and found.');
    equal(foundWidthBox.length, 1, 'width text box found.');
    equal(foundHeightBox.length, 1, 'height text box found.');
});

test('Advanced size are not set when checkbox is not checked', function () {
    "use strict";
    var constrainProportions = false,
        width = 50,
        unit = 'px',
        height = 50,
        container = $ektron('#dialog-container'),
        block = new AdvancedSizeBlock('my-div', document.getElementById('divResult'), i18n);
    clearContainer();
    myblock = block.create();
    container.append(myblock);

    $ektron('#chk-constrainProportions', container).attr('checked', constrainProportions);
    $ektron('input#txtWidth', container).val(width);
    $ektron('input#txtHeight', container).val(height);
    $ektron('.select-box', container).val(unit);
    $ektron('#chk-advancedSize', container).attr('checked', false);
    block.updateStyle();

    equal(typeof $ektron('#divResult', container).attr('style'), 'undefined', 'advanced size is set correctly.');
});

test('Advanced size are set correctly for an image when checkbox is checked', function () {
    "use strict";
    var constrainProportions = false,
        width = 50,
        unit = 'px',
        height = 50,
        container = $ektron('#dialog-container'),
        block = new AdvancedSizeBlock('my-div', document.getElementById('divResult'), i18n);
    clearContainer();
    myblock = block.create();
    container.append(myblock);

    $ektron('#chk-constrainProportions', container).attr('checked', constrainProportions);
    $ektron('input.width', container).val(width);
    $ektron('input.height', container).val(height);
    $ektron('.select-box', container).val(unit);
    $ektron('#chk-advancedSize', container).attr('checked', true);
    block.updateStyle();
    block.showHideModule($ektron('#chk-advancedSize', container).get(0));

    ok($ektron('img#divResult').attr('style').toLowerCase().indexOf('width: ' + width + unit + ';') > -1, 'image width is set correctly.');
    ok($ektron('img#divResult').attr('style').toLowerCase().indexOf('height: ' + height + unit + ';') > -1, 'image height is set correctly.');
});

test('Advanced size are set correctly for a table when checkbox is checked', function () {
    "use strict";
    var width = 70,
        unit = 'px',
        container = $ektron('#dialog-container1'),
        block = new AdvancedSizeBlock('my-div1', document.getElementById('divResult1'), i18n); 
    myblock = block.create();
    container.append(myblock);

    $ektron('input.width', container).val(width);
    $ektron('.select-box', container).val(unit);
    $ektron('#chk-advancedSize', container).attr('checked', true); 
    block.updateStyle();
    block.showHideModule($ektron('#chk-advancedSize', container).get(0));

    ok($ektron('table#divResult1').attr('style').toLowerCase().indexOf('width: ' + width + unit + ';') > -1, 'table width is set correctly.');
    equal($ektron('table#divResult1').attr('style').toLowerCase().indexOf('height: '), -1, 'table height is set correctly.');
    equal($ektron('input.height', container).is(':visible'), false, 'height field is not visible when inspecting a table.');
    equal($ektron('#chk-constrainProportions', container).is(':visible'), false, 'constrain proportions checkbox is not visible when inspecting a table.');
});

test('fields are reloaded correctly when target image contains width and height styles.', function () {
    "use strict";
    var targetElement = $ektron('#divResult2'), 
        width = 10,
        unit = '%',
        height = 10,
        container = $ektron('#dialog-container2'),
        block = new AdvancedSizeBlock('my-div2', targetElement.get(0), i18n);
    targetElement.css('width', width + unit).css('height', height + unit);
    myblock = block.create();
    container.append(myblock);

    equal($ektron('input.width', container).val(), width, 'image width is set correctly.');
    equal($ektron('input.height', container).val(), height, 'image height is set correctly.');
    equal($ektron('select.ddWidthUnit', container).val(), unit, 'image width unit select box is set correctly');
    equal($ektron('input#chk-advancedSize', container).is(':checked'), true, 'Size checkbox is set correctly');
    equal($ektron('input#chk-constrainProportions', container).is(':checked'), true, 'constrain proportions checkbox is set true by default.');
});