define(
['aloha',
    'jquery',
	'aloha/plugin',
	'ui/ui',
	'ui/button',
	'ui/toolbar',
	'ui/ui-plugin',
//'align/align-plugin',
	'aloha/console',
    'css!editInContext/css/editInContext-plugin.css'],
    function (Aloha,
            jQuery,
			Plugin,
			Ui,
			Button,
			i18n,
			i18nCore,
            console) {
        // members
        var namespace = "ektron-aloha-editInContext-";

        // create and register the Plugin
        return Plugin.create("editInContext", {
            defaults: {},

            init: function () {
                // provide locally scoped version of this for use in callbacks, etc. 
                // and define the tab we will insert the button into
                var that = this;

                // executed on plugin initialization
                this.createSaveButton();
                this.createCancelButton();
                this.triggerCallback = Ektron.Controls.Editor.Aloha.triggerCallback;
                $ektron(window).on("editInContextStarted", function () {

                    $ektron(window).on("beforeunload.ektron-editInContext", function () {
                        return Ektron.Controls.Editor.Aloha.Plugins.EditInContext.ResourceText.onbeforeunloadMessage;
                    });

                });
            },

            /* Helpers
            ----------------------------------*/
            createSaveButton: function () {
                // provide locally scoped version of this for use in callbacks, etc. 
                // and define the tab we will insert the button into
                var that = this;

                // define the library button
                this.editInContextSaveButton = Ui.adopt('save', Button, {
                    tooltip: Ektron.Controls.Editor.Aloha.Plugins.EditInContext.ResourceText.saveButtonTitle,
                    icon: 'ektron-aloha-button ' + this.nsString("saveButton"),
                    scope: 'Aloha.continuoustext',
                    click: function () {
                        that.editInContextSave();
                    }
                });
            },

            createCancelButton: function () {
                // provide locally scoped version of this for use in callbacks, etc. 
                // and define the tab we will insert the button into
                var that = this;
                this.editInContextCancelButton = Ui.adopt('cancel', Button, {
                    tooltip: Ektron.Controls.Editor.Aloha.Plugins.EditInContext.ResourceText.cancelButtonTitle,
                    icon: 'ektron-aloha-button ' + this.nsString("cancelButton"),
                    scope: 'Aloha.continuoustext',
                    click: function () {
                        that.editInContextCancel();
                    }
                });
            },

            editInContextSave: function () {
                var activeEditable = jQuery('.aloha-editable-active'),
                    contentBlockUniqueId = activeEditable.data("ektronContentblockUniqueid"),
                    contentBlockClientId = activeEditable.parent().attr("id"),
                    content = encodeURIComponent(Aloha.getActiveEditable().getContents());

                this.removeBindings();
                this.triggerCallback("save", contentBlockUniqueId, contentBlockClientId, {
                    "content": content
                });
                if (jQuery('.aloha-editable').length == 0) {
                    Aloha.Sidebar.right.hide();
                }
                $ektron('.ektron-aloha-advinspector-modal').dialog('close');
                $ektron('.ektron-aloha-advinspector-modal').parent().hide();
                $ektron(document).trigger('editInContextComplete', ['#' + contentBlockClientId]);
            },

            editInContextCancel: function () {
                var confirmation = window.confirm(Ektron.Controls.Editor.Aloha.Plugins.EditInContext.ResourceText.onbeforeunloadMessage);

                if (confirmation) {
                    var activeEditable = jQuery('.aloha-editable-active'),
                    contentBlockUniqueId = activeEditable.data("ektronContentblockUniqueid"),
                    contentBlockClientId = activeEditable.parent().attr("id");

                    this.removeBindings();
                    this.triggerCallback("cancel", contentBlockUniqueId, contentBlockClientId, {});
                    $ektron('.ektron-aloha-advinspector-modal').dialog('close');
                    $ektron('.ektron-aloha-advinspector-modal').parent().hide();
                    $ektron(document).trigger('editInContextComplete', ['#' + contentBlockClientId]);
                }
            },

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            },

            removeBindings: function () {
                $ektron(window).off("beforeunload.ektron-editInContext");
                $ektron(document).trigger("minimizeInspector");
            }
        });
    }
);