﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([
    ''
], function () {
    "use strict";
    var pluginNamespace = 'aloha-text-liststyle',
            chkliststyleid, ddListTypeid, ddListPositionid;
    function ListStyleBlock(id, obj, i18n) {
        this.id = id;
        chkliststyleid = "chkliststyle" + this.id;
        ddListTypeid = "ddListType" + this.id;
        ddListPositionid = "ddListPosition" + this.id;
        this.targetElem = {
            elem : obj,
            type : 0,
            position : 0
        };

        this.create = function () {
            var liststyleBlock, liststyleCheckbox,  liststyleLabel, liststyleGroup,
                liststyleList, ddListTypeid, liststyleSelect,
                jTarget = $ektron(this.targetElem.elem),
                jType = $ektron(jTarget).prop('tagName'),
                currStyle = jTarget.attr('style'), arrStyle,
                that = this;

            this.deserializeStyle(currStyle);
            liststyleBlock = document.createElement('fieldset');
            liststyleBlock.setAttribute('id', this.id);
            liststyleBlock.className = pluginNamespace;

            //fieldset checkbox and label
            liststyleCheckbox = $ektron('<input type="checkbox" id ="' +  chkliststyleid + '" />');
            liststyleCheckbox.on('click', function () { that.showHideModule(this); that.updateStyle(); });
            if (this.hasAlignment(currStyle)) {
                liststyleCheckbox.attr('checked', true);
            }
            liststyleBlock.appendChild(liststyleCheckbox.get(0)); 
            liststyleLabel = document.createElement('label');
            liststyleLabel.setAttribute('for', chkliststyleid);
            liststyleLabel.setAttribute('title', i18n.t('list-style.checkbox.text-liststyle'));
            liststyleLabel.innerHTML = i18n.t('list-style.checkbox.text-liststyle');
            liststyleBlock.appendChild(liststyleLabel); 

            //all styles group
            liststyleGroup = document.createElement('ul');
            //Type liststyle
            ddListTypeid = 'ddListType' + this.id;
            liststyleList = document.createElement('li');
            liststyleLabel = document.createElement('label');
            liststyleLabel.setAttribute('for', ddListTypeid);
            liststyleLabel.setAttribute('title', i18n.t('liststyle.label.text-liststyle-type'));
            liststyleLabel.className = this.nsClass('label');
            liststyleLabel.innerHTML = i18n.t('liststyle.label.text-liststyle-type');
            liststyleList.appendChild(liststyleLabel);
            liststyleGroup.appendChild(liststyleList);
            liststyleList = document.createElement('li');
            if (jType === "UL") {
                liststyleSelect = $ektron('<select id="' + ddListTypeid + '" class="select-box"><option>default</option><option>none</option><option>circle</option><option>disc</option><option>square</option></select>');
            }
            if (jType === "OL") {
                liststyleSelect = $ektron('<select id="' + ddListTypeid + '" class="select-box"><option>default</option><option>none</option><option>armenian</option><option>decimal</option><option>decimal-leading-zero</option><option>georgian</option><option>lower-alpha</option><option>lower-greek</option><option>lower-latin</option><option>lower-roman</option><option>upper-alpha</option><option>upper-latin</option><option>upper-roman</option></select>');
            }
            liststyleSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.type !== 0) {
                liststyleSelect.val(this.targetElem.type);
            }
            liststyleList.appendChild(liststyleSelect.get(0));
            liststyleGroup.appendChild(liststyleList);
            
            //Position liststyle
            liststyleList = document.createElement('li');
            liststyleLabel = document.createElement('label');
            liststyleLabel.setAttribute('for', ddListPositionid);
            liststyleLabel.setAttribute('title', i18n.t('liststyle.label.text-liststyle-position'));
            liststyleLabel.className = this.nsClass('label');
            liststyleLabel.innerHTML = i18n.t('liststyle.label.text-liststyle-position');
            liststyleList.appendChild(liststyleLabel);
            liststyleGroup.appendChild(liststyleList);
            liststyleList = document.createElement('li');
            if (jType === "UL") {
                liststyleSelect = $ektron('<select id="' + ddListPositionid + '" class="select-box"><option>default</option><option>outside</option><option>inside</option></select>');
            }
            if (jType === "OL") {
                liststyleSelect = $ektron('<select id="' + ddListPositionid + '" class="select-box"><option>default</option><option>outside</option><option>inside</option></select>');
            }
            liststyleSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.position !== 0) {
                liststyleSelect.val(this.targetElem.position);
            }
            liststyleList.appendChild(liststyleSelect.get(0));
            liststyleGroup.appendChild(liststyleList);

            liststyleBlock.appendChild(liststyleGroup); 
            if (!this.hasAlignment(currStyle)) {
                liststyleGroup.setAttribute('style', 'display:none');
            }
            return liststyleBlock;
        };

        this.updateStyle = function () {
            var styleString = "", jResult = $ektron(this.targetElem.elem), listStyleType, listStylePosition;
            jResult.css('list-style-type', "").css('list-style-position', "");
            if (true === document.getElementById(chkliststyleid).checked) {
                listStyleType = $ektron('#' + ddListTypeid).val().toLowerCase();
                if (listStyleType !== 'default') {
                    jResult.css('list-style-type', listStyleType);
                }
                listStylePosition = $ektron('#' + ddListPositionid).val().toLowerCase();
                if (listStylePosition !== 'default') {
                    jResult.css('list-style-position', listStylePosition);
                }
            }                
        };

        this.deserializeStyle = function (currStyle) {
            var arrStyle, arrProperty, i, strStyle;
            if (typeof currStyle !== 'undefined' && currStyle !== '') {
                arrStyle = currStyle.split(';');
                for (i = 0; i < arrStyle.length; i += 1)
                {
                    strStyle = $ektron.trim(arrStyle[i]);
                    if (0 === strStyle.indexOf('list-style-type:'))
                    {
                        arrProperty = strStyle.split(' ');
                        this.targetElem.type = arrProperty[1]; 
                    }

                    if (0 === strStyle.indexOf('list-style-position:')) {
                        arrProperty = strStyle.split(' ');
                        this.targetElem.position = arrProperty[1]; 
                    }

                }
            }
        };

        this.hasAlignment = function () {
            if (this.targetElem.type !== 0 || this.targetElem.position !== 0 ) {
                return true;
            }
            return false;
        };

        this.showHideModule = function (elem) {
            $ektron(elem).siblings('ul').toggle();
        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function ()
        {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function ()
            {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return ListStyleBlock;
});