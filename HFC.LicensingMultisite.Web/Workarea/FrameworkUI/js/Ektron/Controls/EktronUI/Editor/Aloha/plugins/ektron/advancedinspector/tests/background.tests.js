﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="../../../../lib/vendor/ektron/ektron.aloha.dialog.js" />
/// <reference path="setup.js" />
/// <reference path="../lib/background.js" />

var block, block1, myblock1, myblock, Background, $ektron, i18njs, cph,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs: { "background.checkbox.label": "Background" },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    },
    cph = function ColorHelper(textElem, obj) {
        "use strict";
        this.textElem = textElem;
        this.obj = obj;
        this.hexColor = "";

        this.create = function () {
            return this;
        };

        // selectedColor is rgb value
        this.start = function (selectedColor) {
            var that = this;
            that.hexColor = that.hexc(selectedColor);
            $ektron(textElem).val(that.hexColor);
            //$ektron(textElem).val(that.hexColor);
            //Initialize colorpicker
            //debugger;
            $(textElem).minicolors({
                change: function (hex, opacity) {
                    //debugger;
                    $.event.trigger({
                        type: "ekcolorChanged",
                        message: hex || 'transparent',
                        obj: $(this)
                    });
                }
            });
            $(textElem)
        };

        this.hexc = function (colorval) {
            //translate color value rgb to hex - if no rgb value return white
            var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            if (parts) {
                delete (parts[0]);
                for (var i = 1; i <= 3; ++i) {
                    parts[i] = parseInt(parts[i]).toString(16);
                    if (parts[i].length == 1) parts[i] = '0' + parts[i];
                }
                return parts.join('');
            }
            else {
                return '';
            } 
        }
        return (this);
    };

module("Background ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        Background = window.createModule(i18n, cph);
        block = new Background();
        //block1 = new Background();
    }
});

test('Background fieldset created successfully', function () {
    "use strict";
    var foundObj = false,
        container = $ektron('#dialog-container'),
        block = new Background('my-div', document.getElementById('divResult'), i18n, cph);
    myblock = block.create();
    container.append(myblock);
    block.start();

    foundObj = $ektron('#ektron-aloha-background-textmy-div', container);
    ok(foundObj.length > 0, 'Background fieldset found.');

});

test('Background fieldset created successfully', function () {
    "use strict";
    var foundObj = false,
        container = $ektron('#dialog-container'),
        block1 = new Background('my-div1', document.getElementById('divResult1'), i18n, cph);
    myblock1 = block1.create();
    container.append(myblock1);
    block1.start();

    foundObj = $ektron('#ektron-aloha-background-textmy-div1', container);
    ok(foundObj.length > 0, 'Background fieldset found.');

});



