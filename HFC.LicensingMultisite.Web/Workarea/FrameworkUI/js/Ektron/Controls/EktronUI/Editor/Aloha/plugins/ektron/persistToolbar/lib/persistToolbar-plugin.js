﻿define([
  'aloha',
  'jquery',
  'aloha/plugin',
  'ui/scopes',
  'ui/ui-plugin',
  'css!./css/persistToolbar-plugin'
], function (
  Aloha,
  jQuery,
  Plugin,
  Scopes,
  UiPlugin
) {

    /**
     * register the plugin with unique name
     */
    return Plugin.create('persistbar', {
        /**
         * Add an event handler to ensure toolbar persistence.
         */
        init: function () {
            var once = true,
                that = this;
            Aloha.bind('aloha-editable-created', function (event, editable) {
                if (once) {
                    var panel;
                    if ($('div#dvContent').length) {
                        panel = $('div#dvContent').before('<div id="alohaContainer"></div>');  
                    } else if (($('#liContent').length == 0) && ($('#liSummary').length)) {
                        panel = $('div#dvSummary').prepend('<div id="alohaContainer"></div>');
                    } else {
                        panel = $('div.ektronPageContainer').prepend('<div id="alohaContainer"></div>');
                    }
                    $('.ektron-aloha', panel).css('height', '99%');
                    $('.aloha-editable', panel).css('height', '99%');
                    if (editable.getContents() == "") {
                        var panelHeight = editable.obj.closest('.ui-tabs-panel').height() - 5;
                        var ua = navigator.userAgent;
                        if (ua.indexOf('Trident/') > -1 && ua.indexOf('rv:') > -1) {
                            editable.obj.css('min-height', panelHeight + 'px');
                        }
                        if (true == Aloha.jQuery.browser.msie) {
                            editable.setContents("<p></p>");
                        }
                        else {
                            editable.setContents("<p><br/></p>");
                        }
                    }
                    once = false;
                }

                editable.activate();

                var toolbar = jQuery('.aloha-toolbar');
                var toolbarChild = jQuery('.aloha-ui-toolbar');
                var tabs = jQuery('.aloha-ui .ui-tabs-nav');

                tabs.css({ 'cursor': 'default', 'background': '#777 !important;', 'background': 'rgba(0,0,0,0.3) !important;', 'background-image': 'linear-gradient(bottom,rgba(0,0,0,0.35) 0,rgba(0,0,0,0.3) 20%) !important;', 'background-image': '-o-linear-gradient(bottom,rgba(0,0,0,0.35) 0,rgba(0,0,0,0.3) 20%) !important;', 'background-image': '-moz-linear-gradient(bottom,rgba(0,0,0,0.35) 0,rgba(0,0,0,0.3) 20%) !important;', 'background-image': '-webkit-linear-gradient(bottom,rgba(0,0,0,0.35) 0,rgba(0,0,0,0.3) 20%) !important;', 'background-image': '-ms-linear-gradient(bottom,rgba(0,0,0,0.35) 0,rgba(0,0,0,0.3) 20%) !important;' });
                tabs.on('mouseout', function () {
                    tabs.css({ 'cursor': 'default', 'background': '#777 !important;', 'background': 'rgba(0,0,0,0.3) !important;', 'background-image': 'linear-gradient(bottom,rgba(0,0,0,0.35) 0,rgba(0,0,0,0.3) 20%) !important;', 'background-image': '-o-linear-gradient(bottom,rgba(0,0,0,0.35) 0,rgba(0,0,0,0.3) 20%) !important;', 'background-image': '-moz-linear-gradient(bottom,rgba(0,0,0,0.35) 0,rgba(0,0,0,0.3) 20%) !important;', 'background-image': '-webkit-linear-gradient(bottom,rgba(0,0,0,0.35) 0,rgba(0,0,0,0.3) 20%) !important;', 'background-image': '-ms-linear-gradient(bottom,rgba(0,0,0,0.35) 0,rgba(0,0,0,0.3) 20%) !important;' });
                });
                toolbarChild.on('dragstart', function (e) {
                    e.preventDefault();
                    return false;
                });

                toolbar.clearQueue();
                toolbar.stop();
                toolbarChild.clearQueue();
                toolbarChild.stop();

                $(document).trigger('RepositionToolbar');
                jQuery('#alohaContainer').append(toolbar);
                Scopes.setScope('Aloha.continuoustext');
                //console.dir(UiPlugin);
                UiPlugin.showToolbar();
                Aloha.bind('aloha-editable-deactivated', function () {
                    UiPlugin.showToolbar();
                });
            });

            var containerHeight = $('#editContentPageContainer').height();
            $('div#dvContent').height(containerHeight - 260);
            $('div#dvSummary').height(containerHeight - 260);

            $(window).resize(function () {
                var toolbarChild = jQuery('.aloha-ui-toolbar');
                var containerHeight = $('#editContentPageContainer').height();
                var content = $('div#dvContent');
                var summary = $('div#dvSummary');

                content.height(containerHeight - 260);
                summary.height(containerHeight - 260);
            });

            $('.Ektron_Dialog_Tabs_BodyContainer').on('click', function () {
                var editable = $('.Ektron_Dialog_Tabs_BodyContainer .aloha-editable');
                that.setCurrentEditorFocus(editable, 0);
            });

            $('div#dvContent').on('click', function () {
                that.setContentEditorActive();
            });
            $('div#dvSummary').on('click', function () {
                that.setSummaryEditorActive();
            });

            //Make sure the toolbar only shows when it's suppose to
            $('#liContent a, #liSummary a').on('click', function () {
                var toolbar = jQuery('.aloha-toolbar');
                toolbar.show();
            });

            $('#liContent a').on('click', function () {
                if (that.nonHTMLContentPage()) {
                    $('div#alohaContainer').hide();
                } else {
                    $('div#alohaContainer').show();
                }
                that.setContentEditorActive();
            });

            $('#liSummary a').on('click', function () {
                if ($('#dvSummary table').length) {
                    $('div#alohaContainer').hide();
                    $('#dvSummary table div.ektron-aloha div').css({ '-moz-appearance': 'textfield-multiline', '-webkit-appearance': 'textarea', 'border': '1px solid gray', 'font': 'medium -moz-fixed', 'font': '-webkit-small-control', 'height': '28px', 'overflow': 'auto', 'padding': '2px', 'resize': 'both', 'width': '400px' })
                } else {
                    var toolbar = jQuery('.aloha-toolbar'), panel = $('div#dvSummary');
                    $(document).trigger('InsertSpanButton');
                    $(document).trigger('RepositionToolbar');
                    toolbar.show();
                    $('div#alohaContainer').show();
                    that.setSummaryEditorActive();
                    $('#_dvSummaryStandard', panel).css('height', '99%');
                    $('.ektron-aloha', panel).css('height', '99%');
                    $('.aloha-editable', panel).css('height', '99%');
                }
            });

            $('#liMetadata a, #liAlias a, #liSchedule a, #liComment a, #liTemplates a, #liTaxonomy a, #liSubscription a, .publishButton, .undoCheckout, .cancelButton').on('click', function () {
                var toolbar = jQuery('.aloha-toolbar');
                toolbar.hide();
            });

            $(document).on('RepositionToolbar', function () {
                $(document).trigger('TidyUpToolbarButtons');
                $('.aloha-ui-toolbar').css( { 'position': 'relative', 'margin': 'auto', 'width': '100%', 'top': '11px;' } ).addClass("aloha-ui-hover");
                $('.aloha-toolbar .aloha-ui-pin').hide();
            });

            setTimeout(function () {
                if (that.nonHTMLContentPage()) {
                    $('div#alohaContainer').hide();
                    $('div#advInspectorm').parent().hide();
                }
                else {
                    $('#content_title').click();
                }
            }, 1000);
        },

        setContentEditorActive: function () {
            var editable = $('div#dvContent .aloha-editable');
            if (this.nonHTMLContentPage()) {
                this.setCurrentEditorFocus(editable, 0);
            }
        },

        setSummaryEditorActive: function () {
            if ($('#dvSummary table div.ektron-aloha div') == 0) {
                var editable = $('div#dvSummary .aloha-editable'), editorIdx = 1;
                if (this.nonHTMLContentPage()) {
                    editorIdx = 0;
                }
                this.setCurrentEditorFocus(editable, editorIdx);
                if ('' === editable.html() || '<br>' == editable.html()) { // IE 11 have '<br>' in the content without P tags
                    var panelHeight = editable.closest('.ui-tabs-panel').height() - 5;
                    var ua = navigator.userAgent;
                    if (ua.indexOf('Trident/') > -1 && ua.indexOf('rv:') > -1) {
                        editable.css('min-height', panelHeight + 'px');
                    }
                    if (true == Aloha.jQuery.browser.msie) {
                        editable.html("<p></p>");
                    }
                    else {
                        editable.html("<p><br/></p>");
                    }
                }
            }
        },

        setCurrentEditorFocus: function (editable, currIdx) {
            if (!editable.hasClass('aloha-editable-active')) {
                Aloha.editables[currIdx].obj.dblclick(); // need double click for IE. first click to set object, second click to get object in focus.
                Aloha.editables[currIdx].obj.focus();
                Aloha.editables[currIdx].activate();
            }
            Aloha.activeEditable = Aloha.editables[currIdx];
        },

        nonHTMLContentPage: function () {
            if ($('span#fileUploadWrapper').length || $('a#switchversion').length || $('input#media_fileName').length) {
                return true;
            }
            else {
                return false;
            }
        }
    });
});