﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/responsive-image.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, RespImgBlock, $ektron, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs : { "responsive-image.checkbox.responsive-image-selection": "Responsive Image Selection", "responsive-image.label.change-button": "Change",
        "responsive-image.label.no-breakpoints-found": "No breakpoint found in the system.", "responsive-image.dialog.title": "Select Responsive Image for " },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("Responsive Image Selection", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        RespImgBlock = window.createModule(i18n);
        block = new RespImgBlock();
    }
});

test('Responsive Image Selection fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundCheckbox, foundCarousel, foundButtons,
        ajaxMsg = '{"message":"Object reference not set to an instance of an object.","innerMessage":""}',
        container = $ektron('#dialog-container'),
        block = new RespImgBlock('my-div', document.getElementById('divResult'), i18n);
    block.getBreakpoints = function () {
        return block.createBreakpointsCarousel($ektron.parseJSON(ajaxMsg));
    };
    myblock = block.create();
    container.append(myblock);

    foundBlock = $ektron('#my-div', container);
    foundCheckbox = $ektron('input#chk-responsive-image', container);
    foundCarousel = $ektron('ul.jcarousel', container);
    foundButtons = $ektron('.change-btn', container);

    ok(foundBlock.length > 0, 'Responsive Image Selection fieldset found.');
    equal(foundCheckbox.length, 1, 'Responsive Image Selection checkbox found.');
    ok(foundCarousel.length > 0, 'carousel block found.');
    //block.init();
    equal(foundButtons.length, 0, 'changed button for each carousel image is not shown.');
});

test('Responsive Image Selection with Carousel avaiable when breakpoint info is retrieved.', function () {
    "use strict";
    var foundBlock, foundCheckbox, foundCarousel, foundButtons,
        ajaxMsg = '{"WidthPixelSpec":[480,750,1024,1035],"BreakpointDataList":[{"Name":"Smart Phones","Width":480},{"Name":"Small Tablet","Width":750},{"Name":"Tablets","Width":1024},{"Name":"Desktop","Width":1035}],"enableDeviceDetection":"true"}',
        thisId = 'my-div1',
        container = $ektron('#dialog-container1'),
        block = new RespImgBlock(thisId, document.getElementById('divResult1'), i18n);
    block.getBreakpoints = function () {
        return block.createBreakpointsCarousel($ektron.parseJSON(ajaxMsg));
    };
    myblock = block.create();
    container.append(myblock);

    foundBlock = $ektron('#' + thisId, container);
    foundCheckbox = $ektron('input#chk-responsive-image', container);
    foundCarousel = $ektron('ul.aloha-responsive-image-breakpoints', container);
    foundButtons = $ektron('.change-btn', container);

    ok(foundBlock.length > 0, 'Responsive Image Selection fieldset found.');
    equal(foundCheckbox.length, 1, 'Responsive Image Selection checkbox found.');
    ok(foundCarousel.length > 0, 'carousel block found.');
    block.init();
    equal(foundButtons.length, 4, 'correct number of changed button in the carousel.');
});

test('Responsive Image Selection is updated when ui dialog provide a new image when checkbox is checked.', function () {
    "use strict";
    var respImgChecked = true,
        breakpointWidth = 480,
        breakpointName = 'Smart Phones',
        newImgSrc = 'c2937_u_marketing.jpg',
        newImg = {
            "id": "test",
            "title": "test",
            "altText": "test",
            "path": "/uploadedimages/" + newImgSrc,
            "width": "0",
            "height": "0"
        },
        thisId = 'my-div2',
        container = $ektron('#dialog-container2'),
        block = new RespImgBlock(thisId, document.getElementById('divResult2'), i18n);
    block.widthPixelSpec = [480, 1024];
    block.targetElem = {
        elem : document.getElementById('divResult2'),
        title : 'test',
        tagName : document.getElementById('divResult2').tagName,
        imgPath : document.getElementById('divResult2').getAttribute('src')
    };
    $ektron('.aloha-responsive-image-breakpoints', container).jcarousel({
        visible: 3,
        itemFallbackDimension: 100
    });

    //set dialog fields by users
    $ektron('#chk-responsive-image', container).attr('checked', respImgChecked);
    block.currentBreakpoint = {
        imgElem: $ektron('span.thumb[data-ektron-breakpointwidth=' + breakpointWidth + ']', container),
        id: breakpointName.replace(/\s/, "") + '_' + thisId + '_' + breakpointWidth + '-change-btn',
        name: breakpointName,
        width: breakpointWidth
    };
    block.init();
    block.acceptInsert(newImg);
    block.updateStyle();

    ok($ektron('span.thumb[data-ektron-breakpointwidth=' + breakpointWidth + ']', container).css('background-image').toLowerCase().indexOf(newImgSrc.toLowerCase()) > -1, 'new responsive image is inserted correctly.');
    ok($ektron('#divResult2').get(0).tagName === 'FIGURE', 'responsive image is updated correctly.');
});

test('checkbox is reloaded correctly when target element is a FIGURE element.', function () {
    "use strict";
    var targetElement = $ektron('#divResult3'), 
        dataMediaPath, dataMediaWidth, jThis, carouselIdx, correctThumb,
        container = $ektron('#dialog-container3'),
        block = new RespImgBlock('my-div3', targetElement.get(0), i18n);
    targetElement.picture();
    myblock = block.create();
    container.append(myblock);
    block.init();

    equal($ektron('#chk-responsive-image', container).is(':checked'), true, 'value for responsive image selection is loaded correctly.');
    $ektron('.thumb', container).each(function (index) {
        jThis = $ektron(this);
        correctThumb = block.getImageSrc(jThis.attr('data-ektron-breakpoint-image-src'));
        ok(jThis.css('background-image').toLowerCase().indexOf(correctThumb.toLowerCase()) > -1, 'carousel image is loaded correctly.');
    });
    // NOTE: block.init() is needed for the following to be defined.
    ok(typeof Ektron.AdvancedInspector.ResponsiveImage.AcceptInsert !== 'undefined', 'the namespace and the AcceptInsert function is defined for dialog call.');
});