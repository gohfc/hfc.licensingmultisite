﻿
/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/text-treatnment.js" />
/// <reference path="../../../../lib/vendor/ektron/ColorPicker/minicolors.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, TextTreatmentBlock, $ektron, Aloha, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs: {
            "text-treatment.checkbox.label": "Text Treatment",
            "text-treatment.label.font-family": "Font Family:",
            "text-treatment.label.font-size": "Font Size:",
            "text-treatment.label.font-color": "Color:",
            "text-treatment.label.font-weight": "Font Weight:",
            "text-treatment.label.font-style": "Font Style:",
            "text-treatment.label.decoration": "Decoration:",
            "text-treatment.label.transform": "Transform:",
            "text-treatment.label.line-height": "Line Height:"
        },
        t: function (index) {
            return this.i18njs[index];
        }
    },
    cph = function ColorHelper(textElem, obj) {
        "use strict";
        this.textElem = textElem;
        this.obj = obj;
        this.hexColor = "";

        this.create = function () {
            return this;
        };

        // selectedColor is rgb value
        this.start = function (selectedColor) {
            var that = this;
            that.hexColor = that.hexc(selectedColor);
            $ektron(textElem).val(that.hexColor);
            //$ektron(textElem).val(that.hexColor);
            //Initialize colorpicker
            //debugger;
            $(textElem).minicolors({
                change: function (hex, opacity) {
                    //debugger;
                    $.event.trigger({
                        type: "ekcolorChanged",
                        message: hex || 'transparent',
                        obj: $(this)
                    });
                }
            });
            $(textElem)
        };

        this.hexc = function (colorval) {
            //translate color value rgb to hex - if no rgb value return white
            var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            if (parts) {
                delete (parts[0]);
                for (var i = 1; i <= 3; ++i) {
                    parts[i] = parseInt(parts[i]).toString(16);
                    if (parts[i].length == 1) parts[i] = '0' + parts[i];
                }
                return parts.join('');
            }
            else {
                return '';
            } 
        }
        return (this);
    };

Aloha = {
    settings: {
        fontfamilies: [
            'Georgia,serif',
            '"Palatino Linotype","Book Antiqua",Palatino, serif',
            '"Times New Roman",Times,serif',
            'Arial,Helvetica,sans-serif',
            '"Arial Black",Gadget,sans-serif',
            '"Comic Sans MS",cursive,sans-serif',
            '"Lucida Sans Unicode,Lucida Grande",sans-serif',
            'Tahoma, Geneva,sans-serif',
            '"Trebuchet MS",Helvetica, sans-serif',
            'Verdana, Geneva,sans-serif',
            '"Courier New",Courier,monospace',
            '"Lucida Console",Monaco,monospace'
        ]
    }
};
module("Text treatment style ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        TextTreatmentBlock = window.createModule(i18n, cph);
        block = new TextTreatmentBlock();
    }
});

test('Text treatment fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundCheckbox, foundFontFamilydd, foundFontSizedd, foundFontWeightdd, foundFontStyledd, foundDecorationdd, foundTransformdd, foundLineHeightdd,
        container = $ektron('#dialog-container'),
        block = new TextTreatmentBlock('my-div', document.getElementById('divResult'), i18n, cph),
        foundIndentBox, foundSelectBox;
    myblock = block.create();
    container.append(myblock);
    block.start();

    foundBlock = $ektron('#my-div', container);
    foundCheckbox = $ektron('#chk-treatmentmy-div', container);
    foundFontFamilydd = $ektron('#ddFontFamily', container);
    foundFontSizedd = $ektron('#ddFontSizeUnit', container);
    foundFontWeightdd = $ektron('#ddFontWeight', container);
    foundFontStyledd = $ektron('#ddFontStyle', container);
    foundDecorationdd = $ektron('#ddDecoration', container);
    foundTransformdd = $ektron('#ddTransform', container);
    foundLineHeightdd = $ektron('#ddLineHeightUnit', container);


    ok(foundBlock.length > 0, 'Text treatment fieldset found.');
    equal(foundCheckbox.length, 1, 'Text treatment checkbox found.');
    ok(foundFontFamilydd.length > 0, 'Options for font family found.');
    ok(foundFontSizedd.length > 0, 'Options for font size found.');
    ok(foundFontWeightdd.length > 0, 'Options for font weight found.');
    ok(foundFontStyledd.length > 0, 'Options for font style found.');
    ok(foundTransformdd.length > 0, 'Options for transform found.');
    ok(foundLineHeightdd.length > 0, 'Options for line heightfound.');

});
