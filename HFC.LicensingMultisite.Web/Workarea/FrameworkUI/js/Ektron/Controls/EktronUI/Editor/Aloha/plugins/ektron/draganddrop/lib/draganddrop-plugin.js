/*!
* Aloha Embed Plugin
* -----------------
* This plugin provides an interface to allow the user to access the CMS400 embed, 
* and to add assests from the template to the editable container.
* It presents its user interface in the Toolbar and a modal dialog.
*
* Additional dependancies not provided via RequireJS
*     Ektron.Namespace.js
*     Ektron.Controls.Editor.Aloha.js
*     jQuery UI Dialog  
*/

// define the Source Viewer module using RequireJS
define([
    'aloha',
    'aloha/plugin',
    'i18n!aloha/nls/i18n',
    'i18n!draganddrop/nls/i18n',
    'draganddrop/ektrondnd',
    'css!./css/draganddrop',
    'vendor/ektron/jquerypicture/jquery-picture-min'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
			Plugin,
            i18nCore,
            i18n) {
        // members
        var namespace = "ektron-aloha-draganddrop-", folderid = '0';


        // create and register the Plugin
        return Plugin.create("draganddrop", {
            defaults: {},
            lastInserted: null,
            self: null,

            init: function () {
                var that = this, path = Ektron.Context.Cms.UIPath, widthPixelSpec = null, folderid = '0';
                var waiting = $ektron('<div id="ektron-aloha-uploadingbox" class="ektron-ui-control ui-corner-all ektron-ui-message ektron-ui-clearfix" style="display:none;position:fixed;top:0;width:100%;z-index:999999999999;text-align:center;font-weight:normal;font-size:14px;font-weight:bold;padding:5px;"></div>');
                $ektron('body').append(waiting);

                Aloha.bind('aloha-editable-created', function (e, editable) {
                    var activeId = editable.obj[0];
                    $ektron(activeId).on('drop.dnd', function (e) {
                        that.fileUploadError("Editable must be activated before you can drag and drop.");
                        return false;
                    });
                });
                //var fid = Aloha.activeEditable.obj.data("ektronEditorData");

                Aloha.bind('aloha-editable-activated', function (event, myeditable) {
                    var dndCallBack = function () {
                        var activeId = myeditable.editable.obj[0];
                        if (Aloha.activeEditable != null && Aloha.activeEditable.obj != null && Aloha.activeEditable.obj.data("ektronEditorData") != null) {
                            folderid = Aloha.activeEditable.obj.data("ektronEditorData").folderId.toString();
                            $ektron(activeId).off('drop.dnd');
                            $ektron(activeId).EktronDND({ uploadFinished: that.fileUploadFinished, onError: that.fileUploadError, onMsg: that.fileUploadMsg, folderId: folderid, path: Ektron.Context.Cms.UIPath });
                            return;
                        }

                        setTimeout(dndCallBack, 100);
                    };

                    dndCallBack();
                });
            },

            fileUploadFinished: function (src, cordX, cordY) {
                var that = this, path = '/', breakpointsInfo = null;
                var folderid = Aloha.activeEditable.obj.data("ektronEditorData").folderId.toString();
                $ektron.ajax({
                    type: "POST",
                    cache: false,
                    async: false,
                    url: Ektron.Context.Cms.UIPath + "/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/advancedinspector/resources/advancedinspector.ashx",
                    data: { "action": "getdevicebreakpointdatalist", "folderid": folderid },
                    success: function (msg) {
                        breakpointsInfo = $ektron.parseJSON(msg);
                    }
                });
                if (breakpointsInfo != null && breakpointsInfo.folderPath != null) {
                    path = breakpointsInfo.folderPath.replace(/\\/g, "/");
                }
                if (path == "//") {
                    path = "/";
                }
                var range = document.createRange();

                path = path.replace(/ /g, "_");
                var upImage = new Image();
                upImage.src = Ektron.Context.Cms.SitePath + '/uploadedimages' + path + src;

                if (document.caretPositionFromPoint) { //FF
                    var pos = document.caretPositionFromPoint(cordX, cordY);
                    if (pos != null) {
                        range.setStart(pos.offsetNode, pos.offset);
                    } else {
                        range = null;
                    }
                    range.collapse(false);
                    range.insertNode(upImage);
                }
                    // Next, the WebKit way
                else if (document.caretRangeFromPoint) {
                    range = document.caretRangeFromPoint(cordX, cordY);
                    range.collapse(false);
                    range.insertNode(upImage);
                }
                else if (document.body.createTextRange) { //IE
                    var output = jQuery(upImage), range = Aloha.Selection.getRangeObject(),
                        selectedEdit = jQuery('.aloha-editable-active');
                    if (0 == selectedEdit.length) {
                        selectedEdit = jQuery(range.commonAncestorContainer);
                    }
                    // range.collapse == true for drag and drop
                    GENTICS.Utils.Dom.insertIntoDOM(output, range, selectedEdit);
                }

                var breakpoints = null;
                $ektron.ajax({
                    type: "POST",
                    cache: false,
                    async: false,
                    url: Ektron.Context.Cms.UIPath + "/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/advancedinspector/resources/advancedinspector.ashx",
                    data: { "action": "getdevicebreakpointdatalist" },
                    success: function (msg) {
                        breakpoints = $ektron.parseJSON(msg);
                    }
                });

                if (breakpointsInfo != null && breakpointsInfo.WidthPixelSpec != null && breakpointsInfo.enableDeviceDetection == true) {
                    var targetElem = upImage;

                    var output = "", jOutput, jResult = $(upImage), i, nextWidth = '', imgPath, defaultPath, prevPath, jFigureSet;
                    var output = '<figure draggable="false" contenteditable="false" class="ektron-responsive-imageset fancy" title="' + targetElem.title + '" data-ektron-image-src="' + targetElem.src + '" ';
                    if ('string' === typeof jResult.attr('id') && jResult.attr('id') !== 'undefined') {
                        output += 'id="' + jResult.attr('id') + '" ';
                    }
                    //each breakpoint 
                    for (i = 0; i < breakpointsInfo.WidthPixelSpec.length; i += 1) {
                        imgPath = Ektron.Context.Cms.SitePath + '/uploadedimages' + path + src + '?targetTypeId=' + breakpointsInfo.BreakpointDataList[i].Name;
                        if (i + 1 == breakpointsInfo.WidthPixelSpec.length) {
                            imgPath = imgPath.substr(0, imgPath.indexOf('?'));
                        }
                        if (prevPath !== imgPath) {
                            output += 'data-media' + nextWidth + '="' + imgPath + '" ';
                            prevPath = imgPath;
                        }
                        nextWidth = breakpointsInfo.WidthPixelSpec[i] + 1;
                    }
                    output += '>';
                    output += '<noscript>';
                    output += '<img src="' + targetElem.src + '" alt="' + targetElem.title + '"/>';
                    output += '</noscript>';
                    output += '</figure>';
                    jOutput = $ektron(output);
                    jResult.replaceWith(jOutput);
                    jFigureSet = $ektron('figure.ektron-responsive-imageset');
                    //jFigureSet.children().attr('draggable', 'true')
                    if (jFigureSet.length > 0) {
                        jFigureSet.picture();
                        jFigureSet.find('img').css('max-width', '100%');
                        if (Ektron.Namespace.Exists("Ektron.AdvancedInspector.ImageSetBlock.ImageSetInit")) {
                            //Ektron.AdvancedInspector.ImageSetBlock.ImageSetInit(jFigureSet);
                        }
                    }
                }
                else {
                    // let the browser figure out the dimensions. still need works.
                    var jImg = $ektron(upImage), url = jImg.attr('src');
                    jImg.attr('title', url).attr('data-ektron-url', url).prop('width', null).removeAttr('width').prop('height', null).removeAttr('height').addClass('fancy');
                }
                window.DragonDrop.bindDraggables();
                $ektron('#ektron-aloha-uploadingbox').hide();
                //$("img").draggable();
            },
            fileUploadError: function (error) {
                $ektron('#ektron-aloha-uploadingbox').removeClass('ektron-ui-success').addClass('ektron-ui-error').text(error).show().fadeOut(5000);
            },
            fileUploadMsg: function (msg) {
                $ektron('#ektron-aloha-uploadingbox').removeClass('ektron-ui-error').addClass('ektron-ui-success').text(msg).show();
            },
            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            }
        });
    }
);