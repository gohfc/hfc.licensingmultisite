/* 
* Aloha Editor is a WYSIWYG HTML5 inline editing library and editor. 
* Copyright (c) 2010-2012 Gentics Software GmbH, Vienna, Austria.
* Contributors http://aloha-editor.org/contribution.php 
* 
* Aloha Editor is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or any later version.
*
* Aloha Editor is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
* 
* As an additional permission to the GNU GPL version 2, you may distribute
* non-source (e.g., minimized or compacted) forms of the Aloha-Editor
* source code without the copy of the GNU GPL normally required,
* provided you include this license notice and a URL through which
* recipients can access the Corresponding Source.
*/
/* Ektron Aloha Sourceview Plugin
* -----------------
* This plugin allows users of the Aloha Editor to view the raw source of the area being edited, 
* and provides find and replace functionality as well.
*/
define([
	'aloha',
    'jquery',
    'aloha/plugin',
    'ui/ui',
	'ui/button',
    'ace/ace',
    'vendor/ektron/beautify/beautify-html',
    'css!sourceview/css/sourceview-plugin'],
    // callback executed once dependencies are loaded by RequireJS
    function (Aloha,
	    $,
	    Plugin,
	    Ui,
	    Button,
        ace) {
        // members
        var namespace = "ektron-aloha-sourceview-",
            activeEditable,
            contentPanelId = '',
            contentPanel,
            contentPanel2,
            sourceviewDialog,
            findReplaceExpand,
            markerClass = '',
            panelTitle = '',
            resourceText = '',
            uiPanel = '',
            editor,
            that;
        // create and register the Plugin

        return Plugin.create("sourceview", {
            defaults: {},

            init: function () {
                var that = this;
                $(document).ready(function () {
                    // assing string values
                    that.panelTitle = '';
                    that.contentPanelId = that.nsString("contentPanel");
                    that.markerClass = that.nsString("marker");
                    that.resourceText = Ektron.Controls.Editor.Aloha.Plugins.SourceViewer.ResourceText;
                    that.uiPanel = that.nsString("uiPanel");

                    // create the Aloha button to launch the source viewer
                    that.createButton();
                    if (!($ektron(".ektron-aloha-sourceview-modal").is(".ui-dialog"))) {
                        that.sourceviewDialog = that.createModal();
                    }
                    that.sourceviewDialog.parents('.ui-dialog').find('.ui-dialog-titlebar button').remove();
                    var titlebar = that.sourceviewDialog.parents('.ui-dialog').find('.ui-dialog-titlebar');
                    $('<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title=""><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text"><span aria-hidden="true" title="close" data-ux-icon="&#xe01a;"></span></span></button>')
                        .appendTo(titlebar)
                        .click(function () {
                            that.sourceviewDialog.dialog('close');
                });
                });
            },

            /* Helpers
            ----------------------------------*/
            addFindReplace: function () {
                var that = this;

                $.fn.selectRange = function (start, end) {
                    return this.each(function () {
                        if (this.setSelectionRange) {
                            this.focus();
                            this.setSelectionRange(start, end);
                        } else if (this.createTextRange) {
                            var range = this.createTextRange();
                            range.collapse(true);
                            range.moveEnd('character', end);
                            range.moveStart('character', start);
                            range.select();
                        }
                    });
                };

                $.fn.getCursorPosEnd = function () {
                    var pos = 0;
                    var input = this.get(0);
                    // IE Support
                    if (document.selection) {
                        input.focus();
                        var sel = document.selection.createRange();
                        pos = sel.text.length;
                    }
                        // Firefox support
                    else if (input.selectionStart || input.selectionStart == '0')
                        pos = input.selectionEnd;
                    return pos;
                };
            },

            bindInteractions: function () {
                var that = this;
                // now that this is a dialog do we still need this?
                that.contentPanel.live('keyup', function (event) {
                    that.updateEditable();
                });

                $('#' + that.uiPanel + ' #FindBtn').button().on('click', function () {
                    if ($(this).val() != content) {
                        content = $('#FindTxt').val();
                        if ($(this).val().length > 0) {
                            that.SAR.find(content);
                        }
                    }
                });

                $('#' + that.uiPanel + ' #ReplaceBtn').button().on('click', function () {
                    if ($(this).val() != content) {
                        content = $('#FindTxt').val();
                        replacec = $('#ReplaceTxt').val();
                        if ($(this).val().length > 0) {
                            that.SAR.findAndReplace(content, replacec);
                            that.updateEditable();
                        }
                    }
                });

                $('#' + that.uiPanel + ' #ReplaceAllBtn').button().on('click', function () {
                    if ($(this).val() != content) {
                        content = $('#FindTxt').val();
                        replacec = $('#ReplaceTxt').val();
                        if ($(this).val().length > 0) {
                            that.SAR.replaceAll(content, replacec);
                            that.updateEditable();
                        }
                    }
                });

                $('#' + that.uiPanel + ' #FindReplacePlus').on('click', function (event) {
                    that.contentPanel.toggleClass('animated');
                    that.findReplaceExpand.toggleClass('animated');
                });

                $('#' + that.uiPanel + ' #FindTxt').on('keyup', function (event) {
                    if (event.keyCode == 13) {
                        $('#' + that.uiPanel + ' #FindBtn').click();
                        event.stopPropagation();
                    }                    
                });

                $('#' + that.uiPanel + ' #ReplaceTxt').on('keyup', function (event) {
                    if (event.keyCode == 13) {
                        $('#' + that.uiPanel + ' #ReplaceBtn').click();
                        event.stopPropagation();
                    }
                });
            },

            closeDialog: function () {
                var that = this;                
                that.sourceviewDialog.empty();
                Aloha.activateEditable(that.activeEditable);

                if (null === Aloha.getActiveEditable()) {
                    for (i = 0; i < Aloha.editables.length; i++) {
                        if (Aloha.editables[i].obj.is(':visible')) {
                            Aloha.editables[i].obj.focus();
                            Aloha.editables[i].activate();
                            break;
                }
                    }
                }
                else {
                    if ('' == Aloha.getActiveEditable().obj.html()) {
                        if ('undefined' == typeof document.all) {
                            Aloha.getActiveEditable().obj.html('<p><br/></p>');
                        }
                        else { //IE 
                            Aloha.getActiveEditable().obj.html('<p></p>');
                        }
                    }
                    // this is not available if the editor is NOT in focus to begin with.
                    Aloha.getActiveEditable().activate();
                }

            },            

            createButton: function () {
                // provide locally scoped version of this for use in callbacks, etc. 
                // and define the tab we will insert the button into
                var that = this;
                this.sourceviewButton = Ui.adopt('sourceview', Button, {
                    tooltip: Ektron.Controls.Editor.Aloha.Plugins.SourceViewer.ResourceText.buttonTitle,
                    icon: 'ektron-aloha-button ' + this.nsString("button"),
                    scope: 'Aloha.continuoustext',
                    click: function () {
                        var range = Aloha.Selection.getRangeObject();
                        if ('undefined' === typeof range.isCollapsed || null == Aloha.activeEditable) {
                            alert(Ektron.Controls.Editor.Aloha.Plugins.General.ResourceText.EditorNotInFocus);
                        }
                        else {
                        that.openDialog();
                    }
                    }
                });
            },

            createSourceViewUI: function () {
                var that = this;

                that.sourceviewDialog.empty();
                
                that.sourceviewDialog.html('<div id="' + that.uiPanel + '"><div id="tests"></div></div>');
                that.contentPanel = $('#' + that.contentPanelId);
                that.contentPanel2 = $('#tests');
                that.findReplaceExpand = $('#FindReplaceExpand');
            },

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            },

            createModal: function () {
                var that = this;
                $ektron('<div class="ektron-aloha-sourceview-modal"></div>').appendTo("body");
                return $ektron(".ektron-aloha-sourceview-modal").dialog({
                    autoOpen: false,
                    draggable: true,
                    resizable: false,
                        width: 750,
                        height: 500,
                    dialogClass: 'ektron-ux ektron-ux-dialog',
                        modal: false,
                    zIndex: 100000001,
                    closeText: '<span title="' + Ektron.Controls.Editor.Aloha.Plugins.General.ResourceText.CloseLabel + '" class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text"><span aria-hidden="true" title="' + Ektron.Controls.Editor.Aloha.Plugins.General.ResourceText.CloseLabel + '" data-ux-icon="&#xe01a;"></span></span>',
                    title: that.resourceText.panelTitle,
                        close: function (event, ui) {
                            that.closeDialog();
                        }
                    });
            },

            openDialog: function () {
                var that = this;
                that.activeEditable = Aloha.getActiveEditable();
                window.ActiveEditable = Aloha.getActiveEditable();
                // create the sourceViewUI and bind up all of the functionality
                that.createSourceViewUI();
                that.addFindReplace();
                that.bindInteractions();
                that.showSource();

                that.sourceviewDialog.dialog('open');
                if ($ektron('button.ui-dialog-titlebar-minimize').is(':visible')) {
                    $ektron('button.ui-dialog-titlebar-minimize').click();
                }
            },           

            showSource: function () {

                var that = this, viewArea,
                    activeEditable = Aloha.getActiveEditable(),
                    container = that.contentPanel, content, containerhtml;
                if (activeEditable) {
                    content = activeEditable.getContents();
                }
                else {
                    // some element (e.g. inspector) has the focus. none of the editor is activeEditable
                    for (i = 0; i < Aloha.editables.length; i++) {
                        if (Aloha.editables[i].obj.is(':visible')) {
                            content = Aloha.editables[i].getContents();
                            break;
                        }
                    }
                }
                
                containerhtml = content.replace(/<\s*textarea([^>]+)class\s*=\s*"EktronScriptBlock"([\s\S]*?)>([\s\S]*?)(<\/textarea[^>]*>)/ig, "<script$1$2>$3<\/script>")
                    .replace(/<script >/gi, '<script>')
                    .replace(/&amp;/gi, '&')
                    .replace(/&lt;/gi, '<').replace(/&gt;/gi, '>')
                    .replace(/\t/g, '  ')
                    .replace(' class="aloha-end-br"', '')
                    .replace(' data-ektron-tagclick-clicked="true"', '')
                    .replace(' data-ektron-image-tagclick-clicked="true"', '')
                    .replace(' data-ektron-td-tagclick-clicked="true"', '')
                    .replace(' data-ektron-highlight-clicked="true"', '');

                //catch js error in content html (see TT#72039 )
                try {
                    var source = $('<div>').html(containerhtml);
                }
                catch (e) { }
                viewArea = that.contentPanel2;

                editor = ace.edit('tests');
                editor.setTheme("ace/theme/eclipse");
                editor.getSession().setMode("ace/mode/html");
                editor.getSession().setValue(style_html(containerhtml));
                editor.getSession().doc.on('change', that.updateEditable);
                $('.ui-dialog').on('dialogresize', function () {
                    editor.resize();
                });
            },

            updateEditable: function (ae, me) {
                var text,
                    that = this;
                text = editor.getSession().getValue();
                //fix for 72340 - remove the \n that ace insets in getValue()
                text = text.replace(/<script([\s\S]*?)>/gi, '<textarea class="EktronScriptBlock"$1>')
                text = text.replace(/<\/script>/gi, '</textarea>')
                //text = text.replace(/\n/g, "");
                Aloha.activateEditable(window.ActiveEditable);
                window.ActiveEditable.setContents(text);
            }
        });
    }
);