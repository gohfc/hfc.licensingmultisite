﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/table-style.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, TableStyleBlock, $ektron, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs : { "table-style.checkbox.table-styles": "Table Styles", "table-style.label.collapse-cell-borders": "Collapse Cell Borders", "table-style.label.caption-position": "Caption Position", 
		"table-style.label.top": "Top", "table-style.label.bottom": "Bottom" },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("Table styles ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        TableStyleBlock = window.createModule(i18n);
        block = new TableStyleBlock();
    }
});

test('Table Style fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundCheckbox, foundCollapseCheckbox, foundPositionSelectBox,
        container = $ektron('#dialog-container'),
        block = new TableStyleBlock('my-div', document.getElementById('divResult'), i18n),
        foundIndentBox, foundSelectBox, forId;
    myblock = block.create();
    container.append(myblock);

    foundBlock = $ektron('#my-div', container);
    foundCheckbox = $ektron('input#chk-tablestyle', container);
    foundCollapseCheckbox = $ektron('input#chk-bordercollapse', container);
    foundPositionSelectBox = $ektron('.select-box', container);

    ok(foundBlock.length > 0, 'Table styles fieldset found.');
    equal(foundCheckbox.length, 1, 'Table style checkbox found.');
    equal(foundCollapseCheckbox.length, 1, 'Collapse Cell Borders checkbox found.');
    ok(foundPositionSelectBox.length > 0, 'Options for caption position found.');
});

test('Table Styles are not set when checkbox is not checked', function () {
    "use strict";
    var collapseBorder = true,
        captionPos = 'Bottom',
        container = $ektron('#dialog-container'),
        block = new TableStyleBlock('my-div', document.getElementById('divResult'), i18n);
    clearContainer();
    myblock = block.create();
    container.append(myblock);

    $ektron('#chk-bordercollapse', container).attr('checked', collapseBorder);
    $ektron('#ddCaptionPos', container).val(captionPos);
    $ektron('#chk-tablestyle', container).attr('checked', false);
    block.updateStyle();
    block.showHideModule($ektron('#chk-tablestyle', container));
    
    ok(typeof $ektron('#divResult').attr('style') === 'undefined' || 0 === $ektron('#divResult').attr('style').length, 'table style is set correctly.');
});

test('Table Styles are set correctly when checkbox is checked', function () {
    "use strict";
    var collapseBorder = true,
        captionPos = 'Bottom',
        container = $ektron('#dialog-container'),
        block = new TableStyleBlock('my-div', document.getElementById('divResult'), i18n);
    clearContainer();
    myblock = block.create();
    container.append(myblock);

    $ektron('#chk-bordercollapse', container).attr('checked', collapseBorder);
    $ektron('#ddCaptionPos', container).val(captionPos);
    $ektron('#chk-tablestyle', container).attr('checked', true);
    block.updateStyle();
    block.showHideModule($ektron('#chk-tablestyle', container));

    ok($ektron('#divResult').attr('style').toLowerCase().indexOf('border-collapse: collapse;') > -1, 'table border collapse is set correctly.');
    ok($ektron('#divResult').attr('style').toLowerCase().indexOf('caption-side: bottom;') > -1, 'caption position is set correctly.');
});

test('fields are reloaded correctly when target table contains styles.', function () {
    "use strict";
    var targetElement = $ektron('#divResult1'), 
        collapseBorder = true,
        captionPos = 'Bottom',
        container = $ektron('#dialog-container1'),
        block = new TableStyleBlock('my-div1', targetElement.get(0), i18n);
    targetElement.css('border-collapse', (true === collapseBorder ? 'collapse' : 'separate')).css('caption-side', captionPos.toLowerCase());
    myblock = block.create();
    container.append(myblock);

    equal($ektron('#chk-bordercollapse', container).is(':checked'), collapseBorder, 'value for table border is loaded correctly.');
    equal($ektron('#ddCaptionPos', container).val().toLowerCase(), captionPos.toLowerCase(), 'value for caption location is loaded correctly.');
    equal($ektron('#chk-tablestyle', container).is(':checked'), true, 'table styles checkbox is loaded correctly.');
});