﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/spacing-style.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, SpacingBlock, $ektron, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs : { "spacing-style.checkbox.spacing": "Spacing", "spacing-style.label.margin-horizonal": "Horizonal Margin", "spacing-style.label.margin-vertical": "Vertical Margin", "spacing-style.label.padding-horizonal": "Horizonal Padding", "spacing-style.label.padding-vertical": "Vertical Padding" },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("spacing style ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        SpacingBlock = window.createModule(i18n);
        block = new SpacingBlock();
    }
});

test('spacing fieldset created successfully', function () {
    "use strict";
    var foundObj = false,
        container = $ektron('#dialog-container'),
        block = new SpacingBlock('my-div', document.getElementById('divResult'), i18n),
        arrInputBox, arrSelectBox;
    myblock = block.create();
    container.append(myblock);

    foundObj = $ektron('#my-div', container);
    arrInputBox = $ektron('.input-box', container);
    arrSelectBox = $ektron('.select-box', container);

    ok(foundObj.length > 0, 'spacing fieldset found.');
    equal(arrInputBox.length, 4, 'Four input box found.');
    arrInputBox.each(function (i) {
        var jThis = $ektron(this);
        ok(jThis.hasClass('margin') || jThis.hasClass('padding'), 'correct style class name found in input box');
        ok(jThis.hasClass('vertical') || jThis.hasClass('horizontal'), 'correct sub-style class name found in input box');
    });
    equal(arrSelectBox.length, 4, 'Four select box found.');
    arrSelectBox.each(function (i) {
        var jThis = $ektron(this), forId = jThis.siblings('.input-box');
        ok(jThis.hasClass(forId.attr('id')), 'correct class name found in select box');
    });
});

test('spacing styles are set after clicking checkbox correctly', function () {
    "use strict";
    var hMargin = 3,
        vMargin = 2,
        hPadding = 4,
        vPadding = 1,
        unit = "EM",
        container = $ektron('#dialog-container'),
        block = new SpacingBlock('my-div1', document.getElementById('divResult'), i18n);
    clearContainer();
    myblock = block.create();
    container.append(myblock);

    $ektron('#txtHMargin', container).val(hMargin);
    $ektron('#ddHMarginUnit', container).val(unit);
    $ektron('#txtVMargin', container).val(vMargin);
    $ektron('#ddVMarginUnit', container).val(unit);
    $ektron('#txtHPadding', container).val(hPadding);
    $ektron('#ddHPaddingUnit', container).val(unit);
    $ektron('#txtVPadding', container).val(vPadding);
    $ektron('#ddVPaddingUnit', container).val(unit);
    $ektron('#chk-spacing', container).attr('checked', true);
    block.updateStyle();
    block.showHideModule($ektron('#chk-spacing', container).get(0));

    ok($ektron('#divResult').attr('style').toLowerCase().indexOf('margin: 2em 3em;') > -1, 'margin is set correctly.');
    ok($ektron('#divResult').attr('style').toLowerCase().indexOf('padding: 1em 4em;') > -1, 'padding is set correctly.');
});

test('spacing styles fields reload correctly', function () {
    "use strict";
    var targetElement = $ektron('#divResult2'), 
        hMargin = 3,
        vMargin = 2,
        hPadding = 4,
        vPadding = 1,
        unit = "EM",
        container = $ektron('#dialog-container2'),
        block = new SpacingBlock('my-div2', document.getElementById('divResult2'), i18n);
    targetElement.css('margin', vMargin + unit + ' ' + hMargin + unit).css('padding', vPadding + unit + ' ' + hPadding + unit);
    myblock = block.create();
    container.append(myblock);

    equal($ektron('#txtHMargin', container).val(), hMargin, 'horizonal margin is set correctly.');
    equal($ektron('#ddHMarginUnit', container).val(), unit, 'horizonal margin unit is set correctly.');
    equal($ektron('#txtVMargin', container).val(), vMargin, 'vertical margin is set correctly.');
    equal($ektron('#ddVMarginUnit', container).val(), unit, 'vertical margin unit is set correctly.');
    equal($ektron('#txtHPadding', container).val(), hPadding, 'horizonal padding is set correctly.');
    equal($ektron('#ddHPaddingUnit', container).val(), unit, 'horizonal padding unit is set correctly.');
    equal($ektron('#txtVPadding', container).val(), vPadding, 'vertical padding is set correctly.');
    equal($ektron('#ddVPaddingUnit', container).val(), unit, 'horizonal padding unit is set correctly.');
    equal($ektron('#chk-spacing', container).attr('checked'), 'checked', 'spacing block checkbox is set correctly.');
});

