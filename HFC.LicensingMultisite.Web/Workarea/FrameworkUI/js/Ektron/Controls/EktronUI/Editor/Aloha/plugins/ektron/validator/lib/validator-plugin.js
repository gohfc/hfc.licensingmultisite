/*!
* Aloha Validator Plugin
* -----------------
* This plugin provides an interface to allow the user to validator the content 
* by w3c online validator via their Web Services APIs.
* It presents its user interface in the Toolbar.
*
* Additional dependancies not provided via RequireJS
*     Ektron.Namespace.js
*     Ektron.Controls.Editor.Aloha.js
*     jQuery UI Dialog  
*/

// define the Source Viewer module using RequireJS
define([
    'aloha',
    'jquery',
    'aloha/plugin',
    'ui/ui',
	'ui/button',
	'ui/toolbar',
	'ui/ui-plugin',
	'aloha/console',
    '../../../../../../../../../../../FrameworkUI/js/Ektron/Ektron.OnException',
    'css!validator/css/validator-plugin.css'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
            jQuery,
			Plugin,
			Ui,
			Button,
			i18n,
			i18nCore,
            console)
    {
        // members
        var namespace = "ektron-aloha-validator-",
            modal,
            activeEditor = {},
            ekXml;

        // create and register the Plugin
        return Plugin.create("validator", {
            defaults: {},

            init: function () {
                // executed on plugin initialization
                this.createButton();
                if (Ektron.Namespace.Register) {
                    Ektron.Namespace.Register('Ektron.Validator');
                    Ektron.Validator.GetContent = this.getContent;
                    Ektron.Validator.ValidateAccessibility = this.validateAccessibility;
                }

                if ("undefined" == typeof ekXml)
                {
                    ekXml = new Ektron.Xml({srcPath: Ektron.Context.Cms.WorkareaPath + "/contentdesigner/" });
                }

                if (!($ektron(".ektron-aloha-validator-modal").is(".ui-dialog"))) {
                    modal = this.createModal();
                }
                modal.parents('.ui-dialog').find('.ui-dialog-titlebar button').remove();
                var titlebar = modal.parents('.ui-dialog').find('.ui-dialog-titlebar');
                $('<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title=""><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text"><span aria-hidden="true" title="close" data-ux-icon="&#xe01a;"></span></span></button>')
                    .appendTo(titlebar)
                    .click(function () {
                        modal.dialog('close');
                    });
            },

            /* Helpers
            ----------------------------------*/
            getContent: function () {
                var activeEditable = Aloha.getActiveEditable(), content;
                if (activeEditable) {
                    content = activeEditable.getContents();
                }
                else {
                    // some element (e.g. inspector) has the focus. none of the editor is activeEditable
                    for (i = 0; i < Aloha.editables.length; i++) {
                        if (Aloha.editables[i].obj.is(':visible')) {
                            content = Aloha.editables[i].getContents();
                            break;
                        }
                    }
                }

                if (content != null) {
                    content = content.replace(/data-ektron-highlight-clicked=\"true\"/gi, '');
                    content = content.replace(/data-ektron-tagclick-clicked=\"true\"/gi, '');
                    content = content.replace(/contenteditable=\"true\"/gi, '');
                    content = content.replace(/contenteditable=\"false\"/gi, '');
                }

                return content;
            },

            validateAccessibility: function (content) {
                var err,
                    returnMsg = "",
                    args = [
			            { name: "baseURL", value: document.location.protocol + "//" + document.location.port + document.location.host }
		            ,	{ name: "outputFormat", value: "text" }
		            ],
	                xslt = "[srcPath]/ektaccesseval.xslt",
	                sPreHtml = "<html><head></head><body>",
	                sPostHtml = "</body></html>";
	            content = sPreHtml + content + sPostHtml;
	            try
	            {
		            returnMsg = ekXml.xslTransform(content, xslt, args);
	            }
	            catch (ex) {}

	            if (returnMsg.length > 0)
	            {
		            err = {code : -1000, msg : returnMsg, doctype : ""};
	            }
                return err;
            },

            createButton: function () {
                // provide locally scoped version of this for use in callbacks, etc. 
                // and define the tab we will insert the button into
                var that = this;

                // define the validator button
                this.validatorButton = Ui.adopt('validator', Button, {
                    tooltip: Ektron.Controls.Editor.Aloha.Plugins.Validator.ResourceText.buttonTitle,
                    icon: 'ektron-aloha-button ' + this.nsString("button"),
                    scope: 'Aloha.continuoustext',
                    click: function () {
                        var range = Aloha.Selection.getRangeObject();
                        if ('undefined' === typeof range.isCollapsed || null == Aloha.activeEditable) {
                            alert(Ektron.Controls.Editor.Aloha.Plugins.General.ResourceText.EditorNotInFocus);
                        }
                        else {
                            that.validatorButtonClick();
                        }
                    }
                });
            },

            createModal: function () {
                $ektron('<div class="ektron-aloha-validator-modal"><iframe class="ektron-aloha-validator-modal-iframe" id="ektron-aloha-validator-modal-iframe" src="' + Ektron.Context.Cms.WorkareaPath + '/blank.htm" height="500" width="782"/></div>').appendTo("body");
                return $ektron(".ektron-aloha-validator-modal").dialog({
                    autoOpen: false,
                    draggable: true,
                    resizable: false,
                    width: 820,
                    modal: true,
                    dialogClass: 'ektron-ux ektron-ux-dialog',
                    zIndex: 100000001,
                    closeText: '<span title="' + Ektron.Controls.Editor.Aloha.Plugins.General.ResourceText.CloseLabel + '" class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text"><span aria-hidden="true" title="' + Ektron.Controls.Editor.Aloha.Plugins.General.ResourceText.CloseLabel + '" data-ux-icon="&#xe01a;"></span></span>',
                    title: Ektron.Controls.Editor.Aloha.Plugins.Validator.ResourceText.modalTitle
                });
            },

            validatorButtonClick: function () {
                modal.dialog("open");
                activeEditor = jQuery('.aloha-editable-active');
                jQuery("iframe.ektron-aloha-validator-modal-iframe").attr("src", Ektron.Context.Cms.UIPath + "/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/validator/resources/validator-plugin.aspx");
            },

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            }
        });
    }
);