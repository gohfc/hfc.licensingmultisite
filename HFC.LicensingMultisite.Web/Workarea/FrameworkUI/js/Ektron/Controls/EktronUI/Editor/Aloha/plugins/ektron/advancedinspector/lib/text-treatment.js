﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([''], function () {
    "use strict";
    var pluginNamespace = 'aloha-text-treatment', coloridfontcolor, texttreatmentidcheck;
    function TextTreatmentBlock(id, obj, i18n, ColorHelper) {
        this.id = id;
        this.targetElem = obj;
        coloridfontcolor = "aloha-text-treatment-color" + this.id;
        texttreatmentidcheck = "chk-treatment" + this.id;

        this.create = function () {
            var treatmentBlock, treatmentCheckbox, treatmentLabel, treatmentGroup,
                treatmentList, indentInput, treatmentSelect, treatmentOption, fontfamilies, that = this;
            treatmentBlock = document.createElement('fieldset');
            treatmentBlock.setAttribute('id', this.id);
            treatmentBlock.className = pluginNamespace;

            //fieldset checkbox and label
            treatmentCheckbox = $ektron('<input type="checkbox" id="' + texttreatmentidcheck + '" />');
            treatmentCheckbox.on('change', function () { that.showHideModule(this); that.updateStyle(); });
            treatmentBlock.appendChild(treatmentCheckbox.get(0));
            treatmentLabel = document.createElement('label');
            treatmentLabel.setAttribute('for', texttreatmentidcheck);
            treatmentLabel.setAttribute('title', i18n.t('text-treatment.checkbox.label'));
            treatmentLabel.innerHTML = i18n.t('text-treatment.checkbox.label');
            treatmentBlock.appendChild(treatmentLabel);

            //all styles group
            treatmentGroup = document.createElement('ul');
            //Font family treatment
            treatmentList = document.createElement('li');
            treatmentLabel = document.createElement('label');
            treatmentLabel.setAttribute('for', 'ddFontFamily');
            treatmentLabel.setAttribute('title', i18n.t('text-treatment.label.font-family'));
            treatmentLabel.className = this.nsClass('label');
            treatmentLabel.innerHTML = i18n.t('text-treatment.label.font-family');
            treatmentList.appendChild(treatmentLabel);
            treatmentGroup.appendChild(treatmentList);
            treatmentList = document.createElement('li');

            //Initialize select
            treatmentSelect = $ektron('<select id="ddFontFamily" data-treatment="font-family" class="select-box"></select>');
            treatmentOption = $ektron('<option></option>');
            treatmentOption.text('Font Family');
            treatmentOption.appendTo(treatmentSelect);

            //Get font family values from Aloha configuration 
            if (null === Ektron.Controls.Editor.Aloha.Fontfamilies || 'undefined' === typeof Ektron.Controls.Editor.Aloha.Fontfamilies) {
                fontfamilies = this.getfontfamilies();
            }
            else {
                fontfamilies = Ektron.Controls.Editor.Aloha.Fontfamilies;
            }                

            $.each(fontfamilies, function (index, value) {
                treatmentOption = $ektron('<option></option>');;
                treatmentOption.text(value);
                treatmentOption.appendTo(treatmentSelect);
            });

            treatmentSelect.on('change', function () { that.updateStyle(); });
            treatmentList.appendChild(treatmentSelect.get(0));
            treatmentGroup.appendChild(treatmentList);

            //Font Size treatment
            treatmentList = document.createElement('li');
            treatmentLabel = document.createElement('label');
            treatmentLabel.setAttribute('for', 'txtFontSize');
            treatmentLabel.setAttribute('title', i18n.t('text-treatment.label.font-size'));
            treatmentLabel.className = this.nsClass('label');
            treatmentLabel.innerHTML = i18n.t('text-treatment.label.font-size');
            treatmentList.appendChild(treatmentLabel);
            treatmentGroup.appendChild(treatmentList);
            treatmentList = document.createElement('li');

            indentInput = $ektron('<input id="txtFontSize"" type="text" class="input-box textbox-narrow" />');
            indentInput.on('keyup', function () { that.updateStyle(); });
            treatmentList.appendChild(indentInput.get(0));
            treatmentSelect = $ektron('<select id="ddFontSizeUnit" data-treatment = "font-size" class="select-box ">').html('<option>%</option><option>EM</option><option>px</option>');


            treatmentSelect.on('change', function () { that.updateStyle(); });
            treatmentList.appendChild(treatmentSelect.get(0));
            treatmentGroup.appendChild(treatmentList);

            //Font Color treatment  
            treatmentList = document.createElement('li');
            treatmentLabel = document.createElement('label');
            treatmentLabel.setAttribute('for', coloridfontcolor);
            treatmentLabel.setAttribute('title', i18n.t('text-treatment.label.font-color'));
            treatmentLabel.className = this.nsClass('label');
            treatmentLabel.innerHTML = i18n.t('text-treatment.label.font-color');
            treatmentList.appendChild(treatmentLabel);
            treatmentGroup.appendChild(treatmentList);
            treatmentList = document.createElement('li');
            treatmentSelect = $ektron('<div><span><input id="' + coloridfontcolor + '" data-treatment = "color" type="text" /></span></div>');
            treatmentSelect.on('input', function () { that.updateStyle(); });
            treatmentSelect.on('paste', function () { that.updateStyle(); });
            treatmentList.appendChild(treatmentSelect.get(0));
            treatmentGroup.appendChild(treatmentList);

            //Font Weight treatment
            treatmentList = document.createElement('li');
            treatmentLabel = document.createElement('label');
            treatmentLabel.setAttribute('for', 'ddFontWeight');
            treatmentLabel.setAttribute('title', i18n.t('text-treatment.label.font-weight'));
            treatmentLabel.className = this.nsClass('label');
            treatmentLabel.innerHTML = i18n.t('text-treatment.label.font-weight');
            treatmentList.appendChild(treatmentLabel);
            treatmentGroup.appendChild(treatmentList);
            treatmentList = document.createElement('li');
            treatmentSelect = $ektron('<select id="ddFontWeight" data-treatment="font-weight"  class="select-box"><option>default</option><option>normal</option><option>bold</option><option>bolder</option><option>lighter</option></select>');

            treatmentSelect.on('change', function () { that.updateStyle(); });
            treatmentList.appendChild(treatmentSelect.get(0));
            treatmentGroup.appendChild(treatmentList);

            //Font Style treatment
            treatmentList = document.createElement('li');
            treatmentLabel = document.createElement('label');
            treatmentLabel.setAttribute('for', 'ddFontStyle');
            treatmentLabel.setAttribute('title', i18n.t('text-treatment.label.font-style'));
            treatmentLabel.className = this.nsClass('label');
            treatmentLabel.innerHTML = i18n.t('text-treatment.label.font-style');
            treatmentList.appendChild(treatmentLabel);
            treatmentGroup.appendChild(treatmentList);
            treatmentList = document.createElement('li');
            treatmentSelect = $ektron('<select id="ddFontStyle" data-treatment="font-style"  class="select-box"><option>default</option><option>normal</option><option>italic</option><option>oblique</option></select>');

            treatmentSelect.on('change', function () { that.updateStyle(); });
            treatmentList.appendChild(treatmentSelect.get(0));
            treatmentGroup.appendChild(treatmentList);

            // separator
            treatmentList = document.createElement('li');
            treatmentList.innerHTML = '<hr />';
            treatmentGroup.appendChild(treatmentList);

            //Decoration treatment
            treatmentList = document.createElement('li');
            treatmentLabel = document.createElement('label');
            treatmentLabel.setAttribute('for', 'ddDecoration');
            treatmentLabel.setAttribute('title', i18n.t('text-treatment.label.decoration'));
            treatmentLabel.className = this.nsClass('label');
            treatmentLabel.innerHTML = i18n.t('text-treatment.label.decoration');
            treatmentList.appendChild(treatmentLabel);
            treatmentGroup.appendChild(treatmentList);
            treatmentList = document.createElement('li');
            treatmentSelect = $ektron('<select id="ddDecoration" data-treatment="text-decoration"  class="select-box"><option>default</option><option>none</option><option>blink</option><option>line-through</option><option>overline</option><option>underline</option></select>');

            treatmentSelect.on('change', function () { that.updateStyle(); });
            treatmentList.appendChild(treatmentSelect.get(0));
            treatmentGroup.appendChild(treatmentList);

            //Transform treatment
            treatmentList = document.createElement('li');
            treatmentLabel = document.createElement('label');
            treatmentLabel.setAttribute('for', 'ddTransform');
            treatmentLabel.setAttribute('title', i18n.t('text-treatment.label.transform'));
            treatmentLabel.className = this.nsClass('label');
            treatmentLabel.innerHTML = i18n.t('text-treatment.label.transform');
            treatmentList.appendChild(treatmentLabel);
            treatmentGroup.appendChild(treatmentList);
            treatmentList = document.createElement('li');
            treatmentSelect = $ektron('<select id="ddTransform" data-treatment="text-transform"  class="select-box"><option>default</option><option>none</option><option>Capitalize</option><option>UPPERCASE</option><option>lowercase</option></select>');

            treatmentSelect.on('change', function () { that.updateStyle(); });
            treatmentList.appendChild(treatmentSelect.get(0));
            treatmentGroup.appendChild(treatmentList);

            // line height
            treatmentList = document.createElement('li');
            treatmentLabel = document.createElement('label');
            treatmentLabel.setAttribute('for', 'txtLineHeight');
            treatmentLabel.setAttribute('title', i18n.t('text-treatment.label.line-height'));
            treatmentLabel.className = this.nsClass('label');
            treatmentLabel.innerHTML = i18n.t('text-treatment.label.line-height');
            treatmentList.appendChild(treatmentLabel);
            treatmentGroup.appendChild(treatmentList);
            treatmentList = document.createElement('li');
            indentInput = $ektron('<input id="txtLineHeight" type="text" class="input-box textbox-narrow" />');
            indentInput.on('keyup', function () { that.updateStyle(); });
            treatmentList.appendChild(indentInput.get(0));
            treatmentSelect = $ektron('<select id="ddLineHeightUnit" data-treatment = "line-height" class="select-box">').html('<option>%</option><option>EM</option><option>px</option>');
            treatmentSelect.on('change', function () { that.updateStyle(); });
            treatmentList.appendChild(treatmentSelect.get(0));
            treatmentGroup.appendChild(treatmentList);



            treatmentBlock.appendChild(treatmentGroup);
            return treatmentBlock;
        };

        this.updateStyle = function () {
            var styleString = "", jResult = $ektron(this.targetElem), fontFamily, fontSize, fontWeight, fontStyle, decoration, transform, lineHeightBox, colorSelected;
            if (true === $ektron('#' + texttreatmentidcheck).prop('checked')) {
                fontFamily = $ektron('#ddFontFamily').val();
                if (fontFamily.toLowerCase() != 'font family') {
                    jResult.css('font-family', fontFamily);
                }
                fontSize = $ektron('#txtFontSize').val() + $ektron('#ddFontSizeUnit').val();
                jResult.css('font-size', fontSize);

                colorSelected = $ektron('#' + coloridfontcolor).val();
                if (colorSelected.indexOf('#') < 0) { colorSelected = '#' + color; }

                if (colorSelected === '#') {
                    jResult.css('color', "");
                }
                else {
                    if (colorSelected.length > 6) {
                        jResult.css('color', colorSelected);
                    }
                }
                fontWeight = $ektron('#ddFontWeight').val();
                if (fontWeight != 'default') {
                    jResult.css('font-weight', fontWeight);
                }
                else {
                    jResult.css('font-weight', "");
                }

                fontStyle = $ektron('#ddFontStyle').val();
                if (fontStyle != 'default') {
                    jResult.css('font-style', fontStyle);
                }
                else {
                    jResult.css('font-style', "");
                }

                decoration = $ektron('#ddDecoration').val();
                if (decoration != 'default') {
                    jResult.css('text-decoration', decoration);
                }
                else {
                    jResult.css('text-decoration', "");
                }

                transform = $ektron('#ddTransform').val();
                if (transform != 'default') {
                    jResult.css('text-transform', transform.toLowerCase());
                }
                else {
                    jResult.css('text-transform', "");
                }

                lineHeightBox = $ektron('#txtLineHeight');
                if (isNaN(lineHeightBox.val())) {
                    jResult.css('line-height', "");
                }
                else {
                    jResult.css('line-height', lineHeightBox.val() + lineHeightBox.siblings('.select-box').val());
                }


            }
            else {
                jResult.css('font-family', "").css('font-size', "").css('color', "").css('font-weight', "").css('font-style', "").css('text-decoration', "").css('text-transform', "").css('line-height', "");
            }

        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function () {
            var stringBuilder = [], prefix = pluginNamespace;
            jQuery.each(arguments, function () {
                stringBuilder.push(this == '' ? prefix : prefix + '-' + this);
            });
            return jQuery.trim(stringBuilder.join(' '));
        };

        this.start = function () {
            var styleString = "", jResult = $ektron(this.targetElem), colorPicker, styleAttr, styles, style, treatmentval, jCkbox, that = this;
            //initialize existing inline values
            styleAttr = jResult.attr('style');
            if (styleAttr) {
                styles = styleAttr.split(';');
                $.each(styles, function (index, value) {
                    if (value.length > 0) {
                        style = value.split(':');
                        //is style associated to treatment
                        treatmentval = $ektron('[data-treatment="' + style[0].trim() + '"]');
                        if (treatmentval.length > 0) {
                            if (style[0].trim() == 'font-size' || style[0].trim() == 'line-height') {
                                that.setSize(treatmentval, style[1].trim());
                            }
                            else if (style[0].trim() == 'color') {
                                //color is set in colorPicker.start
                                $ektron('#' + texttreatmentidcheck).prop('checked', true);
                            }
                            else {
                                that.setSelected(treatmentval, style[1].trim());
                            }
                        }
                    }

                });
            }

            jCkbox = $ektron('#' + texttreatmentidcheck);
            if (true === jCkbox.is(':checked')) {
                jCkbox.siblings('ul').css('display', 'block');
            }
            else {
                jCkbox.siblings('ul').css('display', 'none');
            }

            //Initialize colorpicker
            colorPicker = new ColorHelper($ektron('#' + coloridfontcolor));
            colorPicker.start(jResult.css('color'));

        };

        this.getfontfamilies = function () {
            var that = this, fontfamilies = null;
            $ektron.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: Ektron.Context.Cms.UIPath + "/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/advancedinspector/resources/advancedinspector.ashx",
                data: { "action": "getallfonts" },
                success: function (msg) {
                    fontfamilies = $ektron.parseJSON(msg);
                    Ektron.Controls.Editor.Aloha.Fontfamilies = fontfamilies;
                }
            });
            return fontfamilies;
        };

        //Helper Functions

        this.setSelected = function (sel, val) {
            $.each(sel[0].options, function (index, value) {
                if ($(this).val().toLowerCase().replace(/, /g, ",") === val.toLowerCase().replace(/, /g, ",")) {
                    $(this).attr('selected', 'selected');
                }

            });
            $ektron('#' + texttreatmentidcheck).prop('checked', true);
        };
        this.setSize = function (sel, val) {

            var myRegexp = /^(\d*)(%|em|px)/i;
            var match = myRegexp.exec(val);
            this.setSelected(sel, match[2]);
            sel.siblings('.input-box').val(match[1]);
            $ektron('#' + texttreatmentidcheck).prop('checked', true);
        };
        this.showHideModule = function (elem) {
            //$ektron(elem).siblings('ul').toggle();
            var jElem = $ektron(elem);
            if (jElem.is(':checked')) {
                jElem.siblings('ul').show();
            }
            else {
                jElem.siblings('ul').hide();
            }
        };
    }
    return TextTreatmentBlock;
});
