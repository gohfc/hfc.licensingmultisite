﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="../../../common/ui/css/jquery-ui-1.9m6.css" />
/// <reference path="../../../../../../../../../../../csslib/ektronTheme/jquery-ui-custom.css" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/image-properties.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}
if ("undefined" == typeof Aloha) { Aloha = {}; }
if ("undefined" == typeof Aloha.activeEditable) { Aloha.activeEditable = {}; }
if ("undefined" == typeof Aloha.activeEditable.obj ) { Aloha.activeEditable.obj = null; };

var block, myblock, ImgPropBlock, $ektron, i18njs,
    clearContainer = function (containerId) {
        "use strict";
        while ($ektron(containerId).children().length > 0) {
            $ektron(containerId).children().remove();
        }
    },
    i18n = {
        i18njs : { "image-properties.label.image-src": "Image Source",
        "image-properties.label.alt-text": "Alternate Text",
        "image-properties.label.edit-image": "Edit Image",
        "image-properties.label.library": "Library",
        "advanced-inspector.label.library": "CMS400 Library"},
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("Image Properties ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        ImgPropBlock = window.createModule(i18n);
        block = new ImgPropBlock();
    }
});

test('Image Properties fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundUrlBox, foundAltTextBox, foundLibBtn, foundEditImgBtn,  
        thisId = 'my-div',
        container = $ektron('#dialog-container'),
        block = new ImgPropBlock(thisId, document.getElementById('divResult'), i18n);
    myblock = block.create();
    container.append(myblock);

    foundBlock = $ektron('.aloha-image-properties', container);
    foundUrlBox = $ektron('input#txturl' + thisId, container);
    foundAltTextBox = $ektron('textarea#txtalt' + thisId, container);
    foundLibBtn = $ektron('span#btnLibrary' + thisId, container);
    foundEditImgBtn = $ektron('span#btnImageEdit' + thisId, container);

    ok(foundBlock.length > 0, 'Image Properties fieldset found.');
    ok(foundUrlBox.length > 0, 'Url Input box is found.');
    ok(foundAltTextBox.length > 0, 'Alternate text box is found.');
    ok(foundLibBtn.length > 0, 'Library Button is found.');
    ok(foundEditImgBtn.length > 0, 'Edit Image Button is found.');
});

test('Image Properties are reloaded correctly when target image is selected.', function () {
    "use strict";
    var targetElement = $ektron('#divResult2'), 
        container = $ektron('#dialog-container2'),
        thisId = 'my-div2',
        block = new ImgPropBlock(thisId, targetElement.get(0), i18n);

    myblock = block.create();
    container.append(myblock);
    block.init();

    equal($ektron('input#txturl' + thisId, container).val(), targetElement.attr('src'), 'image source is set correctly.');
    equal($ektron('textarea#txtalt' + thisId, container).val(), targetElement.attr('alt'), 'iamge alternate text is set correctly.');
});