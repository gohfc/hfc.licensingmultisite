/*!
* Ektron Aloha General Plugin
* -----------------
* This plugin provides an general helper to allow the user to manipulate the content.
* There is no UI for the plugin.
*
* Additional dependancies not provided via RequireJS
*     Ektron.Namespace.js
*     Ektron.Controls.Editor.Aloha.js
*/

// define the Source Viewer module using RequireJS
define([
    'aloha',
    'jquery',
	'aloha/plugin',
    'ui/ui',
    'ui/toggleButton',
    'ui/port-helper-multi-split',
    'PubSub',
    'i18n!general/nls/i18n',
	'i18n!aloha/nls/i18n',
	'aloha/console',
    'css!./css/general-plugin'],
// callback executed once dependencies are loaded by RequireJS
    function (Aloha,
            jQuery,
			Plugin,
            Ui,
            ToggleButton,
            MultiSplitButton,
            PubSub,
            i18n,
            i18nCore,
            console) {
        // members
        var namespace = "ektron-aloha-",
            StyleConfigs = null,
            currentSelection = null,
            _toggleSpanButton;

        // create and register the Plugin
        return Plugin.create("general", {
            defaults: {},

            init: function () {
                var that = this;
                if (Ektron.Namespace.Register) {
                    Ektron.Namespace.Register('Ektron.Controls.Editor.Aloha');
                    Ektron.Controls.Editor.Aloha.GetContent = this.getContent;
                    Ektron.Controls.Editor.Aloha.UpdateAllContents = this.ensureUpdateAllContents;
                }

                $(document).on('TidyUpToolbarButtons', function () {
                    $ektron('button[title="Strong"]').hide();
                    $ektron('button[title="Emphasize"]').hide();
                });

                $(document).on('InsertSpanButton', function () {
                    $ektron('button[title="Strong"]').hide();
                    $ektron('button[title="Emphasize"]').hide();
                    var jH1LargeButton = $ektron('span.aloha-large-icon-h1').parent('button'),
                    jFirstBlock = $ektron('div.aloha-ui-component-group:first');
                    if (jH1LargeButton.length > 0 && 1 == jFirstBlock.children().length) {
                        $ektron(_toggleSpanButton.element).addClass('aloha-large-button');
                        jH1LargeButton.before(_toggleSpanButton.element);
                        jFirstBlock.remove();
                    }
                });

                Aloha.bind('aloha-selection-changed', function (event, rangeObject, originalEvent) {
                    var currElem, jCurrElem;
                    $ektron('*').removeAttr('data-ektron-highlight'); 
                    if (typeof originalEvent !== 'undefined') {
                        currElem = originalEvent.target;
                        if ('IMG' === currElem.tagName || 'FIGURE' === currElem.tagName) {
                            jCurrElem = $ektron(currElem);
                            if('IMG' === currElem.tagName && jCurrElem.closest('figure').hasClass('ektron-responsive-imageset')) {
                                jCurrElem = jCurrElem.closest('figure');
                            }
                            jCurrElem.attr('data-ektron-highlight', true);
                            var sel = {};
                            if (window.getSelection) {
                                sel = window.getSelection();
                            } else if (document.selection) {
                                sel = document.selection.createRange();
                            }
                            if (sel.rangeCount) {
                                sel.removeAllRanges();
                                //return;
                            }
                            else if (document.selection) {
                                document.selection.empty();
                            }
                            rangeObject = null;
                        }
                    }
                });

                //Span formatting button 
                Aloha.bind("aloha-editable-created", function (jEvent, aEvent) { 
                    $(document).trigger('TidyUpToolbarButtons');
                });
                Aloha.bind("aloha-editable-activated", function (jEvent, aEvent) {
                    // TFS#6410 & TFS#6842: show table insert button
                    if (Aloha.settings.plugins.load.indexOf(',common/table,') > -1) {
                        $('div.aloha-ui-toolbar').hover(function () {
                            $('.aloha button[title="Insert Table"]').removeAttr('style');
                        });
                    }

                    // show the hidden SPAN button 
                    $(document).trigger('InsertSpanButton');

                    //Img selection on Click, and Delete action on Keydown (for all elements)
                    that.initPictureSelection(aEvent);

                    //Table Selection
                    that.initTableSelection(aEvent);
                });

                that.createSpanButtons();
                that.createCustomStylingButtons();

            },

            /* Helpers
            ----------------------------------*/   
            //Image and Figure tag selection begin
            initPictureSelection: function(myeditable) {
                var that = this;
                $ektron('#' + myeditable.editable.obj[0].id + '').off('click.general');
                $ektron('#' + myeditable.editable.obj[0].id + '').on('click.general', '*', function (event) {
                    //console.log('tagName at click = ' + this.tagName);
                    $ektron('*').removeAttr('data-ektron-highlight');
                    if ('IMG' === this.tagName) {
                        //if not contained in a figure, then fire onselectionchanged. if contained in a figure, this method will be called again with that selected.
                        $ektron(this).attr('data-ektron-highlight', true);
                        if ($ektron(this).closest('FIGURE').length == 0) {
                            that.onSelectionChanged(Aloha.Selection.getRangeObject());
                        }
                    }
                    else if ('FIGURE' === this.tagName) {
                        $ektron(this).attr('data-ektron-highlight', true);
                        that.onSelectionChanged(Aloha.Selection.getRangeObject());
                    }
                });

                $ektron('#' + myeditable.editable.obj[0].id + '').off('keydown.general');
                $ektron('#' + myeditable.editable.obj[0].id + '').on('keydown.general', function (event) {
                    if ( 46 === event.keyCode && !event.altKey && !event.ctrlKey && !event.shiftKey) {
                        // delete selected image and responsive figure images
                        var jSelectedPicture = $ektron('*[data-ektron-highlight=true]'),
                            tagName = 'P';
                        if (1 == jSelectedPicture.length) {
                            tagName = jSelectedPicture.get(0).tagName;
                        }
                        if ('IMG' === tagName|| 'FIGURE' === tagName) {
                            jSelectedPicture.remove();
                            event.stopPropagation();
                            return false;
                        }

                        // delete does not remove DIV tags in content
                        var rangeObject = Aloha.Selection.rangeObject,
			                i, ancestor;
                        if (rangeObject.isCollapsed()) {
				            return;
			            }

                        $ektron('div', rangeObject.getCommonAncestorContainer()).contents().unwrap();
                        ancestor = rangeObject.getCommonAncestorContainer();
                        if ('DIV' == ancestor.tagName && false == $ektron(ancestor).hasClass('aloha-editable')) {
                            $ektron(ancestor).unwrapInner();
                        }
                        if ('' == $ektron(this).text()) {
                            if ('undefined' == typeof document.all) {
                                $ektron(this).html('<p><br/></p>');
                            }
                            else { //IE 
                                $ektron(this).html('<p></p>');
                            }
                        }
                        try {
                            // select the modified range [IE9 might throw an exception]
			                rangeObject.select();
                        }
                        catch(ex) {} 
                    }
                });
            },
            //Image and Figure tag selection end

            //Table selection begin
            initTableSelection: function(myeditable) {
                var that = this;
                $ektron('#' + myeditable.editable.obj[0].id + '').off('click.table');
                $ektron('#' + myeditable.editable.obj[0].id + '').on('click.table', '*', function (event) {
                    console.log('tagName at click = ' + this.tagName);
                    var tagName = this.tagName, jThis = $ektron(this);
                    if (jThis.hasClass('aloha-block-handle')) {
                        that.updateStyleButtons(jThis.siblings('table').get(0));
                    }
                    else if (jThis.hasClass('aloha-table-selectrow')) { //TH
                        that.updateStyleButtons(jThis.parent('tr').get(0));
                    }
                });
            },
            //Table selection end

            //Custom styling buttons begin
            createCustomStylingButtons: function () {
                var once = true;
                this.getStyleConfig();
                Aloha.bind('aloha-editable-activated', function (event, editable) {
                    if ($('#aloha-ekt-StyleContainer').length == 0) {
                        var customBlock = $('<div id="aloha-ekt-StyleContainer" class="aloha-ui-component-group" unselectable="on"><span unselectable="on"><div class="aloha-multisplit"><div id="aloha-custom-styles" class="aloha-multisplit-content"></div><button id="aloha-class-toggle" class="aloha-multisplit-toggle ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false"><span class="ui-button-text"></span></button></div></span></div>');
                        $('#tab-ui-container-1 .aloha-ui-clear').before(customBlock);
                    }
                });

                $('body').on('click', '#aloha-class-toggle', function () {
                    $('#aloha-custom-styles').parent().toggleClass('aloha-multisplit-open');
                });

                $(document).mouseup(function (e) {
                    if ($('#aloha-custom-styles').parent().hasClass('aloha-multisplit-open')) {
                        $('#aloha-custom-styles').parent().removeClass('aloha-multisplit-open');
                    }
                });

                $ektron('body').off('click', '.styleOpt');
                $ektron('body').on('click', '.styleOpt', function (event) {
                    var targetElem = currentSelection;
                    var clickedelem = $ektron(this);
                    clickedelem.toggleClass("aloha-multisplit-active");
                    var key = clickedelem.attr("data-StyleRef");
                    var type = clickedelem.attr("data-StyleType");

                    var action, strStyleVals = StyleConfigs[targetElem.tagName].styles[key].value, arrStyle, arrProperty, i;
                    if (clickedelem.hasClass("aloha-multisplit-active"))
                        action = "add";
                    else
                        action = "remove";

                    if (StyleConfigs[targetElem.tagName].styles[key].type == "style") {
                        arrStyle = $.trim(strStyleVals).split(';');
                        for (i = 0; i < arrStyle.length; i += 1) {
                            arrProperty = $.trim(arrStyle[i]).split(':');
                            if ('string' == typeof arrProperty[0]) {
                                if (action == "add") {
                                    $ektron(targetElem).css( $.trim(arrProperty[0]), $.trim(arrProperty[1]));
                                }
                                else if (action == "remove") {
                                    $ektron(targetElem).css($.trim(arrProperty[0]), '');
                                }
                            }
                        }
                    } else if (StyleConfigs[targetElem.tagName].styles[key].type == "class") {
                        if (action == "add")
                            $ektron(targetElem).addClass(strStyleVals);
                        else if (action == "remove")
                            $ektron(targetElem).removeClass(strStyleVals);
                    }
                });
                $ektron('body').on('click', '.removeStyling', function (event) {
                    $(currentSelection).attr('style', '').attr('class', '');
                });
            },

            //Span formatting button begin
            createSpanButtons: function () {
                var that = this;
                _toggleSpanButton = Ui.adopt("toggleSpan", ToggleButton, {
                    name: 'span',
                    tooltip: i18n.t('button.span.tooltip'),
                    icon: 'aloha-large-icon aloha-icon aloha-large-icon-span',
                    scope: 'Aloha.continuoustext',
                    click: function () { that.spanButtonClick(); }
                });

                PubSub.sub('aloha.selection.context-change', function (message) {
                    that.onSelectionChanged(message.range);
                });
            },

            spanButtonClick: function () {
                var rangeObject = Aloha.Selection.rangeObject,
                    selectedEdit = jQuery('.aloha-editable-active'),
                    foundMarkup,
                    markup = jQuery('<span></span>');

                // check whether the markup is found in the range (at the start of the range)
                foundMarkup = rangeObject.findMarkup(function () { //TODO: test
                    return this.nodeName.toLowerCase() == markup.get(0).nodeName.toLowerCase();
                }, Aloha.activeEditable.obj);

                if (foundMarkup) {
                    // remove the markup
                    if (rangeObject.isCollapsed()) {
                        // when the range is collapsed, we remove exactly the one DOM element
                        GENTICS.Utils.Dom.removeFromDOM(foundMarkup, rangeObject, true);
                    } else {
                        // the range is not collapsed, so we remove the markup from the range
                        GENTICS.Utils.Dom.removeMarkup(rangeObject, markup, Aloha.activeEditable.obj);
                    }
                } else {
                    // when the range is collapsed, extend it to a word
                    if (rangeObject.isCollapsed()) {
                        GENTICS.Utils.Dom.extendToWord(rangeObject);
                    }

                    // add the markup
                    GENTICS.Utils.Dom.addMarkup(rangeObject, markup);
                }
                // select the modified range
                rangeObject.select();

                // update Button toggle state. We take 'Aloha.Selection.getRangeObject()'
                // because rangeObject is not up-to-date
                this.onSelectionChanged(Aloha.Selection.getRangeObject());
                return false;
            },

            onSelectionChanged: function (rangeObject) {
                var that = this, 
                    jSelectedPicture = $ektron('*[data-ektron-highlight=true]'); 

                if (1 === jSelectedPicture.length) {
                    currentSelection = jSelectedPicture.get(0);
                }
                else if (typeof rangeObject.markupEffectiveAtStart !== 'undefined') {
                    currentSelection = rangeObject.markupEffectiveAtStart[0];
                }
                that.updateStyleButtons(currentSelection);
            },

            updateStyleButtons: function (effectiveMarkup) {
                if ('undefined' === typeof effectiveMarkup) return;
                var that = this,
                    multiSplitItems = $ektron('button.aloha-large-button'),
                    tagName = effectiveMarkup.tagName,
                    jEffectiveMarkup = $ektron(effectiveMarkup);

                if (multiSplitItems.length > 0) {
                    if (typeof effectiveMarkup !== 'undefined' && 'SPAN' === tagName) {
                        multiSplitItems.removeClass('aloha-multisplit-active');
                        $ektron('span.aloha-large-icon-span').closest('button.aloha-large-button').addClass('aloha-multisplit-active').removeClass('aloha-button-active');
                    }
                    else {
                        $ektron('span.aloha-large-icon-span').closest('button.aloha-large-button').removeClass('aloha-multisplit-active');
                    }

                    //Control highlighting of style buttons
                    $('#aloha-custom-styles').empty();

                    // Aloha wraps text node with DIV.aloha-table-cell-editable in table cells.
                    if ('DIV' === tagName && jEffectiveMarkup.hasClass('aloha-table-cell-editable'))
                    {
                        jEffectiveMarkup = jEffectiveMarkup.parent('td');
                        effectiveMarkup = jEffectiveMarkup.get(0);
                        tagName = 'TD';
                    }
                    currentSelection = effectiveMarkup;

                    if (typeof effectiveMarkup !== 'undefined') { 
                        if (StyleConfigs[tagName] != null) {
                            //Reading style data from config, found matched element defined
                            var optionStr = '';
                            var styles = jEffectiveMarkup.attr('style');
                            var styleList = [];
                            if (typeof (styles) != 'undefined') {
                                styleList = styles.split(';');
                            }
                            $ektron.each(eval(styleList), function (key, value) {
                                styleList[key] = value.replace(/\s+/g, '') + ';';
                            });
                            $ektron.each(eval(StyleConfigs[tagName].styles), function (key, value) {
                                var selected = jEffectiveMarkup, selectedOpt = false;
                                if ($.inArray(value.value.replace(/\s+/g, ''), styleList) != -1) {
                                    selectedOpt = true;
                                }

                                if ((selected.hasClass(value.value)) || selectedOpt) {
                                    var btn = $('<button type="button" class="styleOpt ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only aloha-large-button aloha-multisplit-active" role="button" aria-disabled="false" title="' + key + '" data-StyleRef="' + key + '" data-StyleType="' + StyleConfigs[tagName].styles[key].type + '"><div style=margin:2px;overflow:hidden;width:54px;height:45px;background-color:white;"><span style="width:100%;display:block;font-size:13px;margin-top:7px;' + that.getWorkareaBoxValue(tagName, key) + '">AaBbCc</span><span style="color: #777777;font-family: verdana;font-size: 8px;">' + key + '</span></div></button>')
                                    $('#aloha-custom-styles').append(btn);
                                } else {
                                    var btn = $('<button type="button" class="styleOpt ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only aloha-large-button" role="button" aria-disabled="false" title="' + key + '" data-StyleRef="' + key + '" data-StyleType="' + StyleConfigs[tagName].styles[key].type + '"><div style="margin:2px;overflow:hidden;width:54px;height:45px;background-color:white;"><span style="width:100%;display:block;font-size:13px;margin-top:7px;' + that.getWorkareaBoxValue(tagName, key) + '">AaBbCc</span><span style="color: #777777;font-family: verdana;font-size: 8px;">' + key + '</span></div></button>')
                                    $('#aloha-custom-styles').append(btn);
                                }
                            });
                        }
                        if (StyleConfigs['*'] != null) {
                            //Reading style data from config, found wild card '*' defined
                            var optionStr = '';
                            var styles = jEffectiveMarkup.attr('style');
                            var styleList = [];
                            if (typeof (styles) != 'undefined') {
                                styleList = styles.split(';');
                            }
                            $ektron.each(eval(styleList), function (key, value) {
                                styleList[key] = value.replace(/\s+/g, '') + ';';
                            });
                            $ektron.each(eval(StyleConfigs['*'].styles), function (key, value) {
                                var selected = jEffectiveMarkup, selectedOpt = false;
                                if ($.inArray(value.value.replace(/\s+/g, ''), styleList) != -1) {
                                    selectedOpt = true;
                                }

                                if ((selected.hasClass(value.value)) || selectedOpt) {
                                    var btn = $('<button type="button" class="styleOpt ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only aloha-large-button aloha-multisplit-active" role="button" aria-disabled="false" title="' + key + '" data-StyleRef="' + key + '" data-StyleType="' + StyleConfigs['*'].styles[key].type + '"><div style=margin:2px;overflow:hidden;width:54px;height:45px;background-color:white;"><span style="width:100%;display:block;font-size:13px;margin-top:7px;' + that.getWorkareaBoxValue('*', key) + '">AaBbCc</span><span style="color: #777777;font-family: verdana;font-size: 8px;">' + key + '</span></div></button>')
                                    $('#aloha-custom-styles').append(btn);
                                } else {
                                    var btn = $('<button type="button" class="styleOpt ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only aloha-large-button" role="button" aria-disabled="false" title="' + key + '" data-StyleRef="' + key + '" data-StyleType="' + StyleConfigs['*'].styles[key].type + '"><div style="margin:2px;overflow:hidden;width:54px;height:45px;background-color:white;"><span style="width:100%;display:block;font-size:13px;margin-top:7px;' + that.getWorkareaBoxValue('*', key) + '">AaBbCc</span><span style="color: #777777;font-family: verdana;font-size: 8px;">' + key + '</span></div></button>')
                                    $('#aloha-custom-styles').append(btn);
                                }
                            });
                        }
                    }
                    $('#aloha-custom-styles').append('<button type="button" class="removeStyling ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only aloha-ui-multisplit-fullwidth" role="button" aria-disabled="false"><span class="ui-button-text">Remove styling</span></button>')
                }
            },
            //Span formatting button end

            getWorkareaBoxValue: function (index, key) {
                var uiPath = "/";
                if (typeof window.Ektron.PathData === 'undefined') {
                    uiPath = Ektron.Context.Cms.UIPath;
                } else {
                    uiPath = window.Ektron.PathData.uiPath;
                }
                return StyleConfigs[index].styles[key].workareaBox.replace(/[FrameworkUIPath]/gi, uiPath);
            },

            getStyleConfig: function () {
                var that = this, uiPath = "/";

                if (typeof window.Ektron.PathData === 'undefined') {
                    uiPath = Ektron.Context.Cms.UIPath;
                } else {
                    uiPath = window.Ektron.PathData.uiPath;
                }

                if ($ektron.isEmptyObject(StyleConfigs)) {
                    $.ajax({
                        url: uiPath + '/js/Ektron/Controls/EktronUI/Editor/Aloha/plugins/ektron/advancedinspector/lib/StyleConfig.js',
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            StyleConfigs = data; 
                        }
                    });
                }
            },

            ensureUpdateAllContents: function () {
                var i, editor, xhtmlContent, hiddenFieldId;
                for (i = 0; i < Aloha.editables.length; i++) {
                    editorId = Aloha.editables[i].getId();
                    hiddenFieldId = editorId.replace(/_EIC_Body_Content/, '_textValue');
                    xhtmlContent = Aloha.editables[i].getContents();

                    if (Ektron.Controls.Editor.Aloha.HtmlEncoded) {
                        xhtmlContent = xhtmlContent.replace(/</gi, '&lt;').replace(/>/gi, '&gt;');
                    }
                    xhtmlContent = xhtmlContent.replace(/<br>/gi, '<br/>');

                    $ektron('#' + hiddenFieldId).val(xhtmlContent);
                }
            },

            getContent: function () {
                return Aloha.activeEditable.snapshotContent;
            },

            updateEktronUrl: function (element, attr) {
                var srcUrl = element.getAttribute('data-ektron-url');
                elemObj = $ektron(element);
                if (srcUrl && srcUrl.length > 0) {
                    elemObj.attr(attr, srcUrl);
                }
                elemObj.removeAttr('data-ektron-url');
            },

            updateEktronClick: function (element) {
                element = jQuery(element);
                element.attr('onclick', element.attr('onclick').replace(/^(return false;)/, ''));
            },

            makeClean: function (obj) {
                console.log('general make clean');
                var that = this;
                obj.find('img[data-ektron-url]').each(function () { that.updateEktronUrl(this, 'src'); });
                obj.find('a[data-ektron-url]').each(function () { that.updateEktronUrl(this, 'href'); });
                obj.find('a[onclick]').each(function () { that.updateEktronClick(this); });
                obj.find('img[data-ektron-constrainProportions]').removeAttr('data-ektron-constrainProportions');
                obj.find('*[data-ektron-highlight]').removeAttr('data-ektron-highlight');
                obj.find('table').each(function () {
                    var tbl = $(this);
                    if (tbl.hasClass('aloha-table')) {
                        tbl.removeClass('aloha-table');
                    }
                    if (tbl.parent().hasClass('aloha-table-wrapper')) {
                        tbl.unwrap();
                    }
                    tbl.find('.aloha-table-selectcolumn').remove();
                    tbl.find('.aloha-table-selectrow').remove();
                });
                obj.find('.aloha-table-cell-editable').each(function () {
                    $(this).replaceWith(this.childNodes);
                });
            },

            // namespacing method for various strings used within the plugin
            nsString: function (string) {
                return namespace + string;
            }
        });
    }
);