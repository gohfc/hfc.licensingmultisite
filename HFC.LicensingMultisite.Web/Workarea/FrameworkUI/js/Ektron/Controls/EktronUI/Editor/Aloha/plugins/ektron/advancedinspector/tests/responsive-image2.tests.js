﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/responsive-image.js" />

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Context) { Ektron.Context = {}; }
if ("undefined" == typeof Ektron.Context.Cms ) { Ektron.Context.Cms = {}; }
if ("undefined" == typeof Ektron.Context.Cms.WorkareaPath) { 
    var thisLoc = document.location.href, 
        workareaPos = thisLoc.toLowerCase().indexOf('/workarea');
    Ektron.Context.Cms.WorkareaPath = thisLoc.substring(0, workareaPos + 9); 
    Ektron.Context.Cms.UIPath = Ektron.Context.Cms.WorkareaPath + '/frameworkui';
}

var block, myblock, RespImgBlock, $ektron, i18njs,
    clearContainer = function () {
        "use strict";
        while ($ektron('#dialog-container').children().length > 0) {
            $ektron('#dialog-container').children().remove();
        }
    },
    i18n = {
        i18njs : { "responsive-image.checkbox.responsive-image-selection": "Responsive Image Selection", "responsive-image.label.change-button": "Change",
        "responsive-image.label.no-breakpoints-found": "No breakpoint found in the system.", "responsive-image.dialog.title": "Select Responsive Image for " },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    };

module("Responsive Image Selection", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        RespImgBlock = window.createModule(i18n);
        block = new RespImgBlock();
    }
});

test('checkbox is reloaded correctly when target element is a FIGURE element.', function () {
    "use strict";
    var targetElement = $ektron('#divResult'), 
        container = $ektron('#dialog-container'),
        //ajaxMsg = '{"WidthPixelSpec":[480,750,1024,1035],"BreakpointDataList":[{"Name":"Smart Phones","Width":480},{"Name":"Small Tablet","Width":750},{"Name":"Tablets","Width":1024},{"Name":"Desktop","Width":1035}]}',
        block = new RespImgBlock('my-div', targetElement.get(0), i18n);
    targetElement.picture();
    //block.getBreakpoints = function () {
    //    return block.createBreakpointsCarousel($ektron.parseJSON(ajaxMsg));
    //};
    myblock = block.create();
    container.append(myblock);
    block.init();

    equal($ektron('#chk-responsive-image', container).is(':checked'), false, 'value for responsive image selection is loaded correctly.');
    // NOTE: block.init() is needed for the following to be defined.
    ok(typeof Ektron.AdvancedInspector.ResponsiveImage.AcceptInsert !== 'undefined', 'the namespace and the AcceptInsert function is defined for dialog call.');
});
