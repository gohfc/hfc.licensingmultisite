﻿/*global $ektron, define */
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../lib/require.js" />
/// <reference path="../nls/i18n.js" />
define([''], function () {
    "use strict";
    var pluginNamespace = 'aloha-text-alignment';
    function TextAlignmentBlock(id, obj, i18n) {
        this.id = id;
        this.targetElem = {
            elem : obj,
            valign : 0,
            align : 0,
            indent : 0
        }; 

        this.create = function () {
            var alignmentBlock, alignmentCheckbox, alignmentLabel, alignmentGroup,
                alignmentList, indentInput, alignmentSelect, 
                jTarget = $ektron(this.targetElem.elem), 
                currStyle = jTarget.attr('style'), arrStyle, currUnit = 'EM',
                that = this;
            this.deserializeStyle(currStyle);
            alignmentBlock = document.createElement('fieldset');
            alignmentBlock.setAttribute('id', this.id);
            alignmentBlock.className = pluginNamespace;

            //fieldset checkbox and label
            alignmentCheckbox = $ektron('<input type="checkbox" id="chk-alignment" />');
            alignmentCheckbox.on('change', function () { that.showHideModule(this); that.updateStyle(); });
            if (this.hasAlignment(currStyle)) {
                alignmentCheckbox.attr('checked', true);
            }
            alignmentBlock.appendChild(alignmentCheckbox.get(0)); 
            alignmentLabel = document.createElement('label');
            alignmentLabel.setAttribute('for', 'chk-alignment');
            alignmentLabel.setAttribute('title', i18n.t('text-alignment.checkbox.text-alignment'));
            alignmentLabel.innerHTML = i18n.t('text-alignment.checkbox.text-alignment'); 
            alignmentBlock.appendChild(alignmentLabel); 

            //all styles group
            alignmentGroup = document.createElement('ul');
            //Text alignment
            alignmentList = document.createElement('li');
            alignmentLabel = document.createElement('label');
            alignmentLabel.setAttribute('for', 'ddTextAlignment');
            alignmentLabel.setAttribute('title', i18n.t('text-alignment.label.text-alignment'));
            alignmentLabel.className = this.nsClass('label');
            alignmentLabel.innerHTML = i18n.t('text-alignment.label.text-alignment');
            alignmentList.appendChild(alignmentLabel);
            alignmentGroup.appendChild(alignmentList);
            alignmentList = document.createElement('li');
            alignmentSelect = $ektron('<select id="ddTextAlignment" class="select-box"><option>' + i18n.t('text-alignment.label.default') + '</option><option>' + i18n.t('text-alignment.label.left') + '</option><option>' + i18n.t('text-alignment.label.right') + '</option><option>' + i18n.t('text-alignment.label.center') + '</option><option>' + i18n.t('text-alignment.label.justify') + '</option></select>');
            alignmentSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.align !== 0) {
                alignmentSelect.val(this.targetElem.align.charAt(0).toUpperCase() + this.targetElem.align.slice(1));
            }
            alignmentList.appendChild(alignmentSelect.get(0));
            alignmentGroup.appendChild(alignmentList);
            
            //Vertical alignment
            alignmentList = document.createElement('li');
            alignmentLabel = document.createElement('label');
            alignmentLabel.setAttribute('for', 'ddVAlignment');
            alignmentLabel.setAttribute('title', i18n.t('text-alignment.label.vertical-alignment'));
            alignmentLabel.className = this.nsClass('label');
            alignmentLabel.innerHTML = i18n.t('text-alignment.label.vertical-alignment');
            alignmentList.appendChild(alignmentLabel);
            alignmentGroup.appendChild(alignmentList);
            alignmentList = document.createElement('li');
            alignmentSelect = $ektron('<select id="ddVAlignment" class="select-box"><option>' + i18n.t('text-alignment.label.default') + '</option><option>' + i18n.t('text-alignment.label.top') + '</option><option>' + i18n.t('text-alignment.label.middle') + '</option><option>' + i18n.t('text-alignment.label.baseline') + '</option><option>' + i18n.t('text-alignment.label.bottom') + '</option></select>');
            alignmentSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.valign !== 0) {
                alignmentSelect.val(this.targetElem.valign.charAt(0).toUpperCase() + this.targetElem.valign.slice(1));
            }
            alignmentList.appendChild(alignmentSelect.get(0));
            alignmentGroup.appendChild(alignmentList);

            // separator
            alignmentList = document.createElement('li');
            alignmentList.innerHTML = '<hr />';
            alignmentGroup.appendChild(alignmentList);

            //indent first line
            alignmentList = document.createElement('li');
            alignmentLabel = document.createElement('label');
            alignmentLabel.setAttribute('for', 'txtIndent');
            alignmentLabel.setAttribute('title', i18n.t('text-alignment.label.indent-first-line'));
            alignmentLabel.className = this.nsClass('label');
            alignmentLabel.innerHTML = i18n.t('text-alignment.label.indent-first-line');
            alignmentList.appendChild(alignmentLabel);
            alignmentGroup.appendChild(alignmentList);
            alignmentList = document.createElement('li');
            indentInput = $ektron('<input id="txtIndent" type="text" class="input-box textbox-narrow" />');
            indentInput.on('blur', function () { that.updateStyle(); });
            if (this.targetElem.indent !== 0) {
                currUnit = this.targetElem.indent.slice(-2);
                indentInput.val(this.targetElem.indent.substring(0, this.targetElem.indent.length - 2));
            }
            alignmentList.appendChild(indentInput.get(0));
            alignmentSelect = $ektron('<select id="ddHIndentUnit" class="select-box txtIndent">').html('<option>EM</option><option>px</option>');
            alignmentSelect.on('change', function () { that.updateStyle(); });
            if (this.targetElem.indent !== 0) {
                alignmentSelect.val(currUnit);
            }
            alignmentList.appendChild(alignmentSelect.get(0));
            alignmentGroup.appendChild(alignmentList);

            alignmentBlock.appendChild(alignmentGroup); 
            if (!this.hasAlignment(currStyle)) {
                alignmentGroup.setAttribute('style', 'display:none');
            }
            return alignmentBlock;
        };

        this.updateStyle = function () {
            var styleString = "", jResult = $ektron(this.targetElem.elem), textAlign, verticalAlign, indentBox;
            jResult.css('text-align', "").css('vertical-align', "").css('text-indent', "");
            if (true === document.getElementById('chk-alignment').checked) { 
                textAlign = $ektron('#ddTextAlignment').val().toLowerCase();
                if (textAlign !== 'default')
                {
                    jResult.css('text-align', textAlign); 
                }

                verticalAlign = $ektron('#ddVAlignment').val().toLowerCase();
                if (verticalAlign !== 'default')
                {
                    jResult.css('vertical-align', verticalAlign);
                }

                indentBox = $ektron('#txtIndent');
                if (!isNaN(indentBox.val()))
                {
                    jResult.css('text-indent', indentBox.val() + indentBox.siblings('.select-box').val());
                }

            }                
        };

        this.deserializeStyle = function (currStyle) {
            var arrStyle, arrProperty, i, strStyle;
            if (typeof currStyle !== 'undefined' && currStyle !== '') {
                arrStyle = currStyle.split(';');
                for (i = 0; i < arrStyle.length; i += 1)
                {
                    strStyle = $ektron.trim(arrStyle[i]);
                    if (0 === strStyle.indexOf('text-align:'))
                    {
                        arrProperty = strStyle.split(' ');
                        this.targetElem.align = arrProperty[1]; 
                    }

                    if  (0 === strStyle.indexOf('vertical-align:')) {
                        arrProperty = strStyle.split(' ');
                        this.targetElem.valign = arrProperty[1]; 
                    }

                    if  (0 === strStyle.indexOf('text-indent:')) {
                        arrProperty = strStyle.split(' ');
                        this.targetElem.indent = arrProperty[1]; 
                    }
                }
            }
        };

        this.hasAlignment = function () {
            if (this.targetElem.align !== 0 || this.targetElem.valign !== 0 || this.targetElem.indent !== 0) {
                return true;
            }
            return false;
        };

        this.showHideModule = function (elem) {
            //$ektron(elem).siblings('ul').toggle();
            var jElem = $ektron(elem);
            if (jElem.is(':checked')) {
                jElem.siblings('ul').show();
            }
            else {
                jElem.siblings('ul').hide();
            }
        };

        //Creates string with this component's namepsace prefixed the each classname
        this.nsClass = function ()
        {
            var stringBuilder = [], prefix = pluginNamespace;
            $ektron.each(arguments, function ()
            {
                stringBuilder.push(this === '' ? prefix : prefix + '-' + this);
            });
            return $ektron.trim(stringBuilder.join(' '));
        };
    }
    return TextAlignmentBlock;
});