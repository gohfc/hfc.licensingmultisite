﻿/*global $*/
/// <reference path="../vendor/qunit/js/qunit.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/jQuery/jquery.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/Ektron/ektron.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/requirejs/require.min.js" />
/// <reference path="../../../../../../../../../../../../UX/vendor/fakequery/fake-query-0.2.js" />
/// <reference path="setup.js" />
/// <reference path="../nls/i18n.js" />
/// <reference path="../lib/border.js" />
/// <reference path="colorhelper.js" />
/// <reference path="../../../../lib/vendor/ektron/ColorPicker/minicolors.js" />

var block, myblock, BorderBlock, $ektron, i18njs,
    clearContainer = function (containerId) {
        "use strict";
        while ($ektron(containerId).children().length > 0) {
            $ektron(containerId).children().remove();
        }
    },
    i18n = {
        i18njs : { "border.checkbox.border": "Border", "border.label.border-type": "Border Type", "border.label.all": "All", "border.label.top": "Top",
		"border.label.bottom": "Bottom", "border.label.top-bottom": "Top and Bottom", "border.label.left": "Left", "border.label.right": "Right",
		"border.label.left-right": "Left and Right", "border.label.border-thickness": "Border Thickness", "border.label.border-style": "Border Style",
        "border.label.solid": "solid", "border.label.dotted": "dotted", "border.label.dashed": "dashed", "border.label.double": "double", 
        "border.label.groove": "groove", "border.label.ridge": "ridge", "border.label.inset": "inset", "border.label.outset": "outset",
		"border.label.border-color": "Border Color" },
        t: function (index) {
            "use strict";
            return this.i18njs[index];
        }
    },
    cph = function ColorHelper(textElem, obj) {
        "use strict";
        this.textElem = textElem;
        this.obj = obj;
        this.hexColor = "";

        this.create = function () {
            return this;
        };

        // selectedColor is rgb value
        this.start = function (selectedColor) {
            var that = this;
            that.hexColor = that.hexc(selectedColor);
            $ektron(textElem).val(that.hexColor);
            //$ektron(textElem).val(that.hexColor);
            //Initialize colorpicker
            //debugger;
            $(textElem).minicolors({
                change: function (hex, opacity) {
                    //debugger;
                    $.event.trigger({
                        type: "ekcolorChanged",
                        message: hex || 'transparent',
                        obj: $(this)
                    });
                }
            });
            $(textElem)
        };

        this.hexc = function (colorval) {
            //translate color value rgb to hex - if no rgb value return white
            var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            if (parts) {
                delete (parts[0]);
                for (var i = 1; i <= 3; ++i) {
                    parts[i] = parseInt(parts[i]).toString(16);
                    if (parts[i].length == 1) parts[i] = '0' + parts[i];
                }
                return parts.join('');
            }
            else {
                return '';
            } 
        }
        return (this);
    };

module("Border style ", {
    setup: function () {
        "use strict";
        $ektron = window.$ektron;
        BorderBlock = window.createModule(i18n, cph);
        block = new BorderBlock();
    }
});

test('Border style fieldset created successfully', function () {
    "use strict";
    var foundBlock, foundCheckbox, foundBorderTypeSelect, foundThicknessBox, foundThicknessUnitSelect, foundBorderStyleSelect, foundBorderColorBox, foundBorderColorSelect, 
        thisId = 'my-div',
        container = $ektron('#dialog-container'),
        block = new BorderBlock(thisId, document.getElementById('divResult'), i18n, cph);
    myblock = block.create();
    container.append(myblock);

    foundBlock = $ektron('#' + thisId, container);
    foundCheckbox = $ektron('input#chk-border' + thisId, container);
    foundBorderTypeSelect = $ektron('.ddBorderType', container);
    foundThicknessBox = $ektron('input#txtThickness', container);
    foundThicknessUnitSelect = $ektron('.ddThicknessUnit', container);
    foundBorderStyleSelect = $ektron('.ddBorderStyle', container);
    foundBorderColorBox = $ektron('input#txtBorderColor' + thisId, container);

    ok(foundBlock.length > 0, 'Border style fieldset found.');
    equal(foundCheckbox.length, 1, 'Border checkbox found.');
    equal(foundBorderTypeSelect.length, 1, 'border type select box is found.');
    equal(foundThicknessBox.length, 1, 'border thickness box is found.');
    equal(foundThicknessUnitSelect.length, 1, 'border thickness unit select box is found.');
    equal(foundBorderStyleSelect.length, 1, 'border style select box found.');
    equal(foundBorderColorBox.length, 1, 'border color text box found.');
});

test('Border style are not set when checkbox is not checked', function () {
    "use strict";
    var bordertype = 'Bottom',
        thickness = 50,
        unit = '%',
        borderstyle = 'groove',
        bordercolor = '000000',
        container = $ektron('#dialog-container'),
        thisId = 'my-div',
        block = new BorderBlock(thisId, document.getElementById('divResult'), i18n, cph);
    clearContainer('#dialog-container');
    myblock = block.create();
    container.append(myblock);

    $ektron('.ddBorderType', container).val(bordertype);
    $ektron('input#txtThickness', container).val(thickness);
    $ektron('.ddThicknessUnit', container).val(unit);
    $ektron('.ddBorderStyle', container).val(borderstyle);
    $ektron('input#txtBorderColor' + thisId, container).val(bordercolor);
    $ektron('#chk-border' + thisId, container).attr('checked', false);
    block.updateStyle();
    block.init();

    ok(typeof $ektron('#divResult').attr('style') === 'undefined' || 0 === $ektron('#divResult').attr('style').length, 'border style is set correctly.');
});

test('Border style are set correctly when checkbox is checked', function () {
    "use strict";
    var targetElement = $ektron('#divResult1'),
        bordertype = 'top',
        thickness = 15,
        unit = 'px',
        borderstyle = 'double',
        rgbcolor = 'rgb(255, 170, 85)',//'ffaa55'
        bordercolor, colorPicker,
        thisId = 'my-div1',
        container = $ektron('#dialog-container1'),
        block = new BorderBlock(thisId, targetElement.get(0), i18n, cph),
        inlineStyle = '';
    colorPicker = new cph($ektron('input#txtBorderColor' + thisId, container));
    bordercolor = colorPicker.hexc(rgbcolor);

    targetElement.css('border-' + bordertype.toLowerCase(), borderstyle + ' ' + thickness + unit + ' #' + bordercolor);
    clearContainer('#dialog-container1');

    myblock = block.create();
    container.append(myblock);
    block.init();

    equal($ektron('img#divResult1').attr('style').indexOf('border:'), -1, 'image border is set correctly.');
    inlineStyle = $ektron('img#divResult1').attr('style');
    bordertype = bordertype.toLowerCase();
    if (navigator.appVersion.indexOf( "MSIE" ) !== -1 || navigator.appVersion.indexOf( "WebKit" ) !== -1) {
        bordertype += '-style';
    }
    ok(inlineStyle.indexOf('border-' + bordertype.toLowerCase() + ': ') > -1, 'Image ' + bordertype + ' border is set correctly.');
    ok(inlineStyle.indexOf(' ' + thickness + unit) > -1, 'Image border thickness is set correctly.');
    ok(inlineStyle.indexOf(' ' + borderstyle.toLowerCase()) > -1, 'Image border style is set correctly.');
    ok(inlineStyle.indexOf(' ' + rgbcolor.toLowerCase()) > -1, 'Image border color is set correctly.');
});

test('fields are reloaded correctly when target image contains width and height styles.', function () {
    "use strict";
    var targetElement = $ektron('#divResult2'), 
        bordertype = 'right',
        thickness = 3,
        unit = 'px',
        borderstyle = 'dotted',
        rgbcolor = 'rgb(85, 85, 85)',//'555555'
        bordercolor, colorPicker,
        container = $ektron('#dialog-container2'),
        thisId = 'my-div2',
        block = new BorderBlock(thisId, targetElement.get(0), i18n, cph);
    colorPicker = new cph($ektron('input#txtBorderColor' + thisId, container));
    bordercolor = colorPicker.hexc(rgbcolor);
    targetElement.css('border-' + bordertype.toLowerCase(), borderstyle + ' ' + thickness + unit + ' #' + bordercolor);
    clearContainer('#dialog-container2');

    myblock = block.create();
    container.append(myblock);
    block.init();

    equal($ektron('select.ddBorderType', container).val(), bordertype, 'border type is set correctly.');
    equal($ektron('input#txtThickness', container).val(), thickness, 'border thickness is set correctly.');
    equal($ektron('select.ddThicknessUnit', container).val(), unit, 'border thickness unit select box is set correctly');
    equal($ektron('select.ddBorderStyle', container).val(), borderstyle, 'border style select box is set correctly');
    ok($ektron('input#txtBorderColor' + thisId, container).val().indexOf(bordercolor) > -1, 'border color is set correctly');
    equal($ektron('input#chk-border' + thisId, container).is(':checked'), true, 'border style checkbox is set true.');
});