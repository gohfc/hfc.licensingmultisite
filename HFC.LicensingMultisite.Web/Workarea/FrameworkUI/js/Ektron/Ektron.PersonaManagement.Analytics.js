﻿//<!-- Google Analytics -->
var accountId = '', personas = [], options = {}, visitorContext,
    personatitle = 'personaTitle', title = 'anonymous',
    gaAccountIdCookie = '_gaAccountId', gaDominantPersonaCookie = '_DominantPersona';



//*****************************************Start Google Analytics CODE***************************************************************
// Read Cookie.
// Reference: http://www.w3schools.com/js/js_cookies.asp

function getCookie(cName) {
    'use strict';
    var cStart, cEnd, cValue;
    /*ignore jslint start*/
    cValue = document.cookie,
    /*ignore jslint end*/
    cStart = cValue.indexOf(' ' + cName + '=');
    if (cStart === -1) {
        cStart = cValue.indexOf(cName + '=');
    }
    if (cStart === -1) {
        cValue = null;
    }
    else {
        cStart = cValue.indexOf('=', cStart) + 1;
        cEnd = cValue.indexOf(';', cStart);
        if (cEnd === -1) {
            cEnd = cValue.length;
        }
        /*ignore jslint start*/
        cValue = unescape(cValue.substring(cStart, cEnd));
        /*ignore jslint end*/
        cEnd = cValue.indexOf('&');
        if (cEnd !== -1) {
            /*ignore jslint start*/
            cValue = unescape(cValue.substring(0, cEnd));
            /*ignore jslint end*/
        }

    }
    return cValue;
}

/*ignore jslint start*/
(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'personaga');
/*ignore jslint end*/

// Track Custom Variables.
//personaga('create', accountId);
//personaga('send', 'pageview');


var _gaq = _gaq || [];
accountId = getCookie(gaAccountIdCookie);
if (accountId !== null) {
    _gaq.push(['_setAccount', accountId]);
}
title = getCookie(gaDominantPersonaCookie);
if (title !== null) {
    _gaq.push(['_setCustomVar', 1, personatitle, title, 1]);
}
_gaq.push(['_trackPageview']);

/*ignore jslint start*/
(function () {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
/*ignore jslint end*/

//****************************************End Google Analytics CODE******************************************************************



//<!-- End Google Analytics -->