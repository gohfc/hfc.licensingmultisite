﻿/*
    The Ektron.Namespace object gives us a method to create namespaces on the fly 
    with a single line of code like this:

                Ektron.Namespace.Register(Ektron.Controls.Validation);  // creates the namespace
    
    The utility also includes a method that accepts a string representing a namespace to see if that 
    namespace already exists.  This can be useful if you want to merely extend an existing namespace 
    (using jQuery’s .extend method for example), or merge objects within a namespace instead of 
    overwriting them.
                
                // the following line outputs to the console whether or not the
                // Ektron.Controls.Validation.Overrides exists or not.
                console.log("Ektron.Controls.Validation.Overrides exists:  " + Ektron.Namespace.Exists("Ektron.Controls.Validation.Overrides"));

*/

if ("undefined" == typeof Ektron) { Ektron = {}; }
if ("undefined" == typeof Ektron.Namespace) {
    Ektron.Namespace = {
        // Register checks if a namespace already exists, and if not creates it
        Register: function (namespace) {
            namespace = namespace.split('.');

            if (!window[namespace[0]]) {
                window[namespace[0]] = {};
            }

            var strFullNamespace = namespace[0];
            for (var i = 1; i < namespace.length; i++) {
                strFullNamespace += "." + namespace[i];
                eval("if(!window." + strFullNamespace + ")window." + strFullNamespace + "={};");
            }
        },

        // Checks to see if a given namespace already exists.  accepts the string representing the namespace as a parameter.
        Exists: function (namespace) {
            var exists = false;
            try {
                var namespace = eval(namespace);
                if (namespace) {
                    exists = true;
                }
            }
            catch (e) {
                return exists
            }

            return exists;
        }
    }
}