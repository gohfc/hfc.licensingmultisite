// for IE11
if (window && 'undefined' === typeof(window.attachEvent)) {
    window.attachEvent = function (event, handler) {
        var ev = event.replace(/^on(.*)/g, "$1"); // slice "on" convention that IE implements
        if (window.addEventListener) {
            window.addEventListener(ev, handler, false);
        }
        else if (console && 'undefined' !== typeof(console.log)){
            console.log('failed to attach ' + event + ' to:');
            console.log(handler.toString());
        }
    }
}