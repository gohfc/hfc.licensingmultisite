﻿$ektron.validator.addMethod("decimal", function (value, element) {
    var isDecimal = false;
    var elementObj = $ektron(element);
    var culture = elementObj.data()["ektron-global-culture"] ? elementObj.data()["ektron-global-culture"] : "default";

    // ensure the globalization plugin is loaded
    if ("undefined" == typeof ($ektron.global.parseFloat)) {
        throw "The Globalization plugin must be loaded to use this method.";
        return isDecimal;
    }
    try {
        var tryDecimal = $ektron.global.parseFloat(value, 10, culture);
        if (tryDecimal !== null && !(isNaN(tryDecimal))) {
            isDecimal = true;
        }
    }
    catch (e) {
        return false;
    }
    return this.optional(element) || isDecimal;
}, "Please enter a correctly formatted decimal.");