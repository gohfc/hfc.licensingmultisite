﻿$ektron.validator.addMethod("alllowercase", function (value, element) {
    return this.optional(element) || value === value.toLowerCase();
}, "Lower case value only please.");