﻿$ektron.validator.addMethod("time", function (value, element) {
    return this.optional(element) || /^([01][0-9])|(2[0123]):([0-5])([0-9])$/.test(value);
}, "Please enter a valid time, between 00:00 and 23:59"
);