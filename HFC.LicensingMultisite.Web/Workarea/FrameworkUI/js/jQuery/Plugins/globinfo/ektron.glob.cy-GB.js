(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["cy-GB"] = $.extend(true, {}, en, {
            name: "cy-GB",
            englishName: "Welsh (United Kingdom)",
            nativeName: "Cymraeg (y Deyrnas Unedig)",
            language: "cy",
            numberFormat: {
                percent: {
                    pattern: ["-%n", "%n"]
                },
                currency: {
                    pattern: ["-$n", "$n"],
                    symbol: "£"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    firstDay: 1,
                    days: {
                        names: ["Dydd Sul", "Dydd Llun", "Dydd Mawrth", "Dydd Mercher", "Dydd Iau", "Dydd Gwener", "Dydd Sadwrn"],
                        namesAbbr: ["Sul", "Llun", "Maw", "Mer", "Iau", "Gwe", "Sad"],
                        namesShort: ["Su", "Ll", "Ma", "Me", "Ia", "Gw", "Sa"]
                    },
                    months: {
                        names: ["Ionawr", "Chwefror", "Mawrth", "Ebrill", "Mai", "Mehefin", "Gorffennaf", "Awst", "Medi", "Hydref", "Tachwedd", "Rhagfyr", ""],
                        namesAbbr: ["Ion", "Chwe", "Maw", "Ebr", "Mai", "Meh", "Gor", "Aws", "Med", "Hyd", "Tach", "Rhag", ""]
                    },
                    AM: ["a.m.", "a.m.", "A.M."],
                    PM: ["p.m.", "p.m.", "P.M."],
                    patterns: {
                        d: "dd/MM/yyyy",
                        D: "dd MMMM yyyy",
                        t: "HH:mm",
                        T: "HH:mm:ss",
                        f: "dd MMMM yyyy HH:mm",
                        F: "dd MMMM yyyy HH:mm:ss",
                        M: "dd MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["cy-GB"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['cy-GB'] = {
        closeText: 'Done',
        prevText: 'Prev',
        nextText: 'Next',
        currentText: 'Today',
        monthNames: ['Ionawr', 'Chwefror', 'Mawrth', 'Ebrill', 'Mai', 'Mehefin',
        'Gorffennaf', 'Awst', 'Medi', 'Hydref', 'Tachwedd', 'Rhagfyr'],
        monthNamesShort: ['Ion', 'Chw', 'Maw', 'Ebr', 'Mai', 'Meh',
        'Gor', 'Aws', 'Med', 'Hyd', 'Tac', 'Rha'],
        dayNames: ['Dydd Sul', 'Dydd Llun', 'Dydd Mawrth', 'Dydd Mercher', 'Dydd Iau', 'Dydd Gwener', 'Dydd Sadwrn'],
        dayNamesShort: ['Sul', 'Llu', 'Maw', 'Mer', 'Iau', 'Gwe', 'Sad'],
        dayNamesMin: ['Su', 'Ll', 'Ma', 'Me', 'Ia', 'Gw', 'Sa'],
        weekHeader: 'Wy',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['cy-GB']);
})($ektron);