(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["sr"] = $.extend(true, {}, en, {
            name: "sr",
            englishName: "Serbian",
            nativeName: "srpski",
            language: "sr",
            numberFormat: {
                ',': ".",
                '.': ",",
                percent: {
                    pattern: ["-n%", "n%"],
                    ',': ".",
                    '.': ","
                },
                currency: {
                    pattern: ["-n $", "n $"],
                    ',': ".",
                    '.': ",",
                    symbol: "Din."
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': ".",
                    firstDay: 1,
                    days: {
                        names: ["nedelja", "ponedeljak", "utorak", "sreda", "četvrtak", "petak", "subota"],
                        namesAbbr: ["ned", "pon", "uto", "sre", "čet", "pet", "sub"],
                        namesShort: ["ne", "po", "ut", "sr", "če", "pe", "su"]
                    },
                    months: {
                        names: ["januar", "februar", "mart", "april", "maj", "jun", "jul", "avgust", "septembar", "oktobar", "novembar", "decembar", ""],
                        namesAbbr: ["jan", "feb", "mar", "apr", "maj", "jun", "jul", "avg", "sep", "okt", "nov", "dec", ""]
                    },
                    AM: null,
                    PM: null,
                    eras: [{ "name": "n.e.", "start": null, "offset": 0 }],
                    patterns: {
                        d: "d.M.yyyy",
                        D: "d. MMMM yyyy",
                        t: "H:mm",
                        T: "H:mm:ss",
                        f: "d. MMMM yyyy H:mm",
                        F: "d. MMMM yyyy H:mm:ss",
                        M: "d. MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["sr"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['sr'] = {
        closeText: 'Затвори',
        prevText: '&#x3C;',
        nextText: '&#x3E;',
        currentText: 'Данас',
        monthNames: ['Јануар', 'Фебруар', 'Март', 'Април', 'Мај', 'Јун',
        'Јул', 'Август', 'Септембар', 'Октобар', 'Новембар', 'Децембар'],
        monthNamesShort: ['Јан', 'Феб', 'Мар', 'Апр', 'Мај', 'Јун',
        'Јул', 'Авг', 'Сеп', 'Окт', 'Нов', 'Дец'],
        dayNames: ['Недеља', 'Понедељак', 'Уторак', 'Среда', 'Четвртак', 'Петак', 'Субота'],
        dayNamesShort: ['Нед', 'Пон', 'Уто', 'Сре', 'Чет', 'Пет', 'Суб'],
        dayNamesMin: ['Не', 'По', 'Ут', 'Ср', 'Че', 'Пе', 'Су'],
        weekHeader: 'Сед',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['sr']);
})($ektron);