(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["sq"] = $.extend(true, {}, en, {
            name: "sq",
            englishName: "Albanian",
            nativeName: "shqipe",
            language: "sq",
            numberFormat: {
                ',': ".",
                '.': ",",
                percent: {
                    ',': ".",
                    '.': ","
                },
                currency: {
                    pattern: ["-n$", "n$"],
                    ',': ".",
                    '.': ",",
                    symbol: "Lek"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': "-",
                    firstDay: 1,
                    days: {
                        names: ["e diel", "e hënë", "e martë", "e mërkurë", "e enjte", "e premte", "e shtunë"],
                        namesAbbr: ["Die", "Hën", "Mar", "Mër", "Enj", "Pre", "Sht"],
                        namesShort: ["Di", "Hë", "Ma", "Më", "En", "Pr", "Sh"]
                    },
                    months: {
                        names: ["janar", "shkurt", "mars", "prill", "maj", "qershor", "korrik", "gusht", "shtator", "tetor", "nëntor", "dhjetor", ""],
                        namesAbbr: ["Jan", "Shk", "Mar", "Pri", "Maj", "Qer", "Kor", "Gsh", "Sht", "Tet", "Nën", "Dhj", ""]
                    },
                    AM: ["PD", "pd", "PD"],
                    PM: ["MD", "md", "MD"],
                    patterns: {
                        d: "yyyy-MM-dd",
                        D: "yyyy-MM-dd",
                        t: "h:mm.tt",
                        T: "h:mm:ss.tt",
                        f: "yyyy-MM-dd h:mm.tt",
                        F: "yyyy-MM-dd h:mm:ss.tt",
                        Y: "yyyy-MM"
                    }
                })
            }
        }, cultures["sq"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['sq'] = {
        closeText: 'mbylle',
        prevText: '&#x3C;mbrapa',
        nextText: 'Përpara&#x3E;',
        currentText: 'sot',
        monthNames: ['Janar', 'Shkurt', 'Mars', 'Prill', 'Maj', 'Qershor',
        'Korrik', 'Gusht', 'Shtator', 'Tetor', 'Nëntor', 'Dhjetor'],
        monthNamesShort: ['Jan', 'Shk', 'Mar', 'Pri', 'Maj', 'Qer',
        'Kor', 'Gus', 'Sht', 'Tet', 'Nën', 'Dhj'],
        dayNames: ['E Diel', 'E Hënë', 'E Martë', 'E Mërkurë', 'E Enjte', 'E Premte', 'E Shtune'],
        dayNamesShort: ['Di', 'Hë', 'Ma', 'Më', 'En', 'Pr', 'Sh'],
        dayNamesMin: ['Di', 'Hë', 'Ma', 'Më', 'En', 'Pr', 'Sh'],
        weekHeader: 'Ja',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['sq']);
})($ektron);