(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["sl"] = $.extend(true, {}, en, {
            name: "sl",
            englishName: "Slovenian",
            nativeName: "slovenski",
            language: "sl",
            numberFormat: {
                ',': ".",
                '.': ",",
                percent: {
                    pattern: ["-n%", "n%"],
                    ',': ".",
                    '.': ","
                },
                currency: {
                    pattern: ["-n $", "n $"],
                    ',': ".",
                    '.': ",",
                    symbol: "€"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': ".",
                    firstDay: 1,
                    days: {
                        names: ["nedelja", "ponedeljek", "torek", "sreda", "četrtek", "petek", "sobota"],
                        namesAbbr: ["ned", "pon", "tor", "sre", "čet", "pet", "sob"],
                        namesShort: ["ne", "po", "to", "sr", "če", "pe", "so"]
                    },
                    months: {
                        names: ["januar", "februar", "marec", "april", "maj", "junij", "julij", "avgust", "september", "oktober", "november", "december", ""],
                        namesAbbr: ["jan", "feb", "mar", "apr", "maj", "jun", "jul", "avg", "sep", "okt", "nov", "dec", ""]
                    },
                    AM: null,
                    PM: null,
                    patterns: {
                        d: "d.M.yyyy",
                        D: "d. MMMM yyyy",
                        t: "H:mm",
                        T: "H:mm:ss",
                        f: "d. MMMM yyyy H:mm",
                        F: "d. MMMM yyyy H:mm:ss",
                        M: "d. MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["sl"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['sl'] = {
        closeText: 'Zapri',
        prevText: '&#x3C;Prejšnji',
        nextText: 'Naslednji&#x3E;',
        currentText: 'Trenutni',
        monthNames: ['Januar', 'Februar', 'Marec', 'April', 'Maj', 'Junij',
        'Julij', 'Avgust', 'September', 'Oktober', 'November', 'December'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun',
        'Jul', 'Avg', 'Sep', 'Okt', 'Nov', 'Dec'],
        dayNames: ['Nedelja', 'Ponedeljek', 'Torek', 'Sreda', 'Četrtek', 'Petek', 'Sobota'],
        dayNamesShort: ['Ned', 'Pon', 'Tor', 'Sre', 'Čet', 'Pet', 'Sob'],
        dayNamesMin: ['Ne', 'Po', 'To', 'Sr', 'Če', 'Pe', 'So'],
        weekHeader: 'Teden',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['sl']);
})($ektron);