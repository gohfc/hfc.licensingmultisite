(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["ml"] = $.extend(true, {}, en, {
            name: "ml",
            englishName: "Malayalam",
            nativeName: "മലയാളം",
            language: "ml",
            numberFormat: {
                groupSizes: [3, 2],
                percent: {
                    pattern: ["-%n", "%n"],
                    groupSizes: [3, 2]
                },
                currency: {
                    pattern: ["$ -n", "$ n"],
                    groupSizes: [3, 2],
                    symbol: "ക"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': "-",
                    ':': ".",
                    firstDay: 1,
                    days: {
                        names: ["ഞായറാഴ്ച", "തിങ്കളാഴ്ച", "ചൊവ്വാഴ്ച", "ബുധനാഴ്ച", "വ്യാഴാഴ്ച", "വെള്ളിയാഴ്ച", "ശനിയാഴ്ച"],
                        namesAbbr: ["ഞായർ.", "തിങ്കൾ.", "ചൊവ്വ.", "ബുധൻ.", "വ്യാഴം.", "വെള്ളി.", "ശനി."],
                        namesShort: ["ഞ", "ത", "ച", "ബ", "വ", "വെ", "ശ"]
                    },
                    months: {
                        names: ["ജനുവരി", "ഫെബ്റുവരി", "മാറ്ച്ച്", "ഏപ്റില്", "മെയ്", "ജൂണ്", "ജൂലൈ", "ഓഗസ്ററ്", "സെപ്ററംബറ്", "ഒക്ടോബറ്", "നവംബറ്", "ഡിസംബറ്", ""],
                        namesAbbr: ["ജനുവരി", "ഫെബ്റുവരി", "മാറ്ച്ച്", "ഏപ്റില്", "മെയ്", "ജൂണ്", "ജൂലൈ", "ഓഗസ്ററ്", "സെപ്ററംബറ്", "ഒക്ടോബറ്", "നവംബറ്", "ഡിസംബറ്", ""]
                    },
                    patterns: {
                        d: "dd-MM-yy",
                        D: "dd MMMM yyyy",
                        t: "HH.mm",
                        T: "HH.mm.ss",
                        f: "dd MMMM yyyy HH.mm",
                        F: "dd MMMM yyyy HH.mm.ss",
                        M: "dd MMMM"
                    }
                })
            }
        }, cultures["ml"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['ml'] = {
        closeText: 'ശരി',
        prevText: 'മുന്നത്തെ',
        nextText: 'അടുത്തത് ',
        currentText: 'ഇന്ന്',
        monthNames: ['ജനുവരി', 'ഫെബ്രുവരി', 'മാര്‍ച്ച്', 'ഏപ്രില്‍', 'മേയ്', 'ജൂണ്‍',
        'ജൂലൈ', 'ആഗസ്റ്റ്', 'സെപ്റ്റംബര്‍', 'ഒക്ടോബര്‍', 'നവംബര്‍', 'ഡിസംബര്‍'],
        monthNamesShort: ['ജനു', 'ഫെബ്', 'മാര്‍', 'ഏപ്രി', 'മേയ്', 'ജൂണ്‍',
        'ജൂലാ', 'ആഗ', 'സെപ്', 'ഒക്ടോ', 'നവം', 'ഡിസ'],
        dayNames: ['ഞായര്‍', 'തിങ്കള്‍', 'ചൊവ്വ', 'ബുധന്‍', 'വ്യാഴം', 'വെള്ളി', 'ശനി'],
        dayNamesShort: ['ഞായ', 'തിങ്ക', 'ചൊവ്വ', 'ബുധ', 'വ്യാഴം', 'വെള്ളി', 'ശനി'],
        dayNamesMin: ['ഞാ', 'തി', 'ചൊ', 'ബു', 'വ്യാ', 'വെ', 'ശ'],
        weekHeader: 'ആ',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['ml']);
})($ektron);