(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["en-NZ"] = $.extend(true, {}, en, {
            name: "en-NZ",
            englishName: "English (New Zealand)",
            nativeName: "English (New Zealand)",
            numberFormat: {
                currency: {
                    pattern: ["-$n", "$n"]
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    firstDay: 1,
                    AM: ["a.m.", "a.m.", "A.M."],
                    PM: ["p.m.", "p.m.", "P.M."],
                    patterns: {
                        d: "d/MM/yyyy",
                        D: "dddd, d MMMM yyyy",
                        f: "dddd, d MMMM yyyy h:mm tt",
                        F: "dddd, d MMMM yyyy h:mm:ss tt",
                        M: "dd MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["en-NZ"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['en-NZ'] = {
        closeText: 'Done',
        prevText: 'Prev',
        nextText: 'Next',
        currentText: 'Today',
        monthNames: ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        weekHeader: 'Wk',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['en-NZ']);
})($ektron);