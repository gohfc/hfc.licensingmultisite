(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["fo"] = $.extend(true, {}, en, {
            name: "fo",
            englishName: "Faroese",
            nativeName: "føroyskt",
            language: "fo",
            numberFormat: {
                ',': ".",
                '.': ",",
                percent: {
                    pattern: ["-n%", "n%"],
                    ',': ".",
                    '.': ","
                },
                currency: {
                    pattern: ["$ -n", "$ n"],
                    ',': ".",
                    '.': ",",
                    symbol: "kr."
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': "-",
                    firstDay: 1,
                    days: {
                        names: ["sunnudagur", "mánadagur", "týsdagur", "mikudagur", "hósdagur", "fríggjadagur", "leygardagur"],
                        namesAbbr: ["sun", "mán", "týs", "mik", "hós", "frí", "leyg"],
                        namesShort: ["su", "má", "tý", "mi", "hó", "fr", "ley"]
                    },
                    months: {
                        names: ["januar", "februar", "mars", "apríl", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember", ""],
                        namesAbbr: ["jan", "feb", "mar", "apr", "mai", "jun", "jul", "aug", "sep", "okt", "nov", "des", ""]
                    },
                    AM: null,
                    PM: null,
                    patterns: {
                        d: "dd-MM-yyyy",
                        D: "d. MMMM yyyy",
                        t: "HH:mm",
                        T: "HH:mm:ss",
                        f: "d. MMMM yyyy HH:mm",
                        F: "d. MMMM yyyy HH:mm:ss",
                        M: "d. MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["fo"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['fo'] = {
        closeText: 'Lat aftur',
        prevText: '&#x3C;Fyrra',
        nextText: 'Næsta&#x3E;',
        currentText: 'Í dag',
        monthNames: ['Januar', 'Februar', 'Mars', 'Apríl', 'Mei', 'Juni',
        'Juli', 'August', 'September', 'Oktober', 'November', 'Desember'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun',
        'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des'],
        dayNames: ['Sunnudagur', 'Mánadagur', 'Týsdagur', 'Mikudagur', 'Hósdagur', 'Fríggjadagur', 'Leyardagur'],
        dayNamesShort: ['Sun', 'Mán', 'Týs', 'Mik', 'Hós', 'Frí', 'Ley'],
        dayNamesMin: ['Su', 'Má', 'Tý', 'Mi', 'Hó', 'Fr', 'Le'],
        weekHeader: 'Vk',
        dateFormat: 'dd-mm-yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['fo']);
})($ektron);