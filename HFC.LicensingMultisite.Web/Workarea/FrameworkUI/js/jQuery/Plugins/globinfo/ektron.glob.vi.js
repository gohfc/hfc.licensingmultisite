(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["vi"] = $.extend(true, {}, en, {
            name: "vi",
            englishName: "Vietnamese",
            nativeName: "Tiếng Việt",
            language: "vi",
            numberFormat: {
                ',': ".",
                '.': ",",
                percent: {
                    ',': ".",
                    '.': ","
                },
                currency: {
                    pattern: ["-n $", "n $"],
                    ',': ".",
                    '.': ",",
                    symbol: "₫"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    firstDay: 1,
                    days: {
                        names: ["Chủ Nhật", "Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy"],
                        namesAbbr: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
                        namesShort: ["C", "H", "B", "T", "N", "S", "B"]
                    },
                    months: {
                        names: ["Tháng Giêng", "Tháng Hai", "Tháng Ba", "Tháng Tư", "Tháng Năm", "Tháng Sáu", "Tháng Bảy", "Tháng Tám", "Tháng Chín", "Tháng Mười", "Tháng Mười Một", "Tháng Mười Hai", ""],
                        namesAbbr: ["Thg1", "Thg2", "Thg3", "Thg4", "Thg5", "Thg6", "Thg7", "Thg8", "Thg9", "Thg10", "Thg11", "Thg12", ""]
                    },
                    AM: ["SA", "sa", "SA"],
                    PM: ["CH", "ch", "CH"],
                    patterns: {
                        d: "dd/MM/yyyy",
                        D: "dd MMMM yyyy",
                        f: "dd MMMM yyyy h:mm tt",
                        F: "dd MMMM yyyy h:mm:ss tt",
                        M: "dd MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["vi"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['vi'] = {
        closeText: 'Đóng',
        prevText: '&#x3C;Trước',
        nextText: 'Tiếp&#x3E;',
        currentText: 'Hôm nay',
        monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',
        'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],
        monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
        'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
        dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
        dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
        dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
        weekHeader: 'Tu',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['vi']);
})($ektron);