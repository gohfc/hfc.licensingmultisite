(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["mk"] = $.extend(true, {}, en, {
            name: "mk",
            englishName: "Macedonian (FYROM)",
            nativeName: "македонски јазик",
            language: "mk",
            numberFormat: {
                ',': ".",
                '.': ",",
                percent: {
                    ',': ".",
                    '.': ","
                },
                currency: {
                    pattern: ["-n $", "n $"],
                    ',': ".",
                    '.': ",",
                    symbol: "ден."
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': ".",
                    firstDay: 1,
                    days: {
                        names: ["недела", "понеделник", "вторник", "среда", "четврток", "петок", "сабота"],
                        namesAbbr: ["нед", "пон", "втр", "срд", "чет", "пет", "саб"],
                        namesShort: ["не", "по", "вт", "ср", "че", "пе", "са"]
                    },
                    months: {
                        names: ["јануари", "февруари", "март", "април", "мај", "јуни", "јули", "август", "септември", "октомври", "ноември", "декември", ""],
                        namesAbbr: ["јан", "фев", "мар", "апр", "мај", "јун", "јул", "авг", "сеп", "окт", "ное", "дек", ""]
                    },
                    AM: null,
                    PM: null,
                    patterns: {
                        d: "dd.MM.yyyy",
                        D: "dddd, dd MMMM yyyy",
                        t: "HH:mm",
                        T: "HH:mm:ss",
                        f: "dddd, dd MMMM yyyy HH:mm",
                        F: "dddd, dd MMMM yyyy HH:mm:ss",
                        M: "dd MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["mk"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['mk'] = {
        closeText: 'Затвори',
        prevText: '&#x3C;',
        nextText: '&#x3E;',
        currentText: 'Денес',
        monthNames: ['Јануари', 'Февруари', 'Март', 'Април', 'Мај', 'Јуни',
        'Јули', 'Август', 'Септември', 'Октомври', 'Ноември', 'Декември'],
        monthNamesShort: ['Јан', 'Фев', 'Мар', 'Апр', 'Мај', 'Јун',
        'Јул', 'Авг', 'Сеп', 'Окт', 'Ное', 'Дек'],
        dayNames: ['Недела', 'Понеделник', 'Вторник', 'Среда', 'Четврток', 'Петок', 'Сабота'],
        dayNamesShort: ['Нед', 'Пон', 'Вто', 'Сре', 'Чет', 'Пет', 'Саб'],
        dayNamesMin: ['Не', 'По', 'Вт', 'Ср', 'Че', 'Пе', 'Са'],
        weekHeader: 'Сед',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['mk']);
})($ektron);