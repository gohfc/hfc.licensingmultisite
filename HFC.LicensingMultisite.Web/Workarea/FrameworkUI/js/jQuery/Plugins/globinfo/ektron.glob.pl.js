(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["pl"] = $.extend(true, {}, en, {
            name: "pl",
            englishName: "Polish",
            nativeName: "polski",
            language: "pl",
            numberFormat: {
                ',': " ",
                '.': ",",
                percent: {
                    pattern: ["-n%", "n%"],
                    ',': " ",
                    '.': ","
                },
                currency: {
                    pattern: ["-n $", "n $"],
                    ',': " ",
                    '.': ",",
                    symbol: "zł"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': "-",
                    firstDay: 1,
                    days: {
                        names: ["niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"],
                        namesAbbr: ["N", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
                        namesShort: ["N", "Pn", "Wt", "Śr", "Cz", "Pt", "So"]
                    },
                    months: {
                        names: ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień", ""],
                        namesAbbr: ["sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paź", "lis", "gru", ""]
                    },
                    monthsGenitive: {
                        names: ["stycznia", "lutego", "marca", "kwietnia", "maja", "czerwca", "lipca", "sierpnia", "września", "października", "listopada", "grudnia", ""],
                        namesAbbr: ["sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paź", "lis", "gru", ""]
                    },
                    AM: null,
                    PM: null,
                    patterns: {
                        d: "yyyy-MM-dd",
                        D: "d MMMM yyyy",
                        t: "HH:mm",
                        T: "HH:mm:ss",
                        f: "d MMMM yyyy HH:mm",
                        F: "d MMMM yyyy HH:mm:ss",
                        M: "d MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["pl"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['pl'] = {
        closeText: 'Zamknij',
        prevText: '&#x3C;Poprzedni',
        nextText: 'Następny&#x3E;',
        currentText: 'Dziś',
        monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec',
        'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
        monthNamesShort: ['Sty', 'Lu', 'Mar', 'Kw', 'Maj', 'Cze',
        'Lip', 'Sie', 'Wrz', 'Pa', 'Lis', 'Gru'],
        dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
        dayNamesShort: ['Nie', 'Pn', 'Wt', 'Śr', 'Czw', 'Pt', 'So'],
        dayNamesMin: ['N', 'Pn', 'Wt', 'Śr', 'Cz', 'Pt', 'So'],
        weekHeader: 'Tydz',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['pl']);
})($ektron);