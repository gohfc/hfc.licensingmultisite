(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["da"] = $.extend(true, {}, en, {
            name: "da",
            englishName: "Danish",
            nativeName: "dansk",
            language: "da",
            numberFormat: {
                ',': ".",
                '.': ",",
                percent: {
                    ',': ".",
                    '.': ","
                },
                currency: {
                    pattern: ["$ -n", "$ n"],
                    ',': ".",
                    '.': ",",
                    symbol: "kr."
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': "-",
                    firstDay: 1,
                    days: {
                        names: ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"],
                        namesAbbr: ["sø", "ma", "ti", "on", "to", "fr", "lø"],
                        namesShort: ["sø", "ma", "ti", "on", "to", "fr", "lø"]
                    },
                    months: {
                        names: ["januar", "februar", "marts", "april", "maj", "juni", "juli", "august", "september", "oktober", "november", "december", ""],
                        namesAbbr: ["jan", "feb", "mar", "apr", "maj", "jun", "jul", "aug", "sep", "okt", "nov", "dec", ""]
                    },
                    AM: null,
                    PM: null,
                    patterns: {
                        d: "dd-MM-yyyy",
                        D: "d. MMMM yyyy",
                        t: "HH:mm",
                        T: "HH:mm:ss",
                        f: "d. MMMM yyyy HH:mm",
                        F: "d. MMMM yyyy HH:mm:ss",
                        M: "d. MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["da"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['da'] = {
        closeText: 'Luk',
        prevText: '&#x3C;Forrige',
        nextText: 'Næste&#x3E;',
        currentText: 'Idag',
        monthNames: ['Januar', 'Februar', 'Marts', 'April', 'Maj', 'Juni',
        'Juli', 'August', 'September', 'Oktober', 'November', 'December'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun',
        'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
        dayNames: ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag'],
        dayNamesShort: ['Søn', 'Man', 'Tir', 'Ons', 'Tor', 'Fre', 'Lør'],
        dayNamesMin: ['Sø', 'Ma', 'Ti', 'On', 'To', 'Fr', 'Lø'],
        weekHeader: 'Uge',
        dateFormat: 'dd-mm-yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['da']);
})($ektron);