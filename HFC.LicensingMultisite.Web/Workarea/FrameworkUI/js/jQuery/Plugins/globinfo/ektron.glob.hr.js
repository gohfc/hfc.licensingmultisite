(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["hr"] = $.extend(true, {}, en, {
            name: "hr",
            englishName: "Croatian",
            nativeName: "hrvatski",
            language: "hr",
            numberFormat: {
                pattern: ["- n"],
                ',': ".",
                '.': ",",
                percent: {
                    pattern: ["-n%", "n%"],
                    ',': ".",
                    '.': ","
                },
                currency: {
                    pattern: ["-n $", "n $"],
                    ',': ".",
                    '.': ",",
                    symbol: "kn"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': ".",
                    firstDay: 1,
                    days: {
                        names: ["nedjelja", "ponedjeljak", "utorak", "srijeda", "četvrtak", "petak", "subota"],
                        namesAbbr: ["ned", "pon", "uto", "sri", "čet", "pet", "sub"],
                        namesShort: ["ne", "po", "ut", "sr", "če", "pe", "su"]
                    },
                    months: {
                        names: ["siječanj", "veljača", "ožujak", "travanj", "svibanj", "lipanj", "srpanj", "kolovoz", "rujan", "listopad", "studeni", "prosinac", ""],
                        namesAbbr: ["sij", "vlj", "ožu", "tra", "svi", "lip", "srp", "kol", "ruj", "lis", "stu", "pro", ""]
                    },
                    monthsGenitive: {
                        names: ["siječnja", "veljače", "ožujka", "travnja", "svibnja", "lipnja", "srpnja", "kolovoza", "rujna", "listopada", "studenog", "prosinca", ""],
                        namesAbbr: ["sij", "vlj", "ožu", "tra", "svi", "lip", "srp", "kol", "ruj", "lis", "stu", "pro", ""]
                    },
                    AM: null,
                    PM: null,
                    patterns: {
                        d: "d.M.yyyy.",
                        D: "d. MMMM yyyy.",
                        t: "H:mm",
                        T: "H:mm:ss",
                        f: "d. MMMM yyyy. H:mm",
                        F: "d. MMMM yyyy. H:mm:ss",
                        M: "d. MMMM"
                    }
                })
            }
        }, cultures["hr"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['hr'] = {
        closeText: 'Zatvori',
        prevText: '&#x3C;',
        nextText: '&#x3E;',
        currentText: 'Danas',
        monthNames: ['Siječanj', 'Veljača', 'Ožujak', 'Travanj', 'Svibanj', 'Lipanj',
        'Srpanj', 'Kolovoz', 'Rujan', 'Listopad', 'Studeni', 'Prosinac'],
        monthNamesShort: ['Sij', 'Velj', 'Ožu', 'Tra', 'Svi', 'Lip',
        'Srp', 'Kol', 'Ruj', 'Lis', 'Stu', 'Pro'],
        dayNames: ['Nedjelja', 'Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak', 'Subota'],
        dayNamesShort: ['Ned', 'Pon', 'Uto', 'Sri', 'Čet', 'Pet', 'Sub'],
        dayNamesMin: ['Ne', 'Po', 'Ut', 'Sr', 'Če', 'Pe', 'Su'],
        weekHeader: 'Tje',
        dateFormat: 'dd.mm.yy.',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['hr']);
})($ektron);