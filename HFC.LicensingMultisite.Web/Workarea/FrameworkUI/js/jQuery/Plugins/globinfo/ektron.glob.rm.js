(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["rm"] = $.extend(true, {}, en, {
            name: "rm",
            englishName: "Romansh",
            nativeName: "Rumantsch",
            language: "rm",
            numberFormat: {
                ',': "'",
                percent: {
                    pattern: ["-n%", "n%"],
                    ',': "'"
                },
                currency: {
                    pattern: ["$-n", "$ n"],
                    ',': "'",
                    symbol: "fr."
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    firstDay: 1,
                    days: {
                        names: ["dumengia", "glindesdi", "mardi", "mesemna", "gievgia", "venderdi", "sonda"],
                        namesAbbr: ["du", "gli", "ma", "me", "gie", "ve", "so"],
                        namesShort: ["du", "gli", "ma", "me", "gie", "ve", "so"]
                    },
                    months: {
                        names: ["schaner", "favrer", "mars", "avrigl", "matg", "zercladur", "fanadur", "avust", "settember", "october", "november", "december", ""],
                        namesAbbr: ["schan", "favr", "mars", "avr", "matg", "zercl", "fan", "avust", "sett", "oct", "nov", "dec", ""]
                    },
                    AM: null,
                    PM: null,
                    eras: [{ "name": "s. Cr.", "start": null, "offset": 0 }],
                    patterns: {
                        d: "dd/MM/yyyy",
                        D: "dddd, d MMMM yyyy",
                        t: "HH:mm",
                        T: "HH:mm:ss",
                        f: "dddd, d MMMM yyyy HH:mm",
                        F: "dddd, d MMMM yyyy HH:mm:ss",
                        M: "dd MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["rm"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['rm'] = {
        closeText: 'Serrar',
        prevText: '&#x3C;Suandant',
        nextText: 'Precedent&#x3E;',
        currentText: 'Actual',
        monthNames: ['Schaner', 'Favrer', 'Mars', 'Avrigl', 'Matg', 'Zercladur', 'Fanadur', 'Avust', 'Settember', 'October', 'November', 'December'],
        monthNamesShort: ['Scha', 'Fev', 'Mar', 'Avr', 'Matg', 'Zer', 'Fan', 'Avu', 'Sett', 'Oct', 'Nov', 'Dec'],
        dayNames: ['Dumengia', 'Glindesdi', 'Mardi', 'Mesemna', 'Gievgia', 'Venderdi', 'Sonda'],
        dayNamesShort: ['Dum', 'Gli', 'Mar', 'Mes', 'Gie', 'Ven', 'Som'],
        dayNamesMin: ['Du', 'Gl', 'Ma', 'Me', 'Gi', 'Ve', 'So'],
        weekHeader: 'emna',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['rm']);
})($ektron);