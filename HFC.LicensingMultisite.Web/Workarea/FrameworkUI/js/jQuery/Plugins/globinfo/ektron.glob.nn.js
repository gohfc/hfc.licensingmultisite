(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["nn"] = $.extend(true, {}, en, {
            name: "nn",
            englishName: "Norwegian (Nynorsk)",
            nativeName: "norsk (nynorsk)",
            language: "nn",
            numberFormat: {
                ',': " ",
                '.': ",",
                percent: {
                    ',': " ",
                    '.': ","
                },
                currency: {
                    pattern: ["$ -n", "$ n"],
                    ',': " ",
                    '.': ",",
                    symbol: "kr"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': ".",
                    firstDay: 1,
                    days: {
                        names: ["søndag", "måndag", "tysdag", "onsdag", "torsdag", "fredag", "laurdag"],
                        namesAbbr: ["sø", "må", "ty", "on", "to", "fr", "la"],
                        namesShort: ["sø", "må", "ty", "on", "to", "fr", "la"]
                    },
                    months: {
                        names: ["januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember", ""],
                        namesAbbr: ["jan", "feb", "mar", "apr", "mai", "jun", "jul", "aug", "sep", "okt", "nov", "des", ""]
                    },
                    AM: null,
                    PM: null,
                    patterns: {
                        d: "dd.MM.yyyy",
                        D: "d. MMMM yyyy",
                        t: "HH:mm",
                        T: "HH:mm:ss",
                        f: "d. MMMM yyyy HH:mm",
                        F: "d. MMMM yyyy HH:mm:ss",
                        M: "d. MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["nn"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['nn'] = {
        closeText: 'Lukk',
        prevText: '&#xAB;Førre',
        nextText: 'Neste&#xBB;',
        currentText: 'I dag',
        monthNames: ['januar', 'februar', 'mars', 'april', 'mai', 'juni', 'juli', 'august', 'september', 'oktober', 'november', 'desember'],
        monthNamesShort: ['jan', 'feb', 'mar', 'apr', 'mai', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'des'],
        dayNamesShort: ['sun', 'mån', 'tys', 'ons', 'tor', 'fre', 'lau'],
        dayNames: ['sundag', 'måndag', 'tysdag', 'onsdag', 'torsdag', 'fredag', 'laurdag'],
        dayNamesMin: ['su', 'må', 'ty', 'on', 'to', 'fr', 'la'],
        weekHeader: 'Veke',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['nn']);
})($ektron);