(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["lt"] = $.extend(true, {}, en, {
            name: "lt",
            englishName: "Lithuanian",
            nativeName: "lietuvių",
            language: "lt",
            numberFormat: {
                ',': ".",
                '.': ",",
                percent: {
                    pattern: ["-n%", "n%"],
                    ',': ".",
                    '.': ","
                },
                currency: {
                    pattern: ["-n $", "n $"],
                    ',': ".",
                    '.': ",",
                    symbol: "Lt"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': ".",
                    firstDay: 1,
                    days: {
                        names: ["sekmadienis", "pirmadienis", "antradienis", "trečiadienis", "ketvirtadienis", "penktadienis", "šeštadienis"],
                        namesAbbr: ["Sk", "Pr", "An", "Tr", "Kt", "Pn", "Št"],
                        namesShort: ["S", "P", "A", "T", "K", "Pn", "Š"]
                    },
                    months: {
                        names: ["sausis", "vasaris", "kovas", "balandis", "gegužė", "birželis", "liepa", "rugpjūtis", "rugsėjis", "spalis", "lapkritis", "gruodis", ""],
                        namesAbbr: ["Sau", "Vas", "Kov", "Bal", "Geg", "Bir", "Lie", "Rgp", "Rgs", "Spl", "Lap", "Grd", ""]
                    },
                    monthsGenitive: {
                        names: ["sausio", "vasario", "kovo", "balandžio", "gegužės", "birželio", "liepos", "rugpjūčio", "rugsėjo", "spalio", "lapkričio", "gruodžio", ""],
                        namesAbbr: ["Sau", "Vas", "Kov", "Bal", "Geg", "Bir", "Lie", "Rgp", "Rgs", "Spl", "Lap", "Grd", ""]
                    },
                    AM: null,
                    PM: null,
                    patterns: {
                        d: "yyyy.MM.dd",
                        D: "yyyy 'm.' MMMM d 'd.'",
                        t: "HH:mm",
                        T: "HH:mm:ss",
                        f: "yyyy 'm.' MMMM d 'd.' HH:mm",
                        F: "yyyy 'm.' MMMM d 'd.' HH:mm:ss",
                        M: "MMMM d 'd.'",
                        Y: "yyyy 'm.' MMMM"
                    }
                })
            }
        }, cultures["lt"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['lt'] = {
        closeText: 'Uždaryti',
        prevText: '&#x3C;Atgal',
        nextText: 'Pirmyn&#x3E;',
        currentText: 'Šiandien',
        monthNames: ['Sausis', 'Vasaris', 'Kovas', 'Balandis', 'Gegužė', 'Birželis',
        'Liepa', 'Rugpjūtis', 'Rugsėjis', 'Spalis', 'Lapkritis', 'Gruodis'],
        monthNamesShort: ['Sau', 'Vas', 'Kov', 'Bal', 'Geg', 'Bir',
        'Lie', 'Rugp', 'Rugs', 'Spa', 'Lap', 'Gru'],
        dayNames: ['sekmadienis', 'pirmadienis', 'antradienis', 'trečiadienis', 'ketvirtadienis', 'penktadienis', 'šeštadienis'],
        dayNamesShort: ['sek', 'pir', 'ant', 'tre', 'ket', 'pen', 'šeš'],
        dayNamesMin: ['Se', 'Pr', 'An', 'Tr', 'Ke', 'Pe', 'Še'],
        weekHeader: 'SAV',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['lt']);
})($ektron);