(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["is"] = $.extend(true, {}, en, {
            name: "is",
            englishName: "Icelandic",
            nativeName: "íslenska",
            language: "is",
            numberFormat: {
                ',': ".",
                '.': ",",
                percent: {
                    pattern: ["-n%", "n%"],
                    ',': ".",
                    '.': ","
                },
                currency: {
                    pattern: ["-n $", "n $"],
                    decimals: 0,
                    ',': ".",
                    '.': ",",
                    symbol: "kr."
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': ".",
                    firstDay: 1,
                    days: {
                        names: ["sunnudagur", "mánudagur", "þriðjudagur", "miðvikudagur", "fimmtudagur", "föstudagur", "laugardagur"],
                        namesAbbr: ["sun.", "mán.", "þri.", "mið.", "fim.", "fös.", "lau."],
                        namesShort: ["su", "má", "þr", "mi", "fi", "fö", "la"]
                    },
                    months: {
                        names: ["janúar", "febrúar", "mars", "apríl", "maí", "júní", "júlí", "ágúst", "september", "október", "nóvember", "desember", ""],
                        namesAbbr: ["jan.", "feb.", "mar.", "apr.", "maí", "jún.", "júl.", "ágú.", "sep.", "okt.", "nóv.", "des.", ""]
                    },
                    AM: null,
                    PM: null,
                    patterns: {
                        d: "d.M.yyyy",
                        D: "d. MMMM yyyy",
                        t: "HH:mm",
                        T: "HH:mm:ss",
                        f: "d. MMMM yyyy HH:mm",
                        F: "d. MMMM yyyy HH:mm:ss",
                        M: "d. MMMM",
                        Y: "MMMM yyyy"
                    }
                })
            }
        }, cultures["is"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['is'] = {
        closeText: 'Loka',
        prevText: '&#x3C; Fyrri',
        nextText: 'Næsti &#x3E;',
        currentText: 'Í dag',
        monthNames: ['Janúar', 'Febrúar', 'Mars', 'Apríl', 'Maí', 'Júní',
        'Júlí', 'Ágúst', 'September', 'Október', 'Nóvember', 'Desember'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Maí', 'Jún',
        'Júl', 'Ágú', 'Sep', 'Okt', 'Nóv', 'Des'],
        dayNames: ['Sunnudagur', 'Mánudagur', 'Þriðjudagur', 'Miðvikudagur', 'Fimmtudagur', 'Föstudagur', 'Laugardagur'],
        dayNamesShort: ['Sun', 'Mán', 'Þri', 'Mið', 'Fim', 'Fös', 'Lau'],
        dayNamesMin: ['Su', 'Má', 'Þr', 'Mi', 'Fi', 'Fö', 'La'],
        weekHeader: 'Vika',
        dateFormat: 'dd.mm.yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['is']);
})($ektron);