(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["zh-HK"] = $.extend(true, {}, en, {
            name: "zh-HK",
            englishName: "Chinese (Traditional, Hong Kong S.A.R.)",
            nativeName: "中文(香港特別行政區)",
            language: "zh-CHT",
            numberFormat: {
                percent: {
                    pattern: ["-n%", "n%"]
                },
                currency: {
                    symbol: "HK$"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    days: {
                        names: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
                        namesAbbr: ["週日", "週一", "週二", "週三", "週四", "週五", "週六"],
                        namesShort: ["日", "一", "二", "三", "四", "五", "六"]
                    },
                    months: {
                        names: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月", ""],
                        namesAbbr: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月", ""]
                    },
                    AM: ["上午", "上午", "上午"],
                    PM: ["下午", "下午", "下午"],
                    eras: [{ "name": "公元", "start": null, "offset": 0 }],
                    patterns: {
                        d: "d/M/yyyy",
                        D: "yyyy'年'M'月'd'日'",
                        t: "H:mm",
                        T: "H:mm:ss",
                        f: "yyyy'年'M'月'd'日' H:mm",
                        F: "yyyy'年'M'月'd'日' H:mm:ss",
                        M: "M'月'd'日'",
                        Y: "yyyy'年'M'月'"
                    }
                })
            }
        }, cultures["zh-HK"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['zh-HK'] = {
        closeText: '關閉',
        prevText: '&#x3C;上月',
        nextText: '下月&#x3E;',
        currentText: '今天',
        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
        '七月', '八月', '九月', '十月', '十一月', '十二月'],
        monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月',
        '七月', '八月', '九月', '十月', '十一月', '十二月'],
        dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
        dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
        dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
        weekHeader: '周',
        dateFormat: 'dd-mm-yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: '年'
    };
    $.datepicker.setDefaults($.datepicker.regional['zh-HK']);
})($ektron);