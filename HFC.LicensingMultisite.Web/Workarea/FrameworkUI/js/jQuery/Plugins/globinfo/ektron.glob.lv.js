(function ($) {
    var cultures = $.global.cultures,
        en = cultures.en,
        standard = en.calendars.standard,
        culture = cultures["lv"] = $.extend(true, {}, en, {
            name: "lv",
            englishName: "Latvian",
            nativeName: "latviešu",
            language: "lv",
            numberFormat: {
                ',': " ",
                '.': ",",
                percent: {
                    pattern: ["-n%", "n%"],
                    ',': " ",
                    '.': ","
                },
                currency: {
                    pattern: ["-$ n", "$ n"],
                    ',': " ",
                    '.': ",",
                    symbol: "Ls"
                }
            },
            calendars: {
                standard: $.extend(true, {}, standard, {
                    '/': ".",
                    firstDay: 1,
                    days: {
                        names: ["svētdiena", "pirmdiena", "otrdiena", "trešdiena", "ceturtdiena", "piektdiena", "sestdiena"],
                        namesAbbr: ["sv", "pr", "ot", "tr", "ce", "pk", "se"],
                        namesShort: ["sv", "pr", "ot", "tr", "ce", "pk", "se"]
                    },
                    months: {
                        names: ["janvāris", "februāris", "marts", "aprīlis", "maijs", "jūnijs", "jūlijs", "augusts", "septembris", "oktobris", "novembris", "decembris", ""],
                        namesAbbr: ["jan", "feb", "mar", "apr", "mai", "jūn", "jūl", "aug", "sep", "okt", "nov", "dec", ""]
                    },
                    monthsGenitive: {
                        names: ["janvārī", "februārī", "martā", "aprīlī", "maijā", "jūnijā", "jūlijā", "augustā", "septembrī", "oktobrī", "novembrī", "decembrī", ""],
                        namesAbbr: ["jan", "feb", "mar", "apr", "mai", "jūn", "jūl", "aug", "sep", "okt", "nov", "dec", ""]
                    },
                    AM: null,
                    PM: null,
                    patterns: {
                        d: "yyyy.MM.dd.",
                        D: "dddd, yyyy'. gada 'd. MMMM",
                        t: "H:mm",
                        T: "H:mm:ss",
                        f: "dddd, yyyy'. gada 'd. MMMM H:mm",
                        F: "dddd, yyyy'. gada 'd. MMMM H:mm:ss",
                        M: "d. MMMM",
                        Y: "yyyy. MMMM"
                    }
                })
            }
        }, cultures["lv"]);
    culture.calendar = culture.calendars.standard;
    $.datepicker.regional['lv'] = {
        closeText: 'Aizvērt',
        prevText: 'Iepr.',
        nextText: 'Nāk.',
        currentText: 'Šodien',
        monthNames: ['Janvāris', 'Februāris', 'Marts', 'Aprīlis', 'Maijs', 'Jūnijs',
        'Jūlijs', 'Augusts', 'Septembris', 'Oktobris', 'Novembris', 'Decembris'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jūn',
        'Jūl', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
        dayNames: ['svētdiena', 'pirmdiena', 'otrdiena', 'trešdiena', 'ceturtdiena', 'piektdiena', 'sestdiena'],
        dayNamesShort: ['svt', 'prm', 'otr', 'tre', 'ctr', 'pkt', 'sst'],
        dayNamesMin: ['Sv', 'Pr', 'Ot', 'Tr', 'Ct', 'Pk', 'Ss'],
        weekHeader: 'Ned.',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['lv']);
})($ektron);